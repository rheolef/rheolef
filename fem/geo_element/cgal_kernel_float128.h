#ifndef _RHEO_CGAL_KERNEL_FLOAT128_H
#define _RHEO_CGAL_KERNEL_FLOAT128_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// Defines a cgal kernel for boost::multiprecision::float128
//
#if defined(_RHEOLEF_HAVE_FLOAT128) && defined(_RHEOLEF_HAVE_CGAL)
#include "rheolef/compiler.h"
#include <CGAL/utils.h>
#include <CGAL/utils_classes.h>
#include <CGAL/number_utils.h>
#include <CGAL/Gmpq.h>

#include <utility>
#include <cmath>
#include <limits>

#ifdef TODO
#include <math.h> // for nextafter
#endif // TODO

// -----------------------------------------------------------------
// extend boost/multiprecision/float128.hpp to work with CGAL::gmpq
// -----------------------------------------------------------------
namespace boost{ namespace multiprecision{ namespace backends{
template<>
inline void eval_convert_to(CGAL::Gmpq* result, const float128_backend& val)
{
   // TODO CGAL: float128 -> double -> gmpq: not ideal but try it
   *result = static_cast<CGAL::Gmpq>(double(val.value()));
}
}}} // namespace boost::multiprecision::backends

// ---------------------------------------------------------
// extend CGAL to work with boost::multiprecision::float128
// ---------------------------------------------------------
namespace CGAL {
using boost::multiprecision::float128;

template<>
class Is_valid<float128>: public std::unary_function<float128,bool> {
public:
  bool operator()( const float128& x ) const { return ! boost::math::isnan(x); }
};

template<>
class Algebraic_structure_traits<float128>
  : public Algebraic_structure_traits_base<float128,Field_with_kth_root_tag>  {
public:
  typedef Tag_false            Is_exact;
  typedef Tag_true             Is_numerical_sensitive;

  class Sqrt: public std::unary_function<Type,Type> {
    public:
      Type operator() (const Type& x) const { return sqrt(x); }
  };
  class Kth_root: public std::binary_function<int,Type,Type> {
    public:
      Type operator() (int k, const Type& x) const {
        CGAL_precondition_msg(k > 0, "'k' must be positive for k-th roots");
        return pow(x, 1.0/float128(k));
      }
  };
};
template<>
class Real_embeddable_traits<float128>
  : public INTERN_RET::Real_embeddable_traits_base<float128,CGAL::Tag_true> {
public:
    class Abs: public std::unary_function<Type,Type> {
      public:
        Type operator() (const Type& x) const {
          return fabs(x);
        }
    };
    class Is_finite: public std::unary_function<Type,bool> {
      public :
        bool operator() (const Type& x) const {
          return boost::math::isfinite(x);
        }
    };
};
inline
bool
is_integer(float128 d)
{
  return boost::math::isfinite(d) && (ceil(d) == d);
}

// Returns a pair of integers <num,den> such that d == num/den.
inline
std::pair<float128, float128>
split_numerator_denominator(float128 d)
{
  // Note that it could probably be optimized.
  float128 num = d;
  float128 den = 1.0;
  while (ceil(num) != num) {
    num *= 2.0;
    den *= 2.0;
  }
  CGAL_postcondition(d == num/den);
  return std::make_pair(num, den);
}
#ifdef TODO
inline
float128
nextafter(float128 d1, float128 d2)
{
  return ::nextafter(d1,d2);
}
#endif // TODO

} //namespace CGAL
#endif // _RHEOLEF_HAVE_FLOAT128 && _RHEOLEF_HAVE_CGAL
#endif // _RHEO_CGAL_KERNEL_FLOAT128_H
