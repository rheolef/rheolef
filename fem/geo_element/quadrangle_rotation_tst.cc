///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo_element.h"
#include "rheolef/compiler_eigen.h"

using namespace std;
using namespace rheolef;
using namespace Eigen;

void dump (size_t order, int orient, size_t shift) {
  if (orient != 1 && orient != -1) {
    cerr << "invalid orientation " << orient << endl;
    exit (1);
  }
  if (shift > 3) {
    cerr << "invalid shift " << shift << endl;
    exit (1);
  }
  Array<size_t,Dynamic,Dynamic> rot (order+1,order+1);
  size_t coord[4];
  for (size_t j = 1; j <= order-1; j++) {
    for (size_t i = 1; i <= order-1; i++) {
      size_t loc_iqua_j = (order-1)*(j-1) + (i-1);
      rot(i,j) = geo_element::fix_quadrangle_indirect (orient, shift, order, loc_iqua_j);
    }
  }
  cout << "order = "<<order<<", shift = " << shift << ", orientation = " << orient << endl;
  for (size_t j = order-1; j >= 1; j--) {
    for (size_t i = 1; i <= order-1; i++) {
      cout << setw(5) << rot(i,j) << " ";
    }
    cout << endl;
  }
  cout << endl;
}
int main(int argc, char**argv) {
  if (argc < 2) {
    cerr << "usage: prog order orient shift" << endl;
    exit (1);
  }
  size_t order  = argc > 1 ? atoi(argv[1]) : 5;
  for (size_t shift = 0; shift < 4; shift++) {
    dump (order,  1, shift);
    dump (order, -1, shift);
  }
}
