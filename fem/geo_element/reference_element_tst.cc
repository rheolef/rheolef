///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/reference_element.h"
//#include "rheolef/geo_element.h"
using namespace rheolef;
using namespace std;

int main() {
    typedef reference_element::size_type size_type;
    char elt[] = { 'p', 'e', 't', 'q', 'T', 'P', 'H' };

    const size_t max_n_subgeo = 12;
    const size_t max_subgeo_vertex = 8;
    for (size_type k = 0; k < reference_element::max_variant; k++) {
      reference_element K;
      K.set_name(elt[k]);
      cout << K.name() << endl; 
      for (size_type d = 0; d < 4; d++) {
 	 cout << "   subgeo " << d << "D: " << endl; 
         for (size_type i = 0; i < K.n_subgeo(d); i++) {
 	   cout << "    " << i << ": "; 
           for (size_type j = 0; j < K.subgeo_size(d,i); j++) {
 	     size_type vertex = K.subgeo_local_vertex (d,i,j);
	     cout << int(vertex);
	   }
           for (size_type j = K.subgeo_size(d,i); j < max_subgeo_vertex; j++) {
	     cout << "_";
	   }
	   cout << endl;
         }
         for (size_type i = K.n_subgeo(d); i < max_n_subgeo; i++) {
 	   cout << "    " << i << ": "; 
           for (size_type j = 0; j < max_subgeo_vertex; j++) {
	     cout << "_";
	   }
	   cout << endl;
         }
      }
   }
   return 0;
}

