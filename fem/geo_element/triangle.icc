#ifndef _RHEO_TRIANGLE_ICC
#define _RHEO_TRIANGLE_ICC
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

/**
@femclassfile triangle reference element
@addindex reference element

Description
===========
The triangle @ref reference_element_6 is `K = [0,1]`.
@verbatim
        K = { 0 < x0 < 1 and 0 < x1 < 1-x0 }
  
        x1
        2
        | +
        |   +
        |     +
        |       +
        0---------1  x0 
@endverbatim
This two-dimensional @ref reference_element_6 is then transformed,
after the Piola geometrical application, as a triangle
in a 2D or 3D physical space, as a @ref geo_element_6.

Curved high order transformed @ref geo_element_6
Pk triangle (k >= 1) are supported for 2D or 3D geometries.
In these cases, the nodes of an high-order triangle are numbered as:

Note that high-order triangles have additional edge-nodes and face-nodes.
These nodes are numbered as: first vertices, then edge-nodes, following
the edge numbering order and orientation, and finally the face internal nodes,
following the triangle lattice.
@verbatim
      2               2               2
      | +             | +             | +
      |   +           7   6           9   8
      5     4         |     +        10 14  7
      |       +       8   9   5      11 12 13 6
      |         +     |         +     |         +
      0-----3-----1   0---3---4---1   0--3--4--5--1
            P2              P3              P4
@endverbatim

Implementation
==============
@showfromfile
@snippet triangle.icc verbatim_triangle
*/

// [verbatim_triangle]
const size_t dimension = 2;
const Float  measure = 0.5;
const size_t n_vertex = 3;
const point vertex [n_vertex] = {
	point(0, 0),
	point(1, 0),
	point(0, 1) };
const size_t  n_edge = 3;
const size_t edge [n_edge][2] = {
	{ 0, 1 },
	{ 1, 2 },
	{ 2, 0 } };
// [verbatim_triangle]

#endif // _RHEO_TRIANGLE_ICC
