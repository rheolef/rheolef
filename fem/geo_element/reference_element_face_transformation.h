#ifndef _RHEO_REFERENCE_ELEMENT_FACE_TRANSFORMATION_H
#define _RHEO_REFERENCE_ELEMENT_FACE_TRANSFORMATION_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/reference_element.h"
#include "rheolef/geo_element_indirect.h"
#include "rheolef/point.h"

namespace rheolef {

// --------------------------------------------------------------------------
// Let S and K such that S is the i-th side of K
// then map a point hat_x defined in the reference element side hat_S
// into tilde_x defined in the reference element tilde_K
// note: used to build a quadrature formulae on a side of K
// --------------------------------------------------------------------------

struct side_information_type {
    size_t                                 loc_isid;
    size_t                                 shift;
    geo_element_indirect::orientation_type orient;
    size_t                                 dim;
    size_t                                 n_vertex;
    reference_element                      hat;
  side_information_type()
    : loc_isid (std::numeric_limits<size_t>::max()),
      shift(0),
      orient(1),
      dim(0),
      n_vertex(0),
      hat()
  {}
  side_information_type (reference_element hat_K, size_t loc_isid0)
    : loc_isid (std::numeric_limits<size_t>::max()),
      shift(0),
      orient(1),
      dim(0),
      n_vertex(0),
      hat()
  { reset (hat_K, loc_isid0); }
  void reset (reference_element hat_K, size_t loc_isid0);
};
inline
void
side_information_type::reset (
  reference_element      hat_K,
  size_t                 loc_isid0)
{
    loc_isid = loc_isid0;
    hat      = hat_K.side (loc_isid0);
    dim      = hat.dimension();
    n_vertex = hat.size();
    shift    = 0;
    orient   = 1;
}

// --------------------------------------------------------------------------
// local piola-like transformation:
//   side_hat_x <--> tilde_x
// --------------------------------------------------------------------------
template<class T>
point_basic<T>
reference_element_face_transformation (
  reference_element                      tilde_K,
  const side_information_type&           sid,
  const point_basic<T>&	   	         sid_hat_x);

template<class T>
point_basic<T>
reference_element_face_inverse_transformation (
  reference_element                      tilde_K,
  const side_information_type&           sid,
  const point_basic<T>&	   	         tilde_x);

// --------------------------------------------------------------------------
// side_ilat --> ilat : lattice version, with integers instead of floats
// --------------------------------------------------------------------------
point_basic<size_t>
reference_element_face_transformation (
  reference_element             tilde_K,
  const side_information_type&  sid,
  size_t                        k,
  const point_basic<size_t>&    sid_ilat);

} // namespace rheolef
#endif // _RHEO_REFERENCE_ELEMENT_FACE_TRANSFORMATION_H
