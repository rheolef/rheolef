///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// intel c++ portability issue:
//
// point.h declares friend norm(point x) member and this implementations
// generates a buggy result. It has been replaced by an extern implementation
// of norm(point x) and the result is portable.
//
#include "rheolef/point.h"
using namespace std;
using namespace rheolef;
Float phi_1 (const point& x) { return norm(x) - 1; }
Float phi_2 (const point& x) { return sqrt(sqr(x[0])+sqr(x[1])+sqr(x[2])) - 1; }
int main (int argc, char**argv) {
  Float tol = sqrt(std::numeric_limits<Float>::epsilon());
  point x (1.,2.,3.);
  Float p  = sqrt(Float(14))-1;
  Float p1 = phi_1(x);
  Float p2 = phi_2(x);
  Float err = fabs(p-p1);
#ifdef TO_CLEAN
  cout << "phi  (x)="<<p <<endl;
  cout << "phi_2(x)="<<p2<<endl;
  cout << "phi_1(x)="<<p1<<endl;
  cout << "err     ="<<err<<endl;
#endif // TO_CLEAN
  return (err < tol) ? 0 : 1;
}
