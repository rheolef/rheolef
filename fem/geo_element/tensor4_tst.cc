///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/tensor4.h"
using namespace std;
using namespace rheolef;
int main (int argc, char**argv) {
  tensor4 a; 
  for (size_t i = 0; i < 3; i++) for (size_t j = 0; j < 3; j++){
      for (size_t k = 0; k < 3; k++) for (size_t l = 0; l < 3; l++){
          a(i,j,k,l) = 1;
      }
  }
  tensor4 b = a;
  cout << "b(0,0,0,0) = " << b(1,0,2,0) << endl;  
  tensor4 c;
  cout << "c(0,0,0,0) = " << c(0,0,2,0) << endl;  
  for (size_t i = 0; i < 3; i++) for (size_t j = 0; j < 3; j++){
      for (size_t k = 0; k < 3; k++) for (size_t l = 0; l < 3; l++){
          c(i,j,k,l) = i+j+k+l;
      }
  }
  cout << "le tenseur c  = "   << endl;  
  for (size_t i = 0; i < 3; i++) for (size_t j = 0; j < 3; j++){
      for (size_t k = 0; k < 3; k++) for (size_t l = 0; l < 3; l++){
	  cout  << " " << c(i,j,k,l) ;  
         
      }
      cout << endl; 
  }
}
