///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/quadrature.h"
#include "rheolef/gauss_jacobi.h"
namespace rheolef {
using namespace std;

/* ------------------------------------------
 * Gauss quadrature formulae on the triangle
 * order r : exact for all polynoms of P_r
 *
 * References for optimal formulae :
 *   1)  G. Dhatt and G. Touzot,
 *       Une presentation de la methode des elements finis,
 *       Maloine editeur, Paris
 *       Deuxieme edition,
 *       1984,
 *       page 293
 *   2)  M. Crouzeix et A. L. Mignot
 *       Exercices d'analyse des equations differentielles,
 *       Masson,
 *       1986,
 *       p. 63
 * ------------------------------------------ 
 */
template<class T>
void
quadrature_on_geo<T>::init_square  (quadrature_option opt)
{
  quadrature_option::family_type f = opt.get_family();
  // -------------------------------------------------------------------------
  // special case : equispaced, for irregular (e.g. Heaviside) functions
  // -------------------------------------------------------------------------
  if (f == quadrature_option::equispaced) {
    size_type r = opt.get_order();
    if (r == 0) {
      wx (x(0,0), 4);
    } else {
      size_type n = (r+1)*(r+1);
      T w = 4/T(int(n));
      for (size_type i = 0; i <= r; i++) {
        for (size_type j = 0; j <= r; j++) {
          wx (x(2*T(int(i))/r-1,2*T(int(j))/r-1), w);
        }
      }
    }
    return;
  }
  // -------------------------------------------------------------------------
  // TODO: special case : superconvergent patch recovery nodes & weights
  // -------------------------------------------------------------------------

  // -------------------------------------------------------------------------
  // default: gauss
  // -------------------------------------------------------------------------
  check_macro (f == quadrature_option::gauss,
        "unsupported quadrature family \"" << opt.get_family_name() << "\"");
 
  // Gauss-Legendre quadrature formulae 
  //  where Legendre = Jacobi(alpha=0,beta=0) polynoms
  // when using n nodes : quadrature formulae order is 2*r-1
  size_type n = n_node_gauss(opt.get_order());
  vector<T> zeta(n), omega(n);
  gauss_jacobi (n, 0, 0, zeta.begin(), omega.begin());
  for (size_type i = 0; i < n; i++)
    for (size_type j = 0; j < n; j++)
      wx (x(zeta[i], zeta[j]), omega[i]*omega[j]);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template void quadrature_on_geo<T>::init_square (quadrature_option);

_RHEOLEF_instanciation(Float)

}// namespace rheolef
