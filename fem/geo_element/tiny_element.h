# ifndef _RHEO_TINY_ELEMENT_H
# define _RHEO_TINY_ELEMENT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// small element without memory allocation call
// used for i/o of meshes, since geo_element
// has additional data and performs mem alloc
//
// authors: Pierre.Saramito@imag.fr
//
// date: 3 march 2001
//
#include "rheolef/reference_element.h"

namespace rheolef { 
class tiny_element : public reference_element {
public:
    tiny_element(variant_type t = max_variant);
    explicit tiny_element (const class geo_element&);
    void set_variant (variant_type t);
    void set_name (char name);
    void set_index (size_t idx) { _idx = idx; }
    void set_variant (size_type n_vertex, size_type dim);
    const size_type& operator[] (size_type i) const { return t_[i]; }
    size_t index () const { return _idx; }
    size_type& operator[] (size_type i) { return t_[i]; }
    friend std::ostream& operator << (std::ostream& os, const tiny_element& K);
protected:
    size_type t_ [8];
    size_t    _idx;
};
inline
tiny_element::tiny_element (variant_type t)
 : reference_element()
{
#ifdef _RHEOLEF_PARANO
	std::fill (t_, t_+8, std::numeric_limits<size_type>::max());
#endif // _RHEOLEF_PARANO
}
inline
void
tiny_element::set_variant (variant_type t)
{
	reference_element::set_variant(t);
}
inline
void
tiny_element::set_name (char name)
{
    reference_element::set_name(name);
}
inline
void
tiny_element::set_variant (size_type n_vertex, size_type dim)
{
    reference_element::set_variant(n_vertex,dim);
}
}// namespace rheolef
# endif /* _RHEO_TINY_ELEMENT_H */
