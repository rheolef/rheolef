///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
#include "rheolef/tensor.h"
using namespace rheolef;
using namespace std;

Float check(string name, const tensor& a, const tensor& exp_a_exact, size_t dim) 
{
  tensor exp_a = exp(a,dim);
  Float err = norm(exp_a_exact - exp_a);
  cout << name << "="<<endl; a.put(cout,dim); cout <<endl;
  cout << "exp_"<<name<<"="<<endl; exp_a.put(cout,dim); cout<<endl;
  cout << "exp_"<<name<<"_exact="<<endl; exp_a_exact.put(cout,dim); cout <<endl;
  cerr << "err="<<err<<endl<<endl;
  return err;
}
/* 2d case:
                                   [ 1  1 ]
                   A =             [      ]
                                   [ 1  1 ]

                   [        2                             ]
                   [      %e  + 1       (%e - 1) (%e + 1) ]
                   [      -------       ----------------- ]
                   [         2                  2         ]
 exp(A) =          [                                      ]
                   [                           2          ]
                   [ (%e - 1) (%e + 1)       %e  + 1      ]
                   [ -----------------       -------      ]
                   [         2                  2         ]
*/
int test2d() {
  Float tol = 1e-7, err = 0, e = exp(1);
  tensor a = {{1,1},
              {1,1}};
  tensor exp_a = {{(e*e+1)/2, (e*e-1)/2},
                  {(e*e-1)/2, (e*e+1)/2}};
  err += check ("a", a, exp_a, 2);
  // checked with maxima(explicit) and octave(pade' approx)
  tensor b = {{1,1},{1,2}};
  tensor exp_b = {{4.849205464247511, 5.475496883011693},
                  {5.475496883011693, 10.3247023472592 }};
  err += check ("b", b, exp_b, 2);
  tensor c = {{1,3},{3,2}};
  tensor exp_c = {{39.32280970803396, 46.16630143888589},
                  {46.16630143888589, 54.71157685432926}};
  err += check ("c", c, exp_c, 2);
  return (err < tol) ? 0 : 1;
}
/* 3d case: checked with maxima:

    a :matrix([1,1,1], [1,1,1], [1,1,1]);
    matrixexp(a);

                                  [ 1  1  1 ]
                                  [         ]
                A =               [ 1  1  1 ]
                                  [         ]
                                  [ 1  1  1 ]

                         [   3        3        3     ]
                         [ %e  + 2  %e  - 1  %e  - 1 ]
                         [ -------  -------  ------- ]
                         [    3        3        3    ]
                         [                           ]
                         [   3        3        3     ]
                         [ %e  - 1  %e  + 2  %e  - 1 ]
          exp(A) =       [ -------  -------  ------- ]
                         [    3        3        3    ]
                         [                           ]
                         [   3        3        3     ]
                         [ %e  - 1  %e  - 1  %e  + 2 ]
                         [ -------  -------  ------- ]
                         [    3        3        3    ]

 */
int test3d() {
  Float tol = 1e-7, err = 0, e = exp(1), e3 = e*e*e;
  tensor a = {{1,1,1},{1,1,1},{1,1,1}};
  tensor exp_a = {{(e3+2)/3, (e3-1)/3, (e3-1)/3},
                  {(e3-1)/3, (e3+2)/3, (e3-1)/3},
                  {(e3-1)/3, (e3-1)/3, (e3+2)/3}};
  err += check ("a", a, exp_a, 3);
  return (err < tol) ? 0 : 1;
}
int main(int argc, char**argv) {
  return test2d() == 0 && test3d() == 0 ? 0 : 1;
}
