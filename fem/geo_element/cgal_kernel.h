#ifndef _RHEO_CGAL_KERNEL_H
#define _RHEO_CGAL_KERNEL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// defines a cutsom CGAL kernel by using rheolef::point_basic<T>
// => avoid copy of coordinates
//
// Pierre.Saramito@imag.fr
//
// 12 march 2012
//
// References:
//       https://lists-sop.inria.fr/sympa/arc/cgal-discuss/2010-08/msg00205.html
//       examples/Kernel_23/MyKernel.h
// 
// 	https://doc.cgal.org/latest/Kernel_23/index.html#Section_11.5

// cicumvents debian bug #683975 in boost-1.49 (fixed in boost-1.50 or 1.53)
#ifndef CGAL_HAS_NO_THREADS
#define CGAL_HAS_NO_THREADS
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wignored-attributes"
#include <CGAL/Cartesian.h>
#pragma GCC diagnostic pop

#include "rheolef/point.h"

namespace rheolef { namespace custom_cgal {

// -------------------------------------------------------------------------
// 1) segment
// -------------------------------------------------------------------------

template <class R_>
class MySegmentC2
{
  typedef typename R_::FT                   FT;
  typedef typename R_::Point_2              Point_2;
  typedef typename R_::Vector_2             Vector_2;
  typedef typename R_::Direction_2          Direction_2;
  typedef typename R_::Line_2               Line_2;
  typedef typename R_::Segment_2            Segment_2;
  typedef typename R_::Aff_transformation_2 Aff_transformation_2;

  Point_2 sp_, tp_;
public:
  typedef R_                                     R;

  MySegmentC2() {}

  MySegmentC2(const Point_2 &sp, const Point_2 &tp)
    : sp_(sp), tp_(tp) {}

  bool        is_horizontal() const;
  bool        is_vertical() const;
  bool        has_on(const Point_2 &p) const;
  bool        collinear_has_on(const Point_2 &p) const;

  bool        operator==(const MySegmentC2 &s) const;
  bool        operator!=(const MySegmentC2 &s) const;

  const Point_2 &   source() const
  {
      return sp_;
  }

  const Point_2 &   target() const
  {
      return tp_;
  }
  const Point_2 &    start() const;
  const Point_2 &    end() const;

  const Point_2 &   min () const;
  const Point_2 &   max () const;
  const Point_2 &   vertex(int i) const;
  const Point_2 &   point(int i) const;
  const Point_2 &   operator[](int i) const;

  FT          squared_length() const;

  Direction_2 direction() const;
  Vector_2    to_vector() const;
  Line_2      supporting_line() const;
  Segment_2   opposite() const;

  Segment_2   transform(const Aff_transformation_2 &t) const
  {
    return Segment_2(t.transform(source()), t.transform(target()));
  }

  bool        is_degenerate() const;
  CGAL::Bbox_2      bbox() const;
};
template < class R >
inline
bool
MySegmentC2<R>::operator==(const MySegmentC2<R> &s) const
{
  return source() == s.source() && target() == s.target();
}
template < class R >
inline
bool
MySegmentC2<R>::operator!=(const MySegmentC2<R> &s) const
{
  return !(*this == s);
}
template < class R >
inline
const typename MySegmentC2<R>::Point_2 &
MySegmentC2<R>::min () const
{
  typename R::Less_xy_2 less_xy;
  return less_xy(source(),target()) ? source() : target();
}
template < class R >
inline
const typename MySegmentC2<R>::Point_2 &
MySegmentC2<R>::max () const
{
  typename R::Less_xy_2 less_xy;
  return less_xy(source(),target()) ? target() : source();
}
template < class R >
inline
const typename MySegmentC2<R>::Point_2 &
MySegmentC2<R>::vertex(int i) const
{
  return (i%2 == 0) ? source() : target();
}
template < class R >
inline
const typename MySegmentC2<R>::Point_2 &
MySegmentC2<R>::point(int i) const
{
  return (i%2 == 0) ? source() : target();
}
template < class R >
inline
const typename MySegmentC2<R>::Point_2 &
MySegmentC2<R>::operator[](int i) const
{
  return vertex(i);
}
template < class R >
inline
typename MySegmentC2<R>::FT
MySegmentC2<R>::squared_length() const
{
  typename R::Compute_squared_distance_2 squared_distance;
  return squared_distance(source(), target());
}
template < class R >
inline
typename MySegmentC2<R>::Direction_2
MySegmentC2<R>::direction() const
{
  typename R::Construct_vector_2 construct_vector;
  return Direction_2( construct_vector( source(), target()));
}
template < class R >
inline
typename MySegmentC2<R>::Vector_2
MySegmentC2<R>::to_vector() const
{
  typename R::Construct_vector_2 construct_vector;
  return construct_vector( source(), target());
}
template < class R >
inline
typename MySegmentC2<R>::Line_2
MySegmentC2<R>::supporting_line() const
{
  typename R::Construct_line_2 construct_line;

  return construct_line(*this);
}
template < class R >
inline
typename MySegmentC2<R>::Segment_2
MySegmentC2<R>::opposite() const
{
  return MySegmentC2<R>(target(), source());
}
template < class R >
inline
CGAL::Bbox_2
MySegmentC2<R>::bbox() const
{
  return source().bbox() + target().bbox();
}
template < class R >
inline
bool
MySegmentC2<R>::is_degenerate() const
{
  return R().equal_y_2_object()(source(), target());
}
template < class R >
inline
bool
MySegmentC2<R>::is_horizontal() const
{
  return R().equal_y_2_object()(source(), target());
}
template < class R >
inline
bool
MySegmentC2<R>::is_vertical() const
{
  return R().equal_x_2_object()(source(), target());
}
template < class R >
inline
bool
MySegmentC2<R>::
has_on(const typename MySegmentC2<R>::Point_2 &p) const
{
  return R().collinear_are_ordered_along_line_2_object()(source(), p, target());
}
template < class R >
inline
bool
MySegmentC2<R>::
collinear_has_on(const typename MySegmentC2<R>::Point_2 &p) const
{
  return R().collinear_has_on_2_object()(*this, p);
}
template < class R >
std::ostream &
operator<<(std::ostream &os, const MySegmentC2<R> &s)
{
#if   CGAL_VERSION_NR >= 1041101000
    switch(CGAL::get_mode(os)) {
#elif CGAL_VERSION_NR >= 1040801000
    switch(os.iword(CGAL::IO::get_static_mode())) {
#else
    switch(os.iword(CGAL::IO::mode)) {
#endif
    case CGAL::IO::ASCII :
        return os << s.source() << ' ' << s.target();
    case CGAL::IO::BINARY :
        return os << s.source() << s.target();
    default:
        return os << "MySegmentC2(" << s.source() <<  ", " << s.target() << ")";
    }
}
template < class R >
std::istream &
operator>>(std::istream &is, MySegmentC2<R> &s)
{
    typename R::Point_2 p, q;

    is >> p >> q;

    if (is)
	s = MySegmentC2<R>(p, q);
    return is;
}
// -------------------------------------------------------------------------
// 2) bbox
// -------------------------------------------------------------------------
template <class ConstructBbox_2>
class MyConstruct_bbox_2 : public ConstructBbox_2 {
public:
  using ConstructBbox_2::operator();
  CGAL::Bbox_2 operator()(const point_basic<Float>& p) const {
    return CGAL::Bbox_2(p.x(), p.y(), p.x(), p.y());
  }
};
// -------------------------------------------------------------------------
// 3) coordinate iterator
// -------------------------------------------------------------------------
template <class T>
class MyConstruct_coord_iterator {
public:
  const T* operator() (const point_basic<T>& p) { return &p.x(); }
  const T* operator() (const point_basic<T>& p, int) {
    const T* pyptr = &p.y();
    pyptr++;
    return pyptr;
  }
};
// -------------------------------------------------------------------------
// 4) construct point
// -------------------------------------------------------------------------
template <typename K, typename OldK>
class MyConstruct_point_2
{
  typedef typename K::RT         RT;
  typedef typename K::Point_2    Point_2;
  typedef typename K::Line_2     Line_2;
  typedef typename Point_2::Rep  Rep;
public:
  typedef Point_2                result_type;

  // Note : the CGAL::Return_base_tag is really internal CGAL stuff.
  // Unfortunately it is needed for optimizing away copy-constructions,
  // due to current lack of delegating constructors in the C++ standard.
  Rep // Point_2
  operator() (CGAL::Return_base_tag, CGAL::Origin o) const
  { return Rep(o); }

  Rep // Point_2
  operator() (CGAL::Return_base_tag, const RT& x, const RT& y) const
  { return Rep(x, y); }

#ifdef TO_CLEAN
  Rep // Point_2
  operator() (CGAL::Return_base_tag, const RT& x, const RT& y, const RT& w) const
  { return Rep(x, y); }
#endif // TO_CLEAN

  Point_2
  operator()(const CGAL::Origin& o) const
  { return point_basic<RT>(0, 0); }

  Point_2
  operator()(const RT& x, const RT& y) const
  {
    return point_basic<RT>(x, y);
  }

  const Point_2&
  operator()(const Point_2 & p) const
  {
    return p;
  }

  Point_2
  operator()(const Line_2& l) const
  {
    typename OldK::Construct_point_2 base_operator;
    Point_2 p = base_operator(l);
    return p;
  }

  Point_2
  operator()(const Line_2& l, int i) const
  {
    typename OldK::Construct_point_2 base_operator;
    return base_operator(l, i);
  }

  // We need this one, as such a functor is in the Filtered_kernel
  Point_2
  operator() (const RT& x, const RT& y, const RT& w) const
  {
    if(w != 1){
      return point_basic<RT>(x/w, y/w);
    } else {
      return point_basic<RT>(x,y);
    }
  }
};
// -------------------------------------------------------------------------
// cartesian kernel
// -------------------------------------------------------------------------
template <typename NewKernel, typename BaseKernel>
class my_cartesian2d_base : public BaseKernel::template Base<NewKernel>::Type {
  typedef typename BaseKernel::template Base<NewKernel>::Type   OldKernel;
public:
  typedef typename BaseKernel::FT           FT;
  typedef NewKernel                         Kernel;
  typedef point_basic<FT>                   Point_2;
#ifdef TODO
  typedef point_basic<FT>                   Point_1;
  typedef point_basic<FT>                   Point_3;
#endif // TODO
  typedef MySegmentC2<Kernel>               Segment_2;
  typedef MyConstruct_bbox_2<typename OldKernel::Construct_bbox_2> Construct_bbox_2;
  typedef MyConstruct_coord_iterator<FT>    Construct_cartesian_const_iterator_2;
  typedef const FT*                         Cartesian_const_iterator_2;
  typedef MyConstruct_point_2<Kernel, OldKernel>       Construct_point_2;

  Construct_point_2 construct_point_2_object() const { return Construct_point_2(); }
  Construct_bbox_2  construct_bbox_2_object()  const { return Construct_bbox_2(); }
  Construct_cartesian_const_iterator_2 construct_cartesian_const_iterator_2_object() const
  { return Construct_cartesian_const_iterator_2(); }

  template <typename Kernel2>
  struct Base {
    typedef my_cartesian2d_base<Kernel2, BaseKernel>  Type;
  };
};
template <typename FT_>
struct kernel_2d
  : public CGAL::Type_equality_wrapper<
                my_cartesian2d_base<kernel_2d<FT_>, CGAL::Cartesian<FT_> >,
                kernel_2d<FT_> >
{};
template <typename NewKernel, typename BaseKernel>
class my_cartesian3d_base : public BaseKernel::template Base<NewKernel>::Type {
  typedef typename BaseKernel::template Base<NewKernel>::Type   OldKernel;
public:
  typedef typename BaseKernel::FT           FT;
  typedef NewKernel                         Kernel;
#ifdef TODO
  typedef point_basic<FT>                   Point_2;
  typedef point_basic<FT>                   Point_1;
#endif // TODO
  typedef point_basic<FT>                   Point_3;
#ifdef TODO
  typedef MySegmentC2<Kernel>               Segment_2;
  typedef MyConstruct_bbox_2<typename OldKernel::Construct_bbox_2> Construct_bbox_2;
  typedef MyConstruct_coord_iterator<FT>    Construct_cartesian_const_iterator_2;
  typedef const FT*                         Cartesian_const_iterator_2;
  typedef MyConstruct_point_2<Kernel, OldKernel>       Construct_point_2;

  Construct_point_2 construct_point_2_object() const { return Construct_point_2(); }
  Construct_bbox_2  construct_bbox_2_object()  const { return Construct_bbox_2(); }
  Construct_cartesian_const_iterator_2 construct_cartesian_const_iterator_2_object() const
  { return Construct_cartesian_const_iterator_2(); }
#endif // TODO

  template <typename Kernel2>
  struct Base {
    typedef my_cartesian3d_base<Kernel2, BaseKernel>  Type;
  };
};
template <typename FT_>
struct kernel_3d
  : public CGAL::Type_equality_wrapper<
                my_cartesian3d_base<kernel_3d<FT_>, CGAL::Cartesian<FT_> >,
                kernel_3d<FT_> >
{};

}} // namespace rheolef::custom_cgal
#endif // _RHEO_CGAL_KERNEL_H
