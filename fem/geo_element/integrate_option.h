#ifndef _RHEO_INTEGRATE_OPTION_H
#define _RHEO_INTEGRATE_OPTION_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@functionfile integrate_option expression integration options
@addindex quadrature formulae
@addindex axisymmetric geometry
@addindex matrix inversion
@addindex invert
@addindex mass lumping

Description
===========
This class sends options to the @ref integrate_3 function,
when building a @ref form_2 or a @ref field_2.

Its allows one to choose the quadrature formula used
during the numerical integration (Gauss, Gauss-Lobatto, etc)
and the polynomial degree that is exactly integrated.
This exactly integrated polynomial degree is called here the `order` of the quadrature formula.
See also @ref quadrature_1 for examples of quadrature formula.

In addition to the customization of the quadrature formula,
the present class provides some booleaan flags, e.g.
computation of the inverse matrix.

Quadrature families
===================
`gauss`
>	The Gauss formula is the default.
`gauss_lobatto`
>	The Gauss-Lobatto formula is useful in some cases,
>	e.g. when using the method of @ref characteristic_2.
>	It is actually implemented for order <= 2.
`gauss_radau`
>	The Gauss-Radau formula is another classical alternative.
`middle_edge`
>	The nodes are located at the midle edge.
>	This formula is useful in some special cases
>	and its order is limited.
`superconvergent`
>	Another special case, for testing superconvergence of
>	the finite element method at some specific nodes.
`equispaced`
>	When the solution has low regularity, e.g. is discontinuous
>	across elements, this simple formula is also useful.
>	In that case, the `order` parameter refers to the number of
>	nodes  used and not to the degree of polynomial exactly integated.
>	For instance, `order=1` refers to the trapezoidal formulae
>	and for the general case, there are `order+1` nodes per edges.

Boolean flags
=============
`invert`
>	This flag, when set, performs a local inversion
>	on the matrix at the element level:
>	This procedure is allowed only when the global matrix
>	is block diagonal, e.g. for discontinuous or bubble approximations.
>	This property is true when basis functions have a compact support
>	inside exactly one element.
>	Default is `invert=false`.

`ignore_sys_coord`
>	This flag has effects only for axisymmetric coordinate systems.
>	When set, it omits the `r` weight in the `r dr dz` measure
>	during the numerical integration performed the @ref integrate_3 function.
>	This feature is useful for computing the stream function
>	in the axisymmetric case.
>	Default is `ignore_sys_coord=false`.

`lump`
>	This flag, when set, performs a *mass lumping* procedure
>	on the matrix at the element level:

	a(i,i) += sum(j!=i) a(i,j)

>	The resulting matrix is diagonal.
>	This feature is useful for computing a diagonal approximation
>	of the mass matrix for the continuous `P1` element.
>	Default is `lump=false`.

Implementation
==============
@showfromfile
@snippet integrate_option.h verbatim_integrate_option
@snippet integrate_option.h verbatim_integrate_option_cont
*/
} // namespace rheolef

#include "rheolef/smart_pointer.h"
#include "rheolef/reference_element.h"
#include "rheolef/point.h"

namespace rheolef { 

// [verbatim_integrate_option]
//! @brief see the @ref integrate_option_3 page for the full documentation
class integrate_option {
public:
// typedefs:

  typedef size_t size_type;

  typedef enum {
	gauss		= 0,
	gauss_lobatto	= 1,
	gauss_radau	= 2,
	middle_edge	= 3,
	superconvergent	= 4,
	equispaced	= 5,
	max_family	= 6
  } family_type; // update also family_name[] in quatrature.cc

  static const size_type   unset_order    = std::numeric_limits<size_type>::max();
  static const size_type   default_order  = unset_order;
  static const family_type default_family = gauss;

// allocators:

  integrate_option(
        family_type ft = default_family,
        size_type   k  = default_order);

  integrate_option (const std::string& name);
  integrate_option (const integrate_option& iopt);
  integrate_option& operator= (const integrate_option& iopt);

// accessors & modifiers:

  std::string    name() const;
  size_t         get_order() const;
  family_type    get_family() const;
  std::string    get_family_name() const;
  void reset (const std::string& name);
  void set_order (size_t r);
  void set_family (family_type type);
  void set_family (std::string name);

// data:

  bool invert, ignore_sys_coord, lump;

// [verbatim_integrate_option]
// internals:

  template<class T>
  static T epsilon() { return 1e3*std::numeric_limits<T>::epsilon(); }

protected:
  family_type   _family;
  size_t        _order;
public:
  mutable bool  _is_on_interface, _is_inside_on_local_sides; // internals
// [verbatim_integrate_option_cont]
};
// [verbatim_integrate_option_cont]

// backward compatibility:
using quadrature_option      = integrate_option;
using quadrature_option_type = integrate_option;
using integrate_option_type  = integrate_option;

// ------------------------------------------------------------
// inlined
// ------------------------------------------------------------
inline 
integrate_option::integrate_option (family_type ft, size_type k)
 : invert (false),
   ignore_sys_coord (false),
   lump   (false),
   _family(ft),
   _order(k),
   _is_on_interface(false),
   _is_inside_on_local_sides(false)
{
}
inline
integrate_option::integrate_option (const std::string& name)
 : invert (false),
   ignore_sys_coord (false),
   lump   (false),
   _family(default_family),
   _order(default_order),
   _is_on_interface(false),
   _is_inside_on_local_sides(false)
{
  reset (name);
}
inline
integrate_option::integrate_option (const integrate_option& iopt)
 : invert (iopt.invert),
   ignore_sys_coord (iopt.ignore_sys_coord),
   lump   (iopt.lump),
   _family(iopt._family),
   _order(iopt._order),
   _is_on_interface(iopt._is_on_interface),
   _is_inside_on_local_sides(iopt._is_inside_on_local_sides)
{
}
inline
integrate_option&
integrate_option::operator= (const integrate_option& iopt)
{
  invert           = iopt.invert;
  ignore_sys_coord = iopt.ignore_sys_coord;
  lump             = iopt.lump;
  _family          = iopt._family;
  _order           = iopt._order;
  _is_on_interface          = iopt._is_on_interface;
  _is_inside_on_local_sides = iopt._is_inside_on_local_sides;
  return *this;
}
inline
integrate_option::size_type
integrate_option::get_order () const
{
    return _order;
}
inline
integrate_option::family_type
integrate_option::get_family () const
{
    return _family;
}
inline
void
integrate_option::set_order (size_t r)
{
    _order = r;
}
inline
void
integrate_option::set_family (family_type ft)
{
    _family = ft;
}

}// namespace rheolef
#endif // _RHEO_INTEGRATE_OPTION_H
