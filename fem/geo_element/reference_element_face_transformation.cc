///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "reference_element_face_transformation.h"

namespace rheolef {

// Let S and K such that S is the i-th side of K
// then map a point hat_x defined in the reference element side hat_S
// into tilde_x defined in the reference element tilde_K
// note: used to build a quadrature formulae on a side of K

template<class T>
point_basic<T>
reference_element_face_transformation (
  reference_element             tilde_K,
  const side_information_type&  sid,
  const point_basic<T>&		sid_hat_x)
{
  // d=1: tilde_x = tilde_a0
  // d=2: tilde_x = tilde_a0 + sid_hat_x[0]*(tilde_a1 - tilde_a0)
  // d=3: tilde_x = tilde_a0 + sid_hat_x[0]*(tilde_a1 - tilde_a0) + sid_hat_x[1]*(tilde_a2 - tilde_a0)
  // where tilde_a(i) are the vertices of the transformed side tilde_S in tilde_K
  typedef reference_element::size_type size_type;
  size_type sid_dim = tilde_K.dimension()-1;
  switch (sid_dim) {
    case 0: {
      check_macro (sid.shift == 0, "unexpected shift="<<sid.shift<<" on a0d side");
      check_macro (sid.orient > 0, "unexpected orient="<<sid.orient<<" on a 0d side");
      size_type i0 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, 0);
      const point& tilde_a0 = tilde_K.vertex (i0);
      return tilde_a0;
    }
    case 1: {
      check_macro (sid.shift == 0, "unexpected shift="<<sid.shift<<" on an edge");
      size_type i0 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, 0),
                i1 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, 1);
      if (sid.orient < 0) std::swap (i0, i1);
      const point& tilde_a0 = tilde_K.vertex (i0);
      const point& tilde_a1 = tilde_K.vertex (i1);
      return tilde_a0 + sid_hat_x[0]*(tilde_a1 - tilde_a0);
    }
    case 2: {
      reference_element hat_S = tilde_K.side(sid.loc_isid);
      size_type nv = hat_S.size();
      size_type i0 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, sid.shift%nv),
                i1 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, (sid.shift+1)%nv),
                i2 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, (sid.shift+nv-1)%nv);
      if (sid.orient < 0) std::swap (i1, i2);
      const point& tilde_a0 = tilde_K.vertex (i0);
      const point& tilde_a1 = tilde_K.vertex (i1);
      const point& tilde_a2 = tilde_K.vertex (i2);
      if (hat_S.variant() == reference_element::t) {
        return tilde_a0 + sid_hat_x[0]*(tilde_a1 - tilde_a0)
                        + sid_hat_x[1]*(tilde_a2 - tilde_a0);
      } else {
        return tilde_a0 + 0.5*(1+sid_hat_x[0])*(tilde_a1 - tilde_a0)
                        + 0.5*(1+sid_hat_x[1])*(tilde_a2 - tilde_a0);
      }
    }
    default: {
	fatal_macro ("invalid subgeo_dim="<<sid_dim);
	return point_basic<T>();
    }
  }
}
template<class T>
point_basic<T>
reference_element_face_inverse_transformation (
  reference_element                      tilde_K,
  const side_information_type&           sid,
  const point_basic<T>&                  tilde_x)
{
  // d=1: sid_hat_x = 0
  // d=2: sid_hat_x = dot2d(tilde_a1 - tilde_a0, tilde_x - tilde_a0)/dot2d(tilde_a1 - tilde_a0,tilde_a1 - tilde_a0)
  // d=3: ?
  // where tilde_ai are the vertices of the transformed side tilde_S in tilde_K
  typedef reference_element::size_type size_type;
  size_type sid_dim = tilde_K.dimension()-1;
  switch (sid_dim) {
    case 0: {
      return point_basic<T>(0);
    }
    case 1: {
      check_macro (sid.shift == 0, "unexpected shift="<<sid.shift<<" on an edge");
      size_type i0 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, 0),
                i1 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, 1);
      if (sid.orient < 0) std::swap (i0, i1);
      const point& tilde_a0 = tilde_K.vertex (i0);
      const point& tilde_a1 = tilde_K.vertex (i1);
      T deno = sqr(tilde_a1[0] - tilde_a0[0])
             + sqr(tilde_a1[1] - tilde_a0[1]);
      T num  =    (tilde_x [0] - tilde_a0[0])
                 *(tilde_a1[0] - tilde_a0[0])
                + (tilde_x [1] - tilde_a0[1])
                 *(tilde_a1[1] - tilde_a0[1]);
      return point_basic<T>(num/deno);
    }
    case 2: {
      reference_element hat_S = tilde_K.side(sid.loc_isid);
      size_type nv = hat_S.size();
      size_type i0 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, sid.shift%nv),
                i1 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, (sid.shift+1)%nv),
                i2 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, (sid.shift+nv-1)%nv);
      if (sid.orient < 0) std::swap (i1, i2);
      const point& tilde_a0 = tilde_K.vertex (i0);
      const point& tilde_a1 = tilde_K.vertex (i1);
      const point& tilde_a2 = tilde_K.vertex (i2);
      point_basic<T> hat_x;
      hat_x[0] = dot(tilde_x - tilde_a0, tilde_a1 - tilde_a0)/norm2(tilde_a1 - tilde_a0);
      hat_x[1] = dot(tilde_x - tilde_a0, tilde_a2 - tilde_a0)/norm2(tilde_a2 - tilde_a0);
      return hat_x;
    }
    default: {
	fatal_macro ("invalid subgeo_dim="<<sid_dim);
	return point_basic<T>();
    }
  }
  return point_basic<T>();
}
// NOTE: dirty implementation: goes to Float and go-back to size_t
// TODO: how to directly do it with size_t ?
point_basic<size_t>
reference_element_face_transformation (
  reference_element             tilde_K,
  const side_information_type&  sid,
  size_t                        k,
  const point_basic<size_t>&	sid_ilat)
{
  typedef reference_element::size_type size_type;
  size_type sid_dim = tilde_K.dimension()-1;
  Float km = (k == 0) ? 1 : k; // manage also k=0 case
  switch (sid_dim) {
    case 0: {
      check_macro (sid.shift == 0, "unexpected shift="<<sid.shift<<" on a0d side");
      check_macro (sid.orient > 0, "unexpected orient="<<sid.orient<<" on a 0d side");
      size_type i0 = tilde_K.subgeo_local_vertex (sid_dim, sid.loc_isid, 0);
      return point_basic<size_type>(i0);
    }
    case 1: {
      point_basic<Float> sid_hat_x (sid_ilat[0]/km);
      point_basic<Float> hat_x = reference_element_face_transformation (tilde_K, sid, sid_hat_x);
      point_basic<size_type> ilat;
      if (tilde_K.variant() == reference_element::t) {
         ilat = point_basic<size_type>(size_type(k*hat_x[0]+0.5), size_type(k*hat_x[1]+0.5));
      } else { // tilde_K=q
         ilat = point_basic<size_type>(size_type(k*(1+hat_x[0])/2+0.5), size_type(k*(1+hat_x[1])/2+0.5));
      }
      return ilat;
    }
    case 2: {
      reference_element hat_S = tilde_K.side(sid.loc_isid);
      point_basic<Float> sid_hat_x;
      if (hat_S.variant() == reference_element::t) {
        sid_hat_x = point_basic<Float>(sid_ilat[0]/km, sid_ilat[1]/km);
      } else {
        sid_hat_x = point_basic<Float>(2*sid_ilat[0]/km-1, 2*sid_ilat[1]/km-1);
      }
      point_basic<Float> hat_x = reference_element_face_transformation (tilde_K, sid, sid_hat_x);
      point_basic<size_type> ilat;
      if (tilde_K.variant() == reference_element::T) {
         ilat = point_basic<size_type>(size_type(k*hat_x[0]+0.5), size_type(k*hat_x[1]+0.5), size_type(k*hat_x[2]+0.5));
      } else if (tilde_K.variant() == reference_element::H) {
         ilat = point_basic<size_type>(size_type(k*(1+hat_x[0])/2+0.5), size_type(k*(1+hat_x[1])/2+0.5), size_type(k*(1+hat_x[2])/2+0.5));
      } else { // tilde_K=P
         ilat = point_basic<size_type>(size_type(k*hat_x[0]+0.5), size_type(k*hat_x[1]+0.5), size_type(k*(1+hat_x[2])/2+0.5));
      }
      return ilat;
    }
    default: {
	fatal_macro ("invalid subgeo_dim="<<sid_dim);
	return point_basic<size_type>();
    }
  }
  return point_basic<size_type>();
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------

#define _RHEOLEF_instanciation(T) 				\
template							\
point_basic<T>							\
reference_element_face_transformation (				\
  reference_element             tilde_K,			\
  const side_information_type&  sid,				\
  const point_basic<T>&		hat_x);				\
template							\
point_basic<T>							\
reference_element_face_inverse_transformation (			\
  reference_element             tilde_K,			\
  const side_information_type&  sid,				\
  const point_basic<T>&         tilde_x);

_RHEOLEF_instanciation(Float)

} // namespace rheolef
