///
/// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// author: Pierre.Saramito@imag.fr
// date: 5 february 2019

namespace rheolef {
/**
@commandfile quadrature show a quadrature formula
@addindex quadrature

Synopsis
========
  
    quadrature element quadname
  
Examples
========

      quadrature e 'gauss(3)'
      quadrature T 'gauss(3)'

Description
===========
Shows a quadrature formula: lists the nodes on the specified reference elements.

TODO:
  gnuplot support to show nodes.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/quadrature.h"
using namespace rheolef;
using namespace std;

int main(int argc, char**argv) {
    if (argc < 3) {
	cerr << "usage: quadrature_show element 'name(order)'" << endl
	     << "ex:" << endl
             << "       quadrature_tst t 'gauss(2)'" << endl;
        exit (0);
    }
    char   c  = argv[1][0];
    string name = argv[2];
    reference_element hat_K;
    hat_K.set_name (c);
    quadrature<Float> quad;
    quad.reset (name);

    size_t d = hat_K.dimension();
    cout << setprecision(numeric_limits<Float>::digits10)
         << quad.size(hat_K) << endl;
    for (quadrature<Float>::const_iterator first = quad.begin(hat_K), last = quad.end(hat_K);
		first != last; first++) {
      const point& xq = (*first).x;
      const Float& wq = (*first).w;
      cout << wq << "\t";
      xq.put (cout, d);
      cout << endl;
    }
}
