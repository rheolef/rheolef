///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/tiny_element.h"
#include "rheolef/geo_element.h"
using namespace rheolef;
using namespace std;

ostream& 
operator << (ostream& os, const tiny_element& K)
{
    if (K.variant() == tiny_element::max_variant) {
        error_macro ("unexpected type");
        return os;
    }
    if (K.dimension() >= 2) {
	os << K.name() << "\t";
    }
    for (tiny_element::size_type i = 0; i < K.size(); i++) {
        os << K[i];
        if (i != K.size()-1) {
	    os << " ";
	}
    }
    return os;
}
tiny_element::tiny_element (const geo_element& x)
{
    set_variant (x.variant());
    set_index   (x.index());
    for (size_type i = 0; i < x.size(); i++)
        t_[i] = x[i];
}

