///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// extended arithmetic issue:
//
// 0.5*point(1,0) -> (0,0) : bug between double(0.5) and point_basic<Float>
// it was because there was two specialized:
//	point operator* (int k,   point x)
//	point operator* (Float k, point x)
// and scalar "double" prefer to convert implicitely into "int" than
// into "Float", that represents the extended arithmetic.
// The same situation occurs with Float=dd_real and Float=float128.
//
// Bug was fixed by removing the "int" specialized version:
// then "double" converts implicitely into "Float".
//
// Removing the point operator* (int k, point x) version cause 
//	point v = 2*u;
// to be illegal: only "2.0*u" is allowed. 
// This issue is solved by using Sfinae technique, grouping int and Float cases.
//
#include "rheolef/point.h"
#include "rheolef/tensor.h"
#include "rheolef/tensor3.h"
#include "rheolef/tensor4.h"
using namespace std;
using namespace rheolef;
int main (int argc, char**argv) {
  Float tol = std::numeric_limits<Float>::epsilon();
  Float err = 0;
  // point case
  {
    point x1 = 0.5*point(2,2);
    point x2 = point(1,1);
    err += norm(x1-x2);
    cout << "x1="<<x1<<endl;
    x1 = point(2,2)*0.5;
    err += norm(x1-x2);
    cout << "x1="<<x1<<endl;
    x1 = point(2,2)/2;
    err += norm(x1-x2);
    cout << "x1="<<x1<<endl;
    x1 = 2*point(0.5,0.5);
    err += norm(x1-x2);
    cout << "x1="<<x1<<endl;
    x1 = point(0.5,0.5)*2;
    err += norm(x1-x2);
    cout << "x1="<<x1<<endl;
  }
  // tensor case
  {
    tensor t1 = 0.5*tensor(2);
    tensor t2 = tensor(1);
    err += norm(t1-t2);
    cout << "t1="<<t1<<endl;
    t1 = tensor(2)*0.5;
    err += norm(t1-t2);
    cout << "t1="<<t1<<endl;
    t1 = tensor(2)/2;
    err += norm(t1-t2);
    cout << "t1="<<t1<<endl;
    t1 = 2*tensor(0.5);
    err += norm(t1-t2);
    cout << "t1="<<t1<<endl;
    t1 = tensor(0.5)*2;
    err += norm(t1-t2);
    cout << "t1="<<t1<<endl;
  }
  // tensor3 case
  {
    tensor3 a1 = 0.5*tensor3(2);
    tensor3 a2 = tensor3(1);
    err += norm(a1-a2);
    cout << "a1="<<a1<<endl;
    a1 = tensor3(2)*0.5;
    err += norm(a1-a2);
    cout << "a1="<<a1<<endl;
    a1 = tensor3(2)/2;
    err += norm(a1-a2);
    cout << "a1="<<a1<<endl;
    a1 = 2*tensor3(0.5);
    err += norm(a1-a2);
    cout << "a1="<<a1<<endl;
    a1 = tensor3(0.5)*2;
    err += norm(a1-a2);
    cout << "a1="<<a1<<endl;
  }
  // tensor4 case
  {
    tensor4 a1 = 0.5*tensor4(2);
    tensor4 a2 = tensor4(1);
    err += norm(a1-a2);
    //cout << "a1="<<a1<<endl;
    a1 = tensor4(2)*0.5;
    err += norm(a1-a2);
    //cout << "a1="<<a1<<endl;
    a1 = tensor4(2)/2;
    err += norm(a1-a2);
    //cout << "a1="<<a1<<endl;
    a1 = 2*tensor4(0.5);
    err += norm(a1-a2);
    //cout << "a1="<<a1<<endl;
    a1 = tensor4(0.5)*2;
    err += norm(a1-a2);
    //cout << "a1="<<a1<<endl;
  }
  return err < tol ? 0 : 1;
}
