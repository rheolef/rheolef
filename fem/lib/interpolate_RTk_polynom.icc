#ifndef _RHEOLEF_INTERPOLATE_RTK_POLYNOM_ICC
#define _RHEOLEF_INTERPOLATE_RTK_POLYNOM_ICC
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check the RT1 basis interpolatation on a triangle or a quadrangle
//
// author: Pierre.Saramito@imag.fr
//
// date: 29 april 2019
//
#include "rheolef/basis.h"
// -----------------------------------------------------------------------------
// 1) polynomial function
// -----------------------------------------------------------------------------
struct psi {
  point operator() (const point& x) const {
    switch (hat_K_name) {
      case 't': {
        switch (i) {
	  // P0^2 + (x,y)*P0
          case 0:  return point(1,0);
          case 1:  return point(0,1);
          case 2:  return point(x[0],x[1]);
	  // P1^2 + (x,y)*P1
          case 3:  return point(x[0],-x[1]);
          case 4:  return point(0,x[0]);
          case 5:  return point(x[1],0);
          case 6:  return point(sqr(x[0]),x[0]*x[1]);
          default: return point(x[0]*x[1],sqr(x[1]));
        }
      }
      case 'q':
      default: {
        switch (i) {
	  // P_{1,0}*P_{0,1}
          case 0:  return point(1,0);
          case 1:  return point(0,1);
          case 2:  return point(x[0],0);
          case 3:  return point(0,x[1]);
	  // P_{2,1}*P_{1,2}
          case 4:  return point(0,x[0]);
          case 5:  return point(x[1],0);
          case 6:  return point(x[0]*x[1],0);
          case 7:  return point(0,x[0]*x[1]);
          case 8:  return point(sqr(x[0]),0);
          case 9:  return point(0,sqr(x[1]));
          case 10: return point(sqr(x[0])*x[1],0);
          default: return point(0,x[0]*sqr(x[1]));
        }
      }
    }
  }
  Float div (const point& x) const {
    switch (hat_K_name) {
      case 't': {
        switch (i) {
	  // P0^2 + (x,y)*P0
          case 0:  return 0;
          case 1:  return 0;
          case 2:  return 2;
	  // P1^2 + (x,y)*P1
          case 3:  return 0;
          case 4:  return 0;
          case 5:  return 0;
          case 6:  return 3*x[0];
          default: return 3*x[1];
        }
      }
      case 'q':
      default: {
        switch (i) {
	  // P_{2,1}*P_{1,2}
          case 0:  return 0;
          case 1:  return 0;
          case 2:  return 1;
          case 3:  return 1;
	  // P_{1,0}*P_{0,1}
          case 4:  return 0;
          case 5:  return 0;
          case 6:  return x[1];
          case 7:  return x[0];
          case 8:  return 2*x[0];
          case 9:  return 2*x[1];
          case 10: return 2*x[0]*x[1];
          default: return 2*x[0]*x[1];
        }
      }
    }
  }
  psi (char hat_K_name1, size_t i1) : hat_K_name(hat_K_name1), i(i1) {}
  static size_t n_index (char hat_K_name, size_t k) {
    return (hat_K_name == 't') ?
                 ((k == 0) ? 3 : 8) :
                 ((k == 0) ? 4 : 12);
  }
  const char hat_K_name; const size_t i;
};
struct div_psi {
  Float operator() (const point& x) const { return _psi.div (x); }
  div_psi (char hat_K_name, size_t i) : _psi(hat_K_name,i) {}
  psi _psi;
};
// -----------------------------------------------------------------------------
// 2) non-polynomial function
// -----------------------------------------------------------------------------
struct u_exact {
  point operator() (const point& x) const {
    switch (d) {
      case 2:  return point(cos(w*(x[0]+2*x[1])),
                            sin(w*(x[0]-2*x[1])));
      default: return point(cos(w*(x[0]+2*x[1]+x[2])),
                            sin(w*(x[0]-2*x[1]-x[2])),
                            sin(w*(x[0]+2*x[1]-x[2])));
    }
  }
  Float div (const point& x) const {
    switch (d) {
      case 2:  return -w*(    sin(w*( x[0]+2*x[1]))
                          + 2*cos(w*(-x[0]+2*x[1])));
      default: return -w*(    sin(w*( x[0]+2*x[1]+x[2]))
                          + 2*cos(w*(-x[0]+2*x[1]+x[2]))
                          +   cos(w*(-x[0]-2*x[1]+x[2])));
    }
  }
  u_exact(size_t d1, Float w1 = acos(Float(-1))) : d(d1), w(w1) {}
  size_t d; Float w;
};
struct div_u {
  Float operator() (const point& x) const { return _u.div(x); }
  div_u(size_t d, Float w = acos(Float(-1))) : _u(d,w) {}
  u_exact _u;
};

#endif // _RHEOLEF_INTERPOLATE_RTK_POLYNOM_ICC
