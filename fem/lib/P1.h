// file automatically generated by "../../../rheolef/fem/lib/basis_symbolic_cxx.cc"
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#ifndef _RHEOLEF_P1_H
#define _RHEOLEF_P1_H
#include "rheolef/basis.h"
#include "rheolef/tensor.h"
#include "Pk_get_local_idof_on_side.icc"
#include "basis_fem_Pk_lagrange.h"
namespace rheolef {

template<class T>
class basis_P1: public basis_rep<T> {
public:
  typedef basis_rep<T>          base;
  typedef typename base::size_type size_type;
  basis_P1 (const basis_option& sopt);
  ~basis_P1();
  bool have_index_parameter() const { return true; }
  std::string family_name() const { return "P"; }
  void local_idof_on_side (
    reference_element            hat_K,
    const side_information_type& sid,
    Eigen::Matrix<size_type,Eigen::Dynamic,1>& loc_idof) const
  {
    details::Pk_get_local_idof_on_side (hat_K, sid, degree(), loc_idof);
  }
  size_type degree() const;
  bool is_nodal() const { return true; }
  void evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& values) const;
  void grad_evaluate (
    reference_element                               hat_K,
    const point_basic<T>&                           hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& values) const;
  void _compute_dofs (
    reference_element     hat_K,
    const Eigen::Matrix<T,Eigen::Dynamic,1>&   f_xnod,
          Eigen::Matrix<T,Eigen::Dynamic,1>&   dof) const;
  void _initialize_cstor_sizes() const;
  void _initialize_data (reference_element hat_K) const;
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node(reference_element hat_K) const {
    base::_initialize_data_guard(hat_K);
    return _hat_node [hat_K.variant()]; }
  mutable std::array<
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>,
      reference_element::max_variant>        _hat_node;
};
} // namespace rheolef
#endif // _RHEOLEF_P1_H

