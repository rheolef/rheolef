#ifndef _RHEO_BASIS_RAW_H
#define _RHEO_BASIS_RAW_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/reference_element.h"
#include "rheolef/point.h"
#include "rheolef/tensor.h"
#include "rheolef/smart_pointer.h"
#include "rheolef/space_constant.h"
#include "rheolef/compiler_eigen.h"
#include "rheolef/rheostream.h"

namespace rheolef { 
// ---------------------------------------------------
// rep
// ---------------------------------------------------
template<class T>
class basis_raw_rep {
public:
  typedef reference_element::size_type size_type;
  typedef T                            value_type;
  typedef space_constant::valued_type  valued_type;

  basis_raw_rep (std::string name = "");
  void reset (std::string& name);
  virtual ~basis_raw_rep() {}

  // accessors:
  virtual std::string family_name() const = 0;
  virtual size_type   degree()      const { return _degree; }
  std::string         name()        const { return family_name() + std::to_string(degree()); }
  virtual size_type ndof (reference_element hat_K) const = 0;
  virtual bool is_hierarchical() const { return false; }
  virtual valued_type valued_tag() const { return space_constant::scalar; }
  const std::string&  valued()     const { return space_constant::valued_name (valued_tag()); }

  // evaluate:
  virtual void evaluate (
    reference_element                  hat_K,
    const point_basic<T>&              hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const = 0;

  // evaluate the gradient:
  virtual void grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const = 0;

  void put (std::ostream& os, reference_element hat_K) const;

  static basis_raw_rep* make_ptr (std::string name);

// internals:

          void _clear() const;

protected:
          void _initialize_guard (reference_element hat_K) const;
  virtual void _initialize       (reference_element hat_K) const = 0;

// data:
  size_type                                        _degree;
  mutable std::array<bool,
             reference_element::max_variant>       _have_initialize;
};
// ----------------------------------------------------------------------------
// inlined
// ----------------------------------------------------------------------------
template <class T>
inline
void
basis_raw_rep<T>::_initialize_guard (reference_element hat_K) const
{
  if (_have_initialize [hat_K.variant()]) return;
  _have_initialize [hat_K.variant()] = true;
  _initialize (hat_K);
}
template<class T>
inline
void
basis_raw_rep<T>::_clear() const
{
  _have_initialize.fill (false);
}
// ---------------------------------------------------
// interface class
// ---------------------------------------------------
template<class T>
class basis_raw_basic : public smart_pointer<basis_raw_rep<T> > {
public:

// typedefs:

  typedef basis_raw_rep<T>          rep;
  typedef smart_pointer<rep>        base;
  typedef typename rep::size_type   size_type;
  typedef typename rep::value_type  value_type;
  typedef typename rep::valued_type valued_type;

// allocators:

  basis_raw_basic (std::string name = "");
  void reset      (std::string& name);

// accessors:

  std::string family_name() const;
  size_type   degree() const;
  std::string name() const;
  size_type   ndof (reference_element hat_K) const;
  bool        is_hierarchical() const;

  valued_type        valued_tag() const;
  const std::string& valued()     const;

  void evaluate (
    reference_element                  hat_K,
    const point_basic<T>&              hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const;

// evaluate the gradient:

  void grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const;	// scalar-valued

// output:

  void put (std::ostream& os, reference_element hat_K) const;

protected:
// internals:

  void _clear() const;
};
typedef basis_raw_basic<Float> basis_raw;
//>basis:
// -----------------------------------------------------------
// inlined
// -----------------------------------------------------------
template<class T>
inline
basis_raw_basic<T>::basis_raw_basic(std::string name)
 : base()
{
  reset (name);
}
template<class T>
inline
void
basis_raw_basic<T>::_clear() const
{
  return base::data()._clear();
}
template<class T>
inline
std::string
basis_raw_basic<T>::family_name() const
{
  return base::data().family_name();
}
template<class T>
inline
typename basis_raw_basic<T>::size_type 
basis_raw_basic<T>::degree() const
{
  return base::data().degree();
}
template<class T>
inline
std::string
basis_raw_basic<T>::name() const
{
  return base::data().name();
}
template<class T>
inline
typename basis_raw_basic<T>::size_type
basis_raw_basic<T>::ndof (reference_element hat_K) const
{
  return base::data().ndof (hat_K);
}
template<class T>
inline
bool
basis_raw_basic<T>::is_hierarchical() const
{
  return base::data().is_hierarchical();
}
template<class T>
inline
typename basis_raw_basic<T>::valued_type
basis_raw_basic<T>::valued_tag() const
{
  return base::data().valued_tag();
}
template<class T>
inline
const std::string&
basis_raw_basic<T>::valued() const
{
  return base::data().valued();
}
template<class T>
inline
void
basis_raw_basic<T>::evaluate (
  reference_element                  hat_K,
  const point_basic<T>&              hat_x,
  Eigen::Matrix<T,Eigen::Dynamic,1>& value) const
{
  return base::data().evaluate  (hat_K, hat_x, value);
}
template<class T>
inline
void
basis_raw_basic<T>::grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const
{
  base::data().grad_evaluate (hat_K, hat_x, value);
}
template<class T>
inline
void
basis_raw_basic<T>::put (std::ostream& os, reference_element hat_K) const
{
  base::data().put (os, hat_K);
}

}// namespace rheolef
#endif // _RHEO_BASIS_RAW_H
