#ifndef _RHEO_BASIS_OPTION_H
#define _RHEO_BASIS_OPTION_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// author: Pierre.Saramito@imag.fr
// date: 2 september 2017

namespace rheolef {
/**
@classfile basis_option finite element method options
@addindex  finite element

Description
===========
The `basis_option` class is used to manage options for
the @ref basis constructor of the finite element method.
There are two main options: Lagrange nodes and raw polynomial basis.

String representation
=====================
The `is_option(string)` and `set(string)` members
leads to easy setting of combined options at run time.
By this way, options can be specified, together with basis basename,
on the command line or from a file.

The `stamp()` member returns an unique string.
This string is used for specifying basis options,
e.g. on command line or in files.
This string is empty when all options are set to default values.
Otherwise, it returns a comma separated list of options,
enclosed by braces, specifying only non-default options.
For instance, combining Warburton node set and Dubiner raw
polynomials leads to `"[warburton]"}.
Also, combining Warburton node set and Bernstein raw
polynomials leads to `"[warburton,bernstein]"`.

Note that the continuous or discontinuous feature
is not specified by the `stamp()` string:
it will be specified into the basis basename, by
appending a `"d"` letter, as in `"P6d[warburton]"`.

Notes
=====
There are two distinct kind of polynomial basis:
the raw basis and the finite element one.
(see @ref basis_1 and @ref basis_2).
When using the `Pk` Lagrange finite element basis,
these options are used to transform from one raw (initial)
polynomial basis to the Lagrange one, based on a node set.
When using an alternative finite element basis, e.g.
the spectral `Sk` or the Bernstein `Bk`,
these options do not have any effect.

Implementation
==============
@showfromfile
@snippet basis_option.h verbatim_basis_option
@snippet basis_option.h verbatim_basis_option_cont
*/
} // namespace rheolef

#include "rheolef/smart_pointer.h"
#include "rheolef/reference_element.h"
#include "rheolef/space_constant.h"

namespace rheolef { 

// [verbatim_basis_option]
class basis_option {
public:
// typedefs:

  typedef size_t                          size_type;
  typedef space_constant::valued_type     valued_type;
  typedef space_constant::coordinate_type coordinate_type;

  typedef enum {
    equispaced = 0,
    warburton  = 1,
    fekete     = 2,
    max_node   = 3
  } node_type; // update also node_name[]

  typedef enum {
    monomial	       = 0,
    bernstein	       = 1,
    dubiner 	       = 2,
    max_raw_polynomial = 3
  } raw_polynomial_type; // update also raw_polynomial_name[]

  static const node_type           default_node           = basis_option::equispaced;
  static const raw_polynomial_type default_raw_polynomial = basis_option::dubiner;

// allocators:

  basis_option(
    node_type           nt = default_node,
    raw_polynomial_type pt = default_raw_polynomial);

  basis_option (const basis_option& sopt);
  basis_option& operator= (const basis_option& sopt);

// accessors & modifiers:

  node_type           get_node() const;
  raw_polynomial_type get_raw_polynomial() const;
  std::string         get_node_name() const;
  std::string         get_raw_polynomial_name() const;
  bool                is_continuous() const;
  bool	  	      is_discontinuous() const;
  bool                is_trace_n() const;
  valued_type         valued_tag() const;
  const std::string&  valued() const;
  size_type           dimension() const { return _dimension; }
  coordinate_type     coordinate_system() const { return _sys_coord; }

  void set_node           (node_type type);
  void set_raw_polynomial (raw_polynomial_type type);
  void set                (std::string option_name);
  void set_node           (std::string node_name);
  void set_raw_polynomial (std::string raw_polynomial_name);
  void set_continuous     (bool c = true);
  void set_discontinuous  (bool d = true);
  void set_trace_n        (bool r = true);
  void set_valued_tag     (valued_type v) { _valued_tag = v; }
  void set_dimension      (size_type d) { _dimension = d; }
  void set_coordinate_system  (coordinate_type sc) { _sys_coord = sc; }

  bool is_node_name           (std::string name) const;
  bool is_raw_polynomial_name (std::string name) const;
  bool is_option_name         (std::string name) const;

  std::string stamp() const;
// [verbatim_basis_option]

// data:
protected:
  node_type            _node;
  raw_polynomial_type  _poly;
  bool                 _is_continuous;
  bool                 _is_trace_n;
  valued_type          _valued_tag;
  size_type            _dimension;
  coordinate_type      _sys_coord;
// [verbatim_basis_option_cont]
};
// [verbatim_basis_option_cont]
// ------------------------------------------------------------
// ------------------------------------------------------------
// inlined
// ------------------------------------------------------------
inline 
basis_option::basis_option(
        node_type           nt,
        raw_polynomial_type pt)
 : _node(nt),
   _poly(pt),
   _is_continuous(true),
   _is_trace_n(false),
   _valued_tag(space_constant::scalar),
   _dimension(std::numeric_limits<size_type>::max()),
   _sys_coord(space_constant::cartesian)
{
}
inline
basis_option::basis_option (const basis_option& sopt)
 : _node(sopt._node),
   _poly(sopt._poly),
   _is_continuous(sopt._is_continuous),
   _is_trace_n(sopt._is_trace_n),
   _valued_tag(sopt._valued_tag),
   _dimension(sopt._dimension),
   _sys_coord(sopt._sys_coord)
{
}
inline
basis_option&
basis_option::operator= (const basis_option& sopt)
{
  _node          = sopt._node;
  _poly          = sopt._poly;
  _is_continuous = sopt._is_continuous;
  _is_trace_n    = sopt._is_trace_n;
  _valued_tag    = sopt._valued_tag;
  _dimension     = sopt._dimension;
  _sys_coord     = sopt._sys_coord;
  return *this;
}
inline
basis_option::node_type
basis_option::get_node () const
{
    return _node;
}
inline
basis_option::raw_polynomial_type
basis_option::get_raw_polynomial () const
{
    return _poly;
}
inline
bool
basis_option::is_continuous() const
{
  return  _is_continuous;
}
inline
bool
basis_option::is_discontinuous() const
{
  return !_is_continuous;
}
inline
bool
basis_option::is_trace_n() const
{
  return  _is_trace_n;
}
inline
basis_option::valued_type
basis_option::valued_tag() const
{
  return _valued_tag;
}
inline
const std::string&
basis_option::valued() const
{
  return space_constant::valued_name (valued_tag());
}
inline
void
basis_option::set_node (node_type nt)
{
    _node = nt;
}
inline
void
basis_option::set_raw_polynomial (raw_polynomial_type pt)
{
    _poly = pt;
}
inline
void
basis_option::set_continuous (bool c)
{
  _is_continuous = c;
}
inline
void
basis_option::set_discontinuous (bool d)
{
  _is_continuous = !d;
}
inline
void
basis_option::set_trace_n (bool r)
{
  _is_trace_n = r;
}

}// namespace rheolef
#endif // _RHEO_BASIS_OPTION_H
