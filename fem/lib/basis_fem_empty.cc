///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "basis_fem_empty.h"
#include "basis_fem_Pk_lagrange.h"
#include "piola_fem_lagrange.h"
namespace rheolef {
using namespace std;

template<class T>
const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> basis_fem_empty<T>::_dummy_hat_node;

template<class T>
basis_fem_empty<T>::basis_fem_empty()
 : base(basis_option())
{
  base::_sopt.set_continuous(false);
  _initialize_cstor_sizes();
  base::_name = "empty";
  base::_piola_fem.piola_fem<T>::base::operator= (new_macro(piola_fem_lagrange<T>));
}
template<class T>
basis_fem_empty<T>::~basis_fem_empty()
{
}
template<class T>
void
basis_fem_empty<T>::evaluate (
    reference_element     hat_K,
    const point_basic<T>& hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& values) const
{
  values.resize (0);
}
template<class T>
void
basis_fem_empty<T>::grad_evaluate (
    reference_element        hat_K,
    const point_basic<T>&    hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& values) const
{
  values.resize (0);
}
template<class T>
void
basis_fem_empty<T>::_compute_dofs (
  reference_element     hat_K,
  const Eigen::Matrix<T,Eigen::Dynamic,1>&   f_xnod,
        Eigen::Matrix<T,Eigen::Dynamic,1>&   dof) const
{
  dof.resize(0);
}
template<class T>
void
basis_fem_empty<T>::_initialize_cstor_sizes() const
{
  basis_fem_Pk_lagrange<T>::initialize_local_first (
    size_t(-1),
    false,
    base::_ndof_on_subgeo_internal,
    base::_ndof_on_subgeo,
    base::_nnod_on_subgeo_internal,
    base::_nnod_on_subgeo,
    base::_first_idof_by_dimension_internal,
    base::_first_idof_by_dimension,
    base::_first_inod_by_dimension_internal,
    base::_first_inod_by_dimension);
}
template<class T>
void
basis_fem_empty<T>::_initialize_data(
    reference_element hat_K) const
{
}
// instantiation in library:
template class basis_fem_empty<Float>;
} // namespace rheolef
