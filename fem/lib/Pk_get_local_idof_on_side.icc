#ifndef _RHEOLEF_PK_GET_LOCAL_IDOF_ON_SIDE_ICC
#define _RHEOLEF_PK_GET_LOCAL_IDOF_ON_SIDE_ICC
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// evaluate the Dubiner-Sherwin basis on a point of the reference element
// see SheKar-2005, pages 52 (1d) & 114 (2d) & app. D
//
// author: Pierre.Saramito@imag.fr
//
// date: 5 september 2017
//
#include "rheolef/reference_element.h"
#include "rheolef/reference_element_face_transformation.h"

namespace rheolef { namespace details {

// TODO: .h and .cc instead of copying multiple binary code
static
void
Pk_get_local_idof_on_side (
  reference_element            tilde_K,
  const side_information_type& sid,
  size_t                       k,
  Eigen::Matrix<size_t,Eigen::Dynamic,1>& loc_idof)
{
  typedef reference_element::size_type size_type;
  check_macro (sid.shift == 0, "sid.shift !=0 : not yet, sorry"); // TODO: honor also 3d sid.shift
  switch (tilde_K.variant()) {
    case reference_element::p: {
      loc_idof.resize (0);
      return;
    }
    case reference_element::e: {
      loc_idof.resize (2);
      loc_idof [0] = 0;
      loc_idof [1] = 1;
      return;
    }
    case reference_element::t: {
      size_type loc_ndof = k+1;
      loc_idof.resize (loc_ndof);
      size_type loc_sid_idof = 0;
      for (size_type u = 0; u <= k; ++u, ++loc_sid_idof) {
        point_basic<size_type> side_ilat (u);
        point_basic<size_type> ilat = reference_element_face_transformation (tilde_K, sid, k, side_ilat);
        loc_idof [loc_sid_idof] = reference_element_t::ilat2loc_inod (k, ilat);
      }
      return;
    }
    case reference_element::q: {
      size_type loc_ndof = k+1;
      loc_idof.resize (loc_ndof);
      size_type loc_sid_idof = 0;
      for (size_type u = 0; u <= k; ++u, ++loc_sid_idof) {
        point_basic<size_type> side_ilat (u);
        point_basic<size_type> ilat = reference_element_face_transformation (tilde_K, sid, k, side_ilat);
        loc_idof [loc_sid_idof] = reference_element_q::ilat2loc_inod (k, ilat);
      }
      return;
    }
    case reference_element::T: {
      size_type loc_ndof = (k+1)*(k+2)/2;
      loc_idof.resize (loc_ndof);
      size_type loc_sid_idof = 0;
      for (size_type u = 0; u <= k; ++u) {
      for (size_type v = 0; v <= k-u; ++v, ++loc_sid_idof) {
        point_basic<size_type> side_ilat (u,v);
        point_basic<size_type> ilat = reference_element_face_transformation (tilde_K, sid, k, side_ilat);
        loc_idof [loc_sid_idof] = reference_element_T::ilat2loc_inod (k, ilat);
      }}
      return;
    }
    case reference_element::H: {
      size_type loc_ndof = (k+1)*(k+1);
      loc_idof.resize (loc_ndof);
      size_type loc_sid_idof = 0;
      for (size_type u = 0; u <= k; ++u) {
      for (size_type v = 0; v <= k; ++v, ++loc_sid_idof) {
        point_basic<size_type> side_ilat (u,v);
        point_basic<size_type> ilat = reference_element_face_transformation (tilde_K, sid, k, side_ilat);
        loc_idof [loc_sid_idof] = reference_element_H::ilat2loc_inod (k, ilat);
      }}
      return;
    }
    case reference_element::P: {
      reference_element hat_S = tilde_K.side (sid.loc_isid);
      size_type loc_ndof = (hat_S.variant() == reference_element::t) ? (k+1)*(k+2)/2 : (k+1)*(k+1);
      loc_idof.resize (loc_ndof);
      size_type loc_sid_idof = 0;
      for (size_type u = 0; u <= k; ++u) {
        size_type vf = (hat_S.variant() == reference_element::t) ? k-u : k;
        for (size_type v = 0; v <= vf; ++v, ++loc_sid_idof) {
          point_basic<size_type> side_ilat (u,v);
          point_basic<size_type> ilat = reference_element_face_transformation (tilde_K, sid, k, side_ilat);
          loc_idof [loc_sid_idof] = reference_element_P::ilat2loc_inod (k, ilat);
        }
      }
      return;
    }
    default: error_macro ("get_local_idof_on_side: element "<<tilde_K.name()<<" not yet supported");
  }
}

}} // namespace rheolef::details
#endif // _RHEOLEF_PK_GET_LOCAL_IDOF_ON_SIDE_ICC
