#ifndef _RHEOLEF_MEMORIZED_VALUE_H
#define _RHEOLEF_MEMORIZED_VALUE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// utility:
// use internal data area: avoid repetitive memory allocation
//
// internal data area accessor: return
//	_scalar_val
//	_vector_val
//	_tensor_val
// depending upon Value template parameter
//   => allows generic programmation vs Value
//
// implementation note:
//   template specialized accessor: use class-specialization
//   since specialization is neither available for func and class-member
// 
// ----------------------------------------------------------------------------
#include "rheolef/compiler_eigen.h"

namespace rheolef { namespace details {


// ----------------------------------------------------------------------------
// vector-valued data
// ----------------------------------------------------------------------------
template <class T, class Value> struct memorized_vector {};


#define _RHEOLEF_class_specialization(VALUE,MEMBER)			\
template<class T>							\
struct memorized_vector<T,VALUE> {					\
  template <class Object>						\
  Eigen::Matrix<VALUE,Eigen::Dynamic,1>&				\
  get (const Object& obj, const reference_element& hat_K) const {	\
    return obj.MEMBER [hat_K.variant()];				\
  }									\
};									\

_RHEOLEF_class_specialization(T,_scalar_val)
_RHEOLEF_class_specialization(point_basic<T>,_vector_val)
_RHEOLEF_class_specialization(tensor_basic<T>,_tensor_val)
_RHEOLEF_class_specialization(tensor3_basic<T>,_tensor3_val)
_RHEOLEF_class_specialization(tensor4_basic<T>,_tensor4_val)
#undef _RHEOLEF_class_specialization

// ----------------------------------------------------------------------------
// matrix-valued data
// ----------------------------------------------------------------------------
template <class T, class Value> struct memorized_matrix {};

#define _RHEOLEF_class_specialization(VALUE,MEMBER)			\
template<class T>							\
struct memorized_matrix<T,VALUE> {					\
  template <class Object>						\
  Eigen::Matrix<VALUE,Eigen::Dynamic,Eigen::Dynamic>&			\
  get (const Object& obj, const reference_element& hat_K) const {	\
    return obj.MEMBER [hat_K.variant()];				\
  }									\
};									\

_RHEOLEF_class_specialization(T,_scalar_val)
_RHEOLEF_class_specialization(point_basic<T>,_vector_val)
_RHEOLEF_class_specialization(tensor_basic<T>,_tensor_val)
_RHEOLEF_class_specialization(tensor3_basic<T>,_tensor3_val)
_RHEOLEF_class_specialization(tensor4_basic<T>,_tensor4_val)
#undef _RHEOLEF_class_specialization

// ----------------------------------------------------------
// similar accessors for values on sides
// ----------------------------------------------------------
template <class T, class Value> struct memorized_side_value {};

#define _RHEOLEF_class_specialization(VALUE,MEMBER)			\
template<class T>							\
struct memorized_side_value<T,VALUE> {					\
  template <class Object>						\
  Eigen::Matrix<VALUE,Eigen::Dynamic,Eigen::Dynamic>&			\
  get (									\
    const Object&                   obj,				\
    const reference_element&        hat_K,				\
    const side_information_type&    sid) const {			\
    size_t ori_idx = (sid.orient == 1) ? 0 : 1;			\
    return obj.MEMBER [hat_K.variant()] [sid.loc_isid] [ori_idx] [sid.shift]; \
  }									\
};									\

_RHEOLEF_class_specialization(T,_sid_scalar_val)
_RHEOLEF_class_specialization(point_basic<T>,_sid_vector_val)
_RHEOLEF_class_specialization(tensor_basic<T>,_sid_tensor_val)
_RHEOLEF_class_specialization(tensor3_basic<T>,_sid_tensor3_val)
_RHEOLEF_class_specialization(tensor4_basic<T>,_sid_tensor4_val)
#undef _RHEOLEF_class_specialization

}} // namespace rheolef::details
#endif // _RHEOLEF_MEMORIZED_VALUE_H
