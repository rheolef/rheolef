#ifndef _RHEO_BASIS_H
#define _RHEO_BASIS_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   7 january 2004, update 11 september 2017

namespace rheolef {
/**
@classfile basis finite element method
@addindex  polynomial basis
@addindex  reference element

Description
===========
The class constructor takes a string that characterizes
the finite element method,
e.g. `"P0"`, `"P1"` or `"P2"`.
The letters characterize the finite element family
and the number is the polynomial degree in this family.
The finite element also depends upon the reference element,
e.g. triangle, square, tetrahedron, etc.
See @ref reference_element_6 for more details.
For instance, on the square reference element,
the `"P1"` string designates the common `Q1` four-nodes
finite element.

Available finite elements
=========================
`Pk`
>	The classical Lagrange finite element family,
>	where `k` is any polynomial degree greater or equal to 1.

`Pkd`
>	The discontinuous Galerkin finite element family,
>	where `k` is any polynomial degree greater or equal to 0.
>	For convenience, `P0d` could also be written as `P0`.

`bubble`
>	The product of baycentric coordinates.
>	Only simplicial elements are supported:
>	@ref edge_6, @ref triangle_6 and @ref tetrahedron_6.

`RTk` \n
`RTkd`
>	The vector-valued Raviart-Thomas family,
>	where `k` is any polynomial degree greater or equal to 0.
>	The `RTkd` piecewise discontinuous version is fully implemented
>	for the @ref triangle_6.
>	Its `RTk` continuous counterpart is still under development.

`Bk`
>	The Bernstein finite element family,
>	where `k` is any polynomial degree greater or equal to 1.
>	This `basis` was initially introduced by Bernstein
>       (Comm. Soc. Math. Kharkov, 2th series, 1912) and more
>       recently used in the context of finite elements.
>	This feature is still under development.

`Sk`
>	The spectral finite element family,
>	as proposed by Sherwin and Karniadakis
>       (Oxford University Press, 2nd ed., 2005).
>	Here, `k` is any polynomial degree greater or equal to 1.
>	This feature is still under development.

Continuous feature
==================
Some finite element families could support either
continuous or discontinuous junction at inter-element boundaries.
For instance, the `Pk` Lagrange finite element family, with k >= 1,
has a continuous interelements junction:
it defines a piecewise polynomial and globally continuous
finite element functional @ref space_2.
Conversely, the `Pkd` Lagrange finite element family, with k >= 0,
has a discontinuous interelements junction:
it defines a piecewise polynomial and globally discontinuous
finite element functional @ref space_2.

Options
=======
The `basis` class supports some options
associated to each finite element method.
These options are appended to the string
bewteen square braces, and are separated
by a coma, e.g. `"P5[feteke,bernstein]"`.
See @ref basis_1 for some examples.

Lagrange nodes option
=====================
`equispaced`
>	Nodes are equispaced.
>	This is the default.
`fekete`
>	Node are non-equispaced: 
>	for high-order polynomial degree, e.g. greater or equal to 3,
>	their placement is fully optimized, 
>	as proposed by Taylor, Wingate and Vincent, 2000, SIAM J. Numer. Anal.
>	With this choice, the interpolation error is dramatically
>	decreased for high order polynomials.
>	This feature is still restricted to the triangle reference element
>	and to polynomial degree lower or equal to 19.
>	Otherwise, it fall back to the following `warburton` node set.
`warburton`
>	Node are non-equispaced: 
>	for high-order polynomial degree, e.g. greater or equal to 3,
>	their placement is optimized thought an heuristic solution,
>	as proposed by Warburton, 2006, J. Eng. Math.
>	With this choice, the interpolation error is dramatically
>	decreased for high order polynomials.
>	This feature applies to any reference element and polynomial degree.

Raw polynomial basis
====================
The raw (or initial) polynomial basis is used for building the 
dual basis, associated to degrees of freedom,
via the Vandermonde matrix and its inverse.
Changing the raw basis do not change the definition of the FEM basis,
but only the way it is constructed.
There are three possible raw basis:

`monomial`
>	The monomial basis is the simplest choice.
>	While it is suitable for low polynomial degree (less than five),
>	for higher polynomial degree, the 
>	Vandermonde matrix becomes ill-conditioned and
>	its inverse leads to errors in double precision.

`dubiner`
>	The Dubiner basis (see Dubiner, 1991 J. Sci. Comput.)
>	leads to much better condition number.
>	This is the default.

`bernstein`
>	The Bernstein basis could also be an alternative raw basis.

Internals: evaluate and interpolate
===================================
The `basis` class defines member functions that evaluates
all the polynomial basis functions of a finite element
and their derivatives at a point or a set of point.

The basis polynomial functions are evaluated
by the `evaluate` member function.
This member function is called during the 
assembly process of the @ref integrate_3 function.

The interpolation nodes used by the @ref interpolate_3 function
are available by the member function `hat_node`.
Conversely, the member function `compute_dofs`
compute the degrees of freedom.

Implementation
==============
@showfromfile
The `basis` class is simply an alias to the `basis_basic` class

@snippet basis.h verbatim_basis
@par

The `basis_basic` class provides an interface
to a data container:

@snippet basis.h verbatim_basis_basic
@snippet basis.h verbatim_basis_basic_cont
*/
} // namespace rheolef

#include "rheolef/reference_element.h"
#include "rheolef/reference_element_face_transformation.h"
#include "rheolef/point.h"
#include "rheolef/tensor.h"
#include "rheolef/persistent_table.h"
#include "rheolef/space_constant.h"
#include "rheolef/basis_raw.h"
#include "rheolef/basis_option.h"
#include "rheolef/piola_fem.h"
#include <unordered_map>

namespace rheolef {

template<class T> class basis_basic; // forward declaration

template<class T>
class basis_rep {
public:

// typedefs:

  typedef reference_element::size_type size_type;
  typedef T                            value_type;
  typedef space_constant::valued_type  valued_type;

// allocators:

  basis_rep (const basis_option& sopt);
  virtual ~basis_rep();

// numbering accessors:

  virtual size_type   degree() const = 0;
  virtual size_type   family_index() const { return degree(); }
  virtual std::string family_name() const = 0;
  std::string name() const { return _name; }

  virtual valued_type valued_tag() const { return space_constant::scalar; }
  virtual bool is_nodal() const = 0;
  virtual bool have_index_parameter()   const { return true; }
  virtual bool have_continuous_feature() const { return true; }
  virtual bool is_hierarchical() const { return false; }
  virtual size_type size() const { return 1; }
  virtual const class basis_basic<T>& operator[] (size_type i_comp) const;
  virtual bool have_compact_support_inside_element() const { return ! option().is_continuous(); }
  const basis_option& option() const { return _sopt; }
  const std::string&  valued()     const { return space_constant::valued_name (valued_tag()); }
  bool is_continuous() const    { return option().is_continuous(); }
  bool is_discontinuous() const { return ! is_continuous(); }

  // accessors to ndof and nnod
  size_type ndof (reference_element hat_K) const {
    return _first_idof_by_dimension [hat_K.variant()][hat_K.dimension()+1];
  }
  size_type nnod (reference_element hat_K) const {
    return _first_inod_by_dimension [hat_K.variant()][hat_K.dimension()+1];
  }
  // accessors to subgeo ndof & nnod by dimension (not available for basis_trace)
  // numbering: dofs & nodes are organized by variant and dimension=0..3 of subgeos, for each hat_K
  // in the discontinous case, this distinction remains in xxx_internal
  size_type ndof_on_subgeo (size_type map_dim, size_type subgeo_variant) const {
    return _ndof_on_subgeo [map_dim][subgeo_variant];
  }
  size_type nnod_on_subgeo (size_type map_dim, size_type subgeo_variant) const {
    return _nnod_on_subgeo [map_dim][subgeo_variant];
  }
  size_type first_idof_by_dimension (reference_element hat_K, size_type dim) const {
    return _first_idof_by_dimension [hat_K.variant()][dim];
  }
  size_type first_inod_by_dimension (reference_element hat_K, size_type dim) const {
    return _first_inod_by_dimension [hat_K.variant()][dim];
  }
  // internal accessors to subgeo ndof & nnod by dimension (not available for basis_trace)
  // differs only for discontinuous elements: internals conserve the subgeo structure
  size_type ndof_internal (reference_element hat_K) const {
    return _first_idof_by_dimension_internal [hat_K.variant()][hat_K.dimension()+1];
  }
  size_type nnod_internal (reference_element hat_K) const {
    return _first_inod_by_dimension_internal [hat_K.variant()][hat_K.dimension()+1];
  }
  // accessors to subgeo ndof & nnod by dimension (not available for basis_trace)
  size_type ndof_on_subgeo_internal (size_type map_dim, size_type subgeo_variant) const {
    return _ndof_on_subgeo_internal [map_dim][subgeo_variant];
  }
  size_type nnod_on_subgeo_internal (size_type map_dim, size_type subgeo_variant) const {
    return _nnod_on_subgeo_internal [map_dim][subgeo_variant];
  }
  size_type first_idof_by_dimension_internal (reference_element hat_K, size_type dim) const {
    return _first_idof_by_dimension_internal [hat_K.variant()][dim];
  }
  size_type first_inod_by_dimension_internal (reference_element hat_K, size_type dim) const {
    return _first_inod_by_dimension_internal [hat_K.variant()][dim];
  }

  // used by DG, HDG:
  virtual size_type local_ndof_on_side (
        reference_element            hat_K,
        const side_information_type& sid) const;
  virtual void local_idof_on_side (
        reference_element            hat_K,
        const side_information_type& sid,
        Eigen::Matrix<size_type,Eigen::Dynamic,1>& loc_idof) const;

  virtual const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node (reference_element hat_K) const;
  virtual const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& vdm     (reference_element hat_K) const;
  virtual const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& inv_vdm (reference_element hat_K) const;

// valued accessors :

  const piola_fem<T>& get_piola_fem() const { return _piola_fem; }

  // evaluation and compute_dofs:
  // the abstract class provide a rich interface, for each valued
  // case because the valued type is run-time dependent.
  // For each concrete basis, only one of the valued case is implemented.

#define _RHEOLEF_evaluate(MEMBER,VALUED,VALUE)			\
  virtual							\
  void								\
  MEMBER (							\
    reference_element                      hat_K,		\
    const point_basic<T>&                  hat_x,		\
    Eigen::Matrix<VALUE,Eigen::Dynamic,1>& value) const		\
  {								\
    error_macro ("basis \""<<name()<<"\": " 			\
     << VALUED << "-valued " << #MEMBER				\
     << "() member not implemented");				\
  }								\

_RHEOLEF_evaluate(evaluate,"scalar",T)
_RHEOLEF_evaluate(evaluate,"vector",point_basic<T>)
_RHEOLEF_evaluate(evaluate,"tensor",tensor_basic<T>)
_RHEOLEF_evaluate(evaluate,"tensor3",tensor3_basic<T>)
_RHEOLEF_evaluate(evaluate,"tensor4",tensor4_basic<T>)
_RHEOLEF_evaluate(grad_evaluate,"scalar",T)
_RHEOLEF_evaluate(grad_evaluate,"vector",point_basic<T>)
_RHEOLEF_evaluate(grad_evaluate,"tensor",tensor_basic<T>)
_RHEOLEF_evaluate(grad_evaluate,"tensor3",tensor3_basic<T>)
_RHEOLEF_evaluate(grad_evaluate,"tensor4",tensor4_basic<T>)
#undef _RHEOLEF_evaluate

#define _RHEOLEF_evaluate_on_side(VALUED,VALUE)			\
  virtual								\
  void								\
  evaluate_on_side (						\
    reference_element                  tilde_K,			\
    const side_information_type&       sid,			\
    const point_basic<T>&              hat_x,			\
    Eigen::Matrix<VALUE,Eigen::Dynamic,1>& value) const		\
  {								\
    error_macro ("basis \""<<name()<<"\": "			\
      << VALUED << "-valued evaluate_on_side() member not implemented"); \
  }

_RHEOLEF_evaluate_on_side("scalar",T)
_RHEOLEF_evaluate_on_side("vector",point_basic<T>)
#undef _RHEOLEF_evaluate_on_side

#define _RHEOLEF_compute_dofs(VALUED,VALUE)			\
  virtual							\
  void								\
  _compute_dofs (						\
    reference_element     		     hat_K, 		\
    const Eigen::Matrix<VALUE,Eigen::Dynamic,1>& f_xnod,	\
          Eigen::Matrix<T,Eigen::Dynamic,1>& dof) const		\
  {								\
    error_macro ("basis \"" << name() << "\": "			\
      << VALUED << "-valued _compute_dofs() member not implemented"); \
  }

_RHEOLEF_compute_dofs("scalar",T)
_RHEOLEF_compute_dofs("vector",point_basic<T>)
_RHEOLEF_compute_dofs("tensor",tensor_basic<T>)
#undef _RHEOLEF_compute_dofs

// internals:

  void put                       (std::ostream& os, reference_element hat_K) const;
  virtual void put_scalar_valued (std::ostream& os, reference_element hat_K) const;
  virtual void put_vector_valued (std::ostream& os, reference_element hat_K) const;
  void put_hat_node              (std::ostream& os, reference_element hat_K) const;
  void put_hat_node_on_side      (std::ostream& os, reference_element hat_K,
    			                 const side_information_type& sid) const;

  // helper for building class:
  static basis_rep* make_ptr (const std::string& name);
  static std::string standard_naming (std::string family_name, size_t degree, const basis_option& sopt);
  static bool have_index_parameter   (std::string family_name);
  static bool have_continuous_feature (std::string family_name);

  virtual void _initialize_cstor_sizes () const = 0;
  void         _initialize_data_guard  (reference_element hat_K) const;
  virtual void _initialize_data        (reference_element hat_K) const = 0;

// internals:

  void _clear() const;

  // internal helper:
  // 1) automatically deduce _first_ixxx_by_dimension from _nxxx_on_subgeo
  //    where xxx = dof or nod
  static void _helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (
          const std::array<
            std::array<
              size_type
             ,reference_element::max_variant>
            ,4>&                                    _nxxx_on_subgeo,
          std::array<
            std::array<
              size_type
              ,5>
            ,reference_element::max_variant>&       _first_ixxx_by_dimension);
  // 2) inplace change nxxx_on_subgeo for discontinuous elements
  static void _helper_make_discontinuous_ndof_on_subgeo (
      bool is_continuous,
      const 
      std::array<
        std::array<
          size_type
         ,reference_element::max_variant>
            ,4>&                                    nxxx_on_subgeo_internal,
      std::array<
        std::array<
          size_type
         ,reference_element::max_variant>
            ,4>&                                    nxxx_on_subgeo);

protected:
// data:
  std::string					    _name;
  basis_option                                      _sopt;
  piola_fem<T>					    _piola_fem;

  mutable std::array<bool,
             reference_element::max_variant>        _have_initialize_data;

  // nodes and dofs are organized by variant and dimension=0..3 of subgeos, for each hat_K:
  mutable std::array<
             std::array<
               size_type
              ,reference_element::max_variant>
             ,4>                                    _ndof_on_subgeo_internal,
                                                    _ndof_on_subgeo,
                                                    _nnod_on_subgeo_internal,
                                                    _nnod_on_subgeo;
  mutable std::array<
            std::array<
              size_type
             ,5>
           ,reference_element::max_variant>         _first_idof_by_dimension_internal,
						    _first_idof_by_dimension,
						    _first_inod_by_dimension_internal,
						    _first_inod_by_dimension;
};
// -----------------------------------------------------------
// inlined
// -----------------------------------------------------------
template<class T>
inline
const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
basis_rep<T>::hat_node (reference_element hat_K) const
{
  error_macro ("basis \""<<name()<<"\": hat_node() member not implemented");
  static const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> _dummy;
  return _dummy;
}
template<class T>
inline
const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
basis_rep<T>::vdm (reference_element hat_K) const
{
  error_macro ("basis \""<<name()<<"\": vdm() member not implemented");
  static const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> _dummy;
  return _dummy;
}
template<class T>
inline
const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
basis_rep<T>::inv_vdm (reference_element hat_K) const
{
  error_macro ("basis \""<<name()<<"\": inv_vdm() member not implemented");
  static const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> _dummy;
  return _dummy;
}
// extract local dof-indexes on a side
template<class T>
inline
typename basis_rep<T>::size_type
basis_rep<T>::local_ndof_on_side (
  reference_element            hat_K,
  const side_information_type& sid) const
{
  error_macro ("basis \""<<name()<<"\": local_ndof_on_side() member not implemented"); return 0;
}
template<class T>
inline
void
basis_rep<T>::local_idof_on_side (
  reference_element            hat_K,
  const side_information_type& sid,
  Eigen::Matrix<size_type,Eigen::Dynamic,1>& loc_idof) const
{
  error_macro ("basis \""<<name()<<"\": local_idof_on_side() member not implemented");
}
// ---------------------------------------------------------
// compute_dof
// ---------------------------------------------------------
// implementation note:
//   b.compute_dof   (f,&dof)
// is impossible with base_rep
// because template functions cannot be virtual
// cannot call a "b.compute_dof(f,&dof)" template-virtual member
// so we have to write at top level basis_rep a general
//   compute_dof   (b,f,&dof)
// and switch on all nodal/modal and valued variants
//
// for vector-valued RTk, it is problematic, because we
// only require f.n on the boundary and not all f components
//
namespace details {

template <class T, class Function>
typename
std::enable_if<
  is_scalar<typename function_traits<Function>::result_type>::value
 ,void
>::type
compute_dof (
  const basis_rep<T>& b,
  reference_element      hat_K,
  const Function&        f,
  Eigen::Matrix<T,Eigen::Dynamic,1>& dof)
{
  typedef reference_element::size_type  size_type;
  check_macro (b.valued_tag() == space_constant::scalar,
    "interpolate: incompatible scalar-valued function and "<<b.valued()<<"-valued basis");
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node = b.hat_node (hat_K);
  // modal basis:
  Eigen::Matrix<T,Eigen::Dynamic,1> f_xnod (hat_node.size());
  for (size_type loc_inod = 0, loc_nnod = hat_node.size(); loc_inod < loc_nnod; ++loc_inod) {
    f_xnod [loc_inod] = f (hat_node [loc_inod]);
  }
  b._compute_dofs (hat_K, f_xnod, dof);
}
template <class T, class Function>
typename
std::enable_if<
  is_point<typename function_traits<Function>::result_type>::value
 ,void
>::type
compute_dof (
  const basis_rep<T>& b,
  reference_element      hat_K,
  const Function&        f,
  Eigen::Matrix<T,Eigen::Dynamic,1>& dof)
{
  typedef reference_element::size_type  size_type;
  check_macro (b.valued_tag() == space_constant::vector,
    "interpolate: incompatible vector-valued function and "<<b.valued()<<"-valued basis");
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node = b.hat_node (hat_K);
  // modal basis:
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> f_xnod (hat_node.size());
  for (size_type loc_inod = 0, loc_nnod = hat_node.size(); loc_inod < loc_nnod; ++loc_inod) {
    f_xnod [loc_inod] = f (hat_node [loc_inod]);
  }
  b._compute_dofs (hat_K, f_xnod, dof);
}

} // namespace details

// [verbatim_basis_basic]
template<class T>
class basis_basic : public smart_pointer_nocopy<basis_rep<T> >, 
                    public persistent_table<basis_basic<T> > {
public:

// typedefs:

  typedef basis_rep<T>              rep;
  typedef smart_pointer_nocopy<rep> base;
  typedef typename rep::size_type   size_type;
  typedef typename rep::value_type  value_type;
  typedef typename rep::valued_type valued_type;

// allocators:

  basis_basic (std::string name = "");
  void reset (std::string& name);
  void reset_family_index (size_type k);

// accessors:

  bool is_initialized() const { return base::operator->() != 0; }
  size_type   degree() const;
  size_type   family_index() const;
  std::string family_name() const;
  std::string name() const;
  size_type   ndof (reference_element hat_K) const;
  size_type   nnod (reference_element hat_K) const;
  bool is_continuous() const;
  bool is_discontinuous() const;
  bool is_nodal() const;
  bool have_continuous_feature() const;
  bool have_compact_support_inside_element() const;
  bool is_hierarchical() const;
  size_type size() const;
  const basis_basic<T>& operator[] (size_type i_comp) const;
  bool have_index_parameter() const;
  const basis_option& option() const;
  valued_type        valued_tag() const;
  const std::string& valued()     const;
  const piola_fem<T>& get_piola_fem() const;
// [verbatim_basis_basic]

  size_type local_ndof_on_side (
        reference_element            hat_K,
        const side_information_type& sid) const;
  void local_idof_on_side (
        reference_element            hat_K,
        const side_information_type& sid,
        Eigen::Matrix<size_type,Eigen::Dynamic,1>& loc_idof) const;

  size_type ndof_on_subgeo (size_type map_dim, size_type subgeo_variant) const;
  size_type nnod_on_subgeo (size_type map_dim, size_type subgeo_variant) const;
  size_type first_inod_by_dimension (reference_element hat_K, size_type dim) const;
  size_type first_idof_by_dimension (reference_element hat_K, size_type dim) const;

  size_type ndof_on_subgeo_internal (size_type map_dim, size_type subgeo_variant) const;
  size_type nnod_on_subgeo_internal (size_type map_dim, size_type subgeo_variant) const;
  size_type first_inod_by_dimension_internal (reference_element hat_K, size_type dim) const;
  size_type first_idof_by_dimension_internal (reference_element hat_K, size_type dim) const;

  // evaluate the basis:
  template<class Value>
  void evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&  value) const;

  // evaluation restricted to a side:
  template<class Value>
  void evaluate_on_side (
    reference_element            tilde_K,
    const side_information_type& sid,
    const point_basic<T>&        hat_x,
    Eigen::Matrix<Value,Eigen::Dynamic,1>& value) const;

  // evaluate the gradient of the basis:
  template<class Value>
  void grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<Value,Eigen::Dynamic,1>& value) const;

  // interpolation nodes:
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node (reference_element hat_K) const;

  // compute_dof:
  template <class Function>
  void compute_dof (
    reference_element           hat_K,
    const Function&             f,
    Eigen::Matrix<T,Eigen::Dynamic,1>&   dofs) const;
  
  template <class Value>
  void compute_dofs (
    reference_element                            hat_K, 
    const Eigen::Matrix<Value,Eigen::Dynamic,1>& f_xnod, 	             // scalar-valued case:
          Eigen::Matrix<T,Eigen::Dynamic,1>&     dof) const;

  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& vdm     (reference_element hat_K) const;
  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& inv_vdm (reference_element hat_K) const;

  // output:
  void put          (std::ostream& os, reference_element hat_K) const;
  void put_hat_node (std::ostream& os, reference_element hat_K) const;
  void put_hat_node_on_side (
    std::ostream&                os,
    reference_element            hat_K,
    const side_information_type& sid) const;

protected:
// internals:

  void _clear() const;
// [verbatim_basis_basic_cont]
};
// [verbatim_basis_basic_cont]

// [verbatim_basis]
typedef basis_basic<Float> basis;
// [verbatim_basis]

// -----------------------------------------------------------
// inlined
// -----------------------------------------------------------
template<class T>
inline
basis_basic<T>::basis_basic(std::string name)
 : base(),
   persistent_table<basis_basic<T>>()
{
  reset (name);
}
template<class T>
inline
void
basis_basic<T>::_clear() const
{
  return base::data()._clear();
}
template<class T>
inline
std::string
basis_basic<T>::family_name() const
{
  return base::data().family_name();
}
template<class T>
inline
typename basis_basic<T>::size_type
basis_basic<T>::family_index() const
{
  return base::data().family_index();
}
template<class T>
inline
std::string
basis_basic<T>::name() const
{
  return base::data().name();
}
template<class T>
inline
typename basis_basic<T>::size_type 
basis_basic<T>::degree() const
{
  return base::data().degree();
}
template<class T>
inline
typename basis_basic<T>::size_type
basis_basic<T>::ndof (reference_element hat_K) const
{
  return base::data().ndof (hat_K);
}
template<class T>
inline
typename basis_basic<T>::size_type
basis_basic<T>::nnod (reference_element hat_K) const
{
  return base::data().nnod (hat_K);
}
template<class T>
inline
const basis_option&
basis_basic<T>::option() const
{
  return base::data().option();
}
template<class T>
inline
bool
basis_basic<T>::is_continuous() const
{
  return base::data().is_continuous();
}
template<class T>
inline
bool
basis_basic<T>::is_discontinuous() const
{
  return base::data().is_discontinuous();
}
template<class T>
inline
bool
basis_basic<T>::is_nodal() const
{
  return base::data().is_nodal();
}
template<class T>
inline
bool
basis_basic<T>::have_continuous_feature() const
{
  return base::data().have_continuous_feature();
}
template<class T>
inline
bool
basis_basic<T>::have_compact_support_inside_element() const
{
  return base::data().have_compact_support_inside_element();
}
template<class T>
inline
typename basis_basic<T>::size_type
basis_basic<T>::size() const
{
  return base::data().size();
}
template<class T>
inline
bool
basis_basic<T>::is_hierarchical() const
{
  return base::data().is_hierarchical();
}
template<class T>
inline
const basis_basic<T>&
basis_rep<T>::operator[] (size_type i_comp) const
{
  error_macro ("basis \""<<name()<<"\": invalid basis indexation [i_comp]");
  static const basis_basic<T> dummy; return dummy;
}
template<class T>
inline
const basis_basic<T>&
basis_basic<T>::operator[] (size_type i_comp) const
{
  return base::data().operator[] (i_comp);
}
template<class T>
inline
bool
basis_basic<T>::have_index_parameter() const
{
  return base::data().have_index_parameter();
}
template<class T>
inline
typename basis_basic<T>::valued_type
basis_basic<T>::valued_tag() const
{
  return base::data().valued_tag();
}
template<class T>
inline
const piola_fem<T>&
basis_basic<T>::get_piola_fem() const
{
  return base::data().get_piola_fem();
}
template<class T>
inline
const std::string&
basis_basic<T>::valued() const
{
  return base::data().valued();
}
template<class T>
inline
typename basis_basic<T>::size_type
      basis_basic<T>::ndof_on_subgeo (size_type map_dim, size_type subgeo_variant) const
{
  return base::data().ndof_on_subgeo (map_dim, subgeo_variant);
}
template<class T>
inline
typename basis_basic<T>::size_type
      basis_basic<T>::nnod_on_subgeo (size_type map_dim, size_type subgeo_variant) const
{
  return base::data().nnod_on_subgeo (map_dim, subgeo_variant);
}
template<class T>
inline
typename basis_basic<T>::size_type
      basis_basic<T>::first_idof_by_dimension (reference_element hat_K, size_type dim) const
{
  return base::data().first_idof_by_dimension (hat_K, dim);
}
template<class T>
inline
typename basis_basic<T>::size_type
      basis_basic<T>::first_inod_by_dimension (reference_element hat_K, size_type dim) const
{
  return base::data().first_inod_by_dimension (hat_K, dim);
}
template<class T>
inline
typename basis_basic<T>::size_type
      basis_basic<T>::ndof_on_subgeo_internal (size_type map_dim, size_type subgeo_variant) const
{
  return base::data().ndof_on_subgeo_internal (map_dim, subgeo_variant);
}
template<class T>
inline
typename basis_basic<T>::size_type
      basis_basic<T>::nnod_on_subgeo_internal (size_type map_dim, size_type subgeo_variant) const
{
  return base::data().nnod_on_subgeo_internal (map_dim, subgeo_variant);
}
template<class T>
inline
typename basis_basic<T>::size_type
      basis_basic<T>::first_idof_by_dimension_internal (reference_element hat_K, size_type dim) const
{
  return base::data().first_idof_by_dimension_internal (hat_K, dim);
}
template<class T>
inline
typename basis_basic<T>::size_type
      basis_basic<T>::first_inod_by_dimension_internal (reference_element hat_K, size_type dim) const
{
  return base::data().first_inod_by_dimension_internal (hat_K, dim);
}
template<class T>
inline
const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
basis_basic<T>::vdm (reference_element hat_K) const
{
  return base::data().vdm(hat_K);
}
template<class T>
inline
const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
basis_basic<T>::inv_vdm (reference_element hat_K) const
{
  return base::data().inv_vdm(hat_K);
}
template<class T>
template<class Value>
inline
void
basis_basic<T>::evaluate (
  reference_element      hat_K,
  const point_basic<T>&  hat_x,
  Eigen::Matrix<Value,Eigen::Dynamic,1>& value) const
{
  base::data().evaluate (hat_K, hat_x, value);
}
template<class T>
template<class Value>
inline
void
basis_basic<T>::evaluate_on_side (
    reference_element            tilde_K,
    const side_information_type& sid,
    const point_basic<T>&        hat_x,
    Eigen::Matrix<Value,Eigen::Dynamic,1>& value) const
{
  base::data().evaluate_on_side (tilde_K, sid, hat_x, value);
}
template<class T>
template<class Value>
inline
void
basis_basic<T>::grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<Value,Eigen::Dynamic,1>& value) const
{
  base::data().grad_evaluate (hat_K, hat_x, value);
}
template<class T>
template <class Function>
inline
void
basis_basic<T>::compute_dof (
  reference_element                  hat_K,
  const Function&                    f,
  Eigen::Matrix<T,Eigen::Dynamic,1>& dof) const
{
  return details::compute_dof (base::data(), hat_K, f, dof);
}
template<class T>
template<class Value>
inline
void
basis_basic<T>::compute_dofs (
    reference_element                            hat_K, 
    const Eigen::Matrix<Value,Eigen::Dynamic,1>& f_xnod,
          Eigen::Matrix<T,Eigen::Dynamic,1>&     dof) const
{
  base::data()._compute_dofs (hat_K, f_xnod, dof);
}
template<class T>
inline
const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
basis_basic<T>::hat_node (reference_element hat_K) const
{
  return base::data().hat_node (hat_K);
}
template<class T>
inline
void
basis_basic<T>::put (std::ostream& os, reference_element hat_K) const
{
  base::data().put (os, hat_K);
}
template<class T>
inline
void
basis_basic<T>::put_hat_node (std::ostream& os, reference_element hat_K) const
{
  base::data().put_hat_node (os, hat_K);
}
template<class T>
inline
typename basis_basic<T>::size_type
basis_basic<T>::local_ndof_on_side (
        reference_element            hat_K,
        const side_information_type& sid) const
{
  return base::data().local_ndof_on_side (hat_K, sid);
}
template<class T>
inline
void
basis_basic<T>::local_idof_on_side (
        reference_element            hat_K,
        const side_information_type& sid,
	Eigen::Matrix<size_type,Eigen::Dynamic,1>& loc_idof) const
{
  base::data().local_idof_on_side (hat_K, sid, loc_idof);
}
template<class T>
inline
void
basis_basic<T>::put_hat_node_on_side (
    std::ostream&                os,
    reference_element            hat_K,
    const side_information_type& sid) const
{
  base::data().put_hat_node_on_side (os, hat_K, sid);
}

}// namespace rheolef
#endif // _RHEO_BASIS_H
