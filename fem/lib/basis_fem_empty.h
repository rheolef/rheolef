#ifndef _RHEOLEF_BASIS_FEM_EMPTY_H
#define _RHEOLEF_BASIS_FEM_EMPTY_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*Class:sherwin
NAME: @code{empty} - the empty finite element
@cindex  polynomial basis
@clindex space
@clindex basis
@clindex reference_element
SYNOPSIS:
  space Zh (omega,"empty");
DESCRIPTION:
  @noindent
  This polynomial @code{basis} is used when an empty
  finite element set should be declared.
  This could be useful when product of space
  are considered and a special empty set is required, e.g.:
  @example
    string approx = (k > 0) ? "P"+std::to_string(k-1)+"d" : "empty";
    space Xh (omega, approx);
  @end example
  In this example, it represents the "P_{-1}" polynomial
  space that is empty by convention.
AUTHOR: Pierre.Saramito@imag.fr
DATE:   2 january 2019
End:
*/
#include "rheolef/basis.h"
namespace rheolef {

template<class T>
class basis_fem_empty: public basis_rep<T> {
public:
  typedef basis_rep<T>          base;
  typedef typename base::size_type size_type;
  basis_fem_empty();
  ~basis_fem_empty();
  std::string name() const { return family_name(); }
  bool have_index_parameter() const { return false; }
  std::string family_name() const { return "empty"; }
  size_type degree () const { return 0; }
  bool have_compact_support_inside_element() const { return true; }
  bool is_nodal() const { return true; }
  void evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& values) const;
  void grad_evaluate (
    reference_element                               hat_K,
    const point_basic<T>&                           hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& values) const;
  void _compute_dofs (
    reference_element     hat_K,
    const Eigen::Matrix<T,Eigen::Dynamic,1>&   f_xnod,
          Eigen::Matrix<T,Eigen::Dynamic,1>&   dof) const;
  void _initialize_cstor_sizes() const;
  void _initialize_data (reference_element hat_K) const;
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node(reference_element hat_K) const {
    base::_initialize_data_guard(hat_K);
    return _dummy_hat_node;
  }
protected:
  static const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> _dummy_hat_node;
};

} // namespace rheolef
#endif // _RHEOLEF_BASIS_FEM_EMPTY_H
