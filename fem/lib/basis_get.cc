///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// input basis from string or files
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 october 2017
//
// Note:
//  - some technical stuff with bison & flex
//  - some others for distributed issues:
//        * lecture du fichier sur proc=0 -> spec sur proc=0
//        * lecture ecriture spec sur stringstream sur proc=0
//        * broadcast string sur ts procs
//        * lecture du stringstream sur ts les proc -> spec
//        * conversion spec en basis sur ts les procs
//

// 
#include <sstream> // flex include it, but in namespace rheolef
#include <cstring>
#include "rheolef/basis.h"
#include "rheolef/basis_get.h"

namespace rheolef {

// ================================================================================
// part 1 : basis specification
// ================================================================================

static family_index_option_type _current_fio;

} // namespace rheolef

// ================================================================================
// part 2 : read from istream and build family_index_option_type basis specification
// ================================================================================
/* AIX requires this to be the first thing in the file.  */
#ifndef __GNUC__
# if _RHEOLEF_HAVE_ALLOCA_H
#  include <alloca.h>
# else
#  ifdef _AIX
#pragma alloca
#  else
#   ifndef alloca /* predefined by HP cc +Olibcalls */
char *alloca ();
#   endif
#  endif
# endif
#endif

namespace rheolef {

using namespace std;

typedef size_t size_type;

static size_type   basis_line_no = 1;
static size_type   basis_n_error = 0;
static std::string basis_input_string;

extern int basis_lex();
void basis_error (const char* msg) {
  std::string near;
  error_macro("invalid basis name: \"" << basis_input_string << "\"");
  basis_n_error++;
}
int basis_wrap () { return 1; }

static std::vector<std::string> symbol_table;
static const std::string& symbol (size_t i) { return symbol_table[i]; }
static size_t insert (const std::string& str) {
        size_t i = symbol_table.size();
        symbol_table.push_back (str);
        return i;
}
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++" // avoid FlexLexer warnings
#define YYMALLOC ::malloc
#define YYFREE   ::free
#include "basis_yacc.cc"
// avoid re-definition of YY_NULL within flex
#ifdef YY_NULL
#undef YY_NULL
#endif
#include "basis_lex.cc"
#pragma GCC diagnostic pop

static yyFlexLexer input_basis;

int basis_lex() { return input_basis.yylex(); }

static void basis_get_pass_2 (std::string& str);

// ================================================================================
// read from string via istrstream
// ================================================================================
// parse from string on all procs and re-build result_ptr available on all procs
static
void
internal_build_fio_from_string (const std::string& str)
{
  std::istringstream istrstr;
  // force reading until end of string
  // otherwise skip all optional terms as "d" and "{...}" !
  std::string str_eol = str + ";";
  istrstr.str (str_eol);
  input_basis.yyrestart(istrstr);
  symbol_table.clear();
  _current_fio = family_index_option_type();
  basis_line_no = 1;
  basis_n_error = 0;
  basis_input_string = str; // for error msg
  if (basis_parse() != 0 || basis_n_error != 0) {
    _current_fio = family_index_option_type();
    error_macro ("invalid basis name: \"" << str << "\"");
  }
  symbol_table.clear();
  // then _current_fio is available on all procs
}
void
basis_parse_from_string (const std::string& str, family_index_option_type& fio) 
{
  internal_build_fio_from_string (str);
  fio = _current_fio;
}
// the same for raw basis, without options:
template<class T>
void
basis_raw_basic<T>::reset (std::string& str)
{
  if (str == "") {
    base::operator= (0);
    return;
  }
  internal_build_fio_from_string (str);
  std::string name = _current_fio.family + std::to_string(_current_fio.index);
  base::operator= (basis_raw_rep<T>::make_ptr(name));
  _clear();
  _current_fio = family_index_option_type();
}
// -----------------------------------------------------------------------------
// instanciation in library
// -----------------------------------------------------------------------------
#define _RHEOLEF_instanciate(T) 						\
template void basis_raw_basic<T>::reset (std::string&);				\

_RHEOLEF_instanciate(Float)

#undef _RHEOLEF_instanciate

} // namespace rheolef
