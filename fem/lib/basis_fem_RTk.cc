///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "basis_fem_RTk.h"
#include "equispaced.icc"
#include "warburton.icc"
#include "basis_ordering.icc"
#include "reference_element_aux.icc"
#include "eigen_util.h"
#include "basis_on_pointset_evaluate.icc"
#include "piola_fem_hdiv.h"

namespace rheolef {
using namespace std;

// =========================================================================
// basis members
// =========================================================================
// allocators
// -------------------------------------------------------------------------
template<class T>
basis_fem_RTk<T>::~basis_fem_RTk()
{
}
template<class T>
basis_fem_RTk<T>::basis_fem_RTk (size_type k, const basis_option& sopt) 
  : basis_rep<T> (sopt),
    _b_pre_kp1(),
    _hat_node(),
    _vdm(),
    _inv_vdm(),
    _quad(),
    _bar_a(),
    _bkm1_node_internal_d()
{
  base::_name = base::standard_naming (family_name(), k, base::_sopt);
  // requieres a hierarchical pre basis => Dubiner or monomial
  // - monomial is ill conditionned, but easier to decompose
  // - dubiner: TODO project the tilde_psi basis on it
  // note: the internal dof basis is independent 
  _b_pre_kp1 = basis_raw_basic<T> ("M"+std::to_string(k+1));
  _initialize_cstor_sizes();

  // piola FEM transformation:
  typedef piola_fem_hdiv<T> piola_fem_type;
  base::_piola_fem.piola_fem<T>::base::operator= (new_macro(piola_fem_type));
}
template<class T>
const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
basis_fem_RTk<T>::hat_node (reference_element hat_K) const
{
  base::_initialize_data_guard (hat_K);
  return _hat_node [hat_K.variant()];
}
template<class T>
const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
basis_fem_RTk<T>::vdm (reference_element hat_K) const
{
  base::_initialize_data_guard (hat_K);
  return _vdm [hat_K.variant()];
}
template<class T>
const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
basis_fem_RTk<T>::inv_vdm (reference_element hat_K) const
{
  base::_initialize_data_guard (hat_K);
  return _inv_vdm [hat_K.variant()];
}
template<class T>
void
basis_fem_RTk<T>::_initialize_cstor_sizes() const
{
  size_type k = degree()-1;
  for (size_type map_d = 0; map_d < 4; ++map_d) {
    base::_ndof_on_subgeo_internal [map_d].fill (0);
  }
  // --------------------------------------------------------------------------
  // 1. compute ndof and dof pointers:
  // --------------------------------------------------------------------------
  // see BreFor-1991 p. 116 for K=t,T & p. 119 for K=q,H ; BerDur-2013 for K=P
  base::_ndof_on_subgeo_internal [0][reference_element::p] = 1;
  base::_ndof_on_subgeo_internal [1][reference_element::p] = 1;
  base::_ndof_on_subgeo_internal [1][reference_element::e] = k; // RT0 is P1 on 1d with K=e
  base::_ndof_on_subgeo_internal [2][reference_element::e] = k+1;
  base::_ndof_on_subgeo_internal [2][reference_element::t] = k*(k+1);
  base::_ndof_on_subgeo_internal [2][reference_element::q] = 2*k*(k+1); // Rav-1981, p. 23
  base::_ndof_on_subgeo_internal [3][reference_element::t] = (k+1)*(k+2)/2;
  base::_ndof_on_subgeo_internal [3][reference_element::q] = sqr(k+1);
  base::_ndof_on_subgeo_internal [3][reference_element::T] = 3*k*(k+1)*(k+2)/6;
  base::_ndof_on_subgeo_internal [3][reference_element::P] = sqr(k)*(k+1)/2; // see BerDur-2013
  base::_ndof_on_subgeo_internal [3][reference_element::H] = 3*k*sqr(k+1);
  base::_helper_make_discontinuous_ndof_on_subgeo (base::is_continuous(), base::_ndof_on_subgeo_internal, base::_ndof_on_subgeo);
  base::_helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (base::_ndof_on_subgeo_internal, base::_first_idof_by_dimension_internal);
  base::_helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (base::_ndof_on_subgeo,          base::_first_idof_by_dimension);
  // --------------------------------------------------------------------------
  // 2. nodes for dofs : interpolate v.n on sides + internal quadrature nodes
  // --------------------------------------------------------------------------
  // nodes coincides with dofs on sides : Lagrange nodes
  // interior nodes are quadrature node
  quadrature_option qopt;
  qopt.set_family (quadrature_option::gauss);
  // order = 2*(k-1)) for P_{k-1}     internal modes for     simplicial elts hat_K=etT
  //       = 2*k      for P_{k-1,k,k} internal modes for non-simplicial elts hat_K=qHP
  qopt.set_order  (2*(k+2)); // takes the max to have an uniform quadrature for any cases TODO: too much ? min-adjust=2*k to converge
  _quad = quadrature<T> (qopt);
  for (size_type map_d = 0; map_d < 4; ++map_d) {
    base::_nnod_on_subgeo_internal [map_d].fill (0);
  }
  base::_nnod_on_subgeo_internal [0][reference_element::p] = 1;
  base::_nnod_on_subgeo_internal [1][reference_element::p] = 1;
  base::_nnod_on_subgeo_internal [1][reference_element::e] = k == 0 ? 0 : _quad.size (reference_element::e);
  base::_nnod_on_subgeo_internal [2][reference_element::e] = k+1;
  base::_nnod_on_subgeo_internal [2][reference_element::t] = k*(k+1)/2; // k == 0 ? 0 : _quad.size (reference_element::t);
  base::_nnod_on_subgeo_internal [2][reference_element::q] = k == 0 ? 0 : _quad.size (reference_element::q);
  base::_nnod_on_subgeo_internal [3][reference_element::t] = (k+1)*(k+2)/2;
  base::_nnod_on_subgeo_internal [3][reference_element::q] = sqr(k+1);
  base::_nnod_on_subgeo_internal [3][reference_element::T] = k == 0 ? 0 : _quad.size (reference_element::T);
  base::_nnod_on_subgeo_internal [3][reference_element::P] = k == 0 ? 0 : _quad.size (reference_element::P);
  base::_nnod_on_subgeo_internal [3][reference_element::H] = k == 0 ? 0 : _quad.size (reference_element::H);
  base::_helper_make_discontinuous_ndof_on_subgeo (base::is_continuous(), base::_nnod_on_subgeo_internal, base::_nnod_on_subgeo);
  base::_helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (base::_nnod_on_subgeo_internal, base::_first_inod_by_dimension_internal);
  base::_helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (base::_nnod_on_subgeo,          base::_first_inod_by_dimension);
}
template<class T>
void
basis_fem_RTk<T>::_initialize_data (reference_element hat_K) const
{
  if (hat_K.dimension() == 0) return; // nothing to do

  // abbrevs
  size_type d = hat_K.dimension();
  size_type k = degree()-1;
  size_type variant = hat_K.variant();

  // --------------------------------------------------------------------------
  // 1. nodes for dofs : interpolate v.n on sides + internal quadrature nodes
  // --------------------------------------------------------------------------
  // 1.1. insert nodes
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node = _hat_node [hat_K.variant()];
  hat_node.resize (base::_first_inod_by_dimension [variant][d+1]);
  size_type loc_nnod = hat_node.size();
  // 1.2. nodes on sides
  size_type loc_inod = 0;
  for (size_type loc_isid = 0, loc_nsid = hat_K.n_subgeo(d-1); loc_isid < loc_nsid; ++loc_isid) {
    reference_element hat_S = hat_K.subgeo (d-1, loc_isid);
    size_type sid_variant  = hat_S.variant();
    size_type loc_nsidvert = hat_S.n_vertex();
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> hat_node_sid;
    const size_type internal = 1;
    switch (base::_sopt.get_node()) {
      case basis_option::warburton:
#ifdef TODO
        // TODO: pointset warburton could support the "internal=1" option
        pointset_lagrange_warburton  (sid_variant, k, hat_node_sid, internal); break;
#endif // TODO
      case basis_option::equispaced:
        pointset_lagrange_equispaced (sid_variant, k, hat_node_sid, internal); break;
      default:
        error_macro ("unsupported node set: " << base::_sopt.get_node_name());
    }
    point loc_vertex [4]; // restricted to point_basic<Float> in reference_element::vertex()
    for (size_type loc_jsidvert = 0; loc_jsidvert < loc_nsidvert; ++loc_jsidvert) {
      size_type loc_jvertex = hat_K.subgeo_local_vertex (d-1, loc_isid, loc_jsidvert);
      loc_vertex[loc_jsidvert] = hat_K.vertex (loc_jvertex);
    }
    for (size_type loc_inod_sid = 0, loc_nnod_sid = hat_node_sid.size(); loc_inod_sid < loc_nnod_sid; ++loc_inod_sid) {
      check_macro (loc_inod < size_t(hat_node.size()), "invalid loc_inod");
      T xi0 = hat_node_sid [loc_inod_sid][0],
        xi1 = hat_node_sid [loc_inod_sid][1];
      if (loc_nsidvert == 4) { // map from [-1,1]^2 to [0,1]^2
        xi0 = (1+xi0)/2;
        xi1 = (1+xi1)/2;
      }
      for (size_type alpha = 0; alpha < d; ++alpha) {
        hat_node[loc_inod][alpha] = loc_vertex [0][alpha];
        if (d >= 2) {
          hat_node[loc_inod][alpha] += xi0*(loc_vertex[1][alpha] - loc_vertex[0][alpha]);
        }
        if (d == 3) {
          hat_node[loc_inod][alpha] += xi1*(loc_vertex[loc_nsidvert-1][alpha] - loc_vertex[0][alpha]);
        }
      }	
      ++loc_inod;
    }
  }
  // 1.3. insert internal quadrature nodes for integrating exactly P(k-1) in hat_K, when k>0
  if (k > 0)  {
    if (variant == reference_element::t) {
      // experimental for triangle: internal interpolation without quadrature
      Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> hat_node_internal;
      const size_type internal = 1;
      switch (base::_sopt.get_node()) {
        case basis_option::warburton:
#ifdef TODO
	    // TODO: pointset warburton could support the "internal=1" option
            pointset_lagrange_warburton  (variant, k-1, hat_node_internal, internal); break;
#endif // TODO
        case basis_option::equispaced:
            pointset_lagrange_equispaced (variant, k-1, hat_node_internal, internal); break;
        default:
            error_macro ("unsupported node set: " << base::_sopt.get_node_name());
      }
      trace_macro ("hat_node_internal.size="<<hat_node_internal.size());
      for (size_type loc_inod_int = 0; loc_inod_int < size_type(hat_node_internal.size()); ++loc_inod_int) {
        trace_macro ("hat_node_internal["<<loc_inod_int<<"]="<<hat_node_internal[loc_inod_int]);
        hat_node[loc_inod] = hat_node_internal [loc_inod_int];
        ++loc_inod;
      }
    } else { // variant == TPH : still quadrature
      for (typename quadrature<T>::const_iterator iter_q = _quad.begin(hat_K),
	    last_q = _quad.end(hat_K); iter_q != last_q; iter_q++) {	
        hat_node[loc_inod] = (*iter_q).x;
        ++loc_inod;
      }
    }
  }
  check_macro (loc_inod == loc_nnod, "invalid node count: loc_inod="<<loc_inod<<" and loc_nnod="<<loc_nnod);

  // --------------------------------------------------------------------------
  // 2. build a transformation tilde_A for evaluating polynomials from (b_pre_{k+1})^d
  //    from a pre-basis:
  //    tilde_psi = {(p,0), (0,p), p in Bkm1} cup {(x0*p,x1*p), p in bar_Bk}
  // --------------------------------------------------------------------------
  // 2.1. compute ndof per side, for each side (prism have different sides)
  size_type dim_Pkm1 = (k == 0) ? 0 : reference_element::n_node(hat_K.variant(), k-1);
  size_type dim_Pk   =  reference_element::n_node (hat_K.variant(), k);
  size_type dim_Pkp1 = _b_pre_kp1.ndof (hat_K);
  size_type loc_ndof_sid_tot = 0;
  for (size_type loc_isid = 0, loc_nsid = hat_K.n_subgeo(d-1); loc_isid < loc_nsid; ++loc_isid) {
    reference_element hat_S = hat_K.subgeo (d-1, loc_isid);
    // dofs on sides are Lagrange dofs, associated to nodes:
    loc_ndof_sid_tot += base::_nnod_on_subgeo_internal [d][hat_S.variant()];
  }
  size_type loc_ndof = base::_first_idof_by_dimension [variant][d+1];
  size_type loc_nnod_sid_tot = loc_ndof_sid_tot; // dofs on sides are Lagrange one, associated to nodes

#ifdef TO_CLEAN
  // check size
  warning_macro ("first_inod(hat_K,d)  ="<<base::first_inod(hat_K,d));
  warning_macro ("first_inod(hat_K,d+1)="<<base::first_inod(hat_K,d+1));
  warning_macro ("dim(P"<<int(k-1)<<")="<<dim_Pkm1);
  warning_macro ("dim(P"<<k<<")="<<dim_Pk);
  warning_macro ("dim(P"<<k+1<<")="<<dim_Pkp1);
  warning_macro ("loc_ndof_sid_tot="<<loc_ndof_sid_tot);
  warning_macro ("size(hat_K)="<<base::ndof (hat_K));
  warning_macro ("first_idof(hat_K,d-1)="<<base::first_idof(hat_K,d-1));
  warning_macro ("first_idof(hat_K,d)  ="<<base::first_idof(hat_K,d));
  warning_macro ("first_idof(hat_K,d+1)="<<base::first_idof(hat_K,d+1));
  warning_macro ("dim(RT"<<k<<")="<<loc_ndof);
  warning_macro ("dim((P"<<k+1<<")^"<<d<<")="<<d*dim_Pkp1);
  warning_macro ("a_tilde(loc_ndof)="<<loc_ndof<<",d*dim(Pkp1)="<<d*dim_Pkp1<<")");
#endif // TO_CLEAN

  check_macro (loc_ndof == base::ndof (hat_K), "invalid ndof="<<loc_ndof<<" != "<< base::ndof (hat_K));
  //
  // 2.2. decompose the tilde_psi RTk basis on the basis of (P_{k+1})^d
  //      note: explicit easy decomposition when using monomial basis for (P_{k+1})^d
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> tilde_a (loc_ndof, d*dim_Pkp1);
  tilde_a.fill (0);
  // 2.2.1 homogeneous polynoms:
  //     (q,0) and (0,r), q,r in basis(Pk) 
  //     leads to a diag matrix block
  //     since basis(Pk) subset basis(P_{k+1}) and the basis is hierarchical
  trace_macro ("basis(1): (q,0) and (0,r), q,r in basis(Pk)...");
  for (size_type loc_comp_idof = 0; loc_comp_idof < dim_Pk; ++loc_comp_idof) {
    for (size_type alpha = 0; alpha < d; ++alpha) {
      size_type loc_idof  = d*loc_comp_idof + alpha;
      size_type loc_jpkp1 = d*loc_comp_idof + alpha;
      trace_macro ("loc_idof="<<loc_idof<<", loc_jpkp1="<<loc_jpkp1);
      tilde_a (loc_idof, loc_jpkp1) = 1;
    }
  }
  // 2.2.2 non-homogeneous polynoms
  trace_macro ("basis(2): (x0*p,x1*p), p in basis(P_k)\basis(P_{k-1}) [exactly k degree]");
  std::vector<size_type> inod2ideg_kp1;
  build_inod2ideg (hat_K, k+1, inod2ideg_kp1);
  switch (hat_K.variant()) {
    case reference_element::e: 
    case reference_element::t: 
    case reference_element::T: { 
      // triangle :  (x0*p,x1*p), p in basis(P_k)\basis(P_{k-1}) [exactly k degree] 
      //            =(b_iO,b_i1) in P_{k+1} when b is the hierarchical monomial basis
      //           and i0=ilat2ideg([i+1,k-i  ]), i=0..k
      //               i1=ilat2ideg([i  ,k-i+1])
      std::vector<point_basic<size_type> > power_index;
      make_power_indexes_sorted_by_degrees (hat_K, k, power_index);
      for (size_type loc_ideg = dim_Pkm1, loc_ndeg = power_index.size(); loc_ideg < loc_ndeg; ++loc_ideg) {
        for (size_type alpha = 0; alpha < d; ++alpha) {
          point_basic<size_type> ilat = power_index [loc_ideg];
          size_type loc_idof = d*dim_Pk + (loc_ideg - dim_Pkm1);
          ilat [alpha]++; // x_alpha*monomial(i,k-i) = x^{i+1}*y^{k-i} when alpha=0, etc
          size_type loc_inod_kp1 = ilat2loc_inod (hat_K, k+1, ilat);
          size_type loc_ideg_kp1 = inod2ideg_kp1 [loc_inod_kp1];
          size_type loc_jpkp1d = d*loc_ideg_kp1 + alpha;
          trace_macro ("loc_idof="<<loc_idof<<", loc_jpkp1d="<<loc_jpkp1d);
          tilde_a (loc_idof, loc_jpkp1d) = 1;
	}
      }
      break;
    }
    case reference_element::q:
    case reference_element::H: {
      // quadrangle : (x0*p,   0)
      //              (    ,x1*q), p,q in basis(P_k)\basis(P_{k-1}) [exactly k degree] 
      //             =(b_i0,   0) 
      //              (   0,b_i1), b_i0, b_i1 in P_{k+1} when b is the hierarchical monomial basis
      //           and i0=ilat2ideg([i+1,j  ]), i=0..k
      //               i1=ilat2ideg([i  ,j+1])
      size_type sub_variant = (d == 2) ? reference_element::e : reference_element::q;
      reference_element hat_sub (sub_variant); // q=e*e ; H = e*q
      std::vector<point_basic<size_type> > power_index_sub;
      make_power_indexes_sorted_by_degrees (hat_sub, k, power_index_sub);
      for (size_type loc_ideg = 0, loc_ndeg = power_index_sub.size(); loc_ideg < loc_ndeg; ++loc_ideg) {
        for (size_type alpha = 0; alpha < d; ++alpha) {
          point_basic<size_type> ilat_sub = power_index_sub [loc_ideg];
          size_type loc_idof = d*dim_Pk + d*loc_ideg + alpha;
          point_basic<size_type> ilat (0,0,0);
          ilat [alpha]       = k+1; // (x^{k+1}*p(y), 0) & (0, p(x)*y^{k+1}) with p in Pk(sub) 
          ilat [(alpha+1)%d] = ilat_sub[0]; 
          if (d == 3) ilat [(alpha+2)%d] = ilat_sub[1]; 
          size_type loc_inod_kp1 = ilat2loc_inod (hat_K, k+1, ilat);
          size_type loc_ideg_kp1 = inod2ideg_kp1 [loc_inod_kp1];
          size_type loc_jpkp1d = d*loc_ideg_kp1 + alpha;
          trace_macro ("loc_idof="<<loc_idof<<", loc_jpkp1d="<<loc_jpkp1d);
          tilde_a (loc_idof, loc_jpkp1d) = 1;
        }
      }
      break;
    }
    case reference_element::P:
    default: error_macro ("unexpected element `"<<hat_K.name()<<"' (HINT: see BerDur-2013)");
  }
#undef DEBUG_RTK // print matrix
#ifdef DEBUG_RTK
  cout << setprecision(std::numeric_limits<T>::digits10)
       << "tilde_a=[" << endl << tilde_a <<"]"<<endl
       << "[u,s,vt]=svd(tilde_a)" << endl
       << "id=eye(size(s))" << endl
       << "a=u*id*vt" << endl
    ;
#endif // DEBUG_RTK
  // --------------------------------------------------------------------------
  // 3. build a transformation A for evaluating polynomials from (b_pre_{k+1})^d
  //    for a raw-basis:
  //      psi = A*b_pre
  // --------------------------------------------------------------------------
  // get psi raw RTk basis by othogonalizing the tilde_psi RTk basis 
  // note: use SVD
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>
    u  (tilde_a.rows(), tilde_a.rows()),
    vt (tilde_a.cols(), tilde_a.cols());
  Eigen::Matrix<T,Eigen::Dynamic,1> s  (tilde_a.rows());
  Eigen::JacobiSVD<Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> >
	svd (tilde_a, Eigen::ComputeFullU | Eigen::ComputeFullV);
  // SVD: tilde_a = u*s1*trans(vt) TODO check with eigen u/ut v/vt !
  s  = svd.singularValues();
  u  = svd.matrixU();
  vt = svd.matrixV();
  size_type rank_s = 0;
  T eps = std::numeric_limits<T>::epsilon();
  for (size_type loc_idof = 0; loc_idof < size_type(s.size()); ++loc_idof) {
    rank_s += (abs(s[loc_idof]) > eps) ? 1 : 0;
  }
  check_macro (rank_s == loc_ndof,
    "invalid polynomial space dimension = " << rank_s << " < loc_ndof = " << loc_ndof);
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> id (tilde_a.rows(), tilde_a.cols());
  id.setIdentity();
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> a = id*vt.transpose();

#ifdef DEBUG_RTK
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> s1 (tilde_a.rows(), tilde_a.cols());
  s1.fill(0);
  for (size_type iloc = 0; iloc < size_t(tilde_a.rows()); iloc++) {
    s1(iloc,iloc) = s(iloc);
  }
  cout << "s1  = ["<< endl << s1 <<"];" << endl
       << "u1  = ["<< endl << u  <<"];"<<endl
       << "vt1 = ["<< endl << vt <<"];"<<endl
       << "id1 = ["<< endl << id <<"];"<<endl
       << "a1  = ["<< endl << a  <<"]"<<endl
       << "err=norm(tilde_a-u*s*vt')"<<endl
       << "err1=norm(tilde_a-u1*s1*vt1')"<<endl
       << "err_a=abs(a-a1)"<<endl
       << "err_svd=max(max(abs(a-a1)))"<<endl
       << "err_u=max(max(abs(u-u1)))"<<endl
       << "err_v=max(max(abs(vt-vt1)))"<<endl
       << "err_s=max(diag(s1)-diag(s))"<<endl
    ;
  T err_svd = (tilde_a - u*s1*vt.transpose()).norm();
  cout << "err_svd = " << err_svd << endl;
#endif // DEBUG_RTK

  // -------------------------------------------------------------------------------
  // 4. build a transformation bar_A for evaluating polynomials from (b_pre_{k+1})^d
  //    for the dof-basis:
  //      phi = A*b_pre
  // -------------------------------------------------------------------------------
  // 4.1. defines the quadrature nodes, for integral dofs
  //      and evaluate the basis of P_{k-1} on it
  std::array<
    Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>
   ,3>&
   bkm1_node_internal_d = _bkm1_node_internal_d [variant];
  if (k > 0) {
    size_type loc_nnod_int = base::nnod_on_subgeo (d,hat_K.variant());
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> hat_node_internal (loc_nnod_int);
    for (size_type loc_inod_int = 0; loc_inod_int < loc_nnod_int; ++loc_inod_int) {
      size_type loc_inod = loc_nnod_sid_tot + loc_inod_int;
      hat_node_internal [loc_inod_int] = hat_node [loc_inod];
    }
    string basis_dof_name;
    switch (base::_sopt.get_raw_polynomial()) {
      case basis_option::monomial:  basis_dof_name = "M"; break;
      case basis_option::dubiner:   basis_dof_name = "D"; break;
      case basis_option::bernstein: basis_dof_name = "B"; break;
      default: error_macro ("unsupported raw polynomial basis `"<<base::_sopt.get_raw_polynomial_name()<<"'");
    }
trace_macro("basis_dof_name="<<basis_dof_name);
    // bkm1_node_internal(i,j) = bj(xi)
    switch (variant) {
      case reference_element::e: 
      case reference_element::t: 
      case reference_element::T: { 
        // int_K b*v dx with v=[p,0],[0,p] and p in P_{k-1}
        basis_raw_basic<T> b_dof_km1 (basis_dof_name+std::to_string(k-1));
        details::basis_on_pointset_evaluate (b_dof_km1, hat_K, hat_node_internal, bkm1_node_internal_d[0]);
        for (size_type alpha = 1; alpha < d; ++alpha) {
          bkm1_node_internal_d [alpha] = bkm1_node_internal_d [0];
        }
        break;
      }
      case reference_element::q:
      case reference_element::H: {
        // int_K b*v dx with v=[p,0],[0,q] and p in P_{k-1,k}, q in P_{k,k-1}
        basis_raw_basic<T> b_dof_km1 (basis_dof_name+std::to_string(k-1)),
                           b_dof_k   (basis_dof_name+std::to_string(k));
        std::array<Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>,3> hat_node_internal_comp;
        std::array<
          Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>
         ,3> 
         bkm1_node_internal_comp,
         bk_node_internal_comp;
        reference_element hat_e (reference_element::e);
        for (size_type alpha = 0; alpha < d; ++alpha) {
          hat_node_internal_comp [alpha].resize (hat_node_internal.size());
          for (size_type loc_inod_int = 0, loc_nnod_int = hat_node_internal.size(); loc_inod_int < loc_nnod_int; ++loc_inod_int) {
            // resize from q=[-1,1]^2 to e=[0,1]
            hat_node_internal_comp [alpha][loc_inod_int][0] = (1+hat_node_internal [loc_inod_int][alpha])/2;
          }
          details::basis_on_pointset_evaluate (b_dof_km1, hat_e, hat_node_internal_comp[alpha], bkm1_node_internal_comp[alpha]);
          details::basis_on_pointset_evaluate (b_dof_k,   hat_e, hat_node_internal_comp[alpha],   bk_node_internal_comp[alpha]);
        }
        size_type loc_ndof_int = bkm1_node_internal_comp[0].cols()*pow(bk_node_internal_comp[0].cols(),d-1);
        for (size_type alpha = 0; alpha < d; ++alpha) {
          size_type alpha2 = (alpha+1)%d;
          size_type alpha3 = (alpha+2)%d;
          bkm1_node_internal_d [alpha].resize (hat_node_internal.size(), loc_ndof_int);
          for (size_type loc_inod_int = 0, loc_nnod_int = hat_node_internal.size(); loc_inod_int < loc_nnod_int; ++loc_inod_int) {
            size_type loc_idof_int = 0;
            if (variant == reference_element::q) {
              for (size_type i = 0; i < size_type(bkm1_node_internal_comp [alpha].cols()); ++i) {
              for (size_type j = 0; j < size_type(  bk_node_internal_comp [alpha].cols()); ++j) {
                   bkm1_node_internal_d    [alpha ] (loc_inod_int,loc_idof_int)
                 = bkm1_node_internal_comp [alpha ] (loc_inod_int,i)
                 *   bk_node_internal_comp [alpha2] (loc_inod_int,j);
                 ++loc_idof_int;
              }}
            } else {
              for (size_type i = 0; i < size_type(bkm1_node_internal_comp [alpha].cols()); ++i) {
              for (size_type j = 0; j < size_type(  bk_node_internal_comp [alpha].cols()); ++j) {
              for (size_type k = 0; k < size_type(  bk_node_internal_comp [alpha].cols()); ++k) {
                   bkm1_node_internal_d    [alpha ] (loc_inod_int,loc_idof_int)
                 = bkm1_node_internal_comp [alpha ] (loc_inod_int,i)
                 *   bk_node_internal_comp [alpha2] (loc_inod_int,j)
                 *   bk_node_internal_comp [alpha3] (loc_inod_int,k);
                 ++loc_idof_int;
              }}}
            }
          }
        }
        break;
      }
      case reference_element::P:
      default: error_macro ("unexpected element `"<<hat_K.name()<<"'");
    }
  }
  // -----------------------------------------
  // 4.2. compute basis of (P_{k+1})^d at all nodes
  // -----------------------------------------
  size_type loc_n_bkp1 = _b_pre_kp1.ndof (hat_K);
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> bkp1_node (loc_nnod, loc_n_bkp1);
  details::basis_on_pointset_evaluate (_b_pre_kp1, hat_K, hat_node, bkp1_node); // bkp1_node(i,j) = bj(xi)
  // vector expansion: bkp1d_node(i,d*j+alpha) = [bj(xi),0,0], [0,bj(xi),0], etc
  // -> compute the dofs for all this expansion
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> bkp1d_dof (loc_ndof, d*loc_n_bkp1); // dof_i(bjd)
  bkp1d_dof.fill (std::numeric_limits<T>::max());
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>     bkp1d_j_node (loc_nnod);
  Eigen::Matrix<T,Eigen::Dynamic,1>                  bkp1d_j_dof  (loc_ndof);
  for (size_type loc_j_bkp1 = 0; loc_j_bkp1 < loc_n_bkp1; ++loc_j_bkp1) {
    for (size_type alpha = 0; alpha < d; ++alpha) {
      for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
        bkp1d_j_node [loc_inod]     = point_basic<T>(0,0,0);
        bkp1d_j_node [loc_inod][alpha] = bkp1_node(loc_inod,loc_j_bkp1);
      }
      bkp1d_j_dof.fill (std::numeric_limits<T>::max());
      _compute_dofs (hat_K, bkp1d_j_node, bkp1d_j_dof);
      size_type loc_j_bkp1d = d*loc_j_bkp1 + alpha;
      check_macro (bkp1d_dof.rows() == bkp1d_j_dof.size(), "invalid sizes");
      bkp1d_dof.col (loc_j_bkp1d) = bkp1d_j_dof;
    }
  }
  // -----------------------------------------
  // 4.3. vdm
  // -----------------------------------------
  // VDM(i,j) = dof_i(phi_j)
  //          = phi_j.n(xi) for side dofs
  //            int phi_j(x)*p(x)*dx, for all p in P(k-1)
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&     vdm = _vdm    [hat_K.variant()];
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& inv_vdm = _inv_vdm[hat_K.variant()];
  vdm = bkp1d_dof*a.transpose(); // = trans(a*trans(bkp1d_dof));
  bool invert_ok = invert(vdm, inv_vdm);
  check_macro (invert_ok,
        "unisolvence failed for " << base::name() <<"(" << hat_K.name() << ") basis");
  // -----------------------------------------
  // 4.4. final composition matrix: bar_a = trans(inv_vdm)*a
  // -----------------------------------------
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& bar_a = _bar_a [hat_K.variant()];
  bar_a = inv_vdm.transpose()*a;
#ifdef DEBUG_RTK
  cout << "bkp1d_dof=[" << endl << bkp1d_dof <<"]"<<endl
       << "vdm=[" << endl << vdm <<"]"<<endl
       << "det_vdm=" << vdm.determinant() <<endl
       << "cond_vdm=" << cond(vdm) <<endl
       << "bar_a1=inv(vdm)'*a;"<<endl
       << "bar_a=[" << endl << bar_a <<"]"<<endl;
#endif // DEBUG_RTK
}
// ----------------------------------------------------------------------------
// evaluation of all basis functions at hat_x:
// ----------------------------------------------------------------------------
template<class T>
void
basis_fem_RTk<T>::evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<value_type,Eigen::Dynamic,1>& value) const
{
  base::_initialize_data_guard (hat_K);
  size_type d = hat_K.dimension();
  size_type loc_ndof = base::ndof (hat_K);
  //
  // 1) evaluate the basis of P_{k+1}(hat_K) at hat_x : bkp1(x)
  //
  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& bar_a = _bar_a [hat_K.variant()];
  Eigen::Matrix<T,Eigen::Dynamic,1> bkp1;
  _b_pre_kp1.evaluate (hat_K, hat_x, bkp1);
  value.resize (loc_ndof);
  //
  // 2) evaluate the basis of RTk at hat_x: phi(x)
  //      [phi(x)] = [A2]*[bkp1d(x)]
  //    where bkp1d = basis of (P_{k+1}(hat_K))^d by vectorization
  //                      nb
  //                     ---
  //                     \.
  //  phi_{i,a}(hat_x) = /    A(i,[j,a])*b_j(hat_x)
  //                     ---
  //                     j=0
  for (size_type loc_idof = 0; loc_idof < loc_ndof; ++loc_idof) {
    value[loc_idof] = point_basic<T>(0,0,0);
    for (size_type loc_jdof_bkp1 = 0, loc_ndof_bkp1 = bkp1.size(); loc_jdof_bkp1 < loc_ndof_bkp1; ++loc_jdof_bkp1) {
      for (size_type alpha = 0; alpha < d; ++alpha) {
        size_type loc_jdof_bkp1d = d*loc_jdof_bkp1 + alpha;
        value[loc_idof][alpha] += bar_a(loc_idof,loc_jdof_bkp1d)*bkp1[loc_jdof_bkp1];
      }
    }
  }
}
template<class T>
void
basis_fem_RTk<T>::grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,1>& value) const
{
  base::_initialize_data_guard (hat_K);
  size_type d = hat_K.dimension();
  size_type loc_ndof = base::ndof (hat_K);
  //
  // 1) evaluate the grad basis of P_{k+1}(hat_K) at hat_x : grad bkp1(x)
  //
  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& bar_a = _bar_a [hat_K.variant()];
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> grad_bkp1;
  _b_pre_kp1.grad_evaluate (hat_K, hat_x, grad_bkp1);
  value.resize (loc_ndof);
  //
  // 2) evaluate the grad basis of RTk at hat_x: grad phi(x)
  //      [grad phi(x)] = [A2]*[grad bkp1d(x)]
  //    where grad bkp1d = grad basis of (P_{k+1}(hat_K))^d by vectorization
  //
  //                      nb
  //                     ---
  // d phi_{i,a}         \                 d b_j
  // ----------(hat_x) = /    A(i,[j,a])*--------(hat_x)
  // d hat_x_b           ---             d hat_x_b
  //                     j=0
  for (size_type loc_idof = 0; loc_idof < loc_ndof; ++loc_idof) {
    value[loc_idof] = tensor_basic<T>();
    for (size_type loc_jdof_bkp1 = 0, loc_ndof_bkp1 = grad_bkp1.size(); loc_jdof_bkp1 < loc_ndof_bkp1; ++loc_jdof_bkp1) {
      for (size_type alpha = 0; alpha < d; ++alpha) {
        size_type loc_jdof_bkp1d = d*loc_jdof_bkp1 + alpha;
        for (size_type beta = 0; beta < d; ++beta) {
          value[loc_idof](alpha,beta) += bar_a(loc_idof,loc_jdof_bkp1d)*grad_bkp1[loc_jdof_bkp1][beta];
        }
      }
    }
  }
}
// ----------------------------------------------------------------------------
// dofs for a scalar-valued function
// ----------------------------------------------------------------------------
// note: as virtual and template members are not available,
//       the function "f" has been already evaluated on the hat_node[] set
// note2: moments on sides are scalar products with normals (not integrals on sides)
//        for RT0 on the triangle we have the basis psi associated with momentum as:
//          psi: matrix([x,y-1],[sqrt(2)*x,sqrt(2)*y],[x-1,y]);
//          n  : matrix([0,-1],[1/sqrt(2),1/sqrt(2)],[-1,0]);
//          xm : matrix([1/2,0],[1/2,1/2],[0,1/2]);
//          moment(i,f) := subst(xm[i][1],x,subst(xm[i][2],y,f.n[i]));
//
template<class T>
void
basis_fem_RTk<T>::_compute_dofs (
  reference_element                                 hat_K,
  const Eigen::Matrix<value_type,Eigen::Dynamic,1>& f_xnod,
        Eigen::Matrix<T,Eigen::Dynamic,1>&          dof) const
{
trace_macro ("_compute_dofs (hat_K="<<hat_K.name()<<")...");
  base::_initialize_data_guard (hat_K);
  size_type k = degree()-1;
  size_type d = hat_K.dimension();
  size_type loc_ndof = base::ndof (hat_K);

  dof.resize (loc_ndof);
  if (d == 0) return;

  // side dofs are Lagrange ones, associated to nodes
  size_type loc_inod = 0;
  size_type loc_idof = 0;
  for (size_type loc_isid = 0, loc_nsid = hat_K.n_subgeo(d-1); loc_isid < loc_nsid; ++loc_isid) {
    reference_element hat_S = hat_K.subgeo (d-1, loc_isid);
    point_basic<Float> hat_n; 
    Float hat_S_meas = hat_K.side_measure (loc_isid);
    hat_K.side_normal (loc_isid, hat_n);
    size_type loc_ndof_sid = base::_nnod_on_subgeo_internal [d][hat_S.variant()];
    for (size_type loc_idof_sid = 0; loc_idof_sid < loc_ndof_sid; ++loc_idof_sid) {
      dof[loc_idof] = hat_S_meas*dot(f_xnod[loc_inod],hat_n);
      loc_idof++;
      loc_inod++;
    }
  }
  // internal dofs
  if (k == 0) return; // no internal dofs when k==0
  const std::array<
          Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>
         ,3>&
   bkm1_node_internal_d = _bkm1_node_internal_d [hat_K.variant()];
  
  size_type loc_ndof_boundary = loc_idof;
  size_type loc_ndof_int_d    = d*bkm1_node_internal_d[0].cols();
  size_type first_loc_inod_int = loc_inod;
  check_macro (loc_ndof == loc_ndof_boundary + loc_ndof_int_d,
  	"invalid internal dof count: loc_ndof="<<loc_ndof
  	<< ", loc_ndof_boundary="<<loc_ndof_boundary
  	<< ", loc_ndof_int_d="<<loc_ndof_int_d);
  
  size_type variant = hat_K.variant();
  if (variant == reference_element::t) {
    // interpolate all vector components on a lattice of internal nodes
    for (size_type loc_idof_int = 0, loc_ndof_int = bkm1_node_internal_d[0].cols(); loc_idof_int < loc_ndof_int; ++loc_idof_int) {
      size_type loc_inod = first_loc_inod_int + loc_idof_int;
      for (size_type alpha = 0; alpha < d; ++alpha) {
        dof [loc_idof] = f_xnod [loc_inod][alpha];
        loc_idof++;
      }
    }
  } else { // variant == qTPH : quadrature
    for (size_type loc_idof_int = 0, loc_ndof_int = bkm1_node_internal_d[0].cols(); loc_idof_int < loc_ndof_int; ++loc_idof_int) {
      for (size_type alpha = 0; alpha < d; ++alpha) {
        loc_inod = first_loc_inod_int;
        T sum = 0;
        size_type inod_q = 0;
        for (typename quadrature<T>::const_iterator iter_q = _quad.begin(hat_K),
            last_q = _quad.end(hat_K); iter_q != last_q; iter_q++, inod_q++, ++loc_inod) {	
          sum += f_xnod [loc_inod][alpha]
                *bkm1_node_internal_d[alpha] (inod_q, loc_idof_int)
                *(*iter_q).w;
        }
        check_macro (loc_idof < loc_ndof, "invalid size");
        dof [loc_idof] = sum;
        loc_idof++;
      } 
    }
  }
  check_macro (loc_idof == loc_ndof, "invalid dof count");
trace_macro ("_compute_dofs (hat_K="<<hat_K.name()<<") done");
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template class basis_fem_RTk<T>;

_RHEOLEF_instanciation(Float)

}// namespace rheolef
