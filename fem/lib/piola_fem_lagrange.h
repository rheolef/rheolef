#ifndef _RHEOLEF_PIOLA_FEM_LAGRANGE_H
#define _RHEOLEF_PIOLA_FEM_LAGRANGE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/piola_fem.h"
namespace rheolef { 

/*Class:piola_fem
NAME: @code{piola_fem_lagrange} - maps a Lagrange finite element method
@cindex  piola transformation
@cindex  finite element method
SYNOPSIS:
  @noindent
  The @code{piola_fem_lagrange} defines how a finite element
  method of Lagrange type maps from a corresponding method defined on a reference element:
@iftex
@tex
  $$
      u(F(\widehat{\boldsymbol{x}})) = \widehat{u}(\widehat{\boldsymbol{x}})
  $$
  for all $\widehat{\boldsymbol{x}}$ in the reference element $\widehat{K}$
  and where $F$ denotes the Piola transformation that maps the
  the reference element $\widehat{K}$
  into the element $\widehat{K}$.
@end tex
@end iftex
@ifnottex
  @example
      u(F(hat_x)) = hat_u(hat_x)
  @end example
  for all hat_x in the reference element hat_K
  and where F denotes the Piola transformation that maps the
  the reference element hat_K
  into the element K.
@end ifnottex
  See also the
  @code{piola_fem} abstract class
  and the @code{basis} class for finite element methods definied on a
  reference element.
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
   | Pierre.Saramito@imag.fr
DATE: 26 january 2019
End:
*/
template<class T>
class piola_fem_lagrange: public piola_fem_rep<T> {
public:
  typedef piola_fem_rep<T>           base;
  typedef typename base::value_type  value_type;
  piola_fem_lagrange() : base() {}
  std::string name() const { return "Lagrange"; }

  bool transform_need_piola() const { return false; }

#define _RHEOLEF_transform(Value,GradValue)						\
  void transform         (const piola<T>& p, const Value& hat_u, Value&     u) const; 	\
  void inv_transform     (const piola<T>& p, const Value&     u, Value& hat_u) const; 	\
  void grad_transform    (								\
          const piola<T>&                  p,						\
          const Value&                     hat_u,					\
          const GradValue&                 hat_grad_u,					\
          const details::differentiate_option& gopt,					\
                GradValue&                 grad_u) const;				\

_RHEOLEF_transform(T,point_basic<T>)
_RHEOLEF_transform(point_basic<T>,tensor_basic<T>)
_RHEOLEF_transform(tensor_basic<T>,tensor3_basic<T>)
#undef _RHEOLEF_transform
};

}// namespace rheolef
#endif // _RHEOLEF_PIOLA_FEM_LAGRANGE_H
