#ifndef _RHEOLEF_EIGEN_UTIL_H
#define _RHEOLEF_EIGEN_UTIL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// convert a dense matrix to sparse one
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 september 2017
//
#include "rheolef/compiler_eigen.h"

namespace rheolef {
// -----------------------------------------------------
// eigen utilities
// -----------------------------------------------------
// convert to sparse
template <class T>
void
eigen_dense2sparse (
  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& a, 
        Eigen::SparseMatrix<T,Eigen::RowMajor>&         as)
{
  T a_max = 0;
  for (size_t i = 0; i < size_t(a.rows()); ++i) {
  for (size_t j = 0; j < size_t(a.cols()); ++j) {
    a_max = std::max (a_max, abs(a(i,j)));
  }}
  T eps = a_max*std::numeric_limits<T>::epsilon();
  as.resize (a.rows(), a.cols());
  as.setZero();
  for (size_t i = 0; i < size_t(a.rows()); ++i) {
  for (size_t j = 0; j < size_t(a.cols()); ++j) {
    if (abs(a(i,j)) > eps) {
       as.coeffRef (i,j) = a(i,j);
    }
  }}
}
template <class T>
T
cond (const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& a) {
  if (a.rows() == 0 || a.cols() == 0) return 0;
  Eigen::JacobiSVD<Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> > svd(a);
  return svd.singularValues()(0)
       / svd.singularValues()(svd.singularValues().size()-1);
}
template <class T, int Nrows, int Ncols>
bool
invert (const Eigen::Matrix<T,Nrows,Ncols>& a, Eigen::Matrix<T,Nrows,Ncols>& inv_a) {
  using namespace Eigen;
  FullPivLU <Matrix<T,Nrows,Ncols> > lu_a (a);
  if (! lu_a.isInvertible()) return false;
  Matrix<T,Nrows,Ncols> id = Matrix<T,Nrows,Ncols>::Identity (a.rows(),a.cols());
  inv_a = lu_a.solve (id);
  return true;
}
template <class T>
void
put_matrix_market (
  std::ostream& out,
  const Eigen::SparseMatrix<T,Eigen::RowMajor>& a)
{
  out << "%%MatrixMarket matrix coordinate real general" << std::endl
      << a.rows() << " " << a.cols() << " " << a.nonZeros() << std::endl;
  for (size_t i = 0; i < size_t(a.rows()); ++i) {
    for (typename Eigen::SparseMatrix<T,Eigen::RowMajor>::InnerIterator p(a,i); p; ++p) {
      out << i+1 << " " << p.index()+1 << " " << p.value() << std::endl;
    }
  }
}
template <class T>
void
put_matrix_market (
  std::ostream& out,
  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& a)
{
  out << "%%MatrixMarket matrix coordinate real general" << std::endl
      << a.rows() << " " << a.cols() << " " << a.rows()*a.cols() << std::endl;
  for (size_t i = 0; i < size_t(a.rows()); ++i) {
  for (size_t j = 0; j < size_t(a.cols()); ++j) {
    out << i+1 << " " << j+1 << " " << a(i,j) << std::endl;
  }}
}

}// namespace rheolef
#endif // _RHEOLEF_EIGEN_UTIL_H
