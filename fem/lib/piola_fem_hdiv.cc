///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// Piola transformation for H(div,omega) approximations
//
// author: Pierre.Saramito@imag.fr
//
// date: 9 may 2019
//
// references:
// [1] P.-A. Raviart and J.-M. Thomas.
//     A mixed finite element method for 2-nd order elliptic problems.
//     in "Mathematical aspects of finite element methods",
//     pages 292--315, Springer, 1977.
// [2] J. E. Roberts and J.-M. Thomas,
//     Mixed and hybrid methods,
//     In "Handbook of numerical analysis. Volume 2.
//         Finite element methods (part 1)",
//     chapter 4, Elsevier, 1991.
//     see page 554
//
#include "piola_fem_hdiv.h"
#include "piola_fem_grad_post.icc"
namespace rheolef {

template<class T>
void
piola_fem_hdiv<T>::transform (
  const piola<T>&       p,
  const point_basic<T>& hat_u_in,
        point_basic<T>&     u) const
{
  point_basic<T> hat_u = hat_u_in; // avoid the aliasing problem (u,hat_u)
  u = (1/p.detDF)*(p.DF*hat_u); // TODO: DVT_OPTIM_2D
}
template<class T>
void
piola_fem_hdiv<T>::inv_transform (
  const piola<T>&       p,
  const point_basic<T>&     u_in,
        point_basic<T>& hat_u) const
{
  point_basic<T> u = u_in; // avoid the aliasing problem (u,hat_u)
  hat_u = p.detDF*(p.invDF*u); // TODO: DVT_OPTIM_2D
}
template<class T>
void
piola_fem_hdiv<T>::grad_transform (
  const piola<T>&                           p,
  const point_basic<T>&                 hat_u_in,
  const tensor_basic<T>&           hat_grad_u_in,
  const details::differentiate_option&   gopt,
        tensor_basic<T>&               grad_u) const
{
  point_basic<T>       hat_u =      hat_u_in; // avoid the aliasing problem (u,hat_u)
  tensor_basic<T> hat_grad_u = hat_grad_u_in;
  grad_u = (1/p.detDF)*p.DF*hat_grad_u*p.invDF; // TODO: DVT_OPTIM_2D
  point_basic<T> u;
  transform (p, hat_u, u); // TODO: used only in the axi case: could be optimized
  grad_post (p, gopt, u, grad_u);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                               \
template class piola_fem_hdiv<T>;						\

_RHEOLEF_instanciation(Float)

}// namespace rheolef
