#ifndef _RHEOLEF_BASIS_FEM_VECTOR_H
#define _RHEOLEF_BASIS_FEM_VECTOR_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*Class:sherwin
NAME: @code{vector(X)} - vector-valued basis
@cindex  polynomial basis
@clindex space
@clindex basis
@clindex reference_element
SYNOPSIS:
  space Mh (omega,"P1","vector");
DESCRIPTION:
  @noindent
  This polynomial @code{basis} is used for vector-valued basis
  when all components are approcimated by the same scalar-valued basis.
AUTHOR: Pierre.Saramito@imag.fr
DATE:   25 january 2019
End:
*/
#include "rheolef/basis.h"
namespace rheolef {

template<class T>
class basis_fem_vector: public basis_rep<T> {
public:

// typedefs:

  typedef basis_rep<T>                 base;
  typedef typename base::size_type     size_type;
  typedef point_basic<T>               value_type;
  typedef space_constant::valued_type  valued_type;

// allocators:

  basis_fem_vector (const basis_basic<T>& scalar_basis, const basis_option& sopt);
  ~basis_fem_vector();

// accessors:

  std::string family_name()  const { return _scalar_basis.family_name(); }
  size_type   family_index() const { return _scalar_basis.family_index(); }
  size_type degree() const { return _scalar_basis.degree(); }
  valued_type valued_tag() const { return space_constant::vector; }
  bool is_hierarchical() const { return true; }
  size_type size() const { return _n_comp; }
  const basis_basic<T>& operator[] (size_type i_comp) const { return _scalar_basis; }
  bool is_continuous() const    { return _scalar_basis.is_continuous(); }
  bool have_index_parameter()   const { return _scalar_basis.have_index_parameter(); }
  bool have_continuous_feature() const { return _scalar_basis.have_continuous_feature(); }
  bool is_nodal() const    { return _scalar_basis.is_nodal(); }
  bool have_compact_support_inside_element() const { return _scalar_basis.have_compact_support_inside_element(); }

  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
  hat_node (reference_element hat_K) const;

// evaluation of all basis functions at hat_x:

  void evaluate (
    reference_element                               hat_K,
    const point_basic<T>&                           hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const;

  void grad_evaluate (
    reference_element                                hat_K,
    const point_basic<T>&                            hat_x,
    Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,1>& value) const;

// internals:

  void _initialize_cstor_sizes() const;
  void _initialize_data (reference_element hat_K) const;

// dofs for a vector-valued function
  void _compute_dofs (
    reference_element                                     hat_K,
    const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& f_xnod,
          Eigen::Matrix<T,Eigen::Dynamic,1>&              dof) const;

protected:
// data:
  size_type                                               _n_comp;
  basis_basic<T>                                          _scalar_basis;

// working area:
  mutable Eigen::Matrix<T,Eigen::Dynamic,1>               _scalar_value;
  mutable Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>  _vector_value;
};

} // namespace rheolef
#endif // _RHEOLEF_BASIS_FEM_VECTOR_H
