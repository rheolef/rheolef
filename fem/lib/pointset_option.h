#ifndef _RHEOLEF_POINTSET_OPTION_H
#define _RHEOLEF_POINTSET_OPTION_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
/*Class:pointset_option
NAME: @code{poinset_option} - options for a set of points in the reference element
@clindex pointset_option
@clindex pointset
@cindex  reference element
@clindex reference_element
SYNOPSIS:
  @noindent
  The @code{pointset_option} class defines options for
  a set of points in the reference element.
  See @ref{reference_element iclass} and @ref{pointset iclass}.
OPTIONS:
@table @code
  @item interior
    	The optional argument interior specifies how many points from
        the boundary to omit.
        For example, on an edge reference element with @code{n=2} and @code{interior=0},
        this function will return the vertices and midpoint,
        but with @code{interior=1}, it will only return the midpoint.
@end table

AUTHOR: Pierre.Saramito@imag.fr
DATE:   7 may 2019
End:
*/
#include "rheolef/reference_element.h"

namespace rheolef {

//<verbatim:
struct pointset_option {
  size_t interior; // how many points from the boundary to omit
  pointset_option() :
    interior()
  {}
};
//>verbatim:

}// namespace rheolef
#endif // _RHEOLEF_POINTSET_OPTION_H
