## process this file with automake to produce Makefile.in
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
include ../../config/am_options.mk
include ${top_builddir}/config/config.mk

docdir = $(prefix)/share/doc/@doc_dir@
exampledir = $(docdir)/examples

# -----------------------------------------------------------------------------
# documentation: refman
# -----------------------------------------------------------------------------

DOC_1_LIST =							\
	basis


DOC_2_LIST =							\
	basis							

# TODO_DOX: all manN here
DOC_TODO_LIST = 						\
	basis_option.7						\
	ad3.7							\
	pointset.7						\
	pointset_option.7					\
	basis_on_pointset.7					\
	lagrange.7						\
	bernstein.7						\
	sherwin.7						\
	raviart-thomas.7					

if HAVE_DOX_DOCUMENTATION
man_MANS = 							\
	$(DOC_1_LIST:=.1rheolef)				\
	$(DOC_2_LIST:=.2rheolef)

DOC_DOX  = 							\
	$(DOC_1_LIST:=.man1)					\
	$(DOC_2_LIST:=.man2)

else
man_MANS =
DOC_DOX  =
endif

MAN_LIST =                      \
        basis.1      		\
        basis.7		      	\
        basis_option.7   	\
        basis_fem_Pk_lagrange.7 \
        basis_fem_Pk_bernstein.7 \
        basis_fem_Pk_sherwin.7 	\
        basis_fem_RTk.7    	\
        piola_fem.7	    	\
        piola_fem_lagrange.7   	\
        piola_fem_hdiv.7   	\
        ad3.7    

# -----------------------------------------------------------------------------
# the commands
# -----------------------------------------------------------------------------

bin_PROGRAMS  = 						\
	basis							

basis_SOURCES  = basis.cc
basis_LDADD    = libbasis.la $(LDADD)

# -----------------------------------------------------------------------------
# basis by symbolic computation (low fixed order)
# -----------------------------------------------------------------------------
# Pk.cc         <-- Pk_symbolic.cc for k=0,1,2
# bubble.cc     <-- bubble_symbolic.cc
# P1qd.cc       <-- P1qd_symbolic.cc

SYMB_BASIS_LIST  = P0 P1 bubble P1qd

basis_list.cc: Makefile.am make_basis_list_cxx.sh
	sh $(srcdir)/make_basis_list_cxx.sh ${SYMB_BASIS_LIST} > basis_list.cc.new
	@x="basis_list.cc";				\
	if test ! -f $${x}; then			\
  	  mv $${x}.new $${x};				\
  	  echo "! file $${x} created." 1>&2;		\
	elif diff $${x} $${x}.new >/dev/null; then	\
  	  echo "! file $${x} unchanged." 1>&2;		\
  	  rm -f $${x}.new;				\
	else						\
  	  mv $${x}.new $${x};				\
  	  echo "! file $${x} modified." 1>&2;		\
	fi

if HAVE_GINAC
noinst_PROGRAMS = ${SYMB_BASIS_LIST:=_symbolic}
BASIS_LIST_GENERATED_SRC = 					\
	$(SYMB_BASIS_LIST:=.h)					\
	$(SYMB_BASIS_LIST:=.cc)
else
noinst_PROGRAMS =
BASIS_LIST_GENERATED_SRC =
endif

if HAVE_GINAC
P0_symbolic_SOURCES = P0_symbolic.cc
P0_symbolic_LDADD   = libbasis_symbolic.la $(LDADD_GINAC) $(LDADD)

P1_symbolic_SOURCES = P1_symbolic.cc
P1_symbolic_LDADD   = libbasis_symbolic.la $(LDADD_GINAC) $(LDADD)

bubble_symbolic_SOURCES = bubble_symbolic.cc
bubble_symbolic_LDADD   = libbasis_symbolic.la $(LDADD_GINAC) $(LDADD)

P1qd_symbolic_SOURCES    = P1qd_symbolic.cc
P1qd_symbolic_LDADD      = libbasis_symbolic.la $(LDADD_GINAC) $(LDADD)

$(srcdir)/P0.h: P0_symbolic
	./P0_symbolic -h  > $@
$(srcdir)/P0.cc: P0_symbolic
	./P0_symbolic -cc > $@
$(srcdir)/P1.h: P1_symbolic
	./P1_symbolic -h  > $@
$(srcdir)/P1.cc: P1_symbolic
	./P1_symbolic -cc > $@
$(srcdir)/bubble.h: bubble_symbolic
	./bubble_symbolic -h  > $@
$(srcdir)/bubble.cc: bubble_symbolic
	./bubble_symbolic -cc > $@
$(srcdir)/P1qd.h: P1qd_symbolic
	./P1qd_symbolic -h  > $@
$(srcdir)/P1qd.cc: P1qd_symbolic
	./P1qd_symbolic -cc > $@
endif

# -----------------------------------------------------------------------------
# the libraries
# -----------------------------------------------------------------------------
#
# libbasis : Pk, Sk, Bk directly by c++
# libbasis_symbolic : bubble, P1qd

if HAVE_GINAC
noinst_LTLIBRARIES = libbasis.la libbasis_symbolic.la
else
noinst_LTLIBRARIES = libbasis.la
endif

pkginclude_HEADERS =			\
	pointset_option.h		\
	pointset.h			\
	basis_option.h			\
	basis_get.h			\
	basis_raw.h			\
	basis.h				\
	basis_on_pointset.h		\
	ad3.h				\
	eigen_util.h			\
	memorized_value.h		\
	basis_fem_Pk_lagrange.h		\
	basis_fem_RTk.h			\
	basis_fem_trace_n.h		\
	basis_fem_vector.h		\
	basis_fem_tensor.h		\
	basis_fem_empty.h		\
	piola.h				\
	piola_fem.h			

CXX_INC = 				\
	reference_element_aux.icc	\
	equispaced.icc			\
	warburton.icc			\
	fekete.icc			\
	basis_ordering.icc		\
	monomial.icc			\
	bernstein.icc			\
	dubiner.icc			\
	sherwin.icc			\
	Pk_get_local_idof_on_side.icc	\
	basis_on_pointset_evaluate.icc	\
	basis_visu_gnuplot.icc		\
	interpolate_RTk_polynom.icc

noinst_HEADERS =			\
	basis_raw_monomial.h		\
	basis_raw_bernstein.h		\
	basis_raw_dubiner.h		\
	basis_fem_Pk_bernstein.h	\
	basis_fem_Pk_sherwin.h		\
	piola_fem_lagrange.h		\
	piola_fem_hdiv.h		\
	${CXX_INC}			\
	basis_symbolic.h		\
	piola_fem_grad_post.icc

libbasis_la_SOURCES =                  	\
	basis_option.cc			\
	set_simplex_leb_gll.cc		\
	basis_raw.cc			\
	basis_raw_visu_gnuplot.cc	\
	basis_raw_monomial.cc		\
	basis_raw_bernstein.cc		\
	basis_raw_dubiner.cc		\
	basis_rep.cc			\
	basis_get.cc			\
	basis_list.cc			\
	basis_fem_visu_gnuplot.cc	\
	basis_fem_Pk_lagrange.cc	\
	basis_fem_Pk_bernstein.cc	\
	basis_fem_Pk_sherwin.cc		\
	basis_fem_RTk.cc		\
	basis_fem_trace_n.cc		\
	basis_fem_vector.cc		\
	basis_fem_tensor.cc		\
	basis_fem_empty.cc		\
	$(SYMB_BASIS_LIST:=.cc)		\
	basis_raw_list.cc		\
	basis_on_pointset.cc		\
	piola_fem_lagrange.cc		\
	piola_fem_hdiv.cc	

if HAVE_GINAC
libbasis_symbolic_la_SOURCES =          \
	basis_symbolic.cc		\
	basis_symbolic_cxx.cc
endif

# -----------------------------------------------------------------------------
# extra rules bison and flex
# -----------------------------------------------------------------------------

BISON_SRC = 							\
        basis_yacc.y

FLEX_SRC = 							\
        basis_lex.flex 

$(srcdir)/basis_lex.cc: $(srcdir)/basis_lex.flex
	$(FLEX) -Pbasis_ -+ -t $< \
	    | ${SHELL} ${top_srcdir}/config/flexfix.sh $(FLEXLEXER_H) > $@

$(srcdir)/basis_yacc.cc: $(srcdir)/basis_yacc.y
	$(BISON) --name-prefix="basis_" -v $< -o $@

basis_get.lo basis_get.o: \
	$(srcdir)/basis_get.cc \
	$(srcdir)/basis_lex.cc  \
	$(srcdir)/basis_yacc.cc

BISON_GENERATED_SRC =                   \
	$(BISON_SRC:.y=.cc)             \
	$(FLEX_SRC:.flex=.cc)


EXTRA_DIST =                    \
	Makefile.am 		\
        $(MAIN_CXX)		\
        $(CXX_INC)		\
	$(LIB_CXX)		\
	$(BISON_SRC)		\
	$(FLEX_SRC)		\
	$(BISON_GENERATED_SRC)	\
	$(SYMB_BASIS_LIST:=.h)		\
	$(SYMB_BASIS_LIST:=.cc)		\
	make_basis_list_cxx.sh

BUILT_SOURCES =                                                 \
	stamp-symlink.in					\
	$(BASIS_LIST_GENERATED_SRC)				\
	$(BISON_GENERATED_SRC)					\
	basis_list.cc

CLEANFILES = *.ii *.ii_pic *.da *.gcov *.[oa1359] 		\
	*.[1-9]rheolef *.man[1-9] 				\
	cxx_repository/* KCC_files/* KCC_pic/* 			\
	.deps/*.Q .deps/*.R 					\
	stamp-symlink.in version.tex ${DOC_DOX}			\
	${man_MANS} ${noinst_MANS} $(BUILT_SOURCES)

# now added to git, for simplicity : otherwise, requires ginac to be installed
OLD_CVSIGNORE = 		\
	$(SYMB_BASIS_LIST:=.h)	\
	$(SYMB_BASIS_LIST:=.cc) 

CVSIGNORE = 			\
	Makefile.in		\
	stamp-symlink.in	\
	basis_list.cc		\
	$(BISON_GENERATED_SRC) 	

WCIGNORE = 			\
	$(VALID)

LICENSEIGNORE = 		\
	$(VALID)

# -----------------------------------------------------------------------------
# compilation rules
# -----------------------------------------------------------------------------

AM_CPPFLAGS = 				\
	-I${top_builddir}/include 	\
	$(INCLUDES_BOOST_MPI) 		\
	$(INCLUDES_CGAL) 		\
	$(INCLUDES_MPI) 		\
	$(INCLUDES_FLOAT)

LDADD    =  				\
	../geo_element/libgeo_element.la	\
	../../util/lib/libutil.la	\
	$(LDADD_BOOST_MPI)		\
	$(LDADD_MPI) 			\
	$(LDADD_FLOAT)

OLD_LADD = \
	$(LDADD_CGAL) 			\
	$(LDADD_SOLVER) 		\
	$(LDADD_PARTITIONNER) 		

lib:
	cd ../../config; ${MAKE}
	cd ../../util/lib; ${MAKE}
	cd ../quadrature; ${MAKE}
	cd ../geo_element; ${MAKE}
	${MAKE} all

symlink: $(pkginclude_HEADERS)
	$(MKSYMLINK) $(pkginclude_HEADERS)

stamp-symlink.in: Makefile.am $(pkginclude_HEADERS)
	@$(MKSYMLINK) $(pkginclude_HEADERS)
	@touch stamp-symlink.in

# ----------------------------------------------------------------------------
# refman rules
# ----------------------------------------------------------------------------
all-local: ${man_MANS} ${DOC_DOX}
if HAVE_TEX_DOCUMENTATION
dvi-local: all-local $(PAPER_TEX:.tex=.pdf)
else
dvi-local: all-local
endif

lagrange.7rheolef: basis_fem_Pk_lagrange.h
	cat $(srcdir)/basis_fem_Pk_lagrange.h | ${SRC2MAN} -name point -section 7rheolef > lagrange.7rheolef
lagrange.7texi: basis_fem_Pk_lagrange.h
	cat $(srcdir)/basis_fem_Pk_lagrange.h | ${SRC2TEXI} -file ${DIR_PREFIX}basis_fem_Pk_lagrange.h -node "$* iclass,,, Internal classes" > lagrange.7texi

bernstein.7rheolef: basis_fem_Pk_bernstein.h
	cat $(srcdir)/basis_fem_Pk_bernstein.h | ${SRC2MAN} -name point -section 7rheolef > bernstein.7rheolef
bernstein.7texi: basis_fem_Pk_bernstein.h
	cat $(srcdir)/basis_fem_Pk_bernstein.h | ${SRC2TEXI} -file ${DIR_PREFIX}basis_fem_Pk_bernstein.h -node "$* iclass,,, Internal classes" > bernstein.7texi

sherwin.7rheolef: basis_fem_Pk_sherwin.h
	cat $(srcdir)/basis_fem_Pk_sherwin.h | ${SRC2MAN} -name point -section 7rheolef > sherwin.7rheolef
sherwin.7texi: basis_fem_Pk_sherwin.h
	cat $(srcdir)/basis_fem_Pk_sherwin.h | ${SRC2TEXI} -file ${DIR_PREFIX}basis_fem_Pk_sherwin.h -node "$* iclass,,, Internal classes" > sherwin.7texi

raviart-thomas.7rheolef: basis_fem_RTk.h
	cat $(srcdir)/basis_fem_RTk.h | ${SRC2MAN} -name point -section 7rheolef > raviart-thomas.7rheolef
raviart-thomas.7texi: basis_fem_RTk.h
	cat $(srcdir)/basis_fem_RTk.h | ${SRC2TEXI} -file ${DIR_PREFIX}basis_fem_RTk.h -node "$* iclass,,, Internal classes" > raviart-thomas.7texi

maintainer-clean-local:
	cd ${srcdir}; rm -f ${BISON_GENERATED_SRC}

