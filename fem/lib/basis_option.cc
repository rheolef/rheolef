///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 september 2017
//
#include "basis_option.h"

namespace rheolef {
using namespace std;

static const char* static_node_name[basis_option::max_node+1] = {
	"equispaced"	,
	"warburton"	,
	"fekete"	,
	"undefined"
};
static const char* static_raw_polynomial_name[basis_option::max_raw_polynomial+1] = {
	"monomial"	,
	"bernstein"	,
	"dubiner"	,
	"undefined"
};
static
basis_option::node_type
node_name2type (string name)
{
  for (size_t i =  0; i < basis_option::max_node; ++i) {
    if (static_node_name[i] == name) {
      return (basis_option::node_type) (i);
    }
  }
  error_macro ("unexpected space node name `" << name << "'");
  return basis_option::max_node;
}
static
basis_option::raw_polynomial_type
raw_polynomial_name2type (string name)
{
  for (size_t i =  0; i < basis_option::max_raw_polynomial; ++i) {
    if (static_raw_polynomial_name[i] == name) {
      return (basis_option::raw_polynomial_type) (i);
    }
  }
  error_macro ("unexpected raw polynomial name `" << name << "'");
  return basis_option::max_raw_polynomial;
}
bool
basis_option::is_node_name (std::string name) const
{
  for (size_t i =  0; i < basis_option::max_node; ++i) 
    if (static_node_name[i] == name) return true;
  return false;
}
bool
basis_option::is_raw_polynomial_name (std::string name) const
{
  for (size_t i =  0; i < basis_option::max_raw_polynomial; ++i) 
    if (static_raw_polynomial_name[i] == name) return true;
  return false;
}
bool
basis_option::is_option_name (std::string name) const
{
  return is_node_name(name) ||
         is_raw_polynomial_name(name) ||
         name == "trace_n";
}
void
basis_option::set_node (string name)
{
  set_node (node_name2type (name));
}
void
basis_option::set_raw_polynomial (string name)
{
  set_raw_polynomial (raw_polynomial_name2type (name));
}
string
basis_option::get_node_name() const
{
  check_macro (_node >= 0 && _node <= max_node,
	"unexpected space node number = " << _node);
  return static_node_name[_node];
}
string
basis_option::get_raw_polynomial_name() const
{
  check_macro (_poly >= 0 && _poly <= max_raw_polynomial,
	"unexpected space raw polynomial number = " << _poly);
  return static_raw_polynomial_name[_poly];
}
void
basis_option::set (std::string option_name)
{
  if (is_node_name (option_name)) { 
    set_node (option_name);
  } else if (is_raw_polynomial_name (option_name)) { 
    set_raw_polynomial (option_name);
  } else if (option_name == "trace_n") { 
    set_trace_n();
  } else {
    error_macro ("unexpected option name `" << option_name << "'");
  }
}
string
basis_option::stamp() const
{
  if (_node == default_node && _poly == default_raw_polynomial) {
    return "";
  }
  string opt_trace = ""; // trace_n : arround as "trace(Pkd)"
  if (_poly == default_raw_polynomial) {
    return "[" + opt_trace + get_node_name() + "]";
  }
  if (_node == default_node) {
    return "[" + opt_trace + get_raw_polynomial_name() + "]";
  }
  return   "[" + opt_trace + get_node_name() + "," + get_raw_polynomial_name() + "]";
}

} // namespace rheolef
