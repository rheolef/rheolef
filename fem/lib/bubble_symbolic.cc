///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// bubble approximation
//
#include "basis_symbolic.h"
using namespace rheolef;
using namespace std;
using namespace GiNaC;

class bubble_symbolic : public basis_symbolic_nodal
{
public:
    bubble_symbolic ();
};
bubble_symbolic::bubble_symbolic ()
: basis_symbolic_nodal("bubble",4)
{
  on('p') << node(0) 
	  << poly (1)
	  << end;
  on('e') << node (0.5)
	  << poly (4*x*(1-x))
	  << end;
  on('t') << node (ex(1)/3, ex(1)/3)
          << poly (27*x*y*(1-x-y))
	  << end;
  on('T') << node (1./4, 1./4, 1./4)
          << poly (256*x*y*z*(1-x-y-z))
          << end;
}
int main (int argc, char **argv) {
	bubble_symbolic bubble;
	bubble.put_cxx_main (argc,argv);
}
