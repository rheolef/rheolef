#ifndef _RHEOLEF_EQUISPACED_ICC
#define _RHEOLEF_EQUISPACED_ICC
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute the equispaced point set on the reference element
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 september 2017
//
#include "rheolef/reference_element.h"
#include "reference_element_aux.icc"
#include "rheolef/compiler_eigen.h"

namespace rheolef {

// The optional argument "interior" specifies
// how many points from the boundary to omit.
// For example, on an "edge" reference element with order=2 and interior=0,
// this function will return the vertices and midpoint,
// but with interior=1, it will only return the midpoint.

template<class T>
void
pointset_lagrange_equispaced (
  reference_element                                hat_K,
  size_t                                           order_in,
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&  hat_xnod,
  size_t                                           internal = 0)
{
trace_macro("hat_K="<<hat_K.name()<<": order_in="<<order_in<<", internal="<<internal);
  typedef reference_element::size_type  size_type;
  typedef point_basic<size_type>        ilat;
  if (order_in == 0) {
    hat_xnod.resize(1);
    reference_element_barycenter (hat_K, hat_xnod[0]);
trace_macro("barycenter: hat_xnod [0]="<<hat_xnod[0]);
    return;
  }
  size_t d = hat_K.dimension();
  // when only internal nodes: compute the effective "order" and the lattice size
  size_t order = 0, size = 0;
  switch (hat_K.variant()) {
    case reference_element::p:
      order = order_in;
      size  = reference_element::n_node(hat_K.variant(), order);
      break;
    case reference_element::e:
    case reference_element::t:
    case reference_element::T:
      order = order_in + (d+1)*internal;
      size  = (order >= (d+1)*internal) ? reference_element::n_node(hat_K.variant(), order-(d+1)*internal) : 0;
      break;
    case reference_element::q:
    case reference_element::H:
      order = order_in + 2*internal;
      size = (order >= 2*internal) ? reference_element::n_node(hat_K.variant(), order-2*internal) : 0;
      break;
    case reference_element::P:
      check_macro (internal == 0, "internal: not yet");
      order = order_in;
      size = reference_element::n_node(hat_K.variant(), order);
      break;
    default:
      error_macro ("unexpected element type `"<<hat_K.name()<<"'");
  }
  size_t first = internal;
  size_t last  = (order >= internal) ? order-internal : 0;
trace_macro("hat_K="<<hat_K.name()<<": order="<<order<<", first="<<first<<", last="<<last<<", size="<<size);
  hat_xnod.resize (size);
  switch (hat_K.variant()) {
    case reference_element::p: {
      hat_xnod.resize (1);
      hat_xnod [0] = point_basic<T> (T(0));
      break;
    } 
    case reference_element::e: {
      for (size_type i = first; i <= last; i++) {
        size_type loc_idof = reference_element_e::ilat2loc_inod (order-(d+1)*internal, ilat(i-internal));
        hat_xnod [loc_idof] = point_basic<T> ((T(int(i)))/T(int(order)));
trace_macro("e::lattice("<<i<<"): hat_xnod ["<<loc_idof<<"]="<<hat_xnod [loc_idof]);
      }
      break;
    } 
    case reference_element::t: {
      for (size_type j = first; j   <= last; j++) { 
      for (size_type i = first; i+j <= last; i++) { 
        size_type loc_idof = reference_element_t::ilat2loc_inod (order-(d+1)*internal, ilat(i-internal,j-internal));
        hat_xnod [loc_idof] = point_basic<T> ((T(int(i)))/T(int(order)), T(int(j))/T(int(order)));
trace_macro("t::lattice("<<i<<","<<j<<"): hat_xnod ["<<loc_idof<<"]="<<hat_xnod [loc_idof]);
      }}
      break;
    } 
    case reference_element::q: {
      for (size_type j = first; j <= last; j++) { 
      for (size_type i = first; i <= last; i++) { 
        size_type loc_idof = reference_element_q::ilat2loc_inod (order-2*internal, ilat(i-internal,j-internal));
        hat_xnod [loc_idof] = point_basic<T> (-1+2*(T(int(i)))/T(int(order)), -1+2*T(int(j))/T(int(order)));
trace_macro("q::lattice("<<i<<","<<j<<"): hat_xnod ["<<loc_idof<<"]="<<hat_xnod [loc_idof]);
      }}
      break;
    } 
    case reference_element::T: {
      check_macro (internal == 0, "internal: not yet");
      for (size_type k = first; k     <= last; k++) {
      for (size_type j = first; j+k   <= last; j++) {
      for (size_type i = first; i+j+k <= last; i++) { 
        size_type loc_idof = reference_element_T::ilat2loc_inod (order, ilat(i,j,k));
        hat_xnod [loc_idof] = point_basic<T> ((T(int(i)))/T(int(order)), T(int(j))/T(int(order)), T(int(k))/T(int(order)));
      }}}
      break;
    } 
    case reference_element::P: {
      check_macro (internal == 0, "internal: not yet");
      for (size_type k = first; k   <= last; k++) { 
      for (size_type j = first; j   <= last; j++) { 
      for (size_type i = first; i+j <= last; i++) { 
        size_type loc_idof = reference_element_P::ilat2loc_inod (order, ilat(i,j,k));
        hat_xnod [loc_idof] = point_basic<T> ((T(int(i)))/T(int(order)), T(int(j))/T(int(order)), -1+2*T(int(k))/T(int(order)));
      }}}
      break;
    } 
    case reference_element::H: {
      check_macro (internal == 0, "internal: not yet");
      for (size_type k = first; k <= last; k++) { 
      for (size_type j = first; j <= last; j++) { 
      for (size_type i = first; i <= last; i++) { 
        size_type loc_idof = reference_element_H::ilat2loc_inod (order, ilat(i,j,k));
        hat_xnod [loc_idof] = point_basic<T> (-1+2*(T(int(i)))/T(int(order)), -1+2*T(int(j))/T(int(order)), -1+2*T(int(k))/T(int(order)));
      }}}
      break;
    } 
    default: error_macro ("unexpected element type `"<<hat_K.name()<<"'");
  }
}

} // namespace rheolef
#endif // _RHEOLEF_EQUISPACED_ICC
