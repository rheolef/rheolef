#ifndef _RHEOLEF_BASIS_FEM_PK_SHERWIN_H
#define _RHEOLEF_BASIS_FEM_PK_SHERWIN_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*Class:sherwin
NAME: @code{Sk} - Dubiner-Sherwin-Karniadakis polynomial basis
@cindex  polynomial basis
@clindex space
@clindex basis
@clindex reference_element
SYNOPSIS:
  space Vh(omega,"S5");
DESCRIPTION:
  @noindent
  This @code{basis} is described for the triangle 
  by Dubiner (J. Sci. Comput., 1991)
  and extended by Sherwin and Karniadakis (2005, Cambridge Univ. Press)
  to others reference elements.
  It is indicated in the @code{space} (see @ref{space class})
  by a string starting with
  the letter @code{"S"},
  followed by digits indicating the polynomial order.

OPTIONS:
  This basis recognizes the equispaced/warburton node option
  for degrees of freedom located on sides.
  See @ref{basis_option class}.

AUTHOR: Pierre.Saramito@imag.fr
DATE:   23 september 2017
End:
*/
#include "rheolef/basis.h"
#include "rheolef/ad3.h"
namespace rheolef {

template<class T>
class basis_fem_Pk_sherwin: public basis_rep<T> {
public:

// typedefs:

  typedef basis_rep<T>                 base;
  typedef reference_element::size_type size_type;
  typedef T                            value_type;

// allocators:

  basis_fem_Pk_sherwin (size_type degree, const basis_option& sopt);
  ~basis_fem_Pk_sherwin();

// accessors:

  std::string family_name() const { return "S"; }
  size_type degree() const { return _degree; }
  bool is_nodal() const { return false; }

// evaluation of all basis functions at hat_x:

  void evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const;

// evaluate the gradient:

  void grad_evaluate (
    reference_element                               hat_K,
    const point_basic<T>&                           hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const;

  void _initialize_cstor_sizes() const;
  void _initialize_data (reference_element hat_K) const;
  void _compute_dofs (
    reference_element     hat_K, 
    const Eigen::Matrix<T,Eigen::Dynamic,1>&   f_xnod, // scalar-valued case
          Eigen::Matrix<T,Eigen::Dynamic,1>&   dof) const;

protected:
// data:
  size_type                                      _degree;
  T                                              _alpha; 
  T                                              _beta;

#ifdef TODO
  mutable std::array<std::vector<ad3_basic<T> >,
             reference_element::max_variant>     _value_ad;

  mutable std::vector<ad3_basic<T> >             _work0_ad, _work1_ad, _work2_ad;

  mutable Eigen::Matrix<T,Eigen::Dynamic,1>      _work0, _work1, _work2;
#endif // TODO
};

} // namespace rheolef
#endif // _RHEOLEF_BASIS_FEM_PK_SHERWIN_H
