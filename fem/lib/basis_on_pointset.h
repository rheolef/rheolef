#ifndef _RHEO_BASIS_ON_POINTSET_V2_H
#define _RHEO_BASIS_ON_POINTSET_V2_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

/*Class:basis_on_pointset
NAME: @code{basis_on_pointset} - pre-evaluated polynomial basis
@cindex  polynomial basis
@clindex basis
@cindex  reference element
@clindex reference_element
SYNOPSIS:
  @noindent
  The @code{basis_on_pointset} class is able to memorize the evaluation
  of a polynomial basis and its derivatives on a set of point of the reference element
  (@pxref{reference_element iclass}).
  The basis is described by the @code{basis} class (@pxref{basis class}).
  The set of points could be
  either quadrature nodes on the reference element (@pxref{quadrature iclass})
  or Lagrange nodes associated to another basis.
  For application of an integration of on a side, the set of nodes could 
  be defined on a specific side only.
  In all these cases, the evaluation of polynomials could be performed
  one time for all on the reference element and its result stored and reused
  for all elements of the mesh: the speedup is important, espcially for
  high order polynomials. 

AUTHOR: Pierre.Saramito@imag.fr
DATE:   4 january 2018
End:
*/

#include "rheolef/basis.h"
#include "rheolef/quadrature.h"
namespace rheolef {

// -----------------------------------------------------------------------
// basis evaluated on lattice of quadrature formulae
// -----------------------------------------------------------------------
template<class T>
class basis_on_pointset_rep {
public:
// typedefs:

    typedef typename std::vector<T>::size_type  size_type;

    typedef enum {
      quad_mode     = 0,
      nodal_mode    = 1,
      max_mode      = 2
    } mode_type;

// allocators:

    ~basis_on_pointset_rep();
    basis_on_pointset_rep (const std::string& name = "");
    basis_on_pointset_rep (const basis_on_pointset_rep<T>&);
    basis_on_pointset_rep<T>& operator= (const basis_on_pointset_rep<T>&);

// modifiers:

    void reset (const std::string& name);

// accessors:

    std::string name() const;
    bool is_set() const { return _mode != max_mode; }
    size_type ndof (reference_element hat_K) const;
    size_type nnod (reference_element hat_K) const;
    const basis_basic<T>& get_basis() const { return _b; }
    bool has_quadrature() const { return _mode == quad_mode; }
    const quadrature<T>& get_quadrature() const;
    const basis_basic<T>& get_nodal_basis() const;

    template<class Value>
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
    evaluate (reference_element hat_K) const;

    template<class Value>
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
    evaluate_on_side (   
      reference_element		   tilde_L,
      const side_information_type& sid) const;

    template<class Value>
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
    grad_evaluate (reference_element hat_K) const;

    template<class Value>
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
    grad_evaluate_on_side (
      reference_element		                              tilde_L,
      const side_information_type&		              sid) const;

// internal:
    static basis_on_pointset_rep<T>* make_ptr (const std::string& name);
    static std::string _make_name(
  	mode_type          mode,
  	const std::string& basis_name,
  	const std::string& pointset_name);
    static mode_type _parse_name (	
	const std::string& name,
              std::string& basis_name,
              std::string& node_name);
protected:
// data:
    basis_basic<T>                                      _b;
    mutable mode_type                                   _mode; 
    quadrature<T>                                       _quad; // when mode: on quadrature pointset
    basis_basic<T>                                      _nb;   // when mode: on nodal basis pointset
public:

// _val [tilde_K] (inod,idof)
#define _RHEOLEF_declare_member(VALUE,MEMBER)				\
    mutable std::array<							\
              Eigen::Matrix<VALUE,Eigen::Dynamic,Eigen::Dynamic>	\
             ,reference_element::max_variant>           	MEMBER;	\

_RHEOLEF_declare_member(T,_scalar_val)
_RHEOLEF_declare_member(point_basic<T>,_vector_val)
_RHEOLEF_declare_member(tensor_basic<T>,_tensor_val)
_RHEOLEF_declare_member(tensor3_basic<T>,_tensor3_val)
_RHEOLEF_declare_member(tensor4_basic<T>,_tensor4_val)
#undef _RHEOLEF_declare_member

// sid_val [tilde_L][loc_isid][orient][shift] (inod,idof)
#define _RHEOLEF_declare_member(VALUE,MEMBER)				\
    mutable std::array<							\
              std::array<						\
                std::array<						\
                  std::array<						\
                    Eigen::Matrix<VALUE,Eigen::Dynamic,Eigen::Dynamic>,	\
	            8>,							\
                  2>,							\
                8>,							\
              reference_element::max_variant>           MEMBER;

_RHEOLEF_declare_member(T,_sid_scalar_val)
_RHEOLEF_declare_member(point_basic<T>,_sid_vector_val)
_RHEOLEF_declare_member(tensor_basic<T>,_sid_tensor_val)
_RHEOLEF_declare_member(tensor3_basic<T>,_sid_tensor3_val)
_RHEOLEF_declare_member(tensor4_basic<T>,_sid_tensor4_val)
#undef _RHEOLEF_declare_member

protected:
    mutable std::array<bool,
	               reference_element::max_variant>  _initialized;
    mutable std::array<bool,
	               reference_element::max_variant>  _grad_initialized;
    mutable std::array<bool,
	               reference_element::max_variant>  _sid_initialized;
// internals:
    void          _initialize           (reference_element hat_K) const;
    void     _grad_initialize           (reference_element hat_K) const;
    void          _initialize_continued (reference_element hat_K,
							const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node) const;
    void     _grad_initialize_continued (reference_element hat_K,
							const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node) const;
    void      _sid_initialize           (reference_element tilde_L) const;
    void _sid_grad_initialize           (reference_element tilde_L) const;
    void      _sid_initialize           (reference_element tilde_L, const side_information_type& sid) const;
    void _sid_grad_initialize           (reference_element tilde_L, const side_information_type& sid) const;
    void      _sid_initialize_continued (reference_element tilde_L, const side_information_type& sid,
						        const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node) const;
    void _sid_grad_initialize_continued (reference_element tilde_L, const side_information_type& sid,
						        const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node) const;
};
// -----------------------------------------------------------------------
// interface with shallow copy semantic
// -----------------------------------------------------------------------
//<verbatim:
template<class T>
class basis_on_pointset: public smart_pointer<basis_on_pointset_rep<T>>,
                         public persistent_table<basis_on_pointset<T>> {
public:

  typedef basis_on_pointset_rep<T>          rep;
  typedef smart_pointer<rep>                base;
  typedef typename rep::size_type           size_type;

// allocators:

  basis_on_pointset (const std::string& name = "");
  basis_on_pointset (const quadrature<T>&  quad, const basis_basic<T>& b);
  basis_on_pointset (const basis_basic<T>& nb,   const basis_basic<T>& b);

// modifiers:

  void reset (const std::string& name);
  void set (const quadrature<T>&  quad, const basis_basic<T>& b);
  void set (const basis_basic<T>& nb,   const basis_basic<T>& b);

// accessors:

  bool is_set() const;
  const basis_basic<T>& get_basis() const;
  size_type ndof (reference_element hat_K) const;
  size_type nnod (reference_element hat_K) const;
  bool has_quadrature() const;
  const quadrature<T>& get_quadrature() const;
  const basis_basic<T>& get_nodal_basis() const;

  template<class Value>
  const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
  evaluate (reference_element hat_K) const;

  template<class Value>
  const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
  evaluate_on_side (
    reference_element		 tilde_L,
    const side_information_type& sid) const;

  template<class Value>
  const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
  grad_evaluate (reference_element hat_K) const;

  template<class Value>
  const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
  grad_evaluate_on_side (
    reference_element		 tilde_L,
    const side_information_type& sid) const;
};
//>verbatim:

// -----------------------------------------------------------------------
// inlined
// -----------------------------------------------------------------------
template<class T>
inline
const basis_basic<T>&
basis_on_pointset<T>::get_basis() const
{
  return base::data().get_basis();
}
template<class T>
inline
typename basis_on_pointset<T>::size_type
basis_on_pointset<T>::ndof (reference_element hat_K) const
{
  return base::data().ndof (hat_K);
}
template<class T>
inline
typename basis_on_pointset<T>::size_type
basis_on_pointset<T>::nnod (reference_element hat_K) const
{
  return base::data().nnod (hat_K);
}
template<class T>
inline
bool
basis_on_pointset<T>::has_quadrature() const
{
  return base::data().has_quadrature();
}
template<class T>
inline
const quadrature<T>&
basis_on_pointset<T>::get_quadrature() const
{
  return base::data().get_quadrature();
}
template<class T>
inline
const basis_basic<T>&
basis_on_pointset<T>::get_nodal_basis() const
{
  return base::data().get_nodal_basis();
}
template<class T>
inline
bool
basis_on_pointset<T>::is_set() const
{
  return base::data().is_set();
}
template<class T>
template<class Value>
inline
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
basis_on_pointset<T>::evaluate (reference_element hat_K) const
{
  return base::data().template evaluate<Value> (hat_K);
}
template<class T>
template<class Value>
inline
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
basis_on_pointset<T>::grad_evaluate (reference_element hat_K) const
{
  return base::data().template grad_evaluate<Value> (hat_K);
}
template<class T>
template<class Value>
inline
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
basis_on_pointset<T>::evaluate_on_side (
    reference_element				        tilde_L,
    const side_information_type&		        sid) const
{
  return base::data().template evaluate_on_side<Value> (tilde_L, sid);
}
template<class T>
template<class Value>
inline
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
basis_on_pointset<T>::grad_evaluate_on_side (
    reference_element		 tilde_L,
    const side_information_type& sid) const
{
  return base::data().template grad_evaluate_on_side<Value> (tilde_L, sid);
}

}// namespace rheolef
#endif // _RHEO_BASIS_ON_POINTSET_V2_H
