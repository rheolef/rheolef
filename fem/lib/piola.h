#ifndef _RHEOLEF_PIOLA_H
#define _RHEOLEF_PIOLA_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// piola transformation:
//
//	F : hat_K ---> K
//          hat_x +--> x = F(hat_x)
//
// i.e. map any geo_element K from a reference_element hat_K
//
#include "rheolef/tensor.h"
#include "rheolef/space_constant.h"
namespace rheolef {


// ----------------------------------------------------------------------------
// options for gradient computations
// ----------------------------------------------------------------------------
namespace details {

struct differentiate_option {
  typedef enum {
    none       = 0,
    gradient   = 1,
    divergence = 2,
    curl       = 3
  } type;

  bool symmetrized;             // compute D(u): symmetrize grad(u) when u is a vector
  bool surfacic;                // compute grad_s(u): applies also the P=I-nxn projector on the surface
  bool broken;                  // compute grad_h(u): applies also for discontinuous Galerkin
  bool batchelor_curl;          // for computing the stream function: Batchelor trick in axisymmetric geometries

  differentiate_option()
  : symmetrized(false),
    surfacic(false),
    broken(false),
    batchelor_curl(false)
  {}
};

} // namespace details
// ----------------------------------------------------------------------------
// the piola class
// ----------------------------------------------------------------------------
template<class T>
struct piola {

  typedef typename tensor_basic<T>::size_type size_type;
  typedef space_constant::coordinate_type     coordinate_type;

  piola();
  void clear();

// data:
  size_type       d, map_d;
  coordinate_type sys_coord;
  bool ignore_sys_coord;
  point_basic<T>  F;
  tensor_basic<T> DF, invDF, P;
  T               detDF;
  bool            has_nt_basis; // for vector-valued space with block_n boundary condition
  point_basic<T>  normal;       // idem
};
template<class T>
inline
piola<T>::piola()
  : d(0),
    map_d(0),
    sys_coord(space_constant::cartesian),
    ignore_sys_coord(false),
    F(),
    DF(),
    invDF(),
    P(),
    detDF(0),
    has_nt_basis(false),
    normal()
{
}
template<class T>
inline
void
piola<T>::clear()
{
  d = 0;
  map_d = 0;
  sys_coord = space_constant::cartesian;
  ignore_sys_coord = false;
  F = point_basic<T>(0,0,0);
  DF.fill(0);
  invDF.fill(0);
  P.fill(0);
  detDF = 0;
  has_nt_basis = false;
  normal = point_basic<T>(0,0,0);
}

}// namespace rheolef
#endif // _RHEOLEF_PIOLA_H
