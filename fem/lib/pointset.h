#ifndef _RHEOLEF_POINTSET_H
#define _RHEOLEF_POINTSET_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
/*Class:pointset
NAME: @code{poinset} - a set of points in the reference element
@clindex pointset
@cindex  reference element
@clindex reference_element
SYNOPSIS:
  @noindent
  The @code{pointset} class defines a set of points in the reference element
  (see @ref{reference_element iclass}).
  It could be associated either to a quadrature formulae or to a lattice,
  e.g. equispaced Lagrange lattice.
OPTIONS:
  The pointset basis recognize some options,
  transmitted to the constructor of the basis class:
  see @ref{pointset_option iclass}.
AUTHOR: Pierre.Saramito@imag.fr
DATE:   7 may 2019
End:
*/
#include "rheolef/point.h"
#include "rheolef/reference_element.h"
#include "rheolef/pointset_option.h"

namespace rheolef {

template<class T>
class pointset_rep {
public:

// typedefs:

  typedef reference_element::size_type size_type;
  typedef T                            value_type;

// allocators:

  pointset_rep (const pointset_option& popt);
  void reset   (const pointset_option& popt);

// accessors:

  const pointset_option& option() const { return _sopt; }
  size_type nnod (const reference_element& hat_K) const;
  const point_basic<T>& node (const reference_element& hat_K, size_i inod) const

protected:
// data:
  const std::array<bool,reference_element::max_variant>    _is_initialized;;
  const std::array<
  	  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>  
         ,reference_element::max_variant>                  _hat_x;
  pointset_option                                          _sopt;
};

//<pointset:
template<class T>
class pointset: public smart_pointer_nocopy<basis_rep<T> > {
public:

// typedefs:

  typedef pointset_rep<T>           rep;
  typedef smart_pointer_nocopy<rep> base;
  typedef typename rep::size_type   size_type;
  typedef typename rep::value_type  value_type;

// allocators:

  pointset   (const pointset_option& popt = pointset_option());
  void reset (const pointset_option& popt);

// accessors:

  const pointset_option& option() const
	{ return data().option(); }
  size_type nnod (const reference_element& hat_K) const
	{ return data().nnod(hat_K); }
  const point_basic<T>& node (const reference_element& hat_K, size_i inod) const
	{ return data().node (hat_K, inod); }
};
//>verbatim:

}// namespace rheolef
#endif // _RHEOLEF_POINTSET_H
