///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// See trace-n-fig.fig
//  
#include "basis_fem_trace_n.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"

#include "equispaced.icc"
#include "warburton.icc"
#include "fekete.icc"
#include "eigen_util.h"

namespace rheolef {

using namespace std;

// =========================================================================
// basis members
// =========================================================================
template<class T>
basis_fem_trace_n<T>::~basis_fem_trace_n()
{
}
template<class T>
basis_fem_trace_n<T>::basis_fem_trace_n (const basis_basic<T>& vec_basis)
  : basis_rep<T>(vec_basis.option()),
    _vec_basis  (vec_basis),
    _tr_n_basis (),
    _hat_node(),
    _first_sid_inod(),
    _first_sid_idof(),
    _sid_value()
{
  check_macro(_vec_basis.valued_tag() == space_constant::vector,
    "unexpected "<< _vec_basis.valued() <<"-valued basis \""<<_vec_basis.name()
    <<"\": a vector-valued basis is expected for the trace_n(.) operator");
  base::_sopt.set_trace_n (true);
  size_type    tr_n_degree = _vec_basis.family_index(); // RTkd.degree=k+1 but RTkd.family_index=k: it fully contains (Pkd)^d
  basis_option tr_n_opt    = _vec_basis.option();
  std::string  tr_name     = base::standard_naming ("P", tr_n_degree, tr_n_opt);
  _tr_n_basis = basis_basic<T> (tr_name);
  base::_name = base::standard_naming (family_name(), family_index(), base::_sopt);
  base::_piola_fem = _tr_n_basis.get_piola_fem();
  _initialize_cstor_sizes();
  if (base::option().is_continuous()) {
    // "trace_n(RTkd)" : discontinuous and multi-valued along sides connections (edges in 3d, vertives in 2d)
    // "trace_n(RTk)"  : could be continuous, but not yet supported
    warning_macro("normal trace of continuous approximation not yet supported");
    error_macro  ("HINT: replace \""<<_vec_basis.name()<<"\" by its dicontinuous version");
  }
}
template<class T>
void
basis_fem_trace_n<T>::_initialize_cstor_sizes() const
{
  size_type k = degree();
  for (size_type d = 0; d < 4; ++d) {
    base::_ndof_on_subgeo_internal [d].fill (0);
    base::_nnod_on_subgeo_internal [d].fill (0);
    if (d == 0) continue;
    for (size_type subgeo_variant = reference_element::first_variant_by_dimension(d-1);
                   subgeo_variant < reference_element:: last_variant_by_dimension(d-1);
                   subgeo_variant++) {
      // customized for _vec_basis="RTkd" on subgeo-variant : all dofs in "_vec_basis" are merged on (d-1) = dim(subgeo_variant)
      // => all dofs are associated to the "d" dimension in the present element
      // => cannot handle yet the case _vec_basis="RTkd" here : it requires "_vec_basis.ndof_on_subgeo_internal(d,subgeo_variant)
      base::_ndof_on_subgeo_internal [d][subgeo_variant] = _tr_n_basis.ndof_on_subgeo (d-1, subgeo_variant);
      base::_nnod_on_subgeo_internal [d][subgeo_variant] = _tr_n_basis.nnod_on_subgeo (d-1, subgeo_variant);
    }
  }
  bool is_continuous = false;
  base::_helper_make_discontinuous_ndof_on_subgeo (is_continuous, base::_ndof_on_subgeo_internal, base::_ndof_on_subgeo);
  base::_helper_make_discontinuous_ndof_on_subgeo (is_continuous, base::_nnod_on_subgeo_internal, base::_nnod_on_subgeo);
  base::_helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (base::_ndof_on_subgeo_internal, base::_first_idof_by_dimension_internal);
  base::_helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (base::_ndof_on_subgeo,          base::_first_idof_by_dimension);
  base::_helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (base::_nnod_on_subgeo_internal, base::_first_inod_by_dimension_internal);
  base::_helper_initialize_first_ixxx_by_dimension_from_nxxx_on_subgeo (base::_nnod_on_subgeo,          base::_first_inod_by_dimension);
}
template<class T>
void
basis_fem_trace_n<T>::_initialize_data (reference_element tilde_K) const
{
  size_type k = degree();
  size_type d = tilde_K.dimension();
  size_type variant = tilde_K.variant();

  // nodes:
  _first_sid_idof [variant].fill (0);
  size_type loc_nnod = base::_first_inod_by_dimension [variant][d+1];
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& tilde_node = _hat_node [variant];
  tilde_node.resize (loc_nnod);
  size_type loc_first_sid_inod = 0;
  for (size_type loc_isid = 0, loc_nsid = tilde_K.n_side(); loc_isid < loc_nsid; ++loc_isid) {
    side_information_type sid (tilde_K, loc_isid);
    reference_element hat_S = sid.hat;
    const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& side_hat_node = _tr_n_basis.hat_node (hat_S);
    size_type loc_sid_nnod = side_hat_node.size();
    for (size_type loc_sid_inod = 0; loc_sid_inod < loc_sid_nnod; ++loc_sid_inod) {
      // piola transform side nodes from hat_S to partial tilde_K
      size_type loc_inod = loc_first_sid_inod + loc_sid_inod;
      tilde_node[loc_inod] = reference_element_face_transformation (tilde_K, sid, side_hat_node[loc_sid_inod]);
    }
    loc_first_sid_inod += loc_sid_nnod;
    _first_sid_idof [variant][loc_isid+1] = loc_first_sid_inod;
  }
  _first_sid_inod [variant] = _first_sid_idof [variant];
}
template<class T>
const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
basis_fem_trace_n<T>::hat_node (reference_element hat_K) const
{
  base::_initialize_data_guard (hat_K);
  return _hat_node [hat_K.variant()];
}
// evaluation on a side of all related basis functions at hat_x in hat_S
// NOTE: by convention, the value array has a full size for all sides dofs
//       and is filled by zero on all others sides
template<class T>
void
basis_fem_trace_n<T>::evaluate_on_side (
    reference_element            tilde_K,
    const side_information_type& sid,
    const point_basic<T>&        hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const
{
  base::_initialize_data_guard (tilde_K);
  size_type variant = tilde_K.variant();
  size_type loc_ndof = base::ndof (tilde_K);
  value.resize (loc_ndof);
  value.fill(T(0));
  size_type loc_first_sid_idof = _first_sid_inod [variant][sid.loc_isid];
  reference_element hat_S = sid.hat;
  _tr_n_basis.evaluate (hat_S, hat_x, _sid_value);
  for (size_type loc_sid_idof = 0, loc_sid_ndof = _sid_value.size(); loc_sid_idof < loc_sid_ndof; ++loc_sid_idof) {
    size_type loc_idof = loc_first_sid_idof + loc_sid_idof;
    value [loc_idof] = _sid_value [loc_sid_idof];
  }
}
// extract local dof-indexes on a side
template<class T>
typename basis_fem_trace_n<T>::size_type
basis_fem_trace_n<T>::local_ndof_on_side (
  reference_element            tilde_K,
  const side_information_type& sid) const
{
  base::_initialize_data_guard (tilde_K);
  return _first_sid_inod [tilde_K.variant()][sid.loc_isid+1]
       - _first_sid_inod [tilde_K.variant()][sid.loc_isid];
}
template<class T>
void
basis_fem_trace_n<T>::local_idof_on_side (
  reference_element            tilde_K,
  const side_information_type& sid,
  Eigen::Matrix<size_type,Eigen::Dynamic,1>& loc_idof) const
{
  base::_initialize_data_guard (tilde_K);
  size_type variant = tilde_K.variant();
  size_type first_loc_inod = _first_sid_inod [variant][sid.loc_isid];
  size_type   loc_sid_nnod = _first_sid_inod [variant][sid.loc_isid+1]
                           - _first_sid_inod [variant][sid.loc_isid];
  loc_idof.resize (loc_sid_nnod);
  for (size_type  loc_sid_inod = 0; loc_sid_inod < loc_sid_nnod; ++loc_sid_inod) {
    loc_idof [loc_sid_inod] = first_loc_inod + loc_sid_inod;
  }
}
// dofs for a scalar-valued function
template<class T>
void
basis_fem_trace_n<T>::_compute_dofs (
  reference_element     tilde_K,
  const Eigen::Matrix<T,Eigen::Dynamic,1>&   f_xnod, 
        Eigen::Matrix<T,Eigen::Dynamic,1>&   dof) const
{
  check_macro (is_nodal(), "_compute_dofs: only supported for nodal basis");
  dof = f_xnod;
}
// -----------------------------------------------------------------------------
// visu: gnuplot in elevation for 2d (t,q)
// -----------------------------------------------------------------------------
template<class T>
void
basis_fem_trace_n<T>::put_scalar_valued (ostream& os, reference_element tilde_K) const
{
  bool verbose  = iorheo::getverbose(os);
  bool clean    = iorheo::getclean(os);
  bool execute  = iorheo::getexecute(os);
  size_type nsub = iorheo::getsubdivide(os);
  if (nsub <= 1) nsub = 20; // default value

  size_type loc_ndof = base::ndof (tilde_K);
  string basename  = "basis-" + base::name() + "-" + tilde_K.name();
  string filelist;
  // --------------------
  // .plot
  // --------------------
  string plot_name = basename + ".plot";
  string gdat_name = basename + ".gdat";
  ofstream plot (plot_name.c_str());
  cerr << "! file \"" << plot_name << "\" created" << endl;
  filelist += " \"" + plot_name + "\"";
  size_type d = tilde_K.dimension();
  check_macro (d==2, "unsupported dimension " << d);
  plot << "gdat = \"" << gdat_name << "\"" << endl
       << "set colors classic" << endl
       << "set size square" << endl
       << "pause_duration = 1.5" << endl
       << "set view 55, 145" << endl
      ;
  if (tilde_K.variant() == reference_element::t) {
    plot << "set xrange [-0.1:1.1]" << endl
         << "set yrange [-0.1:1.1]" << endl
         << "set zrange [-1.1:1.1]" << endl
         << "set arrow from 0,0,0 to 1,0,0 nohead lc 0" << endl
         << "set arrow from 1,0,0 to 0,1,0 nohead lc 0" << endl
         << "set arrow from 0,1,0 to 0,0,0 nohead lc 0" << endl
	;
  } else {
    plot << "set xrange [-1.1:1.1]" << endl
         << "set yrange [-1.1:1.1]" << endl
         << "set zrange [-1.1:1.1]" << endl
         << "set arrow from -1,-1,0 to  1,-1,0 nohead lc 0" << endl
         << "set arrow from  1,-1,0 to  1, 1,0 nohead lc 0" << endl
         << "set arrow from  1, 1,0 to -1, 1,0 nohead lc 0" << endl
         << "set arrow from -1, 1,0 to -1,-1,0 nohead lc 0" << endl
	;
  }
  plot << "do for [loc_isid=0:"<<tilde_K.n_side()-1<<"] {" << endl
       << " loc_sid_ndof=" << degree()+1 << endl
       << " do for [i=0:loc_sid_ndof-1] {" << endl
       << "  ip3=i+3" << endl
       << "  splot gdat index loc_isid u 1:2:ip3 t sprintf(\"side %d: L%d\",loc_isid,i) w l lc 1" << endl
       << "  pause pause_duration" << endl
       << " }" << endl
       << "}" << endl
       << "pause -1 \"<return>\"" << endl
    ;
  plot.close();
  // --------------------
  // .gdat
  // --------------------
  ofstream gdat (gdat_name.c_str());
  cerr << "! file \"" << gdat_name << "\" created" << endl;
  filelist += " \"" + gdat_name + "\"";
  gdat << setprecision(std::numeric_limits<T>::digits10)
       << "# basis   " << base::name() << endl
       << "# element " << tilde_K.name() << endl
       << "# degree  " << degree() << endl
    ;
  Eigen::Matrix<T,Eigen::Dynamic,1> value;
  for (size_type loc_isid = 0, loc_nsid = tilde_K.n_side(); loc_isid < loc_nsid; ++loc_isid) {
    side_information_type sid (tilde_K, loc_isid);
    reference_element hat_S = sid.hat;
    size_type loc_sid_ndof = first_sid_idof (tilde_K,loc_isid+1) - first_sid_idof (tilde_K,loc_isid);
    gdat << "# side " << loc_isid << ": size " << loc_sid_ndof << endl
         << "# x y";
    for (size_type loc_sid_idof = 0; loc_sid_idof < loc_sid_ndof; ++loc_sid_idof) {
      gdat << " L" << loc_sid_idof+1;
    }
    gdat << endl;
    switch (hat_S.variant()) {
      case reference_element::e: {
        for (size_type i = 0; i <= nsub; i++) { 
          point_basic<T> sid_hat_x (T(int(i))/T(int(nsub)));
          evaluate_on_side (tilde_K, sid, sid_hat_x, value);
	  point_basic<T> tilde_x = reference_element_face_transformation (tilde_K, sid, sid_hat_x);
	  gdat << tilde_x[0] << " " << tilde_x[1];
	  for (size_type loc_idof = first_sid_idof (tilde_K,loc_isid), last_loc_idof = first_sid_idof (tilde_K,loc_isid+1);
		loc_idof < last_loc_idof; ++loc_idof) {
	    gdat << " " << value[loc_idof];
          }
          gdat << endl;
        }
        gdat << endl << endl;
        break;
      }
      default:
        error_macro ("unexpected side type `"<<hat_S.name()<<"'");
    }
  }
  // -----------
  if (execute) {
  // -----------
    string command = "gnuplot \"" + plot_name + "\"";
    if (verbose) clog << "! " << command << endl;
    int status = system (command.c_str());
  }
  // -----------
  if (clean) {
  // -----------
    string command = "/bin/rm -f " + filelist;
    if (verbose) clog << "! " << command << endl;
    int status = system (command.c_str());
  }
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template class basis_fem_trace_n<T>;

_RHEOLEF_instanciation(Float)

}// namespace rheolef
