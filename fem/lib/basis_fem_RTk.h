#ifndef _RHEOLEF_BASIS_FEM_RTK_H
#define _RHEOLEF_BASIS_FEM_RTK_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*Class:sherwin
NAME: @code{RTk} - The Raviart-Thomas vector-valued polynomial basis
@cindex  polynomial basis
@clindex space
@clindex basis
SYNOPSIS:
  space Vh(omega,"RT0");
DESCRIPTION:
  @noindent
  This @code{basis} is described 
  in Raviart and Thomas (Mathematical aspects of finite element methods, Springer, 1977).
  It is indicated in the @code{space} (see @ref{space class})
  by a string starting with
  the two letters @code{"RT"},
  followed by digits indicating the polynomial order.

OPTIONS:
  This basis recognizes the equispaced/warburton node option
  for degrees of freedom located on sides.
  See @ref{basis_option class}.

AUTHOR: Pierre.Saramito@imag.fr
DATE:   12 september 2017
End:
*/
#include "rheolef/basis.h"
#include "rheolef/quadrature.h"
namespace rheolef {

template<class T>
class basis_fem_RTk: public basis_rep<T> {
public:

// typedefs:

  typedef basis_rep<T>                 base;
  typedef reference_element::size_type size_type;
  typedef point_basic<T>               value_type;
  typedef typename base::valued_type   valued_type;

// allocators:

  basis_fem_RTk (size_type k, const basis_option& sopt);
  ~basis_fem_RTk();

// accessors:

  size_type degree() const { return _b_pre_kp1.degree(); }
  std::string family_name()  const { return "RT"; }
  size_type   family_index() const { return degree()-1; }
  bool is_nodal() const { return false; }
  valued_type        valued_tag() const { return space_constant::vector; }
  const std::string& valued()     const { return space_constant::valued_name (valued_tag()); }

  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
  hat_node (reference_element hat_K) const;

  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
  vdm (reference_element hat_K) const;

  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
  inv_vdm (reference_element hat_K) const;

// evaluation of all basis functions at hat_x:

  void evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<value_type,Eigen::Dynamic,1>& value) const;

  void grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,1>& value) const;

// internals:

  void _initialize_cstor_sizes() const;
  void _initialize_data (reference_element hat_K) const;
  void _compute_dofs (
    reference_element                                   hat_K, 
    const Eigen::Matrix<value_type,Eigen::Dynamic,1>&   f_xnod,
          Eigen::Matrix<T,Eigen::Dynamic,1>&            dof) const;

protected:

// data:
  // valued: initialized by _initialize_data:
  basis_raw_basic<T> 		  		    _b_pre_kp1;

  mutable std::array<
             Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>,
             reference_element::max_variant>        _hat_node;

  mutable std::array<Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>,
             reference_element::max_variant>        _vdm, _inv_vdm;

  mutable quadrature<T> 		            _quad;

  mutable std::array<
            Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>,
            reference_element::max_variant>         _bar_a;

  mutable std::array<
            std::array<
              Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>
             ,3>,
            reference_element::max_variant>         _bkm1_node_internal_d;
};

} // namespace rheolef
#endif // _RHEOLEF_BASIS_FEM_RTK_H
