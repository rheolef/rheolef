#ifndef _RHEOLEF_BASIS_FEM_PK_BERNSTEIN_H
#define _RHEOLEF_BASIS_FEM_PK_BERNSTEIN_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*Class:sherwin
NAME: @code{Bk} - Bernstein polynomial basis
@cindex  polynomial basis
@clindex space
@clindex basis
@clindex reference_element
@clindex basis_option
SYNOPSIS:
  space Vh(omega,"B5");
DESCRIPTION:
  @noindent
  This @code{basis} was initially introduced by Bernstein
  (Comm. Soc. Math. Kharkov, 2th series, 1912) and more
  recently used in the context of finite elements.
  It is indicated in the @code{space} (see @ref{space class})
  by a string starting with
  the letter @code{"B"},
  followed by digits indicating the polynomial order.

OPTIONS:
  This basis do not recognizes any option.
  See @ref{basis_option class}.

AUTHOR: Pierre.Saramito@imag.fr
DATE:   23 september 2017
End:
*/
//
// Bernstein Pk basis
//
// author: Pierre.Saramito@imag.fr
//
// date: 11 september 2017
//
#include "rheolef/basis.h"
namespace rheolef {

template<class T>
class basis_fem_Pk_bernstein: public basis_rep<T> {
public:

// typedefs:

  typedef basis_rep<T>              base;
  typedef reference_element::size_type size_type;
  typedef T                            value_type;

// allocators:

  basis_fem_Pk_bernstein (size_type degree, const basis_option& sopt);
  ~basis_fem_Pk_bernstein();

// accessors:

  std::string family_name() const { return "B"; }
  size_type degree() const { return _raw_basis.degree(); }
  bool is_nodal() const { return false; }

  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
  hat_node (reference_element hat_K) const;

  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
  vdm (reference_element hat_K) const;

  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
  inv_vdm (reference_element hat_K) const;


// evaluation of all basis functions at hat_x:

  void evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>&                     value) const;

// evaluate the gradient:

  void grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const;

// internals:
  
  void _initialize_cstor_sizes() const;
  void _initialize_data (reference_element hat_K) const;
  void _compute_dofs (
    reference_element     hat_K, 
    const Eigen::Matrix<T,Eigen::Dynamic,1>& f_xnod, // scalar-valued case
          Eigen::Matrix<T,Eigen::Dynamic,1>& dof) const;

protected:
// data:
  basis_raw_basic<T> _raw_basis;

  mutable std::array<
             Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>,
             reference_element::max_variant>        _hat_node;

  mutable std::array<Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>,
             reference_element::max_variant>        _vdm, _inv_vdm;
};

} // namespace rheolef
#endif // _RHEOLEF_BASIS_FEM_PK_BERNSTEIN_H
