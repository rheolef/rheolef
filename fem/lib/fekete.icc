#ifndef _RHEOLEF_FEKETE_ICC
#define _RHEOLEF_FEKETE_ICC
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute the Fekete point set (see BriSomVia-2012)
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 october 2017
//
#include "warburton.icc"

namespace rheolef {

template<class T>
void set_simplex_leb_gll (size_t degree, Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& pts);

// ---------------------------------------------------------------------------
// all cases
// ---------------------------------------------------------------------------
template<class T>
void
pointset_lagrange_fekete (
  reference_element                                hat_K,
  size_t                                           degree,
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&  hat_xnod,
  bool                                             map_on_reference_element = true)
{
  hat_xnod.resize (reference_element::n_node(hat_K.variant(), degree));
  if (degree == 0) {
    reference_element_barycenter (hat_K, hat_xnod[0]);
    return;
  }
  switch (hat_K.variant()) {
    case reference_element::t:
      set_simplex_leb_gll (degree, hat_xnod); break;
    // ------------------------------------------------------------------
    // others are provided with Warburton or Legendre node tensor product
    // ------------------------------------------------------------------
    case reference_element::p:
      hat_xnod [0] = point_basic<T> (T(0)); break;
    case reference_element::e:
      warburton_e (degree, hat_xnod, map_on_reference_element); break;
    case reference_element::q:
      warburton_q (degree, hat_xnod, map_on_reference_element); break;
    case reference_element::H:
      warburton_H (degree, hat_xnod, map_on_reference_element); break;
    case reference_element::P: // TODO: as triangle*Legendre_edge
      warburton_P (degree, hat_xnod, map_on_reference_element); break;
    case reference_element::T: // TODO: how to compute nodes ?
      warburton_T (degree, hat_xnod, map_on_reference_element); break;
    default: error_macro ("unexpected element type `"<<hat_K.name()<<"'");
  }
}

} // namespace rheolef
#endif // _RHEOLEF_WARBURTON_ICC
