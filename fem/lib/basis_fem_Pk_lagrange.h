#ifndef _RHEOLEF_BASIS_FEM_PK_LAGRANGE_H
#define _RHEOLEF_BASIS_FEM_PK_LAGRANGE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*Class:sherwin
NAME: @code{Pk} - Lagrange polynomial basis
@cindex  polynomial basis
@clindex space
@clindex basis
@clindex reference_element
SYNOPSIS:
  space Vh(omega,"P1");
DESCRIPTION:
  @noindent
  This is the most popular polynomial @code{basis} for the finite element method.
  It is indicated in the @code{space} (see @ref{space class})
  by a string starting with
  the letter @code{"P"},
  followed by digits indicating the polynomial order.

OPTIONS:
  This basis recognizes the equispaced/warburton node option
  and the raw polynomial option.
  See @ref{basis_option class}.

AUTHOR: Pierre.Saramito@imag.fr
DATE:   11 september 2017
End:
*/
#include "rheolef/basis.h"
namespace rheolef {

template<class T>
class basis_fem_Pk_lagrange: public basis_rep<T> {
public:

// typedefs:

  typedef basis_rep<T>                 base;
  typedef reference_element::size_type size_type;
  typedef T                            value_type;

// allocators:

  basis_fem_Pk_lagrange (size_type degree, const basis_option& sopt);
  ~basis_fem_Pk_lagrange();

// accessors:

  virtual std::string family_name() const { return "P"; }
  size_type degree() const { return _raw_basis.degree(); }
  bool is_nodal() const;

  size_type local_ndof_on_side (
        reference_element            hat_K,
        const side_information_type& sid) const;
  void local_idof_on_side (
        reference_element            hat_K,
        const side_information_type& sid,
        Eigen::Matrix<size_type,Eigen::Dynamic,1>& loc_idof) const;

  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
  hat_node (reference_element hat_K) const;

  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
  vdm (reference_element hat_K) const;

  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
  inv_vdm (reference_element hat_K) const;

// evaluation of all basis functions at hat_x:

  void evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const;

// evaluate the gradient:

  void grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const;

// interpolate:
  void _compute_dofs (
    reference_element                          hat_K, 
    const Eigen::Matrix<T,Eigen::Dynamic,1>&   f_xnod, // scalar-valued case
          Eigen::Matrix<T,Eigen::Dynamic,1>&   dof) const;

// internals:

  void _initialize_cstor_sizes() const;
  void _initialize_data (reference_element hat_K) const;

  // code used also by basis_fem_Pk_bernstein.cc & basis_fem_Pk_sherwin.cc
  static void init_local_first_idof_by_dimension (
  	reference_element hat_K,
  	size_type k,
  	std::array<size_type,5>& first_idof_by_dimension);
  static void initialize_local_first (
    size_type k,
    bool is_continuous,
    std::array<std::array<size_type,reference_element::max_variant>,4>& ndof_on_subgeo_internal,
    std::array<std::array<size_type,reference_element::max_variant>,4>& ndof_on_subgeo,
    std::array<std::array<size_type,reference_element::max_variant>,4>& nnod_on_subgeo_internal,
    std::array<std::array<size_type,reference_element::max_variant>,4>& nnod_on_subgeo,
    std::array<std::array<size_type,5>,reference_element::max_variant>& first_idof_by_dimension_internal,
    std::array<std::array<size_type,5>,reference_element::max_variant>& first_idof_by_dimension,
    std::array<std::array<size_type,5>,reference_element::max_variant>& first_inod_by_dimension_internal,
    std::array<std::array<size_type,5>,reference_element::max_variant>& first_inod_by_dimension);
  // code used also by numbering_Pkd:
  static void init_local_ndof_on_subgeo (size_type k,
    std::array<size_type,reference_element::max_variant>& loc_ndof_on_subgeo);

protected:
// data:
  basis_raw_basic<T>                                _raw_basis;

  mutable std::array<
             Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>,
             reference_element::max_variant>        _hat_node;

  mutable std::array<Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>,
             reference_element::max_variant>        _vdm, _inv_vdm;
};

} // namespace rheolef
#endif // _RHEOLEF_BASIS_FEM_PK_LAGRANGE_H
