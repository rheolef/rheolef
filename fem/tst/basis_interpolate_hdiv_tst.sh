#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"${TOP_SRCDIR}/fem/lib"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"
. "${TOP_SRCDIR}/config/float_compare.sh"


#echo "      not yet (skiped)"
#exit 0

status=0
# hat_K  k  err_l2  err_div_l2
err_l2_valid_list="
t 0 8e-1 8e-1
t 1 2e-1 6e-1
t 2 8e-2 2e-1
t 3 1e-2 5e-2
q 0 8e-1 2e-0
q 1 5e-1 1e-0
q 2 2e-1 6e-1
q 3 5e-2 2e-1
T 0 7e-1 7e-1
T 1 2e-1 5e-1
T 2 5e-2 4e-1
T 3 5e-3 6e-2
H 0 2e-0 2e-0
H 1 7e-1 2e-0
H 2 3e-1 7e-1
H 3 6e-2 3e-1
"

while test "$err_l2_valid_list" != ""; do
  K=`echo $err_l2_valid_list | gawk '{print $1}'`
  k=`echo $err_l2_valid_list | gawk '{print $2}'`
  err_l2_valid=`echo $err_l2_valid_list | gawk '{print $3}'`
  err_div_l2_valid=`echo $err_l2_valid_list | gawk '{print $4}'`
  err_l2_valid_list=`echo $err_l2_valid_list | gawk '{for (i=5; i <= NF; i++) print $i}'`
  command="./basis_interpolate_hdiv_tst RT${k} $K $err_l2_valid $err_div_l2_valid >/dev/null 2>/dev/null"
# echo "      $command"
  run $command
  if test $? -ne 0; then status=1; fi
done

exit $status
