///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// for checking the spectral convergence exp(-ak)
// on the reference element
//
// author: Pierre.Saramito@imag.fr
//
// date: 1 october 2017
//
#include "field_element.h"
#include "form_element.h"
using namespace rheolef;
using namespace std;
using namespace Eigen;

// ---------------------------------------------------
// - Laplace u = f  in hat_K
//   u = g          on partial hat_K
// ---------------------------------------------------
// for this test with the FEM, see :
//   rheolef/doc/pexamples/dirichlet-nh.cc
//   rheolef/doc/pexamples/cosinusprod_helmholtz.icc
//   rheolef/doc/pexamples/cosinusprod.icc
// TODO: inclure ces fonctions au lieu de les copier
struct f {
  Float operator() (const point& x) const { 
    return d*pi*pi*cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  f(size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; const Float pi;
};
struct g {
  Float operator() (const point& x) const { 
    return cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  g(size_t d1) : pi(acos(Float(-1))) {}
  const Float pi;
};
struct u_exact {
  Float operator() (const point& x) const { 
    return cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  u_exact(size_t d1) : pi(acos(Float(-1.0))) {}
  Float pi;
};
// ---------------------------------------------------
// build rhs
// ---------------------------------------------------
template<class T, class Basis, class Function>
void
build_rhs (
  const Function&    f,
  const Basis&       b,
  reference_element  hat_K,
  Matrix<T,Dynamic,1>&      lk)
{
  typedef reference_element::size_type size_type;
  quadrature_option qopt;
  qopt.set_order(4*b.degree()+4);
  quadrature<Float> quad (qopt);
  size_type loc_ndof = b.ndof (hat_K);
  Matrix<T,Dynamic,1> phi (loc_ndof);
  lk.resize (loc_ndof);
  lk.fill (T(0));
  for (typename quadrature<T>::const_iterator iter_q = quad.begin(hat_K),
        last_q = quad.end(hat_K); iter_q != last_q; iter_q++) {
    b.evaluate (hat_K, (*iter_q).x, phi);
    lk += ((*iter_q).w*f((*iter_q).x))*phi;
  }
}
// ---------------------------------------------------
int main(int argc, char**argv) {
// ---------------------------------------------------
  string approx = (argc > 1) ?      argv[1]    : "P3";
  char   t      = (argc > 2) ?      argv[2][0] : 't';
  size_t nsub   = (argc > 3) ? atoi(argv[3])   : 0;
  Float  tol    = (argc > 4) ? atof(argv[4])   : 1;
  bool   dump   = (argc > 5);
  basis b (approx);
  reference_element hat_K;
  hat_K.set_name(t);
  size_t d = hat_K.dimension();
  SparseMatrix<Float,RowMajor>  m, a;
  Matrix<Float,Dynamic,1>       lk;
  build_mass      (b, hat_K, m);
  build_grad_grad (b, hat_K, a);
  build_rhs (f(d), b, hat_K, lk);
  auto boundary = make_pair(b.data().first_idof_by_dimension(hat_K,0), b.data().first_idof_by_dimension(hat_K,d));
  auto interior = make_pair(b.data().first_idof_by_dimension(hat_K,d), b.data().first_idof_by_dimension(hat_K,d+1));
  size_t nb = boundary.second - boundary.first;
  size_t ni = interior.second - interior.first;
  // TODO: send a mail for a(interior,interior) and u(interior) or u[interior] : avoid stupid bugs
  Matrix<Float,Dynamic,Dynamic> a_ii = a.block (interior.first,interior.first, ni,ni);
  Matrix<Float,Dynamic,Dynamic> a_ib = a.block (interior.first,boundary.first, ni,nb);
  space_element Vk (b, hat_K);
  field_element gk (Vk);
  gk.interpolate (g(d));
  field_element uk (Vk);
  uk._dof.block(boundary.first,0,nb,1) = gk._dof.block (boundary.first,0, nb,1);
  Matrix<Float,Dynamic,1> uk_b         = uk._dof.block (boundary.first,0, nb,1);
  Matrix<Float,Dynamic,1> lk_i         =      lk.block (interior.first,0, ni,1);
  Matrix<Float,Dynamic,1> rhs = lk_i - a_ib*uk_b;
  Matrix<Float,Dynamic,1> uk_i (ni);
  PartialPivLU<Matrix<Float,Dynamic,Dynamic> > lu_a_ii = a_ii.lu();
  uk_i =  lu_a_ii.solve (rhs);
  Matrix<Float,Dynamic,1> r = a_ii*uk_i - rhs;
  uk._dof.block (interior.first,0, ni, 1) = uk_i;
  field_element pi_k_u (Vk);
  pi_k_u.interpolate (u_exact(d));
  Float err_l2   = error_l2   (uk, u_exact(d), nsub);
  Float err_linf = error_linf (uk, u_exact(d), nsub);
  cout << "approx   " << b.name() << endl
       << "degree   " << b.degree() << endl
       << "node     " << b.option().get_node_name() << endl
       << "raw_poly " << b.option().get_raw_polynomial_name() << endl
       << "nsub     " << nsub << endl
       << "err_l2   " << err_l2 << endl
       << "err_linf " << err_linf << endl
    ;
  if (dump) {
    ofstream out ("local.m");
    out << setprecision(16)
        << "uk_i = [" << endl << uk_i << "];" << endl
        << "a_ii = [" << endl << a_ii << "];" << endl
        << "a_ib = [" << endl << a_ib << "];" << endl
        << "lk_i = [" << endl << lk_i << "];" << endl
        << "uk_b = [" << endl << uk_b << "];" << endl
        << "rhs_loc = lk_i - a_ib*uk_b;" << endl
	  ;
  }
#ifdef TODO
#endif // TODO
  return (err_linf < tol) ? 0 : 1;
}
