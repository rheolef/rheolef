///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check the basis orderings
//
// author: Pierre.Saramito@imag.fr
//
// date: 9 september 2017
//
#include "rheolef/basis.h"
using namespace rheolef;
using namespace std;
#include "basis_ordering.icc"
#include "dubiner.icc"

void
put_basis_ordering (
  reference_element			   hat_K,
  size_t                                   degree,
  const std::vector<point_basic<size_t> >& power_index,
  ostream&                                 out)
{
  size_t d = hat_K.dimension();
  char symbol [3] = {'x', 'y', 'z'};
  for (size_t loc_idof = 0, loc_ndof = power_index.size(); loc_idof < loc_ndof; ++loc_idof) {
    for (size_t mu = 0; mu < d; ++mu) {
      out << symbol[mu] << "^" << power_index[loc_idof][mu];
      if (mu+1 != d) out << "*";
    }
    out << "  deg=" << get_degree (hat_K, power_index[loc_idof]) << endl;
  }
}
int
compare (
  const std::vector<point_basic<size_t> >& power_index_1,
  const std::vector<point_basic<size_t> >& power_index_2)
{
  for (size_t loc_idof = 0, loc_ndof = power_index_1.size(); loc_idof < loc_ndof; ++loc_idof) {
    if (power_index_1 [loc_idof] != power_index_2 [loc_idof]) return 0;
  }
  return 1;
}
bool 
check_ordering (
  reference_element hat_K,
  size_t            degree,
  bool              do_dump = false)
{
  if (do_dump) {
    cerr << "------------------------------------------------" << endl
         << "hat_K = "<<hat_K.name() << " degree = " << degree << endl
         << "------------------------------------------------" << endl;
  }
  point_basic<size_t> no_init(-1,-1,-1);
  size_t loc_ndof = reference_element::n_node (hat_K.variant(), degree);
  // --------------------------------------------------
  if (do_dump) cout << "lattice ordering:"<<endl;
  // --------------------------------------------------
  std::vector<point_basic<size_t> > power_index_lat (loc_ndof, no_init);
  make_power_indexes           (hat_K, degree, power_index_lat);
  if (do_dump) put_basis_ordering (hat_K, degree, power_index_lat, cout);
  // --------------------------------------------------
  if (do_dump) cout << "degree ordering:"<<endl;
  // --------------------------------------------------
  std::vector<point_basic<size_t> > power_index_deg (loc_ndof, no_init);
  make_power_indexes_sorted_by_degrees (hat_K, degree, power_index_deg);
  if (do_dump) put_basis_ordering         (hat_K, degree, power_index_deg, cout);
  // --------------------------------------------------
  if (do_dump) cout << "node ordering:"<<endl;
  // --------------------------------------------------
  std::vector<point_basic<size_t> > power_index_inod (loc_ndof, no_init);
  make_power_indexes_sorted_by_inodes (hat_K, degree, power_index_inod);
  if (do_dump) put_basis_ordering        (hat_K, degree, power_index_inod, cout);
  // --------------------------------------------------
  if (do_dump) cerr << "dubiner natural ordering (= inod?):"<<endl;
  // --------------------------------------------------
  std::vector<point_basic<size_t> > power_index_inod2 (loc_ndof, no_init);
  Eigen::Matrix<Float,Eigen::Dynamic,1> value;
  std::vector<size_t> id (loc_ndof, -1);
  for (size_t loc_idof = 0; loc_idof < loc_ndof; ++loc_idof) {
    id[loc_idof] = loc_idof;
  }
  eval_dubiner_basis (point_basic<Float>(), hat_K, degree, id, value, power_index_inod2);
  if (do_dump) put_basis_ordering             (hat_K, degree,            power_index_inod2, cerr);
  int inod_ok = compare (power_index_inod, power_index_inod2);
  if (do_dump) cerr << "equal to inod : " << inod_ok << endl;
  // --------------------------------------------------
  if (do_dump) cerr << "dubiner degree ordering (= degree?):"<<endl;
  // --------------------------------------------------
  std::vector<point_basic<size_t> > power_index_deg2 (loc_ndof, no_init);
  make_power_indexes (hat_K, degree, power_index_deg2);
  std::vector<size_t> ilat2inod (loc_ndof, -1);
  std::vector<size_t> ideg2ilat (loc_ndof, -1);
  std::vector<point_basic<size_t> > power_index_tmp (loc_ndof, no_init);
  make_power_indexes (hat_K, degree, power_index_tmp);
  sort_by_inodes     (hat_K, degree, power_index_tmp, ilat2inod);
  std::vector<point_basic<size_t> > power_index_tmp2 (loc_ndof, no_init);
  make_power_indexes (hat_K, degree, power_index_tmp2);
  sort_by_degrees (hat_K, degree, power_index_tmp2, ideg2ilat);
  std::vector<size_t> ilat2ideg (loc_ndof, -1);
  std::vector<size_t> inod2ilat (loc_ndof, -1);
  invert_permutation  (ideg2ilat, ilat2ideg);
  invert_permutation  (ilat2inod, inod2ilat);
  std::vector<size_t> inod2ideg (loc_ndof, -1);
  for (size_t iloc = 0, nloc = inod2ideg.size(); iloc < nloc; ++iloc) {
    inod2ideg [iloc] = ilat2ideg [inod2ilat[iloc]]; // pas mal !!
  }
  eval_dubiner_basis (point_basic<Float>(), hat_K, degree, inod2ideg, value, power_index_deg2);
  if (do_dump) put_basis_ordering             (hat_K, degree,                   power_index_deg2, cerr);
  int deg_ok = compare (power_index_deg, power_index_deg2);
  if (do_dump) cerr << "equal to degree : " << deg_ok << endl;
  bool ok = inod_ok && deg_ok;
  if (!do_dump) {
    cout << "P" << degree << "(" << hat_K.name() << "): "
	 << (ok ? "ok" : "**FAILED**") << endl;
  }
  return ok;
}
int main(int argc, char**argv) {
  if ((argc > 1) && (argv[1][0] < '0' || argv[1][0] > '9')) {
    // example :
    //   basis_ordering_tst t 3
    //   => individual check with do_dump
    char   t      = (argc > 1) ? argv[1][0]    : 't';
    size_t degree = (argc > 2) ? atoi(argv[2]) : 10;
    reference_element hat_K;
    hat_K.set_name(t);
    return check_ordering (hat_K, degree, true);
  }
  // example :
  //   basis_ordering_tst 15
  //   => loop check, silent mode
  size_t degree_max = (argc > 1) ? atoi(argv[1]) : 15;
  bool do_dump      = (argc > 2);
  reference_element hat_K;
  bool ok = true;
  for (size_t degree = 0; degree <= degree_max; ++degree) {
    for (size_t variant = 0; variant < reference_element::max_variant; ++variant) {
      hat_K.set_variant(variant);
      ok = ok && check_ordering (hat_K, degree, do_dump);
    }
  }
  cout << "status " << (ok ? "ok" : "**FAILED**") << endl;
  return ok ? 0 : 1;
}
