///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check hdiv approximation : interpolation and commutation
//
#include "field_element.h"
#include "form_element.h"
using namespace rheolef;
using namespace std;
#include "interpolate_RTk_polynom.icc"

// --------------------------------------------------------------------------
// check interpolate
// --------------------------------------------------------------------------
//
//   |pi(u)-u| < tol_h
//     see RobTho-1991, HNA vol 2, page 553, theorem 6.3
//
int check_interpolate (
  const basis&             b,
  const reference_element& hat_K,
  size_t                   nsub,
  Float                    err_l2_valid,
  Float                    err_div_l2_valid)
{
  size_t d = hat_K.dimension();
  space_element Vk (b, hat_K);
  field_element pi_u (Vk);
  pi_u.interpolate (u_exact(d,1)); // TODO: pi_u = interpolate(Vk,u)
  if (nsub == 0) nsub = 2*b.degree() + 2;
  Float err_l2   = error_l2   (pi_u, u_exact(d,1), nsub);
  Float err_linf = error_linf (pi_u, u_exact(d,1), nsub);
  Float err_div_l2   = error_div_l2 (pi_u, u_exact(d,1), nsub);
  Float err_div_linf = error_div_linf (pi_u, u_exact(d,1), nsub);
  cout << "approx       " << b.name() << endl
       << "degree       " << b.degree() << endl
       << "node         " << b.option().get_node_name() << endl
       << "raw_poly     " << b.option().get_raw_polynomial_name() << endl
       << "nsub         " << nsub << endl
       << "hat_K        " << hat_K.name() << endl
       << "err_l2       " << err_l2 << endl
       << "err_linf     " << err_linf << endl
       << "err_div_l2   " << err_div_l2 << endl
       << "err_div_linf " << err_div_linf << endl
    ;
  return (err_l2 <= err_l2_valid && err_div_l2 <= err_div_l2_valid) ? 0 : 1;
}
// --------------------------------------------------------------------------
// check commutation: preliminaries
// --------------------------------------------------------------------------
//
// m(pi(u)-u) == 0 for all momentum
//  see RobTho-1991, HNA vol 2, page 551, theorem 6.1, eqn (6.14)
//
int check_commute (
  const basis&             b,
  const reference_element& hat_K,
  Float                    tol)
{
  size_t d = hat_K.dimension();
  space_element Vk (b, hat_K);
  field_element pi_u (Vk);
  pi_u.interpolate (u_exact(d,1)); // TODO: field pi_u = interpolate(Vk,u)
  field_element pi2_u (Vk);
  pi2_u.interpolate (pi_u); // TODO: field pi2_u = interpolate(Vk,pi_u)
  Float err_commute = 0;
  for (size_t idof = 0, ndof = pi_u._dof.size(); idof < ndof; ++idof) {
    err_commute = std::max (err_commute, abs(pi_u._dof[idof] - pi2_u._dof[idof]));
  }
  cout << "err_commute  " << err_commute << endl;
  return (err_commute <= tol) ? 0 : 1;
}
// --------------------------------------------------------------------------
// check the commutation of the divergence: de Rham diagram
// --------------------------------------------------------------------------
//                 div
//     H(div,K) --------> L2(K)
//         |                 |
//         | pi              | pj
//         v       div       v
//       RTk    -------->   Pk
//
//     div(pi(u)) == pj(div(u))
//
//  where pi is the Lagrange interpolator in the RTk space
//    and pj is the L2       projector    in the Pk  space
//  see RobTho-1991, HNA vol 2, page 552, theorem 6.1, eqn (6.15)
//
int check_commute_div (
  const basis&             b,
  const reference_element& hat_K,
  size_t                   nsub,
  Float                    tol)
{
  // 1) build Lagrange interpolation of u:
  size_t d = hat_K.dimension();
  space_element Vk (b,  hat_K);
  field_element pi_u (Vk);
  pi_u.interpolate (u_exact(d,1)); // TODO: field pi_u = interpolate(Vk,u)
  // 2) build l2 projection of div_u:
  size_t k = b.family_index();
  basis bk ("P"+std::to_string(k));
  space_element Pk (bk, hat_K);
  Eigen::SparseMatrix<Float,Eigen::RowMajor> sp_mass;
  build_mass (bk, hat_K, sp_mass);
  Eigen::Matrix<Float,Eigen::Dynamic,Eigen::Dynamic> mass = sp_mass;
  field_element lk (Vk);
  // TODO: lk.riesz (div_u(d,1)); : le resultat depend de la quadrature !?
  // 3) error evaluate at a lattice:
  Eigen::Matrix<point_basic<Float>,Eigen::Dynamic,1> hat_xnod;
  pointset_lagrange_equispaced (hat_K, nsub, hat_xnod);
  for (size_t loc_inod = 0, loc_nnod = hat_xnod.size(); loc_inod < loc_nnod; ++loc_inod) {
    tensor grad_pi_u_xi = pi_u.template grad_evaluate<tensor>(hat_xnod[loc_inod]);
    Float div_pi_u_xi = tr(grad_pi_u_xi);
  }
  return 0;
}
// --------------------------------------------------------------------------
int main(int argc, char**argv) {
// --------------------------------------------------------------------------
  string approx          = (argc > 1) ?      argv[1]    : "RT0";
  char   t               = (argc > 2) ?      argv[2][0] : 't';
  Float err_l2_valid     = (argc > 3) ? atof(argv[3]) : 0;
  Float err_div_l2_valid = (argc > 4) ? atof(argv[4]) : 0;
  size_t nsub            = (argc > 5) ? atoi(argv[5]) : 0;
  Float  tol             = sqrt(std::numeric_limits<Float>::epsilon());
  basis b (approx);
  reference_element hat_K;
  hat_K.set_name(t);
  int status = 0;
  status |= check_interpolate (b, hat_K, nsub, err_l2_valid, err_div_l2_valid);
  status |= check_commute     (b, hat_K, tol);
  status |= check_commute_div (b, hat_K, nsub, tol);
  return status;
}
