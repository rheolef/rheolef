///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// for basis_sides, e.g. "P3d[sides]"
//
// compute the element mass matrix condition number
// for non-regression test purpose
//
// author: Pierre.Saramito@imag.fr
//
// date: 3 january 2019
//
#include "rheolef/basis.h"
#include "rheolef/quadrature.h"
#include "rheolef/eigen_util.h"
#include "form_element.h"
using namespace rheolef;
using namespace std;
using namespace Eigen;

Float one_f (const point& x) { return 1; }

Float
meas_dK_exact (reference_element tilde_K)
{ Float m = 0;
  for (size_t loc_isid = 0, loc_nsid = tilde_K.n_side(); loc_isid < loc_nsid; ++loc_isid) {
    m += tilde_K.side_measure(loc_isid);
  }
  return m;
}
int 
show_mass (
  const basis&      b,
  reference_element tilde_K,
  bool              dump)
{
warning_macro("show_mass(0)...");
  Float tol = sqrt(std::numeric_limits<Float>::epsilon());
  SparseMatrix<Float,RowMajor> mass_bdr;
warning_macro("show_mass(1)...");
  build_mass_bdr (b, tilde_K, mass_bdr);
warning_macro("show_mass(2)...");
  Matrix<Float,Dynamic,Dynamic> full_mass_bdr = mass_bdr;
warning_macro("show_mass(3)...");
  cout << setprecision(numeric_limits<Float>::digits10)
       << "approx    " << b.name() << endl
       << "element   " << tilde_K.name() << endl
       << "det       " << full_mass_bdr.determinant() << endl
       << "cond      " << cond(full_mass_bdr) << endl
       << "size      " << mass_bdr.rows() << endl
       << "nnz       " << mass_bdr.nonZeros()<< endl
       << "fill      " << 1.*mass_bdr.nonZeros()/sqr(mass_bdr.rows())<< endl
    ;
  if (dump) {
    ofstream out ("mass_bdr.mtx");
    put_matrix_market(out, mass_bdr);
  }
  Matrix<Float,Dynamic,1> one_dof;
  b.compute_dof (tilde_K, one_f, one_dof);
  Matrix<Float,Dynamic,1> meas_dK = one_dof.transpose()*mass_bdr*one_dof;
  Float err = fabs(meas_dK_exact(tilde_K) - Float(meas_dK(0,0)));
  cout << setprecision(numeric_limits<Float>::digits10)
       << "node      " << b.option().get_node_name() << endl
       << "raw_poly  " << b.option().get_raw_polynomial_name() << endl
       << "meas_dK   " << meas_dK(0,0) << endl
       << "meas_dK_e " << meas_dK_exact(tilde_K) << endl
       << "error     " << err << endl
    ;
  return (err < tol) ? 0 : 1;
}
int main(int argc, char**argv) {
  string approx = (argc > 1) ? argv[1]    : "P3d[sides]";
  char   t      = (argc > 2) ? argv[2][0] : 't';
  bool   dump   = (argc > 3);
  reference_element tilde_K;
  tilde_K.set_name(t);
  basis b (approx);
  return show_mass (b, tilde_K, dump);
}
