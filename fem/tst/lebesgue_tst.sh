#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"${TOP_SRCDIR}/fem/lib"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"
. "${TOP_SRCDIR}/config/float_compare.sh"

status=0
k=5
poly="dubiner"
# Pk(K) Lambda:
#     	equispaced	warburton
Lambda_valid_list="
      3.10496 	1.77859456941537
      5.45184   3.11365457950357
      7.864197  5.12818965492722
      9.640776  3.16339864235383
      16.48361  5.40829151503538
      29.04978  5.20280279467494
"
#          e   t   T   q   P  H
nsub_list_orig="50 50 15 50 15 15"

nsub_list=$nsub_list_orig
for K in e t T q P H; do
  nsub=`echo $nsub_list | gawk '{print $1}'`
  nsub_list=`echo $nsub_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for node in equispaced warburton; do
    Lambda_valid=`echo $Lambda_valid_list | gawk '{print $1}'`
    Lambda_valid_list=`echo $Lambda_valid_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
    command="./lebesgue_tst P${k}[$node,$poly] $K $nsub"
    echo "      $command"
    Lambda=`eval "$command 2>/dev/null| grep lebesgue | gawk '{print \\$2}'"`
    #echo "      Lambda=\"$Lambda\""
    if float_differ "$Lambda" "$Lambda_valid" 1e-3; then
      echo "      ** FAILED"; status=1
      echo "      -> Lambda ($Lambda - $Lambda_valid)/$Lambda_valid"
    fi
  done
done
# --------------
# Bk(K) Lambda
# --------------
nsub_list=$nsub_list_orig
for K in e t T q P H; do
  nsub=`echo $nsub_list | gawk '{print $1}'`
  nsub_list=`echo $nsub_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
  Lambda_valid=1
  command="./lebesgue_tst B${k}[equispaced,bernstein] $K $nsub"
  echo "      $command"
  Lambda=`eval "$command 2>/dev/null| grep lebesgue | gawk '{print \\$2}'"`
  #echo "      Lambda=\"$Lambda\""
  if float_differ "$Lambda" "$Lambda_valid" 1e-3; then
      echo "      ** FAILED"; status=1
      echo "      -> Lambda ($Lambda - $Lambda_valid)/$Lambda_valid"
  fi
done

exit $status
