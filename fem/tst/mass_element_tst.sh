#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"${TOP_SRCDIR}/fem/lib"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"
. "${TOP_SRCDIR}/config/float_compare.sh"

status=0

k=5
tmp="/tmp/vdm-tst-$$"
# Pk(etT): cond(mass):
meas_valid_list="1 0.5 0.1666666666666667 4 1 8"
#    e t T q P H
#  / equispaced     warburton 	          bernstein	     raw-dubiner sherwin
cond_valid_list="
   23.6484277730    11.989825704979             462.            1.	  355.24
  103.3656695987    61.544017658754             792.           66      113530.75
  409.8688107399   362.138789243354            1287.          286.	  1.
  559.2481448719   143.755920435796          213444.            1.	  1.
 2444.4355908095   737.902044912678          365904.           66.	  1.
13225.339464511	 1723.608430084185         98611128.8           1.	  1.
"
approx_list="
	P${k}[equispaced,dubiner]
	P${k}[warburton,dubiner]
	B${k}
	D${k}
	S${k}
"
for K in e t T q P H; do
  meas_valid=`echo $meas_valid_list | gawk '{print $1}'`
  meas_valid_list=`echo $meas_valid_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for approx in $approx_list; do
    cond_valid=`echo $cond_valid_list | gawk '{print $1}'`
    cond_valid_list=`echo $cond_valid_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
    if test $approx = S${k}; then
      continue; # not yet TODO
    fi
    if test $approx = S${k} && test $K = q -o $K = T -o $K = P -o $K = H; then
      continue; # not yet TODO
    fi
    command="./mass_element_tst $approx $K"
    echo "      $command"
    eval "$command > $tmp.txt 2>/dev/null"
    cond=`grep cond $tmp.txt | gawk '{print $2}'`
    meas=`grep meas $tmp.txt | gawk '{print $2}'`
    if test $approx != "D${k}"; then
      if float_differ "$cond" "$cond_valid" 1e-2 || float_differ "$meas" "$meas_valid" 1e-3; then
 	echo "      ** FAILED";
 	status=1;
         echo "      cond -> ($cond - $cond_valid)/$cond_valid"
         echo "      meas -> ($meas - $meas_valid)/$meas_valid"
      fi
    else
      if float_differ "$cond" "$cond_valid" 1e-3; then
 	echo "      ** FAILED";
 	status=1;
         echo "      cond -> ($cond - $cond_valid)/$cond_valid"
      fi
    fi
  done
done
rm -f $tmp.txt
exit $status
