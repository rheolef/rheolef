///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// automatic differentiation with respect to space variables in R^3
// i.e. compute both a quantiy of its gradient
//
// author: Pierre.Saramito@imag.fr
//
// date: 26 september 2017
//
#include "rheolef/ad3.h"
using namespace rheolef;
using namespace std;

template<class T>
T f (const point_basic<T>& x) { 
  return sqr(x[0]) + x[0]*x[1] + 1/x[2];
}

int main(int argc, char**argv) {
  point x;
  x[0] = (argc > 1)  ? atof(argv[1]) : 1;
  x[1] = (argc > 2)  ? atof(argv[2]) : 1;
  x[2] = (argc > 3)  ? atof(argv[3]) : 1;
  point_basic<ad3> x_ad = ad3::point (x);
  ad3 y_ad = f(x_ad);
  cout << "y="<<y_ad <<endl;
  cout << "grad="<<y_ad.grad() <<endl;
}
