#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"${TOP_SRCDIR}/fem/lib"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

#echo "      no yet (skiped)"
#exit 0

for hat_K in t q; do
  if test $hat_K = t; then ndx=8; else ndx=12; fi
  idx=0;
  while test $idx -lt $ndx; do
    command="./interpolate_RTk_tst $hat_K $idx >/dev/null 2>/dev/null"
    run $command
    if test $? -ne 0; then status=1; fi
    idx=`expr $idx + 1`
  done
done

exit $status
