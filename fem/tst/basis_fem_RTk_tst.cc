///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check that the RTk basis is the Lagrange one for dofs:
//	dof_i(basis_j) = delta_ij
//
// author: Pierre.Saramito@imag.fr
//
// date: 19 september 2017
//
#include "rheolef/basis.h"
using namespace rheolef;
using namespace std;
#include "eigen_util.h"
using namespace Eigen;

struct phi {
  phi (basis b, reference_element hat_K, size_t idof)
   : _b(b), _hat_K(hat_K), _idof(idof), _phi(), _hat_x_prec(std::numeric_limits<Float>::max()) {}
  point operator() (const point& hat_x) const {
    if (hat_x != _hat_x_prec) {
      _b.evaluate (_hat_K, hat_x, _phi);
      _hat_x_prec = hat_x;
    }
    return _phi[_idof];
  }
  basis           _b;
  reference_element  _hat_K;
  size_t             _idof;
  mutable Matrix<point,Dynamic,1> _phi;
  mutable point _hat_x_prec;
};

int main(int argc, char**argv) {
  string approx = (argc > 1) ?      argv[1]    : "RT0";
  char   t      = (argc > 2) ?      argv[2][0] : 't';
  Float  tol    = (argc > 3) ? atof(argv[3])   : sqrt(std::numeric_limits<Float>::epsilon());
  basis b (approx);
  reference_element hat_K;
  hat_K.set_name(t);
  size_t loc_ndof = b.ndof(hat_K);
  Matrix<Float,Dynamic,1> dof;
  Matrix<Float,Dynamic,Dynamic> vdm (loc_ndof, loc_ndof);
  for (size_t loc_jdof = 0; loc_jdof < loc_ndof; ++loc_jdof) {
    phi p(b,hat_K,loc_jdof);
    b.compute_dof (hat_K, p, dof);
    for (size_t loc_idof = 0; loc_idof < loc_ndof; ++loc_idof) {
      // vdm(i,j) = dof_i(pj)
      vdm(loc_idof, loc_jdof) = dof[loc_idof];
    }
  }
  Matrix<Float,Dynamic,Dynamic> id (loc_ndof, loc_ndof);
  id.setIdentity();
  Float err = (vdm-id).norm();
  cout << setprecision(numeric_limits<Float>::digits10)
       << "approx    " << b.name() << endl
       << "element   " << hat_K.name() << endl
       << "node      " << b.option().get_node_name() << endl
       << "raw_poly  " << b.option().get_raw_polynomial_name() << endl
       << "cond(vdm) " << cond(b.vdm(hat_K)) << endl
       << "error_id  " << err << endl
    ;
  int status = (err < tol) ? 0 : 1;
  if (status != 0) {
    cout << "  ** error_id > tol = " << tol << endl;
    // cout << "vdm=["<< endl << vdm << "];" << endl;
  }
  return status;
}
