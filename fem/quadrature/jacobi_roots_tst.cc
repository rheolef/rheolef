///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/jacobi.h"
#include "rheolef/jacobi_roots.h"
#include "rheolef/compiler.h"
#include <vector>
#include <limits>
#include <numeric>
#include <iostream>
#include <iomanip>
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) {
        size_t R    = argc > 1 ? atoi(argv[1]) : 5;
        Float alpha = argc > 2 ? atof(argv[2]) : 0;
        Float beta  = argc > 3 ? Float(atof(argv[3])) : alpha;
        Float tol   = argc > 4 ? Float(atof(argv[4])) : sqrt(numeric_limits<Float>::epsilon());
	vector<Float> zeta (R);
	jacobi_roots (R, alpha, beta, zeta.begin());
	cout << setprecision (numeric_limits<Float>::digits10)
	     << "# jacobi roots\n"
	     << "# alpha = "<< alpha << endl
	     << "# beta  = "<< beta << endl
	     << "# R     = "<< R << endl
	     << "# r\tzeta\n";
	jacobi<Float> P (R, alpha, beta);
	Float err = 0;
	for (size_t r = 0; r < R; r++) {
	    cout << r << "\t" << zeta[r] << endl;
	    err = std::max (err, fabs(P(zeta[r])));
        }
	if (err >= tol) cerr << "err = " << err << endl;
        return err < tol ? 0 : 1;
}
