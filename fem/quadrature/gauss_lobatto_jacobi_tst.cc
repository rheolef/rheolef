///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/compiler.h"
#include "rheolef/gauss_jacobi.h"
#include "rheolef/gauss_chebyschev.h"
#include "rheolef/gauss_lobatto_jacobi.h"
#include "rheolef/gauss_jacobi_check.h"
#include <vector>
#include <iostream>
#include <iomanip>
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) {
	size_t R = argc > 1 ? atoi(argv[1]) : 5;
	Float alpha = argc > 2 ? atof(argv[2]) : 0;
	Float beta  = argc > 3 ? Float(atof(argv[3])) : alpha;
        Float tol   = argc > 4 ? Float(atof(argv[4])) : sqrt(numeric_limits<Float>::epsilon());
	if (R < 2) return 0;
	vector<Float> zeta(R), omega(R);
	gauss_lobatto_jacobi (R, alpha, beta, zeta.begin(), omega.begin());
        cout << setprecision (numeric_limits<Float>::digits10)
             << "# gauss_lobatto_jacobi" << endl
	     << "# R     = "<< R << endl
	     << "# alpha = "<< alpha << endl
	     << "# beta  = "<< beta << endl
	     << "# r\tzeta\tomega" << endl;
	for (size_t r = 0; r < R; r++)
	    cout << r << "\t" << zeta[r] << "\t" << omega[r] << endl;
	Float err = 0;
	if (floor(alpha) == alpha && floor(beta) == beta) {
	    size_t p = static_cast<int>(alpha);
	    size_t q = static_cast<int>(beta);
 	    size_t Rtilde = R + int((p+q+1)/2);
            vector<Float> x(Rtilde), w(Rtilde);
	    gauss_jacobi (Rtilde, 0, 0, x.begin(), w.begin());
	    err = gauss_jacobi_check (R, p, q, zeta.begin(), omega.begin(),
		Rtilde, x.begin(), w.begin(), 2*R-3);
	} else if (fabs(alpha-floor(alpha)) == Float(0.5) && fabs(beta-floor(beta)) == Float(0.5)) {
	    size_t p = static_cast<int>(alpha+0.5);
	    size_t q = static_cast<int>(beta+0.5);
            size_t Rtilde = R + int((p+q+1)/2);
	    vector<Float> x(Rtilde), w(Rtilde);
	    gauss_chebyschev (Rtilde, x.begin(), w.begin());
	    err = gauss_jacobi_check (R, p, q, zeta.begin(), omega.begin(),
			Rtilde, x.begin(), w.begin(), 2*R-3);
	} else {
	    cerr << "hybrid integer and half-integer jacobi parameters not checked" << endl;
	    exit (1);
	}
	if (err >= tol) cerr << "err = " << err << endl;
	return err < tol ? 0 : 1;
}
