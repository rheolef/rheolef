///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/compiler.h"
#include "rheolef/gauss_jacobi.h"
#include "rheolef/gauss_legendre_check.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include <numeric>
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) {
	size_t R  = argc > 1 ? atoi(argv[1]) : 5;
        Float tol = argc > 2 ? Float(atof(argv[2])) : sqrt(numeric_limits<Float>::epsilon());
	if (R == 0) return 0;
	vector<Float> zeta(R), omega(R);
	gauss_jacobi (R, 0, 0, zeta.begin(), omega.begin());
        cout << setprecision (numeric_limits<Float>::digits10)
             << "# gauss_legendre" << endl
	     << "# R = "<< R << endl
	     << "# r\tzeta\tomega" << endl;
	for (size_t r = 0; r < R; r++)
	    cout << r << "\t" << zeta[r] << "\t" << omega[r] << endl;
	Float err = gauss_legendre_check (R, zeta.begin(), omega.begin(), 2*R-1);
	if (err >= tol) cerr << "err = " << err << endl;
        return err < tol ? 0 : 1;
}
