///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito 
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/compiler.h"
#include <iterator>
template <class Iterator1, class Iterator2, class Size>
typename std::iterator_traits<Iterator1>::value_type
gauss_jacobi_check (Size R, Size p, Size q, Iterator1 zeta, Iterator2 omega, 
	Size Rtilde, Iterator1 x, Iterator2 w, Size order)
{
warning_macro ("gauss_jacobi_check...");
        typedef typename std::iterator_traits<Iterator1>::value_type T;
        T error = 0;
        for (Size k = 0; k <= order; k++) {
            T exact = 0, value = 0;
            for (Size s = 0; s < Rtilde; s++)
                exact += pow (x[s], k)*pow (1-x[s], p)*pow (1+x[s], q)*w[s];
            for (Size r = 0; r < R; r++)
                value += pow (zeta[r], k)*omega[r];
	    error = std::max(error, fabs (exact - value));
        }
warning_macro ("gauss_jacobi_check: done : error="<<error);
        return error;
}
