///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito 
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include <cmath>
#include <iterator>
template <class Size, class OutputIterator1, class OutputIterator2>
void gauss_radau_chebyschev (Size R, OutputIterator1 zeta, OutputIterator2 omega) {
        typedef typename std::iterator_traits<OutputIterator1>::value_type T;
	static T pi = acos(T(-1.));
      	zeta  [0] = 1;
      	omega [0] = pi/T(2.*R-1);
  	for (Size r = 1; r < R; r++) {
      	    zeta [r] = cos(2*r*pi/T(2.*R-1));
      	    omega[r] = 2*pi/T(2.*R-1);
	}
}
