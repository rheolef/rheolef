#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../.."}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0
#
# integer values
#
for R in 1 2 3 4 5 6 7 8; do
  for alpha in 0 1 2 3 4 5 6 7 8; do
    for beta in 0 1 2 3 4 5 6 7 8; do
        run "./gauss_jacobi_tst $R $alpha $beta >/dev/null 2>/dev/null"
	if test $? -ne 0; then status=1; fi
    done
  done
done
#
# half integer values
#
for R in 1 2 3 4 5 6 7 8; do
  for alpha in -0.5 0.5 1.5 2.5 3.5 4.5 5.5 6.5 7.5; do
    for beta in -0.5 0.5 1.5 2.5 3.5 4.5 5.5 6.5 7.5; do
        run "./gauss_jacobi_tst $R $alpha $beta >/dev/null 2>/dev/null"
	if test $? -ne 0; then status=1; fi
    done
  done
done
exit $status
