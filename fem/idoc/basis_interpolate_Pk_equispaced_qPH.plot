#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal cairolatex pdf color standalone
set output "basis_interpolate_Pk_equispaced_qPH.tex"

set log y
set size square
set xrange [0:10]
set yrange [1e-10:1e1]
set ytics (\
   '[r]{$1$}'        1, \
   '[r]{$10^{-5}$}'  1e-5, \
   '[r]{$10^{-10}$}' 1e-10, \
   '[r]{$10^{-15}$}' 1e-15)
set xlabel '[c]{$k$}' 
set  label '[l]{$P_k$/equispaced}' at graph 0.1, 0.3

plot \
'basis_interpolate_Pk_equispaced.gdat' \
  i 3 \
  u 1:2 \
  t '[r]{$\|u-\pi_k(u)\|_{L^2}$, $\widehat{K}=q$}' \
  w lp lc rgb '#ff0000' dt 1 pt 1, \
'basis_interpolate_Pk_equispaced.gdat' \
  i 3 \
  u 1:3 \
  t '[r]{$\|u-\pi_k(u)\|_{L^\infty}$, $\widehat{K}=q$}' \
  w lp lc rgb '#ff0000' dt 2 pt 2, \
'basis_interpolate_Pk_equispaced.gdat' \
  i 4 \
  u 1:2 \
  t '[r]{$P$}' \
  w lp lc rgb '#008800' dt 1 pt 1, \
'basis_interpolate_Pk_equispaced.gdat' \
  i 4 \
  u 1:3 \
  not '[r]{$P$}' \
  w lp lc rgb '#008800' dt 2 pt 2, \
'basis_interpolate_Pk_equispaced.gdat' \
  i 5 \
  u 1:2 \
  t '[r]{$H$}' \
  w lp lc rgb '#0000ff' dt 1 pt 1, \
'basis_interpolate_Pk_equispaced.gdat' \
  i 5 \
  u 1:3 \
  not '[r]{$H$}' \
  w lp lc rgb '#0000ff' dt 2 pt 2

#pause -1
