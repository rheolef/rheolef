#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal cairolatex pdf color standalone
set output "basis_interpolate_RTk_q.tex"

set size square
set key at graph 0.7, 0.98
set log y
set xrange [0:10]
set yrange [1e-11:1e4]
set xtics (\
   '[l]{$0$}'        0, \
   '[c]{$5$}'        5, \
   '[r]{$10$}'      10)
set ytics (\
   '[r]{$1$}'        1, \
   '[r]{$10^{-5}$}'  1e-5, \
   '[r]{$10^{-10}$}' 1e-10, \
   '[r]{$10^{-15}$}' 1e-15)
set xlabel '[c]{$k$}' 
set  label '[l]{\scriptsize $RT_k$, $\widehat{K}=q$}' at graph 0.52, 0.65
hat_K_index = 1

plot \
'basis_interpolate_RTk.gdat' \
  i hat_K_index \
  u 1:2 \
  t '[r]{\scriptsize $\|u-\pi_k(u)\|_{L^2}\ $}' \
  w lp lc rgb '#ff0000' dt 1 pt 1 ps 0.5, \
'basis_interpolate_RTk.gdat' \
  i hat_K_index \
  u 1:3 \
  t '[r]{\scriptsize $\|u-\pi_k(u)\|_{L^\infty}$}' \
  w lp lc rgb '#ff0000' dt 2 pt 2 ps 0.5, \
'basis_interpolate_RTk.gdat' \
  i hat_K_index \
  u 1:4 \
  t '[r]{\scriptsize $\|\mathrm{div}(u-\pi_k(u))\|_{L^2}\ $}' \
  w lp lc rgb '#0000ff' dt 1 pt 1 ps 0.5, \
'basis_interpolate_RTk.gdat' \
  i hat_K_index \
  u 1:5 \
  t '[r]{\scriptsize $\|\mathrm{div}(u-\pi_k(u))\|_{L^\infty}$}' \
  w lp lc rgb '#0000ff' dt 2 pt 1 ps 0.5

#pause -1
