#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal cairolatex pdf color standalone
set output "warburton-t-11.tex"

set size ratio -1

set xrange [-0.1:1.1]
set yrange [-0.1:1.1]
set arrow from 0,0 to 1,0 nohead lc 0
set arrow from 1,0 to 0,1 nohead lc 0
set arrow from 0,1 to 0,0 nohead lc 0
set noborder
set noxtics
set noytics
plot \
'warburton-t-11.gdat' \
  notitle \
  w p pt 7 ps 0.8 lc rgb "#ff0000" 

#pause -1
