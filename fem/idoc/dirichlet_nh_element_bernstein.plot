#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal cairolatex pdf color standalone
set output "dirichlet_nh_element_bernstein.tex"

set size square
set key at graph 0.9, 0.98
set log y
set xrange [0:30]
set yrange [1e-16:1e5]
set ytics (\
   '[r]{$1$}'        1, \
   '[r]{$10^{-5}$}'  1e-5, \
   '[r]{$10^{-10}$}' 1e-10, \
   '[r]{$10^{-15}$}' 1e-15)
set xlabel '[c]{$k$}' 
set  label '[l]{\scriptsize $P_k$/Lagrange, $\widehat{K}=e$}' at graph 0.02, 0.09

plot \
'dirichlet_nh_element_bernstein.gdat' \
  i 0 \
  u 1:2 \
  t '[r]{\scriptsize $\widehat{K}=e$, $\|u-\pi_k(u)\|_{L^2}\ $}' \
  w lp lc rgb '#ff0000' dt 1 pt 1 ps 0.5, \
'dirichlet_nh_element_bernstein.gdat' \
  i 0 \
  u 1:3 \
  t '[r]{\scriptsize $\|u-\pi_k(u)\|_{L^\infty}$}' \
  w lp lc rgb '#ff0000' dt 2 pt 2 ps 0.5, \
'dirichlet_nh_element_bernstein.gdat' \
  i 1 \
  u 1:2 \
  t '[r]{\scriptsize $\widehat{K}=t$, $\|u-\pi_k(u)\|_{L^2}\ $}' \
  w lp lc rgb '#008800' dt 1 pt 1 ps 0.5, \
'dirichlet_nh_element_bernstein.gdat' \
  i 1 \
  u 1:3 \
  t '[r]{\scriptsize $\|u-\pi_k(u)\|_{L^\infty}$}' \
  w lp lc rgb '#008800' dt 2 pt 2 ps 0.5, \
'dirichlet_nh_element_bernstein.gdat' \
  i 2 \
  u 1:2 \
  t '[r]{\scriptsize $\widehat{K}=q$, $\|u-\pi_k(u)\|_{L^2}\ $}' \
  w lp lc rgb '#0000ff' dt 1 pt 1 ps 0.5, \
'dirichlet_nh_element_bernstein.gdat' \
  i 2 \
  u 1:3 \
  t '[r]{\scriptsize $\|u-\pi_k(u)\|_{L^\infty}$}' \
  w lp lc rgb '#0000ff' dt 2 pt 2 ps 0.5

#pause -1
