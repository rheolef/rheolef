## process this file with automake to produce Makefile.in
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
include ../../config/am_options.mk
include ${top_builddir}/config/config.mk

docdir = $(prefix)/share/doc/@doc_dir@
exampledir = $(docdir)/examples

# -----------------------------------------------------------------------------
# documentation: internal specs
# -----------------------------------------------------------------------------
PAPER_TEX =                             \
	internals_basis.tex			

TEX_INC =

PLOT_SRC = \
	warburton-t-11.plot \
	warburton-T-7.plot  \
	lebesgue_equispaced.plot \
	lebesgue_equispaced_qPH.plot \
	lebesgue_warburton.plot \
	lebesgue_warburton_qPH.plot \
	basis_interpolate_Pk_equispaced.plot \
	basis_interpolate_Pk_equispaced_qPH.plot \
	basis_interpolate_Pk_warburton.plot \
	basis_interpolate_Pk_warburton_qPH.plot \
	basis_interpolate_Pk_lagrange_e.plot \
	basis_interpolate_Pk_lagrange_t.plot \
	basis_equispaced_P10_e.plot \
	basis_warburton_P10_e.plot \
	cond_vdm_Pk_equispaced_monom.plot \
	cond_vdm_Pk_equispaced_monom_qPH.plot \
	cond_vdm_Pk_equispaced_bernstein.plot \
	cond_vdm_Pk_equispaced_bernstein_qPH.plot \
	cond_vdm_Pk_equispaced_dubiner.plot \
	cond_vdm_Pk_equispaced_dubiner_qPH.plot \
	cond_vdm_Pk_warburton_monom.plot \
	cond_vdm_Pk_warburton_monom_qPH.plot \
	cond_vdm_Pk_warburton_bernstein.plot \
	cond_vdm_Pk_warburton_bernstein_qPH.plot \
	cond_vdm_Pk_warburton_dubiner.plot \
	cond_vdm_Pk_warburton_dubiner_qPH.plot \
	cond_mass_element_Pk_equispaced.plot \
	cond_mass_element_Pk_equispaced_qPH.plot \
	cond_mass_element_Pk_warburton.plot \
	cond_mass_element_Pk_warburton_qPH.plot \
	cond_mass_element_Pk_monomial.plot \
	cond_mass_element_Pk_monomial_qPH.plot \
	cond_mass_element_Pk_bernstein.plot \
	cond_mass_element_Pk_bernstein_qPH.plot \
	cond_mass_element_Pk_dubiner.plot \
	cond_mass_element_Pk_dubiner_qPH.plot \
	dirichlet_nh_element_lagrange_e.plot  \
	dirichlet_nh_element_lagrange_t.plot  \
	dirichlet_nh_element_lagrange_q.plot \
	dirichlet_nh_element_bernstein.plot \
	basis_interpolate_RTk_t.plot \
	basis_interpolate_RTk_q.plot \
	basis_interpolate_RTk_upperT.plot \
	basis_interpolate_RTk_H.plot \
	basis_interpolate_RTk_h_t.plot \
	basis_interpolate_RTk_h_q.plot \
	basis_interpolate_RTk_div_h_t.plot \
	basis_interpolate_RTk_div_h_q.plot 

GDAT = \
	warburton-t-11.gdat \
	warburton-T-7.gdat \
	lebesgue.gdat \
	basis_interpolate_Pk_equispaced.gdat \
	basis_interpolate_Pk_warburton.gdat \
	basis_interpolate_Pk_lagrange_e.gdat \
	basis_interpolate_Pk_lagrange_t.gdat \
	basis_equispaced_P10_e.gdat \
	basis_warburton_P10_e.gdat \
	cond_vdm_Pk_equispaced_monom.gdat \
	cond_vdm_Pk_equispaced_bernstein.gdat \
	cond_vdm_Pk_equispaced_dubiner.gdat \
	cond_vdm_Pk_warburton_monom.gdat \
	cond_vdm_Pk_warburton_bernstein.gdat \
	cond_vdm_Pk_warburton_dubiner.gdat \
	cond_mass_element_Pk_equispaced.gdat \
	cond_mass_element_Pk_warburton.gdat \
	cond_mass_element_Pk_monomial.gdat  \
	cond_mass_element_Pk_bernstein.gdat  \
	cond_mass_element_Pk_dubiner.gdat \
	dirichlet_nh_element_lagrange_e.gdat  \
	dirichlet_nh_element_lagrange_t.gdat  \
	dirichlet_nh_element_lagrange_q.gdat \
	dirichlet_nh_element_bernstein.gdat \
	basis_interpolate_RTk.gdat \
	basis_interpolate_RTk_h_t.gdat \
	basis_interpolate_RTk_h_q.gdat

FIG_SRC = 		\
	trace-n-fig.fig

PNG_SRC = 		\
	hermite.jpg	\
	morley.jpg	\
	argyris.jpg	\
	bell.jpg

# dependencies:
PAPER_DEPEND = 		\
	$(TEX_INC) 	\
	$(PNG_SRC) 	\
	$(FIG_SRC:.fig=.pdf) 	\
	$(PLOT_SRC:.plot=.pdf) \
	version.tex

$(PAPER_TEX:.tex=.pdf): $(PAPER_DEPEND)

EXTRA_DIST =                    \
	Makefile.am 		\
	$(PAPER_TEX)		\
	$(PAPER_TEX:.tex=-autobib.bib)		\
	$(TEX_INC) 		\
        $(PLOT_SRC)		\
        $(GDAT)			\
	$(FIG_SRC)	 	\
        $(PNG_SRC)		\
	cond_vdm_Pk_gdat.sh	\
	cond_mass_element_Pk_gdat.sh	\
	lebesgue_gdat.sh	\
	basis_interpolate_gdat.sh \
	basis_interpolate_hdiv_gdat.sh \
	dirichlet_nh_element_gdat.sh 

CLEANFILES = *.ii *.ii_pic *.da *.gcov *.[oa1359] 		\
	*.[1-9]rheolef *.man[1-9] 				\
	cxx_repository/* KCC_files/* KCC_pic/* 			\
	.deps/*.Q .deps/*.R 					\
	stamp-symlink.in version.tex

# now added to git, for simplicity : otherwise, requires ginac to be installed
CVSIGNORE = 			\
	Makefile.in		\
        ${PLOT_SRC:.plot=.pdf}

WCIGNORE = 			\
	$(GDAT)			

LICENSEIGNORE = 		\
	$(PAPER_TEX:.tex=-autobib.bib)		\
	$(PNG_SRC) 		\
	$(GDAT)			\
	$(FIG_SRC) 		


# ----------------------------------------------------------------------------
# latex rules
# ----------------------------------------------------------------------------
if HAVE_TEX_DOCUMENTATION
dvi-local: $(PAPER_TEX:.tex=.pdf)
else
dvi-local: 
endif

AUTOBIB = autobib

%-autobib.bib %.pdf: %.tex
	bash ${top_srcdir}/config/check-non-ascii.sh ${abs_top_builddir}/config/check_non_ascii ${srcdir}
	x=$*; rm -f $${x}.aux $${x}.*.aux $${x}.bbl $${x}.toci $${x}.cnd $${x}.ind $${x}.idx $${x}.nav
	if test x"${PDFLATEX}" != x"" -a x"${BIBTEX}" != x""; then              \
	     TEXINPUTS=":.:$(srcdir)/:$(top_srcdir)/fem/idoc/:$(top_srcdir)/doc/usrman:$$TEXINPUTS"; \
	     BIBINPUTS=":.:$(srcdir)/:$(top_srcdir)/fem/idoc/:$(top_srcdir)/doc/usrman:$$BIBINPUTS"; \
	     export TEXINPUTS BIBINPUTS; \
	  $(PDFLATEX) $* \
	  && (x=$*; \
	  for f in `ls $${x}.*aux`; do \
	    g=`expr $$f : '\(.*\)\.aux' `; \
	    ncite=`grep '\\citation{' $$f | wc -l`; \
	    nrefs=`grep 'bibcite' $$f | wc -l`; \
	    echo "ncite=$$ncite nrefs=$$nrefs"; \
	    if test $$ncite -gt 0 -a $$nrefs -eq 0; then \
	      status=0; \
	      echo "$(AUTOBIB) $$g"; \
	      mv $(srcdir)/$*-autobib.bib $(srcdir)/$*-autobib.bib.old 2>/dev/null || \
		 touch $(srcdir)/$*-autobib.bib.old; \
              $(AUTOBIB) $$g; status=$$?; \
	      if test -f autobib.bib; then mv autobib.bib $(srcdir)/$*-autobib.bib; fi; \
	      if test $$status -ne 0; then \
		echo "autobib failed"; \
	        mv $(srcdir)/$*-autobib.bib.old $(srcdir)/$*-autobib.bib 2>/dev/null; \
	      fi; \
	      echo "strip bibtex url=..."; \
	      sed -e 's/^[    ]*url[  ]*=/omit_url=/' < $(srcdir)/$*-autobib.bib >  $(srcdir)/$*-autobib.bib.new && \
	      mv $(srcdir)/$*-autobib.bib.new $(srcdir)/$*-autobib.bib; \
	      echo "bibtex $$g"; \
	      bibtex $$g | tee $$g.biblog; \
	      if grep -i warning $$g.biblog >/dev/null; then exit 1; else true; fi; \
	      if grep -i error   $$g.biblog >/dev/null; then exit 1; else true; fi; \
	    fi; \
	  done) \
	  && $(PDFLATEX) $* \
	  && $(PDFLATEX) $* \
	  && $(PDFLATEX) $* \
	  && (cat < $*.log 2>/dev/null | grep Warning   		\
	      | grep -aiv 						\
		-e 'hyperref' 		 			 	\
		-e 'float' 						\
		-e 'You have requested'					\
		-e 'Foreign command' 					\
		-e 'Font Warning' 					\
		-e 'pagebackref'	 			 	\
		-e 'marginpar' 		 			 	\
		-e 'minitoc' 		 			 	\
		-e 'has changed' 	 			 	\
              | tee $*-warnings.log || true)				\
          && test `cat < $*-warnings.log | wc -l` -eq 0			\
	  && if test x"${GS}" != x""; then				\
	       command="${GS} -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dPrinted=false -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$*-default.pdf $*.pdf";  		\
	          echo "$${command}"; 					\
	          eval "$${command}" 					\
	       && mv $*-default.pdf $*.pdf; 				\
	     fi  							\
	  && if test x"${PDFFONTS}" != x""; then			\
	       command="test \`${PDFFONTS} $*.pdf | grep 'Type 3' | wc -l\` -eq 0"; \
	          echo "$${command}"; 					\
	          eval "$${command}";					\
	     fi; 							\
	else								\
	  true;								\
	fi

version.tex: $(top_srcdir)/configure.ac ${top_srcdir}/VERSION
	@sh ${top_srcdir}/config/mk_version.sh $(srcdir) $(top_srcdir) `cat $(top_srcdir)/VERSION` internals_basis $(exampledir)

.plot.pdf:
	GNUPLOT_LIB=$(srcdir) bash $(top_srcdir)/config/plot2pdf.sh -gnuplot5 $<
	rm -f $(srcdir)/$*.glog

