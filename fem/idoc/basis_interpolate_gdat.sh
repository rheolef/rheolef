#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
approx="P"
raw_poly="dubiner"
kmax=${2-"50"}
tmp="/tmp/tmp$$"

echo "# basis_interpolate_tst"
for K in t; do
#for K in e t T q P H; do
  for node in fekete; do
    echo "# approx   ${approx}k"
    echo "# node     $node"
    echo "# raw_poly $raw_poly"
    echo "# element $K"
    echo "# k err_l2 err_linf"
    k=0
    while test $k -le $kmax; do
      ./basis_interpolate_tst $approx$k $K $node $raw_poly > $tmp.txt 2>/dev/null
      err_linf=`grep err_linf $tmp.txt | gawk '{print $2}'`
      err_l2=`grep err_l2 $tmp.txt | gawk '{print $2}'`
      echo "$k $err_l2 $err_linf"
      k=`expr $k + 1`
    done
    echo; echo
  done
done
rm -f $tmp.txt
