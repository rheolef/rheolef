#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal cairolatex pdf color standalone
set output "basis_warburton_P10_e.tex"

gdat = "basis_warburton_P10_e.gdat"
set colors classic
set size square

plot \
  gdat u 1:2 t "L1" w l,\
  gdat u 1:3 t "L2" w l,\
  gdat u 1:4 t "L3" w l,\
  gdat u 1:5 t "L4" w l,\
  gdat u 1:6 t "L5" w l,\
  gdat u 1:7 t "L6" w l,\
  gdat u 1:8 t "L7" w l,\
  gdat u 1:9 t "L8" w l,\
  gdat u 1:10 t "L9" w l,\
  gdat u 1:11 t "L10" w l,\
  gdat u 1:12 t "L11" w l

#pause -1 "<return>"
