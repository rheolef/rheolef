#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal cairolatex pdf color standalone
set output "warburton-T-7.tex"

x0 = 0
y0 = 0
z0 = 0

x1 = 1
y1 = 0
z1 = 0

x2 = 0
y2 = 1
z2 = 0

x3 = 0
y3 = 0
z3 = 1
d  = 0.1
set xrange [-d:1+d]
set yrange [-d:1+d]
set zrange [-d:1+d]
set view equal xyz # equal scales
set xyplane at -d

set nokey
set arrow from x0,y0,z0 to x1,y1,z1 nohead lc 0
set arrow from x0,y0,z0 to x2,y2,z2 nohead lc 0
set arrow from x1,y1,z1 to x2,y2,z2 nohead lc 0
set arrow from x0,y0,z0 to x3,y3,z3 nohead lc 0
set arrow from x1,y1,z1 to x3,y3,z3 nohead lc 0
set arrow from x2,y2,z2 to x3,y3,z3 nohead lc 0
set noborder
set noxtics
set noytics
set noztics
splot \
'warburton-T-7.gdat' \
  notitle \
  w p pt 7 ps 0.4 lc rgb "#ff0000"

#pause -1 "<retour>"
