#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
nsub=${1-"20"}
poly=${2-"dubiner"}
kmax=15
for K in e t T q P H; do
   echo "# approx   Pk"
   echo "# nsub     $nsub"
   echo "# raw_poly $poly"
   echo
   echo "# element  $K"
   echo "# k  equispaced warburton"
   k=0
   while test $k -le $kmax; do
      Lambda_e=`./lebesgue_tst P$k $K equispaced $poly $nsub | grep lebesgue | gawk '{print $2}'`
      Lambda_w=`./lebesgue_tst P$k $K warburton  $poly $nsub | grep lebesgue | gawk '{print $2}'`
      echo "$k	$Lambda_e  $Lambda_w"
      k=`expr $k + 1`
   done
   echo
   echo
done
