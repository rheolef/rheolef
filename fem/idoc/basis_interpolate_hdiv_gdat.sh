#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
approx="RT"
kmax=${2-"12"}
tmp="/tmp/tmp$$"

echo "# basis_interpolate_hdiv_tst"
for K in t q T P H; do
    echo "# approx  ${approx}k"
    echo "# element $K"
    echo "# k err_l2 err_linf err_div_l2 err_div_linf"
    k=0
    while test $k -le $kmax; do
      ./basis_interpolate_hdiv_tst $approx$k $K > $tmp.txt 2>/dev/null
      err_l2=`grep err_l2 $tmp.txt | gawk '{print $2}'`
      err_linf=`grep err_linf $tmp.txt | gawk '{print $2}'`
      err_div_l2=`grep err_div_l2 $tmp.txt | gawk '{print $2}'`
      err_div_linf=`grep err_div_linf $tmp.txt | gawk '{print $2}'`
      echo "$k $err_l2 $err_linf $err_div_l2 $err_div_linf"
      k=`expr $k + 1`
    done
    echo; echo
done
rm -f $tmp.txt
