# ---  Rheolef ci templates ---
# 
# This file contains common definitions and templates for
# continuous integration job of rheolef project.
#
# Usage :
# add in .gitlab-ci.yml :
# include: <path-to-this-file>/ci-rheolef-templates.yml


variables:
  # By default we allow builds to run in parallel, but certain
  # configurations may require non-parallel builds
  allow_parallel_build: 1

stages:
  # --- Docker build stage ---
  # The first stage contains jobs used to build
  # docker images 'ready to use' for a Rheolef build/install.
  # Requirement for jobs in this stage :
  # - should build and push a docker image to rheolef project registry
  # - should be allowed to failed (in order to avoid blocking of last stage jobs)
  # - should run only when commit message contains [docker-build]
  # - use Dockerfile from ci_gitlab/dockerfiles/<image-name>
  #
  # Templates for these jobs : .docker-build (see below).
  - docker-build
  # --- Build stage ---
  # jobs run on images generated in previous stage, available in rheolef registry:
  # https://gricad-gitlab.univ-grenoble-alpes.fr/rheolef/rheolef/container_registry
  # - configure and build rheolef
  # - run tests
  # -install rheolef
  #
  # Templates for these jobs : .rheolef-build (see below).
  - configure
  - build
  - test
  - install


# Define rules used to create (or not) CI jobs
# Each rule is checked and if the condition is not true, the next one is checked, until the end. 
workflow:
  rules:
    # No CI when commit message starts with wip
    - if: $CI_COMMIT_MESSAGE =~ /^wip.*$/
      when: never
    # Other cases: delegates the choice of the behavior to each job (see templates)
    - if: '$CI_COMMIT_BRANCH' 
  
# --- Templates definitions ---
# Each template can be used in CI jobs, with the keyword 'extends'.

.docker-rules:
   rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[docker-build\]/
      when: always
#    - if: $CI_COMMIT_BRANCH == "master"
#      when: always
    - if: $CI_COMMIT_TAG
      when: always
    - when: never

.docker-manual-rules:
    rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[docker-build\]/
      when: manual
    - when: never


# -- Template used to describe docker-build jobs --
#
# - build and push a docker image into the project registry
#   image name : $CI_REGISTRY_IMAGE/$IMAGE_NAME
# - should be allowed to failed (in order to avoid blocking of the jobs in the next stages)
# - should run only when commit message contains [docker-build]
# - use Dockerfile from ci_gitlab/dockerfiles/<IMAGE_NAME>
# - will be tagged <IMAGE_NAME>:latest.
# Based on Kaniko stuff. For details, see https://docs.gitlab.com/ee/ci/docker/using_kaniko.html.
.docker-create-image:
  # Created if the commit message starts with [docker-build] or
  # when the master branch is updated.
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    GIT_STRATEGY: clone
  stage: docker-build
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/ci_gitlab/dockerfiles/$IMAGE_NAME/Dockerfile --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest 

.docker-build:
  extends:
    - .docker-rules
    - .docker-create-image

.docker-manual:
  extends:
    - .docker-manual-rules
    - .docker-create-image

# Use these rules for jobs that must be created:
# - when updating master branch
# - when the commit message contains [all-jobs]
# - when commiting a tag
.full-ci-rules:
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
    - if: $CI_COMMIT_TAG
      when: always
    - if: $CI_COMMIT_MESSAGE =~ /\[all-jobs\]/
      when: always
    - when: never
    
# -- Template used to define jobs to build rheolef --
# - Pull an image (possibly from rheolef registry)
#   named  IMAGE_NAME
# - Run for all branches and for all push
# - bootstrap/configure rheolef.
# - use artifacts to keep results for next stage.
.rheolef-configure:
  image: $IMAGE_NAME
  stage: configure
  script:
    - "sh ci_gitlab/configure_rheolef.sh"
  artifacts: # keep build dir for next jobs
    paths:
      - build
    expire_in: 2 days  

# -- Template used to define jobs to build rheolef --
# - Pull an image (possibly from rheolef registry)
#   named  IMAGE_NAME
# - Run for all branches and for all push
# - Use 'configure' stage artifacts
# - Build rheolef and run make check
.rheolef-build:
  image: $IMAGE_NAME
  variables:
    GIT_STRATEGY: none # do not clean git repo to keep configure results !
  stage: build
  script:
    - cd $CI_PROJECT_DIR/build
    - make -j `nproc --all`
  artifacts: # keep build dir for next jobs
    paths:
      - build
    expire_in: 2 days  


.rheolef-test:
  image: $IMAGE_NAME
  stage: test
  variables:
    GIT_STRATEGY: none # do not clean git repo to keep configure results !
  script:
    - cd $CI_PROJECT_DIR/build
    - make check
    
# 
.rheolef-ready:
  stage: install
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    #GIT_STRATEGY: none
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/ci_gitlab/dockerfiles/$DOCKERFILE_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE/rheolef-$IMAGE_NAME:$CI_COMMIT_SHORT_SHA --destination $CI_REGISTRY_IMAGE/rheolef-$IMAGE_NAME:latest --cleanup
  when: manual

