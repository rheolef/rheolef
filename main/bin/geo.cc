//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// author: Pierre.Saramito@imag.fr
// date: 16 september 2011

namespace rheolef {
/**
@commandfile geo plot a finite element mesh 
@addindex command `geo`
@addindex plotting mesh

Synopsis
========

    geo [options] file[.geo[.gz]]

Description
===========

Plot or upgrade a finite element mesh.

Examples
========

Plot a mesh:

        geo square.geo
        geo box.geo
        geo box.geo -full

Plot a mesh into a file:

        geo square.geo -image-format png

Convert from a old geo file format to the new one:

        geo -upgrade - < square-old.geo > square.geo

See below for the geo file format specification.
The old file format does not contains edges and faces connectivity in 3d geometries,
or edges connectivity in 2d geometries.
The converter add it automatically into the upgraded file format.
Conversely, the old file format is useful when combined with a
translator from another file format that do not provides edges and faces connectivity.

Input file specification
========================

filename
>       Specifies the name of the file containing the input mesh.
>       The ".geo" suffix extension is assumed.

`-`
>       Read mesh on standard input instead on a file.

`-name` *string*
>       When mesh comes from standard input, the mesh name
>       is not known and is set to "output" by default.
>       This option allows one to change this default.
>       This option is useful when dealing with output formats (graphic, format conversion)
>       that creates auxiliary files, based on this name.

@addindex RHEOPATH environment variable
`-I`@e dir \n
`-I`  *dir*
>       Add *dir* to the Rheolef file search path.
>       This mechanism initializes a search path given by the environment variable `RHEOPATH`.
>       If the environment variable `RHEOPATH` is not set, the default value is the current directory.
`-check`
>       Check the mesh file: numbering, bounds and that for all element, its orientation is positive.

Input format options
====================

@addindex command `bamg`
@addindex file format `.bamg` mesh
@addindex file format `.vtk`  mesh

`-if` *format* \n
`-input-format`  *format*
>       Load mesh in a given file format.
>       Supported input formats are: `geo`, `bamg`, `vtk`.

Render specification
====================

@addindex graphic render
@addindex command `gnuplot`

`-gnuplot`
>       Use the `gnuplot` tool.
>       This is the default for 1D geometry.

@addindex command `paraview`

`-paraview`
>       Use the `paraview` tool.
>       This is the default for 2D and 3D geometries.

Render options
==============

`-[no]lattice`
> 	When using a high order geometry, the lattice inside any element appears.
>	Default is on.

`-[no]full`
> 	All internal edges appears, for 3d meshes.
>	Default is off.

`-[no]fill`
>       Fill mesh faces using light effects, when available.

@addindex stereo 3D rendering
`-[no]stereo`
>       Rendering mode suitable for red-blue anaglyph 3D stereoscopic glasses.
>       Option only available with `paraview`.

`-[no]shrink`
>       shrink elements (with `paraview` only).

`-[no]cut`
>       cut by plane and clip (with `paraview` only).
 
`-[no]showlabel`
>       Show or hide labels, boundary domains and various annotations.
>       By default, domains are showed with a specific color.

`-round [*float*]`
>       Round the input up to the specified precision.
>       This option, combined with `-geo`, leads to a round filter.
>       Useful for non-regression test purpose, in order
>       to compare numerical results between files with a limited precision,
>       since the full double precision is machine-dependent.

Output format options
=====================

`-geo`
>       output mesh on standard output stream in geo text file format,
>       instead of plotting it.

`-upgrade`
>       Convert from a old geo file format to the new one.

@addindex command `gmsh`
@addindex file format `.gmsh` mesh

`-gmsh`
>       Output mesh on standard output stream in gmsh text file format,
>       instead of plotting it.

@addindex image file format
@addindex file format `.png` image
@addindex file format `.jpg` image
@addindex file format `.gif` image
@addindex file format `.tif` image
@addindex file format `.ppm` image
@addindex file format `.bmp` image
@addindex file format `.pdf` image
@addindex file format `.eps` image
@addindex file format `.ps`  image
@addindex file format `.svg` image

`-image-format` *string*
>       The argument is any valid image format, such as
>       bitmap `png`, `jpg`, `gif`, `tif`, `ppm`, `bmp`
>       or vectorial `pdf`, `eps`, `ps`, `svg`
>	image file formats.
>	this option can be used with the `paraview` and the `gnuplot` renders.
>	The output file is e.g. basename.`png` when basename
>	is the name of the mesh, or can be set with the `-name` option.

`-resolution` *int* *int*
>       The two arguments represent a couple of sizes, for the image resolution,
>       e.g. 1024 and 768 for a 1024x768 image size.
>	This option can be used together with the `-image-format`
>	for any of the bitmap image formats.
>	This option requires the `paraview` render.

Others options
==============

@addindex mesh subdividing

`-subdivide` *int*
>	Subdivide each edge in *k* parts,
>	where *k* is the prescribed argument.
>	The new vertices are numbered so that they coincide with the P@e k Lagrange nodes.
>	It can be combined with the `-geo` option to get the subdivided mesh.
>	In that case, default value is 1, i.e. no subdividing.
>	It can also be combined with a graphic option, such that `-gnuplot` or `paraview`:
>  	When dealing with a curved high order geometry, 
>  	*k* corresponds to the number of points per edge used to draw a curved element. 
>	In that case, this option is activated by default and value is the curved mesh order.

@addindex command `bamg`
`-add-boundary`
>       Check for a domain named "boundary". If this domain
> 	does not exists, extract the boundary of the geometry
>	and append it to the domain list.
>	This command is useful for mesh converted from 
>	generators, as `bamg`, that cannot have more than
>	one domain specification per boundary edge.

`-rz` \n
`-zr`
>	Specifies the coordinate system. Useful when converting
>	from `bamg` or `gmsh` format, that do not have 
>	any coordinate system specification.

`-[no]verbose`
>       Print messages related to graphic files created and
>       command system calls (this is the default).

`-[no]clean
>       Clear temporary graphic files (this is the default).
    
`-[no]execute`
>       Execute graphic command (this is the default).
>       The `-noexecute` variant is useful
>       in conjunction with the `-verbose` and `-noclean` options
>       in order to modify some render options by hand.

`-dump`
>       Used for debug purpose.

Inquire options
===============

`-size` \n
`-n-element`
>     	Print the mesh size, i.e. the number of elements and then exit.

`-n-vertex`
>       Print the number of elements and then exit.

`-sys-coord`
>       Print the coordinate system and then exit.

`-hmin` \n
`-hmax`
>       Print the smallest (resp. largest) edge length and then exit.

`-xmin` \n
`-xmax`
>       Print the bounding box lower-left (resp. top-right) corner and exit.

`-min-element-measure` \n
`-max-element-measure`
>       Print the smallest (resp. largest) element measure and then exit.

File format conversion
======================

For the `gmsh` and `bamg` mesh generators,
automatic file conversion is provided by 
the `msh2geo` and `bamg2geo` commands
(see @ref bamg2geo_1 and @ref msh2geo_1 ).

For conversion from the `.vtk` legacy ascii file format to the `.geo` one,
simply writes:

    geo -if vtk -geo - < input.vtk > output.geo


The geo file format
===================
@addindex file format `.geo` mesh

This is the default mesh file format.
It contains two entities, namely a mesh and a list of domains.
The mesh entity starts with the `mesh` keyword,
that should be at the beginning of a line.
It is followed by the geo format version number:
the current mesh format version number is 4.
Next comes the header, containing global information:
the space dimension (e.g. 1, 2 or 3), the number of nodes
and the number of elements, for each type of element (tetrahedron, etc).
When dimension is three, the number of faces (triangles, quadrangles) is specified,
and then, when dimension is two or three, the number of edges is also specified.
Follows the node coordinates list and the elements connectivity list.
Each element starts with a letter indicating the element type:

        letter | element type
        -------|-------------
        p      | point 
        e      | edge 
        t      | triangle
        q      | quadrangle
        T      | tetrahedron
        P      | prism
        H      | hexahedron
  
Then, for each element, comes the vertex indexes. A vertex index is numbered in the C-style,
i.e. the first index started at 0 and the larger one
is the number of vertices minus one. A sample mesh writes:

        mesh
        4
        header
         dimension 2
         nodes     4
         triangles 2
         edges     5
        end header
        0 0
        1 0
        1 1
        0 1
        t   0 1 3
        t   1 2 3
        e   0 1
        e   1 2
        e   2 3
        e   3 0
        e   1 3

Note that information about edges for 2d meshes and faces for 3d one
are required for maintaining P2 and higher order approximation fields in a consistent way:
degrees of freedom associated to sides requires that sides are well defined.

The second entity is a list of domains, that finishes with the end of file.
A domain starts with the `domain` keyword,
that should be at the beginning of a line.
It is followed by the domain name, a simple string.
Then, comes the domain format version:
the current domain version number is 2.
Next, the domain dimension and its number of elements.
Finally, the list of elements: they are specified by
the element index in the mesh, preceded by its orientation.
A minus sign specifies that the element (generally a side)
has the opposite orientation, while the plus sign is omitted.
A sample domain list writes:

        domain
        bottom
        2 1 1
        0
        
        domain
        top
        2 1 1
        2

Copy and paste the previous sample mesh data in a file, e.g. "square.geo".
Be carreful for the "mesh" and "domain" to be at the beginning of a line.
Then enter:

    geo square.geo

and the mesh is displayed.

The simplified geo file format
==============================

@addindex command `gmsh`
@addindex command `bamg`

Information about edges for 2d meshes and faces for 3d one is not
provided by most mesh generators (e.g. gmsh or bamg).
It could be complex to build this list, so a simplified file format is 
also supported, without faces and/or edges connectivity,
and the geo command proposes to build it automatically and save it
in a more complete, upgraded geo file. 

The simplified version of the previous mesh is:

        mesh
        4
        header
         dimension 2
         nodes     4
         triangles 2
        end header
        0 0
        1 0
        1 1
        0 1
        t   0 1 3
        t   1 2 3

The domain list is no more able to refer to existing sides:
edges are simply listed by their complete connectivity, thanks
to the domain format version number 1.
For the previous example, we have:

        domain
        bottom
        1 1 1
        e 0 1
        
        domain
        top
        1 1 1
        e 2 3

Copy and paste the previous simplified sample mesh data in a file, e.g. `square0.geo`.
Be carreful for the `mesh` and `domain` keywords to be at the beginning of a line.
Then enter:

        geo -upgrade -geo square0.geo

and the previous mesh with its complete connectivity is displayed:
edges has been automatically identified and numbered,
and domains now refers to edge indexes.

Implementation
==============
@showfromfile
*/ } // namespace rheolef

# include "rheolef.h"
# include "rheolef/iofem.h"

#ifdef TO_CLEAN
// ****************************************************************
namespace rheolef {
// output all small objects that do not have 
//   ostdistream& operator<<
// but have
//   ostream&     operator<< 
template <class T>
typename std::enable_if<
  std::is_pointer<
    decltype(
      static_cast
        <std::ostream& (*)(std::ostream&, const T&)>
        (operator<<)
    )
  >::value
 ,odiststream&
>::type
operator<< (odiststream& s, T x)
{
    if (s.nop()) return s;
    s.os() << x;
    return s;
}
} // namespace rheolef {
// ****************************************************************
#endif // TO_CLEAN

using namespace rheolef;
void usage() {
      std::cerr
           << "geo: usage:" << std::endl
           << "geo "
           << "{-|file[.geo[.gz]]}"
           << "[-Idir|-I dir] "
           << "[-check] "
           << "[-upgrade] "
           << "[-subdivide int] "
           << "[-if {geo,bamg,vtk}] "
           << "[-geo|-gmsh|-gnuplot|-paraview] "
           << "[-name string] "
           << "[-[no]full|-[no]fill|-[no]lattice] "
           << "[-[no]stereo|-[no]full|-[no]cut|-[no]shrink|-[no]showlabel] "
           << "[-image-format string] "
           << "[-resolution int [int]] "
           << "[-add-boundary] "
           << "[-zr|-rz] "
           << "[-size] [-n-vertex] "
           << "[-hmin] [-hmax] "
           << "[-[min|max]-element-measure] "
           << "[-round [float]] "
           << "[-[no]clean] [-[no]execute] [-[no]verbose] "
           << std::endl;
      exit (1);
}
void set_input_format (idiststream& in, std::string input_format) 
{
       if (input_format == "bamg") in.is() >> bamg;
  else if (input_format == "vtk")  in.is() >> vtk;
  else if (input_format == "geo")  in.is() >> rheo;
  else {
      std::cerr << "geo: invalid input format \""<<input_format<<"\"" << std::endl;
      usage();
  }
}
typedef enum {
        no_render,
        gnuplot_render,
        paraview_render,
        plotmtv_render,
        vtk_render,
        gmsh_render,
        atom_render,
        x3d_render,
        file_render
} render_type;

#ifdef TO_CLEAN
void h_extrema (const geo_basic<Float,sequential>& omega, Float& hmin, Float& hmax)
{
  hmin = std::numeric_limits<Float>::max();
  hmax = 0;
  for (size_t iedg = 0, nedg = omega.geo_element_ownership(1).size(); iedg < nedg; ++iedg) {
    const geo_element& E = omega.get_geo_element (1, iedg);
    const point& x0 = omega.dis_node(E[0]);
    const point& x1 = omega.dis_node(E[1]);
    Float hloc = dist(x0,x1);
    hmin = min(hmin, hloc);
    hmax = max(hmax, hloc);
  }
#ifdef TODO
 // M=sequential here (just if this fct goes into the geo<M,T> class later)
#ifdef _RHEOLEF_HAVE_MPI
  hmin = mpi::all_reduce (comm(), hmin, mpi::minimum<Float>());
  hmax = mpi::all_reduce (comm(), hmax, mpi::maximum<Float>());
#endif // _RHEOLEF_HAVE_MPI
#endif // TODO
}
#endif // TO_CLEAN

Float extrema_element_measure (const geo_basic<Float,sequential>& omega, bool is_min)
{
  space_basic<Float,sequential> Xh (omega, "P0");
  form_basic<Float,sequential>  m (Xh, Xh, "mass");
  field_basic<Float,sequential> one (Xh, 1);
  return is_min ? (m*one).min_abs() : (m*one).max_abs();
}
int main(int argc, char**argv) {
    environment distributed(argc, argv);
    check_macro (communicator().size() == 1, "geo: command may be used as mono-process only");
    // ----------------------------
    // default options
    // ----------------------------
    bool do_upgrade  = false ;
    bool do_check    = false ;
    bool do_add_bdry = false ;
    bool do_stereo   = false ;
    bool show_n_element = false ;
    bool show_n_vertex = false ;
    bool show_sys_coord = false ;
    bool show_hmin = false ;
    bool show_hmax = false ;
    bool show_xmin = false ;
    bool show_xmax = false ;
    bool show_min_element_measure = false ;
    bool show_max_element_measure = false ;
    bool do_round  = false ;
    Float round_prec = pow(10., -std::numeric_limits<Float>::digits10/2);
    std::string name = "output";
    dout.os() << verbose; bool bverbose = true;
    render_type render = no_render;
    geo_basic<Float,sequential> omega;
    dout.os() << lattice;
    dout.os() << nofill;
    dout.os() << grid;
    dout.os() << showlabel;
    // this normal is not so bad for the dirichlet.cc demo on the cube:
    dout.os() << setnormal(point(-0.015940197423022637, -0.9771157601293953, -0.21211011624358989));
    dout.os() << setorigin(point(std::numeric_limits<Float>::max()));
    std::string filename;
    std::string input_format = "geo";
    std::string sys_coord = "";
    // ----------------------------
    // scan the command line
    // ----------------------------
    for (int i = 1; i < argc; i++) {

	// general options:
             if (strcmp (argv[i], "-clean") == 0)     dout.os() << clean;
        else if (strcmp (argv[i], "-noclean") == 0)   dout.os() << noclean;
        else if (strcmp (argv[i], "-execute") == 0)   dout.os() << execute;
        else if (strcmp (argv[i], "-noexecute") == 0) dout.os() << noexecute;
        else if (strcmp (argv[i], "-verbose") == 0)   { bverbose = true; dout.os() << verbose; }
        else if (strcmp (argv[i], "-noverbose") == 0) { bverbose = false; dout.os() << noverbose; }
        else if (strcmp (argv[i], "-add-boundary") == 0) { do_add_bdry = true; }
        else if (strcmp (argv[i], "-rz") == 0)        { sys_coord = "rz"; }
        else if (strcmp (argv[i], "-zr") == 0)        { sys_coord = "zr"; }
        else if (strcmp (argv[i], "-size") == 0)      { show_n_element = true; }
        else if (strcmp (argv[i], "-n-element") == 0) { show_n_element = true; }
        else if (strcmp (argv[i], "-n-vertex") == 0)  { show_n_vertex = true; }
        else if (strcmp (argv[i], "-sys-coord") == 0) { show_sys_coord = true; }
        else if (strcmp (argv[i], "-hmin") == 0)      { show_hmin = true; }
        else if (strcmp (argv[i], "-hmax") == 0)      { show_hmax = true; }
        else if (strcmp (argv[i], "-xmin") == 0)      { show_xmin = true; }
        else if (strcmp (argv[i], "-xmax") == 0)      { show_xmax = true; }
        else if (strcmp (argv[i], "-min-element-measure") == 0)  { show_min_element_measure = true; }
        else if (strcmp (argv[i], "-max-element-measure") == 0)  { show_max_element_measure = true; }
        else if (strcmp (argv[i], "-I") == 0)         {
            if (i == argc-1) { std::cerr << "geo -I: option argument missing" << std::endl; usage(); }
            append_dir_to_rheo_path (argv[++i]);
        }
        else if (argv [i][0] == '-' && argv [i][1] == 'I') { append_dir_to_rheo_path (argv[i]+2); }
	// output file option:
        else if (strcmp (argv[i], "-geo") == 0)       { dout.os() << rheo;     render = file_render; }
        else if (strcmp (argv[i], "-gmsh") == 0)      { dout.os() << gmsh;     render = gmsh_render; }

	// render spec:
        else if (strcmp (argv[i], "-gnuplot") == 0)   { dout.os() << gnuplot;  render = gnuplot_render; }
        else if (strcmp (argv[i], "-paraview") == 0)  { dout.os() << paraview; render = paraview_render; }

	// render option:
        else if (strcmp (argv[i], "-full") == 0)      { dout.os() << full; }
        else if (strcmp (argv[i], "-nofull") == 0)    { dout.os() << nofull; }
        else if (strcmp (argv[i], "-fill") == 0)      { dout.os() << fill; }
        else if (strcmp (argv[i], "-nofill") == 0)    { dout.os() << nofill; }
        else if (strcmp (argv[i], "-stereo") == 0)    { dout.os() << stereo; do_stereo = true; }
        else if (strcmp (argv[i], "-nostereo") == 0)  { dout.os() << nostereo; }
        else if (strcmp (argv[i], "-cut") == 0)       { dout.os() << cut; }
        else if (strcmp (argv[i], "-nocut") == 0)     { dout.os() << nocut; }
        else if (strcmp (argv[i], "-shrink") == 0)    { dout.os() << shrink; }
        else if (strcmp (argv[i], "-noshrink") == 0)  { dout.os() << noshrink; }
        else if (strcmp (argv[i], "-lattice") == 0)   { dout.os() << lattice; }
        else if (strcmp (argv[i], "-nolattice") == 0) { dout.os() << nolattice; }
        else if (strcmp (argv[i], "-showlabel") == 0)  { dout.os() << showlabel; }
        else if (strcmp (argv[i], "-noshowlabel") == 0){ dout.os() << noshowlabel; }
        else if (strcmp (argv[i], "-subdivide") == 0) {
            if (i == argc-1) { std::cerr << "geo -subdivide: option argument missing" << std::endl; usage(); }
            size_t nsub = atoi(argv[++i]);
	    dout.os() << setsubdivide (nsub);
	}
        else if (strcmp (argv[i], "-image-format") == 0) {
            if (i == argc-1) { std::cerr << "geo -image-format: option argument missing" << std::endl; usage(); }
            std::string format = argv[++i];
	    if (format == "jpeg")       format = "jpg";
	    if (format == "postscript") format = "ps";
	    dout.os() << setimage_format(format);
        }
        else if (strcmp (argv[i], "-resolution") == 0) {
            if (i == argc-1 || !isdigit(argv[i+1][0])) { std::cerr << "geo -resolution: option argument missing" << std::endl; usage(); }
            size_t nx = atoi(argv[++i]);
            size_t ny = (i < argc-1 && isdigit(argv[i+1][0])) ? atoi(argv[++i]) : nx;
	    dout.os() << setresolution(point_basic<size_t>(nx,ny));
        }
        else if (strcmp (argv[i], "-round") == 0)   {

	    do_round = true;
            if (i+1 < argc && is_float(argv[i+1])) {
              round_prec = to_float (argv[++i]);
            }
        }
	// input options:
        else if (strcmp (argv[i], "-upgrade") == 0)   { do_upgrade = true; dout.os() << rheo; render = file_render; }
        else if (strcmp (argv[i], "-check") == 0)   { do_check = true; }
        else if (strcmp (argv[i], "-name") == 0) {
            if (i == argc-1) { std::cerr << "geo -name: option argument missing" << std::endl; usage(); }
            name = argv[++i];
        }
        else if (strcmp (argv[i], "-if") == 0 ||
                 strcmp (argv[i], "-input-format") == 0) {
            if (i == argc-1) { std::cerr << "geo "<<argv[i]<<": option argument missing" << std::endl; usage(); }
            input_format = argv[++i];
        }
        else if (strcmp (argv[i], "-") == 0) {
	    filename = "-"; // geo on stdin
	}
        else if (argv[i][0] != '-') {
	    filename = argv[i]; // input on file	
        }
        else { usage(); }
    }
    // ----------------------------
    // geo treatment
    // ----------------------------
    if (filename == "") {
      std::cerr << "geo: no input file specified" << std::endl;
      usage();
    } else if (filename == "-") {
      // geo on stdin
      if (do_upgrade) std::cin >> upgrade;
      std::string thename;
      if (name != "output") thename = name;
      std::cin >> setbasename(thename);
      set_input_format (din, input_format);
      din >> omega;
      dout.os() << setbasename(name) << reader_on_stdin;
    } else {
      // input geo on file
      std::string suffix = input_format;
      if (name == "output") {
        name = get_basename(delete_suffix (delete_suffix (filename, "gz"), suffix));
      }
      idiststream ips;
      ips.open (filename, suffix);
      check_macro(ips.good(), "\"" << filename << "[."<<suffix<<"[.gz]]\" not found.");
      if (do_upgrade) ips.is() >> upgrade;
      std::string root_name = delete_suffix (delete_suffix(filename, "gz"), suffix);
      name = get_basename (root_name);
      ips.is() >> setbasename(name);
      set_input_format (ips, input_format);
      ips >> omega;
      dout.os() << setbasename(name);
    }
    if (sys_coord != "") { omega.set_coordinate_system(sys_coord); }
    if (do_add_bdry) { omega.boundary(); }

    if (render == file_render) {
      size_t nsub = iorheo::getsubdivide (std::cout);
      if (nsub >= 2) {
        geo_basic<Float,sequential> new_omega;
        new_omega.build_by_subdividing (omega, nsub);
        omega = new_omega;
      }
    }
    if (do_check) {
        check_macro (omega.check(bverbose), "geo check failed");
    }
    bool show = show_min_element_measure
             || show_max_element_measure
             || show_hmin || show_hmax
             || show_xmin || show_xmax
	     || show_n_element || show_n_vertex || show_sys_coord;
    if (show) {
      if (show_hmin)      { dout << "hmin = " << omega.hmin() << std::endl; }
      if (show_hmax)      { dout << "hmax = " << omega.hmax() << std::endl; }
      if (show_xmin)      { dout << "xmin = " << omega.xmin() << std::endl; }
      if (show_xmax)      { dout << "xmax = " << omega.xmax() << std::endl; }
      if (show_n_element) { dout << "size = " << omega.dis_size()  << std::endl; }
      if (show_n_vertex)  { dout << "n_vertex  = " << omega.dis_size(0) << std::endl; }
      if (show_sys_coord) { dout << "sys_coord = " << omega.coordinate_system_name() << std::endl; }
      if (show_min_element_measure) {
        dout << "min_element_measure = " << extrema_element_measure (omega, true) << std::endl;
      }
      if (show_max_element_measure) {
        dout << "max_element_measure = " << extrema_element_measure (omega, false) << std::endl;
      }
      return 0;
    }
    if (do_round) {
      dout.os() << setrounding_precision(round_prec);
    }
    if (render == no_render) {
      // try to choose the best render from dimension
#if (_RHEOLEF_PARAVIEW_VERSION_MAJOR >= 5) && (_RHEOLEF_PARAVIEW_VERSION_MINOR >= 5)
      // paraview version >= 5.5 has high order elements
      if (do_stereo || omega.dimension() >= 2) {
#else 
      if (do_stereo || omega.dimension() == 3) {
#endif 
        dout.os() << paraview;
      } else {
        dout.os() << gnuplot;
      }
    }
    dout << omega;
}
