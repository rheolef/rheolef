//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// The field unix command
// author: Pierre.Saramito@imag.fr
// last change: 11 oct 2011. initial version: 7 july 1997

namespace rheolef {
/**
@commandfile field plot a field 
@addindex field
@addindex plot a field

Synopsis
========

    field [options] file[.field[.gz]]

Description
===========

Read and output a finite element field from file.

Example
=======

        field square.field
        field square.field -bw
        field box.field

Input file specification
========================

filename
>	Specifies the name of the file containing the input field.

`-`
>	Read field on standard input instead on a file.
    
`-name`
>       When the field comes from standard input, the file base name
>       is not known and is set to "output" by default.
>       This option allows one to change this default.
>       Useful when dealing with output formats (graphic, format conversion)
>       that creates auxiliary files, based on this name.

@addindex RHEOPATH environment variable

`-I`@e dir \n
`-I` *dir* 
>       Add *dir* to the Rheolef file search path.
>	This option is useful e.g. when the mesh .geo and the .field fikes are in
>	different directories.
>       This mechanism initializes a search path given by the environment variable `RHEOPATH`.
>       If the environment variable `RHEOPATH` is not set, the default value is the current directory.

`-mark` *string* \n
`-catch` *string* \n
`-catchmark` *string*
>	Jump across the file to the specified *string* .
>       Label start at the beginning of a line, preceded by a `#` mark (see @ref catchmark_3).

Output file format options
==========================

`-field`
`-text`
>	Output field on standard output stream in Rheolef ascii (field or geo) text file format.

@addindex command `gmsh`
@addindex file format `.gmsh` mesh
@addindex file format `.gmsh_pos` field

`-gmsh`
>	Output field on standard output stream in `.gmsh` file format.

`-gmsh-pos`
>	Output field on standard output stream in `.gmsh-pos` file format,
>	suitable for mesh adaptation purpose.

@addindex command `bamg`
@addindex file format `.bamg` mesh
@addindex file format `.bamg_bb` field

`-bamg-bb`
>	Output field on standard output stream in bamg-bb text file format,
>       suitable for mesh adaptation purpose.

@addindex graphic render
@addindex image file format
@addindex file format `.png` image
@addindex file format `.jpg` image
@addindex file format `.gif` image
@addindex file format `.tif` image
@addindex file format `.ppm` image
@addindex file format `.bmp` image
@addindex file format `.pdf` image
@addindex file format `.eps` image
@addindex file format `.ps`  image
@addindex file format `.svg` image

`-image-format` *string*
>       For image or video capture.
>       The supported argument are `.jpg`, `.png`, `.tif` and `.bmp`.
>       This option should be combined with the `paraview` render.
>       The output file is basename.png where *basename*
>       is the name of the mesh, or can be set with the `-name` option.

`-resolution` *int* *int*
>       For the resolution of an image or a video capture.
>       The argument is a couple of sizes, separated by a white space.
>       This option can be used together with the `-image-format`
>       for any of the bitmap image formats.
>       This option requires the `paraview` render.

Getting information
===================

`-min` \n
`-max`
>       Print the min (resp. max) value of the scalar field and then exit.

`-get-geo`
>       Print the name of the mesh associated to the field and exit.

Render options
==============

@addindex command `gnuplot`
@addindex command `paraview`

`-gnuplot`
>	Use the `gnuplot` tool.
>	This is the default in one dimension.

`-paraview`
>       Use the `paraview` tool.
>       This is the default for two- and tri-dimensional geometries.

Rendering options
=================

`-color` \n
`-gray` \n
`-black-and-white` \n
`-bw`
>	Use (color/gray scale/black and white) rendering.
>	Color rendering is the default.
`-[no]showlabel`
>       Show or hide title, color bar and various annotations.
>       Default is to show labels.
`-label *string*
>       Set the label to show for the represented value.
>	This supersedes the default value.
`-[no]elevation`
>	For two dimensional field, represent values as elevation
>	in the third dimension.
>	The default is no elevation.
`-scale` *float*
>       Applies a multiplicative factor to the field.
>       This is useful e.g. in conjunction with the `elevation` option.
>       The default value is 1.
`-[no]stereo`
>       Rendering mode suitable for red-blue anaglyph 3D stereoscopic glasses.
>       This option is only available with `paraview`.
`-[no]fill`
>	Isoline intervals are filled with color.
>	This is the default.
>	When `-nofill`, draw isolines by using lines.
`-[no]volume`
>	For 3D data, render values using a colored translucid volume.
>       This option requires the `paraview` code.
`-[no]cut`
>       Cut by a specified plane.
>       The cutting plane is specified by its origin point and normal vector.
>       This option requires the `paraview` code.
`-origin` *float* [*float* [*float*]]
>       Set the origin of the cutting plane.
>       Default is (0.5, 0.5, 0.5).
`-normal` *float* [*float* [*float*]]
>       Set the normal of the cutting plane.
>       Default is (1, 0, 0).
`-isovalue` [*float*] \n
`-iso`      [*float*]
>       Draw 2d isoline or 3d isosurface.
>       When the optional float is not provided, a median value is used.
>       This option requires the `paraview` code.
`-noisovalue`
>       Do not draw isosurface.
>	This is the default.
`-n-iso` *int*
>       For 2D visualizations, the isovalue table contains
>       regularly spaced values from fmin to fmax, the bounds
>       of the field.

@addindex vorticity
@addindex stream function
`-n-iso-negative` *int*
>       The isovalue table is split into negatives and positives values.
>       Assume there is n_iso=15 isolines: if 4 is requested by this option,
>       then, there will be 4 negatives isolines, regularly spaced
>       from fmin to 0 and 11=15-4 positive isolines, regularly spaced
>       from 0 to fmax.
>       This option is useful when plotting e.g. vorticity or stream
>       functions, where the sign of the field is representative.

@addindex projection
@addindex approximation P1
`-proj` *approx*
`-proj`
>       Convert all selected fields to approximation *approx* by using a L2 projection.
>       When argument is omitted, `P1` approximation is assumed.
`-lumped-proj`
>       Force `P1` approximation for L2 projection and use a lumped mass matrix for it.
`-round` [*float*]
>       Round the input up to the specified precision.
>       This option, combined with `-field`, leads to a round filter.
>       Useful for non-regression test purpose, in order
> 	to compare numerical results between files with a limited precision,
>       since the full double precision is machine-dependent.

`-subdivide` *int*
>       When using a high order geometry, the number of points per edge used to draw
>       a curved element. Default value is the mesh order.

`-deformation`
`-velocity`
>       Render vector-valued fields as deformed mesh
>       using `paraview` or `gnuplot`.
>       This is the default vector field representation.
>	When `velocity`, render vector-valued fields as arrows using `paraview`
>	instead.

Component extraction and domain reduction
=========================================

`-comp` *int* \n
`-comp` *string*
>	Extract the i-th component of a vector-valued field.
>       For a tensor-valued field, indexing components as `00`, `01`, `11`... is supported.

`-domain` *name*
>	Reduce the visualization to the specified domain name.

Others options
==============

`-[no]verbose`
>       Print messages related to graphic files created and
>       command system calls (this is the default).

`-[no]clean
>       Clear temporary graphic files (this is the default).
    
`-[no]execute`
>       Execute graphic command (this is the default).
>       The `-noexecute` variant is useful
>       in conjunction with the `-verbose` and `-noclean` options
>       in order to modify some render options by hand.

Field file format
=================

@addindex file format `.field` 

It contains a header and a list values at degrees of freedom.
The header contains the `field`
keyword followed by a line containing a format version number (presently 1),
the number of degrees of freedom (i.e. the number of values listed),
the mesh file name without the `.geo` extension
the approximation (e.g. P1, P2, etc), and finally the list of values:

A sample field file write

        field
        1 4
        square
        P1
        0.0
        1.0
        2.0
        3.0

See also @ref geo_1 for the `.geo` mesh file format.

Examples
========
The following command send to `vtk` the cuted 2d plane of the 3d field:

        field cube.field -cut -normal 0 1 0 -origin 0.5 0.5 0.5 -vtk

Next, let us generate the cuted 2d field and its associated mesh:

        field cube.field -cut -normal 0 1 0 -origin 0.5 0.5 0.5 -text > cube-cut.field

For drawing the isosurface:

        field cube.field -isovalue 0.5

Finally, let us generate the isosurface as a 3d surface mesh in
the `.geo` file format:

        field cube.field -isovalue 0.5 -text > isosurf.geo

This file is then suitable for others treatments.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

# include "rheolef.h"
# include "rheolef/iofem.h"
using namespace rheolef;
using namespace std;
void usage() {
      cerr << "field: usage:" << endl
           << "field "
           << "{-|file[.field[.gz]]}"
           << "[-Idir|-I dir] "
           << "[-min|-max|-get-geo] "
           << "[-name string] "
           << "[-[catch]mark string] "
           << "[-comp string] "
           << "[-domain string] "
           << "[-round [float]] "
           << "[-proj [string]] "
           << "[-lumped-proj] "
           << "[-paraview|-gnuplot] "
           << "[-field|-text|-gmsh|-gmsh-pos|-bamg-bb] "
           << "[-[no]clean] [-[no]execute] [-[no]verbose] "
           << "[-color|-gray|-black-and-white|-bw] "
           << "[-[no]elevation] "
           << "[-[no]volume] "
           << "[-[no]fill] "
           << "[-[no]showlabel] "
           << "[-label string] "
           << "[-[no]stereo] "
           << "[-[no]cut] "
           << "[-normal x [y [z]]] "
           << "[-origin x [y [z]]] "
           << "[-scale float] "
	   << "[-iso[value] float|-noiso[value]] "
           << "[-n-iso int] "
           << "[-n-iso-negative int] "
           << "[-velocity|-deformation] "
           << "[-image-format string] "
           << "[-resolution int [int]] "
           << "[-subdivide int] "
           << endl;
      exit (1);
}
// extern:
namespace rheolef {
  template <class T> geo_basic<T,sequential> paraview_extract_isosurface (
    const field_basic<T,sequential>& uh);
  template <class T> field_basic<T,sequential> paraview_plane_cut (
    const field_basic<T,sequential>& uh,
    const point_basic<T>&            origin,
    const point_basic<T>&            normal);
} // namespace rheolef

typedef enum {
        no_render,
        gnuplot_render,
        paraview_render,
        plotmtv_render,
        vtk_render,
        atom_render,
        x3d_render,
        file_render
} render_type;

typedef enum {
        deformation_style,
        velocity_style,
        undef_vector_style
} vector_style_type;

typedef enum {
        show_none,
        show_min,
        show_max,
        show_geo,
} show_type;

int main(int argc, char**argv) {
    //environment_option_type eopt;
    //eopt.thread_level = environment_option_type::no_thread;
    //environment rheolef (argc, argv, eopt);
    environment rheolef (argc, argv);
    check_macro (communicator().size() == 1, "field: command may be used as mono-process only");
    // ----------------------------
    // default options
    // ----------------------------
    string filename = "";
    string name = "output";
    dout.os() << setbasename(name);
    string format = "";
    dout.os() << verbose; bool bverbose = true;
    std::string mark = "";
    render_type render = no_render;
    vector_style_type vector_style = deformation_style;
    field_basic<Float,sequential> uh;
    bool def_fill_opt = false;
    bool do_iso3d = false;
    std::string i_comp_name = "";
    bool do_proj = false;
    string use_proj_approx = "";
    bool do_lumped_mass = false;
    bool do_cut  = false;
    bool do_round = false;
    std::string reduce_to_domain = "";
    Float round_prec = sqrt(std::numeric_limits<Float>::epsilon());
    show_type show = show_none;
    dout.os() << showlabel;
    std::string label;
    // this normal is not so bad for the dirichlet.cc demo on the cube:
    cout << setnormal(point(-0.015940197423022637, -0.9771157601293953, -0.21211011624358989));
    cout << setorigin(point(std::numeric_limits<Float>::max()));
    // ----------------------------
    // scan the command line
    // ----------------------------
    for (int i = 1; i < argc; i++) {

	// general options:
             if (strcmp (argv[i], "-clean") == 0)     dout.os() << clean;
        else if (strcmp (argv[i], "-noclean") == 0)   dout.os() << noclean;
        else if (strcmp (argv[i], "-execute") == 0)   dout.os() << execute;
        else if (strcmp (argv[i], "-noexecute") == 0) dout.os() << noexecute;
        else if (strcmp (argv[i], "-verbose") == 0)   { bverbose = true; dout.os() << verbose; }
        else if (strcmp (argv[i], "-noverbose") == 0) { bverbose = false; dout.os() << noverbose; }
        else if (strcmp (argv[i], "-I") == 0)         {
            if (i+1 == argc) { cerr << "geo -I: option argument missing" << endl; usage(); }
            append_dir_to_rheo_path (argv[++i]);
        }
        else if (argv [i][0] == '-' && argv [i][1] == 'I')  { append_dir_to_rheo_path (argv[i]+2); }
	// output file option:
        else if (strcmp (argv[i], "-field") == 0)           { dout.os() << rheo;     render = file_render; }
        else if (strcmp (argv[i], "-text") == 0)            { dout.os() << rheo;     render = file_render; }
        else if (strcmp (argv[i], "-gmsh") == 0)            { dout.os() << gmsh;     render = file_render; }
        else if (strcmp (argv[i], "-gmsh-pos") == 0)        { dout.os() << gmsh_pos; render = file_render; }
        else if (strcmp (argv[i], "-bamg-bb") == 0)         { dout.os() << bamg;     render = file_render; }
        else if (strcmp (argv[i], "-name") == 0)            {
            if (i+1 == argc) { std::cerr << "field -name: option argument missing" << std::endl; usage(); }
            name = argv[++i];
	}
        else if (strcmp (argv[i], "-label") == 0)            {
            if (i+1 == argc) { std::cerr << "field -label: option argument missing" << std::endl; usage(); }
            label = argv[++i];
            dout.os() << setlabel(label);
	}
        // render spec:
        else if (strcmp (argv[i], "-gnuplot") == 0)         { dout.os() << gnuplot;  render = gnuplot_render; }
        else if (strcmp (argv[i], "-paraview")  == 0)       { dout.os() << paraview; render = paraview_render; }

	// filter 
        else if (strcmp (argv[i], "-proj") == 0)            {
	    do_proj = true;
            if (i+1 < argc && argv[i+1][0] != '-') {
              use_proj_approx = argv[++i];
            }
	}
        else if (strcmp (argv[i], "-lumped-proj") == 0)     {
	    do_proj = true;
	    do_lumped_mass = true;
	    use_proj_approx = "P1";
        }
        // inquire option
        else if (strcmp (argv[i], "-min") == 0)       { show = show_min; }
        else if (strcmp (argv[i], "-max") == 0)       { show = show_max; }
        else if (strcmp (argv[i], "-get-geo") == 0)   { show = show_geo; }

	// render options:
        else if (strcmp (argv[i], "-velocity") == 0)        { dout.os() << velocity;    vector_style = velocity_style; }
        else if (strcmp (argv[i], "-deformation") == 0)     { dout.os() << deformation; vector_style = deformation_style; }
        else if (strcmp (argv[i], "-fill") == 0)            { dout.os() << fill;   def_fill_opt = true; }
        else if (strcmp (argv[i], "-nofill") == 0)          { dout.os() << nofill; def_fill_opt = false; }
        else if (strcmp (argv[i], "-elevation") == 0)       { dout.os() << elevation; }
        else if (strcmp (argv[i], "-noelevation") == 0)     { dout.os() << noelevation; }
        else if (strcmp (argv[i], "-color") == 0)           { dout.os() << color; }
        else if (strcmp (argv[i], "-gray") == 0)            { dout.os() << gray; }
        else if (strcmp (argv[i], "-black-and-white") == 0) { dout.os() << black_and_white; }
        else if (strcmp (argv[i], "-bw") == 0)              { dout.os() << black_and_white; }
        else if (strcmp (argv[i], "-showlabel") == 0)       { dout.os() << showlabel; }
        else if (strcmp (argv[i], "-noshowlabel") == 0)     { dout.os() << noshowlabel; }
        else if (strcmp (argv[i], "-stereo") == 0)          { dout.os() << stereo; 
							      if (render != paraview_render) {
                                                                dout.os() << paraview; 
							        render  = paraview_render;
                                                              }
                                                            }
        else if (strcmp (argv[i], "-nostereo") == 0)        { dout.os() << nostereo; }
        else if (strcmp (argv[i], "-volume") == 0)          { dout.os() << paraview << volume;   
							      render  = paraview_render; }
        else if (strcmp (argv[i], "-novolume") == 0)        { dout.os() << novolume; }
        else if (strcmp (argv[i], "-cut") == 0)             { do_cut = true; }
        else if (strcmp (argv[i], "-nocut") == 0)           { do_cut = false; }
        else if (strcmp (argv[i], "-noisovalue") == 0)      { dout.os() << noiso; do_iso3d = false; }
        else if (strcmp (argv[i], "-isovalue") == 0 || strcmp (argv[i], "-iso") == 0)   {

	    do_iso3d = true;
            dout.os() << iso;
            if (i+1 < argc && is_float(argv[i+1])) {
              Float iso_value = to_float (argv[++i]);
              dout.os() << setisovalue(iso_value);
            }
        } else if (strcmp (argv[i], "-n-iso") == 0)   {

            if (i+1 == argc || !isdigit(argv[i+1][0])) usage();
            size_t idx = atoi (argv[++i]);
            dout.os() << setn_isovalue(idx);

        } else if (strcmp (argv[i], "-n-iso-negative") == 0)   {

            if (i+1 == argc || !isdigit(argv[i+1][0])) usage();
            size_t idx = atoi (argv[++i]);
            dout.os() << setn_isovalue_negative(idx);

        } else if (strcmp (argv[i], "-scale") == 0)   {

            if (i+1 == argc || !is_float(argv[i+1])) usage();
            Float scale = to_float (argv[++i]);
            cout << setvectorscale (scale);

	} else if (strcmp (argv[i], "-image-format") == 0) {
            if (i == argc-1) {
                cerr << "field -image-format: option argument missing" << endl;
                usage();
            }
            string format = argv[++i];
            if (format == "jpeg")       format = "jpg";
            if (format == "postscript") format = "ps";
            dout.os() << setimage_format(format);
        }
        else if (strcmp (argv[i], "-resolution") == 0) {
            if (i == argc-1 || !isdigit(argv[i+1][0])) { std::cerr << "geo -resolution: option argument missing" << std::endl; usage(); }
            size_t nx = atoi(argv[++i]);
            size_t ny = (i < argc-1 && isdigit(argv[i+1][0])) ? atoi(argv[++i]) : nx;
	    dout.os() << setresolution(point_basic<size_t>(nx,ny));
        }
        else if (strcmp (argv[i], "-mark") == 0 || strcmp (argv[i], "-catch") == 0 || strcmp (argv[i], "-catchmark") == 0) {
            if (i == argc-1) {
                cerr << "field -mark: option argument missing" << endl;
                usage();
            }
	    mark = argv[++i];
        }
        else if (strcmp (argv[i], "-subdivide") == 0) {
            if (i == argc-1) { cerr << "field -subdivide: option argument missing" << endl; usage(); }
            size_t nsub = atoi(argv[++i]);
            dout.os() << setsubdivide (nsub);
        }
        else if (strcmp (argv[i], "-comp") == 0)   {

            if (i+1 == argc || !isdigit(argv[i+1][0])) usage();
            i_comp_name = argv[++i];
        }
        else if (strcmp (argv[i], "-domain") == 0)   {

            if (i+1 == argc) usage();
            reduce_to_domain = argv[++i];
        }
        else if (strcmp (argv[i], "-round") == 0)   {

	    do_round = true;
            if (i+1 < argc && is_float(argv[i+1])) {
              round_prec = to_float (argv[++i]);
            }
        }
        else if ((strcmp (argv[i], "-origin") == 0) || (strcmp (argv[i], "-normal") == 0))   {

	    point x;
	    unsigned int io = i;
	    if (i+1 == argc || !is_float(argv[i+1])) {
	    	warning_macro ("invalid argument to `" << argv[i] << "'");
	    	usage();
	    }
	    x[0] = to_float (argv[++i]);
	    if (i+1 < argc && is_float(argv[i+1])) {
	        x[1] = to_float (argv[++i]);
	        if (i+1 < argc && is_float(argv[i+1])) {
	            x[2] = to_float (argv[++i]);
	        }	
	    }	
            if (strcmp (argv[io], "-origin") == 0)   {
		cout << setorigin(x);
	    } else {
		cout << setnormal(x);
	    }
        }
	// input options:
        else if (strcmp (argv[i], "-") == 0) {

	    // field on stdin
            filename = "-";
	}
        else if (argv[i][0] != '-') {
            // input field on file	
	    filename = argv[i];
        }
        else {
            cerr << "field: unknown option `" << argv[i] << "'" << endl;
            usage();
        }
    }
    // ----------------------------
    // field treatment
    // ----------------------------
    if (filename == "") {
        cerr << "field: no input file specified" << endl;
        usage();
    } else if (filename == "-") {
        // field on stdin
        std::string thename;
        if (name != "output") thename = name;
        std::cin >> setbasename(thename);
        if (mark != "") din >> catchmark(mark);
        dout.os() << setbasename(name) << reader_on_stdin;
        din >> uh;
    } else {
        idiststream ids;
        ids.open (filename, "field");
        check_macro(ids.good(), "\"" << filename << "[.field[.gz]]\" not found.");
        if (mark != "") ids >> catchmark(mark);
        ids >> uh;
        std::string root_name = delete_suffix (delete_suffix(filename, "gz"), "field");
        name = get_basename (root_name);
        dout.os() << setbasename(name);
    }
    if (uh.valued_tag() == space_constant::vector) { 
      if (vector_style == velocity_style && render != paraview_render) {
        // gnuplot does not support yet velocity style: requieres paraview
        dout.os() << paraview; render = paraview_render;
      }
    }
    if (render == no_render) {
        // try to choose the best render from dimension
        if (uh.get_geo().map_dimension() >= 2 ||
            uh.get_geo().dimension() == 3) {
          dout.os() << paraview;
        } else {
          dout.os() << gnuplot;
        }
    }
    if (uh.get_geo().map_dimension() == 3) {
      // default 3D is iso+cut and nofill; default 2D is nocut and fill...
      if (!def_fill_opt)       dout.os() << nofill;
    }
    if (reduce_to_domain != "") {
      geo_basic<Float,sequential> dom = uh.get_geo()[reduce_to_domain];
      field_basic<Float,sequential> vh;
      if (uh.get_space().get_basis().is_discontinuous() && dom.map_dimension()+1 == uh.get_geo().map_dimension()) {
        // brdy trace of a discontinuous field
        space_basic<Float,sequential> Wh (dom, uh.get_approx());
	uh.get_geo().neighbour_guard();
        vh = interpolate (Wh, uh);
      } else {
        vh = uh[reduce_to_domain];
      }
      uh = vh;
      if (render == file_render) {
	// put field on dout: save also the cutted geo:
	odiststream geo_out (uh.get_geo().name(), "geo");
        geo_out << uh.get_geo();
      }
    }
    if (do_proj && uh.get_space().get_basis().have_compact_support_inside_element()) {
      const space_basic<Float,sequential>& Uh = uh.get_space();
      size_t k = Uh.degree();
      if (k == 0) k++;
      std::string approx = (do_lumped_mass || use_proj_approx == "") ? "P1" : use_proj_approx;
      space_basic<Float,sequential> Vh (uh.get_geo(), approx, uh.valued());
      test_basic<Float,sequential,details::vf_tag_10> u (Uh), v (Vh);
      test_basic<Float,sequential,details::vf_tag_01> vt (Vh);
      integrate_option fopt;
      fopt.lump = do_lumped_mass;
      form_basic<Float,sequential> m, p;
      switch (Uh.valued_tag()) {
        case space_constant::scalar:
	  m = integrate(v*vt, fopt);
	  p = integrate(u*vt);
	  break;
        case space_constant::vector:
	  m = integrate(dot(v,vt), fopt);
	  p = integrate(dot(u,vt));
	  break;
        case space_constant::tensor:
        case space_constant::unsymmetric_tensor:
	  m = integrate(ddot(v,vt), fopt);
	  p = integrate(ddot(u,vt));
	  break;
	default:
	  error_macro ("proj: unexpected valued field: " << Uh.valued());
      }
      solver_basic<Float,sequential> sm (m.uu());
      field_basic<Float,sequential> vh (Vh);
      vh.set_u() = sm.solve((p*uh).u());
      uh = vh;
    }
    if (i_comp_name != "") {
      // compute uh component
      field_basic<Float,sequential> vh;
      switch (uh.valued_tag()) {
        case space_constant::vector: {
          size_t i_comp = atoi (i_comp_name.c_str());
          vh = uh[i_comp];
          break;
        }
        case space_constant::tensor:
        case space_constant::unsymmetric_tensor: {
	  // string as "00" "21" "22" etc
	  check_macro (i_comp_name.size() == 2, "invalid component `"<<i_comp_name<<"'");
          size_t i = i_comp_name[0] - '0';
          size_t j = i_comp_name[1] - '0';
          vh = uh(i,j);
          break;
        }
        default: error_macro ("component: unexpected "<<uh.valued()<<"-valued field");
      }
      uh = vh;
    }
    if (do_cut) {
       point origin = iofem::getorigin(cout);
       point normal = iofem::getnormal(cout);
       uh = paraview_plane_cut (uh, origin, normal);
       if (render == file_render) {
	 // put field on dout: save also the cutted geo:
	 odiststream geo_out (uh.get_geo().name(), "geo");
	 geo_out << uh.get_geo();
       }
    }
    if (do_round) {
      uh = round (uh, round_prec);
    }
    if (show == show_min) {
      cout << std::setprecision(std::numeric_limits<Float>::digits10)
           << uh.min() << endl;
      return 0;
    }
    if (show == show_max) {
      cout << std::setprecision(std::numeric_limits<Float>::digits10)
           << uh.max() << endl;
      return 0;
    }
    if (show == show_geo) {
      cout << uh.get_geo().name() << endl;
      return 0;
    }
    if (do_iso3d && (render == file_render || render == gnuplot_render)) {
      // explicitely build the isosurface: for file or gnuplot
      geo_basic<Float,sequential> uh_iso_surface = paraview_extract_isosurface (uh);
      dout << uh_iso_surface;
      return 0;
    }
    // generates label:
    if (label == "") {
      string root_filename = iorheo::getbasename(dout.os());
      label = (root_filename == "output") ? "" : root_filename;
      if (mark  != "") label = mark;
      if (i_comp_name != "") { 
         label = (label == "") ? "value" : label;
         label = label + "[" + i_comp_name + "]";
      }
      if (reduce_to_domain != "") {
         label = (label == "") ? "value" : label;
         label = label + "[" + reduce_to_domain + "]";
      }
      dout.os() << setlabel (label);
    }
    // final output:
    dout << uh;
}
