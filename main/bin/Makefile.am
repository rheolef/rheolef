## process this file with automake to produce Makefile.in
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
include ${top_builddir}/config/config.mk

# -----------------------------------------------------------------------------
# the file set
# -----------------------------------------------------------------------------

bin_PROGRAMS  = 						\
	geo							\
	field							\
	branch							

geo_SOURCES 	= geo.cc
field_SOURCES 	= field.cc
branch_SOURCES 	= branch.cc

EXTRA_DIST = Makefile.am

CLEANFILES = *.[1-9]rheolef *.man[1-9]

CVSIGNORE = Makefile.in $(srcdir)/Makefile.in

# -----------------------------------------------------------------------------
# documentation
# -----------------------------------------------------------------------------

if HAVE_DOX_DOCUMENTATION
man_MANS = ${bin_PROGRAMS:=.1rheolef}
DOC_DOX  = ${bin_PROGRAMS:=.man1}
else
man_MANS =
DOC_DOX  =
endif

# -----------------------------------------------------------------------------
# extra rules
# -----------------------------------------------------------------------------
dvi-local: ${man_MANS} $(DOC_DOX)
all-local: dvi-local

clean-local:
	rm -rf cxx_repository
	for f in $(DEPDIR)/*; do		\
	    rm -f $$f;			\
	    touch $$f;				\
	done

AM_CPPFLAGS = 				\
	-I${top_builddir}/include 	\
	$(INCLUDES_SOLVER) 		\
	$(INCLUDES_PARTITIONNER)	\
	$(INCLUDES_BOOST_MPI) 		\
	$(INCLUDES_MPI) 		\
	$(INCLUDES_FLOAT)

LDADD    =  				\
	../lib/librheolef.la		\
	$(LDADD_SOLVER) 		\
	$(LDADD_PARTITIONNER) 		\
	$(LDADD_CGAL)			\
	$(LDADD_BOOST_MPI)		\
	$(LDADD_GINAC)			\
	$(LDADD_MPI) 			\
	$(LDADD_FLOAT)			\
	$(LDADD_DMALLOC)			

LOOP_SUBDIRS  = ../../config ../../util/lib ../../linalg/lib ../../fem/quadrature ../../fem/geo_element ../../fem/lib ../lib ../sbin
lib:
	here=`pwd`; 			\
	for d in $(LOOP_SUBDIRS); do 	\
	  cd $$d && ${MAKE} && cd $$here;    \
	  if test $$? -ne 0; then exit 1; fi; \
	done

