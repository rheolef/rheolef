#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
GEODIR=${GEODIR-"$SRCDIR"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

#
# check boundary mass forms
#
for coord_sys in cartesian rz; do
  case $coord_sys in
    cartesian)  LD="2 3";;
    rz)         LD="2";;
  esac
  LP="P1 P2"
  for d in $LD; do
    #echo "dimension $d"
    case $d in
      2) LG="left right top bottom"
         LW1="1 x y"
         LW2="$LW1 x2 y2 xy"
         LW3="$LW2 x3 y3 x2y xy2"
         input="carre-bamg-v2 carre-bamg-q-dom-v2 carre-tq-10-dom-v2"
         ;;
      3) LG="left right top bottom front back"
         LW1="1 x y z"
         LW2="$LW1 x2 y2 z2 xy xz yz"
         LW3="$LW2 x3 y3 z3 x2y xy2 x2z xz2 y2z yz2 xyz"
         input="cube-dom-v2 cube-H-6-dom-v2 cube-P-5-dom-v2 my_cube_PH-5-v2 my_cube_TP-5-v2 my_cube_TPH-5-v2"
         ;;
    esac
    for geo in $input; do
      for p in $LP; do
        #echo " polynom $p"
        case $p in
          "P1") LW=$LW1;;
          "P2") LW=$LW2;;
        esac
        for w in $LW; do
          #echo "  poid $w"
          for g in $LG; do
            #echo "   domaine $g"
            if test $coord_sys = "rz"; then
  	    opt="-rz"
  	  else
  	    opt=""
  	  fi
            loop_mpirun "./form_mass_bdr_tst -app $p -weight $w $opt -I${GEODIR} $geo $g >/dev/null 2>/dev/null"
            if test $? -ne 0; then status=1; fi
          done
        done
      done
    done
  done
done

exit $status

