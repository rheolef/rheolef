#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skipped)"
#exit 0

# -------------------------------------------------------------
# check for low degrees and equispaced node set
# -------------------------------------------------------------
# geo          	
L="
line-dom-v2
carre-bamg-v2
carre-bamg-q-dom-v2
carre-tq-10-dom-v2
cube-dom-v2
cube-H-6-dom-v2
cube-P-5-dom-v2
my_cube_PH-5-v2
my_cube_TP-5-v2
my_cube_TPH-5-v2
"

n=0
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  loop_mpirun "./form_lazy_tst $SRCDIR/$geo 2>/dev/null >/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
