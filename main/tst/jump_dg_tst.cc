///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// various dg tests with mkgeo_grid -region
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "jump_dg_tst.h"
// ----------------------------------------------------------------------------
int test_jump(geo omega, string approx, Float tol) {
// ----------------------------------------------------------------------------
  space Xh (omega, approx),
        Xh_west (omega["west"], approx),
        Xh_east (omega["east"], approx);
  size_t d = omega.dimension();
  field uh (Xh);
  uh["west"] = lazy_interpolate (Xh_west, u_west(d));
  uh["east"] = lazy_interpolate (Xh_east, u_east(d));
  size_t k = Xh.degree();
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(2*k+1);
  point n_interf; // BUG_INT_BDR_JUMP: TODO: fix the sign of the interface: should be done when using jump
	          // when the "ds" interface measure has the opposite sign, e.g. "interface2"
  if (d != 3) n_interf = point(1, 0, 0);
  else        n_interf = point(0, 1, 0);
  Float value_jump    = integrate (omega["interface"],  jump(uh)*dot(n_interf,normal()), qopt);
  Float value_average = integrate (omega["interface"],  average(uh), qopt);
  Float value_jump_expected    =  u_west::integrate_interface(d) - u_east::integrate_interface(d);
  Float value_average_expected = (u_west::integrate_interface(d) + u_east::integrate_interface(d))/2;
  Float err_jump = max(abs(value_jump    - value_jump_expected),
                  abs(value_average - value_average_expected));
  derr << setprecision(16);
  derr << "value_jump             = " << value_jump << endl;
  derr << "value_jump_expected    = " << value_jump_expected << endl;
  derr << "value_average          = " << value_average << endl;
  derr << "value_average_expected = " << value_average_expected << endl;
  derr << "err_jump               = " << err_jump << endl
       << endl;
  return (err_jump < tol)  ? 0 : 1;
}
// ----------------------------------------------------------------------------
int test_mass(geo omega, string approx, Float tol) {
// ----------------------------------------------------------------------------
  space Xh (omega, approx),
        Xh_west (omega["west"], approx),
        Xh_east (omega["east"], approx);
  size_t d = omega.dimension();
  field uh (Xh);
  uh["west"] = lazy_interpolate (Xh_west, u_west(d));
  uh["east"] = lazy_interpolate (Xh_east, u_east(d));
  trial u(Xh); test v(Xh);
  form m = integrate(u*v);
  field one (Xh,1.);
  Float integrate_u = m(uh,one);
  Float integrate_u_expected = u_west::integrate_region(d) + u_east::integrate_region(d);
  Float err_mass = fabs(integrate_u_expected - integrate_u);
  derr << "integrate_u          = " << integrate_u << endl
       << "integrate_u_expected = " << integrate_u_expected << endl
       << "err_mass             = " << err_mass << endl
       << endl;
  return (err_mass < tol) ? 0 : 1;
}
// ----------------------------------------------------------------------------
int main(int argc, char**argv) {
// ----------------------------------------------------------------------------
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  string approx  = (argc > 2) ?      argv[2]  : "P1d";
  Float tol_jump = (argc > 3) ? atof(argv[3]) : 1e300;
  Float tol_mass = (argc > 4) ? atof(argv[4]) : tol_jump;
  size_t status = 0;
  status += test_jump (omega, approx, tol_jump);
  status += test_mass (omega, approx, tol_mass);
  return status;
}
