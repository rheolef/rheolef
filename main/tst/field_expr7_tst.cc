///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check: field x = y[dom];     with a space cstor
//        field x = y[dom]/2;   with a space cstor
// and    field x (Wh, y[dom]); without
//        field x (Wh, y[dom]/2);
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float f  (const point& x) { return sin(x[0]+x[1]+0.5); }

int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  string dom_name = (argc > 2) ? argv[2] : "right";
  Float prec = (argc > 3) ? atof(argv[3]) : 1e-10;
  space Vh (omega, "P1");
  space Wh (omega[dom_name], "P1");
  field fh = lazy_interpolate (Vh, f);
  field gh1 = fh[dom_name]/2;	// re-build Wh like space
  field gh2 = fh[dom_name];     // re-build Wh like space
  field gh  = (2*gh1 + gh2)/2;
  dout << round (gh, prec);
}
