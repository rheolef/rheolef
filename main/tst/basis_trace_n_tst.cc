///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// basis_trace_n: "trace_n(RTkd)"
// for HDG postprocessing: Lagrange multiplier bi-valued on sides
// it is as a DG field, e.g. P3d but values in any element K
// are only defined on the boundary partial K
// 
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "jump_dg_tst.h"

// cosinusprod.icc
struct u_exact {
  Float operator() (const point& x) const {
    return cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  u_exact(size_t d1) : d(d1), pi(acos(Float(-1.0))) {}
  size_t d; Float pi;
};
// ----------------------------------------------------------------------------
// io debug utilities
// ----------------------------------------------------------------------------
template <class Expr>
void put_field (string name, const Expr& expr) {
  field lh = integrate(expr);
  odiststream out (name+".field");
  out << lh.u();
  out.close();
}
template <class Expr>
void put_form (string name, const geo& omega, const Expr& expr) {
  form ah = integrate(omega,expr);
  odiststream out (name+".mtx");
  out << ah.uu();
  out.close();
}
// ----------------------------------------------------------------------------
size_t test_meas_sides (geo omega, string approx, Float eps) {
// ----------------------------------------------------------------------------
  space Mhs (omega, approx);
  warning_macro("Mhs("<<Mhs.get_geo().name()<<","<<Mhs.get_basis().name()<<").dis_ndof="<<Mhs.dis_ndof());
  check_macro (Mhs.get_basis().option().is_trace_n(),
	"expect a normal trace basis, e.g. trace_n(RT1d)");
  field lambda_hs (Mhs, 1.);
  trial lambda_s (Mhs); test mu_s (Mhs);
  field lhs = integrate (omega, on_local_sides(mu_s));
  form  m   = integrate (omega, on_local_sides(lambda_s*mu_s));
  Float meas_sides0 = 2*integrate (omega["internal_sides"]) + integrate (omega["boundary"]);
  Float meas_sides1 = dual(lambda_hs,lhs); // internal sides are counted twice
  Float meas_sides2 = m (lambda_hs,lambda_hs);
  Float meas_sides_err = fabs(meas_sides1 - meas_sides0) + fabs(meas_sides2 - meas_sides0);
  derr << "test_meas_sides : " << endl
       << "meas_sides0    = "<< meas_sides0 << endl
       << "meas_sides1    = "<< meas_sides1 << endl
       << "meas_sides2    = "<< meas_sides2 << endl
       << "meas_sides_err = "<< meas_sides_err << endl
       << endl;
  return (meas_sides_err < eps) ? 0 : 1;
}
// ----------------------------------------------------------------------------
size_t test_indirect (geo omega, string approx, Float tol) {
// ----------------------------------------------------------------------------
  // just print values: visual check
  if (!omega.have_domain_indirect("interface")) return 0;
  space Mhs (omega, approx);
  space Mhs_west (omega["west"], approx);
  field lambda_hs (Mhs, 0.);
  field lambda_hs_west (Mhs_west, 0.);
  lambda_hs_west ["interface"] = 1; // BUG in field_indirect & Pkd on interface
  lambda_hs["west"] = lambda_hs_west;
  dout << catchmark("lambda_s") << lambda_hs
       << catchmark("lambda_s_west") << lambda_hs_west;
  return 0;
}
// ----------------------------------------------------------------------------
size_t test_interface (geo omega, string approx, Float eps, interface_side check) {
// ----------------------------------------------------------------------------
  // check: on_local_sides(lambda*v), lambda in trace_n(RTkd) & v in Pkd
  // where lambda_h = indic(interface) and uh={u_weast,u_east}
  if (!omega.have_domain_indirect("interface")) return 0;
  space Mhs      (omega, approx),
        Mhs_west (omega["west"], approx),
        Mhs_east (omega["east"], approx);
  size_t k = Mhs.degree();
  space Xh      (omega,         "P"+std::to_string(k)+"d"),
        Xh_west (omega["west"], "P"+std::to_string(k)+"d"),
        Xh_east (omega["east"], "P"+std::to_string(k)+"d");
  space Mh  (omega["sides"], "P"+std::to_string(k)+"d");
  size_t d = omega.dimension();
  field lambda_hs(Mhs, 0.), lambda_h (Mh , 0.);
  lambda_h ["interface"] = 1;
  // indirect way to do: lambda_hs ["interface"] = 1;
  // TODO: try a better way ?
  field lambda_hs_west (Mhs_west, 0.);
  field lambda_hs_east (Mhs_east, 0.);
  lambda_hs_west ["interface"] = 1;
  lambda_hs_east ["interface"] = 1;
  lambda_hs["west"] = lambda_hs_west;
  lambda_hs["east"] = lambda_hs_east;
  field uh (Xh, 0.);
  if (check == west_side || check == both_side) {
    uh["west"] = lazy_interpolate (Xh_west, p_west(d,k));
  }
  if (check == east_side || check == both_side) {
    uh["east"] = lazy_interpolate (Xh_east, p_east(d,k));
  }
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(2*k+1);
  trial lambda_s(Mhs), lambda(Mh); test v(Xh);
  form  m   = integrate (omega, on_local_sides(lambda*v), qopt);
  field lh  = integrate (omega, on_local_sides(lambda*uh), qopt);
  form  ms  = integrate (omega, on_local_sides(lambda_s*v), qopt);
  field lhs = integrate (omega, on_local_sides(lambda_s*uh), qopt);
  Float value_interface_m        = m (lambda_h, uh);
  Float value_interface_l        = dual (lh,  lambda_h);
  Float value_interface_sm       = ms(lambda_hs,uh);
  Float value_interface_sl       = dual (lhs, lambda_hs);
  Float value_interface_expected = 0;
  if (check == west_side || check == both_side) {
    value_interface_expected += p_west::integrate_interface(d,k);
  }
  if (check == east_side || check == both_side) {
    value_interface_expected += p_east::integrate_interface(d,k);
  }
  Float error_interface_s        = fabs(value_interface_sm - value_interface_expected)
                                 + fabs(value_interface_sl - value_interface_expected);
  Float error_interface_h        = fabs(value_interface_sm - value_interface_m)
                                 + fabs(value_interface_sl - value_interface_l);
  derr << setprecision(16)
       << "test_interface : " << side_name[check] << endl
       << "value_interface_sm       = " << value_interface_sm << endl
       << "value_interface_sl       = " << value_interface_sl << endl
       << "value_interface_m        = " << value_interface_m << endl
       << "value_interface_l        = " << value_interface_l << endl
       << "value_interface_expected = " << value_interface_expected << endl
       << "error_interface_s        = " << error_interface_s << endl
       << "error_interface_h        = " << error_interface_h << endl
       << endl;
#ifdef TO_CLEAN
  odiststream dump ("dump.field", io::nogz);
  dump << catchmark("lambda_s") << lambda_hs
       << catchmark("u") << uh
      ;
  dump.close();
#endif // TO_CLEAN
  return (error_interface_s < eps && error_interface_h < eps)  ? 0 : 1;
}
// ----------------------------------------------------------------------------
size_t test_interface_variant (geo omega, string approx, Float eps, interface_side check) {
// ----------------------------------------------------------------------------
  // check: on_local_sides(lambda*v), lambda in trace_n(RTkd) & v in Pkd
  // where lambda_h = indic(interface)*{u_weast,u_east} and uh=1
  if (!omega.have_domain_indirect("interface")) return 0;
  size_t d = omega.dimension();
  space Mhs      (omega, approx),
        Mhs_west (omega["west"], approx),
        Mhs_east (omega["east"], approx);
  size_t k = Mhs.degree();
  field indic_interface_h (Mhs,0.);
  field indic_interface_h_west (Mhs_west, 0.);
  field indic_interface_h_east (Mhs_east, 0.);
  indic_interface_h_west ["interface"] = 1;
  indic_interface_h_east ["interface"] = 1;
  indic_interface_h["west"] = indic_interface_h_west;
  indic_interface_h["east"] = indic_interface_h_east;
#ifdef DVT_HDG_POST_INTERPOLATE
  field lambda_hs = lazy_interpolate (Mhs, interface_indicator*indic_interface_h);
#endif // DVT_HDG_POST_INTERPOLATE
  field lambda_hs (Mhs);
  lambda_hs["west"] = lazy_interpolate(Mhs_west, p_west(d,k));
  lambda_hs["east"] = lazy_interpolate(Mhs_east, p_east(d,k));
  for (size_t idof = 0; idof < lambda_hs.ndof(); ++idof) {
    lambda_hs.dof(idof) *= indic_interface_h.dof(idof);
  }
  space Xh (omega, "P"+std::to_string(k)+"d");
  field uh (Xh, 1.);
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(2*k+1);
  trial lambda_s(Mhs); test v(Xh);
  form  ms  = integrate (omega, on_local_sides(lambda_s*v), qopt);
  field lhs = integrate (omega, on_local_sides(lambda_s*uh), qopt);
  Float value_interface_expected = p_west::integrate_interface(d,k)
    			         + p_east::integrate_interface(d,k);
  Float value_interface_sm       = ms(lambda_hs,uh);
  Float value_interface_sl       = dual (lhs, lambda_hs);
  Float error_interface_s        = fabs(value_interface_sm - value_interface_expected)
                                 + fabs(value_interface_sl - value_interface_expected);
  derr << setprecision(16)
       << "test_interface_variant : " << side_name[check] << endl
       << "value_interface_sm       = " << value_interface_sm << endl
       << "value_interface_sl       = " << value_interface_sl << endl
       << "value_interface_expected = " << value_interface_expected << endl
       << "error_interface_s        = " << error_interface_s << endl
       << endl;
  return (error_interface_s < eps)  ? 0 : 1;
}
// ----------------------------------------------------------------------------
size_t test_interface_variant2 (geo omega, string approx, Float eps) {
// ----------------------------------------------------------------------------
  // check: on_local_sides(lambda*mu), lambda, mu in Pkd[sides]
  // where lambda_h = {u_weast,u_east} and mu_h = indic(interface)
  if (!omega.have_domain_indirect("interface")) return 0;
  size_t d = omega.dimension();
  space Mhs      (omega, approx),
        Mhs_west (omega["west"], approx),
        Mhs_east (omega["east"], approx);
  size_t k = Mhs.degree();
  field indic_interface_h (Mhs,0.);
  field indic_interface_h_west (Mhs_west, 0.);
  field indic_interface_h_east (Mhs_east, 0.);
  indic_interface_h_west ["interface"] = 1;
  indic_interface_h_east ["interface"] = 1;
  indic_interface_h["west"] = indic_interface_h_west;
  indic_interface_h["east"] = indic_interface_h_east;
#ifdef DVT_HDG_POST_INTERPOLATE
  // fails during interppolate: have to evaluate indic_interface_h & basis_fem_sides::eval()=undef
  field lambda_hs = lazy_interpolate (Mhs, interface_indicator*indic_interface_h);
#endif // DVT_HDG_POST_INTERPOLATE
  field lambda_hs (Mhs);
  lambda_hs["west"] = lazy_interpolate(Mhs_west, p_west(d,k));
  lambda_hs["east"] = lazy_interpolate(Mhs_east, p_east(d,k));
  for (size_t idof = 0; idof < lambda_hs.ndof(); ++idof) {
    lambda_hs.dof(idof) *= indic_interface_h.dof(idof);
  }
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(2*k+1);
  trial lambda_s(Mhs); test mu_s(Mhs);
  form  ms  = integrate (omega, on_local_sides(lambda_s*mu_s), qopt);
  field lhs = integrate (omega, on_local_sides(lambda_s*indic_interface_h), qopt);
  Float value_interface_expected = p_west::integrate_interface(d,k)
    			         + p_east::integrate_interface(d,k);
  Float value_interface_sm       = ms(lambda_hs,indic_interface_h);
  Float value_interface_sl       = dual (lhs, lambda_hs);
  Float error_interface_s        = fabs(value_interface_sm - value_interface_expected)
                                 + fabs(value_interface_sl - value_interface_expected);
  derr << setprecision(16)
       << "test_interface_variant2" << endl
       << "value_interface_sm       = " << value_interface_sm << endl
       << "value_interface_sl       = " << value_interface_sl << endl
       << "value_interface_expected = " << value_interface_expected << endl
       << "error_interface_s        = " << error_interface_s << endl
       << endl;
#ifdef TO_CLEAN
  odiststream dump ("dump.field", io::nogz);
  dump << catchmark("indic_interface") << indic_interface_h
       << catchmark("lambda_s") << lambda_hs
      ;
  dump.close();
#endif // TO_CLEAN
  return (error_interface_s < eps)  ? 0 : 1;
}
// ----------------------------------------------------------------------------
size_t test_interface_with_normal (geo omega, string approx, Float eps, string vec_var) {
// ----------------------------------------------------------------------------
  // on_local_sides(lambda*dot(tau,n)) with lamba in trace_n(RTkd) & tau in Pkd(Omega)^d
  // where lambda_h = {u_weast,u_east}*indic(interface) & sigma=normal_interface
  if (!omega.have_domain_indirect("interface")) return 0;
  size_t d = omega.dimension();
  space Mhs      (omega, approx),
        Mhs_west (omega["west"], approx),
        Mhs_east (omega["east"], approx);
  size_t k = Mhs.degree();
  field indic_interface_h (Mhs,0.);
  field indic_interface_h_west (Mhs_west, 0.);
  field indic_interface_h_east (Mhs_east, 0.);
  indic_interface_h_west ["interface"] = 1;
  indic_interface_h_east ["interface"] = 1;
  indic_interface_h["west"] = indic_interface_h_west;
  indic_interface_h["east"] = indic_interface_h_east;
#ifdef DVT_HDG_POST_INTERPOLATE
  // fails during interppolate: have to evaluate indic_interface_h & basis_fem_sides::eval()=undef
  field lambda_hs = lazy_interpolate (Mhs, interface_indicator*indic_interface_h);
#endif // DVT_HDG_POST_INTERPOLATE
  field lambda_hs (Mhs);
  lambda_hs["west"] = lazy_interpolate(Mhs_west, p_west(d,k));
  lambda_hs["east"] = lazy_interpolate(Mhs_east, p_east(d,k));
  for (size_t idof = 0; idof < lambda_hs.ndof(); ++idof) {
    lambda_hs.dof(idof) *= indic_interface_h.dof(idof);
  }
  space Th;
  if (vec_var == "RT") {
    Th = space (omega, "RT"+std::to_string(k)+"d");
  } else {
    Th = space (omega, "P"+std::to_string(k)+"d", "vector");
  }
  field sigma_h = lazy_interpolate (Th, interface_normal(d));
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(2*k+1);
  trial lambda_s(Mhs); test tau(Th);
  form  ms  = integrate (omega, on_local_sides(lambda_s*dot(tau,normal())), qopt);
  field lhs = integrate (omega, on_local_sides(lambda_s*dot(sigma_h,normal())), qopt);
  Float value_interface_expected = p_west::integrate_interface(d,k)
    			         - p_east::integrate_interface(d,k);
  Float value_interface_sm       = ms(lambda_hs,sigma_h);
  Float value_interface_sl       = dual (lhs, lambda_hs);
  Float error_interface_s        = fabs(value_interface_sm - value_interface_expected)
                                 + fabs(value_interface_sl - value_interface_expected);
  derr << setprecision(16)
       << "test_interface_with_normal : approx = " << Th.name() << endl
       << "value_interface_sm       = " << value_interface_sm << endl
       << "value_interface_sl       = " << value_interface_sl << endl
       << "value_interface_expected = " << value_interface_expected << endl
       << "error_interface_s        = " << error_interface_s << endl
       << endl;
  return (error_interface_s < eps)  ? 0 : 1;
}
// ----------------------------------------------------------------------------
size_t test_interface_with_normal_variant (geo omega, string approx, Float eps, string vec_var) {
// ----------------------------------------------------------------------------
  // on_local_sides(lambda*dot(tau,n)) with lamba in trace_n(RTkd) & tau in Pkd(Omega)^d
  // where lambda_h = indic(interface) & sigma=normal_interface*{u_weast,u_east}
  if (!omega.have_domain_indirect("interface")) return 0;
warning_macro("var...");
  size_t d = omega.dimension();
  space Mhs      (omega, approx),
        Mhs_west (omega["west"], approx),
        Mhs_east (omega["east"], approx);
  size_t k = Mhs.degree();
  field indic_interface_h (Mhs,0.);
  field indic_interface_h_west (Mhs_west, 0.);
  field indic_interface_h_east (Mhs_east, 0.);
  indic_interface_h_west ["interface"] = 1;
  indic_interface_h_east ["interface"] = 1;
  indic_interface_h["west"] = indic_interface_h_west;
  indic_interface_h["east"] = indic_interface_h_east;
  space Th, Th_west, Th_east;
  if (vec_var == "RT") {
    Th      = space (omega,         "RT"+std::to_string(k)+"d");
    Th_west = space (omega["west"], "RT"+std::to_string(k)+"d");
    Th_east = space (omega["east"], "RT"+std::to_string(k)+"d");
  } else {
    Th      = space (omega,         "P"+std::to_string(k)+"d", "vector");
    Th_west = space (omega["west"], "P"+std::to_string(k)+"d", "vector");
    Th_east = space (omega["east"], "P"+std::to_string(k)+"d", "vector");
  }
  field sigma_h (Th);
#ifdef DVT_FIELD_INDIRECT_VECTOR
  // merge all cases here
  // RTk: fatal(../../../rheolef/nfem/plib/space_constitution.cc,91): unsupported vector-product of vector-valued approximation "RT1d"
  // Pk : fatal(../../include/rheolef/field_expr.h,393): field [domain] = field_expression : incompatible spaces P1d(mesh-2d-r[west]) and vector(P1d(mesh-2d-r[west]))
  sigma_h["west"] = lazy_interpolate (Th_west, p_interface_normal_west(d,k));
  sigma_h["east"] = lazy_interpolate (Th_east, p_interface_normal_east(d,k));
#endif // DVT_FIELD_INDIRECT_VECTOR

warning_macro("var(1)...");
  if (vec_var == "P") {
    // BUG_RT_INTERPOLATE
    // RTk: fatal(../../../rheolef/nfem/gbasis/basis_on_pointset.cc,617): index iloc=72 out of range [0:72[
    space X0h (omega, "P0");
    field indic_w (X0h, 0.); indic_w["west"] = 1;
    field indic_e (X0h, 0.); indic_e["east"] = 1;
    sigma_h = lazy_interpolate (Th, indic_w*p_interface_normal_west(d,k)
                             + indic_e*p_interface_normal_east(d,k));
  } else {
    // circumvent BUG_RT_INTERPOLATE
    space X0h (omega, "P0");
    field indic_w (X0h, 0.); indic_w["west"] = 1;
    field indic_e (X0h, 0.); indic_e["east"] = 1;
    space T0h (omega, "P"+std::to_string(k)+"d", "vector");
    field sigma_h0 = lazy_interpolate (T0h, indic_w*p_interface_normal_west(d,k)
                                     + indic_e*p_interface_normal_east(d,k));
    // proj L2 from Th0 to Th=RTk
    trial sigma(Th); test tau(Th);
    integrate_option iopt;
    iopt.invert = true;
    form inv_mrt = integrate (dot(sigma,tau), iopt);
    field lh = integrate (dot(sigma_h0,tau));
    sigma_h = inv_mrt*lh;
  }
warning_macro("var(2)...");
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(2*k+1);
  trial lambda_s(Mhs); test tau(Th);
  form  ms  = integrate (omega, on_local_sides(lambda_s*dot(tau,normal())), qopt);
  field lhs = integrate (omega, on_local_sides(lambda_s*dot(sigma_h,normal())), qopt);
  Float value_interface_expected = p_west::integrate_interface(d,k)
    			         - p_east::integrate_interface(d,k);
  Float value_interface_sm       = ms(indic_interface_h,sigma_h);
  Float value_interface_sl       = dual (lhs, indic_interface_h);
  Float error_interface_s        = fabs(value_interface_sm - value_interface_expected)
                                 + fabs(value_interface_sl - value_interface_expected);
  derr << setprecision(16)
       << "test_interface_with_normal_variant : approx = " << Th.name() << endl
       << "value_interface_sm       = " << value_interface_sm << endl
       << "value_interface_sl       = " << value_interface_sl << endl
       << "value_interface_expected = " << value_interface_expected << endl
       << "error_interface_s        = " << error_interface_s << endl
       << endl;
warning_macro("var done");
  return (error_interface_s < eps)  ? 0 : 1;
}
// ----------------------------------------------------------------------------
size_t test_grad_on_local_sides (geo omega, string approx, Float eps) {
// ----------------------------------------------------------------------------
  // grad_h on local sides, used by hho reconstruction:
  //   lambda1 = du/dn on partial K
  //   lambda2 = sigma.n on partial K and sigma = grad(u) on k
  // should be equal
  if  (omega.sizes().ownership_by_variant[reference_element::q].dis_size() != 0 ||
       omega.sizes().ownership_by_variant[reference_element::P].dis_size() != 0 ||
       omega.sizes().ownership_by_variant[reference_element::H].dis_size() != 0) {
    // exact sigma_h = grad(uh) : only for simplicial elements "etT"
    return 0;
  }
  space Mhs (omega, approx);
  size_t d = omega.dimension();
  size_t k = Mhs.degree();
  space Xh (omega, "P"+std::to_string(k+1)+"d");
  space Th (omega, "P"+std::to_string(k)+"d", "vector");
  trial lambda_s(Mhs), sigma(Th);
  test  mu_s    (Mhs), tau  (Th);
  integrate_option iopt;
  iopt.invert = true;
  iopt.set_order (2*(k+1)+1); 
  field uh = lazy_interpolate (Xh, u_exact(d));
  form inv_ms = integrate (omega, on_local_sides(lambda_s*mu_s), iopt);
  field lh1   = integrate (omega, on_local_sides(dot(normal(),grad_h(uh))*mu_s), iopt);
  field lambda1_h = inv_ms*lh1;
  form inv_m  = integrate (dot(sigma,tau), iopt);
  field kh    = integrate (omega, dot(grad_h(uh),tau), iopt);
  field sigma_h = inv_m*kh;
  field lh2   = integrate (omega, on_local_sides(dot(normal(),sigma_h)*mu_s), iopt);
  field lambda2_h = inv_ms*lh2;
  Float err_grad_on_local_sides = field(lambda2_h - lambda1_h).max_abs();
  derr << "err_grad_on_local_sides = " << err_grad_on_local_sides << endl;
  return (err_grad_on_local_sides < eps)  ? 0 : 1;
}
// ----------------------------------------------------------------------------
int main(int argc, char**argv) {
// ----------------------------------------------------------------------------
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "trace_n(RT1d)";
  Float eps  = 1e5*numeric_limits<Float>::epsilon();
  Float eps2 = sqrt(numeric_limits<Float>::epsilon());
  size_t status = 0;
  status += test_meas_sides            (omega, approx, eps2);
  status += test_interface             (omega, approx, eps, west_side);
  status += test_interface             (omega, approx, eps, east_side);
  status += test_interface             (omega, approx, eps, both_side);
  status += test_interface_variant     (omega, approx, eps, west_side);
  status += test_interface_variant2    (omega, approx, eps);
  status += test_interface_with_normal (omega, approx, eps, "P");
  status += test_interface_with_normal (omega, approx, eps, "RT");
  status += test_interface_with_normal_variant (omega, approx, eps, "P");
  status += test_interface_with_normal_variant (omega, approx, eps, "RT");
#ifdef SKIPPED
  status += test_indirect   (omega, approx, eps);
#endif // SKIPPED
  status += test_grad_on_local_sides (omega, approx, eps);
  return status;
}
