///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct phi_f {
  Float operator() (const point& x) const { return x[0]+2*x[1]+3*x[2]; }
};
struct u_f {
  point operator() (const point& x) const { return point(x[0]+2*x[1]+3*x[2], 5*x[1]+x[2], 3*x[2]); }
  static tensor grad(size_t d);
};
tensor
u_f::grad(size_t d) {
  tensor g;
  switch (d) {
    case 0: break;
    case 1: g(0,0)=1; break;
    case 2: g = {{1,2,0},
                 {0,5,0},
                 {0,0,0}}; break;
    default: g={{1,  2,  3},
                {0,  5,  1},
                {0,  0,  3}}; break;
  }
  return g;
};
struct D_u_f {
  tensor operator() (const point& x) const { return g; }
  D_u_f (size_t d) : g (u_f::grad(d)) { g = (g+trans(g))/2; }
  tensor g;
};
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega(argv[1]);
  string approx = argv[2];
  Float tol = (argc > 3) ? atof(argv[3]) : 1e-10;
  size_t d = omega.dimension();
  Float err = 0;
  // ------------------------- 
  // 1) grad(phi_h)
  // ------------------------- 
  {
    space Xh (omega, approx);
    size_t k = Xh.degree();
    string grad_approx = "P" + std::to_string(k-1) + "d";
    space Th (omega, grad_approx, "vector");
    trial phi (Xh); test psi (Xh);
    trial sigma (Th); test tau (Th);
    field phi_h = lazy_interpolate (Xh, phi_f());
    quadrature_option qopt;
    qopt.set_order(2*k+2);
    integrate_option fopt = qopt;
    fopt.invert = true;
    form inv_mass = integrate (dot(sigma,tau), fopt);
    form mgrad = integrate (dot(grad(phi),tau));
    field grad_phi_h = inv_mass*(mgrad*phi_h);
  
    // 1.1) grad(phi_h) in integrate()
    Float err_grad_1 = integrate (omega, norm(grad(phi_h)-grad_phi_h), qopt);
    derr << "err_grad_1 = " << err_grad_1 << endl;
    err = std::max (err, err_grad_1);

    // 1.2) grad(phi_h) in lazy_interpolate()
    field grad_phi_h2 = lazy_interpolate (Th, grad(phi_h));
    Float err_grad_2 = field(grad_phi_h - grad_phi_h2).max_abs();
    derr << "err_grad_2 = " << err_grad_2 << endl;
    err = std::max (err, err_grad_2);
  }
  // ------------------------- 
  // 2) D(uh)
  // ------------------------- 
  {
    space Xh (omega, approx, "vector");
    size_t k = Xh.degree();
    string grad_approx = "P" + std::to_string(k-1) + "d";
    space Th (omega, grad_approx, "tensor");
    trial u (Xh);     test v (Xh);
    trial sigma (Th); test tau (Th);
    field uh = lazy_interpolate (Xh, u_f());
    field pi_h_Du = lazy_interpolate (Th, D_u_f(d));

    // 2.1) D(u) in expression
    quadrature_option qopt;
    qopt.set_order(2*k+6);
    integrate_option fopt = qopt;
    fopt.invert = true;
    form inv_mass = integrate (ddot(sigma,tau), fopt);
    form mD = integrate (ddot(D(u),tau));
    field D_uh = inv_mass*(mD*uh);
    Float err_du_1 = integrate (omega, norm(pi_h_Du-D_uh), qopt);
    derr << "err_du_1 = " << err_du_1 << endl;
    err = std::max (err, err_du_1);

    // 2.2) D(uh) in integrate()
    Float err_du_2 = integrate (omega, norm(pi_h_Du-D(uh)), qopt);
    derr << "err_du_2 = " << err_du_2 << endl;
    err = std::max (err, err_du_2);
  
    // 2.3) D(uh) in lazy_interpolate()
    field D_uh2 = lazy_interpolate (Th, D(uh));
    Float err_du_3 = field(D_uh - D_uh2).max_abs();
    derr << "err_du_3 = " << err_du_3 << endl;
    err = std::max (err, err_du_3);
#ifdef TO_CLEAN
    derr << "Du_f="<<D_u_f(d)(point())<<endl;
    odiststream out1 ("pi_h_du.field", io::nogz); out1 << pi_h_Du;
    odiststream out2 ("du1.field", io::nogz);     out2 << D_uh;
    odiststream out3 ("du2.field", io::nogz);     out3 << D_uh2;
#endif // TO_CLEAN
  }
  // ------------------------- 
  // ------------------------- 
  // 3) div(uh)
  // ------------------------- 
  {
    space Xh (omega, approx, "vector");
    size_t k = Xh.degree();
    string grad_approx = "P" + std::to_string(k-1) + "d";
    space Th (omega, grad_approx);
    trial u (Xh); test v (Xh);
    trial p (Th); test q (Th);
    field uh = lazy_interpolate (Xh, u_f());
    integrate_option fopt;
    fopt.invert = true;
    form inv_mass = integrate (p*q, fopt);
    form m_div = integrate (div(u)*q);
    field div_uh = inv_mass*(m_div*uh);

    // 3.1) in integrate()
    quadrature_option qopt;
    qopt.set_order(5);
    Float err5 = integrate (omega, abs(div(uh)-div_uh), qopt);
    derr << "err5 = " << err5 << endl;
    err = std::max (err, err5);
  
    // 3.2) in lazy_interpolate()
    field div_uh2 = lazy_interpolate (Th, div(uh));
    Float err6 = field(div_uh - div_uh2).max_abs();
    derr << "err6 = " << err6 << endl;
    err = std::max (err, err6);
  }
  // ------------------------- 
  return (err < tol) ? 0 : 1;
}
