///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// given u1h on a mesh omega1, re-interpolate it on a mesh omega2 as u2h
//
// usage: prog omega1 omega2
//
// note: see also interpolate_dom[234]_tst.cc
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct u {
  Float operator() (const point& x) const { return x[0]+x[1]+x[2]; }
};
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  geo omega1 (argv[1]);
  geo omega2 (argv[2]);
  Float tol = (argc > 3) ? atof(argv[3]) : 1e-7;
  space V1h (omega1, "P1");
  space V2h (omega2, "P1");
  field u1h     = lazy_interpolate (V1h, u());
  field u2h     = lazy_interpolate (V2h, u1h);
  field u2h_bis = lazy_interpolate (V2h, u());
  field eh = u2h - u2h_bis;
  Float err = dual(eh,eh);
  derr << "err = " << err << endl;
  return (err < tol) ? 0 : 1;
}
