#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
# a way to compute 2*pi with gmsh
#	gmsh -1 -order 3 circle_s_gmsh.mshcad -o aa.msh
#	gawk -f circle_s_p3_check.awk < aa.msh
# => check convergence with mesh size & order
# ---------------------------------------------------
# functions
# ---------------------------------------------------
function max(x,y)
{
  if (x > y) return x; else return y;
}
function fabs(x,y)
{
  return max(x,-x);
}
# quadrature: Gauss, size=12, exact up to order=6
function set_quadrature()
{
  nquad = 12;
  w[1]  = 0.025422453185103; s1[1]  = 0.063089014491502; s2[1]  = 0.063089014491502
  w[2]  = 0.025422453185103; s1[2]  = 0.873821971016996; s2[2]  = 0.063089014491502
  w[3]  = 0.025422453185103; s1[3]  = 0.063089014491502; s2[3]  = 0.873821971016996
  w[4]  = 0.058393137863189; s1[4]  = 0.24928674517091;  s2[4]  = 0.24928674517091
  w[5]  = 0.058393137863189; s1[5]  = 0.50142650965818;  s2[5]  = 0.24928674517091
  w[6]  = 0.058393137863189; s1[6]  = 0.24928674517091;  s2[6]  = 0.50142650965818
  w[7]  = 0.041425537809187; s1[7]  = 0.310352451033785; s2[7]  = 0.053145049844816
  w[8]  = 0.041425537809187; s1[8]  = 0.053145049844816; s2[8]  = 0.310352451033785
  w[9]  = 0.041425537809187; s1[9]  = 0.636502499121399; s2[9]  = 0.310352451033785
  w[10] = 0.041425537809187; s1[10] = 0.636502499121399; s2[10] = 0.053145049844816
  w[11] = 0.041425537809187; s1[11] = 0.310352451033785; s2[11] = 0.636502499121399
  w[12] = 0.041425537809187; s1[12] = 0.053145049844816; s2[12] = 0.636502499121399
}
### # basis & derivatives: order 3, Lagrange, equispaced point set
### function compute_phi(s)
### {
###   nbasis = order+1;
###   if (order == 1) {
###     phi[1] = -s+1.0;
###     phi[2] =  s;
###   } else if (order == 2) {
###     phi[1] = -3.0*s+2.0*(s*s)+1.0;
###     phi[2] = -s+2.0*(s*s);
###     phi[3] = -4.0*(s*s)+4.0*s;
###   } else if (order == 3) {
###     phi[1] =  -(11.0/2.0)*s-(9.0/2.0)*(s*s*s)+9.0*(s*s)+1.0;
###     phi[2] =  s-(9.0/2.0)*(s*s)+(9.0/2.0)*(s*s*s);
###     phi[3] =  (27.0/2.0)*(s*s*s)+9.0*s-(45.0/2.0)*(s*s);
###     phi[4] =  18.0*(s*s)-(9.0/2.0)*s-(27.0/2.0)*(s*s*s);
###   } else if (order == 4) {
###     phi[1] =  (32.0/3.0)*((s*s)*(s*s))+(70.0/3.0)*(s*s)-(25.0/3.0)*s-(80.0/3.0)*(s*s*s)+1.0;
###     phi[2] =  -16.0*(s*s*s)+(22.0/3.0)*(s*s)-s+(32.0/3.0)*((s*s)*(s*s));
###     phi[3] =  -(208.0/3.0)*(s*s)+96.0*(s*s*s)+16.0*s-(128.0/3.0)*((s*s)*(s*s));
###     phi[4] =  64.0*((s*s)*(s*s))-128.0*(s*s*s)+76.0*(s*s)-12.0*s;
###     phi[5] =  (16.0/3.0)*s+(224.0/3.0)*(s*s*s)-(112.0/3.0)*(s*s)-(128.0/3.0)*((s*s)*(s*s));
###   } else if (order == 5) {
###     phi[1] =  -(137.0/12.0)*s-(2125.0/24.0)*(s*s*s)+(375.0/8.0)*(s*s)-(625.0/24.0)*(s*(s*s)*(s*s))+(625.0/8.0)*((s*s)*(s*s))+1.0;
###     phi[2] =  -(125.0/12.0)*(s*s)+(875.0/24.0)*(s*s*s)+s+(625.0/24.0)*(s*(s*s)*(s*s))-(625.0/12.0)*((s*s)*(s*s));
###     phi[3] =  25.0*s+(3125.0/24.0)*(s*(s*s)*(s*s))-(4375.0/12.0)*((s*s)*(s*s))+(8875.0/24.0)*(s*s*s)-(1925.0/12.0)*(s*s);
###     phi[4] =  (2675.0/12.0)*(s*s)-(7375.0/12.0)*(s*s*s)-(3125.0/12.0)*(s*(s*s)*(s*s))-25.0*s+(8125.0/12.0)*((s*s)*(s*s));
###     phi[5] =  (3125.0/12.0)*(s*(s*s)*(s*s))-625.0*((s*s)*(s*s))+(50.0/3.0)*s+(6125.0/12.0)*(s*s*s)-(325.0/2.0)*(s*s);
###     phi[6] =  -(3125.0/24.0)*(s*(s*s)*(s*s))+(6875.0/24.0)*((s*s)*(s*s))+(1525.0/24.0)*(s*s)-(5125.0/24.0)*(s*s*s)-(25.0/4.0)*s;
###   }
### }
function compute_dphi_ds(s1,s2)
{
  nbasis = (order+1)*(order+2)/2;
  if (order == 1) {
    dphi_ds[1,1] = -1.0;
    dphi_ds[1,2] = -1.0;
    dphi_ds[2,1] =  1.0;
    dphi_ds[2,2] =  0.0;
    dphi_ds[3,1] =  0.0;
    dphi_ds[3,2] =  1.0;
  } else if (order == 2) {
    dphi_ds[1,1] =  4.0*s2+4.0*s1-3.0;
    dphi_ds[1,2] =  4.0*s2+4.0*s1-3.0;
    dphi_ds[2,1] =  4.0*s1-1.0;
    dphi_ds[2,2] =  0.0;
    dphi_ds[3,1] =  0.0;
    dphi_ds[3,2] =  4.0*s2-1.0;
    dphi_ds[4,1] = -4.0*s2-8.0*s1+4.0;
    dphi_ds[4,2] = -4.0*s1;
    dphi_ds[5,1] =  4.0*s2;
    dphi_ds[5,2] =  4.0*s1;
    dphi_ds[6,1] = -4.0*s2;
    dphi_ds[6,2] = -4.0*s1-8.0*s2+4.0;
  } else if (order == 3) {
    dphi_ds[1,1] = -(27.0/2.0)*(s2*s2)-9.0*s2*( 3.0*s1-2.0)+18.0*s1-(27.0/2.0)*(s1*s1)-(11.0/2.0);
    dphi_ds[1,2] = -9.0*s2*( 3.0*s1-2.0)-(27.0/2.0)*(s2*s2)-(27.0/2.0)*(s1*s1)+18.0*s1-(11.0/2.0);
    dphi_ds[2,1] =  (27.0/2.0)*(s1*s1)-9.0*s1+1.0;
    dphi_ds[2,2] =  0.0;
    dphi_ds[3,1] =  0.0;
    dphi_ds[3,2] =  (27.0/2.0)*(s2*s2)-9.0*s2+1.0;
    dphi_ds[4,1] = -45.0*s1+(81.0/2.0)*(s1*s1)+(9.0/2.0)*( 12.0*s1-5.0)*s2+(27.0/2.0)*(s2*s2)+9.0;
    dphi_ds[4,2] =  27.0*(s1*s1)-(45.0/2.0)*s1+27.0*s2*s1;
    dphi_ds[5,1] =  36.0*s1-(81.0/2.0)*(s1*s1)-(9.0/2.0)*( 6.0*s1-1.0)*s2-(9.0/2.0);
    dphi_ds[5,2] =  (9.0/2.0)*s1-(27.0/2.0)*(s1*s1);
    dphi_ds[6,1] =  (9.0/2.0)*( 6.0*s1-1.0)*s2;
    dphi_ds[6,2] =  (27.0/2.0)*(s1*s1)-(9.0/2.0)*s1;
    dphi_ds[7,1] =  (27.0/2.0)*(s2*s2)-(9.0/2.0)*s2;
    dphi_ds[7,2] =  27.0*s2*s1-(9.0/2.0)*s1;
    dphi_ds[8,1] =  (9.0/2.0)*s2-(27.0/2.0)*(s2*s2);
    dphi_ds[8,2] =  (9.0/2.0)*s1-9.0*( 3.0*s1-4.0)*s2-(81.0/2.0)*(s2*s2)-(9.0/2.0);
    dphi_ds[9,1] =  (9.0/2.0)*( 6.0*s1-5.0)*s2+27.0*(s2*s2);
    dphi_ds[9,2] =  9.0*s2*( 6.0*s1-5.0)+(81.0/2.0)*(s2*s2)-(45.0/2.0)*s1+(27.0/2.0)*(s1*s1)+9.0;
    dphi_ds[10,1] = -27.0*(s2*s2)-27.0*s2*( 2.0*s1-1.0);
    dphi_ds[10,2] = -27.0*(s1*s1)-54.0*s2*s1+27.0*s1;
  } else if (order == 4) {
    dphi_ds[1,1] =  (128.0/3.0)*(s1*s1*s1)-80.0*(s1*s1)+(4.0/3.0)*( 96.0*(s1*s1)-120.0*s1+35.0)*s2+(140.0/3.0)*s1+(128.0/3.0)*(s2*s2*s2)+16.0*( 8.0*s1-5.0)*(s2*s2)-(25.0/3.0);
    dphi_ds[1,2] =  (128.0/3.0)*(s1*s1*s1)-80.0*(s1*s1)+(140.0/3.0)*s1+(4.0/3.0)*( 96.0*(s1*s1)-120.0*s1+35.0)*s2+16.0*( 8.0*s1-5.0)*(s2*s2)+(128.0/3.0)*(s2*s2*s2)-(25.0/3.0);
    dphi_ds[2,1] =  (44.0/3.0)*s1+(128.0/3.0)*(s1*s1*s1)-48.0*(s1*s1)-1.0;
    dphi_ds[2,2] = 0.0;
    dphi_ds[3,1] = 0.0;
    dphi_ds[3,2] =  (44.0/3.0)*s2+(128.0/3.0)*(s2*s2*s2)-48.0*(s2*s2)-1.0;
    dphi_ds[4,1] =  -(128.0/3.0)*(s2*s2*s2)+(16.0/3.0)*s2*( 72.0*s1-72.0*(s1*s1)-13.0)-32.0*( 8.0*s1-3.0)*(s2*s2)-(416.0/3.0)*s1+288.0*(s1*s1)-(512.0/3.0)*(s1*s1*s1)+16.0;
    dphi_ds[4,2] =  -128.0*(s2*s2)*s1-(208.0/3.0)*s1+64.0*s2*( 3.0*s1-4.0*(s1*s1))+192.0*(s1*s1)-128.0*(s1*s1*s1);
    dphi_ds[5,1] =  -384.0*(s1*s1)+16.0*(s2*s2)*( 8.0*s1-1.0)+256.0*(s1*s1*s1)+4.0*s2*( 96.0*(s1*s1)-72.0*s1+7.0)+152.0*s1-12.0;
    dphi_ds[5,2] =  -144.0*(s1*s1)+128.0*(s1*s1*s1)+28.0*s1+32.0*( 4.0*(s1*s1)-s1)*s2;
    dphi_ds[6,1] =  -(224.0/3.0)*s1+224.0*(s1*s1)-(512.0/3.0)*(s1*s1*s1)+(16.0/3.0)*s2*( 12.0*s1-24.0*(s1*s1)-1.0)+(16.0/3.0);
    dphi_ds[6,2] =  -(128.0/3.0)*(s1*s1*s1)+32.0*(s1*s1)-(16.0/3.0)*s1;
    dphi_ds[7,1] = -(16.0/3.0)*s2*( 12.0*s1-24.0*(s1*s1)-1.0);
    dphi_ds[7,2] =  (128.0/3.0)*(s1*s1*s1)+(16.0/3.0)*s1-32.0*(s1*s1);
    dphi_ds[8,1] =  -4.0*( 8.0*s1-1.0)*s2+16.0*( 8.0*s1-1.0)*(s2*s2);
    dphi_ds[8,2] =  4.0*s1-32.0*s2*( s1-4.0*(s1*s1))-16.0*(s1*s1);
    dphi_ds[9,1] =  (16.0/3.0)*s2+(128.0/3.0)*(s2*s2*s2)-32.0*(s2*s2);
    dphi_ds[9,2] =  -64.0*s2*s1+(16.0/3.0)*s1+128.0*(s2*s2)*s1;
    dphi_ds[10,1] =  -(16.0/3.0)*s2-(128.0/3.0)*(s2*s2*s2)+32.0*(s2*s2);
    dphi_ds[10,2] =  -(512.0/3.0)*(s2*s2*s2)+(32.0/3.0)*( 6.0*s1-7.0)*s2-32.0*( 4.0*s1-7.0)*(s2*s2)-(16.0/3.0)*s1+(16.0/3.0);
    dphi_ds[11,1] =  -4.0*s2*( 8.0*s1-7.0)+16.0*(s2*s2)*( 8.0*s1-9.0)+128.0*(s2*s2*s2);
    dphi_ds[11,2] =  -16.0*(s1*s1)+28.0*s1+256.0*(s2*s2*s2)+384.0*( s1-1.0)*(s2*s2)+8.0*( 16.0*(s1*s1)-36.0*s1+19.0)*s2-12.0;
    dphi_ds[12,1] =  -64.0*(s2*s2)*( 4.0*s1-3.0)-(16.0/3.0)*( 24.0*(s1*s1)-36.0*s1+13.0)*s2-128.0*(s2*s2*s2);
    dphi_ds[12,2] =  -(512.0/3.0)*(s2*s2*s2)-96.0*(s2*s2)*( 4.0*s1-3.0)+(32.0/3.0)*s2*( 36.0*s1-24.0*(s1*s1)-13.0)-(208.0/3.0)*s1+96.0*(s1*s1)-(128.0/3.0)*(s1*s1*s1)+16.0;
    dphi_ds[13,1] =  32.0*(s2*s2)*( 16.0*s1-7.0)+128.0*(s2*s2*s2)+32.0*s2*( 12.0*(s1*s1)-14.0*s1+3.0);
    dphi_ds[13,2] =  -64.0*s2*( 7.0*s1-8.0*(s1*s1))+384.0*(s2*s2)*s1+96.0*s1+128.0*(s1*s1*s1)-224.0*(s1*s1);
    dphi_ds[14,1] =  32.0*s2*( 10.0*s1-12.0*(s1*s1)-1.0)-32.0*(s2*s2)*( 8.0*s1-1.0);
    dphi_ds[14,2] =  -32.0*s1+64.0*( s1-4.0*(s1*s1))*s2-128.0*(s1*s1*s1)+160.0*(s1*s1);
    dphi_ds[15,1] =  -32.0*(s2*s2)*( 8.0*s1-5.0)+32.0*( 2.0*s1-1.0)*s2-128.0*(s2*s2*s2);
    dphi_ds[15,2] =  64.0*( 5.0*s1-4.0*(s1*s1))*s2-32.0*s1-384.0*s1*(s2*s2)+32.0*(s1*s1);
  }
}
### function init_iseq2imsh()
### {
###   iseq2imsh[1] = 1;
###   iseq2imsh[order+1] = 2;
###   for (i = 2; i <= order; i++) {
###     iseq2imsh[i] = i+1;
###   }
###   for (i = 1; i <= order+1; i++) {
###     imsh2iseq[iseq2imsh[i]] = i;
###   }
### }
### function check_basis()
### {
###   status = 0;
###   for (jseq = 1; jseq <= order+1; jseq++) {
###     sj = (1.0*(jseq-1))/(1.0*order);
###     #j = jseq;
###     j = iseq2imsh[jseq];
###     compute_phi(sj);
###     for (i = 1; i <= order+1; i++) {
###       phi_i_at_sj = phi[i];
###       delta_ij = ((i == j) ? 1 : 0);
###       if (fabs(phi_i_at_sj - delta_ij) > machine_epsilon) {
###         print "FATAL: invalid basis: phi(",i,j,")=phi",i,"(",sj,")=",phi[i] > "/dev/stderr"; 
### 	status = 1;
###       }
###     }
###   }
###   if (status != 0) {
### 	exit(1);
###   }
### }
### # basis & derivatives: evaluation on quadrature point set
### function compute_phi_at_quad()
### {
###   for (q = 1; q <= nquad; q++) {
###    compute_phi(s[q]);
###    for (i = 1; i <= nbasis; i++) {
###      phi_at_quad[i,q] = phi[i]; 
###    }
###   }
### }
function compute_dphi_ds_at_quad()
{
  for (q = 1; q <= nquad; q++) {
   compute_dphi_ds(s1[q],s2[q]);
   for (i = 1; i <= nbasis; i++) {
     dphi_ds_at_quad[i,q,1] = dphi_ds[i,1]; 
     dphi_ds_at_quad[i,q,2] = dphi_ds[i,2]; 
   }
  }
}
### # piola: jacobian at q-th quadrature point in elementb elt: 
### function piola_x(q, elt)
### {
###    xq = 0;
###    for (i = 1; i <= nbasis; i++) {
###      xq += x[elt[i]]*phi_at_quad[i,q]; 
###    }
###    return xq;
### }
### function piola_y(q, elt)
### {
###    yq = 0;
###    for (i = 1; i <= nbasis; i++) {
###      yq += y[elt[i]]*phi_at_quad[i,q]; 
###    }
###    return yq;
### }
function piola_jacobian(q, elt)
{
   # compute tangent vectors dF_ds1 & dF_ds2 at xq:
   for (r = 1; r <= 2; r++) {
     dx_ds[r] = 0;
     dy_ds[r] = 0;
     dz_ds[r] = 0;
   }
   for (i = 1; i <= nbasis; i++) {
     for (r = 1; r <= 2; r++) {
       dx_ds[r] += x[elt[i]]*dphi_ds_at_quad[i,q,r]; 
       dy_ds[r] += y[elt[i]]*dphi_ds_at_quad[i,q,r]; 
       dz_ds[r] += z[elt[i]]*dphi_ds_at_quad[i,q,r]; 
     }
   }
   # S(xq)=vect(dF_ds1(xq),dF_ds2(xq)):
   Sx =   dy_ds[1]*dz_ds[2] - dz_ds[1]*dy_ds[2]; 
   Sy =   dz_ds[1]*dx_ds[2] - dx_ds[1]*dz_ds[2]; 
   Sz =   dx_ds[1]*dy_ds[2] - dy_ds[1]*dx_ds[2];
   # J(xq) = |S(xq)|
   Jq = sqrt(Sx*Sx + Sy*Sy + Sz*Sz);
   return Jq;
}
### # max interpolation error at quadrature point set
### function compute_max_interpolation_error_at_quad(elt)
### {
###   err = 0;
###   for (q = 1; q <= nquad; q++) {
###     xq = piola_x(q, elt);
###     yq = piola_y(q, elt);
###     r = xq*xq + yq*yq;
###     err += max(err, fabs(1-r));
###   }
###   return err;
### }
function compute_measure(elt,nx,ny,nz)
{
  value = 0;
  for (q = 1; q <= nquad; q++) {
    # xq = piola_x(q, elt);
    # yq = piola_y(q, elt);
    # zq = piolazy(q, elt);
    # value += (xq**nx)*(yq**ny)*(zq**nz)*w[q]*piola_jacobian(q, elt);
    value += w[q]*piola_jacobian(q, elt);
  }
  return value;
}
### function check_equispaced(elt)
### {
###    xprec = x[elt[iseq2imsh[1]]];
###    yprec = y[elt[iseq2imsh[1]]];
###    variation = 0;
###    for (i = 2; i <= order+1; i++) {
###      xi = x[elt[iseq2imsh[i]]];
###      yi = y[elt[iseq2imsh[i]]];
###      d = sqrt((xi-xprec)**2 + (yi-yprec)**2);
###      xprec = xi;
###      yprec = yi;
###      if (i == 2) {
###        d0 = d;
###      } else {
###        variation = max(variation, fabs(d-d0));
###      }
###    }
###    return variation;
### }
### #  I_{m,n}
### #  = int_Omega x^m y^n dx
### #  = (int_{r=0}^1 r^{m+n+1} dr)*(int_0^{2*pi} cos(theta)^m sin(theta)^n d theta)
### #  et :
### #   int_{r=0}^1 r^{m+n+1} dr = 1/(m+n+2)
### #  => I_{m,n}   = J_{m,n}/(m+n+2)
### #  J_{m,n} = int_0^{2*pi} cos(theta)^m sin(theta)^n d theta
### #	cf Bro-1985, p. 557 & 561 : recurrence
### #  J_{m,n} = 0 si m ou n impair
### #  J_{2p,2q} = (2q-1)/(2p+2q)*J_{2p,2q-2}
### #  J_{2p,0} = (2p-1)/(2p)*J_{2p-2,0}
### #  J_{0,0} = 2*pi
### #                  (2p)!
### #  => J_{2p,0} = ------------ * (2*pi)        pour q = 0
### #	        (2^p * p!)^2
### #                 (2p)! (2q-1)! pi
### #  => J_{2p,2q} = --------------------------  pour q >=1
### #                 4^(p+q-1) p! (q-1)! (p+q)!
### function factorial(n)
### {
###   res = 1.0;
###   for (i = 1; i <= n; i++) res = res*i;
###   return res;
### }
function integrate_xyz(m,n,p)
{
  pi = 3.14159265358979323846;
  value = 4*pi;
  return value;
}
# ---------------------------------------------------
# initialization: input parameters:
#     order (required)
#     nx    = 0 by default
#     ny    = 0 by default
# ---------------------------------------------------
BEGIN {
        if (order <= 0) { print "FATAL: usage: gawk -v order=value thisfile.awk" > "/dev/stderr"; exit(1); }
        machine_epsilon = 1e-11;
        node_tol        = 1e-5;
        pi = 3.14159265358979323846;
#        init_iseq2imsh();
#        check_basis();
   	set_quadrature();
#        compute_phi_at_quad();
        compute_dphi_ds_at_quad();
	state = 0;
        measure = 0;
#        error_linf = 0;
        node_error = 0;
#        equispaced_error = 0;
        do_fix_boundary = 1;
  }
# ---------------------------------------------------
# line loop
# ---------------------------------------------------
($1 == "$Nodes" && state == 0) {
	state = 1;
	next;
	}
($1 != "" && state == 1) {
	nnod = $1;
	inod = 0;
	state = 2;
	next;
	}
($1 != "" && state == 2) {
	x[$1] = $2;
	y[$1] = $3;
	z[$1] = $4;
        r2 = x[$1]*x[$1] + y[$1]*y[$1] + z[$1]*z[$1];
        if (do_fix_boundary) {
	  # re-project exactly onto the sphere r=1, because of gmsh rounding errors
          r = sqrt(r2);
          x[$1] /= r;
          y[$1] /= r;
          z[$1] /= r;
        }
        r2 = x[$1]*x[$1] + y[$1]*y[$1] + z[$1]*z[$1];
        node_error = max(node_error, sqrt(fabs(1-r2)));
	inod++;
	if (inod == nnod) state = 3;
	next; 
	}
($1 == "$Elements" && state == 3) {
	nelt = $1;
	ielt = 0;
	state = 4;
	next;
	}
($1 != "" && state == 4) {
	nelt = $1;
	ielt = 0;
	state = 5;
	next;
    }
($1 != "" && state == 5) {
        type = $2;
             if (type ==  2) { elt_order = 1; }
        else if (type ==  9) { elt_order = 2; }
        else if (type == 21) { elt_order = 3; }
        else if (type == 23) { elt_order = 4; }
        else                 { print "FATAL: invalid gmsh type ",type > "/dev/stderr"; exit(1); }
        if (elt_order != order) { print "invalid element order ",elt_order,": expect ", order > "/dev/stderr"; exit(1); }
        start = $3 + 4;
        last  = NF + 1;
        elt_nnod = last - start;
        elt_size = (order+1)*(order+2)/2;
        if (elt_nnod != elt_size) {
          print "invalid element size ",elt_nnod,": expect ", elt_size > "/dev/stderr"; exit(1);
        }
	for (i = start; i < last; i++) {
	  elt[i-start+1] = $i;
	}
        elt_measure = compute_measure(elt,nx,ny,nz);
        measure += elt_measure;
#       elt_error = compute_max_interpolation_error_at_quad(elt);
#       error_linf = max(error_linf, elt_error);
	ielt++;
#       elt_variation = check_equispaced(elt);
#       equispaced_error = max(equispaced_error, elt_variation);
	if (ielt == nelt) state = 6;
	next;
    }
# ---------------------------------------------------
# epilogue
# ---------------------------------------------------
END {
        exact_measure = integrate_xyz(nx,ny,nz);
        error_measure = fabs(measure -  exact_measure);
        printf("measure        = %.16g\n", measure);
        printf("exact_measure  = %.16g\n", exact_measure);
        printf("error_measure  = %.16g\n", error_measure);
        printf("node_error     = %.16g\n", node_error);
#       printf("error_linf       = %.16g\n", error_linf);
        if (node_error > node_tol) {
          printf("FATAL: node_error       = %.16g\n", node_error) > "/dev/stderr";
          exit (1);
        }
#       if (equispaced_error > machine_epsilon) {
#         printf("FATAL: equispaced_error  = %.16g\n", equispaced_error) > "/dev/stderr";
#         exit (1);
#       }
    }
