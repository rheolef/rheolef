#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

# -----------
# 1D: e
# -----------
run "../sbin/mkgeo_grid_1d 31 -v4 -a -2 -b 2  > tmp.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun " ./quadrature_equispaced_tst tmp.geo 50 equispaced 0.0007 >/dev/null 2>/dev/null"
if test $? -ne 0; then status=1; fi

# -----------
# 2D: t, q
# -----------
run "../sbin/mkgeo_grid_2d -t 3 -a -2 -b 2 -c -2 -d 2 -v4 2>/dev/null | ../bin/geo -upgrade - > tmp.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun " ./quadrature_equispaced_tst tmp.geo 100 equispaced 0.004 >/dev/null 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "../sbin/mkgeo_grid_2d -q 3 -a -2 -b 2 -c -2 -d 2 -v4 2>/dev/null | ../bin/geo -upgrade - > tmp.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun " ./quadrature_equispaced_tst tmp.geo 100 equispaced 0.004 >/dev/null 2>/dev/null"
if test $? -ne 0; then status=1; fi
# -----------
# 3D: T, P, H
# -----------
run "../sbin/mkgeo_grid_3d -T 3 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2 -v4 2>/dev/null | ../bin/geo -upgrade - > tmp.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun " ./quadrature_equispaced_tst tmp.geo 50 equispaced 0.002 >/dev/null 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "../sbin/mkgeo_grid_3d -P 3 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2 -v4 2>/dev/null | ../bin/geo -upgrade - > tmp.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun " ./quadrature_equispaced_tst tmp.geo 50 equispaced 0.005 >/dev/null 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "../sbin/mkgeo_grid_3d -H 3 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2 -v4 2>/dev/null | ../bin/geo -upgrade - > tmp.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun " ./quadrature_equispaced_tst tmp.geo 50 equispaced 0.008 >/dev/null 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "rm -f tmp.geo"

exit $status
