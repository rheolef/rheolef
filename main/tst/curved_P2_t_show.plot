#!gnuplot
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
set title "curved_P2_t: 1 elements, 3 vertices"
set xrange [-0.1:1.1]
set yrange [-0.1:1.1]
set size ratio -1  # equal scales
set key left Right at graph 1,1
plot \
  "curved_P2_t-fac.gdat" title "faces" with l lc 3 lw 1,\
  "curved_P2_t-edg.gdat" title "edges" with l lc 1 lw 1.5, \
  x*(1-x) w l lc 2, \
  (1-x)+x*(1-x) w l lc 4
pause -1 "<return>"
