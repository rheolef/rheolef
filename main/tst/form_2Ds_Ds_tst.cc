///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// 
// check the "2Ds_Ds" form on some simple geometries
// 
// authors: Mahamar Dicko, Jocelyn Etienne, Pierre Saramito
//
// usage:
//	prog mesh.geo Pk qorder tol u0 u1 u2 v0 v1 v2 n0 n1 n2
//
// examples:
//
//	mkgeo_ball -s -t 10 > ball-P1.geo
// 	./form_2Ds_Ds_tst ball-P1.geo P1 18 0.050 z x^2 'x*y' 'x*y' x^2 z x y z
//
//	mkgeo_ball -s -t 10 -order 2 > ball-P2.geo
// 	./form_2Ds_Ds_tst ball-P2.geo P2 18 0.002 z x^2 'x*y' 'x*y' x^2 z x y z
//
//	mkgeo_grid -T 10 > cube-10.geo
//	./geo_domain_tst cube-10 right > cube-10.right.geo
// 	./form_2Ds_Ds_tst cube-10.right.geo P1 18 1e-14 z x^2 'x*y' 'x*y' x^2 z 0 1 0
// 	./form_2Ds_Ds_tst cube-10.right.geo P2 18 1e-14 z x^2 'x*y' 'x*y' x^2 z 0 1 0
//
// note: the banded level set is also supported: see the form_2Ds_Ds_tst.sh for invocations.
//
#include "form_2Ds_Ds.icc"

// --------------------------------------------------------------
// compare two evaluations of 2Ds(u):Ds(v) :
//  symbolic : integrate_omega f(x) dx
//  		with f = 2Ds(u):Ds(v) and using a quadrature
//  numeric :  form 2Ds_Ds (uh,vh)
// arguments are sring expressions for u,v and n
// --------------------------------------------------------------
Float
compute_error_s (
  const geo&                      omega,
  const string&                   approx,
  const point_basic<std::string>& u_expr,
  const point_basic<std::string>& v_expr,
  const point_basic<std::string>& n_expr,
  const point_basic<symbol>&      x,
  const quadrature_option&   qopt)
{
  // symbolic computation:
  scalar_function f = form_2Ds_Ds (u_expr,v_expr,n_expr,x);
  Float int_f = integrate (omega, f, qopt);

  // numeric computation:
  space Xh (omega, approx, "vector");
  field uh = lazy_interpolate (Xh, vector_function (u_expr,x));
  field vh = lazy_interpolate (Xh, vector_function (v_expr,x));
  trial u (Xh); test v (Xh);
  form a = integrate (2*ddot(Ds(u),Ds(v)), qopt);
  Float int_fh = a (uh, vh);
  derr << setprecision(16)
       << "int_h 2Ds(u):Ds(v)   = " << int_f  << endl
       << "int_h 2Ds(uh):Ds(vh) = " << int_fh << endl;
    
  return fabs(int_f - int_fh);
}
// the same with the banded level set method
// assume the unit sphere:
Float phi (const point& x) { return norm(x) - 1; }

Float
compute_error_band (
  const geo&                      lambda,
  const string&                   approx,
  const point_basic<std::string>& u_expr,
  const point_basic<std::string>& v_expr,
  const point_basic<std::string>& n_expr,
  const point_basic<symbol>&      x,
  const quadrature_option&   qopt)
{
  space Xh  (lambda, approx);
  field phi_h = lazy_interpolate(Xh, phi);
  band gh (phi_h); // TODO: option with quadrangles

  // symbolic computation:
  Float int_f = integrate (gh.level_set(), form_2Ds_Ds (u_expr,v_expr,n_expr,x), qopt);

  // numeric computation:
  space Bh  (gh.band(), approx, "vector");
  trial u (Bh); test v (Bh);
  form a = integrate (gh, 2*ddot(Ds(u),Ds(v)), qopt);
  field uh = lazy_interpolate (Bh, vector_function (u_expr,x));
  field vh = lazy_interpolate (Bh, vector_function (v_expr,x));
  Float int_fh = a (uh, vh);

  derr << setprecision(16)
       << "int_h 2Ds(u):Ds(v)   = " << int_f  << endl
       << "int_h 2Ds(uh):Ds(vh) = " << int_fh << endl;
    
  return fabs(int_f - int_fh);
}
// --------------------------------------------------------------
// usage: prog mesh.geo Pk qorder tol u0 u1 u2 v0 v1 v2 n0 n1 n2
// --------------------------------------------------------------
int main(int argc, char**argv) {
    environment rheolef (argc,argv);
    geo omega (argv[1]);
    string approx = argv[2];
    quadrature_option qopt;
    qopt.set_order  (atoi(argv[3]));
    qopt.set_family (quadrature_option::gauss);
    Float tol = atof (argv[4]);

    size_t d = omega.dimension();
    point_basic<symbol> x (symbol("x"),symbol("y"),symbol("z"));
    point_basic<std::string> u_expr ("0", "0", "0");
    point_basic<std::string> v_expr ("0", "0", "0");
    point_basic<std::string> n_expr ("0", "0", "0");
    size_t next = 5;
    for (size_t i = 0; i < d; ++i) {
      u_expr [i] = (next+i < size_t(argc)) ? argv[next+i] : "0";
    }
    next += d;
    for (size_t i = 0; i < d; ++i) {
      v_expr [i] = (next+i < size_t(argc)) ? argv[next+i] : "0";
    }
    next += d;
    for (size_t i = 0; i < d; ++i) {
      n_expr [i] = (next+i < size_t(argc)) ? argv[next+i] : "0";
    }
    derr << "approx = "<<approx<<endl
         << "qorder = "<<qopt.get_order()<<endl
         << "u = "<<u_expr<<endl
         << "v = "<<v_expr<<endl
         << "n = "<<n_expr<<endl;
    Float err = (omega.map_dimension() < omega.dimension()) ? 
        compute_error_s    (omega, approx, u_expr, v_expr, n_expr, x, qopt) :
        compute_error_band (omega, approx, u_expr, v_expr, n_expr, x, qopt);

    dout << setprecision(16)
         << "# nelt err" << endl
         << omega.dis_size() << " " << err << endl;
    return (err < tol) ? 0 : 1;
}
