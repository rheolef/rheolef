///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check:
//  \int_\Gamma weight(x) dx = mes(\Gamma) with weight
//
// where Gamma is a boundary of the [0,1]^d cube
// 
// example:
//   form_mass_bdr_tst -app P1 -weight 0 -I../data carre-v2 left
//
#include "rheolef/rheolef.h"
using namespace rheolef;
using namespace std;

typedef Float (*function_type)(const point&);

Float weight_1  (const point& x) { return 1; }
Float weight_x  (const point& x) { return x[0]; }
Float weight_y  (const point& x) { return x[1]; }
Float weight_z  (const point& x) { return x[2]; }

Float weight_x2 (const point& x) { return x[0]*x[0]; }
Float weight_xy (const point& x) { return x[0]*x[1]; }
Float weight_y2 (const point& x) { return x[1]*x[1]; }
Float weight_xz (const point& x) { return x[0]*x[2]; }
Float weight_yz (const point& x) { return x[1]*x[2]; }
Float weight_z2 (const point& x) { return x[2]*x[2]; }

const char * domain_name [6] = {
	"left",		// y=0, xz varies
	"back",		// x=0, yz varies (3d)
	"bottom",	// z=0, xy varies

	"right",	// y=1, xz varies
	"front",	// x=1, yz varies (3d)
	"top",		// z=1, xy varies
};
size_t
domain_index (const string& name)
{
    for  (size_t i = 0; i < 6; i++)
	if (domain_name[i] == name) return i;
    error_macro ("unexpected domain name `" << name << "'");
    return 6;
}
struct list_type {
        string name;
        function_type function;
        Float  expect [6];
};
typedef Float T;

//
// 2d cartesian case
//
list_type weight_list_2d[]={

		// left      back    bottom  right      front       top
		// x=0        -      y=0     x=1         -          y=1
                // dy         -      dx      dy	         -          dx 

 {"1", weight_1,  {1,	     -1,     1,	     1,   	-1,         1     } },
 {"x", weight_x,  {0,	     -1,     0.5,    1,         -1,         0.5   } },
 {"y", weight_y,  {0.5,      -1,     0,      0.5,       -1,         1     } },

 {"x2",weight_x2, {0,        -1,     T(1)/3, 1,         -1,         T(1)/3} },
 {"y2",weight_y2, {T(1)/3,   -1,     0,      T(1)/3,    -1,         1     } },
 {"xy",weight_xy, {0,        -1,     0,      0.5,       -1,         0.5   } }
};
const size_t max_weight_2d = sizeof(weight_list_2d)/sizeof(list_type);
//
// 3d cartesian case
//
list_type weight_list_3d[]={

		// left      back    bottom     right     front     top
		// y=0       x=0     z=0        y=1       x=1       z=1
                // dx.dz     dy.dz   dx.dy	dx.dz     dy.dz     dx.dy

 {"1", weight_1,  {1,	     1,	     1,   	1, 	  1, 	    1     } },
 {"x", weight_x,  {0.5,	     0,      0.5,	0.5,      1,        0.5   } },
 {"y", weight_y,  {0,        0.5,    0.5,       1,        0.5,      0.5   } },
 {"z", weight_z,  {0.5,      0.5,    0,         0.5,      0.5,      1     } },

 {"x2",weight_x2, {T(1)/3,   0,      T(1)/3,    T(1)/3,   1,        T(1)/3} },
 {"y2",weight_y2, {0,        T(1)/3, T(1)/3,    1,        T(1)/3,   T(1)/3} },
 {"z2",weight_z2, {T(1)/3,   T(1)/3, 0,         T(1)/3,   T(1)/3,   1     } },
 {"xy",weight_xy, {0,        0,      0.25,      0.5,      0.5,      0.25  } },
 {"xz",weight_xz, {0.25,     0,      0,         0.25,     0.5,      0.5   } },
 {"yz",weight_yz, {0,        0.25,   0,         0.5,      0.25,     0.5   } }
};
const size_t max_weight_3d = sizeof(weight_list_3d)/sizeof(list_type);
//
// axisymmetric case:
//
list_type weight_list_rz []={

		// left     back     bottom     right     front     top
		// r=0      -        z=0        r=1       -         z=1
                // 0        -        r.dr      	dz        -         r.dr
                // (x,y)=(r,z)

 {"1", weight_1,  {0,       -1,      0.5,       1, 	  -1,       0.5     } },
 {"x", weight_x,  {0,       -1,      T(1)/3,    1,        -1,       T(1)/3  } },
 {"x2",weight_x2, {0,       -1,      0.25,      1,        -1,       0.25    } },
 {"y", weight_y,  {0,       -1,      0,         0.5,      -1,       0.5     } },
 {"xy",weight_xy, {0,       -1,      0,         0.5,      -1,       T(1)/3  } },
 {"y2",weight_y2, {0,       -1,      0,         T(1)/3,   -1,       0.5     } }
};
#ifdef TO_CLEAN
list_type old_weight_list_rz []={
                // bottom   left     back       top       right     front
                // z=0      x=0      -          z=1       r=1       -
                // r.dr     0        -          r.dr      dz        -

 {"1", weight_1,  {0.5,      0,      -1,        0.5,      1,        -1     } },
 {"x", weight_x,  {T(1)/3,   0,      -1,        T(1)/3,   1,        -1     } },
 {"x2",weight_x2, {0.25,     0,      -1,        0.25,     1,        -1     } },
 {"y", weight_y,  {0,        0,      -1,        0.5,      0.5,      -1     } },
 {"xy",weight_xy, {0,        0,      -1,        T(1)/3,   0.5,      -1     } },
 {"y2",weight_y2, {0,        0,      -1,        0.5,      T(1)/3,   -1     } }
};
#endif // TO_CLEAN

const size_t max_weight_rz = sizeof(weight_list_rz)/sizeof(list_type);

size_t
weight_index (size_t dim, const string& name, const string& sys_coord_name)
{
  if (sys_coord_name == "cartesian" && dim == 3) {

    for  (size_t i = 0; i < max_weight_3d; i++)
	if (weight_list_3d[i].name == name) return i;
    error_macro ("unexpected 3d weight `" << name << "'");
    return max_weight_3d;

  } else if (sys_coord_name == "cartesian" && dim == 2) {

    for  (size_t i = 0; i < max_weight_2d; i++)
	if (weight_list_2d[i].name == name) return i;
    error_macro ("unexpected 2d weight `" << name << "'");
    return max_weight_2d;

  } else { // axisymmetric rz

    for  (size_t i = 0; i < max_weight_rz; i++)
	if (weight_list_rz[i].name == name) return i;
    error_macro ("unexpected weight `" << name << "' in axisymmetric system");
    return max_weight_rz;
  }
}
function_type
get_function (size_t dim, string weight, string sys_coord_name)
{
  size_t weight_idx = weight_index (dim, weight, sys_coord_name);
  if (sys_coord_name == "cartesian" && dim == 3) {
    return weight_list_3d [weight_idx].function;
  } else if (sys_coord_name == "cartesian" && dim == 2) {
    return weight_list_2d [weight_idx].function;
  } else {
    return weight_list_rz [weight_idx].function;
  }
}
Float
get_expected_value (size_t dim, string weight, string domain_name, string sys_coord_name)
{
  size_t weight_idx = weight_index (dim, weight, sys_coord_name);
  size_t domain_idx = domain_index (domain_name);
  if (sys_coord_name == "cartesian" && dim == 3) {
    return weight_list_3d [weight_idx].expect [domain_idx];
  } else if (sys_coord_name == "cartesian" && dim == 2) {
    return weight_list_2d [weight_idx].expect [domain_idx];
  } else {
    return weight_list_rz [weight_idx].expect [domain_idx];
  }
}
bool
is_on_axis (const space& W)
{
    const point& xmin = W.get_geo().xmin();
    const point& xmax = W.get_geo().xmax();
    return (xmin[0] == Float(0) && xmax[0] == Float(0));
}
void usage()
{
      derr << "form_mass_bdr_tst: usage: form_mass_bdr_tst"
	    << " -app string"
	    << " {-Igeodir}*"
	    << " -|mesh[.geo]"
	    << " {domain}+"
	    << " [-weight string]"
	    << " [-rz]"
	    << endl
            << "example:" << endl
            << "  form_mass_bdr_tst -app P1 -I../data carre top -weight x" << endl;
      exit (1);
}
int main(int argc, char**argv)
{
    environment rheolef (argc, argv);
    typedef geo::size_type size_type;
    Float epsilon = 1e-15;
    //
    // load geometry and options
    //
    geo omega;
    string approx = "P1";
    string weight = "1";
    bool mesh_done = false;
    geo::size_type n_dom = 0;
    string dom_name;
    string sys_coord_name = "cartesian";
    int status = 0;

    if (argc <= 1) usage() ;
    derr << noverbose;

    for (int i=1 ; i < argc ; i++) {

      if (argv [i][0] == '-' && argv [i][1] == 'I')
	append_dir_to_rheo_path (argv[i]+2) ;
      else if (strcmp(argv[i], "-app") == 0)    approx = argv[++i];
      else if (strcmp(argv[i], "-weight") == 0) weight = argv[++i];
      else if (strcmp(argv[i], "-rz") == 0)     sys_coord_name = "rz";
      else if (strcmp(argv[i], "-") == 0) {

	  // input geo on standard input
	  if (mesh_done) usage() ;
	  derr << " ! mass: read geo on stdin" << endl ;
	  din >> omega ;
	  mesh_done = true ;

      } else if (!mesh_done) {

	  // input geo on file
	  omega = geo(argv[i]);
	  mesh_done = true ;
 
      } else {

          // a domain
	  dom_name = argv[i];
      }
    }
    if (!mesh_done) {
      warning_macro("no mesh specified.");
      usage();
    }
    if (weight == "") {
      warning_macro("no weight specified");
      usage();
    }
    if (dom_name == "") {
      warning_macro("no boundary domain specified");
      usage();
    }
    size_t dim = omega.dimension();
    omega.set_coordinate_system (sys_coord_name);
    //
    // build boundary domain
    //
    domain gamma(omega[dom_name]) ;
    //
    // build space
    //
    space V(omega, approx);
    space W(gamma, approx);
    //
    // build field on V
    //
    field one(V,1);
    string sys_coord_name2 = space_constant::coordinate_system_name (omega.coordinate_system());
    function_type weight_fct = get_function (dim,weight, sys_coord_name2);
    field weight_h = lazy_interpolate(V,weight_fct);
    //
    // build forms
    //
    form  m_h (V, V, "mass", gamma) ;
    // ---------------------------------------------------
    // first check : compute boundary area with i-th weight
    // ---------------------------------------------------
    Float mes_gamma_1 = dual(one, m_h*weight_h);
    cout << "mes1(gamma) = " << mes_gamma_1 << endl;
    // ---------------------------------------------------
    // 2nd check : use trace
    // ---------------------------------------------------
    bool axi_and_on_axis = (sys_coord_name == "rz" && is_on_axis(W));
    if (!axi_and_on_axis) {
        //
        // build field on W
        // 
        field weight_bdr_bug = lazy_interpolate(W,weight_fct);
        //
        // build form on W
        //
        form  m_gamma_h (W, W, "mass") ;
        form  m_trace_h (V, W, "mass") ;
        //
        // deduce field on W by solving
        //   m_gamma_h*weight_bdr = m_trace_h*weight_h
        //
        field one_bdr (W, 1.);
        field weight_bdr(W);
        solver sa (m_gamma_h.uu());
        field lh = m_trace_h*weight_h;
        weight_bdr.set_b() = 0;
        weight_bdr.set_u() = sa.solve(lh.u() - m_gamma_h.ub()*weight_bdr.b());
        //
        // compute boundary area with i-th weight
        //
        Float mes_gamma_2 = dual(one_bdr, m_gamma_h*weight_bdr);
        cout << "mes2(gamma) = " << mes_gamma_2 << endl;
        // ---------------------------------------------------
        // 3nd check : compare two results
        // ---------------------------------------------------
        if (fabs(mes_gamma_1-mes_gamma_2) > sqrt(epsilon)) {
          derr << "mes1(gamma) != mes2(gamma): boundary mass problem detected." << endl;
          status = 1;
        }
    }
    // ---------------------------------------------------
    // 4th check : compare with exact data
    // ---------------------------------------------------
    Float expect = get_expected_value (dim, weight, dom_name, sys_coord_name);
    cout << "exact       = " << expect << endl;
    if (fabs(mes_gamma_1-expect) <= sqrt(epsilon)) {
        derr << "ok" << endl;
    } else {
        derr << "error = " << mes_gamma_1-expect << endl;
        status = 1;
    }

#ifdef TO_CLEAN
    cout << ml
         << "weight   = " << weight_h.u << ";" << endl
         << "one_bdr  = " << one_bdr.u << ";" << endl
         << "m_bdr    = " << m_gamma_h.uu << ";" << endl
         << "m_trace  = " << m_trace_h.uu << ";" << endl
         << "weight_bdr  = " << weight_bdr.u << ";" << endl
        ;
#endif // TO_CLEAN

    return status;
}
