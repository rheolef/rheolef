///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check the computation of the normal derivative
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;

// test 1: when Omega=]0,1[^d on domain Gamma={x0=1}
size_t k = 1;
Float u_pow(const point& x) { return pow(x[0],k); }
Float du_dn_pow (const point& x) { return k*pow(x[0],k-1); }

// test 2: when Omega is the unit ball(0,1) in R^d
Float u_ball(const point& x) { return norm(x); }
Float du_dn_ball (const point& x) { return norm(x); }

// test 3: when Omega=]0,1[^d on domain Gamma={x0=1}
struct u_sinusprod {
  Float operator() (const point& x) const {
    switch (d) {
     case 0:  return 0;
     case 1:  return sin(pi*x[0]);
     case 2:  return sin(pi*x[0])*sin(pi*x[1]);
     default: return sin(pi*x[0])*sin(pi*x[1])*sin(pi*x[2]);
    }
  }
  u_sinusprod(size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; Float pi;
};
struct du_dn_sinusprod {
  Float operator() (const point& x) const {
    switch (d) {
     case 0:  return 0;
     case 1:  return pi*cos(pi*x[0]);
     case 2:  return pi*cos(pi*x[0])*sin(pi*x[1]);
     default: return pi*cos(pi*x[0])*sin(pi*x[1])*sin(pi*x[2]);
    }
  }
  du_dn_sinusprod(size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; Float pi;
};
int main(int argc, char**argv) {
    environment rheolef (argc, argv);
    geo omega (argv[1]);  
    string approx   = (argc > 2) ? argv[2] : "P1";
    string dom      = (argc > 3) ? argv[3] : "right";
    Float  tol      = (argc > 4) ? atof(argv[4]) : 1e-10;
    string tst_name = (argc > 5) ? argv[5] : "-pow";
    space Xh (omega, approx); 
    size_t d = omega.dimension();
    k = Xh.degree();
    string grad_approx = "P" + std::to_string(k-1) + "d";
    space Mh (omega[dom], grad_approx);
warning_macro ("main[1]");
    trial u (Xh); test v (Xh);
    trial p (Mh); test q (Mh);
warning_macro ("main[2]");
    form  d_dn = integrate (omega[dom], dot(grad(u),normal())*q);
derr << "d_dn="<<d_dn.uu();
warning_macro ("main[3]");
    integrate_option fopt;
    fopt.invert = true;
    form  inv_m = integrate (p*q, fopt);
    field pi_h_u, pi_h_du_dn;
warning_macro ("main[4]");
    if (tst_name == "-pow") {
	warning_macro ("test: pow");
        pi_h_u     = lazy_interpolate (Xh, u_pow);
        pi_h_du_dn = lazy_interpolate (Mh, du_dn_pow);
    } else if (tst_name == "-ball") {
	warning_macro ("test: ball");
        pi_h_u     = lazy_interpolate (Xh, u_ball);
        pi_h_du_dn = lazy_interpolate (Mh, du_dn_ball);
    } else {
	warning_macro ("test: sinusprod");
        pi_h_u     = lazy_interpolate (Xh, u_sinusprod(d));
        pi_h_du_dn = lazy_interpolate (Mh, du_dn_sinusprod(d));
    }
warning_macro ("main[5]");
    field duh_dn = inv_m*(d_dn*pi_h_u);
    field eh = duh_dn - pi_h_du_dn;
warning_macro ("main[6]");
    Float err = field(eh).max_abs();
    derr << "err = " << err << endl;
    if (err >= tol) {
      dout << catchmark("duh_dn") << duh_dn
           << catchmark("du_dn")  << pi_h_du_dn
           << catchmark("eh")     << eh;
    }
    return (err < tol) ? 0 : 1;
}
