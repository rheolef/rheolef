#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skiped)"
#exit 0

status=0

L_1D_2D="
triangle_p1-v2
triangle_rotate_p1-v2
triangle2_p1-v2
carre-v2
quadrangle_p1-v2
carre-q-10-dom-v2.geo
square_tq_bamg.geo
"

L_3D="
tetra_p1-v2.geo
tetra2_p1-v2.geo
cube-5-bdry-v2.geo
cube-gmsh-v2.geo
hexa_p1-bdry-v2.geo
cube-H-6-dom-v2.geo
"

for geo in $L_1D2D; do
  for k in 0 1 2 3 4 5; do
    loop_mpirun "./raviart_thomas_tst $SRCDIR/$geo RT${k}d 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# for 3D geo, limit tests to lower degrees
for geo in $L_3D; do
  for k in 0 1 2; do
    loop_mpirun "./raviart_thomas_tst $SRCDIR/$geo RT${k}d 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done

exit $status
