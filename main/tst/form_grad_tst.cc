///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check the computation of the gradient
//
// Note: it was a buggy 0.5 factor in the second component of the
//  grad, as for the grad of a vector 
//
#include "rheolef/rheolef.h"
using namespace rheolef;
using namespace std;

typedef Float (*function_type)(const point&);

struct list_type {
	string        name;
	function_type function;
};
Float monom_1  (const point& x) { return 1; }
Float monom_x  (const point& x) { return x[0]; }
Float monom_y  (const point& x) { return x[1]; }
Float monom_x2 (const point& x) { return x[0]*x[0]; }
Float monom_xy (const point& x) { return x[0]*x[1]; }
Float monom_y2 (const point& x) { return x[1]*x[1]; }

list_type xy_monom_list [] = {
	//   monom
	// id   fct    
	{"1",	monom_1},
	{"x",	monom_x},
	{"y",	monom_y},
	{"x2",	monom_x2},
	{"xy",	monom_xy},
	{"y2",	monom_y2}
};
const unsigned int xy_monom_max = sizeof(xy_monom_list)/sizeof(list_type);

void usage()
{
      derr << "form_grad_tst: usage: form_grad_tst"
	   << " mesh[.geo]"
	   << " approx"
	   << endl;
      derr << "example:" << endl;
      derr << "  form_grad_tst square P2" << endl;
      exit (1);
}
int main(int argc, char**argv)
{
    environment rheolef (argc, argv);
    if (argc <= 1) usage();
    geo omega (argv[1]);  
    string approx = (argc > 2) ? argv[2] : "P1";
    Float  tol = (argc > 3) ? atof(argv[3]) : 1e-10;
    space Xh(omega, approx);
    string grad_approx = "P" + std::to_string(Xh.degree()-1) + "d";
    space Th (omega, grad_approx, "vector");
    form  grad  (Xh,Th,"grad");
    form  m     (Th,Th,"mass");
    form  inv_m (Th,Th,"inv_mass");
    field uh = lazy_interpolate (Xh, monom_y);
    field grad_uh = inv_m*(grad*uh);
    Float res = m(grad_uh,grad_uh);
    derr<< "res = " << res << endl;
    Float expected_res = 1;
    return (fabs(res - expected_res) < tol) ? 0 : 1;
}
