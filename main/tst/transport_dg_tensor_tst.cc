///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// track a bug in 
//	 field th_chi_h = integrate (ddot(grad_h(chi_h)*ah,xi), qopt);
// aka tensor transport with a field chi_h
#include "rheolef.h"
using namespace std;
using namespace rheolef;
point  a    (const point& x) { return point (1, 1); }
Float  f    (const point& x) { return x[0]; }
point  u_ex (const point& x) { return point (x[0],0); }
tensor sigma(const point& x) { tensor s; s(0,0)=s(0,1)=s(1,1)=x[0]; return s; }
tensor my_prod (const tensor3& A, const point& u) {
	cerr << "A="; A.put(cerr,2); cerr << endl;
	cerr << "u="; u.put(cerr,2); cerr << endl;
        tensor r = A*u;
	cerr << "r="; r.put(cerr,2); cerr << endl;
        return r;
}
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  Float tol = (argc > 2) ? atof(argv[2]) : 1e-10;
  bool dump = (argc > 3);
  space Xh (omega, "P1",  "vector");
  field ah = lazy_interpolate(Xh, a);
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(4);
  Float err = 0;

  // 0) scalar transport:
  space T0h (omega, "P1d");
  field phi_h = lazy_interpolate(T0h, f);
  trial phi(T0h); test psi(T0h);
  field th_phi_h = integrate (dot(ah,grad_h(phi_h))*psi, qopt);
  form  th0      = integrate (dot(ah,grad_h(phi)  )*psi, qopt);
  field eh0 = th_phi_h - th0*phi_h;
  Float err0 = sqrt(dual(eh0,eh0));
  warning_macro ("err0="<<err0);
  err = max(err, err0);
  if (dump && err0 > tol) {
    dout << catchmark ("th_phi_h")       << th_phi_h
         << catchmark ("th_times_phi_h") << th0*phi_h;
  }
  // 1) vector transport:
  space T1h (omega, "P1d", "vector");
  field uh = lazy_interpolate(T1h, u_ex);
  trial u(T1h); test v(T1h);
  field th_uh = integrate (dot(grad_h(uh)*ah,v), qopt);
  form  th1   = integrate (dot(grad_h(u) *ah,v), qopt);
  field eh1 = th_uh - th1*uh;
  Float err1 = sqrt(dual(eh1,eh1));
  warning_macro ("err1="<<err1);
  err = max(err, err1);
  if (dump && err1 > tol) {
    dout << catchmark ("th_uh")       << th_uh
         << catchmark ("th_times_uh") << th1*uh;
  }
  // 2) tensor transport:
  space T2h (omega, "P1d", "tensor");
  field chi_h = lazy_interpolate(T2h, sigma);
  trial chi(T2h); test xi(T2h);
  field th_chi_h = integrate (ddot(grad_h(chi_h)*ah,xi), qopt);
  form  th       = integrate (ddot(grad_h(chi)  *ah,xi), qopt);
  field eh2 = th_chi_h - th*chi_h;
  Float err2 = sqrt(dual(eh2,eh2));
  warning_macro ("err2="<<err2);
  err = max(err, err2);
  if (dump && err2 > tol) {
    dout << catchmark ("th_chi_h")       << th_chi_h
         << catchmark ("th_times_chi_h") << th*chi_h;
  }
  return (err < tol) ? 0 : 1;
}
