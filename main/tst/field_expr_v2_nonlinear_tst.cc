///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// new implementation of field expressions: basic tests

#include "rheolef.h"
using namespace rheolef;
using namespace std;

Float f1  (const point& x) { return x[0]+2*x[1]+3*x[2]; }
struct f1c {
  Float operator() (const point& x) const { return f1(x); }
};
point f2  (const point& x) { return point (x[0]+2*x[1]+3*x[2], 5*x[1]+x[2], 3*x[2]); }
struct f2c {
  point operator() (const point& x) const { return f2(x); }
}; 
struct f3c {
  tensor operator() (const point& x) const { return tensor(); }
};
template <class Result1, class Result2>
void check (string expr, Result1 I, Result2 I_ex, Float& err_max) {
  Float err = norm (I-I_ex);
  err_max = max (err, err_max);
  dout << "Expr = " << expr << endl;
  dout << "I    = " << I << endl;
  dout << "I_ex = " << I_ex << endl;
  dout << "err  = " << err << endl;
}
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega(argv[1]);
  space Xh (omega, "P1");
  space Vh (omega, "P1","vector");
  Float tol = (argc > 2) ? atof(argv[2]) : 1e-10;
  Float err_max = 0;
  quadrature_option qopt;
  qopt.set_order (3);
  point u0 (1.5, 2.5);

  // interpolate
  field f1_h = lazy_interpolate (Xh, f1c());
  field f2_h = lazy_interpolate (Vh, f2);

  // basic call to integrate()
  check ("integrate (omega, f1_h, qopt)",
          integrate (omega, f1_h, qopt),
          1.5, err_max);
  check ("integrate (omega, f1c(), qopt)",
          integrate (omega, f1c(), qopt),
          1.5, err_max);
  check ("integrate (omega, f2, qopt)",
          integrate (omega, f2, qopt),
          u0, err_max);
  // unary minus
  check ("integrate (omega, -f1c(), qopt)",
          integrate (omega, -f1c(), qopt),
         -1.5, err_max);
  check ("integrate (omega, -functor(f1), qopt)",
          integrate (omega, -functor(f1), qopt),
          -1.5, err_max);
  check ("integrate (omega, -functor(f2), qopt)",
          integrate (omega, -functor(f2), qopt),
          -u0, err_max);
  // predef unary fct
  check ("integrate (omega, abs(f1c()), qopt)",
          integrate (omega, abs(f1c()), qopt),
          1.5, err_max);
  // binary minus
  check ("integrate (omega, f1_h-f1c(), qopt)",
          integrate (omega, f1_h-f1c(), qopt),
          0., err_max);
  check ("integrate (omega, f2_h-f2, qopt)",
          integrate (omega, f2_h-f2, qopt),
          point(0,0), err_max);
  check ("integrate (omega, f2c()-u0, qopt)",
          integrate (omega, f2c()-u0, qopt),
          point(0,0), err_max);
  check ("integrate (omega, f2-u0, qopt)",
          integrate (omega, f2-u0, qopt),
          point(0,0), err_max);
  // binary multiplies
  check ("integrate (omega, f1_h*f1c() - sqr(f1), qopt)",
          integrate (omega, f1_h*f1c() - sqr(f1), qopt),
          0., err_max);
  check ("integrate (omega, 2*(f1_h*f1) - sqr(sqrt(2)*functor(f1)), qopt)",
          integrate (omega, 2*(f1_h*f1) - sqr(sqrt(2)*functor(f1)), qopt),
          0., err_max);
  // binary divides
  check ("integrate (omega, f1c()/f1c() - 1, qopt)",
          integrate (omega, f1c()/f1c() - 1, qopt),
          0., err_max);
  check ("integrate (omega, f1_h/f1_h - 1, qopt)",
          integrate (omega, f1_h/f1_h - 1, qopt),
          0., err_max);
  check ("integrate (omega, 1/f1_h - 1/f1c(), qopt)",
          integrate (omega, 1/f1_h - 1/f1c(), qopt),
          0., err_max);
  // binary function
  check ("integrate (omega, atan2(f1_h,f1_h) - atan2(f1,f1), qopt)",
          integrate (omega, atan2(f1_h,f1_h) - atan2(f1,f1), qopt),
          0., err_max);
  check ("integrate (omega, atan2(f1_h,1) - atan2(f1,1), qopt)",
          integrate (omega, atan2(f1_h,1) - atan2(f1,1), qopt),
          0., err_max);
  return (err_max < tol) ? 0 : 1;
}
