///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute:
//  \int_\Omega weight(x) dx = mes(\Omega) with weight
//
// check value on the unit cube [0,1]^d
//
// TODO: riesz: not yet
#include "rheolef/rheolef.h"
using namespace rheolef;
using namespace std;

//Float f_1 (const point& x) { return 1; }
//Float f_1 = 1;
// check int-to-Float constant conversion:
int f_1 = 1;
struct f_x {
  Float operator() (const point& x) const { return x[0]; }
};
Float f_y  (const point& x) { return x[1]; }
Float f_x2 (const point& x) { return sqr(x[0]); }
Float f_y2 (const point& x) { return sqr(x[1]); }
Float f_xy (const point& x) { return x[0]*x[1]; }

template <class Function>
int check (const space& Vh, Function f, string name, Float expected_value, Float tol) {
  field mfh = riesz (Vh, f);
  field one (Vh, 1.0);
  Float int_f_dx = dual(mfh,one);
  if (fabs(int_f_dx - expected_value) > tol) {
    cout << setprecision(numeric_limits<Float>::digits10)
         << "int " << name << " dx = " << int_f_dx << " but " << expected_value
	 << " was expected (error = " << fabs(int_f_dx - expected_value) << ")" << endl;
    return 1;
  } else {
    cout << "int " << name << " dx = " << int_f_dx << " : ok" << endl;
    return 0;
  }
}
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega_h (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1";
  Float  tol    = (argc > 3) ? atof(argv[3]) : 100*numeric_limits<Float>::epsilon();
  space Vh (omega_h, approx);
  int status = 0;
  status |= check (Vh, f_1, "1", 1, tol);
  if (approx == "P1" || approx == "P2") {
     status |= check (Vh, f_x(), "x", 0.5, tol);
     status |= check (Vh, f_y,   "y", 0.5, tol);
  }
  if (approx == "P2") {
     status |= check (Vh, f_x2, "x2", Float(1)/3, tol);
     status |= check (Vh, f_y2, "y2", Float(1)/3, tol);
     status |= check (Vh, f_xy, "xy", Float(1)/4, tol);
  }
  return status;
}
