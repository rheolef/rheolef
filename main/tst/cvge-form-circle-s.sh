#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# check high order circle_s_gmsh.mshcad
#  - directly : measure & L^inf boundary node interpolation
#  - or via rheolef : only measure
#
if test $# -eq 0; then
  echo "usage: $0 form [-e|-t] order [nx=0] [ny=0] [nz=0] [-rheolef]"
  echo "example: sh 0 mass -e 3 0 0 0"
  echo "         sh $0 mass -e 3 0 0 0 -rheolef"
  echo "         sh $0 mass -t 1 0 0 0"
  exit 1
fi
form="$1"; shift
elt=`echo x"$1" | sed -e 's/x-//'`; shift
order=${1-"3"}
nx=${2-"0"}
ny=${3-"0"}
nz=${4-"0"}
if test x"$5" = x"-rheolef"; then
  use_rheolef=true
  software="rheolef"
  echo "use rheolef"
else
  software="gawk"
  form="mass"; # not yet grad_grad
  echo "use gawk"
fi
GMSH=/usr/bin/gmsh
#GMSH=gmsh
if test $elt = e; then
 mshlist="4 8 16 32 64 128"
else
 mshlist="8 12 16 24 32 48 64"
fi
basename="circle_s_gmsh_check-${order}-nx${nx}-ny${ny}"
tmp="aa"
plot="$basename.plot"
gdat="$basename.gdat"
msh="$tmp.msh"
geo="$tmp.geo"
mshcad="$tmp.mshcad"
mshcad_n="${tmp}-n.mshcad"
log="$tmp.log"
errlog="$tmp.errlog"
removelist="$mshcad $log $errlog ${mshcad_n} $msh fit.log"
verbose=true
# ------------------------------------------
# utility
# ------------------------------------------
my_eval () {
  command="$*"
  if test "$verbose" = true; then echo "! $command" 1>&2; fi
  eval $command
  if test $? -ne 0; then
    echo "$0: error on command: $command"
    exit 1
  fi
}
# ----------------------------------------------
# build .plot
# ----------------------------------------------
cat > $plot << EOF2
set title "$form(x^$nx*y^$ny*z^${nz},1) P$order with $software"
set logscale
a1 = 1.0
c1 = 1.0
f1(log10_h) = a1*log10_h+ c1
g1(h) = 10.0**(f1(log10(h)))
fit f1(x) "$gdat" u (log10(1/\$1)):(log10(\$2)) via a1, c1
titre1 = sprintf("measure slope = %g", a1)
EOF2
if test "${use_rheolef}" != "true" -a $elt = e; then
cat >> $plot << EOF3a
a2 = 1.0
c2 = 1.0
f2(log10_h) = a2*log10_h+ c2
g2(h) = 10.0**(f2(log10(h)))
fit f2(x) "$gdat" u (log10(1/\$1)):(log10(\$3)) via a2, c2
titre2 = sprintf("boundary slope = %g", a2)
plot \
  "$gdat" u (1/\$1):2 t "measure" w lp, \
  g1(x) title titre1, \
  "$gdat" u (1/\$1):3 t "boundary" w lp, \
  g2(x) title titre2
print titre1
print titre2
pause -1 "<retour>"
EOF3a
else
cat >> $plot << EOF3b
plot \
  "$gdat" u (1/\$1):2 t "measure" w lp, \
  g1(x) title titre1
print titre1
pause -1 "<retour>"
EOF3b
fi
# ----------------------------------------------
# build .gdat
# ----------------------------------------------
echo "# order=$order nx=${nx} ny=${ny} nz=${nz}" | tee $gdat
echo "# n error_measure error_linf" | tee -a $gdat
for n in $mshlist; do
  my_eval "mkgeo_ball -s -$elt $n -order $order -noclean > $geo"
  msh="output.msh"
  if test "${use_rheolef}" != "true"; then
    if test "$elt" = e; then
      gawk="circle_s_gmsh_check.awk"
    else
      gawk="sphere_s_gmsh_check.awk"
    fi
    my_eval "gawk -v order=$order -v nx=${nx} -v ny=${ny} -v nz=${nz} -f $gawk < $msh > $log 2> $errlog"
    if grep FATAL $errlog >/dev/null; then
	cat $errlog 1>&2
	exit 1
    fi
    error_measure=`grep error_measure $log | gawk '{print $3}'`
    error_linf=`grep error_linf $log | gawk '{print $3}'`
  else
    my_eval "(./form_circle_tst -form "$form" $geo -app P${order} -nx ${nx} -ny ${ny} -nz ${nz} 2>&1) > $log"
    error_measure=`grep '^error' $log | gawk '{print $3}'`
    error_linf=${error_measure}
  fi
  echo "$n ${error_measure} ${error_linf}" | tee -a $gdat
done
# ----------------------------------------------
# run .plot
# ----------------------------------------------
gnuplot $plot
echo "! file $gdat created" 1>&2
echo "! file $plot created" 1>&2
#rm -f $removelist
