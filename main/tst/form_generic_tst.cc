///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute:
//  \int_\Omega div(u) div(v) dx dy
//
// check value on the unit square [0,1]^2
//
// only tested yet in the 2D cartesian case
//
// TODO: optimize :
//   load one time the meshand build the a(.,.) form
//   then loop on monoms
//
#include "rheolef/rheolef.h"
using namespace rheolef;
using namespace std;
#include "form_2D_D.icc"
#include "form_div_div.icc"

typedef Float (*function_type)(const point&);

struct list_type {
	string        name;
	function_type function;
};
Float monom_1  (const point& x) { return 1; }
Float monom_x  (const point& x) { return x[0]; }
Float monom_y  (const point& x) { return x[1]; }
Float monom_x2 (const point& x) { return x[0]*x[0]; }
Float monom_xy (const point& x) { return x[0]*x[1]; }
Float monom_y2 (const point& x) { return x[1]*x[1]; }

list_type xy_monom_list [] = {
	//   monom
	// id   fct    
	{"1",	monom_1},
	{"x",	monom_x},
	{"y",	monom_y},
	{"x2",	monom_x2},
	{"xy",	monom_xy},
	{"y2",	monom_y2}
};
const unsigned int xy_monom_max = sizeof(xy_monom_list)/sizeof(list_type);

void usage()
{
      derr << "form_generic_tst: usage: form_generic_tst"
	   << " [-form string]"
	   << " {-Igeodir}*"
	   << " -|mesh[.geo]"
	   << " [-approx string]"
	   << endl;
      derr << "example:" << endl;
      derr << "  form_generic_tst -form 2D_D    square P2" << endl;
      derr << "  form_generic_tst -form div_div square P2" << endl;
      exit (1);
}
int main(int argc, char**argv)
{
    environment rheolef (argc, argv);
    //
    // load geometry and options
    //
    geo omega;  
    string form_name = "2D_D";
    string approx = "P2";
    bool mesh_done = false;
    vector<string>        u_id (3,"");
    vector<string>        v_id (3,"");
    vector<size_t>        u_idx (3,0);
    vector<size_t>        v_idx (3,0);
    vector<function_type> u_function (3,function_type(0));
    vector<function_type> v_function (3,function_type(0));

    if (argc <= 1) usage() ;

    for (int i = 1; i < argc; i++ ) {

      if (argv [i][0] == '-' && argv [i][1] == 'I') {
	  append_dir_to_rheo_path (argv[i]+2) ;
      } else if (strcmp(argv[i], "-form") == 0) {
	  form_name = argv[++i];
      } else if (strcmp(argv[i], "-approx") == 0) {
	  approx = argv[++i];
      } else if (strcmp(argv[i], "-") == 0) {
	  // input geo on standard input
	  if (mesh_done) usage() ;
	  derr << "! load geo on stdin" << endl ;
	  din >> omega ;
	  mesh_done = true ;
      } else {
	  // input geo on file
	  if (mesh_done) usage() ;
	  omega = geo(argv[i]);
	  mesh_done = true ;
      }
    }
    if (!mesh_done) {
	derr << "form_generic_tst: no mesh specified" << endl;
	usage() ;
    }
    size_t d = omega.dimension();
    check_macro (d == 2, "only 2D mesh are supported");
    space V0h(omega, approx);
    space Vh(omega, approx, "vector");
    field uh (Vh, Float(0));
    field vh (Vh, Float(0));
    form  a (Vh,Vh,form_name);
#ifdef TODO
    derr << "syscoord = " << omega.coordinate_system() << endl;
#endif // TODO
    Float expect_form [6][6][6][6];
    if      (form_name == "2D_D")    init_expect_form_2D_D    (expect_form);
    else if (form_name == "div_div") init_expect_form_div_div (expect_form);
    else error_macro ("unexpected form name \"" << form_name << "\"");

    size_t nmax = (approx == "P2") ? 6 : 3;
    size_t status = 0;
    for (size_t iu0 = 0; iu0 < nmax; iu0++) {
     uh[0] = lazy_interpolate (V0h, xy_monom_list[iu0].function);
     for (size_t iu1 = 0; iu1 < nmax; iu1++) {
      uh[1] = lazy_interpolate (V0h, xy_monom_list[iu1].function);
      for (size_t iv0 = 0; iv0 < nmax; iv0++) {
       vh[0] = lazy_interpolate (V0h, xy_monom_list[iv0].function);
       for (size_t iv1 = 0; iv1 < nmax; iv1++) {
           vh[1] = lazy_interpolate (V0h, xy_monom_list[iv1].function);
           Float result = a(uh, vh);
           string uname = xy_monom_list[iu0].name + "," + xy_monom_list[iu1].name;
           string vname = xy_monom_list[iv0].name + "," + xy_monom_list[iv1].name;
           derr << setprecision(numeric_limits<Float>::digits10)
            << form_name << "(omega," << approx << "[" << uname << "]," << approx << "[" << vname << "]) = "
	    << result;
           Float expect = expect_form [iu0][iu1][iv0][iv1];
           Float tol = 1e-10;
           if (fabs(result - expect) > tol) {
               derr << " but it was expected " << expect 
	            << " : error = " << fabs(result - expect);
               status = 1;
           }
	   derr << endl;
    }}}}
    if (status == 0) {
      derr << "all test ok for " << form_name << endl;
    } else {
      derr << "some errors for " << form_name << endl;
    }
    return status;
#ifdef TODO
#endif // TODO
}
