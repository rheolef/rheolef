///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check for field expressions as:
//	field u = (a*x + y)*(c*z + t); 
// where: x,y: scalar fields, 
//        z,t: vector fields
// this was buggy (core dump) and has been fixed.
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  Float  tol = (argc > 2) ? atof(argv[2]) : 1e-10;
  space Xh (omega, "P1");
  space Zh (omega, "P1", "vector");
  field xh (Xh,1), yh(Xh,1);
  field zh (Zh,1), th(Zh,1);
  field uh = lazy_interpolate(Zh, (2*xh + yh)*(3*zh + th - zh));
  field uh_expect (Zh, 9);
  Float err = field(uh - uh_expect).max_abs();
  derr << "err = " << err << endl;
  return (err < tol) ? 0 : 1;
}
