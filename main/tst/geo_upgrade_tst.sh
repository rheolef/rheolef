#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
BINDIR="../bin"
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0
L="
edge_s_2d_p9
edge_s_3d_p9
triangle_p1
triangle_p2
triangle_p3
triangle_p4
triangle_p5
triangle_p9
triangle_p10
triangle_s_p9
quadrangle_p1
quadrangle_p2
quadrangle_p9
quadrangle_s_p9
tetra_p1
tetra_p2
tetra_p3
tetra_p4
tetra_p5
tetra_p6
tetra_p7
tetra_p8
tetra_p9
tetra_p10
prism_p1
hexa_p1
triangle2_p5
tetra2_p5
circle_p1
circle_p2
circle_p3
circle_p5
circle_s_p1
circle_s_p2
circle_q_p1
circle_q_p2
sphere_s_p1
sphere_s_p2
sphere_s_q_p1
sphere_s_q_p2
sphere_p1
sphere_p2
sphere_p3
sphere_H_p1
sphere_H_p2

carre-tq-10-bdry
carre-tq-10-dom
cube-dom
cube-H-6-dom
cube-P-5-dom
my_cube_PH-5
my_cube_TP-5
my_cube_TPH-5
"

# conversion from old rheolef fmt (v1 or v2) to the new one (v4: header,ntri,nqua,ect)
L_TODO_v1="
line-dom
carre-dom
carre-bamg
carre-q-10-dom
carre-bamg-q-dom
cube-2
cube-H-6-dom
"
L_TODO_V2="
tetra2.geo.gz
dodecaedre00.geo.gz
dodecaedre01.geo.gz
particule.geo.gz
bielle.geo.gz
"
L_TOO_BIG="
carre-100-dom
cube-10-dom
cube-gmsh
"

for geo in $L; do
  run "${BINDIR}/geo -upgrade -check - < ${SRCDIR}/${geo}-v1.geo 2>/dev/null | diff -B -w ${SRCDIR}/${geo}-v2.geo - >/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
