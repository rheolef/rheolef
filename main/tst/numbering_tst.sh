#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
export TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
export SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
status=0

#command="./basis_tst P2"
#echo "      $command"
#eval $command 2>/dev/null | diff $SRCDIR/P2.out.valid -
#if test $? -ne 0; then status=1; echo "        => *NO*"; fi

command="./numbering_tst P2 2>/dev/null | diff $SRCDIR/P2_numbering.out.valid - >/dev/null"
echo "      $command"
eval $command
if test $? -ne 0; then status=1; echo "        => *NO*"; fi

exit $status
