#!geo

mesh
4
header
 dimension 2
 nodes	9
 triangles	8
 edges	16
end header

0 0
1 0
0 1
1 1
0.5 0
0.5 0.5
0 0.5
1 0.5
0.5 1

t	0 4 6
t	4 5 6
t	6 5 2
t	4 1 5
t	1 7 5
t	7 8 5
t	5 8 2
t	7 3 8

e	0 4
e	4 1
e	1 5
e	5 2
e	2 6
e	6 0
e	1 7
e	7 3
e	3 8
e	8 2
e	4 5
e	5 6
e	6 4
e	7 8
e	8 5
e	5 7
