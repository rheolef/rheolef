///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// usage: thisprog mesh [Pk] [tol] [q] [-dump]
//
//    solve :
//    u - div_s(2Ds(u)) = f on Gamma
//
//    => seems to converge, but not optimaly
//       since u.n is not controled by the energy norm
//       only L2 cv for the tangential ut=u-(u.n)n component ?
//
#include "rheolef.h"
using namespace std;
using namespace rheolef;
point u_exact (const point& x) { return x; }
point f       (const point& x) { return 5.0*x; }
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1";
  Float  tol    = (argc > 3) ? atof(argv[3]) : 1e-10;
  space Xh (omega, approx, "vector");
  size_t k = Xh.degree();
  size_t q  = (argc > 4) ? atoi(argv[4]) : 4*k+2;
  bool dump = (argc > 5);
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(q);
  trial u (Xh); test v(Xh);
  form a = integrate (dot(u,v) + 2*ddot(Ds(u),Ds(v)), qopt);
  form m = integrate (dot(u,v), qopt);
  solver sa (a.uu());
  field pi_h_u = lazy_interpolate(Xh, u_exact);
  field lh = integrate (dot(f,v), qopt);
  field uh (Xh);
  uh.set_u() = sa.solve (lh.u());
  field eh = uh - pi_h_u; 
  Float err_u_l2   = sqrt(m(eh,eh)); 
  Float err_u_linf = eh.max_abs();
  derr << "# geo    " << omega.name() << endl
       << "# order  " << omega.order() << endl
       << "# approx " << approx << endl
       << "# quad   " << q << endl
       << "# nelt err_l2 err_linf" << endl
       << omega.size() << " " 
       << err_u_l2   << " "
       << err_u_linf << endl;
  form mn = integrate (dot(normal(),u)*dot(normal(),v), qopt);
  Float err_un_l2   = sqrt(mn(eh,eh)); 
  derr << "err_un_l2 = " << err_un_l2 << endl;
  form ms = integrate (dot(u,v)-dot(normal(),u)*dot(normal(),v), qopt);
  Float err_us_l2   = sqrt(ms(eh,eh)); 
  derr << "err_us_l2 = " << err_us_l2 << endl;
  form as = integrate (2*ddot(Ds(u),Ds(v)), qopt);
  Float err_us_h1   = sqrt(as(eh,eh)); 
  derr << "err_us_h1 = " << err_us_h1 << endl;
  if (dump) {
    dout << catchmark("uh")     << uh
         << catchmark("pi_h_u") << pi_h_u
         << catchmark("eh")     << eh;
  }
  return (err_us_l2 < tol) ? 0 : 1;
}
