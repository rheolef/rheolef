#!geo

mesh
4
header
 dimension 3
 nodes	8
 hexahedra	1
 quadrangles	6
 edges	12
end header

0 0 0
1 0 0
1 1 0
0 1 0
0 0 1
1 0 1
1 1 1
0 1 1

H	0 1 2 3 4 5 6 7

q	0 3 2 1
q	0 4 7 3
q	0 1 5 4
q	4 5 6 7
q	1 2 6 5
q	2 3 7 6
e	0 1
e	1 2
e	2 3
e	3 0
e	0 4
e	1 5
e	2 6
e	3 7
e	4 5
e	5 6
e	6 7
e	7 4
