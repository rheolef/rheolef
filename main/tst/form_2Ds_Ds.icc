#ifndef _RHEOLEF_FORM_2DS_DS_ICC
#define _RHEOLEF_FORM_2DS_DS_ICC
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// 
// toolbox for checking the Ds operator
// 
#include <iostream>
#include <vector>
#include <ginac/ginac.h>
#include "rheolef.h"
using namespace rheolef;
using namespace std;

//using namespace GiNaC; // clash:  class GiNaC::tensor & rheolef::tensor
using GiNaC::ex;
using GiNaC::matrix;
using GiNaC::symbol;
using GiNaC::symtab;
using GiNaC::exmap;
using GiNaC::parser;
using GiNaC::ex_to;
using GiNaC::numeric;

// --------------------------------------------------------------
// scalar-valued class-function:
// constructor from a string or a ginac expression
// --------------------------------------------------------------
class scalar_function {
public:

// allocators:

  scalar_function (const ex& f, const point_basic<symbol>& x)
   : _x(x), _f(f), _m() {}

  scalar_function (const string& f_expr)
   : _x(symbol("x"),symbol("y"),symbol("z")), _f(), _m()
  {
    symtab table;
    table["x"] = _x[0];
    table["y"] = _x[1];
    table["z"] = _x[2];
    parser reader (table);
    _f = reader (f_expr);
  }
  scalar_function (const scalar_function& fun)
   : _x(fun._x), _f(fun._f), _m(fun._m) {}

  const scalar_function& operator= (const scalar_function& fun)
   { _x=fun._x; _f=fun._f; _m=fun._m; return *this; }
    
  ~scalar_function () {}

// accessor:

  Float operator() (const point& x) const {
     _m[_x[0]] = x[0];
     _m[_x[1]] = x[1];
     _m[_x[2]] = x[2];
     Float r = Float (ex_to<numeric>(_f.subs(_m).evalm()).to_double());
     return r;
  }
// data:
protected:
  point_basic<symbol>  _x; 
  ex                   _f;
  mutable exmap        _m;
};
// --------------------------------------------------------------
// vector-valued class-function:
// constructor from a string or a ginac expression
// --------------------------------------------------------------
class vector_function {
public:

// allocators:

  vector_function (const point_basic<ex>& f, const point_basic<symbol>& x)
   : _x(x), _f(f), _m() {}

  vector_function (const point_basic<string>& f_expr, const point_basic<symbol>& x)
   : _x(x), _f(), _m()
  {
    symtab table;
    table["x"] = _x[0];
    table["y"] = _x[1];
    table["z"] = _x[2];
    parser reader (table);
    for (size_t i = 0; i < 3; i++) {
      _f[i] = reader (f_expr[i]);
    }
  }
  vector_function (const vector_function& fun)
   : _x(fun._x), _f(fun._f), _m(fun._m) {}
  const vector_function& operator= (const vector_function& fun)
   { _x=fun._x; _f=fun._f; _m=fun._m; return *this; }

// accesors:

  point operator() (const point& x) const {
     _m[_x[0]] = x[0];
     _m[_x[1]] = x[1];
     _m[_x[2]] = x[2];
     point res = point(ex_to<numeric>(_f[0].subs(_m).evalm()).to_double(),
		  ex_to<numeric>(_f[1].subs(_m).evalm()).to_double(),
		  ex_to<numeric>(_f[2].subs(_m).evalm()).to_double());
     return res;
  }
// data:
protected:
  point_basic<symbol>  _x; 
  point_basic<ex>      _f;
  mutable exmap        _m;
};
// --------------------------------------------------------------
// tensor-valued class-function:
// constructor from a string or a ginac expression
// --------------------------------------------------------------
class tensor_function {
public:

// allocators:

  tensor_function (const matrix& f, const point_basic<symbol>& x)
   : _x(x), _f(ex()), _m()
  {
    for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) {
      _f(i,j) = f(i,j);
    }}
  }
  tensor_function (const tensor_basic<string>& f_expr)
   : _x(symbol("x"),symbol("y"),symbol("z")), _f(ex()), _m() {
    symtab table;
    table["x"] = _x[0];
    table["y"] = _x[1];
    table["z"] = _x[2];
    parser reader (table);
    for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) {
      _f(i,j) = reader (f_expr(i,j));
    }}
  }

// accesors:

  tensor operator() (const point& x) const {
    _m[_x[0]] = x[0];
    _m[_x[1]] = x[1];
    _m[_x[2]] = x[2];
    tensor r;
    for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) {
      r(i,j) = ex_to<numeric>(_f(i,j).subs(_m).evalm()).to_double();
    }}
    return r;
  }
// data:
protected:
  point_basic<symbol>  _x; 
  tensor_basic<ex>     _f;
  mutable exmap        _m;
};
// --------------------------------------------------------------
// symbolic computation of the form 2Ds_Ds(u,v) with given n(x)
// --------------------------------------------------------------
// call with ginac::ex arguments
matrix
Ds (
  const point_basic<ex>&     u,
  const point_basic<ex>&     n,
  const point_basic<symbol>& x)
{
  // compute Du
  ex Du00 =  diff(u[0],x[0]);
  ex Du01 = (diff(u[0],x[1]) + diff(u[1],x[0]))/2;
  ex Du02 = (diff(u[0],x[2]) + diff(u[2],x[0]))/2;
  ex Du11 =  diff(u[1],x[1]);
  ex Du12 = (diff(u[1],x[2]) + diff(u[2],x[1]))/2;
  ex Du22 =  diff(u[2],x[2]);
  matrix Du(3,3);
  Du = {{Du00, Du01, Du02},
        {Du01, Du11, Du12},
        {Du02, Du12, Du22}};
         
  // compute P 
  matrix P(3,3);
  P = {{1-n[0]*n[0],  -n[0]*n[1],  -n[0]*n[2]},
       {-n[0]*n[1] , 1-n[1]*n[1],  -n[1]*n[2]},
       {-n[0]*n[2] ,  -n[1]*n[2], 1-n[2]*n[2]}};
             
  // compute Ds(u) = P*D(u)*P 
  matrix Dsu = P.mul(Du.mul(P)); 
  return Dsu;
}
ex
form_2Ds_Ds (
  const point_basic<ex>&     u,
  const point_basic<ex>&     v,
  const point_basic<ex>&     n,
  const point_basic<symbol>& x)
{
  // compute Ds(u) and Ds(v)
  matrix D_su = Ds (u, n, x);
  matrix D_sv = Ds (v, n, x);
   
  // compute 2Ds(u):Ds(v)
  ex D_su_D_sv = 0;
  for (size_t i = 0; i < 3; i++){
    for (size_t j = 0; j < 3; j++){
      D_su_D_sv += D_su(i,j)*D_sv(i,j);
    }
  }     
  ex expr = 2*D_su_D_sv;
  derr << "2Ds(u):Ds(v) = " << expr << endl;
  return expr;
}
// call with string arguments
tensor_function
Ds (
  const point_basic<string>& u_expr,
  const point_basic<string>& n_expr,
  const point_basic<symbol>& x)
{
    symtab table;
    table["x"] = x[0];
    table["y"] = x[1];
    table["z"] = x[2];
    parser reader(table);
    point_basic<ex> u, n;
    for (size_t i = 0; i < 3; i++) {
      u[i] = reader(u_expr[i]);
      n[i] = reader(n_expr[i]);
    }
    return tensor_function (Ds(u,n,x), x);
}
scalar_function
form_2Ds_Ds (
  const point_basic<string>& u_expr,
  const point_basic<string>& v_expr,
  const point_basic<string>& n_expr,
  const point_basic<symbol>& x)
{
    symtab table;
    table["x"] = x[0];
    table["y"] = x[1];
    table["z"] = x[2];
    parser reader(table);
    point_basic<ex> u, v, n;
    for (size_t i = 0; i < 3; i++) {
      u[i] = reader(u_expr[i]);
      v[i] = reader(v_expr[i]);
      n[i] = reader(n_expr[i]);
    }
    ex r = form_2Ds_Ds (u,v,n,x);
    scalar_function res = scalar_function (r,x);
    return res;
}
#endif // _RHEOLEF_FORM_2DS_DS_ICC
