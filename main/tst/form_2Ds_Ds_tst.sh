#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"3"}
DATADIR=$SRCDIR
BINDIR="../bin"
SBINDIR="../sbin"
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

# core dump on mobyick but run on salsa: ginac bug ?
#echo "      broken (no more tested)"
#exit $status

epsilon="1e-12";     # tolrerance to machine precision

# ===================================================
# 1) 2D tests
# ===================================================
# ---------------------------------------------------
# 1.1) test on a side of a square
# ---------------------------------------------------
u="'cos(y)'   'sin(y)+1'"; # note: expressions containing * may be quotted
v="'cos(y)^2' 'y^3'"
n="1 0"
qorder="10"
# geo			err_P1  err_P2  err_P3
L="
carre-bamg-v2.right	4e-3	2e-6	6e-8
carre-100-dom-v2.right	4e-5	2e-10	2e-10
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2 P3; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_2Ds_Ds_tst $DATADIR/${geo} $Pk $qorder $err $u $v $n 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# ---------------------------------------------------
# 1.2) test on a circle
# ---------------------------------------------------
u="x^3     'x^2*y'"; # note: expressions containing * may be quotted
v="'x*y^2' x^3    "
n="x y"
qorder="12"
# geo			err_P1	err_P2
L="
circle_s-40-fix		6e-3	3e-5
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2; do 
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_2Ds_Ds_tst $DATADIR/${geo}-${Pk} $Pk $qorder $err $u $v $n 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# ------------------------------------------------------
# 1.3) banded level set: test on the circle
# ------------------------------------------------------
run "$SBINDIR/mkgeo_grid_2d -v4 -t 40 -a -2 -b 2 -c -2 -d 2 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-40.geo 2>/dev/null"
qorder="6"
# geo		err_P1
L="
mesh-2d-40	0.0055
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_2Ds_Ds_tst ${geo} $Pk $qorder $err $u $v $n 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
run "rm -f mesh-2d-40.geo"
# ===================================================
# 2) 3D tests
# ===================================================
# ---------------------------------------------------
# 2.1) test on the right face of a cube
# ---------------------------------------------------
u="'cos(1+x+z)' 'sin(2*x+z)^2' 'cos(x-z)^3'" ; # note: expressions may be quotted
v="'sin(x)^3'   'cos(x)^2'     'z^3'"
n="0 1 0"
qorder="12"
# geo			err_P1	err_P2	err_P3	err_P4
L="
cube-10-dom-v2.right	2e-2	7e-3	5e-7	2e-9
cube-P-5-dom-v2.right	6e-2	6e-5	3e-5	4e-8
my_cube_TPH-5-v2.right	9e-2	1e-4	8e-5	8e-8
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2 P3 P4; do 
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_2Ds_Ds_tst $DATADIR/${geo} $Pk $qorder $err $u $v $n 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# ---------------------------------------------------
# 2.2) test on the surface of a sphere
# ---------------------------------------------------
u=" z    x^2 'x*y'"; # note: expressions containing * may be quotted
v="'x*y' x^2  z"
n="x y z"
qorder="12"
# geo			err_P1	err_P2
L="
sphere_s-10-fix		0.0008  0.0004
sphere_s_q-10-fix	0.0300  0.0003
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2; do 
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_2Ds_Ds_tst $DATADIR/${geo}-${Pk} $Pk $qorder $err $u $v $n 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# ------------------------------------------------------
# 2.3) banded level set: test on the sphere
# ------------------------------------------------------
run "$SBINDIR/mkgeo_grid_3d -v4 -T 20 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2  2>/dev/null| $BINDIR/geo -upgrade - > mesh-3d-20.geo 2>/dev/null"
qorder="6"
# geo			err_P1
L="
mesh-3d-20		0.14
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_2Ds_Ds_tst ${geo} $Pk $qorder $err $u $v $n 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
run "rm -f mesh-3d-20.geo"

exit $status
