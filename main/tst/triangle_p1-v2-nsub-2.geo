#!geo

mesh
4
header
 dimension 2
 nodes	6
 triangles	4
 edges	9
end header

0 0
1 0
0 1
0.5 0
0.5 0.5
0 0.5

t	0 3 5
t	3 4 5
t	5 4 2
t	3 1 4

e	0 3
e	3 1
e	1 4
e	4 2
e	2 5
e	5 0
e	3 4
e	4 5
e	5 3
