///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check field expr with normal() operator

#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  size_t d = omega.dimension();
  string dom_name = (argc > 2) ? argv[2] : "right";
  Float prec = (argc > 3) ? atof(argv[3]) : 1e-10;
  Float tol = 1e-10;
  size_t k = omega.order();
  string n_approx = "P" + std::to_string(k-1) + "d";
  warning_macro ("n_approx="<<n_approx);
  space Wh (omega[dom_name], n_approx, "vector");
  field nh = lazy_interpolate(Wh, normal());
  dout << round (nh, prec);
  point n_right = (d == 2) ? point(1,0) : point(0,1,0);
  quadrature_option qopt;
  qopt.set_order(2*k);
  Float err = integrate(Wh.get_geo(), norm(normal()-n_right), qopt);
  derr << "err="<< err << endl;
  return (err < tol) ? 0 : 1;
}
