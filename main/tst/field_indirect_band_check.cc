///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
#include "rheolef/pretty_name.h"
using namespace std;
using namespace rheolef;
Float phi (const point& x) { return norm(x) - 1; }
int main (int argc, char**argv) {
  environment rheolef(argc, argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-10;
  field phi_h;
  din >> catchmark("phi") >> phi_h;
  const space& Xh     = phi_h.get_geo();
  band gh (phi_h);
  space Bh (gh.band(), "P1");
  Bh.block ("isolated");
  Bh.unblock ("zero");
  field phi_h_band (Bh);
  din >> catchmark("phi_band") >> phi_h_band;
  field eh = phi_h [gh.band()] - phi_h_band;
  Float err = eh.max_abs(); 
  derr << "err = " << err << endl;
  return (err < tol) ? 0 : 1;
}
