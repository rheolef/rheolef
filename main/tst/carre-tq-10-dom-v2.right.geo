#!geo

mesh
4
header
 dimension 2
 nodes	11
 edges	10
end header

1 0
1 1
1 0.1
1 0.2
1 0.3
1 0.4
1 0.5
1 0.6
1 0.7
1 0.8
1 0.9

e	0 2
e	2 3
e	3 4
e	4 5
e	5 6
e	6 7
e	7 8
e	8 9
e	9 10
e	10 1


domain
boundary
2 1 10
0
1
2
3
4
5
6
7
8
9
