#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

run "../sbin/mkgeo_grid_2d 10 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-2d.geo 2>/dev/null"
if test $? -ne 0; then status=1; exit 1; fi

loop_mpirun "./integrate_2d_tst mesh-2d.geo >/dev/null 2>/dev/null "
if test $? -ne 0; then status=1; fi

run "rm -f mesh-2d.geo"
if test $? -ne 0; then status=1; fi

exit $status
