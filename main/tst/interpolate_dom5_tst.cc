///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check interpolate on a bdry space, based on a sub-domain (geo_domain)
// with an undeterminated return type
//
// usage: prog omega approx domain prec
//
// example:
//   mpirun -np 3 ./interpolate_dom5_tst ../../../rheolef/nfem/ptst/my_cube_TP-5-v2.geo P2 left 1e-8 
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
point  v   (const point& x) { return point(1,0); }
point  w   (const point& x) { return point(0,1); }
tensor tau (const point& x) { tensor t = {{0,1},{1,0}}; return t; }
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  geo omega (argv[1]);
  string approx   = (argc > 2) ?      argv[2]  : "P1";
  string dom_name = (argc > 3) ?      argv[3]  : "left";
  Float tol       = (argc > 4) ? atof(argv[4]) : 1e-10;
  space Vh (omega[dom_name], approx, "vector");
  space Th (omega[dom_name], approx, "tensor");
  field vh    = lazy_interpolate (Vh, v);
  field wh    = lazy_interpolate (Vh, w);
  field tau_h = lazy_interpolate (Th, tau);
  field w2h   = lazy_interpolate (Vh, tau_h*vh); // undeterminated Expr::value_type: scalar or vector or tensor
  Float err = field(wh - w2h).max_abs();
  derr << "err = " << err << endl;
  return err < tol ? 0 : 1;
}
