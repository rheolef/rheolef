///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/space_numbering.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;

int
main (int argc, char **argv)
{
  environment distributed (argc, argv);
  if (argc != 2) {
      cerr << "usage: " << argv[0] << " <base>" << endl;
      exit (1);
  }
  {
    basis_basic<Float> b (argv[1]);
    size_t  dim = 2;
    size_t  n = 10;
    size_t  size_by_dimension [4];
    size_t  size_by_variant [reference_element::max_variant];
    size_by_dimension [0] = (n+1)*(n+1);
    size_by_dimension [1] = 2*n*(n+1) + n*n;
    size_by_dimension [2] = 2*n*n;
    size_by_dimension [3] = 0;
    size_by_variant [reference_element::p] = size_by_dimension [0];
    size_by_variant [reference_element::e] = size_by_dimension [1];
    size_by_variant [reference_element::t] = size_by_dimension [2];
    size_by_variant [reference_element::q] = 0;
    size_by_variant [reference_element::T] = 0;
    size_by_variant [reference_element::P] = 0;
    size_by_variant [reference_element::H] = 0;
    geo_size gs;
    for (size_t dim = 0; dim < 4; dim++) {
      gs.ownership_by_dimension [dim] = distributor (distributor::decide, communicator(), size_by_dimension [dim]);
    }
    for (size_t variant = 0; variant < reference_element::max_variant; variant++) {
      gs.ownership_by_variant [variant] = distributor (distributor::decide, communicator(), size_by_variant [variant]);
    }
    size_t dis_ndof = space_numbering::dis_ndof (b, gs, dim);

    cout << "\tndof => " << dis_ndof << endl;
  }
  return 0;
}
