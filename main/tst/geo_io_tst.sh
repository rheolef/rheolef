#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
DATA_DIR=${SRCDIR}
NPROC_MAX=${NPROC_MAX-"6"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0

L="
zero-5.geo
line-1-bdry.geo
line-dom-v2.geo
edge_s_3d_p1-v2.geo
line-2-bdry.geo
line-3-bdry.geo
line-20-bdry.geo
carre-v2.geo
cube-P-5-dom-v2.geo
cube-H-6-dom-v2.geo

circle_s_p1-v2.geo
sphere_s_p1-v2.geo
sphere_s_q_p1-v2.geo
carre-3-dom-v2.geo
carre-dom-v2.geo
cube-dom-v2.geo
carre-100-dom-v2.geo
line-dom-v2.right.geo
carre-100-dom-v2.right.geo
carre-bamg-v2.right.geo
cube-10-dom-v2.right.geo
cube-gmsh-v2.right.geo
circle_p1-v2.geo
circle_p2-v2.geo
circle_p3-v2.geo
circle_p5-v2.geo
circle_q_p1-v2.geo
sphere_p1-v2.geo
sphere_p2-v2.geo
sphere_p3-v2.geo
sphere_H_p1-v2.geo

carre-tq-10-v2.geo
carre-tq-10-dom-v2.geo
my_cube_PH-5-v2.geo
my_cube_TP-5-v2.geo
my_cube_TPH-5-v2.geo
extrude-TP-gmsh-dom-v2.geo

circle_s_p2-v2.geo
circle_p2-v2.geo
circle_q_p2-v2.geo
sphere_s_p2-v2.geo
sphere_s_q_p2-v2.geo
sphere_p2-v2.geo
sphere_H_p2-v2.geo

triangle_p5-v2.geo
triangle2_p5-v2.geo
tetra_p5-v2.geo
tetra2_p5-v2.geo

circle_s_p10-v2.geo
circle_p5-v2.geo
circle_q_p10-v2.geo
circle_tq_p10-v2.geo
sphere_s_p10-v2.geo
sphere_s_q_p10-v2.geo
sphere_p9-v2.geo
"

for geo in $L; do
  loop_mpirun "./geo_io_tst < ${SRCDIR}/$geo 2>/dev/null | diff -B -w ${SRCDIR}/$geo - >/dev/null"
  if test $? -ne 0; then status=1; fi
done

#export TMPDIR="."

L="
"

L_TODO="
tetra2.geo.gz
dodecaedre00.geo.gz
dodecaedre01.geo.gz
particule.geo.gz
bielle.geo.gz
"

for geo in $L; do
  #run "gzip -dc < ${DATA_DIR}/$geo > tmp.geo"
  loop_mpirun "./geo_io_tst < $DATA_DIR/$geo 2>/dev/null | diff -B -w $DATA_DIR/$geo - >/dev/null"
  if test $? -ne 0; then status=1; fi
done
#run "rm -f tmp.geo"
exit $status
