///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// compare two variants of the HDG method for the dirchlet problem
// - one with schur completement that eliminate u and sigma
// - the other without elimination
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
// ----------------------------------------
// cosinusprod
// ----------------------------------------
// cosinusprod_laplace.icc
struct f {
  Float operator() (const point& x) const { 
    return d*pi*pi*cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  f(size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; const Float pi;
};
struct g {
  Float operator() (const point& x) const { 
    return cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  g(size_t d1) : pi(acos(Float(-1))) {}
  const Float pi;
};
// cosinusprod.icc
struct u_exact {
  Float operator() (const point& x) const { 
    return cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  u_exact(size_t d1) : d(d1), pi(acos(Float(-1.0))) {}
  size_t d; Float pi;
};
// cosinusprod_grad.icc
struct grad_u {
  point operator() (const point& x) const { 
    return -pi*point(
     sin(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]),
     cos(pi*x[0])*sin(pi*x[1])*cos(pi*x[2]), 
     cos(pi*x[0])*cos(pi*x[1])*sin(pi*x[2])); }
  grad_u(size_t d1) : d(d1), pi(acos(Float(-1.0))) {}
  size_t d; Float pi;
};
// ----------------------------------------
// 0) hdg step: Pkd
// ----------------------------------------
void dirichlet_hdg (geo omega, string approx, Float alpha,
      field& sigma_h, field& uh, field& lambda_h)
{
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh,
        Mh (omega["sides"], approx);
  Mh.block("boundary");
  space Wh(Mh.get_geo()["boundary"],approx);
  size_t d = omega.dimension();
  size_t k = Xh.degree();
  trial x(Yh); trial lambda(Mh);
  test  y(Yh); test  mu(Mh);
  integrate_option iopt;
  iopt.set_order(2*(k+1)+2);
  iopt.invert = true;
  form inv_a = integrate(dot(x[0],y[0]) + x[1]*div_h(y[0]) + y[1]*div_h(x[0])
  		- alpha*on_local_sides(x[1]*y[1]), iopt);
  iopt.invert = false;
  form b = integrate("internal_sides",
             (-dot(jump(x[0]),normal()) + 2*alpha*average(x[1]))*mu, iopt)
         + integrate("boundary", (-dot(x[0],normal()) + alpha*x[1])*mu, iopt);
  form c = integrate("internal_sides", 2*alpha*lambda*mu, iopt)
         + integrate("boundary",         alpha*lambda*mu, iopt);
  field lh = integrate (omega, -f(d)*y[1], iopt),
        kh(Mh,0); // TODO: integrate(-gn(d)*mu), // Dirichlet-Neumann
  lambda_h = field (Mh,0);
  lambda_h["boundary"] = lazy_interpolate(Wh,g(d));
  form s = c + b*inv_a*trans(b);
  field rh = b*(inv_a*lh) - kh;
  solver ss(s.uu());
  lambda_h.set_u() = ss.solve (rh.u() - s.ub()*lambda_h.b());
  field xh = inv_a*(lh - b.trans_mult(lambda_h));
  sigma_h = xh[0];
  uh      = xh[1];
}
void dirichlet_hdg2 (geo omega, string approx, Float alpha,
      field& sigma_h, field& uh, field& lambda_h)
{
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Mh (omega["sides"], approx);
  Mh.block("boundary");
  space Yh = Th*Xh*Mh;
  space Wh (Mh.get_geo()["boundary"],approx);
  size_t d = omega.dimension();
  size_t k = Xh.degree();
  integrate_option iopt;
  iopt.set_order(2*(k+1)+2);
  trial sigma(Th), u(Xh), lambda(Mh);
  test  tau  (Th), v(Xh), mu    (Mh);
warning_macro ("a...");
  form a_ss = integrate(dot(sigma,tau), iopt);
  form a_us = integrate(div_h(sigma)*v, iopt);
  form a_ls = - integrate("internal_sides", dot(jump(sigma),normal())*mu, iopt)
              - integrate("boundary", dot(sigma,normal())*mu, iopt);
  form a_uu = - alpha*integrate("internal_sides",
	          2*average(u)*average(v) + 0.5*jump(u)*jump(v), iopt)
              - alpha*integrate("boundary", u*v, iopt);
  form a_ll = - alpha*integrate("internal_sides", 2*lambda*mu, iopt)
              - alpha*integrate("boundary", lambda*mu, iopt);
  form a_lu =   alpha*integrate("internal_sides", 2*average(u)*mu, iopt)
              + alpha*integrate("boundary", u*mu, iopt);
warning_macro ("lh...");
  field lh_s (Th,0),
        lh_u = integrate (omega, -f(d)*v, iopt),
        lh_l (Mh,0);
warning_macro ("a bloc...");
  form a = { {a_ss, trans(a_us), trans(a_ls)},
             {a_us,       a_uu,  trans(a_lu)},
             {a_ls,       a_lu,        a_ll} };
warning_macro ("lh bloc...");
  field lh = {lh_s, lh_u, lh_l};
warning_macro ("solve...");
  field xh (Yh, 0);
  lambda_h = field(Mh, 0);
  lambda_h["boundary"] = lazy_interpolate (Wh,g(d));
  xh[2] = lambda_h;
  solver sa(a.uu());
  xh.set_u() = sa.solve (lh.u() - a.ub()*xh.b());
  sigma_h  = xh[0];
  uh       = xh[1];
  lambda_h = xh[2];
}
// ----------------------------------------
// 0e) error for hdg
// ----------------------------------------
void dirichlet_hdg_error (const field& sigma_h, const field& uh, const field& lambda_h)
{
  space Xh = uh.get_space();
  geo omega = Xh.get_geo();
  size_t k = Xh.degree();
  size_t d = omega.dimension();
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(4*k+1);
  Float err_u_l2 = sqrt(integrate (omega, sqr(uh-u_exact(d)), qopt));
  space Xh1 (omega, "P"+std::to_string(k+1)+"d");
  field euh = lazy_interpolate (Xh1, uh-u_exact(d));
  Float err_u_linf = euh.max_abs();
  Float err_u_h1 = sqrt(integrate (omega, norm2(grad_h(euh)), qopt)
                    + integrate (omega.sides(), (1/h_local())*sqr(jump(euh)), qopt));
  derr << "err_u_l2   = " << err_u_l2 << endl
       << "err_u_linf = " << err_u_linf << endl
       << "err_u_h1   = " << err_u_h1 << endl;
  Float meas_sides = integrate (lambda_h.get_geo());
  Float err_lambda_l2 = sqrt(integrate (lambda_h.get_geo(), sqr(lambda_h-u_exact(d)), qopt)/meas_sides);
  space Mh1 (omega["sides"], "P"+std::to_string(k+1)+"d");
  field elh = lazy_interpolate (Mh1, lambda_h-u_exact(d));
  Float err_lambda_linf = elh.max_abs();
  derr << "err_lambda_l2   = " << err_lambda_l2 << endl
       << "err_lambda_linf = " << err_lambda_linf << endl;
  Float err_sigma_l2 = sqrt(integrate (omega, norm2(sigma_h-grad_u(d)), qopt));
  space Th1 (omega, "P"+std::to_string(k+1)+"d", "vector");
  field esh = lazy_interpolate (Th1, sigma_h-grad_u(d));
  Float err_sigma_linf = esh.max_abs();
  derr << "err_sigma_l2   = " << err_sigma_l2 << endl
       << "err_sigma_linf = " << err_sigma_linf << endl;
}
// ----------------------------------------
// 1) hdg post 1: RTkd
// ----------------------------------------
void dirichlet_hdg_post1 (Float alpha,
  const field& sigma_h, const field& uh, const field& lambda_h,
  field& sigmat_h)
{
  const geo& omega = uh.get_geo();
  size_t d = omega.dimension();
  size_t k = uh.get_space().degree();
  space Tht (omega, "RT"+std::to_string(k)+"d");
  trial sigma_t(Tht);
  test  tau_t  (Tht);
  integrate_option iopt;
  iopt.invert = true;
  auto expr1 = dot(sigma_t, tau_t)
	     + on_local_sides(dot(sigma_t,normal())*dot(tau_t,normal()));
  form inv_aht = integrate(omega,expr1, iopt);
  // TODO: group uh-lambda_h : pb with linear expression between diff spaces
  auto expr1b = dot(sigma_h, tau_t)
	     + on_local_sides(
	           (dot(sigma_h,normal()) - alpha*uh)*dot(tau_t,normal())
	         + alpha*lambda_h*dot(tau_t,normal())
               );
  field lht = integrate(omega, expr1b);
  sigmat_h = inv_aht*lht;
}
// ----------------------------------------
// 1e) hdg post 1 error: RTkd
// ----------------------------------------
void dirichlet_hdg_post1_error (const field& sigmat_h)
{
  const space& Tht = sigmat_h.get_space();
  geo omega = Tht.get_geo();
  size_t k = Tht.degree()-1;
  size_t d = omega.dimension();
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(4*k+1);
  Float err_sigmat_l2 = sqrt(integrate (omega, norm2(sigmat_h-grad_u(d)), qopt));
  space Th1 (omega, "P"+std::to_string(k+1)+"d", "vector");
  field esht = lazy_interpolate (Th1, sigmat_h-grad_u(d));
  Float err_sigmat_linf = esht.max_abs();
  derr << "err_sigmat_l2   = " << err_sigmat_l2 << endl
       << "err_sigmat_linf = " << err_sigmat_linf << endl;
}
// ----------------------------------------
// 2) hdg post 2: P(k+1)d
// ----------------------------------------
void dirichlet_hdg_post2 (Float alpha,
  const field& sigma_h, const field& uh, const field& lambda_h, const field& sigmat_h,
  field& sigmas_h, field& ush, field& lambdas_h, field& zeta_hs)
{
  const geo& omega = uh.get_geo();
  size_t d = omega.dimension();
  size_t k = uh.get_space().degree();
  space Ths (omega, "P"+std::to_string(k+1)+"d", "vector"),
        Xhs (omega, "P"+std::to_string(k+1)+"d"),
        Mhs (omega, "P"+std::to_string(k+1)+"d[sides]"),
        Zhs (omega, "P0"),
        Yh = Ths*Xhs*Mhs*Zhs;
  trial x(Yh); test y(Yh);
  auto sigma_s = x[0], us = x[1], lambda_s = x[2], zeta_s = x[3];
  auto tau_s   = y[0], vs = y[1], mu_s     = y[2], xi_s   = y[3];
  auto expr2 = dot(sigma_s,tau_s)
         + div_h(tau_s)*us + div_h(sigma_s)*vs
	 + us*xi_s + vs*zeta_s
         - on_local_sides(  dot(tau_s,  normal())*lambda_s
                          + dot(sigma_s,normal())*mu_s
                          + alpha*(us-lambda_s)*(vs-mu_s))
	;
  auto expr2b = div_h(sigmat_h)*vs + uh*xi_s
               - on_local_sides(dot(sigmat_h,normal())*mu_s);
  integrate_option iopt;
  iopt.invert = true;
  iopt.set_family(quadrature_option::gauss);
  iopt.set_order(2*(k+1)+2);
  form inv_ahs = integrate(omega, expr2, iopt);
  field lhs = integrate (omega, expr2b, iopt);
  field xhs = inv_ahs*lhs;
  sigmas_h  = xhs[0];
  ush       = xhs[1];
  lambdas_h = xhs[2];
  zeta_hs   = xhs[3];
}
// ----------------------------------------
// 2e) hdg post 2 error: P(k+1)d
// ----------------------------------------
void dirichlet_hdg_post2_error (const field& sigmas_h, const field& ush, const field& lambdas_h)
{
  space Xhs = ush.get_space();
  geo omega = Xhs.get_geo();
  size_t d  = omega.dimension();
  size_t k1 = Xhs.degree();
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(4*k1+1);
  Float err_us_l2 = sqrt(integrate (omega, sqr(ush-u_exact(d)), qopt));
  space Xh1 (omega, "P"+std::to_string(k1+1)+"d");
  field eush = lazy_interpolate (Xh1, ush-u_exact(d));
  Float err_us_linf = eush.max_abs();
  Float err_us_h1 = sqrt(integrate (omega, norm2(grad_h(eush)), qopt)
                    + integrate (omega.sides(), (1/h_local())*sqr(jump(eush)), qopt));
  derr << "err_us_l2   = " << err_us_l2 << endl
       << "err_us_linf = " << err_us_linf << endl
       << "err_us_h1   = " << err_us_h1 << endl;
#ifdef TODO
  // fatal(../../include/rheolef/basis.h,353): basis "P2d[sides]": scalar-valued eval() member not implemented
  Float meas_sides = integrate (lambdas_h.get_geo());
  Float err_lambdas_l2 = sqrt(integrate (lambdas_h.get_geo(), sqr(lambdas_h-u_exact(d)), qopt)/meas_sides);
  space Mh1 (omega["sides"], "P"+std::to_string(k1+1)+"d");
  field elh = lazy_interpolate (Mh1, lambdas_h-u_exact(d));
  Float err_lambdas_linf = elh.max_abs();
  derr << "err_lambdas_l2   = " << err_lambdas_l2 << endl
       << "err_lambdas_linf = " << err_lambdas_linf << endl;
#endif // TODO
  Float err_sigmas_l2  = sqrt(integrate (omega, norm2(sigmas_h -grad_u(d)), qopt));
  space Th1 (omega, "P"+std::to_string(k1+1)+"d", "vector");
  field essh  = lazy_interpolate (Th1, sigmas_h -grad_u(d));
  Float err_sigmas_linf  = essh.max_abs();
  derr << "err_sigmas_l2   = " << err_sigmas_l2 << endl
       << "err_sigmas_linf = " << err_sigmas_linf << endl;
}
// ----------------------------------------
int main(int argc, char**argv) {
// ----------------------------------------
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ?      argv[2]  : "P1d";
  Float alpha   = (argc > 3) ? atof(argv[3]) : 1;
  bool dump     = (argc > 4);
  Float tol = 1e5*numeric_limits<Float>::epsilon();
  field sigma_h, uh, lambda_h;
  field sigma2_h, u2h, lambda2_h;
  dirichlet_hdg  (omega, approx, alpha, sigma_h, uh, lambda_h);
  dirichlet_hdg2 (omega, approx, alpha, sigma2_h, u2h, lambda2_h);
  Float err_meth = field(sigma_h-sigma2_h).max_abs()
                 + field(uh-u2h).max_abs()
                 + field(lambda_h-lambda2_h).max_abs();
  derr << "err_meth = " << err_meth << endl;
#ifdef TODO
  dirichlet_hdg_error                 (sigma_h, uh, lambda_h);
  field sigmat_h, sigmas_h, ush, lambdas_h, zeta_hs;
  dirichlet_hdg_post1 (alpha, sigma_h, uh, lambda_h, sigmat_h);
  dirichlet_hdg_post2 (alpha, sigma_h, uh, lambda_h, sigmat_h, sigmas_h, ush, lambdas_h, zeta_hs);
  dirichlet_hdg_post1_error (sigmat_h);
  dirichlet_hdg_post2_error (sigmas_h, ush, lambdas_h);
  if (dump) {
    dout << catchmark("alpha")  << alpha << endl
         << catchmark("u")      << uh
         << catchmark("lambda") << lambda_h
         << catchmark("sigma")  << sigma_h
         << catchmark("sigmat") << sigmat_h
         << catchmark("us")     << ush
         << catchmark("lambdas")<< lambdas_h
         << catchmark("sigmas") << sigmas_h
         << catchmark("zeta")   << zeta_hs;
  }
#endif // TODO
  return (err_meth < tol) ? 0 : 1;
}
