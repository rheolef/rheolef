///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute exactly:
//  \int_\Omega weight(x) dx = mes(\Omega) with weight
//
// where Omega is a Pk transformed multi-element element.
// See also form_mass_curved_tst.cc : when the mesh contains only one element.
//
#include "rheolef.h"
#include "rheolef/rheostream.h"
#include "disto_Pk.h"
#include "disto.h"
using namespace rheolef;

void usage()
{
      derr << "usage: form_mass_disto_tst"
	   << " -|mesh[.geo]"
	   << " {-Igeodir}*"
 	   << " [-tol float]"
	   << std::endl
           << "example:" << std::endl
           << "  mkgeo_ugrid -t 10 -order 3 > square_P3_t.geo" << std::endl
           << "  form_mass_disto_tst square_P3_t" << std::endl
           << "  form_mass_disto_tst square_P3_t -put > disto_P3_t.geo" << std::endl;
      exit (1);
}
struct weight {
  Float operator() (const point& x) const {
    if (nx == 0 && ny == 0) return 1;
    //if (nx == 1 && ny == 1) return x[0]*x[1];
    return pow(x[0],Float(nx)) * pow(x[1],Float(ny));
  }
  weight(size_t nx1=0, size_t ny1=0) : nx(nx1), ny(ny1) {}
  size_t nx, ny;
};
geo transform (const geo& omega_in)
{
  size_t order = omega_in.order();
  disto_t<Float> F(order);
  disarray<point> node = omega_in.get_nodes();
  for (size_t i = 0; i < node.size(); i++) {
    node[i] = F (node[i]);
  }
  geo omega = omega_in;
  omega.set_nodes (node);
  return omega;
}
int
check_one (const geo& omega, const Float& tol, size_t nx, size_t ny, size_t mx, size_t my)
{
  size_t order = omega.order();
  std::string approx = "P" + std::to_string(order);
  warning_macro ("approx = " << approx);
  space V(omega, approx);
  form  m(V,V,"mass");
  field w1h = lazy_interpolate (V, weight(nx,ny));
  field w2h = lazy_interpolate (V, weight(mx,my));
  Float mes_omega = m(w1h, w2h);
  Float expect = disto_Pk [order][nx+mx][ny+my];
  dout << std::setprecision(std::numeric_limits<Float>::digits10);
  std::string msg = "ok";
  if (fabs(mes_omega - expect) > tol) { msg = "NO"; }
  dout << msg << ": meas("<<omega.name()<<",x^" << nx << "y^" << ny << ",x^"<<mx<<"y^"<<my<<") = " << mes_omega << std::endl;
  int status = 0;
  if (fabs(mes_omega - expect) > tol) {
    status = 1;
    dout << "NO: expected  = " << expect << std::endl
         << "NO: error     = " << fabs(mes_omega - expect) << std::endl;
  }
  return status;
}
int
check_all (const geo& omega, const Float& tol)
{
  size_t order = omega.order();
  std::string approx = "P" + std::to_string(order);
  warning_macro ("approx = " << approx);
  space V(omega, approx);
  form  m(V,V,"mass");
  derr << std::setprecision(std::numeric_limits<Float>::digits10);
  int status = 0;
  for (size_t nx = 0; nx    <= order; nx++) {
  for (size_t ny = 0; nx+ny <= order; ny++) {
    if (nx + ny > 1) continue; // cannot exactly interpolate weight
    field w1h = lazy_interpolate (V, weight(nx,ny));
    for (size_t mx = 0; mx    <= order; mx++) {
    for (size_t my = 0; mx+my <= order; my++) {
      if (mx + my > 1) continue; // cannot exactly interpolate weight
      field w2h = lazy_interpolate (V, weight(mx,my));
      Float mes_omega = m(w1h, w2h);
      Float expect = disto_Pk [order][nx+mx][ny+my];
      std::string msg = "ok";
      if (fabs(mes_omega - expect) > tol) { msg = "NO"; }
      derr << msg << ": meas("<<omega.name()<<",x^" << nx << "y^" << ny << ",x^"<<mx<<"y^"<<my<<") = " << mes_omega << std::endl;
      if (fabs(mes_omega - expect) > tol) {
        status = 1;
        derr << "NO: expected  = " << expect << std::endl
	     << "NO: error     = " << fabs(mes_omega - expect) << std::endl;
      }
    }}
  }}
  return status;
}
int main(int argc, char**argv)
{
    //
    // load geometry and options
    //
    environment rheolef (argc,argv);
    geo omega;  
    bool mesh_done = false;
    bool put = false;
    bool do_check_all = false;
    Float tol = 1e-10;
    size_t nx=0, ny=0, mx=0, my=0;

    if (argc <= 1) usage() ;

    for (int i = 1; i < argc; i++ ) {

      if (argv [i][0] == '-' && argv [i][1] == 'I') { append_dir_to_rheo_path (argv[i]+2) ; }
      else if (strcmp(argv[i], "-tol") == 0) { tol = atof(argv[++i]); }
      else if (strcmp(argv[i], "-put") == 0) { put = true; }
      else if (strcmp(argv[i], "-check-all") == 0) { do_check_all = true; }
      else if (strcmp(argv[i], "-nx") == 0) { nx = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-ny") == 0) { ny = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-mx") == 0) { mx = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-my") == 0) { my = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-") == 0) {
	  // input geo on standard input
	  if (mesh_done) usage() ;
	  derr << "! load geo on stdin" << std::endl ;
	  din >> omega ;
	  mesh_done = true ;
      } else {
	  // input geo on file
	  if (mesh_done) usage() ;
	  //derr << "! load " << argv[i] << std::endl ;
	  omega = geo(argv[i]);
	  mesh_done = true ;
      }
    }
    if (!mesh_done) usage();
    omega = transform (omega);
    if (put) { dout << omega; return 0; }
    int status = 0;
    if (do_check_all) status = check_all (omega, tol);
    else              status = check_one (omega, tol, nx, ny, mx, my);
    return status;
}
