///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute:
//  \int_\Omega weight(x) dx = mes(\Omega) with weight
//
// check value on the unit cube [0,1]^d
//
#include "rheolef/rheolef.h"
#include "put_mm.h"
using namespace rheolef;
using namespace std;

typedef Float (*function_type)(const point&);

struct list_type {
	string name;
	function_type function;
	Float  expect;
};
Float weight_1   (const point& x) { return 1; }		
Float weight_x   (const point& x) { return x[0]; }
Float weight_y   (const point& x) { return x[1]; }	
Float weight_z   (const point& x) { return x[2]; }

Float weight_x2  (const point& x) { return x[0]*x[0]; }
Float weight_xy  (const point& x) { return x[0]*x[1]; }
Float weight_y2  (const point& x) { return x[1]*x[1]; }
Float weight_xz  (const point& x) { return x[0]*x[2]; }
Float weight_yz  (const point& x) { return x[1]*x[2]; }
Float weight_z2  (const point& x) { return x[2]*x[2]; }

Float weight_x3  (const point& x) { return x[0]*x[0]*x[0]; }	
Float weight_y3  (const point& x) { return x[1]*x[1]*x[1]; }
Float weight_z3  (const point& x) { return x[2]*x[2]*x[2]; }
Float weight_x2y (const point& x) { return x[0]*x[0]*x[1]; }
Float weight_xy2 (const point& x) { return x[0]*x[1]*x[1]; }
Float weight_x2z (const point& x) { return x[0]*x[0]*x[2]; }
Float weight_xz2 (const point& x) { return x[0]*x[2]*x[2]; }
Float weight_y2z (const point& x) { return x[1]*x[1]*x[2]; }
Float weight_yz2 (const point& x) { return x[1]*x[2]*x[2]; }
Float weight_xyz (const point& x) { return x[0]*x[1]*x[2]; }

// cartesian

list_type weight_list[] = {
	{"1",	weight_1, 	1.},
	{"x",	weight_x,	0.5},
	{"y",	weight_y,	0.5},
	{"z",	weight_z,	0.5},

	{"x2",	weight_x2,	Float(1)/3},
	{"y2",	weight_y2,	Float(1)/3},
	{"z2",	weight_z2,	Float(1)/3},
	{"xy",	weight_xy,	0.25},
	{"xz",	weight_xz,	0.25},
	{"yz",	weight_yz,	0.25},

	{"x3",	weight_x3,	0.25},
	{"y3",	weight_y3,	0.25},
	{"z3",	weight_y3,	0.25},
	{"x2y",	weight_x2y,	Float(1)/6},
	{"xy2",	weight_xy2,	Float(1)/6},
	{"x2z",	weight_x2z,	Float(1)/6},
	{"xz2",	weight_xz2,	Float(1)/6},
	{"y2z",	weight_y2z,	Float(1)/6},
	{"yz2",	weight_yz2,	Float(1)/6},
	{"xyz",	weight_xyz,	0.125}
};

// axisymmetric rz

list_type rz_weight_list[] = {
	{"1",	weight_1, 	0.5},
	{"x",	weight_x,	Float(1)/3},
	{"y",	weight_y,	0.25},

	{"x2",	weight_x2,	0.25},
	{"xy",	weight_xy,	Float(1)/6},
	{"y2",	weight_y2,	Float(1)/6}
};
const unsigned int    weight_max =    sizeof(weight_list)/sizeof(list_type);
const unsigned int rz_weight_max = sizeof(rz_weight_list)/sizeof(list_type);

void usage()
{
      derr << "form_mass_tst: usage: form_mass_tst"
 	   << " -app approx"
 	   << " [-tol float]"
	   << " [-cartesian|-rz]"
	   << " {-Igeodir}*"
	   << " -|mesh[.geo]"
	   << " [-weight string]"
	   << " [-proj approx]"
	   << endl
           << "example:\n"
           << "  form_mass_tst -app P1 -I../data carre\n";
      exit (1);
}
int main(int argc, char**argv)
{
    //
    // load geometry and options
    //
    environment rheolef (argc,argv);
    geo omega;  
    string approx1 = "P1";
    string approx2 = "";
    string weight_id = "";
    bool mesh_done = false;
    function_type weight_function = 0;
    unsigned int weight_idx = 0;
    string coord_sys = "cartesian";
    Float tol = 1e-10;

    if (argc <= 1) usage() ;

    for (int i = 1; i < argc; i++ ) {

      if (argv [i][0] == '-' && argv [i][1] == 'I')
	  append_dir_to_rheo_path (argv[i]+2) ;
      else if (strcmp(argv[i], "-app") == 0)
	  approx1 = argv[++i];
      else if (strcmp(argv[i], "-proj") == 0)
	  approx2 = argv[++i];
      else if (strcmp(argv[i], "-tol") == 0)
	  tol = atof(argv[++i]);
      else if (strcmp(argv[i], "-cartesian") == 0)
	  coord_sys = "cartesian";
      else if (strcmp(argv[i], "-rz") == 0)
	  coord_sys = "rz";
      else if (strcmp(argv[i], "-weight") == 0) {
	  weight_id = argv[++i];
      } else if (strcmp(argv[i], "-") == 0) {
	  // input geo on standard input
	  if (mesh_done) usage() ;
	  derr << " ! load geo on stdin" << endl ;
	  din >> omega ;
	  omega.set_coordinate_system (coord_sys);
	  mesh_done = true ;
      } else {
	  // input geo on file
	  if (mesh_done) usage() ;
	  derr << " ! load " << argv[i] << endl ;
	  omega = geo(argv[i]);
	  omega.set_coordinate_system (coord_sys);
	  mesh_done = true ;
      }
    }
    if (!mesh_done) usage() ;
    switch (omega.coordinate_system()) {
      case space_constant::cartesian: {
        for (size_t i = 0; i < weight_max; i++) {
          if (weight_id == weight_list [i].name) {
	     weight_function = weight_list [i].function;
	     weight_idx = i;
          }
        }
        break;
      }
      case space_constant::axisymmetric_rz: {
        for (size_t i = 0; i < rz_weight_max; i++) {
          if (weight_id == rz_weight_list [i].name) {
	     weight_function = rz_weight_list [i].function;
	     weight_idx = i;
          }
        }
        break;
      }
      default: error_macro ("unexpected coordinate system");
    }
    if (!weight_function) {
	error_macro("invalid weight identifier: " << weight_id);
    }
    if (approx2 == "") {
	approx2 = approx1;
    }
    space V1(omega, approx1);
    space V2(omega, approx2);
    derr << "weight = " << weight_id << endl;
    field weight = lazy_interpolate (V1, weight_function);
    field one(V2,1);
    form  m(V1,V2,"mass");
    Float mes_omega = m(weight, one);
    derr << setprecision(numeric_limits<Float>::digits10)
          << "mes(omega," << approx1 << "," << approx2 << ") = " << mes_omega << endl;
    Float expect = 0;
    switch (omega.coordinate_system()) {
      case space_constant::cartesian:       expect =    weight_list[weight_idx].expect; break;
      case space_constant::axisymmetric_rz: expect = rz_weight_list[weight_idx].expect; break;
      default: error_macro ("unexpected coordinate system");
    }
    if (fabs(mes_omega - expect) <= tol) {
        derr << "ok" << endl;
        return 0;
    } else {
        derr << "but it was expected " << expect << endl
	      << "and error = " << fabs(mes_omega - expect) << endl
	      << "and tol = " << tol << endl;
        return 1;
    }
}
