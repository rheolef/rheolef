#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
DATADIR=${TOP_SRCDIR}/main/tst
BINDIR="../../main/bin"
SBINDIR="../../main/sbin"
NPROC_MAX=${NPROC_MAX-"7"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0

# geo         err_h1
L="
carre-20
carre-41
cube-20
"

run "$SBINDIR/mkgeo_grid_2d -v4 -t 20 -a -2 -b 2 -c -2 -d 2 2>/dev/null | $BINDIR/geo -upgrade - > carre-20.geo 2>/dev/null"
run "$SBINDIR/mkgeo_grid_2d -v4 -t 41 -a -2 -b 2 -c -2 -d 2 2>/dev/null | $BINDIR/geo -upgrade - > carre-41.geo 2>/dev/null"
run "$SBINDIR/mkgeo_grid_3d -v4 -T 20 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2  2>/dev/null| $BINDIR/geo -upgrade - > cube-20.geo 2>/dev/null"
#run "$SBINDIR/mkgeo_grid_3d -v4 -T 21 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2  2>/dev/null| $BINDIR/geo -upgrade - > mesh-3d-21.geo 2>/dev/null"

for geo in $L; do
  loop_mpirun "./band_cc_tst $geo 2>/dev/null | diff -Bw $SRCDIR/${geo}.band.valid.geo - >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done

run "rm -f carre-20.geo carre-41.geo cube-20.geo"

exit $status
