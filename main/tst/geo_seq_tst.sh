#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
DATA_DIR=${SRCDIR}
NPROC_MAX=${NPROC_MAX-"7"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0

L="
zero-5.geo
line-1-bdry.geo
line-2-bdry.geo
line-3-bdry.geo
line-20-bdry.geo
carre-v1.geo
carre-v2.geo
line-dom-v2.geo
carre-dom-v2.geo
cube-dom-v2.geo
cube-H-6-dom-v2.geo
circle_s_p1-v2.geo
circle_s_p2-v2.geo
circle_p1-v2.geo
circle_p2-v2.geo
circle_q_p1-v2.geo
circle_q_p2-v2.geo
sphere_s_p1-v2.geo
sphere_s_p2-v2.geo
sphere_s_q_p1-v2.geo
sphere_s_q_p2-v2.geo
sphere_p1-v2.geo
sphere_p2-v2.geo
sphere_H_p1-v2.geo
sphere_H_p2-v2.geo
cube-P-5-dom-v2.geo
carre-tq-10-dom-v2.geo
"

for geo in $L; do
  run "./geo_seq_tst ${SRCDIR}/$geo 2>/dev/null | diff -B -w ${SRCDIR}/$geo - >/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
