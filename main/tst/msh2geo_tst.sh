#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
BINDIR="../bin"
SBINDIR="../sbin"
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

export TMPDIR="."

#echo "      not yet (skiped)"
#exit 0

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0

L_1D="
edge_s_2d_p9
edge_s_3d_p9
circle_s_p1
"

# msh2geo -noupgrade < circle_s_p2.msh | geo -
#   fatal{0}(../../include/rheolef/geo_element_v4.h,491): cannot change variant from 1 to 0 in a raw element
L_1D_BUG="
circle_s_p2
"

L_2D="
triangle_p1
triangle_p2
triangle_p3
triangle_p4
triangle_p5
triangle_p9
triangle_s_p9
quadrangle_p1
quadrangle_p2
quadrangle_p9
quadrangle_s_p9
circle_p1
circle_p2
circle_p5
circle_q_p1
circle_q_p2
sphere_s_p1
sphere_s_p2
sphere_s_p10
sphere_s_q_p1
sphere_s_q_p2
sphere_s_q_p10
"

L_3D="
tetra_p1
tetra_p2
tetra_p3
tetra_p4
tetra_p5
tetra_p6
tetra_p7
tetra_p8
tetra_p9
tetra2_p9
prism_p1
hexa_p1
hexa_p1
sphere_p1
sphere_p2
sphere_p3
sphere_p9
sphere_H_p1
sphere_H_p2
"

# gmsh change its gnerated .msh when changing gmsh version (TODO: upgrade all my_cube*.field files)
L_UPGRADE_GMSH="
my_cube_TP-5
my_cube_PH-5
my_cube_TPH-5
"
# TODO: prism pk
L_TODO="
prism_p2
"

L="
$L_1D
$L_2D
$L_3D
"
tol="1e-7"
for geo in $L; do
  for opt in noupgrade upgrade; do
    if test $opt = noupgrade; then v=v1; else v=v2; fi
    run "${SBINDIR}/msh2geo -$opt < ${SRCDIR}/${geo}.msh 2>/dev/null | $BINDIR/geo -round $tol -geo - > tmp.geo 2>/dev/null && $BINDIR/geo -round $tol -geo - < ${SRCDIR}/${geo}-$v.geo 2>/dev/null | diff -B -w tmp.geo - >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
run "rm -f tmp.geo"

exit $status
