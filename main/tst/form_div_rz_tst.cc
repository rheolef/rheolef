///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute:
//  \int_\Omega div(u) r dr dz
//
// check value on the unit square [0,1]^2
//
//           d u_r   u_r   d u_z
//  div(u) = ----- + --- + -----
//            d r     r     d z
//
// component 0 : we check the u_r component : u_z = 0
//               and u_r takes monomial values : 1, r, ...
// component 1 : u_r = 0 and u_z is monomial
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;

typedef Float (*function_type)(const point&);

struct list_type {
	string        name;
	function_type function;
	Float  expect [2];
};
Float monom_1  (const point& x) { return 1; }
Float monom_r  (const point& x) { return x[0]; }
Float monom_z  (const point& x) { return x[1]; }
Float monom_r2 (const point& x) { return x[0]*x[0]; }
Float monom_rz (const point& x) { return x[0]*x[1]; }
Float monom_z2 (const point& x) { return x[1]*x[1]; }

// axisymmetric rz

list_type rz_monom_list [] = {
	//   monom		      component
	// id   fct             u_r test     u_z test
	{"1",	monom_1, 	{1,		0} 		},
	{"r",	monom_r,	{1,		0}		},
	{"z",	monom_z,	{0.5,		0.5}		},
	{"r2",	monom_r2,	{1.,		0}		},
	{"rz",	monom_rz,	{0.5,		Float(1)/3}	},
	{"z2",	monom_z2,	{Float(1)/3,	0.5}		}
};
const unsigned int rz_monom_max = sizeof(rz_monom_list)/sizeof(list_type);

void usage()
{
      derr << "form_div_tst: usage: form_div_tst"
	   << " {-Igeodir}*"
	   << " -|mesh[.geo]"
	   << " [-component int]"
	   << " [-monom string]"
	   << endl
           << "example:\n"
           << "  form_div_tst square -monom rz -component 0\n";
      exit (1);
}
int main(int argc, char**argv)
{
    environment rheolef (argc, argv);
    //
    // load geometry and options
    //
    geo omega;  
    string approx1 = "P2";
    string approx2 = "P1";
    string monom_id = "";
    size_t i_comp = 0;
    bool mesh_done = false;
    function_type monom_function = 0;
    unsigned int monom_idx = 0;
    string sys_coord_name = "rz";

    if (argc <= 1) usage() ;

    for (int i = 1; i < argc; i++ ) {

      if (argv [i][0] == '-' && argv [i][1] == 'I')
	  append_dir_to_rheo_path (argv[i]+2) ;
      else if (strcmp(argv[i], "-component") == 0)
	  i_comp = atoi(argv[++i]);
      else if (strcmp(argv[i], "-monom") == 0) {
	  monom_id = argv[++i];
          for (unsigned int i = 0; i < rz_monom_max; i++) {
            if (monom_id == rz_monom_list [i].name) {
	      monom_function = rz_monom_list [i].function;
	      monom_idx = i;
            }
          }
      } else if (strcmp(argv[i], "-") == 0) {
	  // input geo on standard input
	  if (mesh_done) usage() ;
	  derr << " ! load geo on stdin" << endl ;
	  din >> omega ;
	  mesh_done = true ;
      } else {
	  // input geo on file
	  if (mesh_done) usage() ;
	  derr << " ! load " << argv[i] << endl ;
	  omega = geo(argv[i]);
	  mesh_done = true ;
      }
    }
    if (!mesh_done) usage() ;
    omega.set_coordinate_system (sys_coord_name);
    if (!monom_function) {
	error_macro("invalid monom identifier: " << monom_id);
    }
    derr << "syscoord = " << omega.coordinate_system() << endl
         << "monom = " << monom_id << endl;
    space V0h(omega, approx1);
    space Vh (omega, approx1, "vector");
    space Qh(omega, approx2);
    field monom (Vh, Float(0));
    monom[i_comp] = lazy_interpolate (V0h, monom_function);
    field one(Qh, Float(1));
    form  b (Vh,Qh,"div");
    Float result = b(monom, one);
    string vec_monom_id;
    if (i_comp == 0) vec_monom_id = monom_id + ",0";
    else             vec_monom_id = "0," + monom_id;
    derr << setprecision(numeric_limits<Float>::digits10)
         << "b(omega," << approx1 << "[" << vec_monom_id << "]," << approx2 << "[1]) = " << result << endl;
    Float expect = rz_monom_list[monom_idx].expect[i_comp];
    Float tol = 1e-10;

    if (fabs(result - expect) <= tol) {
        derr << "ok" << endl;
        return 0;
    } else {
        derr << "but it was expected " << expect << endl
	     << "and error = " << fabs(result - expect) << endl
	     << "and tol = " << tol << endl;
        return 1;
    }
}
