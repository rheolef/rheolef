#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
if test $# -eq 0; then
  echo "$0: usage: $0 order a b"
  echo "example: $0 3 3 0"
  exit 1
fi
order=${1-"2"}
a=${2-"0"}
b=${3-"0"}

tmp="tmp.mac"
log="tmp.log"

cat > $tmp << EOF_1
a:$a;
b:$b;
k:${order};
/* P${order} piola triangle transformation here */
y_bas : if (k=1) then 0 else x*(1-x)**(k-1);
y_haut: y_bas + (1-x);
J_bas : integrate(y**b,y,0,y_bas);
J_haut: integrate(y**b,y,0,y_haut);
I_bas : integrate(x**a * J_bas, x,0,1);
I_haut: integrate(x**a * J_haut,x,0,1);
load(f90);
res;
f90 ('exact = I_haut-I_bas);
print ("value=", bfloat(I_haut-I_bas));
EOF_1

maxima < $tmp > $log
#cat $log
exact=`grep exact $log | sed -e 's/.*exact[ ]*=[ ]*//'`
value=`grep value $log | sed -e 's/.*value=[ ]*//' -e 's/b/e/'`
echo "integral(a=$a,b=$b) = $exact = $value"
