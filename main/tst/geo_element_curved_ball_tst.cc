///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
#include "rheolef/geo_element_curved_ball.h"
using namespace rheolef;
void put (const geo_basic<Float,sequential>& omega, size_t order, bool check_tetra_with_curved_edge) {
  typedef geo_basic<Float,sequential>::size_type size_type;
  typedef point_basic<size_type>                 ilat;
  size_type d = omega.dimension();
  check_macro (d >= 2, "unexpected dimension "<<d);
  point center(0,0);
  Float radius = 1;
  bool have_quad = false;
  disarray<point,sequential> node = omega.get_nodes();
  std::cout << std::setprecision(7)
            << "set xrange ["<<omega.xmin()[0]<<":"<<omega.xmax()[0]<<"]" << std::endl
            << "set yrange ["<<omega.xmin()[1]<<":"<<omega.xmax()[1]<<"]" << std::endl
            ;
  if (d == 2) {
    std::cout << "set size square" << std::endl;
  } else {
    std::cout << "set zrange ["<<omega.xmin()[2]<<":"<<omega.xmax()[2]<<"]" << std::endl
         << "set xyplane at -0.1" << std::endl
         << "set view equal xyz # equal scales" << std::endl
         ;
  }
  for (size_type ie = 0, ne = omega.size(); ie < ne; ie++) {
    const geo_element& K = omega.get_geo_element(d, ie);
    switch (K.variant()) {
      case reference_element::t: {
	center = point(-1,-1); radius = sqrt(Float(5));
        curved_ball_t<Float> F (node[K[0]], node[K[1]], node[K[2]], 1, center, radius);
        for (size_type i = 1; i < order; i++) {
          for (size_type j = 1; i+j < order; j++) {
            size_type loc_inod = reference_element_t::ilat2loc_inod (order, ilat(i, j));
            point hat_x (Float(1)*i/order, Float(1)*j/order);
            point x = F (hat_x);
            std::cout << "set label \"(" << i << "," << j << ")\" at " << x[0] << "," << x[1] << std::endl;
          }
        }
        break;
      }
      case reference_element::q: {
	have_quad = true;
        curved_ball_q<Float> F (node[K[1]], node[K[2]], node[K[3]], node[K[0]]);
        for (size_type i = 1; i < order; i++) {
          for (size_type j = 1; j < order; j++) {
            size_type loc_inod = reference_element_q::ilat2loc_inod (order, ilat(i, j));
            point hat_x (-1+2.*i/order, -1+2.*j/order);
            point x = F (hat_x);
            std::cout << "set label \"(" << i << "," << j << ")\" at " << x[0] << "," << x[1] << std::endl;
          }
        }
        break;
      }
      case reference_element::T: {
	//center = point(-1,-1,-1); radius = sqrt(Float(6));
        curved_ball_T<Float> F (node[K[0]], node[K[1]], node[K[2]], node[K[3]], center, radius);
        if (!check_tetra_with_curved_edge) {
          F.set_boundary_face (3);
        } else {
          F.set_boundary_edge (1);
	}
        for (size_type i = 1; i < order; i++) {
          for (size_type j = 1; i+j < order; j++) {
            for (size_type k = 1; i+j+k < order; k++) {
              size_type loc_inod = reference_element_T::ilat2loc_inod (order, ilat(i, j, k));
              point hat_x (1.*i/order, 1.*j/order, 1.*k/order);
              point x = F (hat_x);
              std::cout << "set label \"(" << i << "," << j << "," << k << ")\" at "
		   << x[0] << "," << x[1] << "," << x[2] << std::endl;
            }
          }
        }
        break;
      }
      default:
        error_macro ("unexpected variant "<<K.variant());
    }
  }
  std::cout << "r  = " << radius << std::endl
       << "cx = " << center[0] << std::endl
       << "cy = " << center[1] << std::endl
      ;
  if (d == 3) {
    std::cout << "cz = " << center[2] << std::endl;
  }
  if (d == 2) {
    if (have_quad) {
      std::cout << "bdry(x) = (x > 1./sqrt(2.)) ? (cy + sqrt(r**2-(x-cx)**2)) : (1 + ((1. - sqrt(2.))*x))" << std::endl;
    } else {
      std::cout << "bdry(x) = cy + sqrt(r**2-(x-cx)**2)" << std::endl;
    }
    std::cout << "plot bdry(x)" << std::endl;
  } else { // d=3
    std::cout << "bdry(x,y) = cz + sqrt(r**2-(x-cx)**2-(y-cy)**2)" << std::endl;
    std::cout << "splot bdry(x,y)" << std::endl;
  }
  std::cout << "pause -1 \"<retour>\"" << std::endl;
}
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo_basic<Float,sequential> omega (argv[1]);
  size_t order = (argc > 2) ? atoi(argv[2]) : omega.order();
  bool check_tetra_with_curved_edge = (argc > 3);
  put (omega, order, check_tetra_with_curved_edge);
}
