#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skiped)"
#exit 0

status=0

# geo           	approx  err_l2 err_div_l2
L="
triangle_p1-v2		RT0d	1.1    4.2
triangle_p1-v2		RT1d	1.0    4.7
triangle_reverse_p1-v2	RT0d	1.1    4.6
triangle_reverse_p1-v2	RT1d	1.0    4.3
triangle_rotate_p1-v2	RT1d	1.0    4.7
quadrangle_p1-v2	RT0d	1.6    5.4
quadrangle_p1-v2	RT1d	1.1    5.3
carre-10-bdry-v2	RT0d	3e-1   0.8
carre-10-bdry-v2	RT1d	4e-2   2.0
carre-bamg-v2		RT0d    2e-1   0.8
carre-bamg-v2		RT1d    3e-2   1.8
carre-q-10-dom-v2	RT0d	1.5e-1 1.0
carre-q-10-dom-v2	RT1d	1.6e-2 0.9
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  approx=`echo $L | gawk '{print $2}'`
  err_l2=`echo $L | gawk '{print $3}'`
  err_div_l2=`echo $L | gawk '{print $4}'`
  L=`echo $L | gawk '{for (i=5; i <= NF; i++) print $i}'`
  loop_mpirun "./interpolate_hdiv_tst ${SRCDIR}/$geo $approx $err_l2 $err_div_l2 2>/dev/null >/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
