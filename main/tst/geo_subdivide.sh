#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR="${TOP_SRCDIR}/main/tst"
DATA_DIR=${SRCDIR}
NPROC_MAX=${NPROC_MAX-"6"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0

L="
triangle_p1-v2
triangle2_p1-v2
circle_p2-v2
"

for geo in $L; do
  run "../bin/geo -geo -subdivide 2 -round - < ${SRCDIR}/$geo.geo 2>/dev/null | diff -B -w ${SRCDIR}/${geo}-nsub-2.geo - >/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
