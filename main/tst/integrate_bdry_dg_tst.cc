///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// integration of discontinuous approximations on boundary domain
// bug from Veronique Dansereau (feb. 2014)
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  Float tol = 1e-7;
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(1);
  field uh (Xh,1.0);
  Float length = integrate (omega["top"], abs(uh),qopt);
  dout << "length = " << length << endl;
  test v (Xh);
  field lh = integrate ("top", v);
  Float length2 = dual(lh, uh);
  dout << "length2 = " << length2 << endl;
  Float err = fabs(length - 1) + fabs(length2 - 1);
  return err < tol ? 0 : 1;
}
