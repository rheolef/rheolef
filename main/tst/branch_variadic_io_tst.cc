///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string mode = argv[2];
  space Xh (omega, "P1");
  branch ev("t","u","v","w","z");
  if (mode == "io") {
    Float t;
    field uh, vh, wh, zh;
    din  >> ev(t,uh,vh,wh,zh);
    dout << ev(t,uh,vh,wh,zh);
  } else {
    Float t = 3.14;
    field uh (Xh,0), vh(Xh,1), wh(Xh,2), zh(Xh,3);
    dout << ev(t,uh,vh,wh,zh);
  }
}
