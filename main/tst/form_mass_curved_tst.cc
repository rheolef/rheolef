///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute exactly:
//  \int_\Omega weight(x) dx = mes(\Omega) with weight
//
// where Omega is a Pk transformed simple element
// the mesh contains only one element.
//
#include "rheolef.h"
#include "rheolef/rheostream.h"
#include "curved_Pk.h"
#include "put_mm.h"
using namespace rheolef;
using namespace std;

void usage()
{
      derr << "usage: form_mass_curved_tst"
	   << " -|mesh[.geo]"
	   << " {-Igeodir}*"
 	   << " [-tol float]"
	   << endl
           << "example:" << endl
           << "  form_mass_curved_tst curved_P3_t" << endl;
      exit (1);
}
struct weight {
  Float operator() (const point& x) const {
    if (nx == 0 && ny == 0) return 1;
    //if (nx == 1 && ny == 1) return x[0]*x[1];
    return pow(x[0],Float(nx)) * pow(x[1],Float(ny));
  }
  weight(size_t nx1=0, size_t ny1=0) : nx(nx1), ny(ny1) {}
  size_t nx, ny;
};
int
check_curved (const geo& omega, const Float& tol)
{
  size_t order = omega.order();
  string approx = "P" + std::to_string(order);
  warning_macro ("approx = " << approx);
  space V(omega, approx);
  form  m(V,V,"mass");
#ifdef TO_CLEAN
  odiststream mm; mm.open("mass_"+omega.name(),"mm");
  put_mm (mm, m.uu());
  mm.close();
#endif // TO_CLEAN
  dout << setprecision(numeric_limits<Float>::digits10);
  int status = 0;
  for (size_t nx = 0; nx    <= order; nx++) {
  for (size_t ny = 0; nx+ny <= order; ny++) {
    if (nx + order*ny > order) continue; // cannot exactly interpolate weight
    field w1h = lazy_interpolate (V, weight(nx,ny));
    for (size_t mx = 0; mx    <= order; mx++) {
    for (size_t my = 0; mx+my <= order; my++) {
      if (mx + order*my > order) continue; // cannot exactly interpolate weight
      field w2h = lazy_interpolate (V, weight(mx,my));
      Float mes_omega = m(w1h, w2h);
      Float expect = curved_Pk [order][nx+mx][ny+my];
      string msg = "ok";
      if (fabs(mes_omega - expect) > tol) { msg = "NO"; }
      dout << msg << ": meas("<<omega.name()<<",x^" << nx << "y^" << ny << ",x^"<<mx<<"y^"<<my<<") = " << mes_omega << endl;
      if (fabs(mes_omega - expect) > tol) {
        status = 1;
        dout << "NO: expected  = " << expect << endl
	     << "NO: error     = " << fabs(mes_omega - expect) << endl;
      }
    }}
  }}
  return status;
}
int main(int argc, char**argv)
{
    //
    // load geometry and options
    //
    environment rheolef (argc,argv);
    geo omega;  
    size_t nx = 0;
    size_t ny = 0;
    size_t mx = 0;
    size_t my = 0;
    bool mesh_done = false;
    Float tol = 1e-10;

    if (argc <= 1) usage() ;

    for (int i = 1; i < argc; i++ ) {

      if (argv [i][0] == '-' && argv [i][1] == 'I') { append_dir_to_rheo_path (argv[i]+2) ; }
      else if (strcmp(argv[i], "-tol") == 0) { tol = atof(argv[++i]); }
      else if (strcmp(argv[i], "-") == 0) {
	  // input geo on standard input
	  if (mesh_done) usage() ;
	  derr << "! load geo on stdin" << endl ;
	  din >> omega ;
	  mesh_done = true ;
      } else {
	  // input geo on file
	  if (mesh_done) usage() ;
	  //derr << "! load " << argv[i] << endl ;
	  omega = geo(argv[i]);
	  mesh_done = true ;
      }
    }
    if (!mesh_done) usage();
    return check_curved (omega, tol);
}
