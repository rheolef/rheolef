#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
DATADIR=${SRCDIR}
SBIN="../sbin"
BIN="../bin"
NPROC_MAX=${NPROC_MAX-"7"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

# --------------------
# create tmp meshes
# --------------------
run "$SBIN/mkgeo_grid_2d -v4 -t 11  2>/dev/null | $BIN/geo -upgrade - > mesh-2d-11.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
run "$SBIN/mkgeo_grid_2d -v4 -t 20  2>/dev/null | $BIN/geo -upgrade - > mesh-2d-20.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
run "$SBIN/mkgeo_grid_2d -v4 -t 101 2>/dev/null | $BIN/geo -upgrade - > mesh-2d-101.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
run "$SBIN/mkgeo_grid_3d -v4 -T 5   2>/dev/null | $BIN/geo -upgrade - > mesh-3d-5.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
run "$SBIN/mkgeo_grid_3d -v4 -T 6   2>/dev/null | $BIN/geo -upgrade - > mesh-3d-6.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi
# --------------------
# 2D tests
# --------------------
for geo in mesh-2d-11 mesh-2d-20 mesh-2d-101 $DATADIR/square_t_bamg; do

  loop_mpirun "./form_on_band_tst $geo -test 1 -normal 1 0 0 -origin 0.5 0.5 0 -m 1    -a 0 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test 1 -normal 0 1 0 -origin 0.5 0.5 0 -m 1    -a 0 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test x -normal 0 1 0 -origin 0.5 0.5 0 -m 0.333333333 -a 1 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test y -normal 1 0 0 -origin 0.5 0.5 0 -m 0.333333333 -a 1 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test x -normal 1 0 0 -origin 0.5 0.5 0 -m 0.25 -a 0 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test y -normal 0 1 0 -origin 0.5 0.5 0 -m 0.25 -a 0 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done

loop_mpirun "./form_on_band_tst mesh-2d-101 -test x2 -normal 0 1 0 -origin 0.5 0.5 0 -m 0.200010892391878 -a 1.3333006567983 >/dev/null 2>/dev/null"
if test $? -ne 0; then status=1; fi
# --------------------
# 3D tests
# --------------------
for geo in mesh-3d-5 mesh-3d-6 $DATADIR/cube-gmsh-v2; do
  loop_mpirun "./form_on_band_tst $geo -test 1 -normal 0 1 0 -origin 0.5 0.5 0.5 -m 1 -a 0 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test x -normal 0 1 0 -origin 0.5 0.5 0.5 -m 0.333333333333333 -a 1 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test y -normal 1 0 0 -origin 0.5 0.5 0.5 -m 0.333333333333333 -a 1 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test z -normal 1 0 0 -origin 0.5 0.5 0.5 -m 0.333333333333333 -a 1 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./form_on_band_tst $geo -test x -normal 1 0 0 -origin 0.5 0.5 0.5 -m 0.25 -a 0 > /dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done
# --------------
# clean
# --------------
run "rm -f mesh-2d-101.geo mesh-2d-20.geo mesh-2d-11.geo mesh-3d-5.geo mesh-3d-6.geo"

exit $status
