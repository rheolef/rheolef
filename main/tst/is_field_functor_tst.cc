///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/field_expr.h"
#include "rheolef/integrate.h"
using namespace rheolef;
using namespace std;

struct f1 {
  Float operator() (const point& x) const { return x[0]+2*x[1]+3*x[2]; }
};
point f2  (const point& x) { return point (x[0]+2*x[1]+3*x[2], 5*x[1]+x[2], 3*x[2]); }
struct f3 {
  tensor operator() (const point& x) const { return tensor(); }
};

// test for invalid field fonctions
//  missing: const point&
point g1 (point x) { return point (x[0]+2*x[1]+3*x[2], 5*x[1]+x[2], 3*x[2]); } 
//  missing: const qualifier:
struct g2 {
  Float operator() (const point& x) { return x[0]+2*x[1]+3*x[2]; }
};
//  missing: operator()
struct g3 {};

template <class F> 
const typename enable_if<details::is_field_true_function<F>::value, F>::type&
scan (const F& f) {
  //dout << "field_function = "<<pretty_typename_macro(F) << endl; // non-reproductible when float128
  dout << "field_function :" << endl;
  dout << "  is_functor   = "<< details::is_functor<F>::value << endl;
  //dout << "  result       = "<< pretty_typename_macro(typename details::field_function_traits<F>::result_type) << endl;
  //dout << "  result2      = "<< pretty_typename_macro(typename std::function<F>::result_type) << endl;
  return f;
}
template <class F> 
const typename enable_if<details::is_field_functor<F>::value,F>::type&
scan (const F& f) {
  //dout << "field_functor  = "<<pretty_typename_macro(decltype(&F::operator())) << endl; // non-reproductible when float128
  dout << "field_functor  :"<< endl;
  dout << "  is_functor   = "<< details::is_functor<F>::value << endl;
  //dout << "  result       = "<< pretty_typename_macro(typename details::field_function_traits<F>::result_type) << endl;
  //dout << "  result2      = "<< pretty_typename_macro(typename details::get_functor_result<F>::type) << endl;
  return f;
}
template <class F> 
const typename enable_if<
     ! details::is_field_true_function<F>::value
  && ! details::is_field_functor<F>::value
 ,F
>::type&
scan (const F& f) {
  dout << "other          :" << endl;
  dout << "  is_function  = "<< std::is_function<F>::value << endl;
  dout << "  is_functor   = "<< details::is_functor<F>::value << endl;
  return f;
}

int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  scan (f1());
  scan (f2);
  scan (f3());
  scan (g1);
  scan (g2());
  scan (g3());
  scan (field());
  dout << "is_callable(field) = "<< details::is_callable<field,Float (const point&) const>::value << endl;
}
