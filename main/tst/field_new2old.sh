#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
# .field new2old fmt converter
d=$1
geo=$2
Pk=$3
valued=${4-"vector"}
domain=${5-"left"}
gawk -v d=$d -v geo=$geo -v Pk=$Pk -v valued=${valued} -v domain=${domain} '
BEGIN		{
                  if (valued == "vector") {
                    n_comp = d;
                  } else {
                    n_comp = d*(d+1)/2;
                  }
                  state = 0;
		  idof = 0;
		}
(/vector/ || /tensor/)	{ state = 1; next; }
(state == 1 && $1 != "") { value[1+idof] = $1; ++idof; }
END		{
		  ndof = idof;
		  comp_ndof = ndof/n_comp;
		  #print "# d =",d," n_comp =",n_comp, " ndof =",ndof," comp_ndof =",comp_ndof
		  print "field"
		  print "2"
		  print "header"
                  if (domain == "none") {
		    printf ("  constitution %s(%s(%s))\n",     valued, Pk, geo);
                  } else {
		    printf ("  constitution %s(%s(%s[%s]))\n", valued, Pk, geo, domain);
                  }
		  print "end header"
		  print ""
		  for (i_comp = 0; i_comp < n_comp; ++i_comp) { 
		    for (comp_idof = 0; comp_idof < comp_ndof; ++comp_idof) { 
		      idof = comp_idof*n_comp + i_comp;
		      print value[1+idof];
		    }
	 	    print "";
		  }
		}
'
