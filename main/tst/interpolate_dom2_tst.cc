///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check interpolate on a bdry space, based on a sub-domain (geo_domain)
// was buggy with mpirun:
//
//  mpirun -np 3 ./interpolate_dom2_tst ../../../rheolef/nfem/ptst/my_cube_TP-5-v2.geo P2 left 1e-8 -expression
//
//  -> the result contains an "inf" value that was not set at idof=1
//     the interpolate operator was based on a loop on elements
//     in the geo_domain=my_cube_TP-5-v2[left] on proc=0
//	there is no elements (no faces) but exactly one vetex
//     => a P1-dof, located at vertex, is on proc=0 but is not reachable by
//	  a loop^based on elements without any communications
//     => fix  the bug by adding communications on the element-based loop interpolate
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct g {
  Float operator() (const point& x) const { return -1-(x[0]+x[1]+x[2]); }
};
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  space Vh (omega, argv[2]);
  string dom_name = (argc > 3) ?         argv[3] : "left";
  Float prec      = (argc > 4) ?    atof(argv[4]) : 1e-10;
  space Wh (omega[dom_name], argv[2]);
  field gh = lazy_interpolate (Wh, g());  
  dout  << round (gh, prec);
}
