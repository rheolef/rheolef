///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// find a bug from the doc command (oldroyd.tex):
//   field contraction-We-0.3.field -domain axis -mark tau -comp 00 -elevation -gnuplot
// this cause an interpolation on a boundary domain of a discontinuous Pkd field
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float g (const point& x) { return sin(x[0]+x[1]+x[2]); }
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  string approx = argv[2];
  string dom_name = (argc > 3) ? argv[3] : "right";
  Float tol       = (argc > 4) ? atof(argv[4]) : 1e-10;
  space Vh (omega, approx);
  size_t k = Vh.degree();
  field uh = lazy_interpolate (Vh, g);
  space Wh (omega[dom_name], approx);
  field wh1 = lazy_interpolate (Wh, uh); // here, interpolate uh on a bdr domain
  field wh2 = lazy_interpolate (Wh, g);
  quadrature_option qopt;
  qopt.set_family (quadrature_option::gauss);
  qopt.set_order (2*Vh.degree()+1);
  Float err = integrate (Wh.get_geo(), norm(wh1 - wh2), qopt);
  derr << "err = " << err << endl;
  return (k == 0 || err < tol) ? 0 : 1;
}
