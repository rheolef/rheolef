///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// draft-1 version of the geo_element class with arbitrarily order
// solution with std::vector inside each geo_element
//
// avantage : 
//  - souplesse : gere la memoire en tas dans geo (efficace)
//    mais aussi peut creer des elements temporaires, si besoin
//  - inconvenient : tableau de geo_element, qui contiennent des tableaux
//    alloues dynamiquement => la memoire n'est pas contigue globalement
//
#include "rheolef/hack_array.h"
#include "rheolef/geo_element.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;

int main(int argc, char**argv)
{
  environment distributed (argc, argv);
  
  distributor ownership (distributor::decide, communicator(), 1); // n=1
  geo_element::parameter_type param (reference_element::e, 1); // edge, order 1
  hack_array<geo_element_hack, sequential> ge_e (ownership,param);
  geo_element& E = ge_e [0];

  // check raw op[] 
  E [0] = 18;
  E [1] = 12;
  cout << "E  = "; E.put(cout); cout << endl; // pb with cout << E: when Float=float128...
  // check dyn copy cstor
  geo_element_auto<> E2 = E;
  E2[0] = 5;
  cout << "E2 = " << E2 << endl;
  cout << "E  = "; E.put(cout); cout << endl; // pb with cout << E: when Float=float128...

  // check raw op= 
  E = E2;
  cout << "E  = "; E.put(cout); cout << endl; // pb with cout << E: when Float=float128...

  // check dyn op=
  E [0] = 3;
  cout << "E  = "; E.put(cout); cout << endl; // pb with cout << E: when Float=float128...
  E2 = E;
  cout << "E2 = " << E2 << endl;

  cout << "array " << ge_e.size() << endl;
  ge_e.put_values(dout);
#ifdef TODO
#endif // TODO
}
