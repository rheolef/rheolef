///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile reconstruction_hho.cc The hybrid high order method -- reconstruction operator
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int check_lazy_mult (geo omega, Float tol) {
  space Xh(omega, "P1d"),
        Qh(omega, "P0");
  trial u(Xh), p(Qh);
  test  v(Xh), q(Qh);
  auto a = lazy_integrate (p*q);
  auto b = lazy_integrate (u*q);
  form c = a*b;
  form a1 = integrate (p*q);
  form b1 = integrate (u*q);
  form c1 = a1*b1;
  form err_c = c - c1;
  Float err = err_c.uu().max_abs();
  derr << "mult: err = " << err << endl;
  return err < tol ? 0 : 1;
}
int check_lazy_share_form (geo omega, Float tol) {
  space Xh(omega, "P1d");
  trial u(Xh); test v(Xh);
  auto m = lazy_integrate (u*v);
  auto inv_m = inv(m); // then inv_m will be shared in common subexprs
  auto m_inv_m = m*inv_m;
  auto inv_m_m = inv_m*m;
  auto err_m1 = m_inv_m - inv_m_m;
  auto err_m2 = inv_m - inv_m*m_inv_m; // m_inv_m is reused here
  form err_m  = err_m1 + err_m2;
  Float err = err_m.uu().max_abs();
  derr << "share_form: err = " << err << endl;
  return err < tol ? 0 : 1;
}
int check_lazy_share_field (geo omega, Float tol) {
  space Xh(omega, "P1d");
  trial u(Xh); test v(Xh);
  auto m = lazy_integrate (u*v);
  field uh (Xh, 1);
  auto v1h = m*uh;
  auto w1h = m*v1h;
  field e1h = inv(m)*w1h - v1h; // sub-expr v1h is shared here
  Float err1 = e1h.max_abs();
  derr << "share_field: err1 = " << err1 << endl;
  auto v2h = m.trans_mult(uh);
  auto w2h = m.trans_mult(v2h);
  field e2h = inv(m).trans_mult(w2h) - v2h; // sub-expr v2h is shared here
  Float err2 = e2h.max_abs();
  derr << "share_field: err2 = " << err2 << endl;
  return err1 < tol && err2 < tol ? 0 : 1;
}
Float f (const point& x) { return 1; }
int check_lazy_op_plus_field (geo omega, Float tol) {
  space Xh (omega["left"], "P1");
  field uh;
  uh = lazy_interpolate (Xh, f);
  Float err = field(uh - field(Xh, 1)).max_abs();
  derr << "op_set_field:   err = " << err << endl;
  uh += lazy_interpolate (Xh, f);
  err = field(uh - field(Xh, 2)).max_abs();
  derr << "op_plus_field:  err = " << err << endl;
  uh -= lazy_interpolate (Xh, f);
  err = field(uh - field(Xh, 1)).max_abs();
  derr << "op_minus_field: err = " << err << endl;
  return err < tol ? 0 : 1;
}
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  Float tol = sqrt(numeric_limits<Float>::epsilon());
  int status = 0;
  status |= check_lazy_mult          (omega, tol);
  status |= check_lazy_share_form    (omega, tol);
  status |= check_lazy_share_field   (omega, tol);
  status |= check_lazy_op_plus_field (omega, tol);
  return status;
}
