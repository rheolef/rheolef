///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"

using namespace std;
using namespace rheolef;
struct f {
  Float operator() (const point& x) const { return x[0]; }
};
int main (int argc, char **argv){
  environment rheolef(argc,argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  space Wh (omega["top"], argv[2]);
  Float  tol = (argc > 3) ? atof(argv[3]) : 1e-10;
  trial u (Xh), u_top (Wh); test v (Xh);

  form mb = integrate ("top", u_top*v); // TODO: omit "top"
  field one (Xh, 1);
  //
  // test 1: with a field weight arg
  //
  field id_top (Wh, 1);
  form m1_top = integrate ("top", u*v*id_top);
  field mr1a =     mb*one["top"];
  field mr1b = m1_top*one;
  field e1h = mr1a - mr1b;
  Float err1 = e1h.max_abs();
  derr << "err1 = " << err1 << endl;
  //
  // test 2: with a class-fct weight arg (polynomial P1)
  //
  form m2_top = integrate ("top", u*v*f());
  field mr2a = mb*lazy_interpolate(Wh, f()); 
  field mr2b = m2_top*one;
  field e2h = mr2a - mr2b;
  Float err2 = e2h.max_abs();
  derr << "err2 = " << err2 << endl;
  //
  // test 3: with a complex field expression (from lave/fournaise-ray.cc)
  //
  Float Nu = 0.5;
  Float Ra = 0.5;
  Float alpha = 0;
  form m3_top = integrate ("top", u*v*(Nu + Ra*(id_top+2*alpha)*(sqr(id_top+alpha) + sqr(alpha))));
  field mr3b = m3_top*one;
  field e3h = mr1a - mr3b;
  Float err3 = e3h.max_abs();
  derr << "err3 = " << err3 << endl;

  return (err1 + err2 + err3 < tol) ? 0 : 1;
}
