#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
GEODIR=$SRCDIR
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

echo "      BUG_REGRESSION_MPI (skipped))"
exit 0

status=0

# ------------------
# 1D
# ------------------
L="
$GEODIR/line-20-bdry.geo
"
for geo in $L; do
  loop_mpirun "./geo_trace_move_tst -x 0.5 -v  1 -y 1 < $geo >/dev/null 2>/dev/null";
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./geo_trace_move_tst -x 0.5 -v -1 -y 0 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./geo_trace_move_tst -x 0.5 -v 0.25 -y 0.75 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done
# ------------------
# 2D
# ------------------
L="
$GEODIR/carre-10-bdry-v2.geo
$GEODIR/carre-q-10-dom-v2.geo
$GEODIR/carre-tq-10-bdry-v2.geo
"
for geo in $L; do
  loop_mpirun "./geo_trace_move_tst -x 0.5 0.5 -v  1 0 -y 1 0.5 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./geo_trace_move_tst -x 0.5 0.5 -v -1 0 -y 0 0.5 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./geo_trace_move_tst -x 0.5 0   -v  1 0 -y 0.5 0 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./geo_trace_move_tst -x 0.5 0.5 -v 0.25 0 -y 0.75 0.5 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done
# ------------------
# 3D
# ------------------
L="
$GEODIR/cube-5-bdry-v2.geo 
$GEODIR/my_cube_H-5-v2.geo
$GEODIR/my_cube_TP-5-v2.geo
$GEODIR/my_cube_PH-5-v2.geo
$GEODIR/my_cube_TPH-5-v2.geo
"
# this last test fails on "salsa" (27005 Exception en point flottant)
# but not on others machines:
if test "`hostname`" != "salsa"; then
  L=" $L $GEODIR/cube-P-5-dom-v2.geo"
fi
for geo in $L; do
  loop_mpirun "./geo_trace_move_tst -x 0.5 0.5 0.5 -v  1 0 0 -y 1   0.5 0.5 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./geo_trace_move_tst -x 0.5 0.5 0.5 -v -1 0 0 -y 0   0.5 0.5 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./geo_trace_move_tst -x 0.5 0.5 0   -v -1 0 0 -y 0.5 0.5 0   < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  loop_mpirun "./geo_trace_move_tst -x 0.5 0.5 0.5 -v 0.25 0 0 -y 0.75 0.5 0.5 < $geo >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
