/*********************************************************************
 *
 *  Gmsh tutorial 3
 * 
 *  Extruded meshes, options
 *
 *********************************************************************/

// Again, we start by including the first tutorial:

// Include "t1.mshcad";
/********************************************************************* 
 *
 *  Gmsh tutorial 1
 * 
 *  Variables, elementary entities (points, lines, surfaces), physical
 *  entities (points, lines, surfaces)
 *
 *********************************************************************/

// The simplest construction in Gmsh's scripting language is the
// `affectation'. The following command defines a new variable `lc':

lc = 0.009;

// This variable can then be used in the definition of Gmsh's simplest
// `elementary entity', a `Point'. A Point is defined by a list of
// four numbers: three coordinates (X, Y and Z), and a characteristic
// length (lc) that sets the target element size at the point:

Point(1) = {0, 0, 0, lc};

// The distribution of the mesh element sizes is then obtained by
// interpolation of these characteristic lengths throughout the
// geometry. Another method to specify characteristic lengths is to
// use a background mesh (see `t7.mshcad' and `bgmesh.pos').

// We can then define some additional points as well as our first
// curve.  Curves are Gmsh's second type of elementery entities, and,
// amongst curves, straight lines are the simplest. A straight line is
// defined by a list of point numbers. In the commands below, for
// example, the line 1 starts at point 1 and ends at point 2:

Point(2) = {.1, 0,  0, lc} ;
Point(3) = {.1, .3, 0, lc} ;
Point(4) = {0,  .3, 0, lc} ;

Line(1) = {1,2} ;
Line(2) = {3,2} ;
Line(3) = {3,4} ;
Line(4) = {4,1} ;

// The third elementary entity is the surface. In order to define a
// simple rectangular surface from the four lines defined above, a
// line loop has first to be defined. A line loop is a list of
// connected lines, a sign being associated with each line (depending
// on the orientation of the line):

Line Loop(5) = {4,1,-2,3} ;

// We can then define the surface as a list of line loops (only one
// here, since there are no holes--see `t4.mshcad'):

Plane Surface(6) = {5} ;

// At this level, Gmsh knows everything to display the rectangular
// surface 6 and to mesh it. An optional step is needed if we want to
// associate specific region numbers to the various elements in the
// mesh (e.g. to the line segments discretizing lines 1 to 4 or to the
// triangles discretizing surface 6). This is achieved by the
// definition of `physical entities'. Physical entities will group
// elements belonging to several elementary entities by giving them a
// common number (a region number), and specifying their orientation.

// We can for example group the points 1 and 2 into the physical
// entity 1:

Physical Point(1) = {1,2} ;

// Consequently, two punctual elements will be saved in the output
// mesh file, both with the region number 1. The mechanism is
// identical for line or surface elements:

MyLine = 99;
Physical Line(MyLine) = {1,2,4} ;

Physical Surface("My fancy surface label") = {6} ;

// All the line elements created during the meshing of lines 1, 2 and
// 4 will be saved in the output mesh file with the region number 99;
// and all the triangular elements resulting from the discretization
// of surface 6 will be given an automatic region number (100,
// associated with the label "My fancy surface label").

// Note that if no physical entities are defined, then all the
// elements in the mesh will be saved "as is", with their default
// orientation.
/********************************************************************* 
 *  end of Gmsh tutorial 1 "t1.mshcad";
 *********************************************************************/

// As in `t2.mshcad', we plan to perform an extrusion along the z axis.
// But here, instead of only extruding the geometry, we also want to
// extrude the 2D mesh. This is done with the same `Extrude' command,
// but by specifying element 'Layers' (2 layers in this case, the
// first one with 8 subdivisions and the second one with 2
// subdivisions, both with a height of h/2):

h = 0.1;

Extrude {0,0,h} { 
  Surface{6}; Layers{ {8,2}, {0.5,1} }; 
}

// The extrusion can also be performed with a rotation instead of a
// translation, and the resulting mesh can be recombined into prisms
// (we use only one layer here, with 7 subdivisions). All rotations
// are specified by an axis direction ({0,1,0}), an axis point
// ({-0.1,0,0.1}) and a rotation angle (-Pi/2):

Extrude { {0,1,0} , {-0.1,0,0.1} , -Pi/2 } { 
  Surface{122}; Layers{7}; Recombine; 
}

// Note that a translation ({-2*h,0,0}) and a rotation ({1,0,0},
// {0,0.15,0.25}, Pi/2) can also be combined:

out[] = Extrude { {-2*h,0,0}, {1,0,0} , {0,0.15,0.25} , Pi/2 } { 
  Surface{news-1}; Layers{10}; Recombine; 
};

// In this last extrusion command we retrieved the volume number
// programatically by saving the output of the command into a
// list. This list will contain the "top" of the extruded surface (in
// out[0]) as well as the newly created volume (in out[1]).

// We can then define a new physical volume to save all the tetrahedra
// with a common region number (101):

Physical Volume(101) = {1, 2, out[1]};

// Let us now change some options... Since all interactive options are
// accessible in Gmsh's scripting language, we can for example define
// a global characteristic length factor or redefine some colors
// directly in the input file:

Mesh.CharacteristicLengthFactor = 4;
General.Color.Text = White;
Geometry.Color.Points = Orange;
Mesh.Color.Points = {255,0,0};

// Note that all colors can be defined literally or numerically, i.e.
// `Mesh.Color.Points = Red' is equivalent to `Mesh.Color.Points =
// {255,0,0}'; and also note that, as with user-defined variables, the
// options can be used either as right or left hand sides, so that the
// following command will set the surface color to the same color as
// the points:

Geometry.Color.Surfaces = Geometry.Color.Points;

// You can click on the `?'  button in the status bar of the graphic
// window to see the current values of all options. To save all the
// options in a file, you can use the `File->Save as->Gmsh options'
// menu. To save the current options as the default options for all
// future Gmsh sessions, you should use the `Tools->Options->Save as
// defaults' button.
