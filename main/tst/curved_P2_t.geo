#!geo

mesh
4
header
 dimension 2
 order     2
 nodes	6
 triangles	1
 edges	3
end header

0 0
1 0
0 1
0.5 0.25
0.5 0.75
0 0.5

t	0 1 2

e	0 1
e	1 2
e	2 0
