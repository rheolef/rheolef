#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"7"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

L="
zero-5
line-dom-v2
carre-10-bdry-v2
carre-q-10-dom-v2
carre-tq-10-dom-v2
cube-dom-v2
cube-H-6-dom-v2
cube-P-5-dom-v2
my_cube_PH-5-v2
my_cube_TP-5-v2
my_cube_TPH-5-v2
circle_p2-v2
circle_p5-v2
circle_tq_p10-v2
"
#TODO P9(T)
L_HIGH="
sphere_p9-v2
"
for geo in $L; do
  for Pk in P0 P1 P2 P1d P3; do
    loop_mpirun "./space_tst ${SRCDIR}/$geo.geo $Pk 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done

exit $status
