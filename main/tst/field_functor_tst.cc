///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check field_expr<Expr>
#include "rheolef.h"
#include "rheolef/pretty_name.h"
using namespace rheolef;
using namespace std;

Float  fs (const point&) { return Float(5); }
point  fp (const point&) { return point(1,2,3); }
tensor ft (const point&) { return otimes(point(1,2,3),point(4,5,6)); }

struct cs {
  Float operator() (const point&) const { return Float(7); }
};
struct cp {
  point operator() (const point&) const { return point(7,8,9); }
};

struct gs {
  Float operator() (const point&) const { return Float(7); }
};
struct gp {
  point operator() (const point&) const { return point(7,9,11); }
};
struct gt {
  tensor operator() (const point&) const { return otimes(point(1,2,3),point(4,5,6)); }
};

template<class T, class R>
void surcharge (R(*f)(const point_basic<T>&)) { cout << "R-valued true function=" << f(point()) << endl; }

template<class F>
typename
std::enable_if<
  details::is_functor<F>::value
 ,void
>::type
surcharge (const F& f) { cout << "R-valued functor=" << f(point()) << endl; }

void surcharge (const function<Float(const point_basic<Float>&)>& f) { cout << "scalar-valued class function=" << f(point()) << endl; }


#ifdef PB
// class-fct arg cannot be template:
template<class T>
void surcharge (const function<T(const point_basic<T>&)>& f) { cout << "scalar-valued class function=" << f(point()) << endl; }

// class-fct arg cannot have a return type overloaded: ambiguous
void surcharge (const function<point(const point_basic<Float>&)>& f) { cout << "vector-valued class function=" << f(point()) << endl; }
#endif // PB

int main(int argc, char**argv) {
    environment rheolef(argc, argv);

    surcharge (fs);
    surcharge (fp);
    surcharge (ft);

    surcharge (gs());
    surcharge (gp());
    surcharge (gt());

    surcharge (cs());
    // on ne peut pas faire cp et ct: ambiguites
}
