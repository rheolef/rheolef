///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check field expr with normal() operator on a boundary domain
#include "rheolef.h"
using namespace rheolef;
using namespace std;
point u(const point& x) { return point(1,0); }
struct my_dot : std::binary_function<point,point,Float> {
  Float operator() (const point& u, const point& v) const { return dot(u,v); }
};
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  string dom_name = (argc > 2) ? argv[2] : "right";
  Float tol = (argc > 3) ? atof(argv[3]) : 1e-10;
  bool dump = (argc > 4);
  size_t k = omega.order();
  string u_approx = "P" + std::to_string(k);
  string n_approx = "P" + std::to_string(k-1) + "d";
  space Vh (omega,           u_approx, "vector");
  space Wh (omega[dom_name], n_approx, "vector");
  space Zh (omega[dom_name], u_approx);
dis_warning_macro("interpolate fun...");
  field uh = lazy_interpolate(Vh, u);
dis_warning_macro("interpolate field...");
#ifdef TODO
  field ch = lazy_interpolate(Zh, dot(uh,normal()));
#else // TODO
  field ch = lazy_interpolate(Zh, compose(my_dot(),uh,normal()));
#endif // // TODO
dis_warning_macro("interpolate done");
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(2*k+1); 
  field eh = ch - 1;
  Float err = eh.max_abs();
  derr << "err = "<< err << endl;
  if (dump) dout << lazy_interpolate(Wh,normal());
  return (err < tol) ? 0 : 1;
}
