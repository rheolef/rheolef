///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check for field expressions as:
//	field u = (a*x + y)*(c*z + t); 
// where: x,y: scalar fields, 
//        z,t: vector fields
// this was buggy (core dump) and has been fixed.
//
#define _RHEOLEF_VEC_EXPR_H
#define _RHEOLEF_VEC_EXPR_OPS_H
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  Float  tol = (argc > 2) ? atof(argv[2]) : 1e-10;
  size_t d = omega.dimension();
  // scalar case:
  space Xh (omega, "P1");
  field xh (Xh, 1);
  field norm_xh = lazy_interpolate (Xh, norm2(xh));
  field exh = xh - norm_xh;
  Float err_x = dual (exh,exh);
  derr << "err_x = " << err_x << endl;
  // vector case:
  space Vh (omega, "P1", "vector");
  field uh (Vh, 1);
  field norm_uh = lazy_interpolate (Xh, norm2(uh));
  field euh = field(Xh,d) - norm_uh;
  Float err_u = dual (euh,euh);
  derr << "err_u = " << err_u << endl;
  // tensor case:
  space Th (omega, "P1", "tensor");
  field tau_h (Th, 1);
  field norm_tau_h = lazy_interpolate (Xh, norm2(tau_h));
  size_t n = d*d;
  if (omega.coordinate_system() == space_constant::axisymmetric_rz ||
      omega.coordinate_system() == space_constant::axisymmetric_zr) {
    n++; // tau_theta_theta extra component
  }
  field e_tau_h = field(Xh,n) - norm_tau_h;
  Float err_tau = dual (e_tau_h, e_tau_h);
  derr << "err_tau = " << err_tau << endl;

  Float err = err_x + err_u + err_tau;
  return (err < tol) ? 0 : 1;
}
