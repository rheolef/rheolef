///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//  given x, search K in mesh such that x in K
//
// usage: prog x0 x1 x2 < mesh.geo
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  geo_basic<Float> omega;
  din >> omega;
  size_t d = omega.dimension();
  check_macro (d == 2, "dimension == 2 expected");
  point_basic<Float> x, x_nearest_exact;
  x[0] = (argc > 1) ? atof(argv[1]) : 1.51;
  x[1] = (argc > 2) ? atof(argv[2]) : 1.52;
  bool do_check = (argc > 3);
  x_nearest_exact[0] = (argc > 3) ? atof(argv[3]) : 0;
  x_nearest_exact[1] = (argc > 4) ? atof(argv[4]) : 0;
  Float tol = 1e-7;

  point x_nearest; 
  size_t dis_ie = omega.dis_nearest (x, x_nearest); 
  dout << "nearest element index = " << dis_ie << endl;
  dout << "nearest point = " << x_nearest << endl;
  dout << "distance to nearest point = " << dist(x,x_nearest) << endl;
  if (do_check) {
    dout << "expected nearest point = " << x_nearest_exact << endl;
    dout << "error = " << dist(x_nearest, x_nearest_exact) << endl;
  }
  return (dis_ie != numeric_limits<size_t>::max() && dist(x_nearest, x_nearest_exact) < tol) ? 0 : 1;
}
