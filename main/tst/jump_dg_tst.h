#ifndef JUMP_DG_TST_H
#define JUMP_DG_TST_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// for various dg tests with mkgeo_grid -region

typedef enum {
  west_side = 0,
  east_side = 1,
  both_side = 2
} interface_side;

static std::string side_name[] = {"west","east","both"};

// interface x0=1/2
Float interface_indicator (const point& x) {
  static const Float eps = 1e-7;
  return (abs(x[0] - 0.5) > eps) ? 0 : 1;
}
// --------------------------------------------------------------------------
// non-polynomial function with jump at interface x0=1/2
// --------------------------------------------------------------------------
struct u_west {
  Float operator() (const point& x) const {
    return cos(0.5*pi*x[0])
          *cos(0.5*pi*x[1])
          *cos(0.5*pi*x[2]); }
  static Float integrate_interface(size_t d) {
    Float pi = acos(Float(-1));
    switch (d) {
      case 1:  return 1/sqrt(Float(2));
      case 2:  return sqrt(Float(2))/pi;
      default: return 2*sqrt(Float(2))/sqr(pi);
    }
  }
  static Float integrate_region(size_t d) {
    Float pi = acos(Float(-1));
    switch (d) {
      case 1:  return sqrt(Float(2))/pi;
      case 2:  return 2*sqrt(Float(2))/sqr(pi);
      default: return 4*sqrt(Float(2))/pow(pi,3);
    }
  }
  u_west (size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; Float pi;
};
// u_east(x) > u_west(x) on the interface x0=1/2:
struct u_east {
  Float operator() (const point& x) const {
    return (1+sin(0.5*pi*x[0]))
          *(1+sin(0.5*pi*x[1]))
          *(1+sin(0.5*pi*x[2])); }
  static Float integrate_interface(size_t d) {
    Float pi = acos(Float(-1));
    switch (d) {
      case 1:  return  1+1/sqrt(Float(2));
      case 2:  return (1+1/sqrt(Float(2)))*(1+2/pi);
      default: return (1+1/sqrt(Float(2)))*sqr(1+2/pi);
    }
  }
  static Float integrate_region(size_t d) {
    Float pi = acos(Float(-1));
    switch (d) {
      case 1:  return           (2*sqrt(Float(2))+pi)/(2*pi);
      case 2:  return    (2+pi)*(2*sqrt(Float(2))+pi)/(2*sqr(pi));
      default: return sqr(2+pi)*(2*sqrt(Float(2))+pi)/(2*pow(pi,3));
    }
  }
  u_east (size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; Float pi;
};
struct u_west_interface: u_west {
  Float operator() (const point& x) const {
    if (abs(x[0] - 0.5) > eps) return 0;
    return u_west::operator() (x);
  }
  u_west_interface (size_t d) : u_west(d), eps(1e-7) {}
  Float eps;
};
// --------------------------------------------------------------------------
// polynomial function with jump at interface x0=1/2
// --------------------------------------------------------------------------
struct p_west {
  Float operator() (const point& x) const { return (d==1) ? 1 : pow(x[d-1],n); }
  static Float integrate_interface(size_t d, size_t a) { return (d==1) ? 1 : 1.0/Float(a+1); }
  static Float integrate_region   (size_t d, size_t a) { return (d==1) ? 1 : 0.5/Float(a+1); }
  p_west (size_t d1, size_t n1) : d(d1), n(n1) {}
  size_t d, n;
};
struct p_east {
  Float operator() (const point& x) const { return (d==1) ? -3 : -3*pow(x[d-1],n); }
  static Float integrate_interface(size_t d, size_t a) { return (d==1) ? -3 : -3.0/Float(a+1); }
  static Float integrate_region   (size_t d, size_t a) { return (d==1) ? -3 : -1.5/Float(a+1); }
  p_east (size_t d1, size_t n1) : d(d1), n(n1) {}
  size_t d, n;
};
// --------------------------------------------------------------------------
// vector version (eg RT)
// --------------------------------------------------------------------------
struct interface_normal {
  point operator() (const point& x) const { return n; }
  interface_normal (size_t d) : n((d < 3) ? point(1,0) : point(0,1,0)) {}
  point n;
};
struct p_interface_normal_west {
  point operator() (const point& x) const { return p(x)*n(x); }
  p_interface_normal_west (size_t d, size_t k) : p(d,k), n(d) {}
  p_west p;
  interface_normal n;
};
struct p_interface_normal_east {
  point operator() (const point& x) const { return p(x)*n(x); }
  p_interface_normal_east (size_t d, size_t k) : p(d,k), n(d) {}
  p_east p;
  interface_normal n;
};
#endif // JUMP_DG_TST_H
