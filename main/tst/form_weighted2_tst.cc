///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute:
//  \int_\Omega u(x) v(x) weight(x) dx
//
// where weight(x) is given by a function or a class function
//
// variant of the previous code: the weight is obtained as
// a composition of a class-function and a field
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
//
// note: with the following choice:
//
//  1) w(x) is singular at domain boundary
//     thus, w cannot be interpolated as wh
//     and then wh send as a weight
//     here: w is directly interpolated at quadrature nodes
//     that are internal to the elements (gauss)     
//
//  2) \int_\Omega weight(x) dx = pi^d when Omega=]0,1[^d
//     thus the result can be checked
//
struct my_inv : unary_function<Float,Float> {
  Float operator() (const Float& z) const { return 1/z; }
};
struct inv_weight {
  Float operator() (const point& x) const {
    switch(d) {
      case 1:  return sqrt(x[0]*(1-x[0]));
      case 2:  return sqrt(x[0]*x[1]*(1-x[0])*(1-x[1]));
      default: return sqrt(x[0]*x[1]*x[2]*(1-x[0])*(1-x[1])*(1-x[2]));
    }
  }
  inv_weight(size_t d1) : d(d1) {}
  size_t d;
};
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  Float  tol = (argc > 3) ? atof(argv[3]) : 1e-10;
  Float pi = acos(Float(-1));
  size_t d = omega.dimension();
  field inv_wh = lazy_interpolate (Xh,inv_weight(d));
  quadrature_option qopt;
  qopt.set_family (quadrature_option::gauss);
  qopt.set_order  (Xh.degree());
  form  m (Xh, Xh, "mass", compose(my_inv(),inv_wh), qopt);
  field one(Xh,1);
  Float res = m(one,one);
  Float res_exact = pow(pi,d);
  Float err = fabs(res - res_exact);
  derr << "res       = " << res << endl
       << "res_exact = " << res_exact << endl
       << "err       = " << err << endl;
  return (err < tol) ? 0 : 1;
}
