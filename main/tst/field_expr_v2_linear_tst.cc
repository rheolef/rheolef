///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// new implementation of field expressions: basic tests

#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  space Xh (omega, "P1");
  Float err = 0;
  field uh(Xh,1), wh(Xh,0);
  field vh = -uh;
  wh = uh-vh;
  wh = 1-vh;
  wh = uh+1;
  wh = 2*uh;
  wh = uh*2;
  wh = uh/2;
  wh += uh;
  wh *= 2;
  wh /= 3;

  Float value = wh.max();
  Float expected = 1;
  Float err1 = max(fabs(wh.min()-expected), fabs(wh.max()-expected));
  dout << "value = " << value << endl;
  dout << "expected = " << expected << endl;
  dout << "err1 = " << err1 << endl;
  err = max(err1, err);
  //dout << wh;

  Float s = dual (1, 2*uh);
  Float expected2 = 2*uh.dis_ndof();
  Float err2 = fabs(s - expected2);
  dout << "s = " << s << endl;
  dout << "expected2 = " << expected2 << endl;
  dout << "err2 = " << err2 << endl;
  err = max(err2, err);

  Float s3 = dual (uh-vh, uh);
  Float err3 = fabs(s3 - expected2);
  dout << "s3 = " << s3 << endl;
  dout << "expected2 = " << expected2 << endl;
  dout << "err3 = " << err3 << endl;
  err = max(err3, err);

#ifdef BUG_FIELD_COMPONENT_ACCESS
  // field_wdof_sliced
  space Vh (omega, "P1", "vector");
  field uuh (Vh,0);
  uuh[0] = 2*uh;
  uuh[0] += 1*uuh[0];
  uuh[0] *= 0.5;
  Float s4 = dual (1, uuh);
  Float err4 = fabs(s4 - expected2);
  dout << "s4 = " << s4 << endl;
  dout << "expected2 = " << expected2 << endl;
  dout << "err4 = " << err4 << endl;
  err = max(err4, err);

  // field_indirect
  space Wh (omega["boundary"], "P1", "vector");
  uuh = 0;
  uuh[0]["boundary"] = 2*uh["boundary"];
  uuh[0]["boundary"] += 1*uuh[0]["boundary"];
  uuh[0]["boundary"] /= 2;
  Float s5 = dual (1, uuh);
  Float expected5 = Wh.dis_ndof(); // = 2*nb of bdr vertices
  Float err5 = fabs(s5 - expected5);
  dout << "s5 = " << s5 << endl;
  dout << "expected5 = " << expected5 << endl;
  dout << "err5 = " << err5 << endl;
  err = max(err5, err);
#endif // BUG_FIELD_COMPONENT_ACCESS

#ifdef TODO
  // vector-valued linear exprs with constants
  point uu0 (1,1);
  uuh = uuh + uu0;
#endif // TODO

  return (err < 1e-10) ? 0 : 1;
}
