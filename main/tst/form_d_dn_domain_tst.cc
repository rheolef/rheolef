///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check the computation of the normal derivative
// same as the previous test, but use a domain for Gamma
#include "rheolef.h"
using namespace rheolef;
using namespace std;

// test 1: when Omega=]0,1[^d on domain Gamma={x0=1}
size_t k = 1;
Float u(const point& x) { return pow(x[0],k); }
Float du_dn_exact (const point& x) { return k*pow(x[0],k-1); }

// test 2: when Omega is the unit ball(0,1) in R^d
Float u_ball(const point& x) { return norm(x); }
Float du_dn_exact_ball (const point& x) { return norm(x); }

int main(int argc, char**argv) {
    environment rheolef (argc, argv);
    geo omega (argv[1]);  
    string approx = (argc > 2) ? argv[2] : "P1";
    string dom    = (argc > 3) ? argv[3] : "right";
    Float  tol    = (argc > 4) ? atof(argv[4]) : 1e-10;
    bool is_ball  = (argc > 5 && argv[5] == string("-ball"));
    geo gamma = omega[dom];
    space Xh (omega, approx);
    space Wh (gamma, approx);
    k = Xh.degree();
    string grad_approx = "P" + std::to_string(k-1) + "d";
warning_macro("d_dn...");
    form d_dn (Xh,Xh,"d_dn",gamma);
warning_macro("d_dn done");
#ifdef TO_CLEAN
    dout << d_dn.uu();
#endif // TO_CLEAN
    form ms   (Wh,Wh,"mass");
    solver sms (ms.uu());
    field pi_h_u, pi_h_du_dn;
    if (!is_ball) {
        pi_h_u     = lazy_interpolate (Xh, u);
        pi_h_du_dn = lazy_interpolate (Wh, du_dn_exact);
    } else {
        pi_h_u     = lazy_interpolate (Xh, u_ball);
        pi_h_du_dn = lazy_interpolate (Wh, du_dn_exact_ball);
    }
    field m_duh_dn_wide = d_dn*pi_h_u;
#ifdef TO_CLEAN
    dout << m_duh_dn_wide;
#endif // TO_CLEAN
    field m_duh_dn = m_duh_dn_wide[gamma];
    field duh_dn (Wh);
    duh_dn.set_u() = sms.solve (m_duh_dn.u());
    field eh = duh_dn - pi_h_du_dn;
    Float err = eh.max_abs();
    dout << "err = " << err << endl;
    return (err < tol) ? 0 : 1;
}
