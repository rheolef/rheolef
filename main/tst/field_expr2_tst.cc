///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check field_indirect_const::const_iterator
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float g (const point& x) { return sin(x[0]+x[1]+0.5); }
void ff (const field& gh, const field fh, string dom_name, Float prec) {
  field zh (gh.get_space());
#ifdef TODO
  zh = 0.5*(gh + fh[dom_name]);
#endif // TODO
  zh = fh[dom_name];
  dout << round (zh, prec);
}
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  space Vh (omega, argv[2]);
  string dom_name = (argc > 3) ? argv[3] : "right";
  space Wh (omega[dom_name], argv[2]);
  Float prec = (argc > 4) ? atof(argv[4]) : 1e-10;
  field fh = lazy_interpolate(Vh, g);
  field gh = lazy_interpolate(Wh, g);
  ff (gh, fh, dom_name, prec);
}
