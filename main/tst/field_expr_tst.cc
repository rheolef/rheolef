///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check field_expr<Expr>
#include "rheolef.h"
#include "rheolef/pretty_name.h"
using namespace rheolef;
using namespace std;
struct phi : unary_function<Float,Float> {
  Float operator() (const Float& x) { return 2*x; }
};
// for debug:
template <class Expr>
void trace_is_affine (const Expr& expr) {
  derr << "expr         = " << pretty_typename_macro(Expr) << endl;
  derr << "is_aff.type  = " << pretty_typename_macro(typename Expr::is_affine_homogeneous) << endl;
  derr << "is_aff.value = " << Expr::is_affine_homogeneous::value << endl;
  derr << "is_aff.pred  = " << typename_macro(typename details::is_field_expr_affine_homogeneous<Expr>::type) << endl;
}
int main(int argc, char**argv) {
    environment rheolef(argc, argv);
    geo omega (argv[1]);
    space Vh (omega, "P1");
    field x (Vh, 2.5);
    field y (Vh, 4.0);

#ifdef TO_CLEAN
    // debug an expression
    trace_is_affine(x-1);
#endif // TO_CLEAN

    // set size and then assign:
    field z(Vh); 
    z = 2*x - y;
    dout << "z = 2*x -y : z.dis_ndof = " << z.dis_ndof() << endl;
    z.put (dout);
   
    // size is zero and then assign:
    field z1; 
    z1 = 2*x - y;
    dout << "z1.dis_ndof = " << z1.dis_ndof() << endl;
    z1.put (dout);
    
    // copy cstor:
    field z2 = 2*x - y;
    dout << "z2.dis_ndof = " << z2.dis_ndof() << endl;
    z2.put (dout);

    // dot with expression (lazy evaluation)
    dout << "dot(x, 1) = " << dual(x, 1) << endl;
    dout << "dot(2*x-y, 1) = " << dual(2*x-y, 1) << endl;
    dout << "dot(x, z) = " << dual(x, z) << endl;
    dout << "dot(x, 2*x-y) = " << dual(x, 2*x - y) << endl;
    dout << "dot(2*x-y, x) = " << dual(2*x-y, x) << endl;
    dout << "dot(3*x-y, 2*x-y) = " << dual(3*x-y, 2*x-y) << endl;

    // affine and non-homogeneous: requires interpolate
    space Wh (omega["left"], "P1");
    field x3(Wh, 5);
    field z3 = lazy_interpolate (Wh, 2*x3 - y);
    dout << "z3.dis_ndof = " << z3.dis_ndof() << endl;
    z3.put (dout);

#ifdef MAY_FAIL_AT_RUN_TIME
    // affine but non-homogeneous: it may exit(1) at run-time
    field z4 = 2*x3 - y;
#endif // MAY_FAIL_AT_RUN_TIME

#ifdef MAY_FAIL_AT_COMPILE_TIME
    // non-affine : it may fail at compile-time
    field z4 = x*x - y;
#endif // MAY_FAIL_AT_COMPILE_TIME    
}
