///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// new implementation of field expressions: basic tests

#include "rheolef/integrate.h"
using namespace rheolef;
using namespace std;
Float f0 (const point& x) { return 0.5; }
Float f1 (const point& x) { return x[0]+2*x[1]; }
struct f1c {
  Float operator() (const point& x) const { return f1(x); }
};
point f2 (const point& x) { return x; }
struct f2c {
  point operator() (const point& x) const { return f2(x); }
};
template <class Result1, class Result2>
void check (string expr, Result1 I, Result2 I_ex, Float& err_max) {
  Float err = norm (I-I_ex);
  err_max = max (err, err_max);
  dout << "Expr = " << expr << endl;
  dout << "I    = " << I << endl;
  dout << "I_ex = " << I_ex << endl;
  dout << "err  = " << err << endl;
}
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega(argv[1]);
  string approx = (argc > 2) ?      argv[2]  : "P1";
  Float tol     = (argc > 3) ? atof(argv[3]) : 1e-10;
  size_t d = omega.dimension();
  space Xh  (omega, approx);
  space Xvh (omega, approx, "vector");
  test v (Xh);
  test vv(Xvh);
  quadrature_option qopt;
  qopt.set_family (quadrature_option::gauss);
  qopt.set_order  (2*Xh.degree()-1);
  Float meas_omega = integrate (omega);
  dout << "meas(omega) = " << meas_omega << endl;
  Float err_max = 0;

  // unary expr
  check ("dual(integrate(v),1)", 
          dual(integrate(v),1),
          1.0, err_max);
  check ("dual(integrate(-v),1)", 
          dual(integrate(-v),1),
          -1.0, err_max);
  // unary expr
  check ("dual(integrate(v+v),1)", 
          dual(integrate(v+v),1),
          2.0, err_max);

  // binary mult with nonlin expr
  check ("dual(integrate(f1*v),1)", 
          dual(integrate(f1*v),1),
          1.5, err_max);
  check ("dual(integrate(v*f1c()),1)", 
          dual(integrate(v*f1c()),1),
          1.5, err_max);
  check ("dual(integrate(v/f0),1)", 
          dual(integrate(v/f0),1),
          2., err_max);

  // binary mult with a constant
  check ("dual(integrate(2*v),1)", 
          dual(integrate(2*v),1),
          2., err_max);
  check ("dual(integrate(v/2),1)", 
          dual(integrate(v/2),1),
          0.5, err_max);

  // binary mult with a vect func or constant
  point u0 (0,1,0);
  check ("dual(integrate(dot(vv,f2)),1)", 
          dual(integrate(dot(vv,f2)),1),
          1., err_max);
  check ("dual(integrate(dot(vv,u0)),1)", 
          dual(integrate(dot(vv,u0)),1),
          1., err_max);

  return (err_max < tol) ? 0 : 1;
}
