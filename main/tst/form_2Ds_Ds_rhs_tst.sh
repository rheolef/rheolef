#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
#    solve :
#    u - div_s(2Ds(u)) = f on Gamma
#
#    => seems to converge, but not optimaly
#	since u.n is not controled by the energy norm
#	only L2 cv for the tangential ut=u-(u.n)n component ?
#
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"3"}
DATADIR=$SRCDIR
BINDIR="../bin"
SBINDIR="../sbin"
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

echo "      not yet (skipped)"
exit $status

# geo                   err_P1  err_P2  err_P3
L="
sphere_s-10-fix         4e-2    5e-4    3e-4
sphere_s_q-10-fix       2e-1    2e-4    3e-4    
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2 P3; do
    tol=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_2Ds_Ds_full_tst $DATADIR/${geo}-${Pk} $Pk $tol 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done

exit $status
