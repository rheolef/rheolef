#!geo

mesh
4
header
 dimension 3
 nodes	6
 prisms	1
 triangles	2
 quadrangles	3
 edges	9
end header

0 0 -1
1 0 -1
0 1 -1
0 0 1
1 0 1
0 1 1

P	0 1 2 3 4 5

t	0 2 1
t	3 4 5
q	0 1 4 3
q	1 2 5 4
q	0 3 5 2
e	0 1
e	1 2
e	2 0
e	0 3
e	1 4
e	2 5
e	3 4
e	4 5
e	5 3
