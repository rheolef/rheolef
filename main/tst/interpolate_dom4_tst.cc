///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check interpolate on a bdry space, based on a sub-domain (geo_domain)
// was buggy with mpirun
// here is the tensor-valued case 
// for the scalar- and point- valued case, see interpolate_dom[23]_tst.cc 
//
// usage: prog omega approx domain prec
//
// example:
//   mpirun -np 3 ./interpolate_dom4_tst ../../../rheolef/nfem/ptst/my_cube_TP-5-v2.geo P2 left 1e-8 -expression
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct g {
  tensor operator() (const point& x) const { 
	tensor r = { {-x[0]+x[1]+x[2],
                       x[0]-x[1]+x[2],
                       x[0]+x[1]-x[2]},
	             { x[0]-x[1]+x[2],
                       x[0]+x[1]-x[2],
                      -x[0]+x[1]+x[2]},
	             { x[0]+x[1]-x[2],
                      -x[0]+x[1]+x[2],
                       x[0]-x[1]+x[2]} };
	return (r+trans(r))/2.;
  }
};
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  geo omega (argv[1]);
  string approx   = (argc > 2) ?      argv[2]  : "P1";
  string dom_name = (argc > 3) ?      argv[3]  : "left";
  Float prec      = (argc > 4) ? atof(argv[4]) : 1e-10;
  space Vh (omega, approx, "tensor");
  space Wh (omega[dom_name], approx, "tensor");
  field gh = lazy_interpolate (Wh, g());
  dout  << round (gh, prec);
}
