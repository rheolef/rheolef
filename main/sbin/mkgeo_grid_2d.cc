///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
# include "rheolef/compiler.h"
#include <cstring>
using namespace rheolef;
using namespace std;
// 
// generate a mesh for a square
// ----------------------------
# define TRI 3
# define QUA 4

Float A = 0;
Float B = 1;
Float C = 0;
Float D = 1;
int nx = 10;
int ny = 10;
bool order_ij = true;

# define no(i,j) (order_ij ? (i)*(ny+1)+(j) : (ny-(j))*(nx+1)+(i))

void usage(const char *prog)
{
  cerr << "uniform grid nx*ny for the [a,b]x[c,d] rectangle" << endl
       << "usage: " << endl
       << prog << " "
       << "[-t|-q] [nx="<<nx<<"] [ny=nx] "
       << "[-a float=" << A << "] [-b float=" << B << "] [-c float=" << C << "] [-d float=" << D << "] "
       << "[-[no]boundary] " 
       << "[-[no]sides] " 
       << "[-[no]corner] " 
       << "[-[no]region] " 
       << "[-rz|-zr] " << endl
       << "[-v4] " << endl
       << "[-order-ij] [-order-ji] " 
       << "example:" << endl
       << prog << " -t 10" << endl
       << prog << " -t 10 20" << endl
      ;
  exit (1);
}
void invalid (const char* prog, const char *arg) {
  cerr << prog << ": invalid command line argument `" << arg << "'" << endl;
  usage (prog);
}
int main (int argc, char**argv)
{
    char* prog = argv[0];
    bool sides    = true;
    bool boundary = true;
    bool region   = false;
    bool corner   = false;
    bool v4       = false;
    std::string sys_coord_name = "cartesian";
    if (argc == 1) usage(prog);

    // machine-dependent precision
    int digits10 = numeric_limits<Float>::digits10;
    cout << setprecision(digits10);

    // default triangle
    int type = TRI;
    
    // default nb edge on x axis
    int has_reset_nx = 0;

    int i,j ;

    // parse command line 
    for (i = 1; i < argc; i++) {

    	switch (argv[i][0]) {

        case '-' : {
	         if (strcmp ("-sides", argv[i]) == 0)      { sides = true; }
	    else if (strcmp ("-nosides", argv[i]) == 0)    { sides = false; }
	    else if (strcmp ("-boundary", argv[i]) == 0)   { boundary = true; }
	    else if (strcmp ("-noboundary", argv[i]) == 0) { boundary = false; }
	    else if (strcmp ("-region", argv[i]) == 0)     { region = true; }
	    else if (strcmp ("-noregion", argv[i]) == 0)   { region = false; }
	    else if (strcmp ("-corner", argv[i]) == 0)     { corner = true; }
	    else if (strcmp ("-nocorner", argv[i]) == 0)   { corner = false; }
	    else if (strcmp ("-order-ij", argv[i]) == 0)   { order_ij = true; }
	    else if (strcmp ("-order-ji", argv[i]) == 0)   { order_ij = false; }

            else if (strcmp (argv[i], "-rz") == 0)       { sys_coord_name = "rz"; }
            else if (strcmp (argv[i], "-zr") == 0)       { sys_coord_name = "zr"; }
            else if (strcmp (argv[i], "-v4") == 0)       { v4 = true; }
	    else {
		if (strlen(argv[i]) != 2) { invalid(prog,argv[i]); }
                switch (argv[i][1]) {

	          case 't' : type = TRI; break;
	          case 'q' : type = QUA; break;

		  case 'a' : A = atof (argv[++i]); break;
		  case 'b' : B = atof (argv[++i]); break;
		  case 'c' : C = atof (argv[++i]); break;
		  case 'd' : D = atof (argv[++i]); break;

	          default :  invalid(prog,argv[i]);
	        }
	    }
	    break;
        }
	default :
            if (!isdigit(argv[i][0])) { invalid(prog,argv[i]); }
	    if (has_reset_nx) {
		ny = atoi(argv[i]);
	    } else {
		nx = ny = atoi(argv[i]);
		has_reset_nx = 1;
	    }
	    break;
	}
    }
    if (A >= B || C >= D) {
        cerr << prog << ": [a,b]x[c,d] may be non-empty." << endl;
        exit (1);
    }
    if (region && nx % 2 != 0) {
        cerr << prog << ": region: nx may be an even number." << endl;
        exit (1);
    }
    if (type == TRI) cerr << "triangular" ; else cerr << "quadrangular" ;
    cerr << " mesh " << nx << " x " << ny << endl; 
    
    int n_vert = (nx+1)*(ny+1);
    int n_elt;
    int n_edg;
    if (type == TRI) {
	n_elt = 2*nx*ny;
	n_edg = 3*nx*ny+nx+ny; // = (nx+1)*ny + (ny+1)*nx + nx*ny
    } else {
	n_elt = nx*ny;
	n_edg = 2*nx*ny+nx+ny; // = (nx+1)*ny + (ny+1)*nx
    }
    // header
    size_t version = ((sys_coord_name != "cartesian") ? 3 : 2);
    cout << "#!geo" << endl
         << "mesh" << endl;
    if (!v4) {
      cout << version << " 2 " << n_vert << " " << n_elt << " " << n_edg << endl;
      if (sys_coord_name != "cartesian") {
        cout << "coordinate_system " << sys_coord_name << endl;
      }
    } else {
      cout << "4" << endl
           << "header" << endl
           << " dimension 2" << endl;
      if (sys_coord_name != "cartesian") {
        cout << "coordinate_system " << sys_coord_name << endl;
      }
      cout << " nodes     " << n_vert << endl;
      if (type == TRI) {
        cout << " triangles " << n_elt << endl;
      } else {
        cout << " quadrangles " << n_elt << endl;
      }
      cout << " edges     " << n_edg << endl
           << "end header" << endl;
    }
    cout << endl;
    
    // geometry
    if (order_ij) {
      for(i = 0 ; i <= nx ; i++ )
        for(j = 0 ; j <= ny ; j++ )
	  cout << Float((B-A)*i)/Float(nx)+A << " " << Float((D-C)*j)/Float(ny)+C << endl ;
    } else {
      for(j = ny ; j >= 0 ; j-- )
        for(i = 0 ; i <= nx ; i++ )
	  cout << Float((B-A)*i)/Float(nx)+A << " " << Float((D-C)*j)/Float(ny)+C << endl ;
    }
    cout << endl ;

    // connectivity
    //    elements
    for (i = 0; i < nx; i++) {
      for (j = 0; j < ny; j++) {
	if (type == TRI)
	  cout << "t\t"   << no(i,j) << " " << no(i+1,j)   << " " << no(i+1,j+1) 
	       << "\nt\t" << no(i,j) << " " << no(i+1,j+1) << " " << no(i,j+1) << endl ;
	else
	  cout << "q\t" << no(i,j)     << " " << no(i+1,j) << " " 
	       <<          no(i+1,j+1) << " " << no(i,j+1) << endl ;
	}
    }
    cout << endl ;
    //   edges
    //      vertical
    for( int i_ve=0 ; i_ve < nx+1 ; i_ve++ )
      for( int j_ve=0 ; j_ve < ny ; j_ve++ )
	cout << no(i_ve, j_ve) << " " << no(i_ve, j_ve+1) << endl ;
    //      horizontal
    for( int i_he=0 ; i_he < nx ; i_he++ )
      for( int j_he=0 ; j_he < ny+1 ; j_he++ )
	cout << no(i_he, j_he) << " " << no(i_he+1, j_he) << endl ;
    //      diagonal (TRI only)
    if (type==TRI)
      for ( int i_elt = 0 ; i_elt < nx ; i_elt++ )
	for ( int j_elt = 0 ; j_elt < ny ; j_elt++ )
	  cout << no(i_elt, j_elt) <<  " " << no(i_elt+1, j_elt+1) << endl ;
    cout << endl ;

    // domains = 4 sides
    
    if (boundary) {
      cout << "domain\nboundary\n1 1 " << 2*(nx+ny) << endl ;
      for (i = 0; i < nx; i++)
        cout << no(i,0) << " " << no(i+1,0) << endl ;
      for (j = 0; j < ny; j++)
        cout << no(nx,j) << " " << no(nx,j+1) << endl ;
      for (i = 0; i < nx; i++)
        cout << no(i+1,ny) << " " << no(i,ny) << endl ;
      for (j = 0; j < ny; j++)
        cout << no(0,j+1) << " " << no(0,j) << endl ;
      cout << endl ;
    }
    if (sides) {
      cout << "domain\nbottom\n1 1 " << nx << endl ;
      for (i = 0; i < nx; i++)
        cout << no(i,0) << " " << no(i+1,0) << endl ;
      cout << endl ;
      cout << "domain\nright\n1 1 " << ny << endl ;
      for (j = 0; j < ny; j++)
        cout << no(nx,j) << " " << no(nx,j+1) << endl ;
      cout << endl ;
      cout << "domain\ntop\n1 1 " << nx << endl ;
      for (i = 0; i < nx; i++)
        cout << no(i+1,ny) << " " << no(i,ny) << endl ;
      cout << endl ;
      cout << "domain\nleft\n1 1 " << ny << endl ;
      for (j = 0; j < ny; j++)
        cout << no(0,j+1) << " " << no(0,j) << endl ;
      cout << endl ;
    }
    if (region) {
	    int n_face = (type == TRI) ? nx*ny : nx*ny/2;
	    cout << "\ndomain\nwest\n1 2 " << n_face << endl ;
	    for (i = 0; i < nx/2; i++) {
	      for (j = 0; j < ny; j++) {
		if (type == TRI)
		  cout << "t\t"   << no(i,j) << " " << no(i+1,j)   << " " << no(i+1,j+1) 
		       << "\nt\t" << no(i,j) << " " << no(i+1,j+1) << " " << no(i,j+1) << endl ;
		else
		  cout << "q\t" << no(i,j)     << " " << no(i+1,j) << " " 
		       <<          no(i+1,j+1) << " " << no(i,j+1) << endl ;
		}
	    }
	    cout << endl
	         << "\ndomain\neast\n1 2 " << n_face << endl ;
	    for (i = nx/2; i < nx; i++) {
	      for (j = 0; j < ny; j++) {
		if (type == TRI)
		  cout << "t\t"   << no(i,j) << " " << no(i+1,j)   << " " << no(i+1,j+1) 
		       << "\nt\t" << no(i,j) << " " << no(i+1,j+1) << " " << no(i,j+1) << endl ;
		else
		  cout << "q\t" << no(i,j)     << " " << no(i+1,j) << " " 
		       <<          no(i+1,j+1) << " " << no(i,j+1) << endl ;
		}
	    }
	    cout << endl
                 << "domain\ninterface\n1 1 " << ny << endl;
            for (j = 0; j < ny; j++)
              cout << no(nx/2,j) << " " << no(nx/2,j+1) << endl;
            cout << endl ;
	    cout << "domain\ninterface2\n1 1 " << ny << endl;
            for (j = 0; j < ny; j++)
              cout << no(nx/2,j+1) << " " << no(nx/2,j) << endl;
    }
    // corners = 4 0d-domains
    if (corner) {
      	    cout << "\ndomain\nleft_bottom\n1 0 1\n"  << no( 0, 0) << endl
                 << "\ndomain\nright_bottom\n1 0 1\n" << no(nx, 0) << endl
                 << "\ndomain\nright_top\n1 0 1\n"    << no(nx,ny) << endl
                 << "\ndomain\nleft_top\n1 0 1\n"     << no(0, ny) << endl
		 << endl;
    }
}
