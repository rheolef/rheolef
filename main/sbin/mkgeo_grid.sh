#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
# the mkgeo_grid unix command
# author: Pierre.Saramito@imag.fr
# date: 2 february 2004

##
#@commandfile mkgeo_grid structured mesh of a parallelepiped
#@addindex command `mkgeo_grid`
#@addindex command `geo`
#@addindex mesh
#@addindex file format `.geo` mesh
#
#Synopsis
#--------
#
#    mkgeo_grid [options] [nx [ny [nz]]]
#
#Examples
#--------
#The following command build a triangular based 2d 10x10 grid
#of the unit square:
#
#        mkgeo_grid -t 10 > square-10.geo
#        geo square-10.geo
#
#or in one command line:
#
#        mkgeo_grid -t 10 | geo -
#
#Description
#-----------
#This command is useful when testing programs on simple geometries.
#It avoid the preparation of an input file for a mesh generator.
#The optional nx, ny and nz arguments are integer
#that specifies the subdivision in each direction. By default 
#nx=10, ny=nx and nz=ny.
#The mesh files goes on standard output.
#
#The command supports all the possible element types: edges, triangles,
#rectangles, tetrahedra, prisms and hexahedra.
#
#Element type option
#-------------------
#
#`-e`
#>	1d mesh using edges.
#`-t`
#>	2d mesh using triangles.
#`-q`
#>	2d mesh using quadrangles (rectangles).
#`-T`
#>	3d mesh using tetrahedra.
#`-P`
#>	3d mesh using prisms.
#`-H`
#>	3d mesh using hexahedra.
#
#The geometry
#------------
#`-a` *float* \n
#`-b` *float* \n
#`-c` *float* \n
#`-d` *float* \n
#`-f` *float* \n
#`-g` *float*
#>	The geometry can be any [a,b] segment, [a,b]x[c,d] rectangle
#>	or [a,b]x[c,d]x[f,g] parallelepiped. By default a=c=f=0 and b=d=g=1, thus,
#>	the unit boxes are considered.
#
#For instance, the following command meshes the [-2,2]x[-1.5, 1.5] rectangle:
#
#        mkgeo_grid -t 10 -a -2 -b 2 -c -1.5 -d 1.5 | geo -
#
#Boundary domains
#----------------
#`-[no]sides`
#>	The boundary sides are represented by domains: `left`, `right`,
#>	`top`, `bottom`, `front` and `back`.
#`-[no]boundary`
#>	This option defines a domain named `boundary` that groups all sides.
#
#By default, both sides and the whole boundary are defined as domains:
#
#        mkgeo_grid -t 10 -nosides | geo -
#        mkgeo_grid -t 10 -noboundary | geo -
#        mkgeo_grid -t 10 -noboundary -nosides | geo -
#
#Regions
#-------
#`-[no]region`
#>	The whole domain is split into two subdomains: `east` and `west`.
#>	Also, the separating domain is named `interface` in the mesh.
#>	This option is used for testing computations with subdomains (e.g. transmission
#>	problem; see @ref usersguide_page ).
#
#Example:
#
#    	mkgeo_grid -t 10 -region | geo -
#
#Corners
#-------
#`-[no]corner`
#>	The corners (four in 2D and eight in 3D) are defined as OD-domains.
#>	This could be useful for some special boundary conditions.
#
#Example:
#
#        mkgeo_grid -t 10 -corner | geo -
#        mkgeo_grid -T  5 -corner | geo -
#
#Coordinate system options
#-------------------------
#@addindex axisymmetric coordinate system
#
#`-cartesian` \n
#`-rz` \n
#`-zr`
#>       Specifies the coordinate system.
#>       The default is `cartesian` while `-rz`
#>       and `-zr` denotes some axisymmetric coordinate systems.
#>       Recall that most of Rheolef codes are coordinate-system independent
#>       and the coordinate system is specified in the geometry file `.geo`.
#
#Implementation
#--------------
#@showfromfile

usage="mkgeo_grid
	[-{abcdfg} float]
	[-{eptqTPH}]
	[nx [ny nz]]]
	[-[no]sides]
	[-[no]boundary]
	[-[no]region]
	[-[no]corner]
	[-rz|-zr]
	[-v4]
"

if test $# -eq 0; then
  echo ${usage} >&2
  exit 0
fi

args=""
dim="2d"
sys_coord="cartesian"
new_format=`rheolef-config --have-new-code 2>/dev/null`
GEO_BIN=${GEO_BIN-"geo"}
while test $# -ne 0; do
  case $1 in
  -h) echo ${usage} >&2; exit 0;;
  -e)       dim="1d"; args="$args $1";;
  -[tq])    dim="2d"; args="$args $1";;
  -[TPH])   dim="3d"; args="$args $1";;
  [0-9]*)    args="$args $1";;
  -[abcdfg]) args="$args $1 $2"; shift;;
  -rz)       args="$args $1"; sys_coord="rz";;
  -zr)       args="$args $1"; sys_coord="zr";;
  -sides | -boundary | -region | -corner | -nosides | -noboundary | -noregion | -nocorner)
	     args="$args $1";;
  *) echo "mkgeo_grid: unexpected argument $1" 1>&2
     echo ${usage} 1>&2; exit 1;;
  esac
  shift
done
if test ${new_format} = true; then
  args="$args -v4"
fi
#echo "! GEO_BIN=${GEO_BIN}" >&2
#echo "! new_format=${new_format}" >&2
pkgbindir=`rheolef-config --pkglibdir`

if test $sys_coord != "cartesian" -a $dim != 2d; then
  echo "mkgeo_grid: incompatible $dim geometry and non-cartesian \"${sys_coord}\"coordinate system" >&2
  exit 1
fi

command="$pkgbindir/mkgeo_grid_$dim $args 2>/dev/null"
if test $dim = 3d || test ${new_format} = true; then
  command="$command | ${GEO_BIN} -upgrade -geo - 2>/dev/null"
fi
#echo "! $command" >&2
eval $command
status=$?
if test $status -ne 0; then
  echo "$0: command failed" 1>&2
  exit $status
fi
