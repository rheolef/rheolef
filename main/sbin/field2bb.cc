///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// Passage du format field au format bb de Bamg 
//
// author: Pierre.Saramito@imag.fr
//
// date: 03/09/1997 ; 04/01/2018
//
/*Prog:field2bb
NAME: @code{field2bb} - convert from .field file format to bamg .bb one
@pindex field2bb
@fiindex @file{.bb} bamg field 
@fiindex @file{.field} field
@toindex @code{bamg}
SYNOPSIS:
@example
  field2bb < @var{input}.field > @var{output}.bb
@end example

DESCRIPTION:
  Convert a @file{.field} file into a bamg @file{.bb} one.
  The output goes to standart output.
  This command is useful for mesh adaptation with @code{bamg}.

LIMITATION:
  Do not support yet tensor valued .field files.
  This could be interessant for specifying directly
  a metric for mesh adaptation.
End:*/
#include "scatch.icc"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <limits>
using namespace std;
using namespace rheolef;

int main()
{
	// Lecture du fichier field
	//-------------------------
	scatch (cin, "field", true);
	string ch1;
	cin >> ch1;// version
	size_t version = atoi(ch1.c_str());
        if (version != 1) {
	  cerr << "field2bb: version " << version << " field format not yet supported" << endl;
          exit (1);
        }
	cin >> ch1;
	size_t nbpts = atoi(ch1.c_str());
	cin >> ch1  // mesh name 
            >> ch1; // approx
        if (ch1 != "P1") {
	  cerr << "field2bb: approx " << ch1 << " field not yet supported" << endl;
          exit (1);
        }
	vector<double> u (nbpts);
	for (size_t i=0; i<nbpts; i++) {
	  cin >> ch1;
          u[i] = atof(ch1.c_str());
	}
	// Ecriture sur le fichier bb
	//---------------------------
        cout << setprecision(numeric_limits<double>::digits10)
             << "2 1 " << nbpts << " 2" << endl;
	for (size_t i=0; i<nbpts; i++) {
		cout << u[i] << endl; 
        }
}
