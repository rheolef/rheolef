##
## This file is part of Rheolef.
##
## Copyright (C) 2000-2009 Pierre Saramito 
##
## Rheolef is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## Rheolef is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Rheolef; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##
# ==========================================================================
# 1. Utilities
# ==========================================================================
# TODO : xmin,xmax, fmin,fmax a partir de data
# TODO : gl2ps : show.InterpolateScalarsBeforeMapping = 0 sinon blanc
# --------------------------------------------------------------------------
# 1.0. queries in the data
# --------------------------------------------------------------------------
def get_approx (data):
    if data.GetCellDataInformation().GetNumberOfArrays() > 0:
      return "P0"
    elif data.GetPointDataInformation().GetNumberOfArrays() > 0:
      return "P1"
    else:
      print("error(paraview_rheolef.py): no data founded")
      exit(1)

# --------------------------------------------------------------------------
# 1.1. isovalues
# --------------------------------------------------------------------------
def build_isovalue_list_negative (option):

    f_min = option['range'][0]
    f_max = option['range'][1]
    nn = option['n_isovalue_negative']
    np = option['n_isovalue'] - nn
    values = [f_min, f_min+1e-15]
    for i in range(1, nn):
      v = f_min - (1.0*i)/(1.0*nn)*f_min
      values = values + [v]
    #values = values + [0]
    values = values + [-1e-15, 0, 1e-15] # add machine precision zero isolines
    for i in range(1, np):
      v = (1.0*i)/(1.0*np)*f_max
      values = values + [v]
    values = values + [f_max-1e-15,f_max]
    return values

def build_isovalue_list (option):
    if option['n_isovalue_negative'] != 0:
       return build_isovalue_list_negative(option)

    f_min = option['range'][0]
    f_max = option['range'][1]
    f_iso = option['isovalue']
    if (f_min < f_iso and f_iso < f_max):
      if option['color'] == 'black_and_white':
        iso_value = list(range(0,1))
        iso_value[0] = f_iso
      else:
        iso_value = list(range(0,3))
        delta = max(f_max-f_iso, f_iso-f_min)
        iso_value[0] = f_iso - delta
        iso_value[1] = f_iso
        iso_value[2] = f_iso + delta
    else:
      n_iso = option['n_isovalue']
      iso_value = list(range(0,n_iso+1))
      iso_value[0] = f_min
      for i in range(1,n_iso):
        iso_value[i] = f_min + 1.*i*(f_max-f_min)/n_iso
      iso_value[n_iso] = f_max
    return iso_value

# --------------------------------------------------------------------------
# 1.2. color bars
# --------------------------------------------------------------------------
def make_default_color_bar (option):
    # used by geo visu (without fmin,fmax data)
    from paraview.simple import GetLookupTableForArray, CreateScalarBar
    color_opa = make_color_opa (option)
    color_map = GetLookupTableForArray("mesh", 1, RGBPoints=[0, 0, 0, 1, 0, 1, 0, 0], VectorMode='Magnitude', NanColor=[0.498039215686275, 0.498039215686275, 0.498039215686275], ColorSpace='HSV', ScalarRangeInitialized=1)
    color_map.RGBPoints   = [0, 0, 0, 0, 0, 0, 0, 1]
    try:    color_map.ScalarOpacityFunction = color_opa
    except: 0 # paraview-3 style:
    try:    color_map.Annotations = ['0', '0']
    except: 0 # paraview-3 style:
    f_min = option['range'][0]
    f_max = option['range'][1]
    color_map.RescaleTransferFunction(f_min, f_max)
    color_bar = CreateScalarBar(Title='mesh', Enabled=1, LabelFontSize=12, LabelColor=[0, 0, 0], LookupTable=color_map, TitleFontSize=12, TitleColor=[0, 0, 0])
    try:    color_bar.HorizontalTitle = 1
    except: 0; # old paraview
    return color_bar

def get_color_opa (color_map, option):
    try:    color_opa = color_map.GetProperty("ScalarOpacityFunction").GetData() # paraview-4 style:
    except: color_opa = make_color_opa (option)                                  # paraview-3 style:
    return color_opa 

def make_color_opa (option):
    from paraview.simple import CreatePiecewiseFunction
    # how to get data range automatically from data ?
    f_min = option['range'][0]
    f_max = option['range'][1]
    #print('f_min=',f_min,' f_max=',f_max)
    color_opa = CreatePiecewiseFunction()
    if not option['volume']:
      color_opa.Points = [f_min, 1, 0.5, 0, \
                          f_max, 1, 0.5, 0]
    else:
      df = f_max-f_min
      color_opa.Points = [f_min,             0.0, 0.5, 0, \
                          f_min + 0.60*df,   .2, 0.5, 0, 
                          f_min + 0.85*df,   0.8, 0.5, 0, 
                          f_max,             1.0, 0.5, 0]
    color_opa.RescaleTransferFunction(f_min, f_max)
    #print('color_opa.Points=',color_opa.Points)
    return color_opa

def make_color_bar (option):
    from paraview.simple import GetLookupTableForArray, CreateScalarBar
    # how to get data range automatically from data ?
    f_min = option['range'][0]
    f_max = option['range'][1]
    if option['label'] != '':
      name = option['label']
    else:
      name = option['mark']
    n_iso = option['n_isovalue']

    color_opa = make_color_opa (option)

    color_map = GetLookupTableForArray(name, 1)
    color_map.NumberOfTableValues = n_iso
    color_map.ScalarRangeInitialized=1
    color_map.VectorMode='Magnitude'
    color_map.ColorSpace='Diverging'
    if option['color'] == 'color': 
      color_map.ColorSpace = 'HSV'
      color_map.RGBPoints  = [f_min, 0, 0, 1, \
                              f_max, 1, 0, 0]
      color_map.NanColor   = [0.5, 0.5, 0.5]
    else:
      color_map.ColorSpace = 'RGB'
      color_map.RGBPoints  = [f_min, 0, 0, 0, \
                              f_max, 1, 1, 1]
      color_map.NanColor   = [1, 0, 0]
    try: color_map.ScalarOpacityFunction = color_opa # paraview-4
    except: 0; # paraview-3
    color_bar = CreateScalarBar(Title=name, Enabled=1, LabelFontSize=12, LabelColor=[0, 0, 0], LookupTable=color_map, TitleFontSize=12, TitleColor=[0, 0, 0])
    color_bar.ComponentTitle = "";
    try:    color_bar.HorizontalTitle = 1
    except: 0; # old paraview
    try:    color_bar.WindowLocation = 'LowerLeftCorner'
    except: 0; # old paraview
    return color_bar

# --------------------------------------------------------------------------
# 1.3. final render : graphic window or on file
# --------------------------------------------------------------------------
def do_final_render(paraview,basename,option):
    from paraview.simple import GetRenderView, Render, WriteImage, GetActiveView, \
                                SaveScreenshot, SaveAnimation, GetAnimationScene, GetTimeKeeper

    render = GetRenderView()
    render.Background = [1, 1, 1]
    if basename == '':
      basename = "output"
    Render() # the final render is here: do not clean!
    file_format = option['format']
    if file_format == '':
      if option.get('branch_size') != None and option['branch_size'] > 1:
        anim = GetAnimationScene()
        anim.PlayMode = 'Real Time'
        anim.EndTime = option['branch_size']        # nb of frames
        anim.Duration = 25			    # TODO: as a command line parameter
        #anim.NumberOfFrames = option['branch_size'] # nb of frames
        #anim.EndTime        = 25.0                # duration in seconds
      return
    have_recognized_format = 0
    import os.path
    from os import remove
    from os.path import exists
    basename = os.path.basename(basename)
    filename = basename+"."+file_format
    resolution = option.get('resolution')
    if resolution == None: 
      resolution = [1024, 768]
    if exists(filename):
      remove(filename)
      if file_format == 'pdf' and exists(filename+".gz"):
        remove(filename+".gz")

    n_picture = 100
    if   file_format == 'mp4' or file_format == 'avi':
      # note: SaveAnimation is buggy, so use image-per-image + ffmpeg
      anim = GetAnimationScene()
      time_keeper = GetTimeKeeper()
      imgfmt="png"
      to_clean = ""
      for i in range(0,n_picture+1):
        anim.AnimationTime = i
        time_keeper.Time   = i
        os.write(2,b"[%d]"%i) # write to 2=stderr
	# see paraView-xyz/Wrapping/Python/paraview/simple.py :
        if imgfmt == "jpg":
          SaveScreenshot("%s.%04d.%s"%(basename,i,imgfmt),
            render,
            ImageResolution=resolution,
            # jpg options
            Quality=95,
            Progressive=1)
          # OverrideColorPalette='',
          # StereoMode='No change',
          # TransparentBackground=0,
        else:
          SaveScreenshot("%s.%04d.%s"%(basename,i,imgfmt),
            render,
            ImageResolution=resolution)
            # png options: not supported by old paraview-5.4:
            #CompressionLevel='5')
        filename_i = "%s.%04d.%s"%(basename,i,imgfmt)
        to_clean = to_clean + " " + filename_i
 
      os.write(2,b"\n") # write to 2=stderr
      command="rm -f %s.%s"%(basename,file_format)
      print("! " + command)
      status = os.system (command)
      opts = ["-i \"%s.%%04d.%s\""%(basename,imgfmt),
              "-loglevel fatal",
              "-framerate 25",
              "-c:v libx264",
              "-crf 20"]
      if imgfmt == "jpg":
        opts = opts + ["-profile:v high"]
      opts = opts + ["%s.%s"%(basename,file_format)]
      command="ffmpeg " + " ".join(opts)
      print("! " + command)
      import os
      status = os.system (command)
      if status != 0:
        print("ffmpeg failed")
        exit (status)
      command="rm -f %s"%(to_clean)
      print("! " + command)
      status = os.system (command)
      have_recognized_format = 1
    elif file_format == 'png' or \
         file_format == 'bmp' or \
         file_format == 'ppm' or \
         file_format == 'tif' or \
         file_format == 'jpg':
      # png, bmp, ppm, tif, tiff, jpg and jpeg
      # doc: http://www.paraview.org/ParaView3/Doc/Nightly/www/py-doc/paraview.simple.html#paraview.simple.WriteAnimation
      have_recognized_format = 1
      GetRenderView().ViewSize = resolution
      Render()
      WriteImage(filename, render)
    elif file_format == 'vrml':
      # http://www.paraview.org/pipermail/paraview/2010-June/017989.html
      have_recognized_format = 1
      exporters = paraview.servermanager.createModule("exporters")
      vrml = exporters.VRMLExporter()
      vrml.FileName = filename
      vrml.SetView(render)
      vrml.Write()
    elif file_format == 'pdf' or \
         file_format == 'ps'  or \
         file_format == 'eps' or \
         file_format == 'svg':
      # gl2ps: pdf, ps, eps, svg
      # http://www.paraview.org/pipermail/paraview/2010-June/017937.html
      have_recognized_format = 1
      use_tvtk = 0
      if not use_tvtk:
        # -----------------------
	# use raw vtk class
        # -----------------------
        try:
          from vtkRenderingPython import vtkGL2PSExporter # paraview 3
          paraview_version="3.x"
        except:
          try:
            from vtk.vtkRenderingPython import vtkGL2PSExporter # paraview 4.0 : segfault sometimes...
            paraview_version="4.0"
          except:
            try:
              from vtk.vtkIOExportPython import vtkGL2PSExporter # paraview 4.1
              paraview_version="4.1"
            except:
              paraview_version="x.x"
              print("warning(paraview_rheolef.py): vector graphic output: module gl2ps not found (HINT: check paraview installation)")
              #exit(1)

        render.CameraViewUp = [0,-1, 0] # view is 180 deg rotated with pvbatch, not with pvpython and paraview...
        Render()
        view = render.GetRenderWindow()
        gl2ps = vtkGL2PSExporter()
        #help(gl2ps.__class__)
        gl2ps.SetRenderWindow(view)
        gl2ps.SetFilePrefix(basename)
        file_format2code = {'ps': 0, 'eps': 1, 'pdf': 2, 'tex': 3, 'svg': 4}
        if file_format.lower() in file_format2code:
          gl2ps.SetFileFormat(file_format2code[file_format.lower()])
        else:
          print("error(paraview_rheolef.py): invalid file format ",file_format)
          exit(1)
        if file_format.lower() == "pdf":
          gl2ps.SetCompress(True) # abble to compress inside pdf
        else:
          gl2ps.SetCompress(False)
        gl2ps.SetLandscape(False);
        gl2ps.SetSortToBSP()
        gl2ps.DrawBackgroundOn();
        gl2ps.SetWrite3DPropsAsRasterImage(False) # rasterize, as png
        gl2ps.BestRootOn()
        gl2ps.OcclusionCullOn(); # otherwise no fill
        gl2ps.PS3ShadingOff();
        gl2ps.SilentOff()
        gl2ps.SimpleLineOffsetOn();
        gl2ps.TextOn()
        try:    gl2ps.TextAsPathOn(); # write text as path (paraview-4 style)
        except: 0
        gl2ps.Write()
      else: 
        # --------------------------------
	# use tktk (experimental)
        # --------------------------------
        print("saving with tvtk...")
        from tvtk.api import tvtk
        gl2ps = tvtk.GL2PSExporter(file_format='eps', sort='bsp', compress=0)
        gl2ps.file_prefix = basename
        gl2ps.render_window = render.GetRenderWindow()
        if file_format.lower() == "pdf":
          gl2ps.compress = 1
        #open a menu to set options:
        #gl2ps.edit_traits(kind='livemodal')
        gl2ps.write()
    if have_recognized_format == 0:
      print("error(paraview_rheolef.py): unrecognized image format %s"%(format))
      exit(1)
    if format == "pdf" and exists(filename+".gz"):
      import os
      command="mv %s.gz %s"%(filename,filename)
      os.system (command)
    if exists(filename):
      print("! output written to \"%s\""%(filename))
    else:
      print("! failed to create \"%s\""%(filename))
      exit(1)

def do_render(paraview, name, option):
    from paraview.simple import GetRenderView
    x_min = option['bbox'][0][0]
    x_max = option['bbox'][1][0]
    y_min = option['bbox'][0][1]
    y_max = option['bbox'][1][1]
    z_min = option['bbox'][0][2]
    z_max = option['bbox'][1][2]
    if option.get('elevation') != 1: 
      xg = [(x_max+x_min)/2., (y_max+y_min)/2., (z_max+z_min)/2.]
      dx = [x_max-x_min, y_max-y_min, z_max-z_min]
    else:
      f_min = option['range'][0]
      f_max = option['range'][1]
      z_scale = max(x_max-x_min,y_max-y_min)/(f_max-f_min)
      xg = [(x_max+x_min)/2., (y_max+y_min)/2., z_scale*(f_max+f_min)/2.]
      dx = [x_max-x_min, y_max-y_min, z_scale*(f_max-f_min)]

    render = GetRenderView()
    render.Background = [1, 1, 1]
    render.CenterAxesVisibility = 0
    render.CenterOfRotation    = xg
    render.CameraFocalPoint    = xg
    if option['view_2d'] == 1 and option['elevation'] == 0: 
      render.InteractionMode     = '2D'
      render.OrientationAxesVisibility = 0
      from math import sqrt
      render.CameraParallelScale = max(dx[0],dx[1])/sqrt(2.0)
      z_camera = 1+sqrt(3)
      render.CameraPosition      = [xg[0], xg[1], z_camera]
     #render.add_attribute("CameraClippingRange", [0.99*z_camera, 1.015*z_camera]);
      render.CameraViewUp        = [0, 1, 0]
      render.CameraViewAngle     = 30
    else:
      render.InteractionMode     = '3D'
      if option['showlabel'] == 0:
        render.OrientationAxesVisibility = 0
      render.CameraPosition      = [xg[0]+3*dx[0], xg[1]+1.2*dx[1], xg[2]+0.4*dx[2]]
      render.CameraViewUp        = [0, 0, 1]

    from os.path import basename
    do_final_render(paraview, basename(name), option)

# for high-order geo or field: adaptive P1-iso-Pk, by subdividing inside each cell
def get_high_order_data (data, option):
    use_high_order = 0
    if option['high_order']:
      try:
        from paraview.simple import Tessellate
        use_high_order = 1; # have paraview >= 5.5
      except: 0;
    if not use_high_order:
      return data
    tessellate = Tessellate(Input=data)
    tessellate.FieldError = [1e-3]; # error vs the Pk field interpolation
    tessellate.ChordError =  1e-7 ; # error vs the Pk geometry
    tessellate.MaximumNumberofSubdivisions = 3
    tessellate.MergePoints = 1
    if option['view_2d'] == 1: 
      tessellate.OutputDimension = 2
    else:
      tessellate.OutputDimension = 3
    return tessellate

# ==========================================================================
# 2. scalar field
# ==========================================================================
# --------------------------------------------------------------------------
# 2.1. field: 2d scalar color or grayscale
# --------------------------------------------------------------------------

def paraview_branch_2d_scalar_color (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, GetRenderView, 	\
         GetLookupTableForArray, Show,		\
	 CreateScalarBar, GetColorTransferFunction

    paraview.simple._DisableFirstRenderCameraReset()
    n_iso = option['n_isovalue']

    data = LegacyVTKReader(FileNames=filelist)
    data = get_high_order_data (data, option)

    color_bar = make_color_bar (option)
    color_map = color_bar.GetProperty("LookupTable").GetData()
    f_min = option['range'][0]
    f_max = option['range'][1]
    f_iso = option['isovalue']
    # TODO: test on f_min and f_max, that are not always reliable with branch: any idea ?
    if (f_min < f_iso and f_iso < f_max):
      delta = max(f_max-f_iso, f_iso-f_min)
      color_map.RescaleTransferFunction(f_iso - delta, f_iso + delta)
    color_opa = get_color_opa (color_map, option)

    
    iso_contour = Show()
    if option['format'] != '':
      iso_contour.InterpolateScalarsBeforeMapping = 0 # bug gl2ps & paraview
    iso_contour.SelectionPointFieldDataArrayName = option['mark']
    iso_contour.ColorArrayName = option['mark']
    if option['fill'] == 1:
      iso_contour.Representation = 'Surface'
    else:
      iso_contour.Representation = 'Wireframe'
    iso_contour.ScaleFactor   = 1
    iso_contour.ScalarOpacityFunction = color_opa
    iso_contour.LookupTable           = color_map
    iso_contour.AmbientColor         = [0, 0, 0]
   #iso_contour.CubeAxesColor        = [0, 0, 0]
    iso_contour.EdgeColor            = [0, 0, 0.5]
    iso_contour.BackfaceDiffuseColor = [0.3333333333333333, 0.6666666666666666, 1]
    iso_contour.DiffuseColor         = [0.3333333333333333, 0.6666666666666666, 1]
    iso_contour.ScalarOpacityUnitDistance = 0.09596915518332425

    if option['showlabel'] == 1:
      render = GetRenderView()
      render.Representations.append(color_bar)
    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 2.2. branch: 2d scalar black-and-white
# --------------------------------------------------------------------------

def paraview_branch_2d_scalar_bw (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, GetRenderView, 	\
         Show, GetLookupTableForArray, \
	 Outline, SetActiveSource, Contour, Render

    paraview.simple._DisableFirstRenderCameraReset()
    
    data = LegacyVTKReader(FileNames=filelist)
    data = get_high_order_data (data, option)

    if option.get('outline') == None or option.get('outline') == 1:
      outline = Outline()
      outline_show = Show()
      outline_show.ColorArrayName = ''
      outline_show.SelectionPointFieldDataArrayName = option['mark']
      outline_show.DiffuseColor = [0, 0, 0]

    SetActiveSource(data)
    iso_lines = Contour()
    iso_lines.PointMergeMethod = "Uniform Binning"
    iso_lines.ContourBy        = option['mark']
    iso_lines.Isosurfaces      = build_isovalue_list(option)
    iso_show = Show()
    iso_show.SelectionPointFieldDataArrayName = option['mark']
    iso_show.ColorArrayName = ('POINT_DATA', '')
    iso_show.DiffuseColor         = [0, 0, 0]

    do_render(paraview, basename, option)

def paraview_field_2d_scalar_bw (paraview, basename, option):
    paraview_branch_2d_scalar_bw (paraview, basename, [basename+".vtk"], option)

# --------------------------------------------------------------------------
# 2.2.old: field: 2d scalar black-and-white
# --------------------------------------------------------------------------

def old_paraview_branch_2d_scalar_bw (paraview, basename, fielist, option):
    from paraview.simple import LegacyVTKReader, GetRenderView, 	\
         Show, GetLookupTableForArray, \
	 Outline, SetActiveSource, Contour, Render

    paraview.simple._DisableFirstRenderCameraReset()
    
    data = LegacyVTKReader(FileNames=filelist)
    data = get_high_order_data (data, option)

    if option.get('outline') == None or option.get('outline') == 1:
      outline = Outline()
      outline_show = Show()
      outline_show.ColorArrayName = ''
      outline_show.SelectionPointFieldDataArrayName = option['mark']
      outline_show.DiffuseColor = [0, 0, 0]

    SetActiveSource(data)
    iso_lines = Contour()
    iso_lines.PointMergeMethod = "Uniform Binning"
    iso_lines.ContourBy        = option['mark']
    iso_lines.Isosurfaces      = build_isovalue_list(option)
    iso_show = Show()
    iso_show.SelectionPointFieldDataArrayName = option['mark']
    iso_show.ColorArrayName = ('POINT_DATA', '')
    iso_show.DiffuseColor         = [0, 0, 0]

    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 2.3. field: 2d scalar elevation
# --------------------------------------------------------------------------

def paraview_branch_2d_scalar_elevation (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, GetRenderView, 	\
         Show, GetLookupTableForArray, \
	 Outline, SetActiveSource, Contour, 		\
	 WarpByScalar, CreateScalarBar, Hide

    paraview.simple._DisableFirstRenderCameraReset()
    data = LegacyVTKReader(FileNames=filelist)
    data = get_high_order_data (data, option)
    data_show = Show(data)
    Hide(data)

    # how to get data range automatically ?
    x_min = option['bbox'][0][0]
    x_max = option['bbox'][1][0]
    y_min = option['bbox'][0][1]
    y_max = option['bbox'][1][1]
    f_min = option['range'][0]
    f_max = option['range'][1]
    z_scale = max(x_max-x_min,y_max-y_min)/(f_max-f_min)
    xg = [(x_max+x_min)/2., (y_max+y_min)/2., z_scale*(f_max+f_min)/2.]
    dx = [x_max-x_min, y_max-y_min, z_scale*(f_max-f_min)]

    warp = WarpByScalar()
    warp.Input = data
    warp.Scalars = option['mark'] # CHANGE
    warp.ScaleFactor = z_scale

    color_bar = make_color_bar (option)
    color_map = color_bar.GetProperty("LookupTable").GetData()
    color_opa = get_color_opa (color_map, option)

    warp_show = Show(warp)
    warp_show.InterpolateScalarsBeforeMapping = 1
    warp_show.LookupTable           = color_map
    warp_show.ScalarOpacityFunction = color_opa
    if option['fill'] == 1:
      warp_show.ColorArrayName = option['mark']
      warp_show.Representation = 'Surface'
    else:
      warp_show.ColorArrayName = ''
      warp_show.Representation = 'Wireframe'
    warp_show.SelectionPointFieldDataArrayName = option['mark']
    warp_show.ScaleFactor = 1
    warp_show.DiffuseColor         = [0, 0, 0]
    warp_show.AmbientColor         = [0, 0, 0]
    warp_show.BackfaceDiffuseColor = [0, 0, 0]
    warp_show.EdgeColor            = [0, 0, 0.5]
    warp_show.ScalarOpacityUnitDistance = 0.15254729527751434

    if option['showlabel'] == 1 and option['fill'] == 1:
      render = GetRenderView()
      render.Representations.append(color_bar)
    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 2.4.a field: 2d scalar bicolor (continuous)
# --------------------------------------------------------------------------
# - with continuous color map (1024 colors)
# - do not use opacity (skip paraview-2.7.0 bug)
#   => used only in that case
def paraview_branch_2d_scalar_bicolor_continuous (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, GetLookupTableForArray, \
	GetLookupTableForArray, CreateScalarBar, Calculator, Contour, IsoVolume, Show, \
	SetActiveSource, GetRenderView, Render, Text, GetColorTransferFunction, \
        GetOpacityTransferFunction, GetScalarBar, ColorBy, HideScalarBarIfNotNeeded

    render = GetRenderView()
    f_min = option['range'][0]
    f_max = option['range'][1]
    eps = 1e-16
    if ((f_min*f_max >= 0) or (abs(f_min) < eps) or (abs(f_max) < eps)):
      # no change of sign
      paraview_branch_2d_scalar_color (paraview, basename, filelist, option)
      return

    paraview.simple._DisableFirstRenderCameraReset()

    data = LegacyVTKReader(FileNames=[basename+".vtk"] )
    data.UpdatePipeline()
    # --------------------------- 
    # bicolor map
    # --------------------------- 
    n_iso_continuous = 1024
    color_map = GetColorTransferFunction('scalar')
    color_map.NumberOfTableValues = n_iso_continuous
    color_map.ScalarRangeInitialized = 1
    eps = 1./n_iso_continuous
    eps_p = max(f_max, 1)*eps
    eps_m = min(f_min,-1)*eps
    if option['color'] == 'color': 
      color_map.ColorSpace = 'Lab'
      color_map.RGBPoints = [
        # value    R  G  B
        f_min,     0, 0, 1,  # (f_min, blue)
        eps_m,     0, 1, 1,  # (0-,    cyan)
        0,         1, 1, 1,  # (0,     white)
        eps_p,     1, 1, 0,  # (0+,    yellow)
        f_max,     1, 0, 0]  # (f_max, red)
    else:
      g = 0.7
      w = 1.0
      color_map.ColorSpace = 'RGB'
      color_map.RGBPoints = [
        # value    R  G  B
        f_min,     w, w, w,  # (f_min, white)
        eps_m,     w, w, w,  # (0-,    white)
        0,         w, w, w,  # (0,     white)
        eps_p,     g, g, g,  # (0+,    gray)
        f_max,     g, g, g]  # (f_max, gray)

    # ------------------------------
    # bicolor bands
    # ------------------------------
    bicolor_show = Show(data, render)
    bicolor_show.Representation = 'Surface'
    bicolor_show.ColorArrayName = ['POINTS', 'scalar']
    bicolor_show.LookupTable = color_map
    bicolor_show.ScaleFactor = 1

    # ------------------------------
    # colorbar
    # ------------------------------
    color_bar = GetScalarBar(color_map, render)
    color_bar.Visibility = 1
    color_bar.Title = option['label']
    color_bar.ComponentTitle = ''
    color_bar.TitleColor = [0, 0, 0]
    color_bar.LabelColor = [0, 0, 0]

    # ------------------------------
    # iso=0
    # ------------------------------
    zero_contour = Contour(Input=data)
    zero_contour.ContourBy = ['POINTS', option['mark'] ]
    zero_contour.Isosurfaces = [0.0]

    zero_contour_show = Show(zero_contour, render)
    zero_contour_show.ColorArrayName = ['POINTS', '']
    zero_contour_show.Representation = 'Wireframe'
    zero_contour_show.LineWidth = 2
    zero_contour_show.DiffuseColor = [0, 0, 0] # black isoline

    # ----------------------------------------
    # isolines
    # ----------------------------------------
    if option.get('contours') == None or option.get('contours') == 1:
      SetActiveSource(data)
      contour = Contour()
      contour.Isosurfaces = build_isovalue_list(option)
      contour.ContourBy = option['mark']
      contour.PointMergeMethod = "Uniform Binning"
      contour_show = Show()
      contour_show.SelectionPointFieldDataArrayName = option['mark']
      contour_show.DiffuseColor = [0, 0, 0] # black isoline
      try:    ColorBy(contour_show, None)
      except: 0
    if option['showlabel'] == 1 and option['color'] == 'color': 
      render = GetRenderView()
    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 2.4.b field: 2d scalar bicolor (discrete)
# --------------------------------------------------------------------------
# - with discrete color map (n colors)
# - use opacity (problem with paraview-2.7.0 bug)
def paraview_branch_2d_scalar_bicolor_discrete (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, CreatePiecewiseFunction, GetLookupTableForArray, \
	GetLookupTableForArray, CreateScalarBar, Calculator, Contour, IsoVolume, Show, \
	SetActiveSource, GetRenderView, Render, Text

    render = GetRenderView()
    f_min = option['range'][0]
    f_max = option['range'][1]
    eps = 1e-16
    if ((f_min*f_max >= 0) or (abs(f_min) < eps) or (abs(f_max) < eps)):
      # no change of sign
      paraview_branch_2d_scalar_color (paraview, basename, filelist, option)
      return

    paraview.simple._DisableFirstRenderCameraReset()

    data = LegacyVTKReader( FileNames=[basename+".vtk"] )
    data.UpdatePipeline()
    # --------------------------- 
    # two color maps
    # --------------------------- 
    negative_color_opacity = CreatePiecewiseFunction()
    negative_color_opacity.Points = [f_min, 0, 0.5, 0, \
                                     0,     1, 0.5, 0]
    negative_color_map = GetLookupTableForArray("scalar", 1)
    negative_color_map.NumberOfTableValues = option['n_isovalue_negative']
    negative_color_map.ColorSpace = 'RGB'
    negative_color_map.RGBPoints = [f_min,  0, 0, 1, # (f_min, blue)
                                     0,     0, 1, 1] # (0,     cyan)
    negative_color_map.ScalarRangeInitialized = 1
    try:    negative_color_map.LockDataRange = True; # BUG_PARAVIEW_OPACITY
    except: 0
    try: negative_color_map.ScalarOpacityFunction = negative_color_opacity # paraview-4.1:
    except: 0
    try: negative_color_map.LockScalarRange = 1 ; # backward compat pb
    except: 0
    try:
      # paraview-4.1:
      negative_color_map.Annotations = ["%.15g"%(f_min), "%.7g"%(f_min), \
                                        "0",            "0"]
    except: 0

    positive_color_opacity = CreatePiecewiseFunction()
    positive_color_opacity.Points = [ 0,   0,   0.5, 0, \
                                      0.5, 0.5, 0.5, 0, \
                                      1,   1,   0.5, 0]
    positive_color_map = GetLookupTableForArray("Result", 1)
    positive_color_map.NumberOfTableValues = option['n_isovalue'] - option['n_isovalue_negative']
    positive_color_map.ColorSpace = 'RGB'
    positive_color_map.RGBPoints = [
        # value    R  G  B
          0,       1, 1, 0,  # (0, yellow)
          1,       1, 0, 0]  # (1, red)
    positive_color_map.ScalarRangeInitialized = 1
    try: positive_color_map.LockDataRange = True; # BUG_PARAVIEW_OPACITY
    except: 0
    try: positive_color_map.LockScalarRange        = 1; # backward compat pb
    except: 0
    positive_color_map.ScalarOpacityFunction = positive_color_opacity
    try:
      # paraview-4.1:
      positive_color_map.Annotations = ["0", "0", \
                                        "1", "%.7g"%(f_max)]
    except: 0

    negative_color_bar = CreateScalarBar()
    negative_color_bar.LookupTable =  negative_color_map
    negative_color_bar.Title       = ' ' 
    negative_color_bar.Position  = [0.93, 0.25]
#    negative_color_bar.Position2 = [0.13, 0.5]; # backward compat pb
    negative_color_bar.Enabled       = 1
    negative_color_bar.Selectable    = 1
    negative_color_bar.LabelShadow   = 1
    negative_color_bar.AutomaticLabelFormat = 0
#   negative_color_bar.NumberOfLabels = 1; # backward compat pb
    negative_color_bar.TitleFontSize = 10
    negative_color_bar.LabelFontSize = 10
    negative_color_bar.TitleColor    = [0, 0, 0]
    negative_color_bar.LabelColor    = [0, 0, 0]

    positive_color_bar = CreateScalarBar()
    positive_color_bar.LookupTable =  positive_color_map
    positive_color_bar.Title       = ' '
    positive_color_bar.Position  = [0.85, 0.4]
#   positive_color_bar.Position2 = [0.13, 0.5]; # backward compat pb
    positive_color_bar.Enabled     = 1
    positive_color_bar.Selectable  = 1
    positive_color_bar.LabelShadow = 1
    positive_color_bar.AutomaticLabelFormat = 0
#   positive_color_bar.NumberOfLabels = 1; # backward compat pb
    positive_color_bar.TitleFontSize = 10
    positive_color_bar.LabelFontSize = 10
    positive_color_bar.TitleColor = [0, 0, 0]
    positive_color_bar.LabelColor = [0, 0, 0]

    # ----------------------------
    # title for the two color bars
    # ----------------------------
    if option['label'] != '' and option['showlabel'] == 1:
      title = Text()
      title.Text = option['label']
      title_show = Show()
      title_show.Color      = [0,0,0]
      title_show.FontFamily = 'Times'
      title_show.FontSize   = 16
      title_show.Justification = 'Center'
      title_show.Position   = [0.5, 0.95]
      try:
        title_show.WindowLocation = 'Any Location'
      except:
        try:
          title_show.WindowLocation = 'AnyLocation'
        except:
          0;
    # --------------------------- 
    # negative part, in backgroud
    # --------------------------- 
    SetActiveSource(data)
    negative_rescale = Calculator()
    #negative_rescale.Function = "scalar/%.16g"%(abs(f_min))
    negative_rescale.Function = "scalar"
    
    negative_show = Show()
    negative_show.SelectionPointFieldDataArrayName = option['mark']
    negative_show.ColorArrayName                   = option['mark']
    negative_show.ScalarOpacityFunction = negative_color_opacity
    negative_show.LookupTable           = negative_color_map
    negative_show.AmbientColor  = [0, 0, 0]
   #negative_show.CubeAxesColor = [0, 0, 0]
    negative_show.EdgeColor     = [0, 0, 0.5]
    negative_show.ScalarOpacityUnitDistance = 0.4027013671755797
    if option['color'] != 'color': 
      negative_show.Visibility = 0
    negative_show.SetScalarBarVisibility(render, False)
    negative_show.Opacity = 0.5
    negative_show.UpdatePipeline()
    # ----------------------------------------
    # positive part, in foregroud, with a mask
    # ----------------------------------------
    SetActiveSource(data)
    positive_rescale = Calculator()
    positive_rescale.Function = "scalar/%.16g"%(f_max)
   
    positive_only = IsoVolume()
    positive_only.InputScalars   = 'Result'
    positive_only.ThresholdRange = [0, 1]

    positive_show = Show()
    positive_show.SelectionPointFieldDataArrayName = 'Result'
    #positive_show.Opacity = 0.5
    if option['color'] == 'color': 
      positive_show.ColorArrayName = 'Result'
      positive_show.ScalarOpacityFunction = positive_color_opacity
      positive_show.LookupTable           = positive_color_map
    else:
      positive_show.ColorArrayName = ''
      if option['format'] == '':
        gray = 75./100.
      else:
        gray = 95./100.
      positive_show.DiffuseColor   = [gray, gray, gray]
    positive_show.Representation = 'Surface'
    positive_show.AmbientColor  = [0, 0, 0]
   #positive_show.CubeAxesColor = [0, 0, 0]
    positive_show.ScaleFactor = 1
    positive_show.ScalarOpacityUnitDistance = 0.16539091500936676
    positive_show.BackfaceDiffuseColor = [1, 1, 1]
    positive_show.AmbientColor         = [0, 0, 0]
   #positive_show.CubeAxesColor        = [0, 0, 0]
    positive_show.EdgeColor            = [0, 0, 0]
   # before deactivating "rescale to data range":
    positive_show.UpdatePipeline()
    try: positive_show.RescaleTransferFunctionToDataRange(False, True)
    except: 0; # old paraview
    # ----------------------------------------
    # isolines
    # ----------------------------------------
    if option.get('contours') == None or option.get('contours') == 1:
      SetActiveSource(data)
      zero_contour = Contour()
      zero_contour.Isosurfaces = build_isovalue_list(option)
      zero_contour.ContourBy = option['mark']
      zero_contour_show = Show()
      zero_contour_show.ColorArrayName = ['POINTS', '']
      zero_contour_show.Representation = 'Wireframe'
      zero_contour_show.LineWidth = 2
      zero_contour_show.DiffuseColor  = [0, 0, 0]
    
    if option['showlabel'] == 1 and option['color'] == 'color': 
      render = GetRenderView()
      render.Representations.append(negative_color_bar)
      render.Representations.append(positive_color_bar)
    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 2.4.c field: 2d scalar bicolor (merge cases)
# --------------------------------------------------------------------------
def paraview_branch_2d_scalar_bicolor (paraview, basename, filelist, option):
    if option.get('have_opacity_bug') == None or option.get('have_opacity_bug'):
      paraview_branch_2d_scalar_bicolor_continuous (paraview, basename, filelist, option)
    else:
      paraview_branch_2d_scalar_bicolor_discrete   (paraview, basename, filelist, option)

# --------------------------------------------------------------------------
# 2.5. field:  3d scalar: iso+cut or volume
# --------------------------------------------------------------------------
def paraview_branch_3d_scalar (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, GetLookupTableForArray, \
	GetRenderView, Show, Contour,		\
	SetActiveSource, Outline, Slice, active_objects, CreateScalarBar, \
	Render, GetDisplayProperties, Clip

    paraview.simple._DisableFirstRenderCameraReset()
    
    data = LegacyVTKReader(FileNames=filelist)
    data = get_high_order_data (data, option)
    x_min = option['bbox'][0][0]
    x_max = option['bbox'][1][0]
    y_min = option['bbox'][0][1]
    y_max = option['bbox'][1][1]
    z_min = option['bbox'][0][2]
    z_max = option['bbox'][1][2]
    f_min = option['range'][0]
    f_max = option['range'][1]
    xg = [(x_max+x_min)/2., (y_max+y_min)/2., (z_max+z_min)/2.]
    if (option['origin'][0] > x_min and option['origin'][0] < x_max):
      x0 = option['origin']
    else:
      x0 = xg
    dx = [x_max-x_min, y_max-y_min, z_max-z_min]
    approx = get_approx (data)

    color_bar = make_color_bar (option)
    color_map = color_bar.GetProperty("LookupTable").GetData()
    color_opa = get_color_opa (color_map, option)

    #SetActiveSource(data)
    outline = Outline()
    #SetActiveSource(data)
    outline_show = Show()
    outline_show.ColorArrayName = ''
    #outline_show.SelectionPointFieldDataArrayName = option['mark']
    outline_show.DiffuseColor         = [0, 0, 0]
    outline_show.AmbientColor         = [0, 0, 0]
   #outline_show.CubeAxesColor        = [0, 0, 0]
    outline_show.BackfaceDiffuseColor = [0, 0, 0]
    outline_show.EdgeColor            = [0, 0, 0.5]
    
    # 3d default paraview is a boundary isocontours: hiden
    data_show = GetDisplayProperties(data)
    data_show.Visibility = 0
    SetActiveSource(data)
  
    if option['volume'] == 1:
      # -------------------------------
      # translucide color volume
      # -------------------------------
      volume_show = Show()
      volume_show.Representation = 'Volume'
      volume_show.ColorArrayName = option['mark']
      volume_show.SelectionPointFieldDataArrayName = option['mark']
      volume_show.ScaleFactor = 1
      volume_show.ScalarOpacityUnitDistance = 0.1 # TODO: big effect: how to control it ?
      volume_show.LookupTable           = color_map
      volume_show.ScalarOpacityFunction = color_opa
      volume_show.AmbientColor  = [0, 0, 0]
     #volume_show.CubeAxesColor = [0, 0, 0]
      volume_show.EdgeColor     = [0, 0, 0.5]
    else:
      if approx == "P0":
        # ---------------------------------
        # cut with picewise constant colors
        # ---------------------------------
        clip = Clip()
        clip.Scalars = option['mark']
        clip.ClipType = "Plane"
        clip.ClipType.Origin = x0
        clip.ClipType.Normal = option['normal']
        clip_show = Show()
        clip_show.Representation = 'Surface'
        clip_show.SelectionPointFieldDataArrayName = option['mark']
        clip_show.ColorArrayName = option['mark']
        clip_show.ScaleFactor = 1
        clip_show.LookupTable = color_map
        clip_show.ScalarOpacityFunction = color_opa
        clip_show.ScalarOpacityUnitDistance = 0.16539091500936676
        clip_show.DiffuseColor         = [1, 1, 1]
        clip_show.BackfaceDiffuseColor = [1, 1, 1]
        clip_show.AmbientColor         = [0, 0, 0]
       #clip_show.CubeAxesColor        = [0, 0, 0]
        clip_show.EdgeColor            = [0, 0, 0]
      else: # all except P0: P1, P1d, P2, etc
        # -------------------------------
        # cut with contours
        # -------------------------------
        iso_surface = Contour()
        iso_surface.ContourBy   = option['mark']
        iso_surface.ComputeScalars = 1
        if (f_min <= option['isovalue'] and option['isovalue'] <= f_max):
          iso_surface.Isosurfaces = option['isovalue']
        else:
          iso_surface.Isosurfaces = [(f_max+f_min)/2.]
        iso_surface.PointMergeMethod = "Uniform Binning"
        iso_show = Show()
        iso_show.SelectionPointFieldDataArrayName = 'Normals'
        if option['fill'] == 0:
          iso_show.Representation = 'Wireframe'
          iso_show.ColorArrayName = ''
        else:
          iso_show.Representation = 'Surface'
          iso_show.ColorArrayName = option['mark']
          iso_show.ScaleFactor = 1
          iso_show.LookupTable = color_map
        iso_show.DiffuseColor         = [0, 0, 0]
        iso_show.AmbientColor         = [0, 0, 0]
       #iso_show.CubeAxesColor        = [0, 0, 0]
        iso_show.BackfaceDiffuseColor = [0, 0, 0]
        iso_show.EdgeColor            = [0, 0, 0.5]
  
        SetActiveSource(data)
        cut_plane = Slice()
        cut_plane.SliceType = "Plane"
        cut_plane.SliceOffsetValues = [0.]
        cut_plane.SliceType.Origin = x0
        cut_plane.SliceType.Normal = option['normal']
        iso_cut_show = Show()
        iso_cut_show.ColorArrayName = option['mark']
        iso_cut_show.SelectionPointFieldDataArrayName = option['mark']
        iso_cut_show.LookupTable = color_map
        iso_cut_show.DiffuseColor         = [0, 0, 0]
        iso_cut_show.AmbientColor         = [0, 0, 0]
       #iso_cut_show.CubeAxesColor        = [0, 0, 0]
        iso_cut_show.BackfaceDiffuseColor = [0, 0, 0]
        iso_cut_show.EdgeColor            = [0, 0, 0.5]
   
    if option['showlabel'] == 1: 
      render = GetRenderView()
      render.Representations.append(color_bar)
    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 2.6. field: 3d-surface with a scalar: iso contours
# --------------------------------------------------------------------------
# for an EDP on a 3d surface
def paraview_branch_3d_surface_scalar (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, GetRenderView, 	\
         GetLookupTableForArray, Show,		\
	 CreateScalarBar, SetActiveSource, Contour, GetDisplayProperties

    paraview.simple._DisableFirstRenderCameraReset()
    n_iso = option['n_isovalue']

    data = LegacyVTKReader(FileNames=filelist)
    data = get_high_order_data (data, option)

    color_bar = make_color_bar (option)
    color_map = color_bar.GetProperty("LookupTable").GetData()
    color_opa = get_color_opa (color_map, option)

    if option['color'] == 'black_and_white':

      mesh_show = GetDisplayProperties(data)
      mesh_show.ColorArrayName     = ''
      #mesh_show.ColorAttributeType = 'CELL_DATA'
      mesh_show.Representation     = 'Wireframe'
      mesh_show.Opacity            = 0.25
      mesh_show.DiffuseColor       = [0, 0, 0]
      mesh_show.AmbientColor       = [0, 0, 0]
     #mesh_show.CubeAxesColor      = [0, 0, 0]
      mesh_show.EdgeColor            = [0, 0, 0.5]
      #mesh_show.BackfaceDiffuseColor = [0.3333333333333333, 0.6666666666666666, 1]
      #mesh_show.DiffuseColor         = [0.3333333333333333, 0.6666666666666666, 1]
      mesh_show.ScalarOpacityUnitDistance = 0.09596915518332425
    else:
      iso_contour = Show()
      iso_contour.SelectionPointFieldDataArrayName = option['mark']
      iso_contour.ColorArrayName = option['mark']
      if option['fill'] == 1:
        iso_contour.Representation = 'Surface'
      else:
        iso_contour.Representation = 'Wireframe'
#     if option['stereo'] and not option['high_order']:
#       # BUG with paraview: tesselate do not honor opacity
#       iso_contour.Opacity = 0.65
      iso_contour.ScaleFactor   = 1
      iso_contour.ScalarOpacityFunction = color_opa
      iso_contour.LookupTable           = color_map
      iso_contour.AmbientColor         = [0, 0, 0]
     #iso_contour.CubeAxesColor        = [0, 0, 0]
      iso_contour.EdgeColor            = [0, 0, 0.5]
      iso_contour.BackfaceDiffuseColor = [0.3333333333333333, 0.6666666666666666, 1]
      iso_contour.DiffuseColor         = [0.3333333333333333, 0.6666666666666666, 1]
      iso_contour.ScalarOpacityUnitDistance = 0.09596915518332425
      SetActiveSource(data)

    iso_lines = Contour()
    iso_lines.PointMergeMethod = "Uniform Binning"
    iso_lines.ContourBy        = option['mark']
    iso_lines.Isosurfaces      = build_isovalue_list(option)
    iso_show = Show()
    iso_show.SelectionPointFieldDataArrayName = option['mark']
    iso_show.ColorArrayName = ('POINT_DATA', '')
    iso_show.DiffuseColor         = [0, 0, 0]

    if option['color'] != 'black_and_white' and option['showlabel'] == 1: 
      render = GetRenderView()
      render.Representations.append(color_bar)
    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 2.7. scalar field visualization
# --------------------------------------------------------------------------
def paraview_branch_scalar (paraview, basename, filelist, option):
    if option['view_2d'] == 1: 
      if option['elevation'] == 1: 
        paraview_branch_2d_scalar_elevation (paraview, basename, filelist, option)
      elif option['color'] == 'black_and_white': 
        paraview_branch_2d_scalar_bw        (paraview, basename, filelist, option)
      elif option['n_isovalue_negative'] != 0: 
        paraview_branch_2d_scalar_bicolor   (paraview, basename, filelist, option)
      else:
        paraview_branch_2d_scalar_color     (paraview, basename, filelist, option)
    else: 
      if option['view_map'] == 0: 
        paraview_branch_3d_scalar           (paraview, basename, filelist, option)
      else:
        paraview_branch_3d_surface_scalar   (paraview, basename, filelist, option)

def paraview_field_scalar (paraview, basename, option):
    filelist = [basename+".vtk"]
    paraview_branch_scalar (paraview, basename, filelist, option)

# ==========================================================================
# 3. vector field
# ==========================================================================
# 3.1. vector field visualization: deformation
# --------------------------------------------------------------------------
def paraview_field_vector_deformation (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, \
	Show, WarpByVector, Outline, GetRenderView, SetActiveSource, Outline

    paraview.simple._DisableFirstRenderCameraReset()
    data = LegacyVTKReader(FileNames=filelist)
    x_min = option['bbox'][0][0]
    x_max = option['bbox'][1][0]
    y_min = option['bbox'][0][1]
    y_max = option['bbox'][1][1]
    z_min = option['bbox'][0][2]
    z_max = option['bbox'][1][2]
    f_min = option['range'][0]
    f_max = option['range'][1]
    xg = [(x_max+x_min)/2., (y_max+y_min)/2., (z_max+z_min)/2.]
    dx = [x_max-x_min, y_max-y_min, z_max-z_min]
    if option['mark'] == '':
       option['mark'] = "vector"
    if option['label'] == '':
       option['label'] = "|" + option['mark'] + "|"
    print ("deformation: mark = ", option['mark']);

    color_bar = make_color_bar (option)
    color_map = color_bar.GetProperty("LookupTable").GetData()
    color_opa = get_color_opa(color_map, option)

    outline      = Outline()
    outline_show = Show()
    outline_show.DiffuseColor         = [0, 0, 0]
    outline_show.AmbientColor         = [0, 0, 0]
   #outline_show.CubeAxesColor        = [0, 0, 0]
    outline_show.BackfaceDiffuseColor = [0, 0, 0]
    outline_show.EdgeColor            = [0, 0, 0.5]
    
    SetActiveSource(data)
    deformation = WarpByVector()
    deformation.Vectors = option['mark']
    if (f_max-f_min != 0):
      # adjust the deformation scale to 10% of the domain characteristic length	
      # can be adjusted by the "-scale" command line option, or interactively
      length = max(max(x_max-x_min,y_max-y_min),z_max-z_min) 
      deformation.ScaleFactor = option['scale']*(0.1*length)/(f_max-f_min)
    else:
      deformation.ScaleFactor = 1;
    deformation_show = Show()
    deformation_show.ScaleFactor = 1;
    if option['fill'] == 0:
      deformation_show.Representation = 'Wireframe'
    else:
      deformation_show.Representation = 'Surface'
    deformation_show.SelectionPointFieldDataArrayName = option['mark'] + '_norm'
    deformation_show.ColorArrayName = option['mark'] + '_norm'
    deformation_show.LookupTable           = color_map
    deformation_show.ScalarOpacityFunction = color_opa
    deformation_show.ScalarOpacityUnitDistance = 0.05547965123845249
    deformation_show.DiffuseColor         = [0, 0, 0]
    deformation_show.AmbientColor         = [0, 0, 0]
   #deformation_show.CubeAxesColor        = [0, 0, 0]
    deformation_show.BackfaceDiffuseColor = [0, 0, 0]
    deformation_show.EdgeColor            = [0, 0, 0.5]
    
    if option['showlabel'] == 1: 
      render = GetRenderView()
      render.Representations.append(color_bar)
    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 3.2. vector field visualization: arrow
# --------------------------------------------------------------------------
def paraview_field_vector_arrow    (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, \
        SetActiveSource, Show, Glyph, Outline, GetRenderView, GetDisplayProperties
    
    paraview.simple._DisableFirstRenderCameraReset()
    data = LegacyVTKReader(FileNames=filelist)
    x_min = option['bbox'][0][0]
    x_max = option['bbox'][1][0]
    y_min = option['bbox'][0][1]
    y_max = option['bbox'][1][1]
    z_min = option['bbox'][0][2]
    z_max = option['bbox'][1][2]
    xg = [(x_max+x_min)/2., (y_max+y_min)/2., (z_max+z_min)/2.]
    dx = [x_max-x_min, y_max-y_min, z_max-z_min]
    if option['mark'] == '':
       option['mark'] = "vector"
    if option['label'] == '':
       option['label'] = "|" + option['mark'] + "|"
    
    color_bar = make_color_bar (option)
    color_map = color_bar.GetProperty("LookupTable").GetData()
    color_opa = get_color_opa (color_map, option)

    if option['fill'] == 1:
      data_show = Show()
      data_show.ScaleFactor = 1;
      data_show.Representation = 'Surface'
      data_show.SelectionPointFieldDataArrayName = option['mark'] + '_norm'
      data_show.ColorArrayName = option['mark'] + '_norm'
      data_show.LookupTable           = color_map
      data_show.ScalarOpacityFunction = color_opa
      data_show.ScalarOpacityUnitDistance = 0.05547965123845249
      data_show.DiffuseColor         = [0, 0, 0]
      data_show.AmbientColor         = [0, 0, 0]
     #data_show.CubeAxesColor        = [0, 0, 0]
      data_show.BackfaceDiffuseColor = [0, 0, 0]
      data_show.EdgeColor            = [0, 0, 0.5]

    arrow = Glyph()
    try:
      # paraview 5.7:
      arrow.OrientationArray = ['POINTS', option['mark']]
      arrow.ScaleArray       = ['POINTS', option['mark'] + '_norm']
    except:
      # paraview older:
      try:
        arrow.Scalars   = option['mark'] + '_norm'
        arrow.Vectors   = option['mark']
        arrow.ScaleMode = 'vector'
      except: 0;           
    if option['view_2d'] == 1: 
      arrow.GlyphType = '2D Glyph'
      render = GetRenderView()
      arrow_display = GetDisplayProperties(arrow, view=render)
      arrow_display.LineWidth = 2.0
    else:
      arrow.GlyphType   = 'Arrow'

    h_average = 0.1; # TODO: compute element average length
    # old paraview-4.x:
    try:    arrow.SetScaleFactor = h_average*option['scale']; # old paraview 4.x
    except: 0;           
    # new paraview-5.x:
    try:
      arrow.ScaleFactor = h_average*option['scale'];
      arrow.ScaleMode   = 'vector'
      #arrow.ScaleMode  = 'vector_components';
    except: 0;          
    #arrow.RandomMode = 0
    #arrow.MaskPoints = 0

    arrow_show = Show()
    arrow_show.SelectionPointFieldDataArrayName = option['mark'] + '_norm'
    arrow_show.LookupTable = color_map
    if option['fill'] == 1:
      arrow_show.ColorArrayName = ''
      arrow_show.DiffuseColor         = [0, 0, 0]
    else:
      arrow_show.ColorArrayName = option['mark'] + '_norm'
      arrow_show.DiffuseColor         = [0, 0, 0]
    arrow_show.AmbientColor         = [0, 0, 0]
   #arrow_show.CubeAxesColor        = [0, 0, 0]
    arrow_show.BackfaceDiffuseColor = [0, 0, 0]
    arrow_show.EdgeColor            = [0, 0, 0.5]
    
    if option['showlabel'] == 1: 
      render = GetRenderView()
      render.Representations.append(color_bar)
    do_render(paraview, basename, option)

# --------------------------------------------------------------------------
# 3.3. vector field visualization
# --------------------------------------------------------------------------
def paraview_branch_vector (paraview, basename, filelist, option):
    if option['style'] == 'deformation': 
      paraview_field_vector_deformation (paraview, basename, filelist, option)
    else:
      paraview_field_vector_arrow       (paraview, basename, filelist, option)

def paraview_field_vector (paraview, basename, option):
    filelist = [basename+".vtk"]
    paraview_branch_vector (paraview, basename, filelist, option)

# ==========================================================================
# 4. tensor field visualization
# ==========================================================================
# warning: requieres the "TensorGlyph" plugin that should be compiled separately
# TODO: distribute it with rheolef, if it can be compiled with paraview-dev ?
def paraview_branch_tensor (paraview, basename, filelist, option):
    from paraview.simple import LegacyVTKReader, \
	Show, GetRenderView, SetActiveSource, Outline, \
	GetLookupTableForArray, Render
    try:
      from paraview.simple import TensorGlyph
    except:
      print("error(paraview_rheolef.py): TensorGlyph paraview pluging not installed")
      print("HINT: install TensorGlyph paraview pluging from source code and check PV_PLUGIN_PATH environment variable")
      exit(1)

    paraview.simple._DisableFirstRenderCameraReset()
    data = LegacyVTKReader(FileNames=filelist)
    x_min = option['bbox'][0][0]
    x_max = option['bbox'][1][0]
    y_min = option['bbox'][0][1]
    y_max = option['bbox'][1][1]
    z_min = option['bbox'][0][2]
    z_max = option['bbox'][1][2]
    f_min = option['range'][0]
    f_max = option['range'][1]
    xg = [(x_max+x_min)/2., (y_max+y_min)/2., (z_max+z_min)/2.]
    dx = [x_max-x_min, y_max-y_min, z_max-z_min]
    if option['mark'] == '':
       option['mark'] =  'tensor'
    if option['label'] == '':
       option['label'] = "|" + option['mark'] + "|"

    color_bar = make_color_bar (option)
    color_map = color_bar.GetProperty("LookupTable").GetData()
    color_opa = get_color_opa (color_map, option)

    SetActiveSource(data)
    outline = Outline()
    outline_show = Show()
    outline_show.ColorArrayName = ''
    outline_show.SelectionPointFieldDataArrayName = option['mark']
    outline_show.DiffuseColor = [0, 0, 0]

    SetActiveSource(data)
    tensor_norm = Show()
    tensor_norm.SelectionPointFieldDataArrayName = option['mark'] + "_norm"
    tensor_norm.ColorArrayName                   = option['mark'] + "_norm"
    tensor_norm.LookupTable = color_map
    tensor_norm.ScalarOpacityFunction = color_opa
    tensor_norm.ScalarOpacityUnitDistance = 0.2418271175121958
    tensor_norm.ScaleFactor = 1
    tensor_norm.AmbientColor  = [0, 0, 0]
   #tensor_norm.CubeAxesColor = [0, 0, 0]
    tensor_norm.EdgeColor     = [0, 0, 0.5]
    if option['view_2d'] != 1:
      tensor_norm.Visibility  = 0
    
    tensor_glyph = TensorGlyph()
    tensor_glyph.GlyphType = "Sphere"
    tensor_glyph.ColorGlyphs = 1
    tensor_glyph.ExtractEigenvalues = 1
    tensor_glyph.Scalars = option['mark'] + "_norm"
    #tensor_glyph.Tensors = ['POINTS', option['label']]
    tensor_glyph.Tensors = option['mark']
    h_average = 0.1; # TODO: compute element average length
    tensor_glyph.ScaleFactor = 1.5*h_average*option['scale'];
    tensor_glyph.LimitScalingByEigenvalues = 0
    
    tensor_glyph_show = Show()
    tensor_glyph_show.SelectionPointFieldDataArrayName = option['mark'] + "_norm"
    if option['view_2d'] == 1:
      tensor_glyph_show.ColorArrayName = ''
    else:
      tensor_glyph_show.ColorArrayName = option['mark'] + "_norm"
    tensor_glyph_show.LookupTable = color_map
    tensor_glyph_show.ScaleFactor = 1
    gray = 11./12.
    tensor_glyph_show.DiffuseColor         = [gray, gray, gray]
    tensor_glyph_show.BackfaceDiffuseColor = [gray, gray, gray]
    tensor_glyph_show.AmbientColor         = [0, 0, 0]
   #tensor_glyph_show.CubeAxesColor        = [0, 0, 0]
    tensor_glyph_show.EdgeColor            = [0, 0, 0.5]
    
    if option['showlabel'] == 1: 
      render = GetRenderView()
      render.Representations.append(color_bar)
    do_render(paraview, basename, option)

def paraview_field_tensor (paraview, basename, option):
    filelist = [basename+".vtk"]
    paraview_branch_tensor (paraview, basename, filelist, option)

# ==========================================================================
# 5. geo visualization
# ==========================================================================
# global variables (to all domains):
# ----------------------------------
global_geo_color = [				\
	( 0, 0,   0),				\
	( 0, 0,   1),				\
	( 0, 0.5, 0),				\
	( 1, 0,   0),				\
	( 1, 0,   1),				\
	( 0, 1,   1),				\
	( 0.7, 0.5,   0),			\
	(0,      0.9,    0.1    ),		\
	(0.8900, 0.1500, 0.2100	),		\
	(0.5000, 0.3800, 0.0100	) ]

def geo_n_vertex (data):
    n_array = len(data.PointData)
    if (n_array < 1):
      return 0;
    nv = data.PointData.GetFieldData().GetArrayInformation(0).GetNumberOfTuples()
    return nv

def paraview_geo_domain (paraview, option, filename, dom_name, idx):
    from paraview.simple import LegacyVTKReader, Show, \
	ExtractEdges, Shrink, Text, ExtractCellsByRegion, Clip, \
	SetActiveSource, GetRenderView, GetActiveViewOrCreate, GetColorTransferFunction
  
    data = LegacyVTKReader(FileNames=[filename])
    if not (option['shrink'] or option['cut']):
      option['high_order'] = 0
    data = get_high_order_data (data, option)
    render = GetActiveViewOrCreate('RenderView')
   
    #get bbox of the whole mesh, not only the domain:
    x_min = option['bbox'][0][0]
    x_max = option['bbox'][1][0]
    y_min = option['bbox'][0][1]
    y_max = option['bbox'][1][1]
    z_min = option['bbox'][0][2]
    z_max = option['bbox'][1][2]
    xg = [(x_max+x_min)/2., (y_max+y_min)/2., (z_max+z_min)/2.]
    dx = [x_max-x_min, y_max-y_min, z_max-z_min]

    color_map = GetColorTransferFunction('mesh')
    color_map.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 5e-17, 0.865003, 0.865003, 0.865003, 1e-16, 0.705882, 0.0156863, 0.14902]
    color_map.ScalarRangeInitialized = 1.0
    color_opa = get_color_opa (color_map, option)
    if option['color'] == 'black_and_white':
      color = [0, 0, 0]
    else:
      if option['shrink'] == 0 or idx > 0:
        idx_color = idx % len(global_geo_color)
        color = global_geo_color[idx % len(global_geo_color)]
      else:
        gray = 11./12.
        color = [gray, gray, gray]

    if option['showlabel'] == 1 and option['color'] != 'black_and_white' and idx == 0:
      # TODO: ne = ? how to get it ?
      nv = geo_n_vertex(data)
      title = Text()
      import os.path
      title.Text = "%s : %d vertices"%(os.path.basename(dom_name), nv)
      title_show = Show()
      title_show.Color      = [0,0,0]
      title_show.FontFamily = 'Times'
      title_show.FontSize   = 16
      title_show.Justification = 'Left'
      title_show.Position   = [0.025, 0.955]
      try:
        title_show.WindowLocation = 'Any Location'
      except:
        try:
          title_show.WindowLocation = 'AnyLocation'
        except:
          0;
    if option['showlabel'] == 1 and option['color'] != 'black_and_white' and idx != 0:
      title = Text()
      title.Text = dom_name
      title_show = Show()
      title_show.Color = color
      title_show.FontFamily = 'Times'
      title_show.FontSize   = 16
      title_show.Justification = 'Left'
      title_show.Position   = [0.025, 0.955 - 0.040*idx]
      try:
        title_show.WindowLocation = 'Any Location'
      except:
        try:
          title_show.WindowLocation = 'AnyLocation'
        except:
          0;

    SetActiveSource(data)
    #print("dom_name=",dom_name)
    #print("data=",data)
    #print("fill=",option['fill'])
    #print("full=",option['full'])
    if (option['full'] == 0 or option['high_order'] == 1) and option['shrink'] == 0:
      #print("cas 1")
      mesh_show = Show(data,render)
      mesh_show.ColorArrayName = ''
      mesh_show.SelectionPointFieldDataArrayName = 'mesh'
      mesh_show.LookupTable           = color_map
      mesh_show.ScalarOpacityFunction = color_opa
      if ((option['color'] != 'black_and_white' and idx == 0 and option['showlabel'] == 1) or option['fill'] == 0):
        mesh_show.Representation     = 'Wireframe'
        mesh_show.DiffuseColor       = [0,0,0]
        mesh_show.AmbientColor       = color
        #print("wireframe")
        #print("color(1)=",color)
        if (idx != 0):
          mesh_show.LineWidth        = 2
      else:
        mesh_show.Representation     = 'Surface'
        if idx == 0:
          mesh_show.DiffuseColor     = [0.9,0.9,0.9]
        else:
          mesh_show.DiffuseColor     = color
        #print("surface")
        #print("color(2)=",color)
        mesh_show.AmbientColor       = [0, 0, 0]
      if option['cut'] == 1:
        mesh_show.Visibility = 0
        clip = Clip()
        clip.Scalars = 'mesh'
        try:    clip.Invert = 0;    # normal goes out from selected region
        except: clip.InsideOut = 1; # old paraview version
        try:    clip.Crinkleclip = 0;  # paraview-4 style: do not cut inside elements, eliminates them
        except: 0;                     # paraview-3 style: cut inside elements
        clip.ClipType = "Plane"
        clip.ClipType.Origin = xg
        clip.ClipType.Normal = option['normal']
        clip_show = Show()
        clip_show.Representation = 'Surface With Edges'
        clip_show.SelectionPointFieldDataArrayName = 'mesh'
        clip_show.ColorArrayName = ''
        clip_show.ScaleFactor = 1
        clip_show.LookupTable = color_map
        clip_show.ScalarOpacityFunction = []
        clip_show.ScalarOpacityUnitDistance = 0.16539091500936676
        clip_show.DiffuseColor         = [1, 1, 1]
        clip_show.BackfaceDiffuseColor = [1, 1, 1]
        clip_show.AmbientColor         = [0, 0, 0]
        clip_show.EdgeColor            = [0, 0, 0]
    elif option['shrink'] == 1:
      #print("cas 2")
      shrink = Shrink()
      shrink_show = Show()
      shrink_show.ScaleFactor           = 0.0950000023469329
      shrink_show.SelectionPointFieldDataArrayName = 'mesh'
      shrink_show.ColorArrayName = ''
      shrink_show.LookupTable           = color_map
      shrink_show.ScalarOpacityFunction = color_opa
      shrink_show.DiffuseColor          = color
      shrink_show.AmbientColor          = [0, 0, 0]
      shrink_show.BackfaceDiffuseColor  = [0, 0, 0]
      shrink_show.EdgeColor             = [0, 0, 0.5]
      shrink_show.ScalarOpacityUnitDistance = 0.1811050201435371
      if option['cut'] == 1:
        shrink_show.Visibility = 0
        extract = ExtractCellsByRegion(IntersectWith="Plane")
        extract.IntersectWith = "Plane"
        extract.IntersectWith.Origin = xg
        extract.IntersectWith.Normal = option['normal']
        extract_show = Show()
        extract_show.ScaleFactor = 0.09669428616762162
        extract_show.SelectionPointFieldDataArrayName = 'mesh'
        extract_show.ColorArrayName        = ''
        extract_show.LookupTable           = color_map
        extract_show.ScalarOpacityFunction = color_opa
        extract_show.DiffuseColor          = color
        extract_show.AmbientColor  = [0, 0, 0]
        extract_show.EdgeColor     = [0, 0, 0.5]
        extract_show.ScalarOpacityUnitDistance = 0.1896561226641973
    else: # option['full'] == 1 and option['high_order'] == 0:
      #print("cas 3")
      extract_edges = ExtractEdges()
      extract_edges_show = Show()
      extract_edges_show.ColorArrayName = ''
      extract_edges_show.SelectionPointFieldDataArrayName = 'mesh'
      extract_edges_show.LookupTable          = color_map
      extract_edges_show.DiffuseColor         = color
      extract_edges_show.AmbientColor         = [0, 0, 0]
      extract_edges_show.BackfaceDiffuseColor = [0, 0, 0]
      extract_edges_show.EdgeColor            = [0, 0, 0.5]
      if (idx != 0):
        extract_edges_show.LineWidth          = 2

def paraview_geo (paraview, option, d):
    from paraview.simple import GetRenderView
    paraview.simple._DisableFirstRenderCameraReset()
    if option.get('boundary_only') == None or option.get('boundary_only') != 1:
      paraview_geo_domain (paraview, option, d[0]+".vtk", d[0], 0)
    if option['cut'] == 1 or option['stereo'] == 1:
      option['showlabel'] = 0
    if option['showlabel'] == 1 or option['color'] == 'black_and_white':
      for i in range(1,len(d)):
        paraview_geo_domain (paraview, option, d[0]+"."+d[i]+".vtk", d[i], i)
    do_render(paraview, d[0], option)

# ==========================================================================
# 6. functions for "pvbatch" usage
# ==========================================================================
# --------------------------------------------------------------------------
# 6.1. mesh adaptation from Pk-geo to P1-geo via a Pk-field criterion
# --------------------------------------------------------------------------
def adapt_Pk_iso_P1 (paraview, in_criterion_file, out_adapt_file, option):
    from paraview.simple import LegacyVTKReader, GetActiveViewOrCreate, Show,  \
	GetColorTransferFunction, Tessellate, Hide, SaveData

    import sys
    try:
      print("! load \"%s\""%in_criterion_file); # file=sys.stderr)
    except: 0 # old paraview-5.4 style with python2:
    data = LegacyVTKReader (FileNames=[in_criterion_file])
    tessellate = Tessellate (Input=data)
    # 'mark' selection not available with tessellate:
    # it takes the first field for adaptation
    # TODO: tessellate.SelectionPointFieldDataArrayName = option['mark']
    tessellate.ChordError =  option['geo_error']   ; # error vs the Pk geometry
    tessellate.FieldError = [option['field_error']]; # error vs the Pk field interpolation
    tessellate.MaximumNumberofSubdivisions = option['subdivide']
    tessellate.MergePoints = 1
    if option.get('view_2d') != None and option['view_2d'] == 1: 
      tessellate.OutputDimension = 2
    else:
      tessellate.OutputDimension = 3
    render = GetActiveViewOrCreate ('RenderView')
    render.Update()
    SaveData (out_adapt_file, proxy=tessellate, FileType='Ascii')
    try:
      print("! file \"%s\" created"%out_adapt_file); # file=sys.stderr)
    except: 0 # old paraview-5.4 style with python2:
