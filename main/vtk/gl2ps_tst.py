#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#!pvbatch --use-offscreept statement has an optional else clause, which, when present, must follow all except clauses. It is useful for code that must be executed if the try clause does not raise  gl2ps_tst.py 
# => fait le fichier mais core dump !
try:
  from vtkRenderingPython import vtkGL2PSExporter # paraview 3
  paraview_version="3.x"
except:
 try:
  from vtk.vtkRenderingPython  import vtkGL2PSExporter # paraview 4 : segfault
  paraview_version="4.0"
 except:
  try:
   from vtk.vtkIOExportPython import *
   paraview_version="4.1"
  except:
   paraview_version="x.x"

from paraview.simple import Cone, Show, GetRenderView, Render, GetActiveView
from paraview.vtk import *
print "paraview_version=",paraview_version
if paraview_version == "x.x":
  print "gl2ps: python module not found"
  #exit(1)

#paraview.simple._DisableFirstRenderCameraReset()

cone = Cone()
show_cone = Show()
render = GetRenderView()
Render()

#print render
#help(render)

# http://www.paraview.org/pipermail/paraview/2010-June/017944.html
view = GetActiveView().GetRenderWindow()
#print view

format="eps"
try:
  exporter = vtkGL2PSExporter()
except:
  try:
    exporter = vtkInstantiator.CreateInstance("vtkGL2PSExporter")
  except:
    exporter = None
if exporter == None:
  print "gl2ps: cannot instanciate module"
  exit(1)

#help(exporter.__class__)
exporter.SetFilePrefix("testImage")
exporter.Write3DPropsAsRasterImageOn()
exporter.SilentOff()
exporter.DrawBackgroundOff(); # bug: has no effect
exporter.CompressOff() # bug: no effect
exporter.LandscapeOn() 
exporter.SetSortToBSP()
if format == "ps":
  exporter.SetFileFormatToPS()
elif format == "eps":
  exporter.SetFileFormatToEPS()
elif format == "pdf":
  exporter.SetFileFormatToPDF()
elif format == "svg":
  exporter.SetFileFormatToSVG()
elif format == "tex":
  exporter.SetFileFormatToTeX()

exporter.SetRenderWindow(view)
print exporter
#vtkGL2PSExporter (0x9c4fa50)
#  Debug: Off
#  Modified Time: 183679
#  Reference Count: 1
#  Registered Events: (none)
#  Render Window: (0x9475a00)
#  Start Write: (none)
#  End Write: (none)
#  FilePrefix: testImage
#  FileFormat: EPS
#  Sort: Simple
#  Compress: On
#  DrawBackground: On
#  SimpleLineOffset: On
#  Silent: Off
#  BestRoot: On
#  Text: On
#  Landscape: Off
#  PS3Shading: On
#  OcclusionCull: On
#  Write3DPropsAsRasterImage: Off
#  RasterExclusions: (null)
try:
  exporter.Write()
except:
  print "gl2ps: cannot write"
  exit(1)

Render()
