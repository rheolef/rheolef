#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#! DISPLAY=:0.0 LANG=C PYTHONPATH=/home/saramito/dvt/rheolef/nfem/vtk/ pvbatch --use-offscreen-rendering ~/dvt/rheolef/nfem/vtk/gl2ps_pv41_tst.py
print "main(1)..."
try:
   #from paraview.vtk import *
   #from paraview.vtk.vtkRenderingGL2PS import *
   #from vtk.vtkRenderingGL2PSPython import *
   from vtk.vtkIOExportPython import *
   #from paraview.libvtkRenderingPython import *
   #import paraview.vtk ; # yes but no gl2ps
   #from vtkpython import *
   #from paraview.vtk.vtkRenderingGL2PS import * ; # ok but no gl2ps !
   #from paraview.vtk.vtkRenderingGL2PS import *
   #from vtk.vtkRenderingGL2PS import vtkGL2PSExporter # no
   #from paraview.vtk.vtkRenderingGL2PS import vtkGL2PSExporter # no
   #from vtkRenderingGL2PSPython import vtkGL2PSExporter # no
   #from libvtkRenderingGL2PS import vtkGL2PSExporter # no
   paraview_version="4.1"
except:
   paraview_version="x.x"

from paraview.simple import Cone, Show, GetRenderView, Render, GetActiveView
print "paraview_version=",paraview_version
if paraview_version == "x.x":
  print "gl2ps: python module not found"
  #exit(1)

#paraview.simple._DisableFirstRenderCameraReset()

cone = Cone()
show_cone = Show()
render = GetRenderView()
Render()

#print render
#help(render)

# http://www.paraview.org/pipermail/paraview/2010-June/017944.html
view = GetActiveView().GetRenderWindow()
#print view

format="eps"
try:
 exporter = vtkGL2PSExporter()
 print "call vtkGL2PSExporter done"
except:
 exporter = vtkInstantiator.CreateInstance("vtkGL2PSExporter")
 print "call vtkInstantiator done"
 if  exporter == None:
   print "error: exporter=",exporter
   exit(1)

#help(exporter.__class__)
exporter.SetFilePrefix("testImage")
exporter.Write3DPropsAsRasterImageOn()
exporter.SilentOff()
exporter.DrawBackgroundOff(); # bug: has no effect
exporter.CompressOff() # bug: no effect
exporter.LandscapeOn() 
exporter.SetSortToBSP()
if format == "ps":
  exporter.SetFileFormatToPS()
elif format == "eps":
  exporter.SetFileFormatToEPS()
elif format == "pdf":
  exporter.SetFileFormatToPDF()
elif format == "svg":
  exporter.SetFileFormatToSVG()
elif format == "tex":
  exporter.SetFileFormatToTeX()

exporter.SetRenderWindow(view)
print exporter
#vtkGL2PSExporter (0x9c4fa50)
#  Debug: Off
#  Modified Time: 183679
#  Reference Count: 1
#  Registered Events: (none)
#  Render Window: (0x9475a00)
#  Start Write: (none)
#  End Write: (none)
#  FilePrefix: testImage
#  FileFormat: EPS
#  Sort: Simple
#  Compress: On
#  DrawBackground: On
#  SimpleLineOffset: On
#  Silent: Off
#  BestRoot: On
#  Text: On
#  Landscape: Off
#  PS3Shading: On
#  OcclusionCull: On
#  Write3DPropsAsRasterImage: Off
#  RasterExclusions: (null)
exporter.Write()

Render()
