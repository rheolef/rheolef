///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/config.h"

#ifdef _RHEOLEF_HAVE_MPI
#include "rheolef/geo_domain_indirect.h"

namespace rheolef {

template<class T>
distributor
geo_domain_indirect_rep<T,distributed>::geo_element_ios_ownership (size_type dim) const 
{
  if (dim == base::map_dimension()) return  base::_indirect.ini_ownership();
  check_macro (dim  < base::map_dimension(), "unexpected dimension = " << dim << " > domain dimension = " << base::map_dimension());
  return base::_omega.geo_element_ios_ownership(dim);
}
template <class T>
typename geo_domain_indirect_rep<T,distributed>::const_reference
geo_domain_indirect_rep<T,distributed>::dis_get_geo_element (size_type dim, size_type dis_ige) const
{
#ifdef TODO
  // see geo_domain_indirect_seq.cc : get_geo_element (dim, ige) 
  if (dim == map_dimension()) return  base::_omega.get_geo_element (dim, _indirect.oige(ige).index());
#endif // TODO
  return base::_omega.dis_get_geo_element (dim, dis_ige);
}
template<class T>
typename geo_domain_indirect_rep<T,distributed>::size_type
geo_domain_indirect_rep<T,distributed>::ige2ios_dis_ige (size_type dim, size_type ige) const
{
  if (dim == base::map_dimension()) return  base::_indirect.ioige2ini_dis_ioige (ige);
  check_macro (dim  < base::map_dimension(), "unexpected dimension = " << dim << " > domain dimension = " << base::map_dimension());
  return base::_omega.ige2ios_dis_ige (dim, ige);
}
template<class T>
typename geo_domain_indirect_rep<T,distributed>::size_type
geo_domain_indirect_rep<T,distributed>::dis_ige2ios_dis_ige (size_type dim, size_type dis_ige) const
{
  check_macro (dim  < base::map_dimension(), "unexpected dimension = " << dim << " > domain dimension = " << base::map_dimension());
  return base::_omega.dis_ige2ios_dis_ige (dim, dis_ige);
}
template<class T>
typename geo_domain_indirect_rep<T,distributed>::size_type
geo_domain_indirect_rep<T,distributed>::ios_ige2dis_ige (size_type dim, size_type ios_ige) const
{
  if (dim == base::map_dimension()) return  base::_indirect.ini_ioige2dis_ioige (ios_ige);
  check_macro (dim  < base::map_dimension(), "unexpected dimension = " << dim << " > domain dimension = " << base::map_dimension());
  return base::_omega.ios_ige2dis_ige (dim, ios_ige);
}
template<class T>
void
geo_domain_indirect_rep<T,distributed>::set_ios_permutation (
  disarray<size_type,distributed>& idof2ios_dis_idof) const
{
  fatal_macro ("set_ios_permutation: not yet");
}
// used by space_constritution for ios numbering
template<class T>
const std::array<disarray<typename geo_domain_indirect_rep<T,distributed>::size_type,distributed>,reference_element::max_variant>&
geo_domain_indirect_rep<T,distributed>::get_igev2ios_dis_igev() const
{
  fatal_macro ("get_igev2ios_dis_igev: not yet");
  return base::get_background_geo().get_igev2ios_dis_igev(); // return something
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class geo_domain_indirect_rep<Float,distributed>;

} // namespace rheolef
#endif // _RHEOLEF_HAVE_MPI
