# ifndef _RHEOLEF_TEST_H
# define _RHEOLEF_TEST_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@classfile test test-function in variational formulation

Description
===========
This class, and its associated `trial` one,
is used for test and trial functions
involved in variational formulations.
Variational formulations are specified by expressions
of the C++ language.

A `test` function is the formal argument involved
in the expression for the @ref integrate_3 function:

        geo omega ("circle");
        space Xh (omega, "P1");
        test v (Xh);
        field lh = integrate (omega, 2*v);

For a bilinear @ref form_2,
the `test` function represents its second formal argument,
while its first one is referred to as the `trial` one:
  
        trial u (Xh);
        test  v (Xh);
        form a = integrate (omega, dot(grad(u),grad(v)));

Implementation
==============
@showfromfile
The `test` and `trial` classes are simply aliases
to the `test_basic` class:

@snippet test.h verbatim_test
@par

The `test_basic` class provides an interface,
via the @ref smart_pointer_7 class family,
to a data container:

@snippet test.h verbatim_test_basic
@snippet test.h verbatim_test_basic_cont
*/
} // namespace rheolef

#include "rheolef/space.h"
#include "rheolef/fem_on_pointset.h"
#include "rheolef/band.h"
#include "rheolef/piola_util.h"
#include "rheolef/vf_tag.h"

namespace rheolef {

namespace details {

// forward declaration:
template <class T, class M, class VfTag> class test_component;

} // namespace details


template <class T, class M>
class test_rep {
public :
// typedefs:

    using size_type      = std::size_t;
    using memory_type    = M;
    using value_type     = undeterminated_basic<T>;
    using scalar_type    = T;
    using float_type     = typename float_traits<T>::type;
    using geo_type       = geo_basic  <float_type,M>;
    using space_type     = space_basic<float_type,M>;
    using diff_type      = details::differentiate_option::type;
    using is_elementwise = std::true_type;

// allocator/deallocator:
  
    explicit test_rep (const space_type& V);
    test_rep (const test_rep<T,M>&);
    test_rep<T,M>& operator= (const test_rep<T,M>&);

// accessors:

    const space_type&  get_vf_space()  const { return _V; }
    static const space_constant::valued_type valued_hint = space_constant::last_valued;
    space_constant::valued_type valued_tag() const { return get_vf_space().valued_tag(); }
    size_type n_derivative() const { return 0; }

// initializers:

    void initialize (
      const piola_on_pointset<float_type>&             pops,
      const integrate_option&                          iopt);
    void initialize (
      const band_basic<float_type,memory_type>&        gh,
      const piola_on_pointset<float_type>&             pops,
      const integrate_option&                          iopt);
    void initialize (
      const space_basic<float_type,memory_type>&       Xh,
      const piola_on_pointset<float_type>&             pops,
      const integrate_option&                          iopt);

// evaluators:

    template<class Value, diff_type Diff>
    void evaluate (
      const geo_basic<T,M>&                               omega_K,
      const geo_element&                                  K,
      const details::differentiate_option&                gopt,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;

    template<class Value, diff_type Diff>
    void evaluate_on_side (
      const geo_basic<T,M>&                               omega_K,
      const geo_element&                                  K,
      const side_information_type&                        sid,
      const details::differentiate_option&                gopt,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;

    template<class Value>
    void valued_check() const {
      space_constant::valued_type valued_tag = space_constant::valued_tag_traits<Value>::value;
      check_macro (_V.valued_tag() == valued_tag, "unexpected "<<_V.valued()
        << "-valued field while a " << space_constant::valued_name(valued_tag)
        << "-valued one is expected in expression");
    }
    template<class Value>
    void grad_valued_check() const {
      typedef typename space_constant::rank_down<Value>::type A1;
      space_constant::valued_type arg_valued_tag = space_constant::valued_tag_traits<A1>::value;
      check_macro (_V.valued_tag() == arg_valued_tag, "grad(): unexpected "<<_V.valued()
        << "-valued field while a " << space_constant::valued_name(arg_valued_tag)
        << "-valued one is expected in expression");
    }
    template<class Value>
    void div_valued_check() const {
      typedef typename space_constant::rank_up<Value>::type A1;
      space_constant::valued_type arg_valued_tag = space_constant::valued_tag_traits<A1>::value;
      check_macro (_V.valued_tag() == arg_valued_tag, "div(): unexpected "<<_V.valued()
        << "-valued field while a " << space_constant::valued_name(arg_valued_tag)
        << "-valued one is expected in expression");
    }
    template<class Value>
    void curl_valued_check() const {
 	// TODO: depend on dim
    }
    template<class Value>
    void local_dg_merge_on_side (
      const geo_basic<T,M>&                                     omega_K,
      const geo_element&                                        S,
      const geo_element&                                        K0,
      const geo_element&                                        K1,
      const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value0,
      const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value1,
        Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&     value) const;

protected:
// internals
    void _element_initialize         (const geo_element& K) const;
    void _element_initialize_on_side (const geo_element& K, const side_information_type& sid); //BUG with const for DG
    void _evaluate_init              (const reference_element& hat_K) const;

// data:
public:
    space_type                        _V;
    fem_on_pointset<float_type>       _fops;
    bool                              _is_inside_on_local_sides;
    bool                              _is_on_band;
    band_basic<float_type,M>          _gh;
};
// ----------------------
// smart_pointer version:
// ----------------------
// [verbatim_test_basic]
template <class T, class M, class VfTag>
class test_basic : public smart_pointer<test_rep<T,M> > {
public :
// typedefs:

    using rep  = test_rep<T,M>;
    using base = smart_pointer<rep>;
    using size_type      = typename rep::size_type;
    using memory_type    = typename rep::memory_type;
    using value_type     = typename rep::value_type;
    using scalar_type    = typename rep::scalar_type;
    using float_type     = typename rep::float_type;
    using geo_type       = typename rep::geo_type;
    using space_type     = typename rep::space_type;
    using diff_type      = typename rep::diff_type;
    using is_elementwise = typename rep::is_elementwise;
    using vf_tag_type      = VfTag;
    using vf_dual_tag_type = typename details::dual_vf_tag<VfTag>::type;
    using self_type        = test_basic<T,M,VfTag>;
    using dual_self_type   = test_basic<T,M,vf_dual_tag_type>;

// allocator/deallocator:
  
    explicit test_basic (const space_type& V) : base(new_macro(rep(V))) {}

// accessors:

    const space_type&  get_vf_space()  const { return base::data().get_vf_space(); }
    static const space_constant::valued_type valued_hint = rep::valued_hint;
    space_constant::valued_type valued_tag() const { return base::data().valued_tag(); }
    size_type n_derivative() const { return base::data().n_derivative(); }

    size_type size() const { return get_vf_space().size(); }
    details::test_component<T,M,VfTag> operator[] (size_type i_comp) const;
// [verbatim_test_basic]

// mutable modifiers:

    void initialize (
      const piola_on_pointset<float_type>&             pops,
      const integrate_option&                          iopt)
    			{ base::data().initialize (pops, iopt); }
    void initialize (
      const band_basic<float_type,memory_type>&        gh,
      const piola_on_pointset<float_type>&             pops,
      const integrate_option&                          iopt)
    			{ base::data().initialize (gh, pops, iopt); }
    void initialize (
      const space_basic<float_type,memory_type>&       Xh,
      const piola_on_pointset<float_type>&             pops,
      const integrate_option&                          iopt)
    			{ base::data().initialize (Xh, pops, iopt); }

// evaluators :

    template<class Value, diff_type Diff>
    void evaluate (
      const geo_basic<T,M>&                               omega_K,
      const geo_element&                                  K,
      const details::differentiate_option&                gopt,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const 
    	{ base::data().template evaluate<Value,Diff> (omega_K, K, gopt, value); }

    template<class Value, diff_type Diff>
    void evaluate_on_side (
      const geo_basic<T,M>&                               omega_K,
      const geo_element&                                  K,
      const side_information_type&                        sid,
      const details::differentiate_option&                gopt,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                do_local_component_assembly_unused) const
    	{ base::data().template evaluate_on_side<Value,Diff> (omega_K, K, sid, gopt, value); }

    // abbreviation: evaluate without differentiation
    template<class Value>
    void evaluate (
      const geo_basic<T,M>&                               omega_K,
      const geo_element&                                  K,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const 
    {
      details::differentiate_option none;
      base::data().template evaluate<Value,details::differentiate_option::none> (omega_K, K, none, value);
    }
    template<class Value>
    void evaluate_on_side (
      const geo_basic<T,M>&                               omega_K,
      const geo_element&                                  K,
      const side_information_type&                        sid,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                do_local_component_assembly_unused) const
    {
      details::differentiate_option none;
      base::data().template evaluate_on_side<Value,details::differentiate_option::none> (omega_K, K, sid, none, value);
    }
    template<class Value>
    void valued_check() const      { base::data().template valued_check<Value>(); }
    template<class Value>
    void grad_valued_check() const { base::data().template grad_valued_check<Value>(); }
    template<class Value>
    void div_valued_check() const  { base::data().template div_valued_check<Value>(); }
    template<class Value>
    void curl_valued_check() const { base::data().template curl_valued_check<Value>(); }

    template<class Value>
    void local_dg_merge_on_side (
      const geo_basic<T,M>&                                     omega_K,
      const geo_element&                                        S,
      const geo_element&                                        K0,
      const geo_element&                                        K1,
      const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value0,
      const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value1,
            Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
	{ base::data().local_dg_merge_on_side (omega_K, S, K0, K1, value0, value1, value); }
// [verbatim_test_basic_cont]
};
// [verbatim_test_basic_cont]

// [verbatim_test]
typedef test_basic<Float,rheo_default_memory_model,details::vf_tag_01> test;
//! @class trial
//! @brief see the @ref test_2 page for the full documentation
typedef test_basic<Float,rheo_default_memory_model,details::vf_tag_10> trial;
// [verbatim_test]

}// namespace rheolef
# endif /* _RHEOLEF_TEST_H */
