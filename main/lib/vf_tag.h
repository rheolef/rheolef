#ifndef _RHEOLEF_VF_TAG_H
#define _RHEOLEF_VF_TAG_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// vf_tag is used to determine at compile time when a binary 
// variationnal expression as : u*u, u*v, f*v, v*v*u, etc...
// is linear which respect to u or v, where: trial u; test v;
// 
#include "rheolef/compiler.h"
#include "rheolef/expression.h"
//#include "rheolef/field_nonlinear_expr.h"
//#include "rheolef/field_nonlinear_expr_ops.h"
namespace rheolef { namespace details {

// ----------------------------------------------------------
// define tag: pair of compile-time booleans
// that expresses the linearity with respect to each variable
// ----------------------------------------------------------
struct                                      vf_tag_nonlinear {};
typedef std::pair<std::false_type,std::false_type>  vf_tag_00;
typedef std::pair<std::false_type,std::true_type >  vf_tag_01;
typedef std::pair<std::true_type, std::true_type >  vf_tag_11;
typedef std::pair<std::true_type, std::false_type>  vf_tag_10;

// ------------------------------------------------
// dual vf tag: 01 <--> 10 i.e. swap test and trial
// ------------------------------------------------
template <class G>
struct dual_vf_tag                { typedef vf_tag_nonlinear type; };

template<> struct dual_vf_tag<vf_tag_01> { typedef vf_tag_10 type; };
template<> struct dual_vf_tag<vf_tag_10> { typedef vf_tag_01 type; };
template<> struct dual_vf_tag<vf_tag_00> { typedef vf_tag_00 type; };
template<> struct dual_vf_tag<vf_tag_11> { typedef vf_tag_11 type; };

// --------------------------------------------------------------------
// unary operator: default is nonlinear with respect to each variable
// --------------------------------------------------------------------
template <class Op, class G>
struct uf_vf_tag                                            { typedef vf_tag_nonlinear type; };

// TODO: unary +-

// --------------------------------------------------------------------
// binary operator: default is nonlinear with respect to each variable
// --------------------------------------------------------------------
template <class Op, class G1, class G2>
struct bf_vf_tag                                            { typedef vf_tag_nonlinear type; };

// Note: operators plus, multiples, etc are defined in field_nonlinear_expr_ops.h
// they should be grouped in one separated file with vf_tag, or tag defined for each
// operator in field_nonlinear_expr_ops_make.cc ?
// -----------------------------
// plus/minus
// -----------------------------
template<class Tag> struct bf_vf_tag<plus, Tag,Tag> { typedef Tag type; };
template<class Tag> struct bf_vf_tag<minus,Tag,Tag> { typedef Tag type; };

// -----------------------------
// multiplies
// -----------------------------
template<> struct bf_vf_tag<multiplies,vf_tag_00,vf_tag_00> { typedef vf_tag_00 type; };

template<> struct bf_vf_tag<multiplies,vf_tag_00,vf_tag_01> { typedef vf_tag_01 type; };
template<> struct bf_vf_tag<multiplies,vf_tag_01,vf_tag_00> { typedef vf_tag_01 type; };

template<> struct bf_vf_tag<multiplies,vf_tag_00,vf_tag_10> { typedef vf_tag_10 type; };
template<> struct bf_vf_tag<multiplies,vf_tag_10,vf_tag_00> { typedef vf_tag_10 type; };

template<> struct bf_vf_tag<multiplies,vf_tag_01,vf_tag_10> { typedef vf_tag_11 type; };
template<> struct bf_vf_tag<multiplies,vf_tag_10,vf_tag_01> { typedef vf_tag_11 type; };
template<> struct bf_vf_tag<multiplies,vf_tag_00,vf_tag_11> { typedef vf_tag_11 type; };
template<> struct bf_vf_tag<multiplies,vf_tag_11,vf_tag_00> { typedef vf_tag_11 type; };


}} // namespace rheolef::details
#endif // _RHEOLEF_VF_TAG_H
