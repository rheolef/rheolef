///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// geo: vtk input
//
// author: Pierre.Saramito@imag.fr
//
// 5 march 2012
//
#include "rheolef/geo.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"

#include "vtk_cell_type.h"

namespace rheolef {
using namespace std;

// skip optional line, e.g.
//    "OFFSETS vtktypeint64"
//    "CONNECTIVITY vtktypeint64"
// that do not start by a digit
// if a digit is founded, returns an empty string
// otherwise, returns the first keyword, e.g. "OFFSETS" or "CONNECTIVITY"
// and skip until the end of line
//
static
string
skip_optional_line (istream& is) 
{
  is >> ws;
  char c = is.peek();
  if (isdigit(c)){
    return string("");
  }
  string key;
  is >> key;
  c = is.peek();
  while (c != '\n' && is.good()) {
    is.get(c);
  }
  return key;
}
template <class T>
idiststream&
geo_get_vtk (idiststream& ips, geo_basic<T,sequential>& omega)
{
  using namespace std;
  typedef typename geo_basic<T,sequential>::size_type size_type;
  typedef typename geo_basic<T,sequential>::node_type node_type;

  check_macro (ips.good(), "bad input stream for vtk");
  istream& is = ips.is();
  geo_header hdr;
  hdr.order     = 1;
  // ------------------------------------------------------------------------
  // 1) load the coordinates
  // 		POINTS <np> float
  //          {xi yi zi} i=0..np-1
  // ------------------------------------------------------------------------
  check_macro (scatch(is,"POINTS"), "unexpected vtk input file");
  size_type nnod;
  is >> nnod;
  if (nnod == 0) {
    warning_macro("empty vtk mesh file");
    return ips;
  }
  disarray<node_type, sequential> node (nnod);
  scatch (is,"float");
  node.get_values (ips);
  check_macro (ips.good(), "bad input stream for vtk");
  // ------------------------------------------------------------------------
  // 1.2) compute dimension
  // ------------------------------------------------------------------------
  point xmin, xmax;
  for (size_type k = 0; k < 3; ++k) {
    xmin[k] = std::numeric_limits<T>::max();
    xmax[k] = std::numeric_limits<T>::min();
  }
  for (size_t i = 0, n = node.size(); i < n; ++i) {
    for (size_type k = 0; k < 3; ++k) {
      xmin[k] = std::min (node[i][k], xmin[k]);
      xmax[k] = std::max (node[i][k], xmax[k]);
    }
  }
  hdr.dimension = 3;
  for (size_type k = 0; k < 3; ++k) {
    if (fabs(xmax[k] - xmin[k]) < std::numeric_limits<T>::epsilon())
      hdr.dimension--;
  }
  // ------------------------------------------------------------------------
  // 2) load the domain connectivity
  //	LINES <ne> <ncs>
  //    2  {s1 s2} j=0..ne-1
  // or
  //    POLYGONS <ne> <ncs>
  //    p  {s1 .. sp} j=0..ne-1
  // ------------------------------------------------------------------------
  // 2.1) get connectivity header
  string mark;
  is >> ws >> mark;
  if (mark == "VERTICES") { // skip unused paragraph in polydata v3.0
    size_type n, n2, d, idx;
    is >> n >> n2;
    for (size_type i = 0; i < n; i++) {
      is  >> d >> idx;
    }
  }
  do {
    if (mark == "POLYGONS" || mark == "LINES" || mark == "CELLS") break;
  } while (is >> ws >> mark);
  bool have_vtk_cell_type = false;
  string mesh_fmt = mark;
       if (mesh_fmt == "LINES")    hdr.map_dimension = 1;
  else if (mesh_fmt == "POLYGONS") hdr.map_dimension = 2;
  else if (mesh_fmt == "CELLS")  { hdr.map_dimension = 0; have_vtk_cell_type = true; } // yet unknown
  else error_macro ("geo: unexpected `" << mesh_fmt << "' in vtk input."
              << " Expect LINES, POLYGONS or CELLS.");
  // note: when mesh_fmt == "CELLS", we will also get CELL_TYPES and deduce the map_dimension
  size_type ne, ncs;
  is >> ne >> ncs;
  // skip optional line: "OFFSETS vtktypeint64"
  mark = skip_optional_line (is);
  // 2.2) get all connectivity in a flat trash array
  std::vector<size_type> cell_trash (ncs);
  std::vector<size_type> vtk_cell_type (ne);
  typedef geo_element_auto<>             element_t;
  typedef disarray<element_t,sequential> disarray_t;
  disarray_t elt (ne);
  if (mark == "") {
    // old vtk polydata format 
    for (size_type ics = 0; ics < ncs; ics++) {
      is >> cell_trash [ics];
    }
    // skip optional line: "CONNECTIVITY vtktypeint64"
    if (have_vtk_cell_type) {
      is >> ws >> mark;
      check_macro (mark == "CELL_TYPES", "geo: unexpected `" << mark << "' in vtk input."
                << " Expect CELL_TYPES.");
      size_type ne2;
      is >> ne2;
      check_macro (ne == ne2, "geo: unexpected CELL_TYPES size=`" << ne2 << "' , expect size="<<ne);
      for (size_type ie = 0; ie < ne; ie++) {
        is >> vtk_cell_type [ie];
      }
    }
    for (size_type ie = 0, ics = 0; ie < ne; ie++) {
      size_type nloc = cell_trash [ics];
      ics++;
      size_type variant = (!have_vtk_cell_type) ?
                          reference_element::variant (nloc, hdr.map_dimension) :
                          vtk_cell_type2variant (vtk_cell_type [ie]);
      elt[ie].reset (variant, hdr.order);
      for (size_type iloc = 0 ; iloc < nloc; iloc++, ics++) {
        elt[ie][iloc] = cell_trash [ics];
      }
      hdr.dis_size_by_dimension [elt[ie].dimension()]++;
      hdr.dis_size_by_variant   [elt[ie].variant()]++;
      hdr.map_dimension = std::max (hdr.map_dimension, elt[ie].dimension());
    }
    hdr.dis_size_by_dimension [0] = nnod;
    hdr.dis_size_by_variant   [0] = nnod;
  } else {
    // new vtk polydata format 
    for (size_t count_key = 0; mark == "OFFSETS" || mark == "CONNECTIVITY"; ++count_key) {
      if (mark == "OFFSETS") {
        size_type prec_offset = 0;
        is >> prec_offset;
        if (ne > 0) ne--; // first offset is zero
        for (size_type ie = 0; ie < ne; ie++) {
          size_type offset;
          is >> offset;
          size_type cell_nv = size_type(offset - prec_offset);
          vtk_cell_type [ie] = nv2vtk_cell_type (hdr.map_dimension, cell_nv);
          prec_offset = offset;
        }
      } else { // CONNECTIVITY
        for (size_type ics = 0; ics < ncs; ics++) {
          is >> cell_trash [ics];
        }
      }
      if (count_key == 1) break;
      // else : read next keyword
      mark = skip_optional_line (is);
    }
    for (size_type ie = 0, ics = 0; ie < ne; ie++) {
      size_type variant = vtk_cell_type2variant (vtk_cell_type [ie]);
      elt[ie].reset (variant, hdr.order);
      size_type nloc = elt [ie].size();
      for (size_type iloc = 0 ; iloc < nloc; iloc++, ics++) {
        check_macro (ics < ncs, "unexpected ics="<<ics<<" out of range [0:"<<ncs<<"[");
        elt[ie][iloc] = cell_trash [ics];
      }
      hdr.dis_size_by_dimension [elt[ie].dimension()]++;
      hdr.dis_size_by_variant   [elt[ie].variant()]++;
      hdr.map_dimension = std::max (hdr.map_dimension, elt[ie].dimension());
    }
    hdr.dis_size_by_dimension [0] = nnod;
    hdr.dis_size_by_variant   [0] = nnod;
  }
  // ------------------------------------------------------------------------
  // 2) split elements by variants
  // ------------------------------------------------------------------------
  std::array<disarray_t, reference_element::max_variant>  tmp_geo_element;
  if (hdr.map_dimension > 0) {
    for (size_type variant = reference_element::first_variant_by_dimension(hdr.map_dimension);
                   variant < reference_element:: last_variant_by_dimension(hdr.map_dimension); variant++) {
      geo_element_auto<> init_val (variant, hdr.order);
      tmp_geo_element [variant] = disarray_t (hdr.dis_size_by_variant [variant], init_val);
    }
    std::array<size_type, 4> counter;
    counter.fill(0);
    for (size_type ie = 0; ie < ne; ie++) {
      size_type variant = elt[ie].variant();
      size_type ige = counter[variant];
      tmp_geo_element [variant] [ige] = elt[ie];
      counter[variant]++;
    }
  }
  // ------------------------------------------------------------------------
  // 3) build & upgrade
  // ------------------------------------------------------------------------
  omega.build_from_data (hdr, node, tmp_geo_element, true);
  return ips;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template idiststream& geo_get_vtk<Float> (idiststream&, geo_basic<Float,sequential>&);

}// namespace rheolef
