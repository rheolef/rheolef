# ifndef _RHEO_IOFEM_H
# define _RHEO_IOFEM_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

/*Class:iofem
NAME: @code{iofem} - input and output finite element manipulators 
@clindex iofem
DESCRIPTION:       
    @noindent 
    This class implements some specific finite element manipulators.
    For a general presentation of stream manipulators, reports
    to class @code{iostream}.

VALUATED MANIPULATORS:
    @table @code
@cindex origin
@cindex normal
@findex origin
@findex normal
    @itemx origin
    @itemx normal
    	set a cutting plane for visualizations and post-processing.
        @example
	    cout << cut << origin(point(0.5, 0.5, 0.5)) << normal(point(1,1,0) << uh;
        @end example
@cindex topography
@findex topography
    @itemx topography
       specifies the topography field when representing a bidimensionnal
	field in tridimensionnal elevation.
        @example
	    cout << topography(zh) << uh;
        @end example
       This manipulator takes an agument that specifies a scalar field value
       @code{zh} for the elevation, while @code{uh} is the scalar field
       to represent. Then, the z-elevation takes zh(x,y)+uh(x,y).
    @end table

AUTHOR: 
    Pierre.Saramito@imag.fr
DATE:   4 february 1997
End: */

#include "rheolef/iorheobase.h"
#include "rheolef/field.h"
namespace rheolef { 

class iofem {
public:
	// ------------------------------------------------------
	// options associated to a value
	// ------------------------------------------------------
#define o_scalar(t, a)  iorheobase_def_scalar_macro (iofem,t, a)
        o_scalar (field_sequential, topography)                          
        o_scalar (point, origin)                          
        o_scalar (point, normal)                          
        o_scalar (point_basic<size_t>, resolution)                          
# undef o_scalar

	// ------------------------------------------------------
	// 3) flags access
	// ------------------------------------------------------
public:
	//     basics members
	iofem();
	~iofem();

	static long flags  (std::ios& s);
	static long flags  (std::ios& s, long f);
	static long setf   (std::ios& s, long add_f);
	static long setf   (std::ios& s, long add_f, long field);
	static long unsetf (std::ios& s, long del_f);

protected:
	// ------------------------------------------------------
	// memory handler
	// ------------------------------------------------------
protected:
	// local memory handler
	static iofem* get_pointer (std::ios& s);
};
#define o_scalar(t, a)  iorheobase_manip_scalar_macro (iofem,t, a, o)
        o_scalar (field_sequential, topography)                          
        o_scalar (point, origin)                          
        o_scalar (point, normal)                          
        o_scalar (point_basic<size_t>, resolution)                          
# undef o_scalar
}// namespace rheolef
# endif /* _RHEO_IOFEM_H */
