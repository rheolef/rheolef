#ifndef _RHEOLEF_TEST_COMPONENT_H
#define _RHEOLEF_TEST_COMPONENT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// test[i] : indexation for test and trial functions
//
#include "rheolef/test.h"

namespace rheolef { namespace details {

// =========================================================================
// test_component
// =========================================================================
template <class T, class M, class VfTag>
class test_component_rep {
public:

// typename:

  typedef test_basic<T,M,VfTag>                     test_type;
  typedef typename test_type::size_type             size_type;
  typedef typename test_type::value_type            value_type;
  typedef M                                         memory_type;
  typedef typename test_type::scalar_type           scalar_type;
  typedef typename test_type::float_type            float_type;
  typedef details::differentiate_option::type       diff_type;
  typedef space_basic<float_type,M>                 space_type;

// allocators:

  test_component_rep (const test_basic<T,M,VfTag>&     u, size_type i_comp);
  test_component_rep (const test_component<T,M,VfTag>& u, size_type i_comp);
  test_component_rep (const test_component_rep<T,M,VfTag>&);
  test_component_rep<T,M,VfTag>& operator= (const test_component_rep<T,M,VfTag>&);

// accessors:

  const space_type&  get_vf_space()  const { return _u.get_vf_space(); }
  static const space_constant::valued_type valued_hint = space_constant::last_valued;
  space_constant::valued_type valued_tag() const { return _u_comp.get_vf_space().valued_tag(); }

// initializers:

  void initialize (
    const piola_on_pointset<T>& pops,
    const integrate_option&     iopt) 
      { _u_comp.initialize (pops, iopt); }
  void initialize (
    const band_basic<float_type,memory_type>& gh,
    const piola_on_pointset<T>&               pops,
    const integrate_option&                   iopt)
      { _u_comp.initialize (gh, pops, iopt); _is_on_band = true; }
  void initialize (
    const space_basic<float_type,memory_type>& Xh,
    const integrate_option&                    iopt) 
      { _u_comp.initialize (Xh, iopt); }

// evaluators:

  template<class Value, details::differentiate_option::type Diff>
  void evaluate (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;

  template<class Value, details::differentiate_option::type Diff>
  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value,
    bool                                                do_local_component_assembly) const;

  template<class Value>
  void local_dg_merge_on_side (
    const geo_basic<T,M>&                                     omega_K,
    const geo_element&                                        S,
    const geo_element&                                        K0,
    const geo_element&                                        K1,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value0,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value1,
          Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;

// checks:

  template<class ValueType>
  void valued_check() const {
    space_constant::valued_type valued_tag = space_constant::valued_tag_traits<ValueType>::value;
    check_macro (_u_comp.valued_tag() == valued_tag, "unexpected "<<_u_comp.get_vf_space().valued()
      << "-valued field while a " << space_constant::valued_name(valued_tag)
      << "-valued one is expected in expression");
  }
  template<class ValueType>
  void grad_valued_check() const {
    typedef typename space_constant::rank_down<ValueType>::type A1;
    space_constant::valued_type arg_valued_tag = space_constant::valued_tag_traits<A1>::value;
    check_macro (_u_comp.valued_tag() == arg_valued_tag, "grad(): unexpected "<<_u_comp.get_vf_space().valued()
      << "-valued field while a " << space_constant::valued_name(arg_valued_tag)
      << "-valued one is expected in expression");
  }
  template<class ValueType>
  void div_valued_check() const {
    typedef typename space_constant::rank_up<ValueType>::type A1;
    space_constant::valued_type arg_valued_tag = space_constant::valued_tag_traits<A1>::value;
    check_macro (_u_comp.valued_tag() == arg_valued_tag, "div(): unexpected "<<_u_comp.get_vf_space().valued()
      << "-valued field while a " << space_constant::valued_name(arg_valued_tag)
      << "-valued one is expected in expression");
  }

protected:
// internals:
  static space_type _init_space_comp(const space_type& Xh, size_type i_comp);
  void _initialize_numbering (
    const geo_basic<float_type,memory_type>&                  omega_K,
    const geo_element&                                        K) const;
  template<class Value>
  void _evaluate_continued (
    const geo_basic<float_type,memory_type>&                  omega_K,
    const geo_element&                                        K,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value_comp,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&       value) const;
  template<class Value>
  void _evaluate_on_side_continued (
    const geo_basic<float_type,memory_type>&                  omega_K,
    const geo_element&                                        K,
    const side_information_type&                              sid,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value_comp,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&       value) const;

// data:
  size_type                     _i_comp;
  test_basic<T,M,VfTag> 	_u, _u_comp;		// top-level and curent-level test-function u[i0][i1]...[iN]
  const test_component_rep<T,M,VfTag>&  get_u_comp_upper_data() const { return *_ptr_u_comp_upper_data; } 
  bool _has_subcomponent() const { return _ptr_u_comp_upper_data.operator->() != 0; }
  smart_pointer<test_component_rep<T,M,VfTag>> _ptr_u_comp_upper_data;
						        // upper-level test-function u[i0]...[i(N-1)] when N>1
						        // note: clang++ cannot manage directly test_component<..> that has incomplete type
                                                        //       but g++ can, so fall back to smart_pointer for clang++...
  mutable std::array<size_type,reference_element::max_variant> // lazy initialization, on the fly
	                        _loc_ndof,
	                  _first_loc_idof,
	                   _last_loc_idof;
  mutable bool                  _is_on_band;
};
// -------------------------------------------------------------------------
// inlined
// -------------------------------------------------------------------------
template <class T, class M, class VfTag>
test_component_rep<T,M,VfTag>::test_component_rep (const test_component_rep<T,M,VfTag>& x)
: _i_comp         (x._i_comp),
  _u              (x._u),
  _u_comp         (x._u_comp),
  _ptr_u_comp_upper_data   (x._ptr_u_comp_upper_data),
  _loc_ndof       (x._loc_ndof),
  _first_loc_idof (x._first_loc_idof),
  _last_loc_idof  (x._last_loc_idof),
  _is_on_band     (x._is_on_band)
{
  trace_macro ("** PHYSICAL COPY OF TEST_COMPONENT **");
}
template <class T, class M, class VfTag>
test_component_rep<T,M,VfTag>&
test_component_rep<T,M,VfTag>::operator= (const test_component_rep<T,M,VfTag>& x)
{
  trace_macro ("** PHYSICAL ASSIGN OF TEST_COMPONENT **");
  _i_comp         = x._i_comp;
  _u              = x._u;
  _u_comp         = x._u_comp;
  _ptr_u_comp_upper_data   = x._ptr_u_comp_upper_data;
  _loc_ndof       = x._loc_ndof;
  _first_loc_idof = x._first_loc_idof;
  _last_loc_idof  = x._last_loc_idof;
  _is_on_band     = x._is_on_band;
  return *this;
}
template <class T, class M, class VfTag>
test_component_rep<T,M,VfTag>::test_component_rep (const test_basic<T,M,VfTag>& u, size_type i_comp)
 : _i_comp(i_comp),
   _u(u),
   _u_comp (_init_space_comp(u.get_vf_space(),i_comp)),
   _ptr_u_comp_upper_data(),
   _loc_ndof(),
   _first_loc_idof(),
   _last_loc_idof(),
   _is_on_band(false)
{
  _first_loc_idof.fill (std::numeric_limits<size_type>::max());
}
template <class T, class M, class VfTag>
test_component_rep<T,M,VfTag>::test_component_rep (const test_component<T,M,VfTag>& u_comp_upper, size_type i_subcomp)
 : _i_comp(i_subcomp),
   _u(u_comp_upper.data()._u),
   _u_comp (_init_space_comp(u_comp_upper.data()._u_comp.get_vf_space(),i_subcomp)),
   _ptr_u_comp_upper_data(u_comp_upper),
   _loc_ndof(),
   _first_loc_idof(),
   _last_loc_idof(),
   _is_on_band(false)
{
  _first_loc_idof.fill (std::numeric_limits<size_type>::max());
}
template <class T, class M, class VfTag>
typename test_component_rep<T,M,VfTag>::space_type
test_component_rep<T,M,VfTag>::_init_space_comp (const space_type& Xh, size_type i_comp)
{
  check_macro (i_comp < Xh.size(), "test component index "<<i_comp<<" is out of range [0:"<<Xh.size()<<"[");
  space_basic<T,M> Xh_comp = Xh[i_comp];
  return Xh_comp;
}
template <class T, class M, class VfTag>
void
test_component_rep<T,M,VfTag>::_initialize_numbering (
    const geo_basic<float_type,memory_type>&                 omega_K,
    const geo_element&                                       K) const
{
  size_type variant = K.variant();
  if (_has_subcomponent()) {
    const space_type& Xh_upper = get_u_comp_upper_data()._u_comp.get_vf_space();
    get_u_comp_upper_data()._initialize_numbering (omega_K, K); // recursive call
          _loc_ndof[variant] = get_u_comp_upper_data()._loc_ndof      [variant];
    _first_loc_idof[variant] = get_u_comp_upper_data()._first_loc_idof[variant];
    for (size_type j_comp = 0; j_comp < _i_comp; ++j_comp) {
      size_type loc_jcomp_ndof = Xh_upper.get_constitution()[j_comp].assembly_loc_ndof (omega_K, K);
      _first_loc_idof[variant] += loc_jcomp_ndof;
    }
    size_type loc_icomp_ndof = Xh_upper.get_constitution()[_i_comp].assembly_loc_ndof (omega_K, K);
    _last_loc_idof[variant] = _first_loc_idof[variant] + loc_icomp_ndof;
  } else {
    const space_type& Xh_upper = _u.get_vf_space();
          _loc_ndof[variant]   = _u.get_vf_space().get_constitution().assembly_loc_ndof (omega_K, K);
    _first_loc_idof[variant]   = 0;
    for (size_type j_comp = 0; j_comp < _i_comp; ++j_comp) {
      size_type loc_jcomp_ndof = Xh_upper.get_constitution()[j_comp].assembly_loc_ndof (omega_K, K);
      _first_loc_idof[variant] += loc_jcomp_ndof;
    }
    size_type loc_icomp_ndof = 0;
    loc_icomp_ndof = Xh_upper.get_constitution()[_i_comp].assembly_loc_ndof (omega_K, K);
    _last_loc_idof[variant] = _first_loc_idof[variant] + loc_icomp_ndof;
  }
}
template <class T, class M, class VfTag>
template<class Value, details::differentiate_option::type Diff>
void
test_component_rep<T,M,VfTag>::evaluate (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
{
  // 1) evaluate the component: u[i_comp]:
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic> value_comp;
  _u_comp.template evaluate<Value,Diff> (omega_K, K, gopt, value_comp);
  // 2) copy value_comp[] into value[]
  _evaluate_continued (omega_K, K, value_comp, value);
}
template <class T, class M, class VfTag>
template<class Value>
void
test_component_rep<T,M,VfTag>::_evaluate_continued (
    const geo_basic<float_type,memory_type>&                  omega_K,
    const geo_element&                                        K,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value_comp,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&       value) const
{
  check_macro (!_is_on_band, "test_component: not yet on band");
  reference_element hat_K = K;
  _initialize_numbering (omega_K, K);
  size_type loc_comp_ndof = _last_loc_idof[hat_K.variant()] - _first_loc_idof[hat_K.variant()];
  size_type loc_ndof      = _loc_ndof     [hat_K.variant()];
  check_macro (size_type(value_comp.cols()) == loc_comp_ndof, 
     "omega_K="<<omega_K.name()<<", K="<<K.name()<<K.dis_ie()
     <<", space="<<_u.get_vf_space().name()<<", i_comp="<<_i_comp<<", comp_space="<<_u_comp.get_vf_space().name()
     <<", value_comp("<<value_comp.rows()<<","<<value_comp.cols()<<") has incompatible cols size : expect loc_comp_ndof="<<loc_comp_ndof
     << " associated to local dof sub-range ["<<_first_loc_idof[hat_K.variant()]<<":"<< _last_loc_idof[hat_K.variant()]<<"["
     << " in full local dof range [0:"<<loc_ndof<<"[");
  size_type loc_nnod = value_comp.rows();
  value.resize (loc_nnod, loc_ndof);
  value.fill (Value());
  for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
  for (size_type loc_jdof = _first_loc_idof[hat_K.variant()], loc_comp_jdof = 0; loc_comp_jdof < loc_comp_ndof; ++loc_jdof, ++loc_comp_jdof) {
    value (loc_inod,loc_jdof) = value_comp (loc_inod,loc_comp_jdof);
  }}
}
template <class T, class M, class VfTag>
template<class Value, details::differentiate_option::type Diff>
void
test_component_rep<T,M,VfTag>::evaluate_on_side (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value,
    bool                                                do_local_component_assembly) const
{
  if (!do_local_component_assembly) {
    // DG on an internal side: compute only one-side value here
    // as it is bi-valued, it will be later assembled by local_dg_merge_on_side(), when the two values are computed
    _u_comp.template evaluate_on_side<Value,Diff> (omega_K, K, sid, gopt, value, do_local_component_assembly);
    return;
  }
  // on_local_side(DG); is uni-valued as the inner value, then assembled immediately
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic> value_comp;
  _u_comp.template evaluate_on_side<Value,Diff> (omega_K, K, sid, gopt, value_comp, do_local_component_assembly);
  _evaluate_on_side_continued (omega_K, K, sid, value_comp, value);
}
template <class T, class M, class VfTag>
template<class Value>
void
test_component_rep<T,M,VfTag>::_evaluate_on_side_continued (
    const geo_basic<float_type,memory_type>&                  omega_K,
    const geo_element&                                        K,
    const side_information_type&                              sid,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value_comp,
          Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
{
  check_macro (!_is_on_band, "test_component: not yet on band");
  check_macro (K.dimension() > 0, "unexpected 0D element for HDG multiplier");
  size_type sid_map_d = K.dimension() - 1;
  _initialize_numbering (omega_K, K);
  reference_element hat_K = K;
  size_type first_loc_jdof = _first_loc_idof [hat_K.variant()];
  size_type loc_comp_ndof  = _last_loc_idof  [hat_K.variant()] - _first_loc_idof[hat_K.variant()];
  size_type loc_ndof       = _loc_ndof       [hat_K.variant()];
  check_macro (size_type(value_comp.cols()) == loc_comp_ndof, 
     "omega_K="<<omega_K.name()<<", K="<<K.name()<<K.dis_ie()
     <<", space="<<_u.get_vf_space().name()<<", i_comp="<<_i_comp<<", comp_space="<<_u_comp.get_vf_space().name()
     <<", value_comp("<<value_comp.rows()<<","<<value_comp.cols()<<") has incompatible cols size : expect loc_comp_ndof="<<loc_comp_ndof
     << " associated to local dof sub-range ["<<_first_loc_idof[hat_K.variant()]<<":"<< _last_loc_idof[hat_K.variant()]<<"["
     << " in full local dof range [0:"<<loc_ndof<<"[");
  check_macro (first_loc_jdof + loc_comp_ndof <= loc_ndof, "invalid sizes");
  size_type loc_nnod = value_comp.rows();
  value.resize (loc_nnod, loc_ndof);
  value.fill (Value());
  for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
  for (size_type loc_jdof = first_loc_jdof, loc_comp_jdof = 0; loc_comp_jdof < loc_comp_ndof; ++loc_jdof, ++loc_comp_jdof) {
    value (loc_inod,loc_jdof) = value_comp (loc_inod,loc_comp_jdof);
  }}
}
template <class T, class M, class VfTag>
template<class Value>
void
test_component_rep<T,M,VfTag>::local_dg_merge_on_side (
    const geo_basic<T,M>&                                     omega_K,
    const geo_element&                                        S,
    const geo_element&                                        K0,
    const geo_element&                                        K1,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value_comp0,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value_comp1,
          Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
{
trace_macro("local_dg_merge_on_side: omega_K="<<omega_K.name()<<", S="<<S.name()<<S.dis_ie()<<"...");
  _initialize_numbering (omega_K, S);
  reference_element hat_S = S;
  size_type loc_nnod       = value_comp0.rows();
  size_type loc_ndof       = _loc_ndof       [hat_S.variant()];
  size_type loc_comp_ndof  = _last_loc_idof  [hat_S.variant()] - _first_loc_idof[hat_S.variant()];
  size_type first_loc_jdof = _first_loc_idof [hat_S.variant()];
  check_macro (size_type(value_comp0.cols() + value_comp1.cols()) == loc_comp_ndof, 
     "omega_K="<<omega_K.name()<<", S="<<S.name()<<S.dis_ie()
     <<", space="<<_u.get_vf_space().name()<<", i_comp="<<_i_comp<<", comp_space="<<_u_comp.get_vf_space().name()
     <<", value_comp0("<<value_comp0.rows()<<","<<value_comp0.cols()<<") and "
     << " value_comp1("<<value_comp1.rows()<<","<<value_comp1.cols()<<") have incompatible cols sizes : expect loc_comp_ndof="<<loc_comp_ndof
     << " associated to local dof sub-range ["<<_first_loc_idof[hat_S.variant()]<<":"<< _last_loc_idof[hat_S.variant()]<<"["
     << " in full local dof range [0:"<<loc_ndof<<"[");
  check_macro (value_comp0.rows() == value_comp1.rows(), "invalid sizes");
  check_macro (first_loc_jdof + loc_comp_ndof <= loc_ndof, "invalid sizes");
  value.resize (loc_nnod, loc_ndof);
  value.fill (Value());
  for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
    for (size_type loc_jdof = first_loc_jdof, loc_comp0_jdof = 0, loc_comp0_ndof = value_comp0.cols();
         loc_comp0_jdof < loc_comp0_ndof; ++loc_jdof, ++loc_comp0_jdof) {
      value (loc_inod,loc_jdof) = value_comp0 (loc_inod,loc_comp0_jdof);
    }
    for (size_type loc_jdof = first_loc_jdof + value_comp0.cols(), loc_comp1_jdof = 0, loc_comp1_ndof = value_comp1.cols();
         loc_comp1_jdof < loc_comp1_ndof; ++loc_jdof, ++loc_comp1_jdof) {
      value (loc_inod,loc_jdof) = value_comp1 (loc_inod,loc_comp1_jdof);
    }
  }
trace_macro("local_dg_merge_on_side: omega_K="<<omega_K.name()<<", S="<<S.name()<<S.dis_ie()<<" done");
}
// =========================================================================
// test_component
// =========================================================================
template <class T, class M, class VfTag>
class test_component: public smart_pointer<test_component_rep<T,M,VfTag> > {
public:

// typename:

  typedef test_component_rep<T,M,VfTag>             rep; 
  typedef smart_pointer<rep>                        base; 
  typedef test_basic<T,M,VfTag>                     test_type;
  typedef typename test_type::size_type             size_type;
  typedef typename test_type::value_type            value_type;
  typedef M                                         memory_type;
  typedef typename test_type::scalar_type           scalar_type;
  typedef typename test_type::float_type            float_type;
  typedef details::differentiate_option::type       diff_type;
  typedef geo_basic<float_type,M>                   geo_type;
  typedef space_basic<float_type,M>                 space_type;
  typedef VfTag                                     vf_tag_type;
  typedef typename details::dual_vf_tag<VfTag>::type
                                                    vf_dual_tag_type;
  typedef test_component<T,M,VfTag>                 self_type;
  typedef test_component<T,M,vf_dual_tag_type>      dual_self_type;

// allocators:

  test_component () : base(0) {}
  test_component (const test_component<T,M,VfTag>& u) : base(u) {}
  test_component (const test_basic<T,M,VfTag>&     u, size_type i_comp) : base(new_macro(rep(u,i_comp))) {}
  test_component (const test_component<T,M,VfTag>& u, size_type i_comp) : base(new_macro(rep(u,i_comp))) {}

// recursive call:

  test_component<T,M,VfTag> operator[] (size_t i_comp) const { return test_component<T,M,VfTag> (*this, i_comp); }

// accessors:

  const space_type&  get_vf_space()  const { return base::data().get_vf_space(); }
  static const space_constant::valued_type valued_hint = rep::valued_hint;
  space_constant::valued_type valued_tag() const { return base::data().valued_tag(); }
  size_type n_derivative() const { return 0; }

// initializers:

  void initialize (
    const piola_on_pointset<T>& pops,
    const integrate_option&     iopt)
      { base::data().initialize (pops, iopt); }
  void initialize (
    const band_basic<float_type,memory_type>& gh,
    const piola_on_pointset<T>&               pops,
    const integrate_option&                   iopt)
      { base::data().initialize (gh, pops, iopt); }
  void initialize (
    const space_basic<float_type,memory_type>& Xh,
    const integrate_option&                    iopt)
      { base::data().initialize (Xh, iopt); }

// evaluators:

  template<class Value, diff_type Diff>
  void evaluate (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
      { return base::data().template evaluate<Value,Diff> (omega_K, K, gopt, value); }

  template<class Value, diff_type Diff>
  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value,
    bool                                                do_local_component_assembly) const
      { return base::data().template evaluate_on_side<Value,Diff> (omega_K, K, sid, gopt, value, do_local_component_assembly); }

  // abbreviation: evaluate without differentiation
  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    details::differentiate_option none;
    base::data().template evaluate<Value,details::differentiate_option::none> (omega_K, K, none, value);
  }
  template<class Value>
  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value,
    bool                                                do_local_component_assembly) const
  {
    details::differentiate_option none;
    base::data().template evaluate_on_side<Value,details::differentiate_option::none> (omega_K, K, sid, none, value, do_local_component_assembly);
  }



  template<class Value>
  void local_dg_merge_on_side (
    const geo_basic<T,M>&                                     omega_K,
    const geo_element&                                        S,
    const geo_element&                                        K0,
    const geo_element&                                        K1,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value0,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value1,
          Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
      { return base::data().local_dg_merge_on_side (omega_K, S, K0, K1, value0, value1, value); }

// checks:

  template<class ValueType>
  void valued_check() const { base::data().template valued_check<ValueType>(); }
  template<class ValueType>
  void grad_valued_check() const { base::data().template grad_valued_check<ValueType>(); }
  template<class ValueType>
  void div_valued_check() const { base::data().template div_valued_check<ValueType>(); }

};

} // namespace details
// -------------------------------------------------------------------------
// test_basic accessor:
// -------------------------------------------------------------------------
template <class T, class M, class VfTag>
inline
details::test_component<T,M,VfTag>
test_basic<T,M,VfTag>::operator[] (size_type i_comp) const
{
  return details::test_component<T,M,VfTag> (*this, i_comp);
}

} // namespace rheolef
#endif // _RHEOLEF_TEST_COMPONENT_H
