#ifndef _RHEOLEF_EXPRESSION_H
#define _RHEOLEF_EXPRESSION_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// ==========================================================================
// author: Pierre.Saramito@imag.fr
// date: 25 march 2013


namespace rheolef {
/**
@functionfile expression involved by interpolate and integrate

Synopsis
========

    template <class Expression>
    field interpolate (const space& Xh, const Expression& expr);

    template <class Expression>
    Result integrate (const Expression& expr);

Description
===========
The expressions involved by @ref interpolate_3 and @ref integrate_3 functions
base on several predefined operators and functions that are reviewed here.
Let `phi`, `psi` be two scalar-valued @ref field_2 or @ref test_2 objects.
Similarly, let `u`, `v` be two vector-valued objects
and `sigma` and `tau` be two tensor-valued objects.
In the following table, the indexes i and j are summed from
0 to d-1 where d is the dimension of the physical geometry,
as described by the @ref geo_2 class.

    c++ code                | mathematics          
    ------------------------+----------------------------------------------
    dot(u,v)                | u.v = sum_i u_i v_i   
    ddot(sigma,tau)         | sigma:tau = sum_ij sigma_ij tau_ij   
    dddot(sigma,tau)        | A:.B = sum_ijk A_ijk B_ijk   
    tr(sigma)               | sum_i sigma_ii
    trans(sigma)            | sigma^T the transpose
    sqr(phi), norm2(phi)    | phi^2
    norm2(u)                | sum_i u_i^2
    norm2(sigma)            | sum_ij sigma_ij^2
    abs(phi), norm(phi)     | absolute value
    norm(u)                 | sqrt(norm2(u))
    norm(sigma)             | sqrt(norm2(sigma))
    ------------------------+----------------------------------------------
    grad(phi)               | (d phi/d x_i)_i
    grad(u)                 | (d u_i/d x_j)_ij
    D(u)                    | (grad(u) + grad(u)^T)/2
    div(u)                  | sum_i d u_i/d x_i = tr(D(u))
    curl(phi)               | (d phi/d x_1, - d phi/d x_0) when d=2
    curl(u)                 | grad^u when d=3
    ------------------------+----------------------------------------------
    normal()                | n, the outward unit normal
    grad_s(phi)             | P*grad(phi) where P = I - n otimes n
    grad_s(u)               | grad(u)*P
    Ds(u)                   | P*D(u)*P
    div_s(u)                | tr(Ds(u))
    ------------------------+----------------------------------------------
    grad_h(phi)             | grad(phi|K) broken gradient, piecewise
    div_h(u)                | div(u|K)
    Dh(u)                   | D(u|K)
    ------------------------+----------------------------------------------
    jump(phi)               | [phi] = phi|K0 - phi|K1
                            |         on an oriented side S = dK0 inter dK1
    average(phi)            | {phi} = (phi|K0 + phi|K1)/2
    inner(phi)              | phi|K0
    outer(phi)              | phi|K1
    h_local()               | meas(K)^(1/d)
    penalty()               | max(meas(dK0)/meas(K0), meas(dK1)/meas(K1))
    ------------------------+----------------------------------------------
    sin, cos, tan, acos,    | standard math function extended to field
    asin, atan, cosh, sinh, | and test functions
    tanh, log, log10, floor |
    ceil                    |
    pow(phi,psi)            |
    atan2(phi,psi)          | atan(psi/phi)
    fmod(phi,psi)           | phi - floor(phi/psi+0.5)*psi
    min(phi,psi)            |
    max(phi,psi)            |
    ------------------------+----------------------------------------------
    compose(f,phi)          | f(phi), for a given function f
    compose(f,phi1,..,phin) | f(phi0,...,phin)
    compose(phi,X)          | phi(X) for a characteristic X

See also @ref compose_3 and @ref characteristic_2 for details.


Implementation
==============
The practical implementation of expressions involved
by @ref interpolate_3 and @ref integrate_3 functions
bases on two very classical C++ idioms: the 
<a href="https://en.wikipedia.org/wiki/Expression_templates">
expression template</a>
and the
<a href="https://en.wikipedia.org/wiki/Substitution_failure_is_not_an_error">
Substitution failure is not an error (SFINAE)</a>.
These C++ idioms are described in the following book:

  D. Vandevoorde and N. M. Josuttis,
  C++ templates: the complete guide,
  Addison-Wesley, 2002

*/
} // namespace rheolef

/*
This file "expression.h" focuses on operators between fields
since the field return value (scalar,vector,tensor) is not known
at compile time, operators try to infer it when the return type
or a second argument is known at compile time

optherwise, the return value is undeterminated_basic<T> that
will be solved at run time from the field::valued_tag() method.

OVERVIEW:
 0. error utilities
 1. unary operations
    1.1. unary function helper
    1.2. unary operators: +x -x
    1.3. unary standard maths: sin(x), cos(x), ...
    1.4. unary extensions: tr(x), trans(x), norm(x) norm2(x)
 2. binary operations
    2.1. binary function helper
    2.2. binary operators: x+y, x-y, x*y, x/y
    2.3. binary standard maths: pow(x,y), atan2(x,y),...
    2.4. binary extensions: dot(x,y), ddot(x,y), dddot(x,y)
 3. binders
    3.1. binder_first
    3.2. binder_second
    3.3. swapper
*/ 
#include "rheolef/promote.h"
#include "rheolef/undeterminated.h"
#include "rheolef/space_constant.h"

namespace rheolef { namespace details {

// ===========================================================================
// part 0. error utilities
// ===========================================================================
template<class Op, class T1, class T2, class R>
struct binop_error {};

template <class T> struct is_error: std::false_type {};

template<class Op, class T1, class T2, class R>
struct is_error<binop_error<Op,T1,T2,R> >: std::true_type {};

} // namespace details

template<class Op, class T1, class T2, class R>
struct float_traits<details::binop_error<Op,T1,T2,R> > { typedef typename float_traits<R>::type type; };

// ===========================================================================
// part 1. unary operations
// ===========================================================================
namespace details {
// ---------------------------------------------------------------------------
// chap 1.1. unary function helper
// ---------------------------------------------------------------------------
// defualt is STL class-functions
template<class Function>
struct generic_unary_traits {
  template <class Arg>
  struct result_hint {
    typedef typename function_traits<Function>::result_type type;
  };
  template <class Arg, class Result>
  struct hint {
    typedef typename function_traits<Function>::result_type   result_type;
    typedef typename std::decay<typename function_traits<Function>::template arg<0>::type>::type
            							  argument_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type) {
    return space_constant::valued_tag_traits<typename function_traits<Function>::result_type>::value;
  }
};
// ----------------------------------------------------------------------------
// chap 1.2. unary operators: +x -x
// ----------------------------------------------------------------------------
// +x
// ------
struct unary_plus {
  template <class T> T operator() (const T& a) const { return +a; }
};
template<>
struct generic_unary_traits<unary_plus> {
  template <class Arg>
  struct result_hint {
    typedef Arg type;
  };
  template <class Arg, class Result>
  struct hint {
    typedef typename promote<Arg,Result>::type result_type;
    typedef result_type                        argument_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag) { 
    return tag;
  }
};
// ------
// -x
// ------
struct negate {
  template <class T> T operator() (const T& a) const { return -a; }
};
template<>
struct generic_unary_traits<negate> {
  template <class Arg>
  struct result_hint {
    typedef Arg type;
  };
  template <class Arg, class Result>
  struct hint {
    typedef typename promote<Arg,Result>::type result_type;
    typedef result_type                        argument_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag) { 
    return tag;
  }
};
// ----------------------------------------------------------------------------
// chap 1.3. unary standard maths: sin(x), cos(x), ...
// ----------------------------------------------------------------------------
// specialization for is scalar generic functions, as details::sin_, details::cos_, etc
#define _RHEOLEF_generic_unary_scalar(F) 		\
struct F##_ {						\
  template<class T>					\
  T operator() (const T& x) const { return F(x); }	\
};							\
template<>						\
struct generic_unary_traits<F##_> {			\
  template <class Arg>					\
  struct result_hint {					\
    typedef typename scalar_traits<Arg>::type type;	\
  };							\
  template <class Arg, class Result>			\
  struct hint {						\
    typedef typename promote<				\
	typename scalar_traits<Arg>::type		\
       ,typename scalar_traits<Result>::type>::type S;	\
    typedef S result_type;				\
    typedef S argument_type;				\
  };							\
  static space_constant::valued_type			\
  valued_tag (space_constant::valued_type) {		\
    return space_constant::scalar;			\
  }							\
};
// std::cmath
_RHEOLEF_generic_unary_scalar(cos)
_RHEOLEF_generic_unary_scalar(sin)
_RHEOLEF_generic_unary_scalar(tan)
_RHEOLEF_generic_unary_scalar(acos)
_RHEOLEF_generic_unary_scalar(asin)
_RHEOLEF_generic_unary_scalar(atan)
_RHEOLEF_generic_unary_scalar(cosh)
_RHEOLEF_generic_unary_scalar(sinh)
_RHEOLEF_generic_unary_scalar(tanh)
_RHEOLEF_generic_unary_scalar(exp)
_RHEOLEF_generic_unary_scalar(log)
_RHEOLEF_generic_unary_scalar(log10)
_RHEOLEF_generic_unary_scalar(sqrt)
_RHEOLEF_generic_unary_scalar(abs)
_RHEOLEF_generic_unary_scalar(fabs)
_RHEOLEF_generic_unary_scalar(floor)
_RHEOLEF_generic_unary_scalar(ceil)
// rheolef extension:
_RHEOLEF_generic_unary_scalar(sqr)
#undef _RHEOLEF_generic_unary_scalar
// ----------------------------------------------------------------------------
// chap 1.4. unary extensions: tr(x), norm(x) norm2(x)
// ----------------------------------------------------------------------------
// tr(tau)
// ----------
struct tr_ {
  template <class T> T operator() (const tensor_basic<T>& a) const { return tr(a); }
};
template<>
struct generic_unary_traits<tr_> {
  template <class A1>
  struct result_hint {
    typedef typename scalar_traits<A1>::type type;
  };
  template <class A1, class R>
  struct hint {
    typedef typename result_hint<A1>::type result_type;
    typedef tensor_basic<result_type>      argument_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag) { 
    return space_constant::scalar;
  }
};
// ----------
// trans(tau)
// ----------
struct trans_ {
  template <class T> tensor_basic<T> operator() (const tensor_basic<T>& a) const { return trans(a); }
};
template<>
struct generic_unary_traits<trans_> {
  template <class A1>
  struct result_hint {
    typedef tensor_basic<typename scalar_traits<A1>::type> type;
  };
  template <class A1, class R>
  struct hint {
    typedef typename promote<typename scalar_traits<A1>::type,typename scalar_traits<R>::type>::type S;
    typedef tensor_basic<S> argument_type;
    typedef tensor_basic<S> result_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag) { 
    return space_constant::unsymmetric_tensor;
  }
};
// ----------
// norm(x)
// ----------
struct norm_ {
  template<class T>
  typename float_traits<T>::type operator() (const T& x) const { return fabs(x); }
  template<class T>
  typename float_traits<T>::type operator() (const point_basic<T>& x) const { return norm(x); }
  template<class T>
  typename float_traits<T>::type operator() (const tensor_basic<T>& x) const { return norm(x); }
};
template<>
struct generic_unary_traits<norm_> {
  template <class Arg>
  struct result_hint {
    typedef typename float_traits<Arg>::type  type;
  };
  template <class A1, class R>
  struct hint {
    typedef typename result_hint<A1>::type result_type;
    typedef A1                             argument_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag) {
    return space_constant::scalar;
  }
};
// ----------
// norm2(x)
// ----------
struct norm2_ {
  template<class T>
  typename float_traits<T>::type operator() (const T& x) const { return sqr(x); }
  template<class T>
  typename float_traits<T>::type operator() (const point_basic<T>& x) const { return norm2(x); }
  template<class T>
  typename float_traits<T>::type operator() (const tensor_basic<T>& x) const { return norm2(x); }
};
template<>
struct generic_unary_traits<norm2_> {
  template <class Arg>
  struct result_hint {
    typedef typename float_traits<Arg>::type  type;
  };
  template <class A1, class R>
  struct hint {
    typedef typename result_hint<A1>::type result_type;
    typedef A1                             argument_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag) {
    return space_constant::scalar;
  }
};
// ===========================================================================
// part 2. binary operations
// ===========================================================================
// ---------------------------------------------------------------------------
// chap 2.1. binary function helper
// ---------------------------------------------------------------------------
template<class Function>
struct generic_binary_traits {
  template <class Arg1, class Arg2>
  struct result_hint {
    typedef typename function_traits<Function>::result_type type;
  };
  template <class Arg1, class Arg2, class Result>
  struct hint {
    typedef typename std::decay<typename function_traits<Function>::template arg<0>::type>::type
            							         first_argument_type;
    typedef typename std::decay<typename function_traits<Function>::template arg<1>::type>::type
            							         second_argument_type;
    typedef typename std::decay<typename function_traits<Function>::result_type>::type 
                                                                         result_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type, space_constant::valued_type) {
    return space_constant::valued_tag_traits<typename function_traits<Function>::result_type>::value;
  }
};
// ----------------------------------------------------------------------------
// chap 2.2. binary operators: x+y, x-y, x*y, x/y
// ----------------------------------------------------------------------------
// x+y
// --------
struct plus; // foward declaration

// result type
// -----------
template <class A1, class A2, class Sfinae = void>
struct plus_result {
  typedef binop_error<details::plus,A1,A2,undeterminated_basic<Float> > type;
};
// s+s -> s
template <class A1, class A2>
struct plus_result<A1,A2,
  typename std::enable_if<
    is_rheolef_arithmetic<A1>::value &&
    is_rheolef_arithmetic<A2>::value
  >::type> 
{
  typedef typename promote<A1,A2>::type type;
};
// v+v -> v, t+t -> t
template <class A>
struct plus_result<A,A,
  typename std::enable_if<
    ! is_rheolef_arithmetic<A>::value>::type>
{
  typedef A type;
};
struct plus {
  template <class T1, class T2> 
  typename plus_result<T1,T2>::type operator() (const T1& a, const T2& b) const { return a+b; }
};
template<>
struct generic_binary_traits<plus> {
  // --------------------------
  // hint interface
  // --------------------------
  template <class A1, class A2, class R>
  struct hint {
    typedef A1 first_argument_type;
    typedef A2 second_argument_type;
    typedef R  result_type;
  };
  // two types are known
  // --------------------------
  // a1+a2 -> ? : deduce r
  template <class A1, class A2, class R>
  struct hint<A1,A2,undeterminated_basic<R> > {
    typedef A1                                first_argument_type;
    typedef A2                                second_argument_type;
    typedef typename plus_result<A1,A2>::type result_type;
  };
  // ?+a2 -> r : deduce a1
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,A2,R> {
    typedef typename std::conditional<
      details::is_equal<A2,R>::value,
      R,
      binop_error<details::plus,A1,A2,R>
    >::type                                   first_argument_type;
    typedef A2                                second_argument_type;
    typedef R                                 result_type;
  };
  // a1+? -> r : deduce a2
  template <class A1, class A2, class R>
  struct hint<A1,undeterminated_basic<A2>,R> {
    typedef A1                                first_argument_type;
    typedef typename std::conditional<
      details::is_equal<A1,R>::value,
      R,
      binop_error<details::plus,A1,A2,R> >::type
                                              second_argument_type;
    typedef R                                 result_type;
  };
  // ?+? -> r : deduce a1,a2
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,R> {
    typedef R                                first_argument_type;
    typedef R                                second_argument_type;
    typedef R                                result_type;
  };
  // a1+? -> ? : deduce a2,r
  template <class A1, class A2, class R>
  struct hint<A1,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef A1                                first_argument_type;
    typedef A1                                second_argument_type;
    typedef A1                                result_type;
  };
  // ?+a2 -> ? : deduce a2,r
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,A2, undeterminated_basic<R> > {
    typedef A2                                first_argument_type;
    typedef A2                                second_argument_type;
    typedef A2                                result_type;
  };
  // ?+? -> ? : deduce a1, a2,r
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>, undeterminated_basic<A2>, undeterminated_basic<R> > {
    typedef undeterminated_basic<A1>   first_argument_type;
    typedef undeterminated_basic<A2>   second_argument_type;
    typedef undeterminated_basic<R>    result_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag1, space_constant::valued_type tag2) { 
    return (tag2 == space_constant::last_valued) ? tag1 : tag2;
  }
  typedef std::true_type is_symmetric;
  template <class Arg1, class Arg2>
  struct result_hint {
    typedef typename hint<Arg1,Arg2,undeterminated_basic<Float> >::result_type type;
  };
};
// --------
// x-y
// --------
struct minus {
  template <class T1, class T2> 
  typename plus_result<T1,T2>::type operator() (const T1& a, const T2& b) const { return a-b; }
};
template<>
struct generic_binary_traits<minus> {
  template <class A1, class A2, class R>
  struct hint : generic_binary_traits<plus>::template hint<A1,A2,R> {};

  template <class A1, class A2>
  struct result_hint {
    typedef typename hint<A1,A2,undeterminated_basic<Float> >::result_type type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag1, space_constant::valued_type tag2) { 
    return (tag2 == space_constant::last_valued) ? tag1 : tag2;
  }
  typedef std::false_type is_symmetric;
};
// --------
// x*y
// --------
/*
    result: when arg1 & arg2 are given
      1\2 s  v  t  t3 t4
       s  s  v  t  t3 t4
       v  v  E  E  E  E
       t  t  v  t  E  E
       t3 t3 t  t3 E  E
       t4 t4 E  E  E  E

    arg1: deduced when arg2 & result are known
      2\r s  v  t  t3  t4
       s  s  v  t  t3  t4
       v  E (st)t3 E   E
       t  E  E (st)t3  E    st: indetermine'
       t3 E  E  E  s   E
       t4 E  E  E  E   s

    arg2: deduced when arg1 & result are known
      1\r s  v  t  t3  t4
       s  s  v  t  t3  t4
       v  E  s  E  E   E
       t  E  v (st)E   E    st: indetermine'
       t3 E  E  v (st) E
       t4 E  E  E E    E

      il n'y a que deux cas ou l'arg1 est indetermine' :
            quand arg2=v et res_t=v alors arg1=s ou t
            quand arg2=t et res_t=t alors arg1=s ou t
      dans tous les autres cas, on deduit l'arg1

      il n'y a qu'un cas ou l'arg2 est indetermine' :
            quand arg1=t et res_t=t alors arg2=s ou t
      dans tous les autres cas, on deduit l'arg2
*/
struct multiplies; // foward declaration

// result type
// -----------
// s*s -> s
template <class A1, class A2>
struct multiplies_result {
  typedef typename promote<A1,A2>::type type;
};
// s*v -> v
template <class S>
struct multiplies_result<S, point_basic<S> > {
  typedef point_basic<S> type;
};
// s*t -> t
template <class S>
struct multiplies_result<S, tensor_basic<S> > {
  typedef tensor_basic<S> type;
};
// v*s -> v
template <class S>
struct multiplies_result<point_basic<S>, S> {
  typedef point_basic<S> type;
};
// v*v -> err
template <class S1, class S2>
struct multiplies_result<point_basic<S1>, point_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,point_basic<S1>,point_basic<S2>,undeterminated_basic<S> > type;
};
// v*t -> err
template <class S1, class S2>
struct multiplies_result<point_basic<S1>, tensor_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,point_basic<S1>,tensor_basic<S2>,undeterminated_basic<S> > type;
};
// t*s -> t
template <class S>
struct multiplies_result<tensor_basic<S>, S> {
  typedef tensor_basic<S> type;
};
// t*v -> v
template <class S1, class S2>
struct multiplies_result<tensor_basic<S1>, point_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef point_basic<S> type;
};
// t*t -> t
template <class S1, class S2>
struct multiplies_result<tensor_basic<S1>, tensor_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef tensor_basic<S> type;
};
// s*t3 -> t3
template <class S>
struct multiplies_result<S, tensor3_basic<S> > {
  typedef tensor3_basic<S> type;
};
// t*s -> t
template <class S>
struct multiplies_result<tensor3_basic<S>, S> {
  typedef tensor3_basic<S> type;
};
// t3*v -> t
template <class S1, class S2>
struct multiplies_result<tensor3_basic<S1>, point_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef tensor_basic<S> type;
};
// t3*t -> t3
template <class S1, class S2>
struct multiplies_result<tensor3_basic<S1>, tensor_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef tensor3_basic<S> type;
};
// v*t3 -> err
template <class S1, class S2>
struct multiplies_result<point_basic<S1>, tensor3_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,point_basic<S1>,tensor3_basic<S2>,undeterminated_basic<S> > type;
};
// t*t3 -> err
template <class S1, class S2>
struct multiplies_result<tensor_basic<S1>, tensor3_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,tensor_basic<S1>,tensor3_basic<S2>,undeterminated_basic<S> > type;
};
// t3*t3 -> err
template <class S1, class S2>
struct multiplies_result<tensor3_basic<S1>, tensor3_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,tensor3_basic<S1>,tensor3_basic<S2>,undeterminated_basic<S> > type;
};
// s*t4 -> t4 
template <class S>
struct multiplies_result<S, tensor4_basic<S> > {
  typedef tensor4_basic<S> type;
};
// t4*s -> t4
template <class S>
struct multiplies_result<tensor4_basic<S>, S> {
  typedef tensor4_basic<S> type;
};
// v*t4 -> err
template <class S1, class S2>
struct multiplies_result<point_basic<S1>, tensor4_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,point_basic<S1>,tensor4_basic<S2>,undeterminated_basic<S> > type;
};
// t*t4 -> err
template <class S1, class S2>
struct multiplies_result<tensor_basic<S1>, tensor4_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,tensor_basic<S1>,tensor4_basic<S2>,undeterminated_basic<S> > type;
};
// t3*t4 -> err
template <class S1, class S2>
struct multiplies_result<tensor3_basic<S1>, tensor4_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,tensor3_basic<S1>,tensor4_basic<S2>,undeterminated_basic<S> > type;
};
// t4*v -> err
template <class S1, class S2>
struct multiplies_result<tensor4_basic<S1>, point_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,tensor4_basic<S1>,point_basic<S2>,undeterminated_basic<S> > type;
};
// t4*t -> err
template <class S1, class S2>
struct multiplies_result<tensor4_basic<S1>, tensor_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,tensor4_basic<S1>,tensor_basic<S2>,undeterminated_basic<S> > type;
};
// t4*t3 -> err
template <class S1, class S2>
struct multiplies_result<tensor4_basic<S1>, tensor3_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,tensor4_basic<S1>,tensor3_basic<S2>,undeterminated_basic<S> > type;
};
// t4*t4 -> err
template <class S1, class S2>
struct multiplies_result<tensor4_basic<S1>, tensor4_basic<S2> > {
  typedef typename promote<S1,S2>::type S;
  typedef binop_error<details::multiplies,tensor4_basic<S1>,tensor4_basic<S2>,undeterminated_basic<S> > type;
};

struct multiplies {
  template <class T1, class T2>
  typename multiplies_result<T1,T2>::type
  operator() (const T1& a, const T2& b) const { return a*b; }
};
template<>
struct generic_binary_traits<multiplies> {
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag1, space_constant::valued_type tag2) { 
    return space_constant::multiplies_result_tag(tag1,tag2);
  }
  // --------------------------
  // hint arg1 type
  // --------------------------
  // ?*s=s => a1=s
  template <class A2, class R, class Sfinae = void>
  struct first_argument_hint {
    typedef typename promote<A2,R>::type type;
  };
  // ?*s=v => a1=v
  template <class A2, class R>
  struct first_argument_hint<A2,point_basic<R> > {
    typedef point_basic<typename promote<A2,R>::type> type;
  };
  // ?*s=t => a1=t
  template <class A2, class R>
  struct first_argument_hint<A2,tensor_basic<R> > {
    typedef tensor_basic<typename promote<A2,R>::type> type;
  };
  // ?*s=t3 => a1=t3
  template <class A2, class R>
  struct first_argument_hint<A2,tensor3_basic<R> > {
    typedef tensor3_basic<typename promote<A2,R>::type> type;
  };
  // ?*s=t4 => a1=t4
  template <class A2, class R>
  struct first_argument_hint<A2,tensor4_basic<R>, 
	typename std::enable_if<is_rheolef_arithmetic<A2>::value>::type> {
    typedef tensor4_basic<typename promote<A2,R>::type> type;
  };
  // ?*v=s => a1=err
  template <class A2, class R>
  struct first_argument_hint<point_basic<A2>,R> {
    typedef binop_error<details::multiplies,undeterminated_basic<A2>,point_basic<A2>,R> type;
  };
  // ?*v=v => a1={s,t}=undeterminated
  template <class A2, class R>
  struct first_argument_hint<point_basic<A2>,point_basic<R> > {
    typedef undeterminated_basic<typename promote<A2,R>::type> type;
  };
  // ?*v=t => a1=t3
  template <class A2, class R>
  struct first_argument_hint<point_basic<A2>,tensor_basic<R> > {
    typedef tensor3_basic<typename promote<A2,R>::type> type;
  };
  // ?*v=t3 => a1=err
  template <class A2, class R>
  struct first_argument_hint<point_basic<A2>,tensor3_basic<R> > {
    typedef binop_error<details::multiplies,undeterminated_basic<A2>,point_basic<A2>,tensor3_basic<R> > type;
  };
  // ?*v=t4 => a1=err
  template <class A2, class R>
  struct first_argument_hint<point_basic<A2>,tensor4_basic<R> > {
    typedef binop_error<details::multiplies,undeterminated_basic<A2>,point_basic<A2>,tensor4_basic<R> > type;
  };
  // ?*t=s => a1=err
  template <class A2, class R>
  struct first_argument_hint<tensor_basic<A2>,R> {
    typedef binop_error<details::multiplies,undeterminated_basic<A2>,tensor_basic<A2>,R> type;
  };
  // ?*t=v => a1=err
  template <class A2, class R>
  struct first_argument_hint<tensor_basic<A2>,point_basic<R> > {
    typedef binop_error<details::multiplies,undeterminated_basic<A2>,tensor_basic<A2>,point_basic<R> > type;
  };
  // ?*t=t => a1={s,t}=undeterminated
  template <class A2, class R>
  struct first_argument_hint<tensor_basic<A2>,tensor_basic<R> > {
    typedef undeterminated_basic<typename promote<A2,R>::type> type;
  };
  // ?*t=t3 => a1=t3
  template <class A2, class R>
  struct first_argument_hint<tensor_basic<A2>,tensor3_basic<R> > {
    typedef tensor3_basic<typename promote<A2,R>::type> type;
  };
  // ?*t3=s => a1=err
  template <class A2, class R>
  struct first_argument_hint<tensor3_basic<A2>,R> {
    typedef binop_error<details::multiplies,undeterminated_basic<A2>,tensor3_basic<A2>,R> type;
  };
  // ?*t3=v => a1=err
  template <class A2, class R>
  struct first_argument_hint<tensor3_basic<A2>,point_basic<R> > {
    typedef binop_error<details::multiplies,undeterminated_basic<A2>,tensor3_basic<A2>,point_basic<R> > type;
  };
  // ?*t3=t => a1=err
  template <class A2, class R>
  struct first_argument_hint<tensor3_basic<A2>,tensor_basic<R> > {
    typedef binop_error<details::multiplies,undeterminated_basic<A2>,tensor3_basic<A2>,tensor_basic<R> > type;
  };
  // ?*t3=t3 => a1=s
  template <class A2, class R>
  struct first_argument_hint<tensor3_basic<A2>,tensor3_basic<R> > {
    typedef typename promote<A2,R>::type type;
  };
  // ?*t4=s => a1=E
  template <class A2, class R>
  struct first_argument_hint<tensor4_basic<A2>,R,
	typename std::enable_if<is_rheolef_arithmetic<R>::value>::type> {
    typedef typename promote<A2,R>::type S;
    typedef binop_error<details::multiplies,undeterminated_basic<S>,tensor4_basic<A2>,R> type;
  };
  // ?*t4=v => a1=E
  template <class A2, class R>
  struct first_argument_hint<tensor4_basic<A2>,point_basic<R> > {
    typedef typename promote<A2,R>::type S;
    typedef binop_error<details::multiplies,undeterminated_basic<S>,tensor4_basic<A2>,point_basic<R> > type;
  };
  // ?*t4=t => a1=E
  template <class A2, class R>
  struct first_argument_hint<tensor4_basic<A2>,tensor_basic<R> > {
    typedef typename promote<A2,R>::type S;
    typedef binop_error<details::multiplies,undeterminated_basic<S>,tensor4_basic<A2>,tensor_basic<R> > type;
  };
  // ?*t4=t3 => a1=E
  template <class A2, class R>
  struct first_argument_hint<tensor4_basic<A2>,tensor3_basic<R> > {
    typedef typename promote<A2,R>::type S;
    typedef binop_error<details::multiplies,undeterminated_basic<S>,tensor4_basic<A2>,tensor3_basic<R> > type;
  };
  // ?*t4=t4 => a1=s
  template <class A2, class R>
  struct first_argument_hint<tensor4_basic<A2>,tensor4_basic<R> > {
    typedef typename promote<A2,R>::type type;
  };
  // --------------------------
  // hint arg2 type
  // --------------------------
  // s*?=s => a2=s
  template <class A1, class R, class Sfinae = void>
  struct second_argument_hint {
    typedef typename promote<A1,R>::type type;
  };
  // s*?=v => a2=v
  template <class A1, class R>
  struct second_argument_hint<A1,point_basic<R> > {
    typedef point_basic<typename promote<A1,R>::type> type;
  };
  // s*?=t => a2=t
  template <class A1, class R>
  struct second_argument_hint<A1,tensor_basic<R> > {
    typedef tensor_basic<typename promote<A1,R>::type> type;
  };
  // s*?=t3 => a2=t3
  template <class A1, class R>
  struct second_argument_hint<A1,tensor3_basic<R> > {
    typedef tensor3_basic<typename promote<A1,R>::type> type;
  };
  // s*?=t4 => a2=t4
  template <class A1, class R>
  struct second_argument_hint<A1,tensor4_basic<R>,
	typename std::enable_if<is_rheolef_arithmetic<A1>::value>::type> {
    typedef tensor4_basic<typename promote<A1,R>::type> type;
  };
  // v*?=s => a2=err
  template <class A1, class R>
  struct second_argument_hint<point_basic<A1>,R> {
    typedef binop_error<details::multiplies,point_basic<A1>,undeterminated_basic<A1>,R> type;
  };
  // v*?=v => a2=s
  template <class A1, class R>
  struct second_argument_hint<point_basic<A1>,point_basic<R> > {
    typedef typename promote<A1,R>::type type;
  };
  // v*?=t => a2=err
  template <class A1, class R>
  struct second_argument_hint<point_basic<A1>,tensor_basic<R> > {
    typedef binop_error<details::multiplies,tensor_basic<A1>,undeterminated_basic<A1>,tensor_basic<R> > type;
  };
  // t*?=s => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor_basic<A1>,R> {
    typedef binop_error<details::multiplies,tensor_basic<A1>,undeterminated_basic<A1>,R> type;
  };
  // t*?=v => a2=v
  template <class A1, class R>
  struct second_argument_hint<tensor_basic<A1>,point_basic<R> > {
    typedef point_basic<typename promote<A1,R>::type> type;
  };
  // t*?=t => a2={s,t}=undeterminated
  template <class A1, class R>
  struct second_argument_hint<tensor_basic<A1>,tensor_basic<R> > {
    typedef undeterminated_basic<typename promote<A1,R>::type> type;
  };
  // v*?=t3 => a2=err
  template <class A1, class R>
  struct second_argument_hint<point_basic<A1>,tensor3_basic<R> > {
    typedef binop_error<details::multiplies,point_basic<A1>,undeterminated_basic<A1>,tensor3_basic<R> > type;
  };
  // t*?=t3 => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor_basic<A1>,tensor3_basic<R> > {
    typedef binop_error<details::multiplies,tensor_basic<A1>,undeterminated_basic<A1>,tensor3_basic<R> > type;
  };
  // t3*?=s => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor3_basic<A1>,R > {
    typedef binop_error<details::multiplies,tensor3_basic<A1>,undeterminated_basic<A1>,R> type;
  };
  // t3*?=v => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor3_basic<A1>,point_basic<R> > {
    typedef binop_error<details::multiplies,tensor3_basic<A1>,undeterminated_basic<A1>,point_basic<R> > type;
  };
  // t3*?=t => a2=v
  template <class A1, class R>
  struct second_argument_hint<tensor3_basic<A1>,tensor_basic<R> > {
    typedef point_basic<typename promote<A1,R>::type> type;
  };
  // t3*?=t3 => a2={st}
  template <class A1, class R>
  struct second_argument_hint<tensor3_basic<A1>,tensor3_basic<R> > {
    typedef undeterminated_basic<typename promote<A1,R>::type> type;
  };
  // t4*?=s => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor4_basic<A1>,R,
	typename std::enable_if<is_rheolef_arithmetic<R>::value>::type> {
    typedef binop_error<details::multiplies,tensor4_basic<A1>,undeterminated_basic<A1>,R> type;
  };
  // t4*?=v => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor4_basic<A1>,point_basic<R> > {
    typedef binop_error<details::multiplies,tensor4_basic<A1>,undeterminated_basic<A1>,point_basic<R> > type;
  };
  // t4*?=t => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor4_basic<A1>,tensor_basic<R> > {
    typedef binop_error<details::multiplies,tensor4_basic<A1>,undeterminated_basic<A1>,tensor_basic<R> > type;
  };
  // t4*?=t3 => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor4_basic<A1>,tensor3_basic<R> > {
    typedef binop_error<details::multiplies,tensor4_basic<A1>,undeterminated_basic<A1>,tensor3_basic<R> > type;
  };
  // t4*?=t4 => a2=err
  template <class A1, class R>
  struct second_argument_hint<tensor4_basic<A1>,tensor4_basic<R> > {
    typedef typename promote<A1,R>::type S;
    typedef S type;
  };
  // --------------------------
  // hint interface
  // --------------------------
  template <class A1, class A2, class R>
  struct hint {
    typedef A1 first_argument_type;
    typedef A2 second_argument_type;
    typedef R  result_type;
  };
  // two types are known
  // --------------------------
  // a1*a2 -> ? : deduce r
  template <class A1, class A2, class R>
  struct hint<A1,A2,undeterminated_basic<R> > {
    typedef A1                                      first_argument_type;
    typedef A2                                      second_argument_type;
    typedef typename multiplies_result<A1,A2>::type result_type;
  };
  // ?*a2 -> r : deduce a1
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,A2,R> {
    // TODO: promote scalar_type of first arg: tensor<Float>*point<complex> -> point<complex>
    // 		promote_valued<Arg,Scalar>::type
    // e.g.	promote_valued<point_basic<Float>,complex<Float> >::type -> point_basic<cmplex<Float>>
    typedef typename first_argument_hint<A2,R>::type first_argument_type;
    typedef A2                                       second_argument_type;
    typedef R                                        result_type;
  };
  // a1*? -> r : deduce a2
  template <class A1, class A2, class R>
  struct hint<A1,undeterminated_basic<A2>,R> {
    typedef A1                                        first_argument_type;
    typedef typename second_argument_hint<A1,R>::type second_argument_type;
    typedef R                                         result_type;
  };
  // only one type is known
  // -----------------------
  // ?*? -> s : deduce a1=a2=s
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,R> {
    typedef A1                                        first_argument_type;
    typedef A2                                        second_argument_type;
    typedef R                                         result_type;
  };
  // ?*? -> v : deduce (a1,a2)={(s,v),(v,s),(t,v)}=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,point_basic<R> > {
    typedef undeterminated_basic<A1> first_argument_type;
    typedef undeterminated_basic<A2> second_argument_type;
    typedef point_basic<R>           result_type;
  };
  // ?*? -> t : deduce (a1,a2)={(s,t),(t,s),(t,t)}=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,tensor_basic<R> > {
    typedef undeterminated_basic<A1> first_argument_type;
    typedef undeterminated_basic<A2> second_argument_type;
    typedef tensor_basic<R>          result_type;
  };
  // ?*? -> t3 : deduce (a1,a2)={(s,t3),(t3,s),(t3,t)}=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,tensor3_basic<R> > {
    typedef undeterminated_basic<A1> first_argument_type;
    typedef undeterminated_basic<A2> second_argument_type;
    typedef tensor3_basic<R>          result_type;
  };
  // ?*? -> t4 : deduce (a1,a2)={(s,t4),(t4,s)}=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,tensor4_basic<R> > {
    typedef undeterminated_basic<A1> first_argument_type;
    typedef undeterminated_basic<A2> second_argument_type;
    typedef tensor4_basic<R>         result_type;
  };
  // s*? -> ? : deduce (a2,r)={(s,s),(v,v),(t,t)}=?
  template <class A1, class A2, class R>
  struct hint<A1,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef A1                       first_argument_type;
    typedef undeterminated_basic<A2> second_argument_type;
    typedef undeterminated_basic<R>  result_type;
  };
  // v*? -> ? : deduce (a2,r)=(s,v)
  template <class A1, class A2, class R>
  struct hint<point_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef point_basic<A1>          first_argument_type;
    typedef A2                       second_argument_type;
    typedef point_basic<R>           result_type;
  };
  // t*? -> ? : deduce (a2,r)={(v,v),(t,t),(s,t)}=?
  template <class A1, class A2, class R>
  struct hint<tensor_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef tensor_basic<A1>         first_argument_type;
    typedef undeterminated_basic<A2> second_argument_type;
    typedef undeterminated_basic<R>  result_type;
  };
  // t3*? -> ? : deduce (a2,r)={(s,t3),(v,t),(t3,t3)}=?
  template <class A1, class A2, class R>
  struct hint<tensor3_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef tensor3_basic<A1>        first_argument_type;
    typedef undeterminated_basic<A2> second_argument_type;
    typedef undeterminated_basic<R>  result_type;
  };
  // ?*s -> ? : deduce (a1,r)={(s,s),(v,v),(t,t)}=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,A2,undeterminated_basic<R> > {
    typedef undeterminated_basic<A1> first_argument_type;
    typedef A2                       second_argument_type;
    typedef undeterminated_basic<R>  result_type;
  };
  // ?*v -> ? : deduce (a1,r)={(s,v),(t,v)}=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,point_basic<A2>,undeterminated_basic<R> > {
    typedef undeterminated_basic<A1> first_argument_type;
    typedef point_basic<A2>          second_argument_type;
    typedef undeterminated_basic<R>  result_type;
  };
  // ?*t -> ? : deduce (a1,r)={(s,t),(t,t)}=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,tensor_basic<A2>,undeterminated_basic<R> > {
    typedef undeterminated_basic<A1> first_argument_type;
    typedef tensor_basic<A2>         second_argument_type;
    typedef undeterminated_basic<R>  result_type;
  };
  // ?*t3 -> ? : deduce (a1,r)=(s,t3)
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,tensor3_basic<A2>,undeterminated_basic<R> > {
    typedef A1                       first_argument_type;
    typedef tensor3_basic<A2>        second_argument_type;
    typedef tensor3_basic<R>         result_type;
  };
  // ?*t4 -> ? : deduce (a1,r)=(s,t4)
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,tensor4_basic<A2>,undeterminated_basic<R> > {
    typedef A1                       first_argument_type;
    typedef tensor4_basic<A2>        second_argument_type;
    typedef tensor4_basic<R>         result_type;
  };
  // t4*? -> ? : deduce (a2,r)=(s,t4)
  template <class A1, class A2, class R>
  struct hint<tensor4_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef tensor4_basic<A2>        first_argument_type;
    typedef A2                       second_argument_type;
    typedef tensor4_basic<R>         result_type;
  };
  // none types are known
  // -----------------------
  // ?*? -> ? : deduce (a1,a2,r)=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef undeterminated_basic<A1> first_argument_type;
    typedef undeterminated_basic<A2> second_argument_type;
    typedef undeterminated_basic<R>  result_type;
  };
  // short interface
  template <class Arg1, class Arg2>
  struct result_hint {
    typedef typename hint<Arg1,Arg2,undeterminated_basic<Float> >::result_type type;
  };
  typedef std::false_type is_symmetric; // tensor*vector do not commute
}; // generic_binary_traits<multiplies>
// --------
// x/y
// --------
struct divides; // foward declaration

template <class T1, class T2, class Sfinae = void> 
struct divides_result {
  typedef undeterminated_basic<typename promote<typename scalar_traits<T1>::type,typename scalar_traits<T2>::type>::type> type;
};
// s/s -> s
template <class T1, class T2>
struct divides_result<T1,T2,
  typename std::enable_if<
    is_rheolef_arithmetic<T1>::value &&
    is_rheolef_arithmetic<T2>::value
  >::type>
{
  typedef typename promote<T1,T2>::type type;
};
// undef/undef -> undef
template <class T1, class T2>
struct divides_result<T1,T2,
  typename std::enable_if<
    is_undeterminated<T1>::value &&
    is_undeterminated<T2>::value
  >::type>
{
  typedef undeterminated_basic <typename promote<T1,T2>::type> type;
};
// v/v -> err ; t/t -> err
template <class T> 
struct divides_result<T,T,
  typename std::enable_if<
       ! is_rheolef_arithmetic<T>::value
    && ! is_undeterminated<T>::value 
>::type>
{
  typedef typename scalar_traits<T>::type S;
  typedef binop_error<details::divides,T,T,undeterminated_basic<S> > type;
};
template <class T>
struct divides_result<point_basic<T>,T> {
  typedef point_basic<T> type;
};
template <class T>
struct divides_result<tensor_basic<T>,T> {
  typedef tensor_basic<T> type;
};
template <class T>
struct divides_result<T, point_basic<T> > {
  typedef binop_error<details::divides,T,point_basic<T>,undeterminated_basic<T> > type;
};
template <class T>
struct divides_result<T, tensor_basic<T> > {
  typedef binop_error<details::divides,T,tensor_basic<T>,undeterminated_basic<T> > type;
};
template <class T1, class T2>
struct divides_result<point_basic<T1>, tensor_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,point_basic<T1>,tensor_basic<T2>,undeterminated_basic<S> > type;
};
template <class T1, class T2>
struct divides_result<tensor_basic<T1>, point_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,tensor_basic<T1>,point_basic<T2>,undeterminated_basic<S> > type;
};
template <class T>
struct divides_result<tensor3_basic<T>,T> {
  typedef tensor3_basic<T> type;
};
template <class T>
struct divides_result<tensor4_basic<T>,T> {
  typedef tensor4_basic<T> type;
};
template <class T>
struct divides_result<T, tensor3_basic<T> > {
  typedef binop_error<details::divides,T,tensor3_basic<T>,undeterminated_basic<T> > type;
};
template <class T1, class T2>
struct divides_result<point_basic<T1>, tensor3_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,point_basic<T1>,tensor3_basic<T2>,undeterminated_basic<S> > type;
};
template <class T1, class T2>
struct divides_result<tensor_basic<T1>, tensor3_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,tensor_basic<T1>,tensor3_basic<T2>,undeterminated_basic<S> > type;
};
template <class T1, class T2>
struct divides_result<tensor3_basic<T1>, point_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,tensor3_basic<T1>,point_basic<T2>,undeterminated_basic<S> > type;
};
template <class T1, class T2>
struct divides_result<tensor3_basic<T1>, tensor_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,tensor3_basic<T1>,tensor_basic<T2>,undeterminated_basic<S> > type;
};
template <class T>
struct divides_result<T, tensor4_basic<T> > {
  typedef binop_error<details::divides,T,tensor4_basic<T>,undeterminated_basic<T> > type;
};
template <class T1, class T2>
struct divides_result<point_basic<T1>, tensor4_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,point_basic<T1>,tensor4_basic<T2>,undeterminated_basic<S> > type;
};
template <class T1, class T2>
struct divides_result<tensor_basic<T1>, tensor4_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,tensor_basic<T1>,tensor4_basic<T2>,undeterminated_basic<S> > type;
};
template <class T1, class T2>
struct divides_result<tensor3_basic<T1>, tensor4_basic<T2> > {
  typedef typename promote<T1,T2>::type S;
  typedef binop_error<details::divides,tensor3_basic<T1>,tensor4_basic<T2>,undeterminated_basic<S> > type;
};
struct divides {
  template <class T1, class T2>
  typename divides_result<T1,T2>::type
  operator() (const T1& a, const T2& b) const { return a/b; }
};
template<>
struct generic_binary_traits<divides> {
  template <class Arg1, class Arg2>
  struct result_hint {
    typedef typename divides_result<Arg1,Arg2>::type type;
  };
  template <class Arg1, class Arg2, class Result>
  struct hint {
    typedef Arg1   first_argument_type;
    typedef Arg2   second_argument_type;
    typedef Result result_type;
  };
  // two types are known
  // --------------------------
  // a1/a2 -> ? : deduce r=a1 when a2=s (error otherwise)
  template <class A1, class A2, class R>
  struct hint<A1,A2,undeterminated_basic<R> > {
    typedef A1                                   first_argument_type;
    typedef A2                                   second_argument_type;
    typedef typename divides_result<A1,A2>::type result_type;
  };
  // ?/a2 -> r : deduce a1=r when a2=s (error otherwise)
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,A2,R> {
    typedef typename scalar_traits<A2>::type S2;
    typedef typename std::conditional<
      details::is_scalar<A2>::value,
      R,
      binop_error<details::divides,A1,A2,R> >::type  first_argument_type;
    typedef A2                                       second_argument_type;
    typedef R                                        result_type;
  };
  // a1/? -> r : deduce a2=s when a1=r (error otherwise)
  template <class A1, class A2, class R>
  struct hint<A1,undeterminated_basic<A2>,R> {
    typedef typename promote<
      typename scalar_traits<A1>::type,
      typename scalar_traits<R>::type>::type         S;
    typedef A1                                       first_argument_type;
    typedef typename std::conditional<
      details::is_equal<A1,R>::value,
      S,
      binop_error<details::divides,A1,A2,R> >::type  second_argument_type;
    typedef R                                        result_type;
  };
  // two types are unknown
  // --------------------------
  // ?/? -> r : deduce a1=r and a2=s
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,R> {
    typedef R                                        first_argument_type;
    typedef typename scalar_traits<A2>::type         second_argument_type;
    typedef R                                        result_type;
  };
  // a1/? -> ? : deduce r=a1 and a2=s
  template <class A1, class A2, class R>
  struct hint<A1,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef A1                                       first_argument_type;
    typedef typename scalar_traits<A2>::type         second_argument_type;
    typedef A1                                       result_type;
  };
  // ?/a2 -> ? : deduce r=a1=? and a2=s
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,A2,undeterminated_basic<R> > {
    typedef typename scalar_traits<A2>::type         S2;
    typedef typename std::conditional<
      details::is_scalar<A2>::value,
      undeterminated_basic<A1>,
      binop_error<details::divides,A1,A2,R> >::type  first_argument_type;
    typedef A2                                       second_argument_type;
    typedef first_argument_type                      result_type;
  };
  // three types are unknown
  // -----------------------
  // ?/? -> ? : deduce a1=r=? and a2=s
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef undeterminated_basic<A1>                 first_argument_type;
    typedef A2                                       second_argument_type;
    typedef undeterminated_basic<R>                  result_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag1, space_constant::valued_type tag2) { 
    return space_constant::divides_result_tag(tag1,tag2);
  }
  typedef std::false_type is_symmetric;
};
// ----------------------------------------------------------------------------
// chap 2.3. binary standard maths: pow(x,y), atan2(x,y),...
// ----------------------------------------------------------------------------
// specialization for scalar generic functions, as details::pow_, details::atan2_, etc
//ICI
template<class F>
struct scalar_binary_traits {
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag1, space_constant::valued_type tag2) { 
    return space_constant::scalar;
  }
  template <class Arg1, class Arg2>
  struct result_hint {
    typedef typename promote<typename scalar_traits<Arg1>::type, typename scalar_traits<Arg2>::type>::type type;
  };
  template <class A1, class A2, class R>
  struct hint {
    typedef typename scalar_traits<A1>::type  S1;
    typedef typename scalar_traits<A2>::type  S2;
    typedef typename scalar_traits<R>::type   S;
    typedef typename details::and_type<
              details::or_type<
                details::is_scalar<A1>
               ,is_undeterminated<A1>
      	      >
             ,details::or_type<
                details::is_scalar<A2>
               ,is_undeterminated<A2>
      	      >
             ,details::or_type<
                details::is_scalar<R>
               ,is_undeterminated<R>
      	     >
	   >::type  				is_good;
    typedef typename std::conditional<
	     is_undeterminated<A1>::value
	    ,typename std::conditional<
	       is_good::value
              ,S1
              ,binop_error<F,A1,A2,R>
             >::type
            ,A1
           >::type                              first_argument_type;
    typedef typename std::conditional<
	     is_undeterminated<A2>::value
	    ,typename std::conditional<
	       is_good::value
              ,S2
              ,binop_error<F,A1,A2,R>
             >::type
            ,A2
           >::type                              second_argument_type;
    typedef typename std::conditional<
	     is_undeterminated<R>::value
	    ,typename std::conditional<
	       is_good::value
              ,S
              ,binop_error<F,A1,A2,R>
             >::type
            ,R
           >::type                              result_type;
  };
  typedef std::false_type is_symmetric;
};
#define _RHEOLEF_generic_binary_scalar(F) 		\
struct F##_ {						\
  template<class A1, class A2>				\
  typename promote<A1,A2>::type				\
  operator() (const A1& x, const A2& y) const { 	\
  	typedef typename promote<A1,A2>::type R;	\
	return F(R(x),R(y)); } 				\
};							\
template<>						\
struct generic_binary_traits<F##_> : scalar_binary_traits<F##_> {}; \

_RHEOLEF_generic_binary_scalar(atan2)
_RHEOLEF_generic_binary_scalar(pow)
_RHEOLEF_generic_binary_scalar(fmod)
_RHEOLEF_generic_binary_scalar(min)
_RHEOLEF_generic_binary_scalar(max)
#undef _RHEOLEF_generic_binary_scalar
// ----------------------------------------------------------------------------
// chap 2.4. binary extensions: dot(x,y), ddot(x,y), dddot(x,y)
// ----------------------------------------------------------------------------
// dot(x,y)
// ------------
struct dot_ {
  template<class T>
  T operator() (const point_basic<T>& x, const point_basic<T>& y) const { return dot(x,y); }
};
template<>
struct generic_binary_traits<dot_> {
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag1, space_constant::valued_type tag2) { 
    return space_constant::scalar;
  }
  template <class Arg1, class Arg2>
  struct result_hint {
    typedef typename promote<typename float_traits<Arg1>::type, typename float_traits<Arg2>::type>::type type;
  };
  template <class A1, class A2, class R>
  struct hint {
    typedef typename scalar_traits<A1>::type  S1;
    typedef typename scalar_traits<A2>::type  S2;
    typedef typename scalar_traits<R>::type   S;
    typedef typename details::and_type<
              details::or_type<
                details::is_point<A1>
               ,is_undeterminated<A1>
      	      >
             ,details::or_type<
                details::is_point<A2>
               ,is_undeterminated<A2>
      	      >
             ,details::or_type<
                details::is_scalar<R>
               ,is_undeterminated<R>
      	     >
	   >::type  				is_good;
    typedef typename std::conditional<
	     is_undeterminated<A1>::value
	    ,typename std::conditional<
	       is_good::value
              ,point_basic<S1>
              ,binop_error<details::dot_,A1,A2,R>
             >::type
            ,A1
           >::type                              first_argument_type;
    typedef typename std::conditional<
	     is_undeterminated<A2>::value
	    ,typename std::conditional<
	       is_good::value
              ,point_basic<S2>
              ,binop_error<details::dot_,A1,A2,R>
             >::type
            ,A2
           >::type                              second_argument_type;
    typedef typename std::conditional<
	     is_undeterminated<R>::value
	    ,typename std::conditional<
	       is_good::value
              ,S
              ,binop_error<details::dot_,A1,A2,R>
             >::type
            ,R
           >::type                              result_type;
  };
  typedef std::true_type is_symmetric;
};
// ------------
// ddot(x,y)
// ------------
/*
    result: when arg1 & arg2 are given
      1\2 t  t4
       t  s  t
       t4 t  E
*/
struct ddot_;
template <class A1, class A2>
struct ddot_result {
    typedef typename promote<typename float_traits<A1>::type, typename float_traits<A2>::type>::type S;
    typedef binop_error<ddot_,A1,A2,undeterminated_basic<S> > type;
};
template <class A1, class A2>
struct ddot_result<tensor_basic<A1>,tensor_basic<A2> > {
    typedef typename promote<typename float_traits<A1>::type, typename float_traits<A2>::type>::type S;
    typedef S type;
};
template <class A1, class A2>
struct ddot_result<tensor4_basic<A1>,tensor_basic<A2> > {
    typedef typename promote<typename float_traits<A1>::type, typename float_traits<A2>::type>::type S;
    typedef tensor_basic<S> type;
};
template <class A1, class A2>
struct ddot_result<tensor_basic<A1>,tensor4_basic<A2> > {
    typedef typename promote<typename float_traits<A1>::type, typename float_traits<A2>::type>::type S;
    typedef tensor_basic<S> type;
};
struct ddot_ {
  template<class A1, class A2>
  typename ddot_result<A1,A2>::type
  operator() (const A1& x, const A2& y) const { return ddot(x,y); }
};
template<>
struct generic_binary_traits<ddot_> {
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag1, space_constant::valued_type tag2) { 
    return space_constant::scalar;
  }
  template <class A1, class A2, class R>
  struct hint {
    typedef binop_error<ddot_,A1,A2,R> T;
    typedef typename std::conditional<is_undeterminated<A1>::value,T,A1>::type first_argument_type;
    typedef typename std::conditional<is_undeterminated<A2>::value,T,A2>::type second_argument_type;
    typedef typename std::conditional<is_undeterminated<R>::value, T,R> ::type result_type;
  };
  // a:b -> ?
  template <class A1, class A2, class R>
  struct hint<A1,A2,undeterminated_basic<R> > {
    typedef typename ddot_result<A1,A2>::type result_type;
    typedef typename std::conditional<
	     is_undeterminated<A1>::value
          && is_error<result_type>::value
       ,result_type
       ,A1>::type                             first_argument_type;
    typedef typename std::conditional<
             is_undeterminated<A2>::value
          && is_error<result_type>::value
       ,result_type
       ,A2>::type                             second_argument_type;
  };
  // ====================
  // A1 xor A2 is unknown
  // ====================
  // ?:t -> s => a=t
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,tensor_basic<A2>,R> {
    typedef binop_error<ddot_,A1,A2,R> E;
    typedef typename std::conditional<is_scalar<R>::value,tensor_basic<A1>,E>::type
                                              first_argument_type;
    typedef tensor_basic<A2>                  second_argument_type;
    typedef R                                 result_type;
  };
  // t:? -> s => b=t
  template <class A1, class A2, class R>
  struct hint<tensor_basic<A1>,undeterminated_basic<A2>,R> {
    typedef binop_error<ddot_,A1,A2,R> E;
    typedef tensor_basic<A1>                  first_argument_type;
    typedef typename std::conditional<is_scalar<R>::value,tensor_basic<A2>,E>::type
                                              second_argument_type;
    typedef R                                 result_type;
  };
  // ?:t4 -> t => a=t
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,tensor4_basic<A2>,tensor_basic<R> > {
    typedef tensor_basic<A1>                  first_argument_type;
    typedef tensor4_basic<A2>                 second_argument_type;
    typedef tensor_basic<R>                   result_type;
  };
  // t4:? -> t => b=t
  template <class A1, class A2, class R>
  struct hint<tensor4_basic<A1>,undeterminated_basic<A2>,tensor_basic<R> > {
    typedef tensor4_basic<A1>                 first_argument_type;
    typedef tensor_basic<A2>                  second_argument_type;
    typedef tensor_basic<R>                   result_type;
  };
  // =====================
  // A1 and A2 are unknown
  // =====================
  // ?:? -> s => a=b=t
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,R> {
    typedef binop_error<ddot_,A1,A2,R> E;
    typedef typename std::conditional<is_scalar<R>::value,tensor_basic<A1>,E>::type
                                              first_argument_type;
    typedef typename std::conditional<is_scalar<R>::value,tensor_basic<A2>,E>::type
                                              second_argument_type;
    typedef R                                 result_type;
  };
  // ?:? -> t => (a,b)={(t,t4),(t4,t)}
  // =====================
  // A1 and R are unknown
  // =====================
  // ?:t  -> ? => (a,r)={(t,s),(t4,t)}
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,tensor_basic<A2>,undeterminated_basic<R> > {
    typedef undeterminated_basic<A1>          first_argument_type;
    typedef tensor_basic<A2>                  second_argument_type;
    typedef undeterminated_basic<R>           result_type;
  };
  // ?:t4 -> ? => (a,r)=(t,t)
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,tensor4_basic<A2>,undeterminated_basic<R> > {
    typedef tensor_basic<A1>                  first_argument_type;
    typedef tensor4_basic<A2>                 second_argument_type;
    typedef tensor_basic<R>                   result_type;
  };
  // =====================
  // A2 and R are unknown
  // =====================
  // t:?  -> ? => (b,r)={(t,s),(t4,t)}
  template <class A1, class A2, class R>
  struct hint<tensor_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef tensor_basic<A1>                  first_argument_type;
    typedef undeterminated_basic<A2>          second_argument_type;
    typedef undeterminated_basic<R>           result_type;
  };
  // t4:? -> ? => (b,r)=(t,t)
  template <class A1, class A2, class R>
  struct hint<tensor4_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef tensor4_basic<A1>                 first_argument_type;
    typedef tensor_basic<A2>                  second_argument_type;
    typedef tensor_basic<R>                   result_type;
  };
  // ========================
  // A1, A2 and R are unknown
  // ========================
  // ?:? -> ? => a,b,r=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef undeterminated_basic<A1>          first_argument_type;
    typedef undeterminated_basic<A2>          second_argument_type;
    typedef undeterminated_basic<R>           result_type;
  };
  // short interface
  template <class Arg1, class Arg2>
  struct result_hint {
    typedef typename hint<Arg1,Arg2,undeterminated_basic<Float> >::result_type type;
  };
  typedef std::true_type is_symmetric;
};
// ------------
// dddot(x,y)
// ------------
/*
    result: when arg1 & arg2 are given
      1\2 t3 
       t3  s
*/
struct dddot_;
template <class A1, class A2>
struct dddot_result {
    typedef typename promote<typename float_traits<A1>::type, typename float_traits<A2>::type>::type S;
    typedef binop_error<dddot_,A1,A2,undeterminated_basic<S> > type;
};
template <class A1, class A2>
struct dddot_result<tensor3_basic<A1>,tensor3_basic<A2> > {
    typedef typename promote<typename float_traits<A1>::type, typename float_traits<A2>::type>::type S;
    typedef S type;
};
struct dddot_ {
  template<class A1, class A2>
  typename dddot_result<A1,A2>::type
  operator() (const A1& x, const A2& y) const { return dddot(x,y); }
};
template<>
struct generic_binary_traits<dddot_> {
  static space_constant::valued_type
  valued_tag (space_constant::valued_type tag1, space_constant::valued_type tag2) { 
    return space_constant::scalar;
  }
  template <class A1, class A2, class R>
  struct hint {
    typedef binop_error<dddot_,A1,A2,R> T;
    typedef typename std::conditional<is_undeterminated<A1>::value,T,A1>::type first_argument_type;
    typedef typename std::conditional<is_undeterminated<A2>::value,T,A2>::type second_argument_type;
    typedef typename std::conditional<is_undeterminated<R>::value, T,R> ::type result_type;
  };
  // a:.b -> ?
  template <class A1, class A2, class R>
  struct hint<A1,A2,undeterminated_basic<R> > {
    typedef typename dddot_result<A1,A2>::type result_type;
    typedef typename std::conditional<
	     is_undeterminated<A1>::value
          && is_error<result_type>::value
       ,result_type
       ,A1>::type                             first_argument_type;
    typedef typename std::conditional<
             is_undeterminated<A2>::value
          && is_error<result_type>::value
       ,result_type
       ,A2>::type                             second_argument_type;
  };
  // ====================
  // A1 xor A2 is unknown
  // ====================
  // ?:.t3 -> s => a=t3
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,tensor3_basic<A2>,R> {
    typedef binop_error<dddot_,A1,A2,R> E;
    typedef typename std::conditional<is_scalar<R>::value,tensor3_basic<A1>,E>::type
                                              first_argument_type;
    typedef tensor3_basic<A2>                 second_argument_type;
    typedef R                                 result_type;
  };
  // t3:.? -> s => b=t3
  template <class A1, class A2, class R>
  struct hint<tensor3_basic<A1>,undeterminated_basic<A2>,R> {
    typedef binop_error<dddot_,A1,A2,R> E;
    typedef tensor3_basic<A1>                 first_argument_type;
    typedef typename std::conditional<is_scalar<R>::value,tensor3_basic<A2>,E>::type
                                              second_argument_type;
    typedef R                                 result_type;
  };
  // =====================
  // A1 and A2 are unknown
  // =====================
  // ?:.? -> s => a=b=t3
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,R> {
    typedef binop_error<dddot_,A1,A2,R> E;
    typedef typename std::conditional<is_scalar<R>::value,tensor3_basic<A1>,E>::type
                                              first_argument_type;
    typedef typename std::conditional<is_scalar<R>::value,tensor3_basic<A2>,E>::type
                                              second_argument_type;
    typedef R                                 result_type;
  };
  // =====================
  // A2 and R are unknown
  // =====================
  // t3:.?  -> ? => (b,r)={(t3,s)}
  template <class A1, class A2, class R>
  struct hint<tensor3_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef tensor3_basic<A1>                 first_argument_type;
    typedef tensor3_basic<A2>                 second_argument_type;
    typedef R                                 result_type;
  };
  // ========================
  // A1, A2 and R are unknown
  // ========================
  // ?:.? -> ? => a,b,r=?
  template <class A1, class A2, class R>
  struct hint<undeterminated_basic<A1>,undeterminated_basic<A2>,undeterminated_basic<R> > {
    typedef tensor3_basic<A1>                 first_argument_type;
    typedef tensor3_basic<A2>                 second_argument_type;
    typedef R                                 result_type;
  };
  // short interface
  template <class Arg1, class Arg2>
  struct result_hint {
    typedef typename hint<Arg1,Arg2,undeterminated_basic<Float> >::result_type type;
  };
  typedef std::true_type is_symmetric;
};
// ===========================================================================
// part 3. binders
// ===========================================================================
// similar to std::binder1st, std::binder2nd, but for generic operators

// ----------------------------------------------------------------------------
// chap 3.1. binder_first
// ----------------------------------------------------------------------------
template<class BinaryFunction, class A1>
struct binder_first {
  binder_first (const BinaryFunction& f, const A1& x1) : _f(f), _x1(x1) {}
  template<class A2>
  typename generic_binary_traits<BinaryFunction>::template result_hint<A1,A2>::type
  operator() (const A2& x2) const { return _f(_x1,x2); }
protected:
  BinaryFunction  _f;
  A1              _x1;
};
template<class BinaryFunction, class A1>
struct generic_unary_traits<binder_first<BinaryFunction,A1> > {
  template <class A2>
  struct result_hint {
    typedef typename generic_binary_traits<BinaryFunction>::template result_hint<A1,A2>::type type;
  };
  template <class A2, class Result>
  struct hint {
    typedef Result   result_type;
    typedef A2       argument_type;
  };
  // result unknown:
  template <class A2, class T>
  struct hint<A2,undeterminated_basic<T> > {
    typedef typename result_hint<A2>::type   result_type;
    typedef A1                               argument_type;
  };
  // A2 unknown:
  template <class R, class T>
  struct hint<undeterminated_basic<T>,R> {
    typedef R  result_type;
    typedef typename generic_binary_traits<BinaryFunction>::template hint<A1,undeterminated_basic<T>,R>::second_argument_type
            argument_type;
  };
  // A2 & R unknown:
  template <class T1, class T>
  struct hint<undeterminated_basic<T1>,undeterminated_basic<T> > {
    typedef typename generic_binary_traits<BinaryFunction>::template hint<A1,undeterminated_basic<T1>,undeterminated_basic<T> >::second_argument_type
            argument_type;
    typedef typename generic_binary_traits<BinaryFunction>::template hint<A1,undeterminated_basic<T>,undeterminated_basic<T> >::result_type
            result_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type arg_tag) {
    static const space_constant::valued_type arg1_tag = space_constant::valued_tag_traits<A1>::value;
    return generic_binary_traits<BinaryFunction>::valued_tag (arg1_tag, arg_tag) ;
  }
};
// ----------------------------------------------------------------------------
// chap 3.2. binder_second
// ----------------------------------------------------------------------------
template<class BinaryFunction, class A2>
struct binder_second {
  binder_second (const BinaryFunction& f, const A2& x2) : _f(f), _x2(x2) {}
  template<class A1>
  typename generic_binary_traits<BinaryFunction>::template result_hint<A1,A2>::type
  operator() (const A1& x1) const { return _f(x1,_x2); }
protected:
  BinaryFunction  _f;
  A2              _x2;
};
template<class BinaryFunction, class A2>
struct generic_unary_traits<binder_second<BinaryFunction,A2> > {
  template <class A1>
  struct result_hint {
    typedef typename generic_binary_traits<BinaryFunction>::template result_hint<A1,A2>::type type;
  };
  template <class A1, class Result>
  struct hint {
    typedef Result   result_type;
    typedef A1       argument_type;
  };
  // result unknown:
  template <class A1, class T>
  struct hint<A1,undeterminated_basic<T> > {
    typedef typename result_hint<A1>::type   result_type;
    typedef A1                               argument_type;
  };
  // A1 unknown:
  template <class R, class T>
  struct hint<undeterminated_basic<T>,R> {
    typedef R  result_type;
    typedef typename generic_binary_traits<BinaryFunction>::template hint<undeterminated_basic<T>,A2,R>::first_argument_type
            argument_type;
  };
  // A1 & R unknown:
  template <class T1, class T>
  struct hint<undeterminated_basic<T1>,undeterminated_basic<T> > {
    typedef typename generic_binary_traits<BinaryFunction>::template hint<undeterminated_basic<T1>,A2,undeterminated_basic<T> >::first_argument_type
            argument_type;
    typedef typename generic_binary_traits<BinaryFunction>::template hint<undeterminated_basic<T>,A2,undeterminated_basic<T> >::result_type
            result_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type arg_tag) {
    static const space_constant::valued_type arg2_tag = space_constant::valued_tag_traits<A2>::value;
    return generic_binary_traits<BinaryFunction>::valued_tag (arg_tag, arg2_tag) ;
  }
};
// ---------------------------------------------------------------------------
// chap 3.3. binary swapper 
// ---------------------------------------------------------------------------
// as details::mutiplies, for field_vf_bind_bf
// swap is equivalet to binder_second, but with a field instead of a constant
template<class BinaryFunction>
struct swapper {
  swapper (const BinaryFunction& f) : _f(f) {}
  template<class A1, class A2>
  typename generic_binary_traits<BinaryFunction>::template result_hint<A2,A1>::type
  operator() (const A1& x1, const A2& x2) const { return _f(x2,x1); }
protected:
  BinaryFunction  _f;
};
template<class BinaryFunction>
struct generic_binary_traits<swapper<BinaryFunction> > {
  template <class A1, class A2>
  struct result_hint : generic_binary_traits<BinaryFunction>::template result_hint<A2,A1> {};

  template <class A1, class A2, class Result>
  struct hint {
     typedef typename generic_binary_traits<BinaryFunction>::template hint<A2,A1,Result> base;
     typedef typename base::second_argument_type first_argument_type;
     typedef typename base::first_argument_type  second_argument_type;
     typedef typename base::result_type          result_type;
  };
  static space_constant::valued_type
  valued_tag (space_constant::valued_type arg1_tag, space_constant::valued_type arg2_tag) {
    return generic_binary_traits<BinaryFunction>::valued_tag (arg2_tag, arg1_tag) ;
  }
};
// ---------------------------------------------------------------------------
// chap 3.4. unary & binary function wrapper, for compose
// ---------------------------------------------------------------------------
// TODO: use std::function<F> ?
template<class Function>
struct function_wrapper {
  typedef Function type;
};
template<class Result, class Arg>
struct function_wrapper<Result(Arg)> {
  typedef std::function<Result(Arg)> type;
};
template<class Result, class Arg1, class Arg2>
struct function_wrapper<Result(Arg1,Arg2)> {
  typedef std::function<Result(Arg1,Arg2)> type;
};

template<class Function>
inline
Function
function_wrap (Function f)
{
  return f;
}
template<class Result, class Arg>
inline
std::pointer_to_unary_function<Arg,Result>
function_wrap (Result(*f)(Arg))
{
  return std::ptr_fun(f);
}
template<class Result, class Arg1, class Arg2>
inline
std::pointer_to_binary_function<Arg1,Arg2,Result>
function_wrap (Result(*f)(Arg1,Arg2))
{
  return std::ptr_fun(f);
}

}} // namespace rheolef::details
#endif // _RHEOLEF_EXPRESSION_H
