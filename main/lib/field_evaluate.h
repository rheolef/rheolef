#ifndef _RHEOLEF_FIELD_EVALUATE_H
#define _RHEOLEF_FIELD_EVALUATE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// evaluate a field on a predefined point set: hat_x[q], q=0..nq
// See also piola_transformation.h
//
#include "rheolef/field.h"
#include "rheolef/fem_on_pointset.h"
namespace rheolef { 

// -----------------------------------------------------
// scalar-valued case:
// -----------------------------------------------------
template<class T, class M>
void
field_evaluate (
  const field_basic<T,M>&            uh,
  const basis_on_pointset<T>&        bops,
  reference_element                  hat_K,
  const std::vector<size_t>&         dis_idof,
  Eigen::Matrix<T,Eigen::Dynamic,1>& value);
// -----------------------------------------------------
// vector-valued case:
// -----------------------------------------------------
template<class T, class M>
void
vector_field_evaluate (
  const field_basic<T,M>&                         uh,
  const basis_on_pointset<T>&                     bops,
  reference_element                               hat_K,
  const std::vector<size_t>&                      dis_idof_tab,
  const basis_on_pointset<T>&                     piola_on_geo_basis,
        std::vector<size_t>&                      dis_inod_geo,
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value);
// -----------------------------------------------------
// tensor-valued case:
// -----------------------------------------------------
template<class T, class M>
void
tensor_field_evaluate (
  const field_basic<T,M>&                          uh,
  const basis_on_pointset<T>&                      bops,
  reference_element                                hat_K,
  const std::vector<size_t>&                       dis_idof_tab,
  Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,1>& value);
// -----------------------------------------------------
// homogeneous multi-component case: get the i-th value
// -----------------------------------------------------
template<class T, class M>
void
field_component_evaluate (
  const field_basic<T,M>&                         uh,
  const basis_on_pointset<T>&                     bops,
  reference_element                               hat_K,
  const std::vector<size_t>&                      dis_idof_tab,
  size_t                                          k_comp,
  Eigen::Matrix<T,Eigen::Dynamic,1>&              value);
// -----------------------------------------------------
// generic interface
// -----------------------------------------------------
template<class T, class M>
inline
void
general_field_evaluate (
  const field_basic<T,M>&            uh,
  const basis_on_pointset<T>&        bops,
  reference_element                  hat_K,
  const std::vector<size_t>&         dis_idof,
  const basis_on_pointset<T>&        piola_on_geo_basis,
        std::vector<size_t>&         dis_inod_geo,
  Eigen::Matrix<T,Eigen::Dynamic,1>& value)
{
  field_evaluate (uh, bops, hat_K, dis_idof, value);
}
template<class T, class M>
inline
void
general_field_evaluate (
  const field_basic<T,M>&                         uh,
  const basis_on_pointset<T>&                     bops,
  reference_element                               hat_K,
  const std::vector<size_t>&                      dis_idof,
  const basis_on_pointset<T>&                     piola_on_geo_basis,
        std::vector<size_t>&                      dis_inod_geo,
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value)
{
  vector_field_evaluate (uh, bops, hat_K, dis_idof, piola_on_geo_basis, dis_inod_geo, value);
}
template<class T, class M>
inline
void
general_field_evaluate (
  const field_basic<T,M>&                          uh,
  const basis_on_pointset<T>&                      bops,
  reference_element                                hat_K,
  const std::vector<size_t>&                       dis_idof,
  const basis_on_pointset<T>&                      piola_on_geo_basis,
        std::vector<size_t>&                       dis_inod_geo,
  Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,1>& value)
{
   tensor_field_evaluate (uh, bops, hat_K, dis_idof, value);
}
template<class T, class M>
inline
void
general_field_evaluate (
  const field_basic<T,M>&                           uh,
  const basis_on_pointset<T>&                       bops,
  reference_element                                 hat_K,
  const std::vector<size_t>&                        dis_idof,
  const basis_on_pointset<T>&                       piola_on_geo_basis,
        std::vector<size_t>&                        dis_inod_geo,
  Eigen::Matrix<tensor3_basic<T>,Eigen::Dynamic,1>& value)
{
  fatal_macro ("tensor3: not yet");
#ifdef TODO
  value = tensor3_field_evaluate (uh, bops, hat_K, dis_idof, q);
#endif // TODO
}
template<class T, class M>
inline
void
general_field_evaluate (
  const field_basic<T,M>&                           uh,
  const basis_on_pointset<T>&                       bops,
  reference_element                                 hat_K,
  const std::vector<size_t>&                        dis_idof,
  const basis_on_pointset<T>&                       piola_on_geo_basis,
          std::vector<size_t>&                      dis_inod_geo,
  Eigen::Matrix<tensor4_basic<T>,Eigen::Dynamic,1>& value)
{
  fatal_macro ("tensor4: not yet");
#ifdef TODO
  value = tensor4_field_evaluate (uh, bops, hat_K, dis_idof, q);
#endif // TODO
}
// -----------------------------------------------------
// new basis interface
// -----------------------------------------------------
template<class T, class M, class Value>
void
field_evaluate_continued (
  const field_basic<T,M>&                                   uh,
  const geo_basic<T,M>&                                     omega_K,
  const geo_element&                                        K,
  const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& phij_xi,
  Eigen::Matrix<Value,Eigen::Dynamic,1>&                    value);

template<class T, class M, class Value>
void
field_evaluate (
  const field_basic<T,M>&                                   uh,
  const fem_on_pointset<T>&                                 fops,
  const geo_basic<T,M>&                                     omega_K,
  const geo_element&                                        K,
  Eigen::Matrix<Value,Eigen::Dynamic,1>&                    value);

}// namespace rheolef
#endif // _RHEOLEF_FIELD_EVALUATE_H
