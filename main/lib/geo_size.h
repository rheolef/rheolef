#ifndef _RHEOLEF_GEO_SIZE_H
#define _RHEOLEF_GEO_SIZE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/distributor.h"
#include "rheolef/reference_element.h"

namespace rheolef {

struct geo_size {

// typedefs:

  typedef distributor::size_type size_type;

// allocator:

   geo_size();

// accessors:

  size_type map_dimension() const { return _map_dimension; }

// utilities:

  // compute variant from dis_ige:
  size_type dis_ige2variant (size_type map_dim, size_type dis_ige) const;

  // compute dis_igev from dis_ige & variant:
  size_type dis_ige2dis_igev_by_variant   (size_type variant, size_type dis_ige) const;
  size_type dis_ige2dis_igev_by_dimension (size_type map_dim, size_type dis_ige) const;
  size_type dis_ige2dis_igev_by_dimension (size_type map_dim, size_type dis_ige, size_type& variant) const;

  // compute dis_igev from dis_ige
  size_type dis_ige2dis_igev ( size_type map_dim, size_type dis_ige) const;

  // dis_inod 2 dis_iv converter, for high-order meshes, when some nodes are not vertices
  size_type dis_inod2dis_iv (size_type dis_inod) const;
  size_type dis_iv2dis_inod (size_type dis_iv) const;

// data:

  size_type   _map_dimension;
  distributor ownership_by_dimension [4]; // dist. by geo_element dimension: 0,1,2,3
  distributor ownership_by_variant   [reference_element::max_variant]; // by type: t,q...
  distributor node_ownership;             // node dist. differs from vertex one when geo order > 1
  distributor first_by_variant       [reference_element::max_variant]; // cumul by type: (t,q), (T,P,H)...

protected: 
  size_type _dis_ige2dis_igev (size_type map_dim, size_type variant, size_type dis_ige) const;
};
// default cstor with initializers (-Weffc++)
inline
geo_size::geo_size()
: _map_dimension(0),
  ownership_by_dimension(),
  ownership_by_variant(),
  node_ownership(),
  first_by_variant()
{
}

} // namespace rheolef
#endif // _RHEOLEF_GEO_ELEMENT_H
