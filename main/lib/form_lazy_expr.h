# ifndef _RHEOLEF_FORM_LAZY_EXPR_H
# define _RHEOLEF_FORM_LAZY_EXPR_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// form_lazy_expr = result of a lazy integrate() that are combined in exprs
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   30 march 1920

namespace rheolef {
/**
@internalclassfile form_lazy_expr form expressions: concept and class hierarchy

Description
===========
This page explains the design of
the class hierarchy used for the representation of @ref form_2
expressions and how they fit together.
Casual users probably need not concern themselves with these details,
but it may be useful for both advanced users and Rheolef's developers.

Expression features
===================
Expressions involving forms are computed in a **lazy** way:
nothing is done until a conversion to the @ref form_2 class is explicitly required.
Recall that @ref form_2 are represented by distributed sparse matrix.
This approach allows many memory and computing time optimizations.
Moreover, it enable new features, such as matrix inversion
on the fly during assembly process when matrix are block diagonal
at the element level.
Such a feature is heavily used by hybrid discontinuous Galerkin and HHO methods.
In these cases, complex expressions are evaluated in only one loop instead
of several ones with temporary variables.
Note that matrix-matrix 
products cannot be assembled efficiently in a lazy way,
since the sparsity pattern is expected to change:
in these cases, temporaries are used.

The efficient **lazy** implementation is obtained by using the
<a href="https://en.wikipedia.org/wiki/Expression_templates">
expression template</a>
technique in C++.
In consequence,
if `a` is a @ref form_2, then, for instance,
`-a` and `lambda*a`, where `lambda` is a scalar,
are no longer a @ref form_2 but a *form expression*.
It leads to represent at compile time
an expression tree by a hierarchy of classes.
Each class represent a node of the expression tree
while leaves, also called terminals, are 
the field themselves.

Any class representing an expression node tree
derives from the `form_lazy_base` and is called here a `form_lazy`:
it is represented by a C++ concept.
Since concepts in C++ would be available only with the forthcoming 2020 standard,
these concepts are actually represented by traits in Rheolef.

Detailed specifications
========================
The `form_lazy` concept is defined by:

       form_lazy:
            form
          | integrate (opt_domain form_test opts)
          | +- form_lazy
          | constant * form_lazy
          | form_lazy / constant
          | form_lazy +- form_lazy
          | form_lazy * form_lazy
          | trans (form_lazy)
          | inv (form_lazy)
          | lump (form_lazy)

The `opt_domain` stands for an optional domain specification
and opts for an optional @ref integrate_option_3 variable.
Finally, the `form_test` concept involved by the @ref integrate_3 call
is defined by:

        form_test:
            field_test * field_trial
            field_trial * field_test

where `field_test` has been defined by @ref field_lazy_8
and `field_trial` is obtained by replacing `test` by `trial`
in the field expression.

==============
Implementation
==============
The implementation is similar to those of the
@ref field_lazy_8 class hierarchy.
*/
} // namespace rheolef

//
// SUMMARY: see also "form_lazy_terminal.h"
// 3. unary expressions
//    3.1. unary_minus & unary_plus
//    3.2. invert
//    3.3. transpose
// 4. binary expressions
//    4.1. plus & minus
//    4.2. multiply
// 5. form allocator
//

/*
  Question: 
    What is the difference between
	field_lazy_xxx and field_expr_xxx ?
	 form_lazy_xxx and  form_expr_xxx ?
  Response: 
    "lazy" means un-assembled matrix and vectors, here fields and forms.
    The idea is to compute on the fly fields and forms during an iteration
    on the element of the mesh.

  exemple 1:
      wh = uh + vh;
      1) When both uh and vh are assembled (ie field) 
         the loop is unrolled at the "dof" level:
           wh(i)=  uh(i) + vh(i)
	 => field_expr unroll at the "dof" level
      2) Here, either uh or vh are un-assembled (ie field_lazy)
         the loop is unrolled at the element level:
           wh/K = uh/K + vh/K
	 => field_lazy unrolls at the element level

  exemple 2:
      wh = a*uh + b*vh;
      1) When both uh and vh are assembled (ie field) 
         the loop is unrolled at the "dof" level:
           wh(i)=  sum_j a_ij*uh(j) + sum_k b_ik*vh(k)
	 => field_expr unroll at the dof level
      2) Here, either uh or vh are un-assembled (ie field_lazy)
         the loop is unrolled at the element level:
           wh/K = a/K*uh/K + b/K*vh/K
         assuming that we use discontiuous elements (eg "Pkd")
         otherwise a and/or b are not diagonal versus K
	 => field_lazy unrolls at the element level

   exemple 3:
          auto a = ...
          auto b = ...
          auto c = ...
          form S = inv(a + b*inv(c)*trans(b));
      The two imbricated inversions are possible here
      based on an unassembled form_lazy a, b, c.
      Otherwise, with a, b, c as forms, this is not possible.
      Such an idom is typical in the HHO method, see dirichlet_hho.cc

 IMPLEMENTATION NOTES:
 * Expressions operates at the elementy level:
   element matrix are: inverted, transposed, mult, etc.
   Effective computations are delayed until the full
   expr is converted to form_basic<T,M>
 * A base class defines the common methods for all form_lazy_xxx
   it uses the CRTP C++ idiom:
   https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
 * Some computations are expensive :
   - for field:  O(n^p) with p > 1 where n is the typical element-vector size
   - for forms:  O(n^p) with p > 2 where n is the typical element-matrix size
   Are converned:
   - field: a*u, u=integrate(...), u=interpolate(...)    
   - forms: inv(a), a*b, a=integrate(...)
   For these expensive computations, it is possible to share the result
   of computations by speciying where is the common sub-expresion:
   example:
       auto a1 = inv(m)*b - b*inv(m); // inv(m) is computed 2 times
       auto inv_m = inv(m);           // the common sub-expr
       auto a2 = inv_m*b - b*inv_m;   // inv(m) computed once on the fly
   requirements:
   - The initialize() method should be "const" for the smart_pointer
     to avoid generating a true copy when calling initialize()
   - This also avoid the true-copy semantic of the imbedded eigen::matrix
     that stores the shared result


*/
#include "rheolef/form_lazy_terminal.h"

namespace rheolef {

// -------------------------------------------------------------------
// 3. unary expressions
// -------------------------------------------------------------------
// 3.1. unary_minus & unary_plus
// -------------------------------------------------------------------
namespace details {

template<class Unop, class Expr>
class form_lazy_unop: public form_lazy_base<form_lazy_unop<Unop,Expr>> {
public :
// definitions:

  using base = form_lazy_base<form_lazy_unop<Unop,Expr>>;
  using size_type   = geo_element::size_type;
  using memory_type = typename Expr::memory_type;
  using scalar_type = typename Expr::scalar_type;
  using space_type  = typename Expr::space_type;
  using geo_type    = typename Expr::geo_type;
  using float_type  = typename float_traits<scalar_type>::type;
  using band_type   = band_basic<float_type,memory_type>;
  using matrix_element_type = Eigen::Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic>;

// allocator:

  form_lazy_unop (const Unop& unop, const Expr& expr)
    : base(),
      _unop(unop),
      _expr(expr)
    {}

// accessors:

  const geo_type&   get_geo()         const { return _expr.get_geo(); }
  const space_type& get_trial_space() const { return _expr.get_trial_space(); }
  const space_type& get_test_space()  const { return _expr.get_test_space(); }
  bool  is_on_band()                  const { return false; }
  band_type get_band()                const { return band_type(); }

  void initialize (const geo_type& omega_K) { _expr.initialize (omega_K); }
  bool is_diagonal() const                  { return _expr.is_diagonal(); }

  void evaluate (
      const geo_type&            omega_K,
      const geo_element&         K,
            matrix_element_type& ak) const;
// data:
protected:
  Unop  _unop;
  Expr  _expr;
};
// concept;
template<class Unop, class Expr>
struct is_form_lazy <form_lazy_unop<Unop,Expr> > : std::true_type {};

// inlined:
template<class Unop, class Expr>
void
form_lazy_unop<Unop,Expr>::evaluate (
  const geo_type&            omega_K,
  const geo_element&         K,
        matrix_element_type& bk) const
{
  matrix_element_type ak;
  _expr.evaluate (omega_K, K, ak);
  bk = ak.unaryExpr(_unop);
}

}// namespace details

//! @brief -a, +a: see the @ref form_2 page for the full documentation
#define _RHEOLEF_form_lazy_unop(OP,NAME) 				\
template<class Expr, class Sfinae = typename std::enable_if<details::is_form_lazy<Expr>::value, Expr>::type> \
details::form_lazy_unop<NAME,Expr>					\
operator OP (const Expr& a)						\
{									\
  return details::form_lazy_unop<NAME,Expr> (NAME(),a);			\
}
_RHEOLEF_form_lazy_unop(+,details::unary_plus)
_RHEOLEF_form_lazy_unop(-,details::negate)
#undef _RHEOLEF_form_lazy_unop
// -------------------------------------------------------------------
// 3.2. invert
// -------------------------------------------------------------------
namespace details {

template<class Expr>
class form_lazy_invert_rep {
public :
// definitions:

  using size_type   = geo_element::size_type;
  using memory_type = typename Expr::memory_type;
  using scalar_type = typename Expr::scalar_type;
  using space_type  = typename Expr::space_type;
  using geo_type    = typename Expr::geo_type;
  using float_type  = typename float_traits<scalar_type>::type;
  using band_type   = band_basic<float_type,memory_type>;
  using matrix_element_type = Eigen::Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic>;

// allocator:

  form_lazy_invert_rep (const Expr& expr)
   : _expr(expr),
     _prev_omega_K(),
     _prev_K_dis_ie(std::numeric_limits<size_type>::max()), 
     _prev_ak()
   {}

// accessors:

  const geo_type&   get_geo()         const { return _expr.get_geo(); }
  const space_type& get_trial_space() const { return _expr.get_trial_space(); }
  const space_type& get_test_space()  const { return _expr.get_test_space(); }
  bool  is_on_band()                  const { return false; }
  band_type get_band()                const { return band_type(); }

  void initialize (const geo_type& omega_K) const;
  bool is_diagonal() const { return _expr.is_diagonal(); }

  void evaluate (
      const geo_type&            omega_K,
      const geo_element&         K,
            matrix_element_type& ak) const;
// data:
protected:
  mutable Expr                _expr;
  mutable geo_type            _prev_omega_K;
  mutable size_type           _prev_K_dis_ie;
  mutable matrix_element_type _prev_ak;
};
// inlined:
template<class Expr>
void
form_lazy_invert_rep<Expr>::initialize (const geo_type& omega_K) const
{
  check_macro (get_trial_space() == get_test_space(),
    "inv(form): spaces "
        << "\"" <<get_trial_space().name()<<"\" and \"" <<get_test_space().name()<<"\""
        << " should be equal");

  check_macro (get_trial_space().get_constitution().have_compact_support_inside_element() &&
                get_test_space().get_constitution().have_compact_support_inside_element(),
      "inv(form): requires compact support inside elements (e.g. discontinuous or bubble)");
  _expr.initialize (omega_K);
  _prev_omega_K  = omega_K;
  _prev_K_dis_ie = std::numeric_limits<size_type>::max();
}
template<class Expr>
void
form_lazy_invert_rep<Expr>::evaluate (
  const geo_type&            omega_K,
  const geo_element&         K,
        matrix_element_type& ak) const
{
  if (_prev_omega_K == omega_K && _prev_K_dis_ie == K.dis_ie()) {
    trace_macro("inv(K="<<K.name()<<K.dis_ie()<<",prev="<<_prev_K_dis_ie<<"): re-use");
    ak = _prev_ak;
    return;
  }
  trace_macro("inv(K="<<K.name()<<K.dis_ie()<<",prev="<<_prev_K_dis_ie<<"): compute");
  _expr.evaluate (omega_K, K, ak);
#ifdef _RHEOLEF_PARANO
  check_macro (ak.rows() == ak.cols(), "inv: matrix should be square");
#endif // _RHEOLEF_PARANO
  details::local_invert (ak, _expr.is_diagonal());
  _prev_ak       = ak; // expensive to compute, so memorize it for common subexpressions
  _prev_omega_K  = omega_K;
  _prev_K_dis_ie = K.dis_ie();
}

template<class Expr>
class form_lazy_invert: public smart_pointer_nocopy<form_lazy_invert_rep<Expr>>,
                        public form_lazy_base      <form_lazy_invert<Expr>> {
public :
// definitions:

  using rep   = form_lazy_invert_rep<Expr>;
  using base1 = smart_pointer_nocopy<rep>;
  using base2 = form_lazy_base<form_lazy_invert<Expr>>;
  using size_type   = typename rep::size_type;
  using memory_type = typename rep::memory_type;
  using scalar_type = typename rep::scalar_type;
  using space_type  = typename rep::space_type;
  using geo_type    = typename rep::geo_type;
  using band_type   = typename rep::band_type;
  using matrix_element_type = typename rep::matrix_element_type;

// allocator:

  form_lazy_invert (const Expr& expr)
   : base1(new_macro(rep(expr))),
     base2()
   {}

// accessors:

  const geo_type&   get_geo()         const { return base1::data().get_geo(); }
  const space_type& get_trial_space() const { return base1::data().get_trial_space(); }
  const space_type& get_test_space()  const { return base1::data().get_test_space(); }
  bool  is_on_band()                  const { return base1::data().is_on_band(); }
  band_type get_band()                const { return base1::data().get_band(); }

  void initialize (const geo_type& omega_K) const { return base1::data().initialize (omega_K); }
  bool is_diagonal() const { return base1::data().is_diagonal(); }

  void evaluate (
      const geo_type&            omega_K,
      const geo_element&         K,
            matrix_element_type& ak) const
      { return base1::data().evaluate (omega_K, K, ak); }
};
// concept;
template<class Expr> struct is_form_lazy <form_lazy_invert<Expr> > : std::true_type {};

}// namespace details

//! @brief inv: see the @ref form_2 page for the full documentation
template<class Expr, class Sfinae = typename std::enable_if<details::is_form_lazy<Expr>::value, Expr>::type>
details::form_lazy_invert<Expr>
inv (const Expr& a)
{
  return details::form_lazy_invert<Expr> (a);
}
// -------------------------------------------------------------------
// 3.3. transpose
// -------------------------------------------------------------------
namespace details {

template<class Expr>
class form_lazy_transpose: public form_lazy_base<form_lazy_transpose<Expr>> {
public :
// definitions:

  using base        = form_lazy_base<form_lazy_transpose<Expr>>;
  using size_type   = geo_element::size_type;
  using memory_type = typename Expr::memory_type;
  using scalar_type = typename Expr::scalar_type;
  using space_type  = typename Expr::space_type;
  using geo_type    = typename Expr::geo_type;
  using float_type  = typename float_traits<scalar_type>::type;
  using band_type   = band_basic<float_type,memory_type>;
  using matrix_element_type = Eigen::Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic>;

// allocator:

  form_lazy_transpose (const Expr& expr)
   : base(),
    _expr(expr)
   {}

// accessors:

  const geo_type&   get_geo()         const { return _expr.get_geo(); }
  const space_type& get_trial_space() const { return _expr.get_test_space(); }  // swapped!
  const space_type& get_test_space()  const { return _expr.get_trial_space(); } // swapped!
  bool  is_on_band()                  const { return false; }
  band_type get_band()                const { return band_type(); }

  void initialize (const geo_type& omega_K) { _expr.initialize (omega_K); }
  bool is_diagonal() const { return _expr.is_diagonal(); }

  void evaluate (
      const geo_type&            omega_K,
      const geo_element&         K,
            matrix_element_type& ak) const;
// data:
protected:
  Expr _expr;
};
// concept;
template<class Expr> struct is_form_lazy <form_lazy_transpose<Expr> > : std::true_type {};

// inlined:
template<class Expr>
void
form_lazy_transpose<Expr>::evaluate (
  const geo_type&            omega_K,
  const geo_element&         K,
        matrix_element_type& ak) const
{
  _expr.evaluate (omega_K, K, ak);
  ak.transposeInPlace();
}

}// namespace details

//! @brief trans: see the @ref form_2 page for the full documentation
template<class Expr, class Sfinae = typename std::enable_if<details::is_form_lazy<Expr>::value, Expr>::type>
details::form_lazy_transpose<Expr>
trans (const Expr& a)
{
  return details::form_lazy_transpose<Expr> (a);
}
// -------------------------------------------------------------------
// 4. binary expressions
// -------------------------------------------------------------------
// 4.1. add & minus
// -------------------------------------------------------------------
namespace details {

template<class Binop, class Expr1, class Expr2>
class form_lazy_add: public form_lazy_base<form_lazy_add<Binop,Expr1,Expr2>> {
public :
// definitions:

  using base = form_lazy_base<form_lazy_add<Binop,Expr1,Expr2>>;
  using size_type   = geo_element::size_type;
  using memory_type = typename Expr1::memory_type;
  using scalar_type = typename Expr1::scalar_type;
  using space_type  = typename Expr1::space_type;
  using geo_type    = typename Expr1::geo_type;
  using float_type  = typename float_traits<scalar_type>::type;
  using band_type   = band_basic<float_type,memory_type>;
  using matrix_element_type = Eigen::Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic>;

// allocator:

  form_lazy_add (const Expr1& expr1, const Expr2& expr2)
    : base(),
     _binop(),
     _expr1(expr1),
     _expr2(expr2) {}

// accessors:

  const geo_type&   get_geo() const;
  const space_type& get_trial_space() const { return _expr1.get_trial_space(); }
  const space_type& get_test_space()  const { return _expr1.get_test_space(); }
  bool  is_on_band()                  const { return false; }
  band_type get_band()                const { return band_type(); }

  void initialize (const geo_type& omega_K);
  bool is_diagonal() const { return _expr1.is_diagonal() &&
                                    _expr2.is_diagonal(); }

  void evaluate (
      const geo_type&            omega_K,
      const geo_element&         K,
            matrix_element_type& ak) const;
// data:
protected:
  Binop _binop;
  Expr1 _expr1;
  Expr2 _expr2;
};
// concept;
template<class Binop, class Expr1, class Expr2>
struct is_form_lazy <form_lazy_add<Binop,Expr1,Expr2> > : std::true_type {};

// inlined:
template<class Binop, class Expr1, class Expr2>
void
form_lazy_add<Binop,Expr1,Expr2>::initialize (const geo_type& omega_K)
{
  // TODO: subdomains e.g. robin
  check_macro (_expr1.get_geo() == _expr2. get_geo(),
     "lazy_add: different domain not yet supported");

  check_macro (_expr1.get_trial_space()	== _expr2.get_trial_space() &&
               _expr1. get_test_space()	== _expr2. get_test_space(),
    "lazy_add: incompatible spaces "
        << "[\"" <<_expr1.get_trial_space().name()<<"\", \"" <<_expr1.get_test_space().name()<<"\"] and "
        << "[\"" <<_expr2.get_trial_space().name()<<"\", \"" <<_expr2.get_test_space().name()<<"\"]");

  _expr1.initialize (omega_K);
  _expr2.initialize (omega_K);
}
template<class Binop, class Expr1, class Expr2>
const typename form_lazy_add<Binop,Expr1,Expr2>::geo_type&
form_lazy_add<Binop,Expr1,Expr2>::get_geo() const
{
  // TODO: subdomains e.g. robin
  return _expr1.get_geo();
}
template<class Binop, class Expr1, class Expr2>
void
form_lazy_add<Binop,Expr1,Expr2>::evaluate (
  const geo_type&            omega_K,
  const geo_element&         K,
        matrix_element_type& ck) const
{
  matrix_element_type ak, bk;
  _expr1.evaluate (omega_K, K, ak);
  _expr2.evaluate (omega_K, K, bk);
#ifdef _RHEOLEF_PARANO
  check_macro (ak.rows() == bk.rows() && ak.cols() == bk.cols(), "a+b: invalid sizes");
#endif // _RHEOLEF_PARANO
  ck = ak.binaryExpr (bk, _binop);
}

}// namespace details

//! @brief a+b, a-b: see the @ref form_2 page for the full documentation
#define _RHEOLEF_form_lazy_add(OP,NAME) 				\
template<class Expr1, class Expr2,					\
    class Sfinae1 = typename std::enable_if<details::is_form_lazy<Expr1>::value, Expr1>::type, \
    class Sfinae2 = typename std::enable_if<details::is_form_lazy<Expr2>::value, Expr2>::type> \
details::form_lazy_add<details::NAME,Expr1,Expr2>			\
operator OP (const Expr1& a, const Expr2& b)				\
{									\
  return details::form_lazy_add<details::NAME,Expr1,Expr2> (a,b);	\
}
_RHEOLEF_form_lazy_add(+,plus)
_RHEOLEF_form_lazy_add(-,minus)
#undef _RHEOLEF_form_lazy_add
// -------------------------------------------------------------------
// 4.2. multiply
// -------------------------------------------------------------------
// TODO: shared on common subexpressions
namespace details {

template<class Expr1, class Expr2>
class form_lazy_multiply_rep {
public :
// definitions:

  using size_type = geo_element::size_type;
  using memory_type = typename Expr1::memory_type;
  using scalar_type = typename Expr1::scalar_type;
  using space_type  = typename Expr1::space_type;
  using geo_type    = typename Expr1::geo_type;
  using float_type  = typename float_traits<scalar_type>::type;
  using band_type   = band_basic<float_type,memory_type>;
  using matrix_element_type = Eigen::Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic>;

// allocator:

  form_lazy_multiply_rep (const Expr1& expr1, const Expr2& expr2)
   : _expr1(expr1),
     _expr2(expr2),
     _prev_omega_K(),
     _prev_K_dis_ie(std::numeric_limits<size_type>::max()), 
     _prev_ck()
    {}

// accessors:

  const geo_type&   get_geo() const;
  const space_type& get_trial_space() const { return _expr2.get_trial_space(); }
  const space_type& get_test_space()  const { return _expr1.get_test_space(); }
  bool  is_on_band()                  const { return false; }
  band_type get_band()                const { return band_type(); }

  void initialize (const geo_type& omega_K) const;
  bool is_diagonal() const { return _expr1.is_diagonal() && 
                                    _expr2.is_diagonal(); }

  void evaluate (
      const geo_type&            omega_K,
      const geo_element&         K,
            matrix_element_type& ak) const;
// data:
protected:
  mutable Expr1               _expr1;
  mutable Expr2               _expr2;
  mutable geo_type            _prev_omega_K;
  mutable size_type           _prev_K_dis_ie;
  mutable matrix_element_type _prev_ck;
};
// inlined:
template<class Expr1, class Expr2>
void
form_lazy_multiply_rep<Expr1,Expr2>::initialize (const geo_type& omega_K) const
{
  // TODO: subdomains e.g. robin
  check_macro (_expr1.get_geo() == _expr2. get_geo(),
     "lazy_multiply: different domain not yet supported");

  check_macro (_expr1.get_trial_space()	== _expr2. get_test_space(),
    "lazy_multiply: incompatible spaces \""
    <<_expr1.get_trial_space().name()<<"\" and \""
    <<_expr2. get_test_space().name()<<"\"");

  if (! _expr1. get_test_space().get_constitution().have_compact_support_inside_element() ||
      ! _expr1.get_trial_space().get_constitution().have_compact_support_inside_element() ||
      ! _expr2.get_trial_space().get_constitution().have_compact_support_inside_element()   ) {
    warning_macro("lazy_multiply: requires compact support inside elements (e.g. discontinuous or bubble)");
    warning_macro("lazy_multiply: space was "
        << "[\"" <<_expr1.get_trial_space().name()<<"\", \"" <<_expr1.get_test_space().name()<<"\"] and "
        << "[\"" <<_expr2.get_trial_space().name()<<"\", \"" <<_expr2.get_test_space().name()<<"\"]");
    fatal_macro("lazy_multiply: HINT: convert to \"form\" before to do the product");
  }
  _expr1.initialize (omega_K);
  _expr2.initialize (omega_K);
  _prev_omega_K  = omega_K;
  _prev_K_dis_ie = std::numeric_limits<size_type>::max(); 
  trace_macro("mult(omega_K="<<omega_K.name()<<"): init prev:="<<_prev_K_dis_ie<<" for this="<<(this));
}
template<class Expr1, class Expr2>
const typename form_lazy_multiply_rep<Expr1,Expr2>::geo_type&
form_lazy_multiply_rep<Expr1,Expr2>::get_geo() const
{
  // TODO: subdomains e.g. robin
  return _expr1.get_geo();
}
template<class Expr1, class Expr2>
void
form_lazy_multiply_rep<Expr1,Expr2>::evaluate (
  const geo_type&            omega_K,
  const geo_element&         K,
        matrix_element_type& ck) const
{
  if (_prev_omega_K == omega_K && _prev_K_dis_ie == K.dis_ie()) {
    trace_macro("mult(K="<<K.name()<<K.dis_ie()<<",prev="<<_prev_K_dis_ie<<"): re-use this="<<(this));
    ck = _prev_ck;
    return;
  }
  trace_macro("mult(K="<<K.name()<<K.dis_ie()<<",prev="<<_prev_K_dis_ie<<"): compute this="<<(this));
  matrix_element_type ak, bk;
  _expr1.evaluate (omega_K, K, ak);
  _expr2.evaluate (omega_K, K, bk);
#ifdef _RHEOLEF_PARANO
  check_macro (ak.cols() == bk.rows(), "a*b: invalid sizes");
#endif // _RHEOLEF_PARANO
  ck = ak*bk;
  _prev_ck       = ck; // expensive to compute, so memorize it for common subexpressions
  _prev_omega_K  = omega_K;
  _prev_K_dis_ie = K.dis_ie();
  trace_macro("mult(K="<<K.name()<<K.dis_ie()<<"): prev:="<<_prev_K_dis_ie<<" for this="<<(this));
}
template<class Expr1, class Expr2>
class form_lazy_multiply: public smart_pointer_nocopy<form_lazy_multiply_rep<Expr1,Expr2>>,
                          public form_lazy_base      <form_lazy_multiply<Expr1,Expr2>> {
public :
// definitions:

  using rep   = form_lazy_multiply_rep<Expr1,Expr2>;
  using base1 = smart_pointer_nocopy<rep>;
  using base2 = form_lazy_base<form_lazy_multiply<Expr1,Expr2>>;
  using size_type   = typename rep::size_type;
  using memory_type = typename rep::memory_type;
  using scalar_type = typename rep::scalar_type;
  using space_type  = typename rep::space_type;
  using geo_type    = typename rep::geo_type;
  using band_type   = typename rep::band_type;
  using matrix_element_type = typename rep::matrix_element_type;

// allocator:

  form_lazy_multiply (const Expr1& expr1, const Expr2& expr2)
   : base1(new_macro(rep(expr1,expr2))),
     base2()
    {}

// accessors:

  const geo_type&   get_geo()         const { return base1::data().get_geo(); }
  const space_type& get_trial_space() const { return base1::data().get_trial_space(); }
  const space_type& get_test_space()  const { return base1::data().get_test_space(); }
  bool  is_on_band()                  const { return base1::data().is_on_band(); }
  band_type get_band()                const { return base1::data().get_band(); }

  void initialize (const geo_type& omega_K) const { base1::data().initialize (omega_K); }
  bool is_diagonal() const                        { return base1::data().is_diagonal(); }

  void evaluate (
      const geo_type&            omega_K,
      const geo_element&         K,
            matrix_element_type& ak) const
    { base1::data().evaluate (omega_K, K, ak); }

};
// concept;
template<class Expr1, class Expr2> struct is_form_lazy <form_lazy_multiply<Expr1,Expr2> > : std::true_type {};

}// namespace details

//! @brief a*b: see the @ref form_2 page for the full documentation
template<class Expr1, class Expr2,
    class Sfinae1 = typename std::enable_if<details::is_form_lazy<Expr1>::value, Expr1>::type,
    class Sfinae2 = typename std::enable_if<details::is_form_lazy<Expr2>::value, Expr2>::type>
details::form_lazy_multiply<Expr1,Expr2>
operator* (const Expr1& a, const Expr2& b)
{
  return details::form_lazy_multiply<Expr1,Expr2> (a,b);
}
// -------------------------------------------------------
// 5. form allocator
// -------------------------------------------------------
template<class T, class M>
template<class Expr, class Sfinae>
inline
form_basic<T,M>& 
form_basic<T,M>::operator= (const Expr& a)
{
  // here is the main call to the effective computation
  // of all sparse matrix asssembly from element matrix:
  convert_from_form_lazy (a);
  return *this;
}
template<class T, class M>
template<class Expr, class Sfinae>
inline
form_basic<T,M>::form_basic (const Expr& a)
: _X(), _Y(), _uu(), _ub(), _bu(), _bb()
{
  operator= (a);
}

}// namespace rheolef
# endif /* _RHEOLEF_FORM_LAZY_EXPR_H */
