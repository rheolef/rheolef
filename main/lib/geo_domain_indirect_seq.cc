///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo_domain_indirect.h"

namespace rheolef {

template <class T, class M>
const distributor&
geo_domain_indirect_base_rep<T,M>::geo_element_ownership (size_type dim) const 
{
  if (dim == map_dimension()) return  _indirect.ownership();
  check_macro (dim  < map_dimension(), "unexpected dimension = " << dim << " > domain dimension = " << map_dimension());
  return _omega.geo_element_ownership(dim);
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::const_reference
geo_domain_indirect_base_rep<T,M>::get_geo_element (size_type dim, size_type ige) const
{
  if (dim == map_dimension()) return  _omega.get_geo_element (dim, _indirect.oige(ige).index());
  return _omega.get_geo_element (dim, ige);
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::const_iterator_by_variant
geo_domain_indirect_base_rep<T,M>::begin_by_variant (variant_type variant) const
{
  error_macro ("domain.begin: not yet");
  return _omega.begin_by_variant (variant); // (not reached) 
  // Note: here, not valid for dim(variant)=map_dimension ; requires a proxy_reference
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::const_iterator_by_variant
geo_domain_indirect_base_rep<T,M>::end_by_variant (variant_type variant) const
{
  error_macro ("domain.end: not yet");
  return _omega.end_by_variant (variant); // (not reached) 
  // Note: here, not valid for dim(variant)=map_dimension ; requires a proxy_reference
}
template <class T, class M>
const typename geo_domain_indirect_base_rep<T,M>::geo_element_map_type&
geo_domain_indirect_base_rep<T,M>::get_external_geo_element_map (size_type variant) const
{
  error_macro ("domain.get_external_geo_element_map: not yet");
  static const geo_element_map_type dummy;
  return dummy;
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::n_domain_indirect () const
{
  return 0;
}
template <class T, class M>
bool
geo_domain_indirect_base_rep<T,M>::have_domain_indirect(const std::string& name) const
{
  return false;
}
template <class T, class M>
const domain_indirect_basic<M>&
geo_domain_indirect_base_rep<T,M>::get_domain_indirect (size_type i) const
{
  error_macro ("domain[domain] aka sub-sub-domain: not yet");
  return _indirect; // not reached
}
template <class T, class M>
const domain_indirect_basic<M>&
geo_domain_indirect_base_rep<T,M>::get_domain_indirect (const std::string& name) const
{
  error_macro ("domain[domain] aka sub-sub-domain: not yet");
  return _indirect; // not reached
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::insert_domain_indirect (const domain_indirect_basic<M>& dom) const
{
  error_macro ("insert_domain_indirect: cannot insert inside another domain");
}
template <class T, class M>
odiststream&
geo_domain_indirect_base_rep<T,M>::put (odiststream& ops) const
{
  error_macro ("domain.put_geo: not yet");
  return ops;
}
template <class T, class M>
const geo_size&
geo_domain_indirect_base_rep<T,M>::sizes() const
{
  return _omega.sizes();
}
template <class T, class M>
const geo_size&
geo_domain_indirect_base_rep<T,M>::ios_sizes() const 
{ 
  error_macro ("domain.ios_sizes: not yet");
  return _omega.ios_sizes();
}
template <class T, class M>
const typename geo_domain_indirect_base_rep<T,M>::node_type&
geo_domain_indirect_base_rep<T,M>::dis_node (size_type dis_inod) const
{
  return _omega.dis_node (dis_inod);
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::dis_inod (const geo_element& K, std::vector<size_type>& dis_inod) const
{
  _omega.dis_inod (K, dis_inod);
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::dis_inod2dis_iv (size_type dis_inod) const
{
  return _omega.dis_inod2dis_iv (dis_inod);
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::set_nodes (const disarray<node_type,M>&)
{
  error_macro ("domain.set_node: cannot do that");
}
template <class T, class M>
bool
geo_domain_indirect_base_rep<T,M>::check(bool verbose) const
{
  return true;
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::reset_order (size_type order)
{
  error_macro ("domain.reset_order: cannot do that");
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::seq_locate (const point_basic<T>&, size_type) const
{
  error_macro ("domain.seq_locate: no implemented");
  return std::numeric_limits<size_type>::max();
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::dis_locate (const point_basic<T>&, size_type) const
{
  error_macro ("domain.dis_locate: no implemented");
  return std::numeric_limits<size_type>::max();
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::seq_trace_move (
	const point_basic<T>&     x,
        const point_basic<T>&     v,
              point_basic<T>&     y) const
{
  error_macro ("domain.seq_trace_move: no implemented");
  return std::numeric_limits<size_type>::max();
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::dis_trace_move (
	const point_basic<T>&     x,
        const point_basic<T>&     v,
              point_basic<T>&     y) const
{
  error_macro ("domain.dis_trace_move: no implemented");
  return std::numeric_limits<size_type>::max();
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::locate (
  const disarray<point_basic<T>, M>& x,
        disarray<size_type, M>&      dis_ie,
  bool do_check) const
{
  error_macro ("domain.locate: no implemented");
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::trace_ray_boundary (
                const disarray<point_basic<T>,M>&     x,
                const disarray<point_basic<T>,M>&     v,
                      disarray<size_type, M>&         dis_ie,
                      disarray<point_basic<T>,M>&     y,
                bool do_check) const
{
  error_macro ("domain.trace_ray_boundary: no implemented");
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::trace_move (
                const disarray<point_basic<T>,M>&     x,
                const disarray<point_basic<T>,M>&     v,
                      disarray<size_type, M>&         dis_ie,
                      disarray<point_basic<T>,M>&     y) const
{
  error_macro ("domain.trace_move: no implemented");
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::seq_nearest (
                const point_basic<T>&    x,
                      point_basic<T>&    x_nearest) const
{
  error_macro ("domain.seq_nearest: no implemented");
  return std::numeric_limits<size_type>::max();
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::dis_nearest (
                const point_basic<T>&    x,
                      point_basic<T>&    x_nearest) const
{
  error_macro ("domain.dis_nearest: no implemented");
  return std::numeric_limits<size_type>::max();
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::nearest (
                const disarray<point_basic<T>,M>&     x,
                      disarray<point_basic<T>,M>&     x_nearest,
                      disarray<size_type, M>&         dis_ie) const
{
  error_macro ("domain.nearest: no implemented");
}
template <class T, class M>
typename geo_domain_indirect_base_rep<T,M>::size_type
geo_domain_indirect_base_rep<T,M>::neighbour (size_type ie, size_type loc_iside) const
{
  error_macro ("domain.neighbour: no implemented");
  return std::numeric_limits<size_type>::max();
}
template <class T, class M>
void
geo_domain_indirect_base_rep<T,M>::neighbour_guard() const
{
  error_macro ("domain.neighbour_guard: no implemented");
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class geo_domain_indirect_base_rep<Float,sequential>;
#ifdef _RHEOLEF_HAVE_MPI
template class geo_domain_indirect_base_rep<Float,distributed>;
#endif // _RHEOLEF_HAVE_MPI
template class geo_domain_indirect_rep<Float,sequential>;

} // namespace rheolef
