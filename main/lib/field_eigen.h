#ifndef _RHEOLEF_FIELD_EIGEN_H
#define _RHEOLEF_FIELD_EIGEN_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// customize the eigen library for accepting 
//	Array<field> 
//	Array<field,3> 
//
// author: Pierre.Saramito@imag.fr
//
// date: 23 feb 2018
//
#include "rheolef/field.h"
#include "rheolef/compiler_eigen.h"

namespace Eigen {
  template<class T, class M>
  struct NumTraits <rheolef::field_basic<T,M> >
       : NumTraits <typename rheolef::float_traits<T>::type> {
    using NonInteger = T;
    using Nested     = T;
    using Real       = typename rheolef::float_traits<T>::type;
    enum {
      RequireInitialization = 1,
      IsSigned  = 1,
      IsInteger = 0,
      IsComplex = 0, // TODO: check it with some is_complex<T> traits
      ReadCost  = 1,
      AddCost   = 3,
      MulCost   = 3
    };
  };
  template<class BinOp, class T, class M>
  struct ScalarBinaryOpTraits <T, rheolef::field_basic<T,M>, BinOp> {
    using ReturnType = rheolef::field_basic<T,M>;
  };
  template<class BinOp, class T, class M>
  struct ScalarBinaryOpTraits <rheolef::field_basic<T,M>, T, BinOp> {
    using ReturnType = rheolef::field_basic<T,M>;
  };
} // namespace Eigen
#endif // _RHEOLEF_FIELD_EIGEN_H
