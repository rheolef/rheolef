#ifndef _RHEOLEF_FIELD_WDOF_SLICED_H
#define _RHEOLEF_FIELD_WDOF_SLICED_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// proxy class for indexations: uh[0], sigma_h(0,1), etc
// => progress with shift and increment >= 1

// SUMMARY:
// 1) field_sliced_const_iterator
// 2) field_sliced_iterator
// 3) field_rdof_sliced_base
// 4) field_rdof_sliced_const
// 5) field_wdof_sliced

#include "rheolef/field_wdof.h"
#include "rheolef/space_component.h"

namespace rheolef { namespace details {
// =================================================================================
// 1) field_sliced_const_iterator: progress with increment >= 1
// =================================================================================
template <class OutputIterator> class field_sliced_iterator; // forward decl

template <class InputIterator>
class field_sliced_const_iterator {
public:

// definitions:

  using iterator_category = std::forward_iterator_tag;
  using size_type         = std::size_t;
  using value_type        = typename std::iterator_traits<InputIterator>::value_type;
  using reference         = const value_type&;
  using pointer           = const value_type*;
  using difference_type   = std::ptrdiff_t;
  using self_type         = field_sliced_const_iterator<InputIterator>;

// allocators:

  field_sliced_const_iterator() = delete;
  field_sliced_const_iterator(InputIterator iter, size_type incr) : _iter(iter), _incr(incr) {}
  template<class OutputIterator>
  field_sliced_const_iterator(field_sliced_iterator<OutputIterator> i) : _iter(i._iter), _incr(i._incr) {}

// accessors & modifiers:

  reference operator* () const { return *_iter; }
  reference operator[] (size_type n) const { return *(_iter + n*_incr); }

  self_type& operator++ () { _iter += _incr; return *this; }
  self_type  operator++ (int) { self_type tmp = *this; operator++(); return tmp; }
  self_type& operator+= (difference_type n) { _iter += n*_incr; return *this; }
  self_type  operator+  (difference_type n) const { self_type tmp = *this; return tmp += n; }

// comparators:

  bool operator== (const self_type& j) const { return _iter == j._iter; }
  bool operator!= (const self_type& j) const { return ! operator== (j); }
// protected:
// data:
  InputIterator  _iter;
  size_type      _incr;
};
// =================================================================================
// 2) field_sliced_iterator
// =================================================================================
template <class OutputIterator>
class field_sliced_iterator : public field_sliced_const_iterator<OutputIterator> {
public:
// definitions:

  using base       = field_sliced_const_iterator<OutputIterator>;
  using size_type  = typename base::size_type;
  using value_type = typename base::value_type;
  using difference_type   = std::ptrdiff_t;
  using self_type  = field_sliced_iterator<OutputIterator>;

// allocators:

  field_sliced_iterator () = delete;
  field_sliced_iterator(OutputIterator iter, size_type incr) : base(iter,incr) {}

// accessors & modifiers:

  value_type& operator* () { return *base::_iter; }
  value_type& operator[] (size_type n) { return *(base::_iter + n*base::_incr); }

  self_type& operator++ () { base::_iter += base::_incr; return *this; }
  self_type  operator++ (int) { self_type tmp = *this; operator++(); return tmp; }
  self_type& operator+= (difference_type n) { base::_iter += n*base::_incr; return *this; }
  self_type  operator+  (difference_type n) const { self_type tmp = *this; return tmp += n; }
};
// =========================================================================
// 3) field_wdof_sliced_base
// =========================================================================
template<class FieldRdof>
class field_rdof_sliced_base {
public:
// definitions:

  using size_type      = typename FieldRdof::size_type;
  using scalar_type    = typename FieldRdof::scalar_type;
  using memory_type    = typename FieldRdof::memory_type;
  using float_type     = typename float_traits<scalar_type>::type;
  using geo_type       = typename FieldRdof::geo_type;
  using space_type     = typename FieldRdof::space_type;
  using space_constitution_type = space_constitution<float_type,memory_type>;

// allocators:

  field_rdof_sliced_base () : _Vi(), _i_comp(0) {}
  field_rdof_sliced_base (const FieldRdof& uh, size_type i_comp);

// accessors:

  const space_constitution_type& get_constitution() const { return _Vi.get_constitution(); }
#ifdef TO_CLEAN
  std::string name() const { return _Vi.name(); }
  bool have_homogeneous_space (space_type& Xh) const { Xh = get_space(); return true; }
#endif // TO_CLEAN
  const distributor& ownership() const { return _Vi.ownership(); }	
  const communicator& comm() const { return ownership().comm(); }
  const geo_type& get_geo() const { return _Vi.get_geo(); }
  const space_type& get_space() const { return _Vi; }
  size_type     ndof() const { return ownership().size(); }
  size_type dis_ndof() const { return ownership().dis_size(); }

  size_type _shift (const FieldRdof& uh) const;
  size_type _increment (const FieldRdof& uh) const;

protected:
// data:
  space_type _Vi;
  size_type  _i_comp;
};
template<class FieldRdof>
field_rdof_sliced_base<FieldRdof>::field_rdof_sliced_base (const FieldRdof& uh, size_type i_comp)
: _Vi(),
  _i_comp (i_comp)
{
  if (! uh.get_space().get_constitution().is_hierarchical()) {
    _Vi = space_type (uh.get_geo(), uh.get_space().get_constitution().get_basis()[_i_comp].name());
    size_type n_comp = uh.get_space().get_constitution().get_basis().size();
    check_macro (_i_comp < n_comp,
      "field sliced index "<<_i_comp<<" is out of range [0:"<<n_comp<<"[");
  } else {
    _Vi = space_type (uh.get_space() [_i_comp]);
    size_type n_comp = uh.get_space().get_constitution().size();
    check_macro (_i_comp < n_comp,
      "field sliced index "<<_i_comp<<" is out of range [0:"<<n_comp<<"[");
  }
}
template<class FieldRdof>
typename field_rdof_sliced_base<FieldRdof>::size_type
field_rdof_sliced_base<FieldRdof>::_shift (const FieldRdof& uh) const
{
  if (! uh.get_space().get_constitution().is_hierarchical()) {
    return _i_comp;
  } else {
    size_type s = 0;
    for (size_type j_comp = 0; j_comp < _i_comp; j_comp++) {
      s += uh.get_space().get_constitution()[j_comp].ndof();
    }
    return s;
  }
}
template<class FieldRdof>
typename field_rdof_sliced_base<FieldRdof>::size_type
field_rdof_sliced_base<FieldRdof>::_increment (const FieldRdof& uh) const
{
  if (! uh.get_space().get_constitution().is_hierarchical()) {
    return uh.get_space().get_constitution().get_basis().size();
  } else {
    return 1;
  }
}
// =========================================================================
// 4) field_rdof_sliced_const
// =========================================================================
template<class FieldWdof> class field_wdof_sliced; // forward

template<class FieldRdof>
class field_rdof_sliced_const:
  public field_wdof_base<field_rdof_sliced_const<FieldRdof>>
 ,public field_rdof_sliced_base<FieldRdof>
{
public:

// definitions:

  using base           = field_rdof_sliced_base<FieldRdof>;
  using size_type      = typename FieldRdof::size_type;
  using scalar_type    = typename FieldRdof::scalar_type;
  using memory_type    = typename FieldRdof::memory_type;
  using geo_type       = typename FieldRdof::geo_type;
  using space_type     = typename FieldRdof::space_type;
  using const_iterator = field_sliced_const_iterator<typename FieldRdof::const_iterator>;
  using float_type     = typename float_traits<scalar_type>::type;
  using space_constitution_type = space_constitution<float_type,memory_type>;

// allocators:

  field_rdof_sliced_const () = delete;
  field_rdof_sliced_const (const FieldRdof& uh, size_type i_comp);
  field_rdof_sliced_const (const field_wdof_sliced<FieldRdof>& uh_comp);
  field_rdof_sliced_const<FieldRdof>& operator= (const field_rdof_sliced_const<FieldRdof>& uh_comp) = delete;
  const scalar_type& dof (size_type idof) const { return _start [idof]; }
  const_iterator begin_dof() const { return _start; }
  const_iterator end_dof() const   { return _start + base::ndof(); }

protected:
// data:
  const FieldRdof _uh;
  const_iterator  _start;
};
// concept:
template<class FieldRdof>
struct is_field_wdof<field_rdof_sliced_const<FieldRdof>>: std::true_type {};

template<class FieldRdof>
struct field_traits<field_rdof_sliced_const<FieldRdof>> {
  using size_type    = typename FieldRdof::size_type;
  using scalar_type  = typename FieldRdof::scalar_type;
  using memory_type  = typename FieldRdof::memory_type;
};
// -------------------
// inlined
// -------------------
template<class FieldRdof>
field_rdof_sliced_const<FieldRdof>::field_rdof_sliced_const (const FieldRdof& uh, size_type i_comp)
 : base(uh, i_comp),
   _uh(uh),
   _start(uh.begin_dof() + base::_shift(uh), base::_increment(uh))
{
}
template<class FieldRdof>
field_rdof_sliced_const<FieldRdof>::field_rdof_sliced_const (const field_wdof_sliced<FieldRdof>& uh_comp)
 : base(uh_comp),
   _uh(uh_comp._uh),
   _start(uh_comp.begin_dof())
{
}
// =========================================================================
// 5) field_wdof_sliced
// =========================================================================
template<class FieldWdof>
class field_wdof_sliced:
  public field_wdof_base<field_wdof_sliced<FieldWdof>>
 ,public field_rdof_sliced_base<FieldWdof>
{
public:

// definitions:

  using base0          = field_wdof_base<field_wdof_sliced<FieldWdof>>;
  using base           = field_rdof_sliced_base<FieldWdof>;
  using size_type      = typename FieldWdof::size_type;
  using scalar_type    = typename FieldWdof::scalar_type;
  using memory_type    = typename FieldWdof::memory_type;
  using geo_type       = typename FieldWdof::geo_type;
  using space_type     = typename FieldWdof::space_type;
  using iterator       = field_sliced_iterator<typename FieldWdof::iterator>;
  using const_iterator = field_sliced_const_iterator<typename FieldWdof::const_iterator>;
  using dis_reference  = typename FieldWdof::dis_reference;
  using self_type      = field_wdof_sliced<FieldWdof>;
  using float_type     = typename float_traits<scalar_type>::type;
  using space_constitution_type = space_constitution<float_type,memory_type>;

// allocators:

  field_wdof_sliced() = delete;
  field_wdof_sliced(const field_wdof_sliced<FieldWdof>&) = delete;
  field_wdof_sliced<FieldWdof>& operator= (const field_wdof_sliced<FieldWdof>& expr);

  template<class Sfinae
   = typename std::enable_if<
        has_field_wdof_interface<FieldWdof>::value
       ,void
      >::type
  >
  field_wdof_sliced(FieldWdof& uh, size_type i_comp);

  template <class Value>
    typename std::enable_if<
      details::is_rheolef_arithmetic<Value>::value
     ,field_wdof_sliced<FieldWdof>&
    >::type
  operator=  (const Value& value) { base0::operator= (value); return *this; }

  template <class FieldRdof>
  typename std::enable_if<
    has_field_rdof_interface<FieldRdof>::value
   ,field_wdof_sliced<FieldWdof>&
  >::type
  operator= (const FieldRdof& rdof) { base0::operator= (rdof); return *this; }

  template<class FieldLazy>
  typename std::enable_if<
         has_field_lazy_interface<FieldLazy>::value
    && ! has_field_rdof_interface<FieldLazy>::value
   ,field_wdof_sliced<FieldWdof>&
  >::type
  operator= (const FieldLazy& lazy) { base0::operator= (lazy); return *this; }

// accessors & modifiers:

        scalar_type& dof (size_type idof)       { return _start [idof]; }
  const scalar_type& dof (size_type idof) const { return _start [idof]; }
  const scalar_type& dis_dof  (size_type dis_idof) const;
  dis_reference dis_dof_entry (size_type dis_idof);
  template <class SetOp = details::generic_set_op>
  void dis_dof_update (const SetOp& set_op = SetOp()) const;

  iterator begin_dof() { return _start; }
  iterator end_dof()   { return _start + base::ndof(); }
  const_iterator begin_dof() const { return const_iterator(_start); }
  const_iterator end_dof() const   { return const_iterator(_start + base::ndof()); }

// internals:
  friend class field_rdof_sliced_const<FieldWdof>;
  template<class Iterator>
  static void _initialize (
      const space_constitution_type& sup_constit,
      size_type                      i_comp,
      space_constitution_type&       constit,
      Iterator&                      start,
      Iterator&                      last);
protected:
// data:
  FieldWdof&                     _uh;   // WARNING for cstor copy & assignt: contains a reference
  iterator                       _start;
};
// concept:
template<class FieldWdof>
struct is_field_wdof<field_wdof_sliced<FieldWdof>>: std::true_type {};

template<class FieldWdof>
struct field_traits<field_wdof_sliced<FieldWdof>> {
  using size_type    = typename FieldWdof::size_type;
  using scalar_type  = typename FieldWdof::scalar_type;
  using memory_type  = typename FieldWdof::memory_type;
};

template<class FieldWdof>
struct field_wdof2rdof_traits<field_wdof_sliced<FieldWdof>> {
  using type = field_rdof_sliced_const<FieldWdof>;
};

// -------------------
// inlined
// -------------------
template<class FieldWdof>
template<class Sfinae>
field_wdof_sliced<FieldWdof>::field_wdof_sliced (FieldWdof& uh, size_type i_comp)
 : base(uh,i_comp),
   _uh(uh),
   _start(uh.begin_dof() + base::_shift(uh), base::_increment(uh))
{
}
template<class FieldWdof>
field_wdof_sliced<FieldWdof>&
field_wdof_sliced<FieldWdof>::operator= (const field_wdof_sliced<FieldWdof>& expr)
{
  // explicit copy cstor (avoid simple copy of the proxy; see nfem/ptst/field_comp_assign_tst.cc )
  return this -> template  operator=<field_wdof_sliced<FieldWdof>> (expr);
}
template<class FieldWdof>
inline
const typename field_wdof_sliced<FieldWdof>::scalar_type&
field_wdof_sliced<FieldWdof>::dis_dof (size_type comp_dis_idof) const
{
  size_type dis_dof = _uh.get_space().get_constitution().comp_dis_idof2dis_idof (base::_i_comp, comp_dis_idof);
  return _uh.dis_dof (dis_dof);
}
template<class FieldWdof>
inline
typename field_wdof_sliced<FieldWdof>::dis_reference
field_wdof_sliced<FieldWdof>::dis_dof_entry (size_type comp_dis_idof)
{
  size_type dis_dof = _uh.get_space().get_constitution().comp_dis_idof2dis_idof (base::_i_comp, comp_dis_idof);
  return _uh.dis_dof_entry (dis_dof);
}
template<class FieldWdof>
template <class SetOp>
void
field_wdof_sliced<FieldWdof>::dis_dof_update (const SetOp& set_op) const
{
  _uh.dis_dof_update (set_op);
}

}} // namespace rheolef::details
#endif // _RHEOLEF_FIELD_WDOF_SLICED_H
