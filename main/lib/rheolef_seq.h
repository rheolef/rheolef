#ifndef _RHEOLEF_SEQ_H
#define _RHEOLEF_SEQ_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// make the sequential impementation as default: faster on small problems
//
#ifdef _RHEOLEF_H
error_rheolef_header_already_included
#endif // _RHEOLEF_H

#ifdef rheo_default_memory_model
#undef rheo_default_memory_model
#endif

#define rheo_default_memory_model sequential

#include "rheolef.h"

#endif // _RHEOLEF_SEQ_H
