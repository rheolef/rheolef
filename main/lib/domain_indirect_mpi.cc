///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/config.h"

#ifdef _RHEOLEF_HAVE_MPI
#include "rheolef/domain_indirect.h"
#include "rheolef/geo.h"

namespace rheolef {

// ----------------------------------------------------------------------------
// @brief init domain ios from the whole mesh ios data
// ----------------------------------------------------------------------------
template <class T>
void
domain_indirect_rep<distributed>::init_ios (
    const geo_abstract_rep<T,distributed>& omega)
{
  // compute i/o _ioige2ini_dis_ioige & _ioige2ini_dis_ioige numbering tables
  // --------------------------------------------
  // 1) mark used elements in omega ios numbering
  // --------------------------------------------
  const size_type unset = std::numeric_limits<size_type>::max();
  distributor ios_ownership = omega.ios_sizes().ownership_by_dimension [base::_map_dim];
  disarray<size_type,distributed> ios_mark (ios_ownership, unset);
  distributor dom_ownership = base::ownership();
  size_type first_dom_dis_ie = dom_ownership.first_index();
  size_type dom_ie = 0;
  for (const_iterator_ioige iter = base::ioige_begin(), last = base::ioige_end();
	iter != last; iter++, dom_ie++) {
    size_type ie = (*iter).index();
    size_type ios_dis_ie = omega.ige2ios_dis_ige (base::_map_dim, ie);
    size_type dom_dis_ie = first_dom_dis_ie + dom_ie;
    ios_mark.dis_entry (ios_dis_ie) = dom_dis_ie;
  }
  ios_mark.dis_entry_assembly();
  // --------------------------------------------
  // 2) count used elements in omega ios numbering
  // --------------------------------------------
  size_type dom_ini_size = 0;
  for (size_type ios_ie = 0, ios_ne = ios_mark.size(); ios_ie < ios_ne; ios_ie++) {
    if (ios_mark [ios_ie] != unset) dom_ini_size++;
  }
  // --------------------------------------------
  // 3) set _ini_ioige2dis_ioige
  // --------------------------------------------
  distributor dom_ini_ownership (distributor::decide, base::comm(), dom_ini_size);
  _ini_ioige2dis_ioige.resize (dom_ini_ownership, unset);
  size_type dom_ini_ie = 0;
  for (size_type ios_ie = 0, ios_ne = ios_mark.size(); ios_ie < ios_ne; ios_ie++) {
    if (ios_mark [ios_ie] == unset) continue;
    _ini_ioige2dis_ioige [dom_ini_ie] = ios_mark [ios_ie];
    dom_ini_ie++;
  }
  // --------------------------------------------
  // 4) set _ioige2ini_dis_ioige
  // --------------------------------------------
  _ioige2ini_dis_ioige.resize (dom_ownership, unset);
  _ini_ioige2dis_ioige.reverse_permutation (_ioige2ini_dis_ioige);
}
// ----------------------------------------------------------------------------
/// @brief domain_indirect_rep<mpi> i/o in format version 2
// ----------------------------------------------------------------------------
/**
 Implementation notes:

 The domain is implemented as a disarray of elements, e.g. edges or faces,
 but it could also be vertices or volume elements, for volumic domains.
 Says a domain of "geo_element" for simplicity. 
 The orientation is important for boundaries or interfaces
 and the direction of the normal has a sense in domains.
 It is described as a disarray of a pair: an orientation
 and an index of geo_element, denoted in brief as "oige".
 The index part of the pair, in distributed environment, is denoted
 as "dis_ige".
 An index that navigates in this disarray of pair is denoted
 as ioige, a short name for an index of (a pair of) orientation and
 index of a geo_element.

 1) Initial numbering ("ini")

 The disarray of oige pairs that constitutes the domain contains "dis_noige" entries:
 this disarray is denoted as "ini_oige".
 Its distributor is denoted as ini_ioige_ownership,
 and constructed simply from dis_noige and the communicator obtained from
 the geometry omega.
	ini_dis_ige = ini_oige[ini_ioige].index

 2) First renumbering ("ios")

 Indexes of geo_element (the dis_ige part of the pair oige), that are read from the
 input file, may be changed to a new numbering:
 the geo_elements has been renumbered after the partition of the geometry.
 The new geo_element numbering is also used for computations, see e.g. the "space" class.
 The initial numbering is refered in the geometry omega as,
 e.g. ios_dis_iedg or ios_dis_ifac, says here ios_dis_ige.
 The new side numbering is obtained by:
      	dis_ige = omega.ios_ige2dis_ige (ios_ige)
 There is a first drawback in distributed environment:
 this call is valid when ios_ige is local to the current processor,
 while only the global ios_dis_ige index is valid. The ios_ige are
 distributed according to ios_ige_ownership (e.g. ios_edge_ownership
 or ios_face_ownership), as specified in the geometry omega.
 Thus, the disarray of osig pairs may be fisrt re-distributed from
 ini_ioige_ownership to another distributor, denoted as ios_ioige_ownership,
 so that the index part of the pair matches the geo_element ios_ige_ownership
 distribution. After redistribution of the disarray of pairs, these pairs
 have been reordered, i.e. renumbered. The permutation and inverse
 permutation disarrays are denoted by:
	ini_ioige2ios_dis_ioige,	according to ini_ioige_ownership
	ios_ioige2ini_dis_ioige,	according to ios_ioige_ownership
 Also, the initial "ini_oige" disarray elements are reordered and re-distributed
 in a new disarray denoted as "ios_oige". The index of geo_element part of an
 element writes:
	ios_dis_ige = ios_oige [ios_ioige].index
 It can be converted into the
 local geo_element numbering as "ios_ige" and is then suitable for the obtention
 of the new geo_element numbering used for computations
      	dis_ige = omega.ios_ige2dis_ige (ios_ige)

 3) Second renumbering (no prefix)

 The dis_ige index obtained by this way refers to a geo_element that is not owned in
 general by the current processor.
 So, the disarray of pairs may be a second time redistributed
 accordingly to the renumbered geo_element, i.e. the "ige_ownership" distributor
 (e.g. edge_ownership or face_ownership), as specified by the geometry omega.

 A disarray of pairs, containing the orientation and the new geo_element index "dis_ige"
 is created and denoted as "tmp_oige" :
	tmp_oige[ios_ioige] = pair(orient, dis_ige)
 Notice that this disarray is based on the ios_ige_ownership distributor.
 A new distribution follows the distribution of geo_elements and is denoted as "oige":
	oige[ioige] = pair(orient,dis_ige)
 such that the locally owned part of oige contains indexes of geo_elements
 that are also locally owned by the geometry.
 The permutation and inverse permutation disarrays are denoted by:
	ios_ioige2dis_ioige,	according to ios_ioige_ownership
	ioige2ios_dis_ioige,	according to     ioige_ownership

 The "oige" disarray is stored directly in the class domain: domain is 
 implemented as a derived class of the disarray class which represents "oige".
 Thus "oige" is unnamed in the domain class.

 4) Back to "ini" renumbering

 In order to write domains into files with the initial ordering and indexes
 of geo_element, we also build the direct permutation and inverse permutation disarrays:

 for (ios_ioige=0..) {
    ini_dis_ioige = ios_ioige2ini_dis_ioige [ios_ioige]
    dis_ioige     = ios_ioige2dis_ioige     [ios_ioige]
    ini_ioige2dis_ioige.dis_entry (ini_dis_ioige) = dis_ioige
    ioige2ini_dis_ioige.dis_entry (dis_ioige      = ini_dis_ioige
 }
 ioige2ini_dis_ioige.dis_entry_assembly()
 ini_ioige2dis_ioige.dis_entry_assembly()

 5) Output domains

 for (ioige=0...)
    ini_dis_ioige = ioige2ini_dis_ioige [ioige]
    orient = oige [ioige].orient
    ige    = oige [ioige].index
    ios_dis_ige = omega.ige2ios_dis_ige (ige)
    ini_oige.dis_entry (ini_dis_ioige) = pair(orient, ios_dis_ige)
 }
 ini_oige.dis_entry_assembly
 ini_oige.put

*/
template<class U>
idiststream&
domain_indirect_rep<distributed>::get (
    idiststream&                   ips,
    const geo_rep<U,distributed>& omega)
{
  const size_type unset = std::numeric_limits<size_type>::max();
  // -----------------------
  // 1) get initial disarray
  // -----------------------
  // 1.1) get header
  communicator comm = omega.comm();
  if ( ! dis_scatch (ips, comm, "\ndomain")) {
    ips.is().setstate (std::ios::badbit);
    return ips;
  }
  size_type version, dis_noige;
  ips >> base::_name
      >> version
      >> base::_map_dim
      >> dis_noige;
  check_macro (version == 2, "unsupported version="<<version<<" domain format");

  // 1.2) get data
  distributor ini_ioige_ownership (dis_noige, comm);
  disarray<geo_element_indirect,distributed> ini_oige (ini_ioige_ownership);
  ini_oige.get_values (ips);
  // ---------------------------
  // 2) first renumbering (ios)
  // ---------------------------
  // 2.1) compute ios_owner for each oriented-side
  distributor ios_ige_ownership = omega.geo_element_ios_ownership (base::_map_dim);
  disarray<size_type>  ios_owner (ini_ioige_ownership, unset);
  for (size_type ini_ioige = 0, ini_noige = ini_oige.size();
	ini_ioige < ini_noige; ini_ioige++) {
    size_type ios_ige = ini_oige [ini_ioige].index();
    ios_owner [ini_ioige] = ios_ige_ownership.find_owner (ios_ige);
  }
  // 2.2) ios repartition
  disarray<geo_element_indirect>  ios_oige;
  disarray<size_type>  ini_ioige2ios_dis_ioige;
  disarray<size_type>  ios_ioige2ini_dis_ioige;
  ini_oige.repartition (
	ios_owner,
        ios_oige,
  	ios_ioige2ini_dis_ioige,
  	ini_ioige2ios_dis_ioige);

  // ---------------------
  // 3) first renumbering
  // ---------------------
  // 3.1) geo_element renumbering
  distributor ios_ioige_ownership = ios_oige.ownership();
  disarray<geo_element_indirect>  tmp_oige (ios_ioige_ownership);
  for (size_type ios_ioige = 0, ios_noige = ios_ioige_ownership.size();
	ios_ioige < ios_noige; ios_ioige++) {
    orientation_type orient      = ios_oige [ios_ioige].orientation();
    size_type        ios_dis_ige = ios_oige [ios_ioige].index();
    size_type        ios_ige     = ios_dis_ige - ios_ige_ownership.first_index();
    size_type        dis_ige     = omega.ios_ige2dis_ige (base::_map_dim, ios_ige);
    tmp_oige [ios_ioige].set (orient, dis_ige);
  }
  // 3.2) compute ownership for each oriented-side
  distributor ige_ownership = omega.geo_element_ownership (base::_map_dim);
  disarray<size_type>  partition (ios_ioige_ownership, unset);
  for (size_type ios_ioige = 0, ios_noige = ios_ioige_ownership.size();
	ios_ioige < ios_noige; ios_ioige++) {
    size_type ige = tmp_oige [ios_ioige].index();
    partition [ios_ioige] = ige_ownership.find_owner (ige);
  }
  // 3.3) repartition
  disarray<size_type>  ios_ioige2dis_ioige;
  disarray<size_type>  ioige2ios_dis_ioige;
  tmp_oige.repartition (
	partition,
        *this,
  	ioige2ios_dis_ioige,
  	ios_ioige2dis_ioige);
  
  // 3.4) shift from "dis_ioige" to a "ioige" numbering local to each process:
  distributor ioige_ownership = disarray<geo_element_indirect>::ownership();
  for (size_type ioige = 0, noige = ioige_ownership.size(); ioige < noige; ioige++) {
    size_type        dis_ige = operator[] (ioige).index();
    size_type        ige = dis_ige - ige_ownership.first_index();
    operator[] (ioige).set_index (ige);
  }
  // ----------------------------------------------
  // 4) Back to "ini" renumbering: set permutations
  // ----------------------------------------------
  _ini_ioige2dis_ioige.resize (ini_ioige_ownership, unset);
  _ioige2ini_dis_ioige.resize (    ioige_ownership, unset);
  for (size_type ios_ioige = 0, ios_noige = ios_ioige_ownership.size();
	ios_ioige < ios_noige; ios_ioige++) {
    size_type ini_dis_ioige = ios_ioige2ini_dis_ioige [ios_ioige];
    size_type dis_ioige     = ios_ioige2dis_ioige     [ios_ioige];
    _ini_ioige2dis_ioige.dis_entry (ini_dis_ioige) = dis_ioige;
    _ioige2ini_dis_ioige.dis_entry (dis_ioige)     = ini_dis_ioige;
 }
 _ioige2ini_dis_ioige.dis_entry_assembly();
 _ini_ioige2dis_ioige.dis_entry_assembly();

  return ips;
}
template<class U>
odiststream&
domain_indirect_rep<distributed>::put (
    odiststream&                   ops,
    const geo_rep<U,distributed>& omega) const
{
  using namespace std;
  ops << "domain" << endl
      << base::_name << endl
      << "2 " << base::_map_dim << " " << dis_size() << endl;
  distributor ioige_ownership = disarray<geo_element_indirect>::ownership();
  distributor ini_ioige_ownership = _ini_ioige2dis_ioige.ownership();
  distributor ige_ownership = omega.geo_element_ownership (base::_map_dim);
  disarray<geo_element_indirect> ini_oige (ini_ioige_ownership);
  for (size_type ioige = 0, noige = ioige_ownership.size(); ioige < noige; ioige++) {
    size_type ini_dis_ioige = _ioige2ini_dis_ioige [ioige];
    orientation_type orient = operator[] (ioige).orientation();
    size_type        ige    = operator[] (ioige).index();
    size_type   ios_dis_ige = omega.ige2ios_dis_ige (base::_map_dim, ige);
    ini_oige.dis_entry (ini_dis_ioige) = geo_element_indirect (orient, ios_dis_ige);
  }
  ini_oige.dis_entry_assembly();
  ini_oige.put_values (ops);
  return ops;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template
idiststream&
domain_indirect_rep<distributed>::get (
    idiststream&                       ips,
    const geo_rep<Float,distributed>& omega);

template
odiststream&
domain_indirect_rep<distributed>::put (
    odiststream&                       ops,
    const geo_rep<Float,distributed>& omega) const;

#define _RHEOLEF_instanciation(T,M)                                     \
template                                                                \
void									\
domain_indirect_rep<M>::init_ios (const geo_abstract_rep<T,M>&);		\

_RHEOLEF_instanciation(Float,distributed)

} // namespace rheolef
#endif // _RHEOLEF_HAVE_MPI
