#ifndef _RHEOLEF_FIELD_EXPR_VARIATIONAL_TAG_H
#define _RHEOLEF_FIELD_EXPR_VARIATIONAL_TAG_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// in a variational formulation we define things like that:
//   a(u,v) = integrate(expr...);
// where expr should be linear with respect to u and v
// => define tag: pair of compile-time booleans
//    that expresses the linearity with respect to each variable
//
// vf_tag is used to determine at compile time when a binary 
// variationnal expression as : u*u, u*v, f*v, v*v*u, etc...
// is linear which respect to u or v, where:
//	trial u;
//	test  v;
// 
// SUMLMARY:
// 1. concept
// 2. duality
// 3. unary  operators and functions
// 4. binary operators and functions
//
#include "rheolef/compiler.h"
#include "rheolef/expression.h"
namespace rheolef { namespace details {

// ----------------------------------------------------------
// 1. concept: a pair of compile-time booleans
// ----------------------------------------------------------
struct                                               vf_tag_nonlinear {};
typedef std::pair <std::false_type,std::false_type>  vf_tag_00;
typedef std::pair <std::false_type,std::true_type >  vf_tag_01;
typedef std::pair <std::true_type, std::true_type >  vf_tag_11;
typedef std::pair <std::true_type, std::false_type>  vf_tag_10;
// ------------------------------------------------
// 2. duality
// ------------------------------------------------
// vf tag: 01 <--> 10 i.e. swap test and trial
template<class T> struct dual_vf_tag             { typedef vf_tag_nonlinear type; };
template<>        struct dual_vf_tag <vf_tag_01> { typedef vf_tag_10 type; };
template<>        struct dual_vf_tag <vf_tag_10> { typedef vf_tag_01 type; };
template<>        struct dual_vf_tag <vf_tag_00> { typedef vf_tag_00 type; };
template<>        struct dual_vf_tag <vf_tag_11> { typedef vf_tag_11 type; };
// --------------------------------------------------------------------
// 3. unary operators and functions
// --------------------------------------------------------------------
//  default is nonlinear with respect to each variable
template <class F, class T> struct uf_vf_tag                   { typedef vf_tag_nonlinear type; };
template<class Tag>         struct uf_vf_tag <unary_plus, Tag> { typedef Tag type; };
template<class Tag>         struct uf_vf_tag <negate,     Tag> { typedef Tag type; };
// --------------------------------------------------------------------
// 4. binary operators and functions
// --------------------------------------------------------------------
// default is nonlinear with respect to each variable
template <class F, class T1, class T2> struct bf_vf_tag                 { typedef vf_tag_nonlinear type; };

// plus/minus
template<class Tag>                    struct bf_vf_tag <plus, Tag,Tag> { typedef Tag type; };
template<class Tag>                    struct bf_vf_tag <minus,Tag,Tag> { typedef Tag type; };

// multiplies
template<> struct bf_vf_tag <multiplies,vf_tag_00,vf_tag_00> { typedef vf_tag_00 type; };

template<> struct bf_vf_tag <multiplies,vf_tag_00,vf_tag_01> { typedef vf_tag_01 type; };
template<> struct bf_vf_tag <multiplies,vf_tag_01,vf_tag_00> { typedef vf_tag_01 type; };

template<> struct bf_vf_tag <multiplies,vf_tag_00,vf_tag_10> { typedef vf_tag_10 type; };
template<> struct bf_vf_tag <multiplies,vf_tag_10,vf_tag_00> { typedef vf_tag_10 type; };

template<> struct bf_vf_tag <multiplies,vf_tag_01,vf_tag_10> { typedef vf_tag_11 type; };
template<> struct bf_vf_tag <multiplies,vf_tag_10,vf_tag_01> { typedef vf_tag_11 type; };
template<> struct bf_vf_tag <multiplies,vf_tag_00,vf_tag_11> { typedef vf_tag_11 type; };
template<> struct bf_vf_tag <multiplies,vf_tag_11,vf_tag_00> { typedef vf_tag_11 type; };

}} // namespace rheolef::details
#endif // _RHEOLEF_FIELD_EXPR_VARIATIONAL_TAG_H
