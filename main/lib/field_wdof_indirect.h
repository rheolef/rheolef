#ifndef _RHEOLEF_FIELD_WDOF_INDIRECT_H
#define _RHEOLEF_FIELD_WDOF_INDIRECT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// proxy class for the trace on subdomains: uh["boundary"], vh["east"]

// SUMMARY:
// 1. field_indirect_const_iterator
// 2. field_indirect_iterator
// 3. field_indirect_base 
// 4. field_rdof_indirect_const 
// 5. field_wdof_indirect 

#include "rheolef/field_wdof.h"
#include "rheolef/space.h"

namespace rheolef { namespace details {
// =================================================================================
// 1. field_indirect_const_iterator
// =================================================================================
template <class SizeRandomInputIterator
         ,class ScalarForwardInputIterator>
class field_indirect_const_iterator {
public:
// definitions:

  using iterator_category = std::forward_iterator_tag;
  using size_type         = std::size_t;
  using value_type        = typename std::iterator_traits<ScalarForwardInputIterator>::value_type;
  using reference         = const value_type&;
  using pointer           = const value_type*;
  using difference_type   = std::ptrdiff_t;
  using self_type         = field_indirect_const_iterator<SizeRandomInputIterator,ScalarForwardInputIterator>;

// allocator:

  field_indirect_const_iterator (
    SizeRandomInputIterator    dis_idof_iter,
    size_type                  first_dis_idof,
    ScalarForwardInputIterator val)
  : _dis_idof_iter(dis_idof_iter),
    _first_dis_idof(first_dis_idof),
    _val(val)
  {}
 
// accessors & modifiers:

  const value_type& operator* () const { return _val [*_dis_idof_iter - _first_dis_idof]; }
  self_type&        operator++()       { ++_dis_idof_iter; return *this; }
  self_type&        operator+= (difference_type n) { _dis_idof_iter += n; return *this; }
  self_type         operator+  (difference_type n) const { self_type tmp = *this; return tmp += n; }

// comparators:

  bool operator== (const self_type& j) const { return _dis_idof_iter == j._dis_idof_iter; }
  bool operator!= (const self_type& j) const { return ! operator== (j); }
protected:
// data:
  SizeRandomInputIterator    _dis_idof_iter;
  size_type                  _first_dis_idof;
  ScalarForwardInputIterator _val;
};
// =================================================================================
// 2. field_indirect_iterator
// =================================================================================
template <class SizeRandomInputIterator
         ,class ScalarForwardOutputIterator>
class  field_indirect_iterator:
public field_indirect_const_iterator<SizeRandomInputIterator,ScalarForwardOutputIterator>
{
public:
// definitions:

  using base            = field_indirect_const_iterator<SizeRandomInputIterator,ScalarForwardOutputIterator>;
  using size_type       = typename base::size_type;
  using value_type      = typename base::value_type;
  using difference_type = std::ptrdiff_t;
  using self_type       = field_indirect_iterator<SizeRandomInputIterator,ScalarForwardOutputIterator>;

// allocator:

  field_indirect_iterator (
    SizeRandomInputIterator     dis_idof_iter,
    size_type                   first_dis_idof,
    ScalarForwardOutputIterator val)
  : base (dis_idof_iter, first_dis_idof, val)
  {}

// accessors & modifiers:

  value_type& operator* () { return base::_val [*base::_dis_idof_iter - base::_first_dis_idof]; }
  self_type&  operator++() { ++base::_dis_idof_iter; return *this; }
  self_type&  operator+= (difference_type n) { base::_dis_idof_iter += n; return *this; }
  self_type   operator+  (difference_type n) const { self_type tmp = *this; return tmp += n; }
};
// =================================================================================
// 3. field_indirect_base 
// =================================================================================
template <class T, class M>
class field_indirect_base {
public:
// definitions:

  using float_type  = typename float_traits<T>::type;
  using memory_type = M;
  using geo_type    = geo_basic  <float_type,memory_type>;
  using space_type  = space_basic<float_type,memory_type>;
  using size_type   = typename geo_type::size_type;

// allocators:

  field_indirect_base (const space_type& V, const geo_type& dom);

// accessors:

  const distributor& ownership() const { return _W.ownership(); }
  const communicator& comm() const { return ownership().comm(); }
  size_type     ndof() const { return ownership().size(); }
  size_type dis_ndof() const { return ownership().dis_size(); }
  const geo_type&   get_geo()   const { return _W.get_geo(); }
  const space_type& get_space() const { return _W; }
#ifdef TO_CLEAN
  bool have_homogeneous_space (space_type& Xh) const { Xh = get_space(); return true; }
#endif // TO_CLEAN
protected:
#ifdef TO_CLEAN
  std::string _name() const;
#endif // TO_CLEAN
// data:
  space_type                         _V;
  space_type                         _W;
  geo_type                           _dom;
  disarray<size_type,memory_type>    _dom_dis_idof2dis_idof;
  size_type                          _first_dis_idof;
};
// ----------------
// inlined
// ----------------
template <class T, class M>
field_indirect_base<T,M>::field_indirect_base (
  const space_type& V,
  const geo_type&   dom)
: _V(V),
  _W(),
  _dom(dom),
  _dom_dis_idof2dis_idof(),
  _first_dis_idof(V.ownership().first_index())
{
  if (    ! _V.get_basis().option().is_trace_n()
      || (  _V.get_geo().map_dimension() != _dom.map_dimension() + 1)) {
    _W = space_type (dom, _V.get_basis().name());
  } else {
    // restricted on a subdomain: e.g. Pkd[sides](square) => Pkd(square[interface])
    // i.e. the basis should remove the "sides" option
    size_type k = _V.get_basis().degree();
    _W = space_type (dom, "P"+std::to_string(k)+"d", _V.valued());
#ifdef TODO
    // TODO: more general, by skipping "sides" in option basis: how to do that?
    // => fatal(../../include/rheolef/smart_pointer.h,330): no_copy functor called (illegal copy)
    basis b = _V.get_basis();
    basis_option bopt = b.option();
    bopt.set_trace (false);
    b.reset (bopt);
    _W = space_type (dom, b.name()); // TODO: a cstor space(dom,basis) ?
#endif // TODO
  }
  _dom_dis_idof2dis_idof = _V.build_dom_dis_idof2bgd_dis_idof (_W, dom);
}
#ifdef TO_CLEAN
template <class T, class M>
inline
std::string
field_indirect_base<T,M>::_name() const
{
  // e.g. "P1(square[left]", an unique signature for field_expr<Expr> size-like checks
  std::string dom_name = (_dom.variant() != geo_abstract_base_rep<double>::geo_domain) ?
         _V.get_geo().name() + "[" + _dom.name() + "]" :
         _dom.name();
  return _V.get_basis().name() + "{" + dom_name + "}";
}
#endif // TO_CLEAN
// =================================================================================
// 4. field_rdof_indirect_const 
// =================================================================================
// as for:
//   gh = uh["left"]
template <class FieldRdof>
class field_rdof_indirect_const:
  public field_rdof_base<field_rdof_indirect_const <FieldRdof>>
 ,public field_indirect_base<
    typename FieldRdof::scalar_type
   ,typename FieldRdof::memory_type
  >
{
public:
// definitions:

  using scalar_type    = typename FieldRdof::scalar_type;
  using memory_type    = typename FieldRdof::memory_type;
  using base           = field_indirect_base<scalar_type,memory_type>;
  using size_type      = typename base::size_type;
  using geo_type       = typename base::geo_type;
  using space_type     = typename base::space_type;
  using const_iterator = field_indirect_const_iterator<
                           typename disarray<size_type,memory_type>::const_iterator
                          ,typename FieldRdof::const_iterator>;

// allocators:

  field_rdof_indirect_const() = delete;

  template<class Sfinae
   = typename std::enable_if<
           has_field_rdof_interface<FieldRdof>::value
       ,void
      >::type
  >
  field_rdof_indirect_const (const FieldRdof& uh, const geo_type& dom);
  field_rdof_indirect_const (const field_wdof_indirect<FieldRdof>& uh);

// new accessors:

  const scalar_type& dis_dof (size_type dis_idof) const;
  const scalar_type& dof     (size_type idof)     const
      { return _uh_const.begin_dof() [base::_dom_dis_idof2dis_idof [idof] - base::_first_dis_idof]; }

  const_iterator begin_dof() const;
  const_iterator end_dof() const;
protected:
// data:
  const FieldRdof _uh_const;
};

// concepts:
template <class FieldRdof>
struct is_field_rdof           <field_rdof_indirect_const<FieldRdof>>: std::true_type {};

template<class FieldRdof>
struct field_traits<field_rdof_indirect_const<FieldRdof>> {
  using size_type      = typename FieldRdof::size_type;
  using scalar_type    = typename FieldRdof::scalar_type;
  using memory_type    = typename FieldRdof::memory_type;
};
// ----------------
// inlined
// ----------------
template<class FieldRdof>
template<class Sfinae>
field_rdof_indirect_const<FieldRdof>::field_rdof_indirect_const (
  const FieldRdof& uh,
  const geo_type&  dom)
: base(uh.get_space(), dom),
  _uh_const(uh)
{
}
template<class FieldRdof>
field_rdof_indirect_const<FieldRdof>::field_rdof_indirect_const (
  const field_wdof_indirect<FieldRdof>& wdof) 
: base(wdof),
  _uh_const(wdof._uh)
{
}
template<class FieldRdof>
inline
typename field_rdof_indirect_const<FieldRdof>::const_iterator
field_rdof_indirect_const<FieldRdof>::begin_dof() const
{
  return const_iterator (base::_dom_dis_idof2dis_idof.begin(), base::_first_dis_idof, _uh_const.begin_dof());
}
template<class FieldRdof>
inline
typename field_rdof_indirect_const<FieldRdof>::const_iterator
field_rdof_indirect_const<FieldRdof>::end_dof() const
{
  return const_iterator (base::_dom_dis_idof2dis_idof.end(), base::_first_dis_idof, _uh_const.begin_dof());
}
template<class FieldRdof>
inline
const typename field_rdof_indirect_const<FieldRdof>::scalar_type&
field_rdof_indirect_const<FieldRdof>::dis_dof (size_type dom_dis_idof) const 
{
  size_type dis_idof = base::_dom_dis_idof2dis_idof.dis_at (dom_dis_idof);
  return _uh_const.dis_dof (dis_idof);
}
// =================================================================================
// 5. field_wdof_indirect 
// =================================================================================
// as for:
//   uh["left"] = value
//   uh["left"] = gh

template <class FieldWdof>
class  field_wdof_indirect:
  public field_wdof_base<field_wdof_indirect<FieldWdof>>
 ,public field_indirect_base<
    typename FieldWdof::scalar_type
   ,typename FieldWdof::memory_type
  >
{
public:
// definitions:

  using base0          = field_wdof_base<field_wdof_indirect<FieldWdof>>;
  using scalar_type    = typename FieldWdof::scalar_type;
  using memory_type    = typename FieldWdof::memory_type;
  using base           = field_indirect_base<scalar_type,memory_type>;
  using size_type      = typename base::size_type;
  using geo_type       = typename base::geo_type;
  using space_type     = typename base::space_type;
  using const_iterator = field_indirect_const_iterator<
                           typename disarray<size_type,memory_type>::const_iterator
                          ,typename FieldWdof::const_iterator>;
  using iterator       = field_indirect_iterator<
                           typename disarray<size_type,memory_type>::const_iterator
                          ,typename FieldWdof::iterator>;
  using dis_reference  = typename FieldWdof::dis_reference;

// allocators:

  field_wdof_indirect() = delete;
  field_wdof_indirect(const field_wdof_indirect<FieldWdof>&) = delete;
  field_wdof_indirect<FieldWdof>& operator= (const field_wdof_indirect<FieldWdof>&);

  template<class Sfinae
   = typename std::enable_if<
        has_field_wdof_interface<FieldWdof>::value
       ,void
      >::type
  >
  field_wdof_indirect (FieldWdof& uh, const geo_type& dom);

  template <class Value>
  typename std::enable_if<
    details::is_rheolef_arithmetic<Value>::value
   ,field_wdof_indirect<FieldWdof>&
  >::type
  operator=  (const Value& value)  { base0::operator= (value); return *this; }

  template <class FieldRdof>
  typename std::enable_if<
    has_field_rdof_interface<FieldRdof>::value
   ,field_wdof_indirect<FieldWdof>&
  >::type
  operator= (const FieldRdof& rdof) { base0::operator= (rdof); return *this; }

  template<class FieldLazy>
  typename std::enable_if<
        has_field_lazy_interface<FieldLazy>::value
    && ! has_field_rdof_interface<FieldLazy>::value
   ,field_wdof_indirect<FieldWdof>&
  >::type
  operator= (const FieldLazy& lazy) { base0::operator= (lazy); return *this; }

// accessors:

  const scalar_type& dof (size_type idof) const { return _uh.begin_dof() [base::_dom_dis_idof2dis_idof [idof] - base::_first_dis_idof]; }
  scalar_type&       dof (size_type idof)       { return _uh.begin_dof() [base::_dom_dis_idof2dis_idof [idof] - base::_first_dis_idof]; }
  const scalar_type& dis_dof  (size_type dis_idof) const;
  dis_reference dis_dof_entry (size_type dis_idof);
  template <class SetOp = details::generic_set_op>
  void dis_dof_update (const SetOp& set_op = SetOp()) const { _uh.dis_dof_update (set_op); }

  const_iterator begin_dof() const;
  const_iterator end_dof()   const;
  iterator begin_dof();
  iterator end_dof();

protected:
// data:
  friend field_rdof_indirect_const<FieldWdof>;
  FieldWdof&  _uh; // WARNING for cstor copy & assignt: contains a reference
};

// concepts:
template<class FieldWdof>
struct is_field_wdof     <field_wdof_indirect<FieldWdof>>: std::true_type {};

// avoid some mistakes:
template<class FieldWdof>
struct is_field_function <field_wdof_indirect<FieldWdof>>: std::false_type {};

template<class FieldWdof>
struct field_traits<field_wdof_indirect<FieldWdof>> {
  using size_type      = typename FieldWdof::size_type;
  using scalar_type    = typename FieldWdof::scalar_type;
  using memory_type    = typename FieldWdof::memory_type;
};

template<class FieldWdof>
struct field_wdof2rdof_traits<field_wdof_indirect<FieldWdof>> {
  using type = field_rdof_indirect_const<FieldWdof>;
};
// ------------------
// inlined
// ------------------
template<class FieldWdof>
template<class Sfinae>
field_wdof_indirect<FieldWdof>::field_wdof_indirect (
  FieldWdof&       uh,
  const geo_type&  dom)
: base (uh.get_space(), dom),
  _uh(uh)
{
trace_macro("fi(fw,geo)");
}
template<class FieldWdof>
field_wdof_indirect<FieldWdof>&
field_wdof_indirect<FieldWdof>::operator= (const field_wdof_indirect<FieldWdof>& expr)
{
trace_macro("fi::op=(const fi&)");
  // WARNING: the class contains a reference
  // => explicit copy (avoid simple copy of the proxy; see nfem/ptst/field_comp_assign_tst.cc )
  //    call the previous template FieldRdof version of op=
  return this -> template operator=<field_wdof_indirect<FieldWdof>> (expr);
}
template<class FieldWdof>
inline
typename field_wdof_indirect<FieldWdof>::const_iterator
field_wdof_indirect<FieldWdof>::begin_dof() const
{
  return const_iterator (base::_dom_dis_idof2dis_idof.begin(), base::_first_dis_idof, _uh.begin_dof());
}
template<class FieldWdof>
inline
typename field_wdof_indirect<FieldWdof>::const_iterator
field_wdof_indirect<FieldWdof>::end_dof() const
{
  return const_iterator (base::_dom_dis_idof2dis_idof.end(), base::_first_dis_idof, _uh.begin_dof());
}
template<class FieldWdof>
inline
typename field_wdof_indirect<FieldWdof>::iterator
field_wdof_indirect<FieldWdof>::begin_dof()
{
  return iterator (base::_dom_dis_idof2dis_idof.begin(), base::_first_dis_idof, _uh.begin_dof());
}
template<class FieldWdof>
inline
typename field_wdof_indirect<FieldWdof>::iterator
field_wdof_indirect<FieldWdof>::end_dof()
{
  return iterator (base::_dom_dis_idof2dis_idof.end(), base::_first_dis_idof, _uh.begin_dof());
}
template<class FieldWdof>
inline
const typename field_wdof_indirect<FieldWdof>::scalar_type&
field_wdof_indirect<FieldWdof>::dis_dof (size_type dom_dis_idof) const 
{
  size_type dis_idof = base::_dom_dis_idof2dis_idof.dis_at (dom_dis_idof);
  return _uh.dis_dof (dis_idof);
}
template<class FieldWdof>
inline
typename field_wdof_indirect<FieldWdof>::dis_reference
field_wdof_indirect<FieldWdof>::dis_dof_entry (size_type dom_dis_idof)
{
  size_type dis_idof = base::_dom_dis_idof2dis_idof.dis_at (dom_dis_idof);
  return _uh.dis_dof_entry (dis_idof);
}

}}// namespace rheolef::details
# endif /* _RHEOLEF_FIELD_WDOF_INDIRECT_H */
