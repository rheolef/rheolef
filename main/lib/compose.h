#ifndef _RHEOLEF_COMPOSE_H
#define _RHEOLEF_COMPOSE_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// ==========================================================================
// author: Pierre.Saramito@imag.fr
// date: 4 september 2015

namespace rheolef {
/**
@functionfile compose n-ary function application in expressions

Synopsis
========

    template <class Function, class... Expressions>
    Expression compose (const Function& f, const Expressions&... exprs);

Description
===========
Compose a n-ary function f with n fields in @ref interpolate_3
and @ref integrate_3 nonlinear expressions.

Example
=======
The `compose` operator
is used for applying a user-provided function to a field:

        Float f (Float u) { return 1/u + sqrt(u); }
        ...
        field vh = interpolate (Xh, compose(f, uh));

When two arguments are involved:

        Float g (Float u, Float v) { return v/u + sqrt(u*v); }
        ...
        field wh = interpolate (Xh, compose(g, uh, vh));

The `compose` operator supports general n-ary functions and
class-functions.

Characteristic
==============
The `compose` function supports also the method of @ref characteristic_2
used e.g. for convection-diffusion problems:
  
        characteristic X (-delta_t*uh);
        test v (Xh);
        field lh = integrate (compose(uh,X)*v);
  
Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/field_expr.h"

namespace rheolef {

// ---------------------------------------------------------------------------
// N-ary function call: (f expr1...exprN) , N >= 3 only
// ---------------------------------------------------------------------------
namespace details {

template<class NaryFunctor, class... Exprs>
class field_expr_v2_nonlinear_node_nary {
public:
// constants:

  static const size_t N = sizeof...(Exprs);
  typedef typename range_builder<0,N>::type IndexRange;

// typedefs:

  typedef geo_element::size_type                   size_type;
  using nary_functor_traits = functor_traits<typename std::decay<NaryFunctor>::type>;
  using result_type = typename nary_functor_traits::result_type;

  typedef result_type                              value_type;
  typedef typename scalar_traits<value_type>::type scalar_type;
  typedef typename  float_traits<value_type>::type float_type;
  typedef rheo_default_memory_model                memory_type;
#ifdef TODO
  // TODO: extract first type Expr1 from Exprs (HOWTO extract ?) ; 
  //       also, check that all args have the same memory model
  typedef typename Expr1::memory_type              memory_type;
#endif // TODO

// alocators:

  field_expr_v2_nonlinear_node_nary (const NaryFunctor& f, const Exprs&... exprs)
    : _f(f), _exprs(exprs...) {}

// accessors:

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<result_type>::value;

  space_constant::valued_type valued_tag() const {
    return valued_hint; // when N >= 3 : return type should be solved at compile time
#ifdef TODO
    // TODO: when N=1,2 : possible unsolved return type until run-time:
    return details::generic_binary_traits<NaryFunctor>::valued_tag(_expr1.valued_tag(), _expr2.valued_tag());
#endif // TODO
  }

// initializers:

  template<size_t ...Is>
  void _initialize_internal (
      const piola_on_pointset<float_type>&       pops,
      const integrate_option&                    iopt,
      index_list<Is...>                              ) {
    bool status_list[] = {(std::get<Is>(_exprs).initialize (pops, iopt), true)...};
  }
  template<size_t ...Is>
  void _initialize_internal (
      const space_basic<float_type,memory_type>& Xh,
      const piola_on_pointset<float_type>&       pops,
      const integrate_option&                    iopt,
      index_list<Is...>                              ) {
    bool status_list[] = {(std::get<Is>(_exprs).initialize (Xh, pops, iopt), true)...};
  }
  void initialize (
      const piola_on_pointset<float_type>&       pops,
      const integrate_option&                    iopt) {
    _initialize_internal (pops, iopt, IndexRange());
  }
  void initialize (
      const space_basic<float_type,memory_type>& Xh,
      const piola_on_pointset<float_type>&       pops,
      const integrate_option&                    iopt) {
    _initialize_internal (Xh, pops, iopt, IndexRange());
  }

// evaluators:

  template<class Result, size_t ...Is>
  void _evaluate_internal (
    const geo_basic<float_type,memory_type>&  omega_K,
    const geo_element&                        K,
    Eigen::Matrix<Result,Eigen::Dynamic,1>&   value,
    index_list<Is...>) const
  {
    using traits = functor_traits<typename std::decay<NaryFunctor>::type>;
    using vec_args_type
     = std::tuple<
         Eigen::Matrix<
           typename std::decay<
             typename traits::template arg<Is>::type
           >::type
          ,Eigen::Dynamic
          ,1
         >...
       >;
    vec_args_type value_i;
    bool status_list[] = {(std::get<Is>(_exprs).evaluate (omega_K, K, std::get<Is>(value_i)), true)...};
    size_type loc_nnod = std::get<0>(value_i).size();
    static const int narg = sizeof...(Is);
    size_type size_list[] = {std::get<Is>(value_i).size()...};
    for (size_type iarg = 1; iarg < narg; ++iarg) {
      check_macro(size_list[iarg] == loc_nnod, "invalid "<<iarg<<"-th arg-value size="<<size_list[iarg]
			<< " and 0-th arg-value size="<<loc_nnod);
    }
    value.resize (loc_nnod);
    for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
      value[loc_inod] = _f (std::get<Is>(value_i)[loc_inod]...);
    }
  }
  template<class Result>
  void evaluate (
    const geo_basic<float_type,memory_type>&  omega_K,
    const geo_element&                        K,
    Eigen::Matrix<Result,Eigen::Dynamic,1>&   value) const
  {
    _evaluate_internal (omega_K, K, value, IndexRange());
  }
  template<class Result, size_t ...Is>
  bool _valued_check_internal (Result, index_list<Is...>) const {
    bool are_equivalent = (decay_is_same<Result,result_type>::value);
    check_macro (are_equivalent,
	"compose; incompatible function " << typename_macro(NaryFunctor) 
        << " return value " << typename_macro(result_type)
	<< " and expected value " << typename_macro(Result));
    // check function argument type vs Exprs return types via recursive calls
    bool status_list[] = { std::get<Is>(_exprs).template valued_check<
    	typename nary_functor_traits::template arg<Is>::decay_type>()... };
    bool status = true;
    for (bool status_i : status_list) { status &= status_i; }
    return status;
  }
  template<class Result>
  bool valued_check() const {
    return _valued_check_internal (Result(), IndexRange());
  }
protected:
// data:
  NaryFunctor           _f;
  std::tuple<Exprs...>  _exprs;
};
template<class F, class... Exprs> struct is_field_expr_v2_nonlinear_arg     <field_expr_v2_nonlinear_node_nary<F,Exprs...> > : std::true_type {};
template<class F, class... Exprs> struct has_field_lazy_interface           <field_expr_v2_nonlinear_node_nary<F,Exprs...> > : std::true_type {};

} // namespace details
// ------------------------------------------------------
// compose(f,u1...uN)
// ------------------------------------------------------
// TODO: check that Function is a valid n-ary function or functor
// TODO: check that args are valid field_expr or field_constant
//	details::and_type<details::is_field_expr_v2_nonlinear_arg<Exprs>...>::value
// TODO: when i-th arg is field_constant, use bind to support it
// TODO: possibly undetermined return type until run-time, when N=1,2
// TODO: then, unary-compose and binary-compose can be removed
template<class Function, class... Exprs>
inline
typename std::enable_if <
  sizeof...(Exprs) >= 3,
  details::field_expr_v2_nonlinear_node_nary<
    typename details::function_traits<Function>::functor_type
   ,typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Exprs>::type...
  >
>::type
//! @brief see the @ref compose_3 page for the full documentation
compose (const Function& f, const Exprs&... exprs) {
  typedef typename details::function_traits<Function>::functor_type   fun_t;
  return details::field_expr_v2_nonlinear_node_nary
	<fun_t,    typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Exprs>::type...>
        (fun_t(f), typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Exprs>::type(exprs)...);
}

} // namespace rheolef
#endif // _RHEOLEF_COMPOSE_H
