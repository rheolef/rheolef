///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// gmsh visualization
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 may 1997  update: 23 oct 2011
//
#include "rheolef/field_expr.h"
#include "rheolef/piola_util.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/iofem.h"
#include "rheolef/interpolate.h"

using namespace std;
namespace rheolef { 

// ----------------------------------------------------------------------------
// field puts
// ----------------------------------------------------------------------------
// extern:
template <class T>
odiststream&
field_put_gmsh (odiststream& ods, const field_basic<T,sequential>& uh, std::string name);

template <class T>
odiststream&
visu_gmsh (odiststream& ops, const field_basic<T,sequential>& uh)
{
  //
  // 1) prerequises
  //
  using namespace std;
  typedef typename field_basic<T,sequential>::float_type       float_type;
  typedef typename geo_basic<float_type,sequential>::size_type size_type;
  ostream& os = ops.os();
  string valued = uh.get_space().valued();
  bool is_scalar = (valued == "scalar");
  bool verbose  = iorheo::getverbose(os);
  bool clean    = iorheo::getclean(os);
  bool execute  = iorheo::getexecute(os);
  string basename = iorheo::getbasename(os);
  string tmp = get_tmpdir() + "/";
  if (!clean) tmp = "";
  bool fill      = iorheo::getfill(os);    // isocontours or color fill
  size_type n_isovalue          = iorheo::getn_isovalue(os);
  size_type n_isovalue_negative = iorheo::getn_isovalue_negative(os);
  const geo_basic<float_type,sequential>& omega = uh.get_geo();
  size_type subdivide = iorheo::getsubdivide(os);
  if (subdivide == 0) { // subdivide is unset: use default
    subdivide = std::max(omega.order(), subdivide);
    subdivide = std::max(uh.get_space().get_basis().degree (), subdivide);
  }
  bool elevation = iorheo::getelevation(os);
  bool iso       = iorheo::getiso(os);
  T isovalue     = iorheo::getisovalue(os);
  bool color     = iorheo::getcolor(os);
  bool gray      = iorheo::getgray(os);
  bool black_and_white = iorheo::getblack_and_white(os);
  if (black_and_white) fill = false;
  string format  = iorheo::getimage_format(os);
  if (format == "tiff") format = "tif";
  if (format == "jpeg") format = "jpg";
#ifdef TODO
  string mark     = iorheo::getmark(os); 
  typedef point_basic<size_type>                      ilat;
  bool label     = iorheo::getshowlabel(os);
  bool stereo    = iorheo::getstereo(os);
  bool volume    = iorheo::getvolume(os);
  bool cut       = iorheo::getcut(os);
  bool grid      = iorheo::getgrid(os);
  if (mark != "" && !is_scalar) mark = "|"+mark +"|";
  bool velocity    = iorheo::getvelocity(os);
  bool deformation = iorheo::getdeformation(os);
  Float vscale = iorheo::getvectorscale(os);
  point_basic<float_type> origin     = iofem::getorigin(os);
  point_basic<float_type> normal     = iofem::getnormal(os);
  point_basic<size_type>  resolution = iofem::getresolution(os);
  string outfile_fmt = "";

  size_type dim     = omega.dimension();
  size_type map_dim = omega.map_dimension();
  size_type nv      = omega.sizes().ownership_by_dimension[0].size();
  size_type nedg    = omega.sizes().ownership_by_dimension[1].size();
  size_type nfac    = omega.sizes().ownership_by_dimension[2].size();
  size_type nvol    = omega.sizes().ownership_by_dimension[3].size();
  size_type ne      = omega.sizes().ownership_by_dimension[map_dim].size();

  const basis_basic<float_type>& b_fem = uh.get_space().get_basis();
  basis_on_pointset<float_type> bops (b_fem, omega.get_piola_basis());
#endif // TODO
  //
  // 2) output data
  //
  string filelist;
  string msh_name = tmp+basename + ".msh";
  filelist = filelist + " " + msh_name;
  ofstream msh_os (msh_name.c_str());
  odiststream msh (msh_os);
  if (verbose) clog << "! file \"" << msh_name << "\" created.\n";
  field_put_gmsh (msh, uh, "");
  msh.close();
  //
  // 3) create mshopt file
  //
  std::string opt_name = tmp+basename + ".mshopt";
  filelist = filelist + " " + opt_name;
  ofstream opt (opt_name.c_str());
  if (verbose) clog << "! file \"" << opt_name << "\" created.\n";
  Float z_scale = 0;
  Float umin = uh.min();
  Float umax = uh.max();
  if (elevation) {
    Float xy_delta = std::max(omega.xmax()[0]-omega.xmin()[0],
                            omega.xmax()[1]-omega.xmin()[1]);
    Float z_delta = umax-umin;
    z_scale = (fabs(z_delta)+1 == 1) ? 0 : xy_delta/(umax-umin);
  }
  opt << "View.Color.Background2D={255,255,255};" << endl
      << "General.SmallAxes = 0; // no small xyz axes" << endl
      << "General.BackgroundGradient = 0; // no color bg grad" << endl
      << "Mesh.ColorCarousel = 0; // 0: by element type, 1: by elementary entity, 2: by physical entity, 3: by partition" << endl
      << "gray = 125;" << endl
      << "Mesh.Color.Triangles = {gray,gray,gray};" << endl
      << "View.Type=1; // 1: 3D, 2: 2D space, 3: 2D time, 4: 2D" << endl
      << "View.RaiseZ=" << z_scale << "; // elevation view" << endl
      ;
  if (n_isovalue_negative != 0) {
    n_isovalue = 2*n_isovalue_negative; // since cannot control each value
    if (!fill) n_isovalue++; // add a last one
    Float z_delta = std::max(umax, -umin);
    umax =   z_delta;
    umin = - z_delta;
  }
  opt << "View.CustomMin =  " << umin << ";" << endl
      << "View.CustomMax =  " << umax << ";" << endl
      << "View.NbIso = " << n_isovalue << ";" << endl
      << "View.IntervalsType = " << (fill ? 3 : 1) << "; // 1: iso, 2: continuous, 3=discrete(bandes), 4=nums(?)" << endl
      << "View.RangeType = 2; // 1: default, 2: custom, 3: per time step" << endl
      << "View.SaturateValues = 1;" << endl
      ;
  if (black_and_white) {
  opt << "View.ColorTable = { Gray, Black };" << endl
      << "View.LineWidth=3;" << endl
      << "View.ShowScale=0;" << endl
      ;
  } else if (gray) {
  opt << "View.ColormapNumber = 9; // grayscale" << endl
      ;
  }
  if (subdivide) { 
  opt << "nsub = " << subdivide << ";" << endl
      << "View.AdaptVisualizationGrid = 1; // quand degree > 1" << endl
      << "View.MaxRecursionLevel = nsub; // quand degree > 1" << endl
      << "View.TargetError = 1e-7;" << endl
      << "Mesh.NumSubEdges = nsub; // quand order > 1" << endl
      << endl
      ;
  }
  opt.close();
  //
  // 4) create mshsav file
  //
  std::string sav_name = tmp+basename + ".mshsav";
  if (format != "") {
    filelist = filelist + " " + sav_name;
    ofstream sav (sav_name.c_str());
    if (verbose) clog << "! file \"" << sav_name << "\" created.\n";
    sav << "Print.Height =  768;" << endl
        << "Print.Width  = 1024;" << endl
	<< "Draw;" << endl
        << "Print \"" << basename << "." << format << "\";" << endl
	<< "Printf(\"! output written to " << basename << "." << format << "\") > \"/dev/stderr\";" << endl
        << "Exit; // no true batch mode, will open the GL window...\";" << endl
        ;
    sav.close();
  }
  //
  // 4) run gmsh
  //
  int status = 0;
  string command;
  if (execute) {
      command = "gmsh -option " +  opt_name + " " + msh_name;
      if (format != "") command = command + " " + sav_name;
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
  //
  // 4) clear msh data
  //
  if (clean) {
      command = "/bin/rm -f " + filelist;
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
  return ops;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template odiststream& visu_gmsh<Float>  (odiststream&, const field_basic<Float,sequential>&);

}// namespace rheolef
