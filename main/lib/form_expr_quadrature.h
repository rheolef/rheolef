#ifndef _RHEOLEF_FORM_EXPR_QUADRATURE_H
#define _RHEOLEF_FORM_EXPR_QUADRATURE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// variational expressions are integrated by using a quadrature formulae
//
// author: Pierre.Saramito@imag.fr
//
// date: 18 march 2018 
//
// SUMMARY:
// 1. concept
// 2. terminals
//    2.1. integration on one element K
//    2.2. integration on element boundary partial K
// 3. unary function 
//    3.1. unary node
//    3.2. unary calls
// 4. binary operators +-  between two integrated forms
//    4.1. binary node
//    4.2. binary calls
// 5. binary operators */ between a integrated form and a constant
//

/*
   let:
     a(u,v) = int_domain expr(u,v) dx
  
   The integrals are evaluated over each element K of the domain
   by using a quadrature formulae given by iopt
  
   expr(u,v) is a bilinear expression with respect to the
   trial and test functions u and v

   The trial function u is replaced by each of the basis function of 
   the corresponding finite element space Xh: (phi_j), j=0..dim(Xh)-1
 
   The test function v is replaced by each of the basis function of 
   the corresponding finite element space Yh: (psi_i), i=0..dim(Yh)-1
 
   The integrals over the domain omega is the sum of integrals over K.

   The integrals over K are transformed on the reference element with
   the piola transformation:
     F : hat_K ---> K
         hat_x |--> x = F(hat_x)
 
   exemples:
   1) expr(v) = u*v
    int_K phi_j(x)*psi_i(x) dx 
     = int_{hat_K} hat_phi_j(hat_x)*hat_psi_i(hat_x) det(DF(hat_x)) d hat_x
     = sum_q hat_phi_j(hat_xq)*hat_psi_i(hat_xq) det(DF(hat_xq)) hat_wq

    The value(q,i,j) = (hat_phi_j(hat_xq)*hat_psi_i(hat_xq))
    refers to basis values on the reference element.
    There are evaluated on time for all over the reference element hat_K
    and for the given quadrature formulae by:
  	expr.initialize (omega, quad);
    This expression is represented by the 'test' class (see test.h)

   3) expr(v) = dot(grad(u),grad(v)) dx
    The 'grad' node returns  
      value(q,i) = trans(inv(DF(hat_wq))*grad_phi_i(hat_xq) that is vector-valued
    The grad_phi values are obtained by a grad_value(q,i) method on the 'test' class.
    The 'dot' performs on the fly the product
      value(q,i,j) = dot (value1(q,i), value2(q,j))

   This approch generalizes for an expression tree.
*/

#include "rheolef/form_expr_variational.h"
#include "rheolef/init_expr_quadrature.h"

namespace rheolef {

// -------------------------------------------------------------------
// 1. concept
// -------------------------------------------------------------------
namespace details {

// Define a trait type for detecting form expression valid arguments
template<class T> struct is_form_expr_quadrature_arg:         std::false_type {};
template<class T> struct is_form_expr_quadrature_on_side_arg: std::false_type {};

} // namespace details
// ---------------------------------------------------------------------------
// 2. terminals
// ---------------------------------------------------------------------------
// 2.1. integration on one element K
// ---------------------------------------------------------------------------
namespace details {

template<class Expr>
class form_expr_quadrature_on_element {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename Expr::memory_type                    memory_type;
  typedef typename Expr::value_type                     result_hint;
  typedef typename Expr::value_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<float_type,memory_type>		space_type;
  typedef geo_basic  <float_type,memory_type>		geo_type;
  typedef typename Expr::vf_tag_type                    vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef form_expr_quadrature_on_element<Expr>         self_type;
  typedef form_expr_quadrature_on_element<typename Expr::dual_self_type>
                                                        dual_self_type;
  typedef typename Expr::maybe_symmetric::type          maybe_symmetric;

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  template<class Sfinae = typename std::enable_if<is_form_expr_v2_variational_arg<Expr>::value, Expr>::type>
  form_expr_quadrature_on_element (const Expr& expr);

// modifiers:

  void initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt);
  void initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt);

// accessors:

  const space_type&  get_trial_space() const { return _expr.get_trial_space(); }
  const space_type&  get_test_space()  const { return _expr.get_test_space(); }
  size_type n_derivative() const             { return _expr.n_derivative(); }

  template<class Value>
  void evaluate (
      const geo_basic<float_type,memory_type>&            omega_K,
      const geo_element&                                  K,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& ak) const;

  template<class Value>
  bool valued_check() const {
    typedef Value A1;
    if (! is_undeterminated<A1>::value) return _expr.template valued_check<A1>();
    return true;
  }
protected:
// data:
  Expr                                  _expr;
  mutable piola_on_pointset<float_type> _pops;

// working variables:
  mutable Eigen::Tensor<float_type,3>   _value_i;
};
template<class Expr> struct is_form_expr_quadrature_arg <form_expr_quadrature_on_element<Expr> > : std::true_type {};

// ---------------------------------------------------------------------------
// inlined
// ---------------------------------------------------------------------------
template<class Expr>
template<class Sfinae>
inline
form_expr_quadrature_on_element<Expr>::form_expr_quadrature_on_element (const Expr& expr)
 : _expr(expr),
   _pops(),
   _value_i()
{
}
template<class Expr>
void
form_expr_quadrature_on_element<Expr>::initialize (
  const geo_basic<float_type,memory_type>& omega_K,
  const integrate_option&                  iopt)
{
  integrate_option new_iopt   = expr_quadrature_init_iopt (omega_K, get_trial_space(), get_test_space(), n_derivative(), iopt);
  quadrature<float_type> quad = expr_quadrature_init_quad<float_type> (new_iopt);
  _pops.initialize (omega_K.get_piola_basis(),  quad, new_iopt);
  _expr.initialize (_pops, new_iopt);
}
template<class Expr>
void
form_expr_quadrature_on_element<Expr>::initialize (
  const band_basic<float_type,memory_type>& gh,
  const integrate_option&                   iopt)
{  
  integrate_option new_iopt   = expr_quadrature_init_iopt (gh.band(), get_trial_space(), get_test_space(), n_derivative(), iopt);
  quadrature<float_type> quad = expr_quadrature_init_quad<float_type> (new_iopt);
  _pops.initialize (gh.band().get_piola_basis(), quad, new_iopt);
  _expr.initialize (gh,       _pops, new_iopt);
}
template<class Expr>
template<class Value>
void
form_expr_quadrature_on_element <Expr>::evaluate (
  const geo_basic<float_type,memory_type>&            omega_K,
  const geo_element&                                  K,
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& ak) const
{
  const Eigen::Matrix<float_type,Eigen::Dynamic,1>& w = _pops.get_weight (omega_K, K);
  _expr.evaluate (omega_K, K, _value_i);

  // blas3: a_jk = sum_i phi_jk(xi)*w(xi) TODO: DVT_EIGEN_BLAS2
  size_t ni = _value_i.dimension(0);
  size_t nj = _value_i.dimension(1);
  size_t nk = _value_i.dimension(2);
  ak.resize (nj, nk);
  for (size_t j = 0; j < nj; ++j) {
  for (size_t k = 0; k < nk; ++k) {
    Value sum = 0;
    for (size_t i = 0; i < ni; ++i) {
      sum += w[i] * _value_i(i,j,k);
    }
    ak(j,k) = sum;
  }}
}

} // namespace details

// ---------------------------------------------------------------------------
// 2.2. integration on element boundary partial K
// ---------------------------------------------------------------------------
namespace details {

template<class Expr>
class form_expr_quadrature_on_sides {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename Expr::memory_type                    memory_type;
  typedef typename Expr::value_type                     result_hint;
  typedef typename Expr::value_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<float_type,memory_type>		space_type;
  typedef geo_basic  <float_type,memory_type>		geo_type;
  typedef typename Expr::vf_tag_type                    vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef form_expr_quadrature_on_element<Expr>         self_type;
  typedef form_expr_quadrature_on_element<typename Expr::dual_self_type>
                                                        dual_self_type;
  typedef typename Expr::maybe_symmetric::type          maybe_symmetric;

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  template<class Sfinae = typename std::enable_if<is_form_expr_v2_variational_arg<Expr>::value, Expr>::type>
  form_expr_quadrature_on_sides (const Expr& expr);

// accessors:

  const space_type&  get_trial_space() const { return _expr.get_trial_space(); }
  const space_type&  get_test_space()  const { return _expr.get_test_space(); }
  size_type n_derivative() const             { return _expr.n_derivative(); }

// mutable modifiers:

  void initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt);
  void initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt);

  template<class Value>
  void evaluate (
      const geo_basic<float_type,memory_type>&            omega_K,
      const geo_element&                                  K,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& ak) const;

  template<class Value>
  bool valued_check() const {
    typedef Value A1;
    if (! is_undeterminated<A1>::value) return _expr.template valued_check<A1>();
    return true;
  }
protected:
// data:
  mutable Expr                               _expr;
  mutable piola_on_pointset<float_type>      _pops;
  mutable bool                               _ignore_sys_coord;
// working variables:
  mutable Eigen::Tensor<float_type,3>        _value_i;
  mutable std::vector<size_type>             _dis_inod_S;
  mutable tensor_basic<float_type>           _DF;
};
template<class Expr> struct is_form_expr_quadrature_arg         <form_expr_quadrature_on_sides<Expr> > : std::true_type {};
template<class Expr> struct is_form_expr_quadrature_on_side_arg <form_expr_quadrature_on_sides<Expr> > : std::true_type {};

// ---------------------------------------------------------------------------
// inlined
// ---------------------------------------------------------------------------
template<class Expr>
template<class Sfinae>
inline
form_expr_quadrature_on_sides<Expr>::form_expr_quadrature_on_sides (const Expr& expr)
 : _expr(expr),
   _pops(),
   _ignore_sys_coord(false),
   _value_i(),
   _dis_inod_S(),
   _DF()
{
}
template<class Expr>
void
form_expr_quadrature_on_sides<Expr>::initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt)
{
  _ignore_sys_coord = iopt.ignore_sys_coord;
  integrate_option new_iopt   = expr_quadrature_init_iopt (omega_K, get_trial_space(), get_test_space(), _expr.n_derivative(), iopt);
  quadrature<float_type> quad = expr_quadrature_init_quad<float_type> (new_iopt);
  new_iopt._is_inside_on_local_sides = true; // propagate it recursively in the whole expression
  _pops.initialize (omega_K.get_piola_basis(), quad, new_iopt);
  _expr.initialize (_pops, new_iopt);
}
template<class Expr>
void
form_expr_quadrature_on_sides<Expr>::initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt)
{
  _ignore_sys_coord = iopt.ignore_sys_coord;
  integrate_option new_iopt   = expr_quadrature_init_iopt (gh.band(), get_trial_space(), get_test_space(), n_derivative(), iopt);
  quadrature<float_type> quad = expr_quadrature_init_quad<float_type> (new_iopt);
  new_iopt._is_inside_on_local_sides = true; // propagate it recursively in the whole expression
  _pops.initialize (gh.band().get_piola_basis(), quad, new_iopt);
  _expr.initialize (gh, _pops, new_iopt);
  fatal_macro("on_local_sides: banded-level set not yet supported, sorry");
}
template<class Expr>
template<class Value>
void
form_expr_quadrature_on_sides<Expr>::evaluate (
      const geo_basic<float_type,memory_type>&            omega_K,
      const geo_element&                                  K,
      Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
{
  size_type sid_dim = K.dimension()-1;
  size_type nyi =  get_test_space().get_constitution().assembly_loc_ndof (omega_K, K);
  size_type nxj = get_trial_space().get_constitution().assembly_loc_ndof (omega_K, K);
  for (size_type isid = 0, nsid = K.n_subgeo(sid_dim); isid < nsid; ++isid) {
    size_type dis_isid = (K.dimension() == 1) ? K[isid] : (K.dimension() == 2) ? K.edge(isid) : K.face(isid);
    const geo_element& S = omega_K.dis_get_geo_element (sid_dim, dis_isid);
    side_information_type sid;
    K.get_side_informations (S, sid);
    _expr.evaluate_on_side (omega_K, K, sid, _value_i);
    const Eigen::Matrix<float_type,Eigen::Dynamic,1>& w = _pops.get_weight (omega_K, S);
    // blas3: a_jk += sum_i phi_jk(xi)*w(xi) TODO: DVT_EIGEN_BLAS2
    size_t ni = _value_i.dimension(0);
    size_t nj = _value_i.dimension(1);
    size_t nk = _value_i.dimension(2);
    if (isid == 0) {
      value = Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>::Zero(nj, nk);
    }
    for (size_t j = 0; j < nj; ++j) {
    for (size_t k = 0; k < nk; ++k) {
      Value sum = 0;
      for (size_t i = 0; i < ni; ++i) {
        sum += w[i] * _value_i(i,j,k);
      }
      value(j,k) += sum;
    }}
  }
}

} // namespace details

template<class Expr>								
inline										
typename									
std::enable_if<
  details::is_form_expr_v2_variational_arg<Expr>::value				
 ,details::form_expr_quadrature_on_sides<Expr>										
>::type										
on_local_sides (const Expr& expr)							
{
  return details::form_expr_quadrature_on_sides<Expr>(expr);
}

// ---------------------------------------------------------------------------
// 3. unary function
// example: -(u*v), 2*(u*v), (u*v)/2
// ---------------------------------------------------------------------------
// 3.1. unary node
// ---------------------------------------------------------------------------
namespace details {

template<class UnaryFunction, class Expr>
class form_expr_quadrature_unary {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename Expr::memory_type                    memory_type;
  typedef typename details::generic_unary_traits<UnaryFunction>::template result_hint<
          typename Expr::value_type>::type              result_hint;
  typedef typename details::generic_unary_traits<UnaryFunction>::template hint<
	  typename Expr::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<float_type,memory_type>		space_type;
  typedef geo_basic  <float_type,memory_type>		geo_type;
  typedef typename Expr::vf_tag_type                    vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef form_expr_quadrature_unary<UnaryFunction,Expr>           self_type;
  typedef form_expr_quadrature_unary<UnaryFunction, typename Expr::dual_self_type>
                                                        dual_self_type;
  typedef typename Expr::maybe_symmetric::type          maybe_symmetric;

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  template<class Sfinae = typename std::enable_if<is_form_expr_quadrature_arg<Expr>::value, Expr>::type>
  form_expr_quadrature_unary (const UnaryFunction& f, const Expr& expr)
    : _f(f), _expr(expr) {}

// accessors:

  const space_type&  get_trial_space() const { return _expr.get_trial_space(); }
  const space_type&  get_test_space()  const { return _expr.get_test_space(); }
  size_type n_derivative() const             { return _expr.n_derivative(); }

// mutable modifiers:

  void initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt) {
    return _expr.initialize (omega_K, iopt); }
  void initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt) {
    return _expr.initialize (gh,  iopt); }

  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    typedef Value A1; // Value is float_type in general: elementary matrix
    _expr.evaluate (omega_K, K, value);
    for (size_type i = 0, ni = value.rows(); i < ni; ++i) {
    for (size_type j = 0, nj = value.cols(); j < nj; ++j) {
      value(i,j) = _f (value(i,j));
    }}
  }
  template<class Value>
  bool valued_check() const {
    typedef Value A1;
    if (! is_undeterminated<A1>::value) return _expr.template valued_check<A1>();
    return true;
  }
protected:
// data:
  UnaryFunction  _f;
  Expr           _expr;
};
template<class F, class Expr> struct is_form_expr_quadrature_arg    <form_expr_quadrature_unary<F,Expr> > : std::true_type {};

} // namespace details
// ---------------------------------------------------------------------------
// 3.2. unary calls
// ---------------------------------------------------------------------------

#define _RHEOLEF_make_form_expr_quadrature_unary(FUNCTION,FUNCTOR)		\
template<class Expr>								\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_quadrature_arg<Expr>::value				\
 ,details::form_expr_quadrature_unary<						\
    FUNCTOR									\
   ,Expr									\
  >										\
>::type										\
FUNCTION (const Expr& expr)							\
{										\
  return details::form_expr_quadrature_unary <FUNCTOR,Expr> (FUNCTOR(), expr); \
}

_RHEOLEF_make_form_expr_quadrature_unary (operator+, details::unary_plus)
_RHEOLEF_make_form_expr_quadrature_unary (operator-, details::negate)
#undef _RHEOLEF_make_form_expr_quadrature_unary

// ---------------------------------------------------------------------------
// 4. binary operators +-  between two integrated forms
// ---------------------------------------------------------------------------
// 4.1. binary node
// ---------------------------------------------------------------------------
// example: operator+ between two forms as in
//	    (u*v) + on_local_sides(u*v)

namespace details {

template<class BinaryFunction, class Expr1, class Expr2>
class form_expr_quadrature_binary {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename promote_memory<typename Expr1::memory_type,typename Expr2::memory_type>::type 
 				                   	memory_type;
  typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<
          typename Expr1::value_type
         ,typename Expr2::value_type>::type             result_hint;
  typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<float_type,memory_type>		space_type; // TODO: deduce from Exprs
  typedef geo_basic  <float_type,memory_type>		geo_type;
  typedef typename details::bf_vf_tag<BinaryFunction,
	typename Expr1::vf_tag_type,
	typename Expr2::vf_tag_type>::type              vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef form_expr_quadrature_binary<BinaryFunction,Expr1,Expr2>         self_type;
  typedef form_expr_quadrature_binary<BinaryFunction,typename Expr1::dual_self_type,
                                        typename Expr2::dual_self_type>
                                                        dual_self_type;
  typedef typename and_type<typename Expr1::maybe_symmetric::type,
		            typename Expr2::maybe_symmetric::type>::type
	                                               maybe_symmetric;

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:
#ifdef TODO
  template<class Sfinae 
      = typename std::enable_if<
	    is_form_expr_quadrature_arg<Expr1>::value && is_form_expr_quadrature_arg<Expr2>::value
           ,Expr1
          >::type
        >
#endif // TODO
  form_expr_quadrature_binary (const BinaryFunction& f, 
		    const Expr1&    expr1,
                    const Expr2&    expr2)
    : _f(f), _expr1(expr1), _expr2(expr2) {}

// accessors:

  const space_type&  get_trial_space() const { return _expr1.get_trial_space(); }
  const space_type&  get_test_space()  const { return _expr1.get_test_space(); }
  size_type n_derivative() const             { return std::min(_expr1.n_derivative(), _expr2.n_derivative()); }

// mutable modifiers:
  void initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt) {
    integrate_option new_iopt = expr_quadrature_init_iopt (omega_K, get_trial_space(), get_test_space(), n_derivative(), iopt);
    _expr1.initialize (omega_K, new_iopt);
    _expr2.initialize (omega_K, new_iopt);
  }
  void initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt) {
    integrate_option new_iopt = expr_quadrature_init_iopt (gh.band(), get_trial_space(), get_test_space(), n_derivative(), iopt);
    _expr1.initialize (gh,  new_iopt);
    _expr2.initialize (gh,  new_iopt);
  }
  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&            omega_K,
    const geo_element&                                  K,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    // Value is float_type in general: elementary matrix
    // for f=operator+ => sum of two elementary matrix of the same type
    // TODO: otherwise Value and 2 could be obtained from the hint<> helper
    typedef Value A1;
    typedef Value A2;
    Eigen::Matrix<A2,Eigen::Dynamic,Eigen::Dynamic> value2 (value.cols(), value.cols());
    _expr1.evaluate (omega_K, K, value);
    _expr2.evaluate (omega_K, K, value2);
    for (size_type i = 0, ni = value.rows(); i < ni; ++i) {
    for (size_type j = 0, nj = value.cols(); j < nj; ++j) {
      value(i,j) = _f (value(i,j), value2(i,j));
    }}
  }
  template<class Value>
  bool valued_check() const {
    typedef Value A1;
    typedef Value A2;
    bool status = true;
    if (! is_undeterminated<A1>::value)  status &= _expr1.template valued_check<A1>();
    if (! is_undeterminated<A2>::value)  status &= _expr2.template valued_check<A2>();
    return status;
  }
protected:
// data:
  BinaryFunction  _f;
  Expr1           _expr1;
  Expr2           _expr2;
};
template<class F, class Expr1, class Expr2> struct is_form_expr_quadrature_arg    <form_expr_quadrature_binary<F,Expr1,Expr2> > : std::true_type {};

} // namespace details

// ---------------------------------------------------------------------------
// 4.2. binary calls
// ---------------------------------------------------------------------------
// expr_quad := expr_quad +- expr_quad
// expr_quad := expr_var  +- expr_quad
// expr_quad := expr_quad +- expr_var

#define _RHEOLEF_form_expr_quadrature_binary(FUNCTION,FUNCTOR)			\
template <class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
     details::is_form_expr_quadrature_arg <Expr1>::value			\
  && details::is_form_expr_quadrature_arg <Expr2>::value			\
 ,details::form_expr_quadrature_binary<						\
    FUNCTOR									\
   ,Expr1									\
   ,Expr2									\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::form_expr_quadrature_binary					\
    <FUNCTOR,   Expr1, Expr2>							\
    (FUNCTOR(), expr1, expr2);							\
}										\
template <class Expr1, class ExprVar2>						\
inline										\
typename									\
std::enable_if<									\
     details::is_form_expr_quadrature_arg <Expr1>::value			\
  && details::is_form_expr_v2_variational_arg <ExprVar2>::value			\
 ,details::form_expr_quadrature_binary<						\
    FUNCTOR									\
   ,Expr1									\
   ,details::form_expr_quadrature_on_element<ExprVar2>				\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const ExprVar2& expr_var2)			\
{										\
  using Expr2 = details::form_expr_quadrature_on_element<ExprVar2>;		\
  return details::form_expr_quadrature_binary					\
    <FUNCTOR,   Expr1, Expr2>							\
    (FUNCTOR(), expr1, Expr2(expr_var2));					\
}										\
template <class ExprVar1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
     details::is_form_expr_v2_variational_arg <ExprVar1>::value			\
  && details::is_form_expr_quadrature_arg <Expr2>::value			\
 ,details::form_expr_quadrature_binary<						\
    FUNCTOR									\
   ,details::form_expr_quadrature_on_element<ExprVar1>				\
   ,Expr2									\
  >										\
>::type										\
FUNCTION (const ExprVar1& expr_var1, const Expr2& expr2)			\
{										\
  using Expr1 = details::form_expr_quadrature_on_element<ExprVar1>;		\
  return details::form_expr_quadrature_binary					\
    <FUNCTOR,   Expr1, Expr2>							\
    (FUNCTOR(), Expr1(expr_var1), expr2);					\
}										\

_RHEOLEF_form_expr_quadrature_binary (operator+, details::plus)
_RHEOLEF_form_expr_quadrature_binary (operator-, details::minus)

#undef _RHEOLEF_form_expr_quadrature_binary

// ---------------------------------------------------------------------------
// 5. binary operators */ between a integrated form and a constant
// ---------------------------------------------------------------------------
// expr_quad := k*expr_quad
// expr_quad := expr_quad*k
// expr_quad := expr_quad/k

namespace details {
  
template<class Expr1, class Expr2, class Sfinae = void>
struct is_form_expr_quadrature_binary_multiplies_divides_constant_left : std::false_type {};

template<class Expr1, class Expr2>
struct is_form_expr_quadrature_binary_multiplies_divides_constant_left <
  Expr1
 ,Expr2
 ,typename
  std::enable_if<
       is_rheolef_arithmetic      <Expr1>::value
    && is_form_expr_quadrature_arg<Expr2>::value
  >::type
>
: std::true_type
{};

template<class Expr1, class Expr2>
struct is_form_expr_quadrature_binary_multiplies_divides_constant_right
:      is_form_expr_quadrature_binary_multiplies_divides_constant_left <Expr2,Expr1> {};

} // namespace details

#define _RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant_left(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_quadrature_binary_multiplies_divides_constant_left <Expr1,Expr2>::value \
 ,details::form_expr_quadrature_unary<						\
    details::binder_first <FUNCTOR, Expr1> 					\
   ,Expr2 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::form_expr_quadrature_unary 					\
	<details::binder_first <FUNCTOR,Expr1>,                    Expr2> 	\
	(details::binder_first <FUNCTOR,Expr1> (FUNCTOR(), expr1), expr2); 	\
}

#define _RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant_right(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_quadrature_binary_multiplies_divides_constant_right <Expr1,Expr2>::value \
 ,details::form_expr_quadrature_unary<						\
    details::binder_second <FUNCTOR, Expr2> 					\
   ,Expr1 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::form_expr_quadrature_unary 					\
	<details::binder_second <FUNCTOR,Expr2>,                    Expr1> 	\
	(details::binder_second <FUNCTOR,Expr2> (FUNCTOR(), expr2), expr1); 	\
}

#define _RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant(FUNCTION,FUNCTOR)		\
        _RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant_left  (FUNCTION,FUNCTOR) 	\
        _RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant_right (FUNCTION,FUNCTOR)


_RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant       (operator*, details::multiplies)
_RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant_right (operator/, details::divides)

#undef _RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant_right
#undef _RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant_left
#undef _RHEOLEF_make_form_expr_quadrature_binary_operator_multiplies_divides_constant

#ifdef TODO
#endif // TODO

} // namespace rheolef
#endif // _RHEOLEF_FORM_EXPR_QUADRATURE_H
