///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// branch: vtk input
//
// author: Pierre.Saramito@imag.fr
//
// 28 janv 2020
//
#include "rheolef/branch.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"

namespace rheolef {

// extern:
template <class T>
idiststream&
geo_get_vtk (idiststream& ips, geo_basic<T,sequential>& omega);

template <class T>
void
get_header_vtk (idiststream& ips, branch_basic<T,sequential>& b)
{
  typedef typename geo_basic<T,sequential>::size_type size_type;
  if (b._header_in_done) return;
  b._header_in_done = true;
  b._parameter_name = "t"; // unknown yet
  b._n_value = std::numeric_limits<size_type>::max();
}
template <class T>
void
get_event_vtk (idiststream& ips, branch_basic<T,sequential>& b)
{
  using namespace std;
  typedef typename geo_basic<T,sequential>::size_type size_type;
  // 1. get geo
  geo_basic<T,sequential> omega;
  geo_get_vtk (ips, omega);
  check_macro (ips.good(), "bad input stream for vtk");
  istream& is = ips.is();
  string basename = iorheo::getbasename(is);
  omega.set_name(basename);
  // 2. get fields
  string mark;
  b.resize(0);
  while (is) {
    while (is >> ws >> mark) {
      if (mark == "CELL_DATA" || mark == "POINT_DATA" || mark == "FIELD") break;
    }
    if (!is.good()) break; // end of file: close this event
    size_type ndof = 0;
    string name, dummy, approx, valued;
    if (mark == "POINT_DATA" || mark == "CELL_DATA") {
      is >> ndof;
      while (is >> ws >> dummy) { if (dummy == "SCALARS") break; }
      is >> name;
      while (is >> ws >> dummy) { if (dummy == "LOOKUP_TABLE") break; }
      is >> dummy; // the LOOKUP_TABLE name
      // TODO: if mesh is Pk-order, then POINT_DATA are Pk also
      //       => manage omega.order > 1 in get_seq_get_vtk.cc and check it here
      approx = (mark == "CELL_DATA") ? "P0" : "P1";
      valued = "scalar";
      space_basic<T,sequential> Xh (omega, approx, valued);
      field_basic<T,sequential> uh (Xh);
      for (size_type idof = 0; idof < ndof && is.good(); ++idof) { is >> uh.dof (idof); }
      check_macro (is.good(), "bad input stream for vtk");
      b.push_back (std::make_pair(name, uh));
    } else if (mark == "FIELD") {
      size_type n_field;
      is >> dummy // something like "FieldData"
         >> n_field;
      for (size_type i_field = 0; i_field < n_field && is.good(); ++i_field) {
        size_type n_comp, n_comp_dof;
        is >> name;
        if (name == "METADATA") {
           // skit an optional trailing "METADATA INFORMATION 0"
           is >> dummy >> dummy
              >> name;
        } 
        is >> n_comp >> n_comp_dof
           >> dummy; // "float"
        switch (n_comp) {
         case 1: valued = "scalar"; break;
         case 3: valued = "vector"; break;
         case 9: valued = "tensor"; break;
         default: error_macro ("branch: unexpected " << n_comp << "-component valued field");
        }
        ndof = n_comp*n_comp_dof;
        // TODO: if mesh is Pk-order, then FIELD are Pk also
        //       => manage omega.order > 1 in get_seq_get_vtk.cc and check it here
        approx = "P1";
        space_basic<T,sequential> Xh (omega, approx, valued);
        field_basic<T,sequential> uh (Xh);
        for (size_type idof = 0; idof < ndof && is.good(); ++idof) { is >> uh.dof (idof); }
        check_macro (is.good(), "bad input stream for vtk");
        b.push_back (std::make_pair(name, uh));
      }
    } else {
      error_macro ("branch: unexpected `" << mark << "' in vtk input."
              << " Expect POINT_DATA, or FIELD");
    }
  }
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template void get_header_vtk<Float> (idiststream&, branch_basic<Float,sequential>&);
template void get_event_vtk<Float>  (idiststream&, branch_basic<Float,sequential>&);

}// namespace rheolef
