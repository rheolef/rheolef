#ifndef _RHEOLEF_SEQ_FIELD_VISU_GNUPLOT_INTERNAL_H
#define _RHEOLEF_SEQ_FIELD_VISU_GNUPLOT_INTERNAL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// gnuplot geo visualisation
//
// author: Pierre.Saramito@imag.fr
//
// date: 16 sept 2011
//
#include "rheolef/field.h"
#include "rheolef/fem_on_pointset.h"

namespace rheolef {

template <class T>
odiststream& visu_gnuplot (odiststream& ops, const geo_basic<T,sequential>& omega);

// ----------------------------------------------------------------------------
// bounds: for curved geometries, need to commpute it on the fly
// ----------------------------------------------------------------------------
template <class T>
struct bound_type {
// data:
  point_basic<T> xmin, xmax;
  T              umin, umax;
// cstor:
  bound_type() : xmin(), xmax(), umin(), umax() {}
// modifier:
  void update (const point_basic<T>& x, const T& u) {
    for (size_t i = 0; i < 3; i++) {
      xmin[i] = std::min(xmin[i], x[i]);
      xmax[i] = std::max(xmax[i], x[i]);
    }
    umin = std::min(umin, u);
    umax = std::max(umax, u);
  }
};
template<class T>
void
put (
  std::ostream&                    gdat,
  const geo_basic<T,sequential>&   omega,
  const geo_element&               K,
  const field_basic<T,sequential>& uh,
  const fem_on_pointset<T>&        fops,
  size_t                           my_order,
  bound_type<T>&                   bbox);

} // namespace rheolef
#endif // _RHEOLEF_SEQ_FIELD_VISU_GNUPLOT_INTERNAL_H
