#ifndef _RHEOLEF_GEO_NEAREST_H
#define _RHEOLEF_GEO_NEAREST_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// given x, search x* the closest point in the boundary of omega
// gives also K* in mesh such that x* in K*
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 march 2012
//
#include "rheolef/point.h"
#include "rheolef/disarray.h"

namespace rheolef {

// forward declarations:
template <class T, class M> class geo_base_rep;
template <class T, class M> class geo_nearest_abstract_rep;

template <class T, class M>
class geo_nearest {
public:
  typedef typename disarray<T,M>::size_type size_type;
  geo_nearest() : _ptr(0) {}
  geo_nearest(const geo_nearest<T,M>&) : _ptr(0) {}
  geo_nearest<T,M>& operator= (const geo_nearest<T,M>&) { _ptr = 0; return *this; }
  ~geo_nearest();
  static geo_nearest_abstract_rep<T,M>* make_ptr (const geo_base_rep<T,M>& omega);
  size_type seq_nearest (
	const geo_base_rep<T,M>& omega,
	const point_basic<T>&    x,
	      point_basic<T>&    x_nearest) const;
  size_type dis_nearest (
	const geo_base_rep<T,M>& omega,
	const point_basic<T>&    x,
	      point_basic<T>&    x_nearest) const;
// data:
protected:
  mutable geo_nearest_abstract_rep<T,M>* _ptr;
};

} // namespace rheolef
#endif // _RHEOLEF_GEO_NEAREST_H
