# ifndef _RHEO_NEWTON_H
# define _RHEO_NEWTON_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   14 oct 2009

namespace rheolef {
/**
@functionfile newton nonlinear solver
@addindex nonlinear problem
@addindex Newton method

Synopsis
========
@snippet newton.h verbatim_newton

Description
===========
This function implements a generic
Newton method
for the resolution of the following problem:

        F(u) = 0

A simple call to the algorithm writes:

        my_problem P;
        field uh (Xh);
        newton (P, uh, tol, max_iter);
 
The `my_problem` class should contain some methods for the evaluation of F,
i.e. the `residue` of the problem, and its derivative.
The minimal requirements are:

        class my_problem {
        public:
          typedef value_type;
          value_type residue           (const value_type& uh)  const;
          void update_derivative       (const value_type& uh)  const;
          value_type  derivative_solve (const value_type& mrh) const;
          Float dual_space_norm        (const value_type& mrh) const;
        };

The `value_type` could be a @ref field_2.
The Newton method could also be applied when `value_type` is a simple
`Float` scalar. 
Conversely, it supports multi-field extensions.

The `update_derivative` and `derivative_solver` members
are called at each step of the Newton algorithm.

The `dual_space_norm` member function
returns a scalar from the weighted residual field term
`mrh` returned by the `residue` function:
this scalar is used as the stopping criterion of the algorithm.

Example
=======
See the @ref p_laplacian_newton.cc example
and the @ref usersguide_page for more.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

namespace rheolef { 

//! @brief see the @ref newton_3 page for the full documentation
// [verbatim_newton]
template <class Problem, class Field>
int newton (const Problem& P, Field& uh, Float& tol, size_t& max_iter, odiststream *p_derr = 0)
// [verbatim_newton]
{
    if (p_derr) *p_derr << "# Newton:" << std::endl << "# n r" << std::endl << std::flush;
    for (size_t n = 0; true; n++) {
      Field rh = P.residue(uh);
      Float r = P.dual_space_norm(rh);
      if (p_derr) *p_derr << n << " " << r << std::endl << std::flush;
      if (r <= tol) { tol = r; max_iter = n; return 0; }
      if (n == max_iter) { tol = r; return 1; }
      P.update_derivative (uh);
      Field delta_uh = P.derivative_solve (-rh);
      uh += delta_uh;
    }
}
//>newton:
}// namespace rheolef
# endif // _RHEO_NEWTON_H
