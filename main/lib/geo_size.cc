///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// Manage distributed indexes for the geo class
//
// Author: Pierre Saramito
//
#include "rheolef/geo_size.h"

namespace rheolef {

// =========================================================================
// utility: convect geo_element numbering by dimension and by variant 
// =========================================================================
geo_size::size_type
geo_size::dis_ige2variant (
        size_type map_dim,
        size_type dis_ige) const
{
  const distributor& ownership = ownership_by_dimension [map_dim];
  size_type iproc         = ownership.find_owner (dis_ige);
  size_type first_dis_ige = ownership.first_index(iproc);
  size_type ige = dis_ige - first_dis_ige;
  size_type first_v = 0;
  for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                 variant < reference_element:: last_variant_by_dimension(map_dim);
	         variant++) { 
    size_type size_v = ownership_by_variant [variant].size(iproc);
    size_type last_v = first_v + size_v;
    if (ige < last_v) return variant;
    first_v = last_v;
  }
  error_macro ("dis_ie2variant: invalid dis_ige="<<dis_ige<<", ige="<<ige);
  return reference_element::max_variant; // not reached
}
// compute dis_igev from dis_ige & variant:
geo_size::size_type
geo_size::_dis_ige2dis_igev (
        size_type map_dim,
        size_type variant,
        size_type dis_ige) const
{
trace_macro("dis_ige2dis_igev (map_dim="<<map_dim<<", hat_K="<<reference_element(variant).name()<<", dis_ige="<<dis_ige<<")...");
  size_type iproc = ownership_by_dimension [map_dim].find_owner (dis_ige);
  size_type first_dis_v = 0;
  for (size_type prev_variant = reference_element::first_variant_by_dimension(map_dim);
                 prev_variant < variant;
	         prev_variant++) { 
    size_type shift = ownership_by_variant [prev_variant].last_index(iproc);
    first_dis_v += shift;
  }
  for (size_type next_variant = variant+1;
                 next_variant < reference_element::last_variant_by_dimension(map_dim);
	         next_variant++) { 
    size_type shift = ownership_by_variant [next_variant].first_index(iproc);
    first_dis_v += shift;
  }
  assert_macro (dis_ige >= first_dis_v, "invalid index");
  size_type dis_igev = dis_ige - first_dis_v;
trace_macro("dis_ige2dis_igev (map_dim="<<map_dim<<", hat_K="<<reference_element(variant).name()<<", dis_ige="<<dis_ige<<"): dis_igev="<<dis_igev<<"...");
  return dis_igev;
}
geo_size::size_type
geo_size::dis_ige2dis_igev_by_dimension (
        size_type map_dim,
        size_type dis_ige,
        size_type& variant) const
{
  variant = dis_ige2variant (map_dim, dis_ige);
  return _dis_ige2dis_igev (map_dim, variant, dis_ige);
}
geo_size::size_type
geo_size::dis_ige2dis_igev_by_dimension (
        size_type map_dim,
        size_type dis_ige) const
{
  size_type variant;
  return dis_ige2dis_igev_by_dimension (map_dim, dis_ige, variant);
}
geo_size::size_type
geo_size::dis_ige2dis_igev_by_variant (
        size_type variant,
        size_type dis_ige) const
{
  size_type map_dim = reference_element::dimension (variant);
  return _dis_ige2dis_igev (map_dim, variant, dis_ige);
}
// =========================================================================
// utility: vertex ownership follows node ownership, but dis_numbering differ
// for high order > 1 meshes. This function converts numbering.
// =========================================================================
geo_size::size_type
geo_size::dis_inod2dis_iv (size_type dis_inod) const
{
  const distributor& vertex_ownership = ownership_by_dimension[0];
  size_type iproc          =   node_ownership.find_owner(dis_inod);
  size_type first_dis_inod =   node_ownership.first_index(iproc);
  size_type first_dis_iv   = vertex_ownership.first_index(iproc);
  size_type   inod = dis_inod - first_dis_inod;
  size_type     iv = inod;
  size_type dis_iv = first_dis_iv + iv;
  return dis_iv;
}
geo_size::size_type
geo_size::dis_iv2dis_inod (size_type dis_iv) const
{
  const distributor& vertex_ownership = ownership_by_dimension[0];
  size_type iproc          = vertex_ownership.find_owner(dis_iv);
  size_type first_dis_iv   = vertex_ownership.first_index(iproc);
  size_type first_dis_inod =   node_ownership.first_index(iproc);
  size_type       iv = dis_iv - first_dis_iv;
  size_type     inod = iv;
  size_type dis_inod = first_dis_inod + inod;
  return dis_inod;
}

} // namespace
