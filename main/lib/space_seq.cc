///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/space.h"
#include "rheolef/space_mult.h"
#include "rheolef/piola_util.h"

namespace rheolef {

// ---------------------------------------------------------------
// space_rep<seq>
// ---------------------------------------------------------------
template <class T>
space_rep<T,sequential>::space_rep (
    const space_constitution<T,sequential>& constit)
  : space_base_rep<T,sequential>::space_base_rep (constit)
{
}
template <class T>
space_rep<T,sequential>::space_rep (
    const geo_basic<T,sequential>& omega,
    std::string                    approx,
    std::string                    valued)
  : space_base_rep<T,sequential>::space_base_rep (omega, approx, valued)
{
}
template <class T>
space_rep<T,sequential>::space_rep (
    const geo_basic<T,sequential>& omega,
    const basis_basic<T>&          b)
  : space_base_rep<T,sequential>::space_base_rep (omega, b)
{
}
template <class T>
space_rep<T,sequential>::space_rep (
    const space_mult_list<T,sequential>& expr)
  : space_base_rep<T,sequential>::space_base_rep (expr)
{
}
// for compatibility with the distributed case:
static std::set<size_t> empty_set;

template <class T>
const std::set<typename rheolef::space_base_rep<T, sequential>::size_type>&
space_rep<T,sequential>::ext_iu_set() const
{
  return empty_set;
}
template <class T>
const std::set<typename rheolef::space_base_rep<T, sequential>::size_type>&
space_rep<T,sequential>::ext_ib_set() const
{
  return empty_set;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class space_rep<Float,sequential>;

} // namespace rheolef
