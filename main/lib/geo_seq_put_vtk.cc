///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// vtk visualization
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 may 1997 
// update: 23 oct 2011 
// update: 23 jan 2020 : high order vtk Lagrange elements 
//
#include "rheolef/geo.h"
#include "rheolef/space_numbering.h"
#include "rheolef/piola_util.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"

#include "vtk_cell_type.h"
#include "geo_seq_put_vtk.h"

namespace rheolef { 
using namespace std;

// =========================================================================
// low order vtk meshes : for low order rheolef meshes
//                        or for old vtk/paraview version < 5.5
// =========================================================================
// ----------------------------------------------------------------------------
// one element puts
// ----------------------------------------------------------------------------
template <class T>
static
void
put_edge (ostream& vtk, const geo_element& K, const basis_basic<T>& my_numb, const geo_basic<T,sequential>& omega)
{
  typedef typename geo_basic<T,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  std::vector<size_type> inod;
  space_numbering::dis_idof (my_numb, omega.sizes(), K, inod);
  size_type my_order = my_numb.degree();
  for (size_type i = 0; i < my_order; i++) {
    size_type loc_inod0 = reference_element_e::ilat2loc_inod (my_order, ilat(i));
    size_type loc_inod1 = reference_element_e::ilat2loc_inod (my_order, ilat(i+1));
    vtk << "2\t" << inod[loc_inod0] << " " << inod[loc_inod1] << endl;
  }
}
template <class T>
static
void
put_triangle (ostream& vtk, const geo_element& K, const basis_basic<T>& my_numb, const geo_basic<T,sequential>& omega)
{
  typedef typename geo_basic<T,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  std::vector<size_type> inod;
  space_numbering::dis_idof (my_numb, omega.sizes(), K, inod);
  size_type my_order = my_numb.degree();
  for (size_type i = 0; i < my_order; i++) {
    for (size_type j = 0; i+j < my_order; j++) {
      size_type loc_inod00 = reference_element_t::ilat2loc_inod (my_order, ilat(i,   j));
      size_type loc_inod10 = reference_element_t::ilat2loc_inod (my_order, ilat(i+1, j));
      size_type loc_inod01 = reference_element_t::ilat2loc_inod (my_order, ilat(i,   j+1));
      vtk << "3\t" << inod[loc_inod00] << " "
                   << inod[loc_inod10] << " "
                   << inod[loc_inod01] << endl;
      if (i+j+1 >= my_order) continue;
      size_type loc_inod11 = reference_element_t::ilat2loc_inod (my_order, ilat(i+1, j+1));
      vtk << "3\t" << inod[loc_inod10] << " "
                   << inod[loc_inod11] << " "
                   << inod[loc_inod01] << endl;
    }
  }
}
template <class T>
static
void
put_quadrangle (ostream& vtk, const geo_element& K, const basis_basic<T>& my_numb, const geo_basic<T,sequential>& omega)
{
  typedef typename geo_basic<T,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  std::vector<size_type> inod;
  space_numbering::dis_idof (my_numb, omega.sizes(), K, inod);
  size_type my_order = my_numb.degree();
  for (size_type i = 0; i < my_order; i++) {
    for (size_type j = 0; j < my_order; j++) {
      size_type loc_inod00 = reference_element_q::ilat2loc_inod (my_order, ilat(i,   j));
      size_type loc_inod10 = reference_element_q::ilat2loc_inod (my_order, ilat(i+1, j));
      size_type loc_inod11 = reference_element_q::ilat2loc_inod (my_order, ilat(i+1, j+1));
      size_type loc_inod01 = reference_element_q::ilat2loc_inod (my_order, ilat(i,   j+1));
      vtk << "4\t" << inod[loc_inod00] << " "
                   << inod[loc_inod10] << " "
                   << inod[loc_inod11] << " "
                   << inod[loc_inod01] << endl;
    }
  }
}
template <class T>
static
void
put_tetrahedron (ostream& vtk, const geo_element& K, const basis_basic<T>& my_numb, const geo_basic<T,sequential>& omega)
{
  typedef typename geo_basic<T,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  std::vector<size_type> inod;
  space_numbering::dis_idof (my_numb, omega.sizes(), K, inod);
  size_type my_order = my_numb.degree();
  for (size_type i = 0; i < my_order; i++) {
    for (size_type j = 0; i+j < my_order; j++) {
      for (size_type k = 0; i+j+k < my_order; k++) {
        size_type loc_inod000 = reference_element_T::ilat2loc_inod (my_order, ilat(i,   j,   k));
        size_type loc_inod100 = reference_element_T::ilat2loc_inod (my_order, ilat(i+1, j,   k));
        size_type loc_inod010 = reference_element_T::ilat2loc_inod (my_order, ilat(i,   j+1, k));
        size_type loc_inod001 = reference_element_T::ilat2loc_inod (my_order, ilat(i,   j,   k+1));
        vtk << "4\t" << inod[loc_inod000] << " "
                     << inod[loc_inod100] << " "
                     << inod[loc_inod010] << " "
                     << inod[loc_inod001] << endl;
        if (i+j+k+2 > my_order) continue;
	// complete the ijk-th cube: 4 more tetras
        size_type loc_inod110 = reference_element_T::ilat2loc_inod (my_order, ilat(i+1, j+1, k));
        size_type loc_inod101 = reference_element_T::ilat2loc_inod (my_order, ilat(i+1, j,   k+1));
        size_type loc_inod011 = reference_element_T::ilat2loc_inod (my_order, ilat(i,   j+1, k+1));
        vtk << "4\t" << inod[loc_inod100] << " "  // face in x0 & x2 direction 
                     << inod[loc_inod101] << " "
                     << inod[loc_inod010] << " "
                     << inod[loc_inod001] << endl
            << "4\t" << inod[loc_inod010] << " "  // face in x1 & x2 direction
                     << inod[loc_inod011] << " "
                     << inod[loc_inod001] << " "
                     << inod[loc_inod101] << endl
            << "4\t" << inod[loc_inod100] << " "  
                     << inod[loc_inod101] << " "
                     << inod[loc_inod110] << " "
                     << inod[loc_inod010] << endl
            << "4\t" << inod[loc_inod010] << " "
                     << inod[loc_inod110] << " "
                     << inod[loc_inod011] << " "
                     << inod[loc_inod101] << endl;
	// the last 6th sub-tetra that fully fills the ijk-th cube
        if (i+j+k+3 > my_order) continue;
        size_type loc_inod111 = reference_element_T::ilat2loc_inod (my_order, ilat(i+1, j+1, k+1));
        vtk << "4\t" << inod[loc_inod111] << " "  // face in x0 & x2 direction 
                     << inod[loc_inod101] << " "
                     << inod[loc_inod011] << " "
                     << inod[loc_inod110] << endl;
      }
    }
  }
}
static
void
raw_put_prism (ostream& vtk, 
	size_t i000, size_t i100, size_t i010,
	size_t i001, size_t i101, size_t i011)
{
  // vtk prism has swaped x & y axis order: 00z 10z 01z replaced by 00z 01z 10z
  vtk << "6\t" << i000 << " "
               << i010 << " "
               << i100 << " "
               << i001 << " "
               << i011 << " "
               << i101 << endl;
}
template <class T>
static
void
put_prism (ostream& vtk, const geo_element& K, const basis_basic<T>& my_numb, const geo_basic<T,sequential>& omega, const disarray<point_basic<Float>,sequential>& my_node)
{
  typedef typename geo_basic<T,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  std::vector<size_type> inod;
  space_numbering::dis_idof (my_numb, omega.sizes(), K, inod);
  size_type my_order = my_numb.degree();
  for (size_type k = 0; k < my_order; k++) {
    for (size_type j = 0; j < my_order; j++) {
      for (size_type i = 0; i+j < my_order; i++) {
        size_type loc_inod000 = reference_element_P::ilat2loc_inod (my_order, ilat(i,   j,   k));
        size_type loc_inod100 = reference_element_P::ilat2loc_inod (my_order, ilat(i+1, j,   k));
        size_type loc_inod010 = reference_element_P::ilat2loc_inod (my_order, ilat(i,   j+1, k));
        size_type loc_inod001 = reference_element_P::ilat2loc_inod (my_order, ilat(i,   j,   k+1));
        size_type loc_inod101 = reference_element_P::ilat2loc_inod (my_order, ilat(i+1, j,   k+1));
        size_type loc_inod011 = reference_element_P::ilat2loc_inod (my_order, ilat(i,   j+1, k+1));
	raw_put_prism (vtk, 
       		inod[loc_inod000],
                inod[loc_inod100],
                inod[loc_inod010],
                inod[loc_inod001],
                inod[loc_inod101],
		inod[loc_inod011]);
        if (i+j+1 >= my_order) continue;
        size_type loc_inod110 = reference_element_P::ilat2loc_inod (my_order, ilat(i+1, j+1, k));
        size_type loc_inod111 = reference_element_P::ilat2loc_inod (my_order, ilat(i+1, j+1, k+1));
	raw_put_prism (vtk, 
       		inod[loc_inod100],
                inod[loc_inod110],
                inod[loc_inod010],
                inod[loc_inod101],
                inod[loc_inod111],
		inod[loc_inod011]);
      }
    }
  }
}
template <class T>
static
void
put_hexahedron (ostream& vtk, const geo_element& K, const basis_basic<T>& my_numb, const geo_basic<T,sequential>& omega)
{
  typedef typename geo_basic<T,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  std::vector<size_type> inod;
  space_numbering::dis_idof (my_numb, omega.sizes(), K, inod);
  size_type my_order = my_numb.degree();
  for (size_type i = 0; i < my_order; i++) {
    for (size_type j = 0; j < my_order; j++) {
      for (size_type k = 0; k < my_order; k++) {
        size_type loc_inod000 = reference_element_H::ilat2loc_inod (my_order, ilat(i,   j,   k));
        size_type loc_inod100 = reference_element_H::ilat2loc_inod (my_order, ilat(i+1, j,   k));
        size_type loc_inod110 = reference_element_H::ilat2loc_inod (my_order, ilat(i+1, j+1, k));
        size_type loc_inod010 = reference_element_H::ilat2loc_inod (my_order, ilat(i,   j+1, k));
        size_type loc_inod001 = reference_element_H::ilat2loc_inod (my_order, ilat(i,   j,   k+1));
        size_type loc_inod101 = reference_element_H::ilat2loc_inod (my_order, ilat(i+1, j,   k+1));
        size_type loc_inod011 = reference_element_H::ilat2loc_inod (my_order, ilat(i,   j+1, k+1));
        size_type loc_inod111 = reference_element_H::ilat2loc_inod (my_order, ilat(i+1, j+1, k+1));
        vtk << "8\t" << inod[loc_inod000] << " "
                     << inod[loc_inod100] << " "
                     << inod[loc_inod110] << " "
                     << inod[loc_inod010] << " "
                     << inod[loc_inod001] << " "
                     << inod[loc_inod101] << " "
                     << inod[loc_inod111] << " "
                     << inod[loc_inod011] << endl;
      }
    }
  }
}
template <class T>
static
void
put (ostream& vtk, const geo_element& K, const basis_basic<T>& my_numb, const geo_basic<T,sequential>& omega, const disarray<point_basic<Float>,sequential>& my_node)
{
  switch (K.variant()) {
   case reference_element::p: vtk << "1\t" << K[0] << endl; break;
   case reference_element::e: put_edge        (vtk, K, my_numb, omega); break;
   case reference_element::t: put_triangle    (vtk, K, my_numb, omega); break;
   case reference_element::q: put_quadrangle  (vtk, K, my_numb, omega); break;
   case reference_element::T: put_tetrahedron (vtk, K, my_numb, omega); break;
   case reference_element::P: put_prism       (vtk, K, my_numb, omega, my_node); break;
   case reference_element::H: put_hexahedron  (vtk, K, my_numb, omega); break;
   default: error_macro ("unsupported element variant `" << K.name() <<"'");
  }
}
// ----------------------------------------------------------------------------
// geo puts
// ----------------------------------------------------------------------------
template <class T>
odiststream&
geo_put_vtk_old (
  odiststream&                               ops,
  const geo_basic<T,sequential>&             omega,
  const basis_basic<T>&                      my_numb,
  const disarray<point_basic<T>,sequential>& my_node,
  bool                                       append_data)
{
  trace_macro("geo_put_vtk_old: my_numb="<<my_numb.name());
  //
  // 0) pre-requises
  //
  typedef typename geo_basic<T,sequential>::size_type size_type;
  size_type my_order = my_numb.degree();
  ostream& vtk = ops.os();
  check_macro (my_order >= omega.order(), "order="<<omega.order()<<" > field degree="<<my_order);
  //
  // 1) put header
  //
  vtk << setprecision(numeric_limits<T>::digits10)
      << "# vtk DataFile Version 1.0" << endl
      << "Unstructured Grid" << endl
      << "ASCII" << endl
      << "DATASET UNSTRUCTURED_GRID" << endl;
  //
  // 2) put nodes
  //
  vtk << "POINTS " << my_node.size() << " float" << endl;
  for (size_type inod = 0, nnod = my_node.size(); inod < nnod; inod++) {
     vtk << my_node[inod] << endl;
  }
  //
  // 3) count cell data
  //
  size_type map_dim  = omega.map_dimension();
  // count pass, since omega.sizes is not yet valid when omega is a geo_domain_indirect...
  std::array<size_type,reference_element::max_variant> size_by_variant;
  size_by_variant.fill (0);
  for (size_type ie = 0, ne = omega.size(); ie < ne; ie++) {
    const geo_element& K = omega.get_geo_element (map_dim, ie);
    size_by_variant [K.variant()]++;
  }
  size_type ncell = 0;
  size_type ndata = 0;
  std::array<size_type,reference_element::max_variant> loc_ncell;
  std::array<size_type,reference_element::max_variant> loc_ndata;
  for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                 variant < reference_element:: last_variant_by_dimension(map_dim); variant++) {
    size_type d = reference_element::dimension(variant);
    size_type n = reference_element::n_vertex (variant);
    loc_ncell [variant] = pow(my_order,d);
    loc_ndata [variant] = (n+1)*loc_ncell [variant];
    ncell += loc_ncell[variant]*size_by_variant [variant];
    ndata += loc_ndata[variant]*size_by_variant [variant];
  }
  //
  // 4) put cells
  //
  string opt_d = my_numb.is_discontinuous() ? "d" : "";
  string cell_numb_name = "P"+std::to_string(my_numb.degree())+opt_d;
  basis_basic<T> cell_numb (cell_numb_name); // my_numb could be vector-valued
  vtk << "CELLS " << ncell << " " << ndata << endl;
  for (size_type ie = 0, ne = omega.size(); ie < ne; ie++) {
    const geo_element& K = omega.get_geo_element (map_dim, ie);
    put (vtk, K, cell_numb, omega, my_node);
  }
  //
  // 4) put cell types
  //
  std::array<size_type,reference_element::max_variant> cell_type;
  cell_type [reference_element::p] = VTK_VERTEX;
  cell_type [reference_element::e] = VTK_LINE;
  cell_type [reference_element::t] = VTK_TRIANGLE;
  cell_type [reference_element::q] = VTK_QUAD;
  cell_type [reference_element::T] = VTK_TETRA;
  cell_type [reference_element::P] = VTK_WEDGE;
  cell_type [reference_element::H] = VTK_HEXAHEDRON;
  vtk << "CELL_TYPES " << ncell << endl;
  for (size_type ie = 0, ne = omega.size(); ie < ne; ie++) {
    const geo_element& K = omega.get_geo_element (map_dim, ie);
    for (size_type k = 0; k < loc_ncell[K.variant()]; k++) {
      vtk << cell_type [K.variant()] << endl;
    }
  }
  // 5) output some values for vtkDataSet to be happy...
  if (! append_data) return ops;
  std::string data_name = "mesh";
  vtk << "POINT_DATA " << my_node.size() << endl
      << "SCALARS " << data_name << " float" << endl
      << "LOOKUP_TABLE default"  << endl;
  for (size_type inod = 0, nnod = my_node.size(); inod < nnod; inod++) {
    vtk << "0" << endl;
  }
  vtk << endl;

  return ops;
}
// =========================================================================
// high order vtk meshes : for high order rheolef meshes
//                         and for recent vtk/paraview version >= 5.5
// =========================================================================
// there are very few documentation about the local numbering of nodes
// for the vtk high order Lagrange elements : see e.g.
// https://blog.kitware.com/modeling-arbitrary-order-lagrange-finite-elements-in-the-visualization-toolkit
//
// apparent my_order can differ from the official omega.order one:
// useful for P1 geo with P3 field: draw on the P3 lattice
template <class T>
static
void
put_high_nicely_ordered (
  ostream&                       vtk,
  const basis_basic<T>&          my_numb,
  const geo_basic<T,sequential>& omega,
  const geo_element&             K)
{
  typedef typename geo_element::size_type size_type;
  std::vector<size_type> inod;
  space_numbering::dis_idof (my_numb, omega.sizes(), K, inod);
  vtk << inod.size();
  for (size_type loc_inod = 0, loc_nnod = inod.size(); loc_inod < loc_nnod; loc_inod++) {
    vtk << " " << inod[loc_inod];
  }
  vtk << endl;
}
template <class T>
static
void
put_high (
  ostream&                       vtk,
  const basis_basic<T>&          my_numb,
  const geo_basic<T,sequential>& omega,
  const geo_element&             K)
{
  switch (K.variant()) {
   case reference_element::p: vtk << "1\t" << K[0] << endl; break;
   case reference_element::e:
   case reference_element::t: 
   case reference_element::q:
   case reference_element::T:
   case reference_element::P:
   case reference_element::H: put_high_nicely_ordered (vtk, my_numb, omega, K); break;
   default: error_macro ("unsupported element variant `" << K.name() <<"'");
  }
}
template <class T>
odiststream&
geo_put_vtk_high (
  odiststream&                               ops,
  const geo_basic<T,sequential>&             omega,
  const basis_basic<T>&                      my_numb,
  const disarray<point_basic<T>,sequential>& my_node,
  bool                                       append_data = true,
  size_t                                     subgeo_dim  = std::numeric_limits<size_t>::max())
{
  trace_macro("geo_put_vtk_high: my_numb="<<my_numb.name());
  //
  // 0) pre-requises
  //
  typedef typename geo_basic<T,sequential>::size_type size_type;
  size_type my_order = my_numb.degree();
  if (subgeo_dim == std::numeric_limits<size_type>::max()) {
      subgeo_dim = omega.map_dimension();
  }
  ostream& vtk = ops.os();
  //
  // 1) put header
  //
  vtk << setprecision(numeric_limits<T>::digits10)
      << "# vtk DataFile Version 1.0" << endl
      << "Unstructured Grid" << endl
      << "ASCII" << endl
      << "DATASET UNSTRUCTURED_GRID" << endl;
  //
  // 2) put nodes
  //
  vtk << "POINTS " << my_node.size() << " float" << endl;
  for (size_type inod = 0, nnod = my_node.size(); inod < nnod; inod++) {
     vtk << my_node[inod] << endl;
  }
  //
  // 3) count cell data
  //
  // count pass, since omega.sizes is not yet valid when omega is a geo_domain_indirect...
  std::array<size_type,reference_element::max_variant> size_by_variant;
  size_by_variant.fill (0);
  for (size_type ie = 0, ne = omega.size(subgeo_dim); ie < ne; ie++) {
    const geo_element& K = omega.get_geo_element (subgeo_dim, ie);
    size_by_variant [K.variant()]++;
  }
  size_type ncell = 0;
  size_type ndata = 0;
  std::array<size_type,reference_element::max_variant> loc_ndata;
  for (size_type variant = reference_element::first_variant_by_dimension(subgeo_dim);
                 variant < reference_element:: last_variant_by_dimension(subgeo_dim); variant++) {
    size_type d = reference_element::dimension(variant);
    size_type n = reference_element::n_node (variant, my_order);
    loc_ndata [variant] = n+1;
    ncell +=                    size_by_variant [variant];
    ndata += loc_ndata[variant]*size_by_variant [variant];
  }
  //
  // 4) put cells
  //
  string opt_d = my_numb.is_discontinuous() ? "d" : "";
  string cell_numb_name = "P"+std::to_string(my_numb.degree())+opt_d;
  basis_basic<T> cell_numb (cell_numb_name); // my_numb could be vector-valued
  vtk << "CELLS " << ncell << " " << ndata << endl;
  for (size_type ie = 0, ne = omega.size(subgeo_dim); ie < ne; ie++) {
    const geo_element& K = omega.get_geo_element (subgeo_dim, ie);
    put_high (vtk, cell_numb, omega, K);
  }
  //
  // 4) put cell types
  //    TODO: move switch to vtk_cell_type.cc as
  //          cell_type = variant2vtk_cell_type (variant, order);
  //
  std::array<size_type,reference_element::max_variant> cell_type_one;
  cell_type_one  [reference_element::p] = VTK_VERTEX;
  cell_type_one  [reference_element::e] = VTK_LINE;
  cell_type_one  [reference_element::t] = VTK_TRIANGLE;
  cell_type_one  [reference_element::q] = VTK_QUAD;
  cell_type_one  [reference_element::T] = VTK_TETRA;
  cell_type_one  [reference_element::P] = VTK_WEDGE;
  cell_type_one  [reference_element::H] = VTK_HEXAHEDRON;
  std::array<size_type,reference_element::max_variant> cell_type_two;
  cell_type_two  [reference_element::p] = VTK_VERTEX;
  cell_type_two  [reference_element::e] = VTK_QUADRATIC_EDGE;
  cell_type_two  [reference_element::t] = VTK_QUADRATIC_TRIANGLE;
  cell_type_two  [reference_element::q] = VTK_BIQUADRATIC_QUAD;
  cell_type_two  [reference_element::T] = VTK_QUADRATIC_TETRA;
  cell_type_two  [reference_element::P] = VTK_BIQUADRATIC_QUADRATIC_WEDGE;
  cell_type_two  [reference_element::H] = VTK_TRIQUADRATIC_HEXAHEDRON;
  std::array<size_type,reference_element::max_variant> cell_type_high;
  cell_type_high [reference_element::p] = VTK_VERTEX;
  cell_type_high [reference_element::e] = VTK_LAGRANGE_CURVE;
  cell_type_high [reference_element::t] = VTK_LAGRANGE_TRIANGLE;
  cell_type_high [reference_element::q] = VTK_LAGRANGE_QUADRILATERAL;
  cell_type_high [reference_element::T] = VTK_LAGRANGE_TETRAHEDRON;
  cell_type_high [reference_element::P] = VTK_LAGRANGE_WEDGE;
  cell_type_high [reference_element::H] = VTK_LAGRANGE_HEXAHEDRON;
  vtk << "CELL_TYPES " << ncell << endl;
  for (size_type ie = 0, ne = omega.size(subgeo_dim); ie < ne; ie++) {
    const geo_element& K = omega.get_geo_element (subgeo_dim, ie);
    switch (K.variant()) {
      case reference_element::e: {
        switch (my_order) {
          case 1:  vtk << VTK_LINE << endl; break;
          case 2:  vtk << VTK_QUADRATIC_EDGE << endl; break;
          case 3:  vtk << VTK_CUBIC_LINE << endl; break;
          // BUG_PARAVIEW_HIGH: https://discourse.paraview.org/t/possible-bug-with-1d-high-order-lagrange-element/3396/2
          default: vtk << VTK_LAGRANGE_CURVE << endl; break;
        }
        break;
      }
      default: {
        switch (my_order) {
          case 1:  vtk << cell_type_one  [K.variant()] << endl; break;
          case 2:  vtk << cell_type_two  [K.variant()] << endl; break;
          default: vtk << cell_type_high [K.variant()] << endl; break;
        }
        break;
      }
    }
  }
  // 5) output some values for vtkDataSet to be happy...
  if (! append_data) return ops;
  std::string data_name = "mesh";
  vtk << "POINT_DATA " << my_node.size() << endl
      << "SCALARS " << data_name << " float" << endl
      << "LOOKUP_TABLE default"  << endl;
  for (size_type inod = 0, nnod = my_node.size(); inod < nnod; inod++) {
    vtk << "0" << endl;
  }
  vtk << endl;

  return ops;
}
// =========================================================================
// main call
// =========================================================================
template <class T>
odiststream&
geo_put_vtk (
  odiststream&                               ops,
  const geo_basic<T,sequential>&             omega,
  const basis_basic<T>&                      my_numb,
  const disarray<point_basic<T>,sequential>& my_node,
  bool                                       append_data,
  size_t                                     subgeo_dim)
{
#if (_RHEOLEF_PARAVIEW_VERSION_MAJOR >= 5) && (_RHEOLEF_PARAVIEW_VERSION_MINOR >= 5)
  // paraview version >= 5.5 has high order elements
  return geo_put_vtk_high (ops, omega, my_numb, my_node, append_data, subgeo_dim);
#else
  return geo_put_vtk_old  (ops, omega, my_numb,  my_node, append_data);
#endif
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                             	\
template odiststream& geo_put_vtk (				\
  odiststream&                               ops,		\
  const geo_basic<T,sequential>&             omega,		\
  const basis_basic<T>&                      my_numb,		\
  const disarray<point_basic<T>,sequential>& my_node,		\
  bool                                       append_data,	\
  size_t                                     subgeo_dim);

_RHEOLEF_instanciation(Float)

}// namespace rheolef
