#ifndef _RHEOLEF_FEM_ON_POINTSET_H
#define _RHEOLEF_FEM_ON_POINTSET_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// evaluate a FEM basis on a mapped element K
// on a full pointset, e.g. a quadrature node set 
// or an interpolation node set
// where the FEM on K is defined via the Piola transformation:
//
//	F : hat_K ---> K
//          hat_x +--> x = F(hat_x)
// 
// and the corresponding FEM on the reference element
//
#include "rheolef/geo.h"
#include "rheolef/basis_on_pointset.h"
#include "rheolef/piola_on_pointset.h"
namespace rheolef {

// ----------------------------------------------------------------------------
// representation
// ----------------------------------------------------------------------------
template<class T>
class fem_on_pointset_rep {
public: 
  typedef reference_element::size_type          size_type;
  typedef details::differentiate_option::type   diff_type;

// allocators:

  fem_on_pointset_rep();

// accessors:

  const basis_on_pointset<T>& get_basis_on_pointset() const { return _bops; }
  const piola_on_pointset<T>& get_piola_on_pointset() const { return _pops; }
  
// modifiers:

  void initialize (
    const basis_basic<T>&        fem_basis,
    const piola_on_pointset<T>&  pops);

  template<class M, class Value, diff_type Diff>
  void
  evaluate (
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;

  template<class M, class Value, diff_type Diff>
  void
  evaluate_on_side (
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;

// internals:
  template<class M, class Value>
  void _evaluate_post_piola (
    const geo_basic<T,M>&                                     omega_K,
    const geo_element&                                        K,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& hat_phij_xi,
          Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;

//protected:
  basis_on_pointset<T>   _bops;
  piola_on_pointset<T>   _pops;

// working area:
public:
  mutable std::array<
              Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>
             ,reference_element::max_variant>           	_scalar_phij_xi;
  mutable std::array<
  	      Eigen::Matrix<point_basic<T>,Eigen::Dynamic,Eigen::Dynamic>
             ,reference_element::max_variant>           	_vector_phij_xi;
  mutable std::array<
  	      Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,Eigen::Dynamic>
             ,reference_element::max_variant>           	_tensor_phij_xi;
};
template<class T>
fem_on_pointset_rep<T>::fem_on_pointset_rep()
  : _bops(),
    _pops(),
    _scalar_phij_xi(),
    _vector_phij_xi(),
    _tensor_phij_xi()
{
}
// ----------------------------------------------------------------------------
// interface
// ----------------------------------------------------------------------------
template<class T>
class fem_on_pointset: public smart_pointer<fem_on_pointset_rep<T> > {
public:
  typedef fem_on_pointset_rep<T> rep;
  typedef smart_pointer<rep>       base;
  typedef typename rep::size_type  size_type;
  typedef typename rep::diff_type  diff_type;

// allocators:

  fem_on_pointset();

// modifiers:

  void initialize (
    const basis_basic<T>&        fem_basis,
    const piola_on_pointset<T>&  pops)
      { base::data().initialize (fem_basis, pops); }

// accessors:

  const basis_on_pointset<T>& get_basis_on_pointset() const
        { return base::data().get_basis_on_pointset(); }
  const piola_on_pointset<T>& get_piola_on_pointset() const
        { return base::data().get_piola_on_pointset(); }
  
  template<class M, class Value, diff_type Diff>
  void
  evaluate (
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
      { base::data().template evaluate<M,Value,Diff> (omega_K, K, gopt, value); }

  template<class M, class Value, diff_type Diff>
  void
  evaluate_on_side (
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
      { base::data().template evaluate_on_side<M,Value,Diff> (omega_K, K, sid, gopt, value); }
};
template<class T>
inline
fem_on_pointset<T>::fem_on_pointset()
 : base(new_macro(rep))
{
}

}// namespace rheolef
#endif // _RHEOLEF_FEM_ON_POINTSET_H
