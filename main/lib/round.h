#ifndef _RHEOLEF_ROUND_H
#define _RHEOLEF_ROUND_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/rounder.h"
#include "rheolef/field_expr.h"

namespace rheolef {

template<class Expr, class T2>
typename
std::enable_if<
  details::is_field_expr_affine_homogeneous<Expr>::value
 ,field_basic<
    typename Expr::scalar_type
   ,typename Expr::memory_type
  >
>::type
round (
 const Expr& expr,
 const T2&   prec)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  space_basic<T,M> Xh;
  check_macro (expr.have_homogeneous_space (Xh),
    "round(field_expr, prec): field_expr should have homogeneous space. HINT: use round(interpolate(Xh, field_expr), prec)");
  field_basic<T,M> uh = expr; // create a tmp because expr has no expr.end_dof()
  field_basic<T,M> vh (Xh);
  std::transform (uh.begin_dof(), uh.end_dof(), vh.begin_dof(), rounder(prec));
  return vh;
}

}//namespace rheolef
#endif // _RHEOLEF_ROUND_H
