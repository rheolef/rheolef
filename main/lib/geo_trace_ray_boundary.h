#ifndef _RHEOLEF_GEO_TRACE_RAY_BOUNDARY_H
#define _RHEOLEF_GEO_TRACE_RAY_BOUNDARY_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// given x and v, search S on boudary mesh such that ray(x,v) hits S
//  and returns the closest hit to x
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 march 2012
//
#include "rheolef/point.h"
#include "rheolef/disarray.h"

namespace rheolef {

// forward declarations:
template <class T, class M> class geo_base_rep;
template <class T, class M> class geo_trace_ray_boundary_abstract_rep;

template <class T, class M>
class geo_trace_ray_boundary {
public:
  typedef typename disarray<T,M>::size_type size_type;
  geo_trace_ray_boundary() : _ptr(0) {}
  geo_trace_ray_boundary(const geo_trace_ray_boundary<T,M>&) : _ptr(0) {}
  geo_trace_ray_boundary<T,M>& operator= (const geo_trace_ray_boundary<T,M>&) { _ptr = 0; return *this; }
  ~geo_trace_ray_boundary();
  static geo_trace_ray_boundary_abstract_rep<T,M>* make_ptr (const geo_base_rep<T,M>& omega);
  bool seq_trace_ray_boundary (
                const geo_base_rep<T,M>&  omega,
                const point_basic<T>&     x,
                const point_basic<T>&     v,
                      point_basic<T>&     y) const;
  bool dis_trace_ray_boundary (
                const geo_base_rep<T,M>&  omega,
                const point_basic<T>&     x,
                const point_basic<T>&     v,
                      point_basic<T>&     y) const;
// data:
protected:
  mutable geo_trace_ray_boundary_abstract_rep<T,M>* _ptr;
};

} // namespace rheolef
#endif // _RHEOLEF_TRACE_RAY_BOUNDARY_H
