///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/config.h"

#ifdef _RHEOLEF_HAVE_MPI

#include "rheolef/space.h"
#include "rheolef/space_mult.h"

namespace rheolef {

template <class T>
space_rep<T,distributed>::space_rep (
    const space_constitution<T,distributed>& constit)
  : space_base_rep<T,distributed>::space_base_rep (constit),
    _idof2ios_dis_idof(),
    _ios_idof2dis_idof(),
    _ext_iu_set(),
    _ext_ib_set()
{
  base::get_constitution().set_ios_permutations (_idof2ios_dis_idof, _ios_idof2dis_idof);
}
template <class T>
space_rep<T,distributed>::space_rep (
    const geo_basic<T,distributed>& omega_in,
    std::string                     approx,
    std::string                     valued)
  : space_base_rep<T,distributed>::space_base_rep (omega_in, approx, valued),
    _idof2ios_dis_idof(),
    _ios_idof2dis_idof(),
    _ext_iu_set(),
    _ext_ib_set()
{
  if (approx == "") return; // empty space cstor
  base::get_constitution().set_ios_permutations (_idof2ios_dis_idof, _ios_idof2dis_idof);
}
template <class T>
space_rep<T,distributed>::space_rep (
    const geo_basic<T,distributed>& omega_in,
    const basis_basic<T>&           b)
  : space_base_rep<T,distributed>::space_base_rep (omega_in, b),
    _idof2ios_dis_idof(),
    _ios_idof2dis_idof(),
    _ext_iu_set(),
    _ext_ib_set()
{
  if (! b.is_initialized()) return; // empty space cstor
  base::get_constitution().set_ios_permutations (_idof2ios_dis_idof, _ios_idof2dis_idof);
}
template <class T>
space_rep<T,distributed>::space_rep (const space_mult_list<T,distributed>& expr)
  : space_base_rep<T,distributed>::space_base_rep (expr),
    _idof2ios_dis_idof(),
    _ios_idof2dis_idof(),
    _ext_iu_set(),
    _ext_ib_set()
{
  base::get_constitution().set_ios_permutations (_idof2ios_dis_idof, _ios_idof2dis_idof);
}
template <class T>
void
space_rep<T,distributed>::freeze_body () const
{
  base::base_freeze_body();
  // -----------------------------------------------------------------------
  // 1) symbolic assembly: 
  // loop on elements & identify some dofs, that are referenced
  // by locally-managed geo_elements, but these dofs are managed
  // by another processor: e.g. dofs associated to vertices on 
  // a partition boundary.
  // -----------------------------------------------------------------------
  // set local numbering with global indexes
  size_type first_dis_iu = base::_iu_ownership.first_index();
  size_type first_dis_ib = base::_ib_ownership.first_index();
  for (size_type idof = 0, ndof = base::_idof2blk_dis_iub.size(); idof < ndof; idof++) {
    size_type first_dis_iub = base::_idof2blk_dis_iub [idof].is_blocked() ? first_dis_ib : first_dis_iu;
    base::_idof2blk_dis_iub [idof].set_dis_iub (base::_idof2blk_dis_iub[idof].dis_iub() + first_dis_iub);
  }
  // -----------------------------------------------------------------------
  // 2) get external dof numbering:
  // -----------------------------------------------------------------------
  std::set<size_type> ext_dof_set;
  base::get_constitution().compute_external_dofs (ext_dof_set);
  base::_idof2blk_dis_iub.set_dis_indexes (ext_dof_set);
  // -----------------------------------------------------------------------
  // 3) build also ext_iu_set and ext_ib_set, for field::dis_dof(dis_idof)
  // -----------------------------------------------------------------------
  for (typename std::set<size_type>::const_iterator iter = ext_dof_set.begin(), last = ext_dof_set.end();
		iter != last; iter++) {
    size_type dis_idof = *iter;
    const space_pair_type& blk_dis_iub = base::_idof2blk_dis_iub.dis_at (dis_idof);
    if (! blk_dis_iub.is_blocked()) {
      _ext_iu_set.insert (blk_dis_iub.dis_iub());
    } else {
      _ext_ib_set.insert (blk_dis_iub.dis_iub());
    }
  }
}
// ----------------------------------------------------------------------------
// accessors for external dofs
// ----------------------------------------------------------------------------
template <class T>
typename space_rep<T,distributed>::size_type
space_rep<T,distributed>::dis_idof2dis_iub (size_type dis_idof) const 
{
    base::freeze_guard();
    return base::_idof2blk_dis_iub.dis_at (dis_idof).dis_iub();
}
template <class T>
bool
space_rep<T,distributed>::dis_is_blocked (size_type dis_idof) const
{
    base::freeze_guard();
    return base::_idof2blk_dis_iub.dis_at (dis_idof).is_blocked();
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class space_rep<Float,distributed>;

} // namespace rheolef
#endif // _RHEOLEF_HAVE_MPI
