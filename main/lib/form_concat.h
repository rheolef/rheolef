#ifndef _RHEOLEF_FORM_CONCAT_H
#define _RHEOLEF_FORM_CONCAT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// build form from initializer list (c++ 2011)
//
#include "rheolef/field.h"
#include "rheolef/form.h"
#include "rheolef/csr_concat.h"
#include "rheolef/field_expr_recursive.h"

namespace rheolef { namespace details {

// =========================================================================
// 1rst case : one-line matrix initializer
//  A = {a, b};			// matrix & vector
// =========================================================================

template <class T, class M>
struct vector_field_trans {
  vector_field_trans (const std::vector<field_basic<T,M> >& vv1) : vv(vv1) {}
  std::vector<field_basic<T,M> >   vv;
};

} // namespace details

template <class T, class M>
inline
details::vector_field_trans<T,M>
trans (const std::vector<field_basic<T,M> >& vv)
{
  return details::vector_field_trans<T,M> (vv);
}

namespace details {

template <class T, class M>
class form_concat_value {
public:

// typedef:

  typedef typename csr_concat_value<T,M>::size_type       size_type;
  typedef typename csr_concat_value<T,M>::sizes_type      sizes_type;      // [size,dis_size]
  typedef typename csr_concat_value<T,M>::sizes_pair_type sizes_pair_type; // [nrow,ncol] = [[nrow,dis_nrow],[ncol,dis_ncol]]

  static constexpr size_type undef = std::numeric_limits<size_type>::max();
  static constexpr size_type zero  = 0;

  typedef enum {
      scalar,
      field,
      field_transpose,
      vector_field,
      vector_field_transpose,
      form
    } variant_type;

  typedef field_expr_v2_nonlinear_node_unary<
            trans_
           ,field_expr_v2_nonlinear_terminal_field<T, M, differentiate_option::none>
          >
          trans_field_type;

// allocators:

  template <class U,
            class Sfinae
                  = typename std::enable_if<
                      is_rheolef_arithmetic<U>::value
                     ,void
                    >::type
           >
  form_concat_value (const U& x)                                : s(x), v(),                vv(),     m(),  variant(scalar) {}
  form_concat_value (const field_basic<T,M>& x)                 : s(),  v(x),               vv(),     m(),  variant(field) {}
  form_concat_value (const trans_field_type& x)                 : s(),  v(x.expr().expr()), vv(),     m(),  variant(field_transpose) {}
  form_concat_value (const std::vector<field_basic<T,M>>& x)    : s(),  v(),                vv(x),    m(),  variant(vector_field) {}
  form_concat_value (const vector_field_trans<T,M>& x) : s(),  v(),                vv(x.vv), m(),  variant(vector_field_transpose) {}
  form_concat_value (const form_basic<T,M>& x)                  : s(),  v(),                vv(),     m(x), variant(form) {}

// io/debug:
  friend std::ostream& operator<< (std::ostream& o, const form_concat_value<T,M>& x) {
         if (x.variant == scalar) return o << "s";
    else if (x.variant == field)  return o << "f";
    else if (x.variant == field_transpose) return o << "ft";
    else if (x.variant == vector_field) return o << "vf";
    else if (x.variant == vector_field_transpose) return o << "vft";
    else return o << "m";
  }
// data:
public:
  T                              s;
  field_basic<T,M>      v;
  std::vector<field_basic<T,M>>  vv;
  form_basic<T,M>                m;
  variant_type                   variant;
};
template <class T, class M>
class form_concat_line {
public:

// typedef:

  typedef typename csr_concat_value<T,M>::size_type       size_type;
  typedef typename csr_concat_value<T,M>::sizes_type      sizes_type;      // [size,dis_size]
  typedef typename csr_concat_value<T,M>::sizes_pair_type sizes_pair_type; // [nrow,ncol] = [[nrow,dis_nrow],[ncol,dis_ncol]]

  static constexpr size_type undef = std::numeric_limits<size_type>::max();
  static constexpr size_type zero  = 0;

  typedef form_concat_value<T,M>                value_type;
  typedef typename std::list<value_type>::const_iterator const_iterator;

// allocators:

  form_concat_line () : _l() {}

  form_concat_line (const std::initializer_list<value_type>& il) : _l() {
    typedef typename std::initializer_list<value_type>::const_iterator const_iterator;
    for(const_iterator iter = il.begin(); iter != il.end(); ++iter) {
        _l.push_back(*iter);
    }
 }

// accessors:

  const_iterator begin() const { return _l.begin(); }
  const_iterator end()   const { return _l.end(); }

  friend std::ostream& operator<< (std::ostream& o, const form_concat_line<T,M>& x) {
    std::cout << "{";
    for(typename std::list<value_type>::const_iterator iter = x._l.begin(); iter != x._l.end(); ++iter) {
        std::cout << *iter << " ";
    }
    return std::cout << "}";
 }
// internals:

  void build_form_pass0 (std::vector<std::pair<bool,space_basic<T,M> > >& l_Xh, space_basic<T,M>& Yh, size_t i_comp = 0) const;
  static void build_first_space (const std::vector<std::pair<bool,space_basic<T,M> > >& l_Xh, space_basic<T,M>& Xh);
  void build_form_pass1 (space_basic<T,M>& Xh, space_basic<T,M>& Yh) const;
  form_basic<T,M> build_form_pass2 (const space_basic<T,M>& Xh, const space_basic<T,M>& Yh) const;
  form_basic<T,M> build_form () const;

// data:
protected:
  std::list<value_type> _l;
};

} // namespace details

// -------------------------------
// form cstor from std::initializer
// -------------------------------
template <class T, class M>
inline
form_basic<T,M>::form_basic (const std::initializer_list<details::form_concat_value<T,M> >& init_list)
 : _X(), _Y(), _uu(), _ub(), _bu(), _bb()
{
  details::form_concat_line<T,M> cc (init_list);
  form_basic<T,M>::operator= (cc.build_form());
}

namespace details  {
// =========================================================================
// 2nd case : multi-line form initializer
//  A = { {a, b  },
//        {c, d} };
// =========================================================================
template <class T, class M>
class form_concat {
public:

// typedef:

  typedef typename csr_concat_value<T,M>::size_type       size_type;
  typedef typename csr_concat_value<T,M>::sizes_type      sizes_type;      // [size,dis_size]
  typedef typename csr_concat_value<T,M>::sizes_pair_type sizes_pair_type; // [nrow,ncol] = [[nrow,dis_nrow],[ncol,dis_ncol]]

  static constexpr size_type undef = std::numeric_limits<size_type>::max();
  static constexpr size_type zero  = 0;
 
  typedef form_concat_line<T,M>           line_type;
  typedef form_concat_value<T,M>          value_type;

// allocators:

  form_concat () : _l() {}

  form_concat (const std::initializer_list<line_type>& il) : _l() {
    typedef typename std::initializer_list<line_type>::const_iterator const_iterator;
    for(const_iterator iter = il.begin(); iter != il.end(); ++iter) {
        _l.push_back(*iter);
    }
  }
  friend std::ostream& operator<< (std::ostream& o, const form_concat<T,M>& x) {
    std::cout << "{";
    for(typename std::list<line_type>::const_iterator iter = x._l.begin(); iter != x._l.end(); ++iter) {
        std::cout << *iter << " ";
    }
    return std::cout << "}";
  }
// internals:
  form_basic<T,M> build_form () const;

// data:
protected:
  std::list<line_type> _l;
};

} // namespace details

template <class T, class M>
inline
form_basic<T,M>::form_basic (const std::initializer_list<details::form_concat_line<T,M> >& init_list)
 : _X(), _Y(), _uu(), _ub(), _bu(), _bb()
{
  details::form_concat<T,M> cc (init_list);
  form_basic<T,M>::operator= (cc.build_form());
}

} // namespace rheolef
#endif // _RHEOLEF_FORM_CONCAT_H
