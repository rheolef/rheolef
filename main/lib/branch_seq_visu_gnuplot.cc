///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// animations using gnuplot
//
// author: Pierre.Saramito@imag.fr
//
// date: 17 november 2002
//
#include "rheolef/branch.h"
#include "rheolef/rheostream.h"
#include "rheolef/basis_on_pointset.h"
#include "field_seq_visu_gnuplot_internal.h"

namespace rheolef {
using namespace std;

// ====================================================
// yet hard-coded ! TODO: use a -select-contour option
// ====================================================
template <class T>
void
put_header_gnuplot (odiststream& ops, const branch_basic<T,sequential>& b)
{
    ostream& out = ops.os();
    delete_macro (b._p_ctrl_out);
    delete_macro (b._p_data_out);
    string basename = iorheo::getbasename(out);
    if (basename == "") basename = "output";
    bool do_verbose = iorheo::getverbose(clog);
    bool do_execute = iorheo::getexecute(clog);
    bool do_clean   = iorheo::getclean  (clog);
    string tmp = (do_clean ? get_tmpdir() + "/" : "");

    string plot_name = tmp+basename + ".plot";
    string param_name = tmp+basename + "-visu-param.plot";
    b._to_clean = plot_name;
    b._p_ctrl_out = new_macro (ofstream(plot_name.c_str()));
    do_verbose && clog << "! file `" << plot_name << "' created" << endl;
    *(b._p_ctrl_out) << setbasename(basename)
                     << setprecision(numeric_limits<T>::digits10)
                     << "load \"" << param_name << "\"" << endl;
    b._have_u_range.first  = (b._u_range.first  !=  std::numeric_limits<T>::max());
    b._have_u_range.second = (b._u_range.second != -std::numeric_limits<T>::max());
}
template <class T>
void
put_event_gnuplot (odiststream& ops, const branch_basic<T,sequential>& b)
{
  typedef typename branch_basic<T,sequential>::size_type size_type;
  bool do_verbose = iorheo::getverbose(clog);
  size_t nfld = b.n_field();
  if (nfld == 0) return;
  // TODO: assume all fields have the same mesh
  const space_basic<T,sequential>&           Xh    = b[0].second.get_space();
  const geo_basic<T,sequential>&             omega = Xh.get_geo();
  const disarray<point_basic<T>,sequential>& xdof  = Xh.get_xdofs();

  bool do_clean   = iorheo::getclean  (clog);
  string basename = iorheo::getbasename(ops.os());
  if (basename == "") basename = "output";
  string tmp = (do_clean ? get_tmpdir() + "/" : "");
  // ---------------------------------------------------------
  // 1) range : as computed at the first occurrence
  // -----------------------------------------------
  if (b._count_value == 0) {
    *(b._p_ctrl_out) << "set xrange [" << omega.xmin()[0] << ":" << omega.xmax()[0] << "]" << endl;
  }
  if (! b._have_u_range.first || ! b._have_u_range.second) {
    for (size_t ifld = 0; ifld < nfld; ifld++) {
      const field_basic<T,sequential>& u = b[ifld].second;
      if (! b._have_u_range.first) {
        b._u_range.first  = std::min (b._u_range.first,  u.min());
      }
      if (! b._have_u_range.second) {
        b._u_range.second = std::max (b._u_range.second, u.max());
      }
    }
  }
  // -----------------------------------------------
  // 2) plot commands
  // -----------------------------------------------
  const string& pname = b.parameter_name() + " = " + ftos(b.parameter());
  *(b._p_ctrl_out) << "set title \"i = " << b._count_value << ": " << pname << "\" font \"arial,20\"" << endl
                   << "plot \\" << endl;
  for (size_t ifld = 0; ifld < nfld; ifld++) {
      const string& uname = b[ifld].first;
      string gdat_name = tmp + basename + "_" + uname + "_" + std::to_string(b._count_value) + ".gdat";
      *(b._p_ctrl_out) << (ifld == 0 ? " " : ",")
                       << " \"" << gdat_name << "\""
                       << " u 1:2"
                       << " t \"" << uname << "\""
                       << " w l"
                       << (ifld < nfld-1 ? " \\" : "")
                       << endl;
  }
  *(b._p_ctrl_out) << "pause dt_pause" << endl;
  // -----------------------------------------------
  // 3) data
  // -----------------------------------------------
  // TODO: subdivide option not set by the branch unix command
  size_type subdivide = iorheo::getsubdivide(ops.os());
  bool fill           = iorheo::getfill(ops.os());    // show grid or fill elements
  for (size_t ifld = 0; ifld < nfld; ifld++) {
      const string&                 uname = b[ifld].first;
      const field_basic<T,sequential>& uh = b[ifld].second;
      string gdat_name = tmp + basename + "_" + uname + "_" + std::to_string(b._count_value) + ".gdat";
      ofstream gdat (gdat_name.c_str());
      b._to_clean = b._to_clean + " " + gdat_name;
      if (do_verbose) clog << "! file \"" << gdat_name << "\" created.\n";
      gdat << setprecision(numeric_limits<T>::digits10);
      const geo_basic<T,sequential>& omega = uh.get_geo();
      const basis_basic<T>& b_fem = uh.get_space().get_basis();
      if (subdivide == 0) { // subdivide is unset: use default
        subdivide = std::max(omega.order(), subdivide);
        subdivide = std::max(b_fem.degree (), subdivide);
      }
      size_type map_dim = omega.map_dimension();
      size_type used_dim = (fill ? map_dim : 1);
      basis_basic<T> subdivide_pointset ("P"+std::to_string(subdivide));
      piola_on_pointset<T> pops; pops.initialize (omega.get_piola_basis(), subdivide_pointset, integrate_option());
      fem_on_pointset<T>   fops; fops.initialize (b_fem, pops);
      bound_type<T> bbox;
      bbox.xmin = omega.xmin();
      bbox.xmax = omega.xmax();
      bbox.umin = uh.min();
      bbox.umax = uh.max();
      for (size_type ie = 0, ne = omega.size(used_dim); ie < ne; ie++) {
        const geo_element& K = omega.get_geo_element(used_dim,ie);
        put (gdat, omega, K, uh, fops, subdivide, bbox);
      }
      gdat.close();
  }
}
template <class T>
void
put_finalize_gnuplot (odiststream& ops, const branch_basic<T,sequential>& b)
{
  // is the destructor virtual : ofstream::~ofstream() or opstream::~opstream() ?
  string basename = iorheo::getbasename(ops.os());
  if (basename == "") basename = "output";
  bool do_verbose = iorheo::getverbose(clog);
  bool do_execute = iorheo::getexecute(clog);
  bool do_clean   = iorheo::getclean(clog);
  bool reader_on_stdin = iorheo::getreader_on_stdin(ops.os());
  if (! reader_on_stdin) {
    *(b._p_ctrl_out) << "pause -1 \"<end>\"" << endl;
  }
  string tmp = (do_clean ? get_tmpdir() + "/" : "");
  //
  // compute visu params: u_range, pause delay
  //
  string param_name = tmp+basename + "-visu-param.plot";
  ofstream param (param_name.c_str());
  if (do_verbose) clog << "! file \"" << param_name << "\" created.\n";
  b._to_clean = b._to_clean + " " + param_name;
  param << "duration = 30 # in seconds" << endl
        << "n_step   = " << b._count_value << endl
        << "dt_pause = 1.0*duration/n_step" << endl;
  if (b._u_range.second - b._u_range.first < 1e-7) {
      // perhaps the solution is zero ? try to renormalize
      b._u_range.second = b._u_range.first + 1;
  }
  param << "set yrange [" << b._u_range.first << ":" << b._u_range.second << "]" << endl;
  param.close();
  //
  // run gnuplot
  //
  int status = 0;
  string command;
  if (do_execute) {
      command = "gnuplot ";
      if (reader_on_stdin) command += "-persist ";
      command += tmp + basename + ".plot";
      if (do_verbose) clog << "! " << command << endl;
      cin.sync();
      status = system (command.c_str());
  }
  //
  // clear gnuplot data
  //
  if (do_clean) {
        string tmp = get_tmpdir() + "/";
        if (!do_clean) tmp = "";
	string command = "/bin/rm -f " + b._to_clean;
	// to much files to see:
        // do_verbose && clog << "! " << command << endl;
        do_verbose && clog << "! cleaning temporary .plot and .gdat files" << endl;
	int status = system (command.c_str());
  }
  delete_macro (b._p_ctrl_out);
  delete_macro (b._p_data_out);
  b._p_ctrl_out = 0;
  b._p_data_out = 0;
  b._to_clean = "";
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template void put_header_gnuplot   (odiststream&, const branch_basic<Float,sequential>&);
template void put_event_gnuplot    (odiststream&, const branch_basic<Float,sequential>&);
template void put_finalize_gnuplot (odiststream&, const branch_basic<Float,sequential>&);

} // namespace rheolef
