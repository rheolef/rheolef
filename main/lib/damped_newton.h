# ifndef _RHEO_DAMPED_NEWTON_H
# define _RHEO_DAMPED_NEWTON_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   14 oct 2009

namespace rheolef {
/**
@functionfile damped_newton nonlinear solver
@addindex nonlinear problem
@addindex Newton method

Synopsis
========
@snippet damped_newton.h verbatim_damped_newton

Description
===========
This function implements a generic
damped Newton method
for the resolution of the following problem:

        F(u) = 0

Recall that the damped Newton method is more robust
than the basic Newton one: it converges from any initial
value.

A simple call to the algorithm writes:

        my_problem P;
        field uh (Xh);
        damped_newton (P, uh, tol, max_iter);

In addition to the members required for the @ref newton_3 method,
two additional members are required for the damped variant:

        class my_problem {
        public:
          ...
          value_type derivative_trans_mult (const value_type& mrh) const;
          Float space_norm (const value_type& uh) const;
        };

The `derivative_trans_mult` is used for computing the damping coefficient.
The `space_norm` represents usually a L2 norm e.g. formally:

                              /
        space_norm(uh) = sqrt |       |uh(x)|^2 dx
                              / Omega

Example
=======
See the @ref p_laplacian_damped_newton.cc example
and the @ref usersguide_page for more.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/damped-newton-generic.h"
#include "rheolef/newton_add_missing.h"

namespace rheolef { 

// [verbatim_damped_newton]
template <class Problem, class Field, class Real, class Size>
int damped_newton (const Problem& F, Field& u, Real& tol, Size& max_iter, odiststream* p_derr=0)
// [verbatim_damped_newton]
{
  details::add_missing_damped_newton<Problem> G (F); // TODO: avoid a copy of pb F here: put a reference in G ?
  return damped_newton (G, newton_identity_preconditioner(), u, tol, max_iter, p_derr);
}

}// namespace rheolef
# endif // _RHEO_DAMPED_NEWTON_H
