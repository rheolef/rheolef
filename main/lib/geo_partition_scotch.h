#ifndef _RHEOLEF_PARMETIS_TOOLS_H
#define _RHEOLEF_PARMETIS_TOOLS_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/distributor.h"

namespace rheolef {

typedef int my_idxtype;
#define IDX_DATATYPE	MPI_INT
typedef my_idxtype idxtype;

// main call
extern void geo_partition_scotch (
	my_idxtype *elmdist,
	my_idxtype *eptr, 
	std::vector<my_idxtype>& eind,
	my_idxtype *elmwgt, 
	int *ncon,
	int *ncommonnodes,
	int *nparts, 
	float *tpwgts,
	float *ubvec,
	int *edgecut,
	my_idxtype *part, 
	const mpi::communicator& comm);

// internal materials:
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdarg.h>
#include <limits.h>
#include <time.h>
#include <assert.h>

#define UNBALANCE_FRACTION		1.05
#define DBG_INFO			2		/* Perform timing analysis */
#define PMV3_OPTION_DBGLVL		1
#define PMV3_OPTION_SEED		2
#define MAXNCON                 	12
#define	MAXLINE				8192


struct MeshType {
  rheolef::distributor ownership;
  std::vector<my_idxtype> elements;
  int etype;
  int gnelms, gnns;
  int nelms, nns;
  int ncon;
  int esize, gminnode;
  my_idxtype *elmwgt;
  MeshType();
private:
  MeshType(const MeshType& x);
  MeshType& operator= (const MeshType& x);
};
inline
MeshType::MeshType()
: ownership(),
  elements(),
  etype(0),
  gnelms(0),
  gnns(0),
  nelms(0),
  nns(0),
  ncon(0),
  esize(0),
  gminnode(0),
  elmwgt(0)
{
}
#ifdef TODO
inline
MeshType::MeshType(const MeshType& x)
: ownership(x.ownership),
  elements(x.elements),
  etype(x.etype),
  gnelms(x.gnelms),
  gnns(x.gnns),
  nelms(x.nelms),
  nns(x.nns),
  ncon(x.ncon),
  esize(x.esize),
  gminnode(x.gminnode),
  elmwgt(ARGGG)
{
}
#endif // TODO

/*************************************************************************
* The following data structure stores key-value pair
**************************************************************************/

struct KeyValueType {
  my_idxtype key;
  my_idxtype val;
};
typedef struct KeyValueType KeyValueType;

#define amax(a, b) ((a) >= (b) ? (a) : (b))
#define icopy(n, a, b) memcpy((b), (a), sizeof(int)*(n))
#define idxcopy(n, a, b) memcpy((b), (a), sizeof(my_idxtype)*(n))
#define SHIFTCSR(i, n, a) \
   do { \
     for (i=n; i>0; i--) a[i] = a[i-1]; \
     a[0] = 0; \
   } while(0)

template <class Size, class RandomIOIterator>
static inline void init_csr_ptr (Size n, const RandomIOIterator& aptr) {
     for (Size i=1; i<n; i++) aptr[i] += aptr[i-1]; 
     for (Size i=n; i>0; i--) aptr[i] = aptr[i-1];
     aptr[0] = 0;
}
template <class Size, class RandomIOIterator>
static inline void shift_csr_ptr (Size n, const RandomIOIterator& aptr) {
     for (long int i = n; i > 0; i--) aptr[i] = aptr[i-1];
     aptr[0] = 0;
}

/*************************************************************************
* These functions return the index of the minimum element in a vector
**************************************************************************/
inline
int
idxamin(int n, const std::vector<my_idxtype>& x) {
  int min=0;
  for (int i=1; i<n; i++) min = (x[i] < x[min] ? i : min);
  return min;
}
/*************************************************************************
* These functions return the index of the maximum element in a vector
**************************************************************************/
inline
int
idxamax(int n, const std::vector<my_idxtype>& x) {
  int max=0; 
  for (int i=1; i<n; i++) max = (x[i] > x[max] ? i : max);
  return max;
}

} // namespace rheolef
#endif // _RHEOLEF_PARMETIS_TOOLS_H
