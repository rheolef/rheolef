#ifndef _RHEOLEF_FIELD_EXPR_QUADRATURE_H
#define _RHEOLEF_FIELD_EXPR_QUADRATURE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// variational expressions are integrated by using a quadrature formulae
//
// author: Pierre.Saramito@imag.fr
//
// date: 16 december 2018 
//
// SUMMARY:
// 1. concept
// 2. terminals
//    2.1. integration on one element K
//    2.2. integration on element boundary partial K
// 3. unary function 
//    2.1. unary node
//    2.2. unary calls
// 4. binary operators +-  between two integrated fields
//    3.1. binary node
//    3.2. binary calls
// 5. binary operators */ between a integrated field and a constant
//
#include "rheolef/field_expr_variational.h"
#include "rheolef/field_expr_variational_terminal.h"
#include "rheolef/init_expr_quadrature.h"

namespace rheolef {

// -------------------------------------------------------------------
// 1. concept
// -------------------------------------------------------------------
namespace details {

// Define a trait type for detecting field expression valid arguments
template<class T> struct is_field_expr_quadrature_arg: std::false_type {};

} // namespace details

// ---------------------------------------------------------------------------
// 2. terminals
// ---------------------------------------------------------------------------
// 2.1. integration on one element K
// ---------------------------------------------------------------------------
namespace details {

template<class Expr>
class field_expr_quadrature_on_element {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename Expr::memory_type                    memory_type;
  typedef typename Expr::value_type                     result_hint;
  typedef typename Expr::value_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<float_type,memory_type>		space_type;
  typedef geo_basic  <float_type,memory_type>		geo_type;
  typedef typename Expr::vf_tag_type                    vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef field_expr_quadrature_on_element<Expr>        self_type;
  typedef field_expr_quadrature_on_element<typename Expr::dual_self_type>
                                                        dual_self_type;
  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// allocators:

  template<class Sfinae = typename std::enable_if<is_field_expr_v2_variational_arg<Expr>::value, Expr>::type>
  field_expr_quadrature_on_element (const Expr& expr);

// mutable modifiers:

  void initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt);
  void initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt);

// accessors:

  const space_type&  get_vf_space() const { return _expr.get_vf_space(); }
  size_type n_derivative() const          { return _expr.n_derivative(); }

  template<class Value>
  void evaluate (
  	const geo_basic<float_type,memory_type>& omega_K,
  	const geo_element&                       K,
  	Eigen::Matrix<Value,Eigen::Dynamic,1>&   lk) const;

  template<class Value>
  void valued_check() const {
    if (! is_undeterminated<Value>::value) { _expr.template valued_check<Value>(); }
  }
protected:
// data:
  Expr                                  _expr;
  mutable piola_on_pointset<float_type> _pops;
};
template<class Expr> struct is_field_expr_quadrature_arg <field_expr_quadrature_on_element<Expr> > : std::true_type {};

// ---------------------------------------------------------------------------
// inlined
// ---------------------------------------------------------------------------
template<class Expr>
template<class Sfinae>
inline
field_expr_quadrature_on_element<Expr>::field_expr_quadrature_on_element (const Expr& expr)
 : _expr(expr),
   _pops()
{
}
template<class Expr>
void
field_expr_quadrature_on_element<Expr>::initialize (
  const geo_basic<float_type,memory_type>& omega_K,
  const integrate_option&                  iopt)
{
  integrate_option new_iopt   = expr_quadrature_init_iopt (omega_K, get_vf_space(), n_derivative(), iopt);
  quadrature<float_type> quad = expr_quadrature_init_quad<float_type> (new_iopt);
  _pops.initialize (omega_K.get_piola_basis(),  quad, new_iopt);
  _expr.initialize (_pops, new_iopt);
}
template<class Expr>
void
field_expr_quadrature_on_element<Expr>::initialize (
  const band_basic<float_type,memory_type>& gh,
  const integrate_option&                   iopt)
{  
  integrate_option new_iopt   = expr_quadrature_init_iopt (gh.band(), get_vf_space(), n_derivative(), iopt);
  quadrature<float_type> quad = expr_quadrature_init_quad<float_type> (new_iopt);
  _pops.initialize (gh.band().get_piola_basis(),  quad, new_iopt);
  _expr.initialize (gh, _pops, new_iopt);
}
template<class Expr>
template<class Value>
void
field_expr_quadrature_on_element<Expr>::evaluate (
  const geo_basic<float_type,memory_type>& omega_K,
  const geo_element&                       K,
  Eigen::Matrix<Value,Eigen::Dynamic,1>&   lk) const
{
  Eigen::Matrix<float_type,Eigen::Dynamic,Eigen::Dynamic> phij_xi;
  const Eigen::Matrix<float_type,Eigen::Dynamic,1>& w = _pops.get_weight (omega_K, K);
  _expr.evaluate (omega_K, K, phij_xi);
  // blas2: lk(j) = sum_i phi_j(xi)*w(xi) = trans(phi)*w  ; TODO: DVT_EIGEN_BLAS2
  size_t ni = phij_xi.rows();
  size_t nj = phij_xi.cols();
  lk.resize (nj);
  for (size_t j = 0; j < nj; ++j) {
    Value sum = 0;
    for (size_t i = 0; i < ni; ++i) {
      sum += w[i] * phij_xi(i,j);
    }
    lk[j] = sum;
  }
}

} // namespace details

// ---------------------------------------------------------------------------
// 2.2. integration on element boundary partial K
// ---------------------------------------------------------------------------
namespace details {

template<class Expr>
class field_expr_quadrature_on_sides {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename Expr::memory_type                    memory_type;
  typedef typename Expr::value_type                     result_hint;
  typedef typename Expr::value_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<float_type,memory_type>           space_type;
  typedef geo_basic  <float_type,memory_type>		geo_type;
  typedef typename Expr::vf_tag_type                    vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef field_expr_quadrature_on_sides<Expr>          self_type;
  typedef field_expr_quadrature_on_sides<typename Expr::dual_self_type>
                                                        dual_self_type;
  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  template<class Sfinae = typename std::enable_if<is_field_expr_v2_variational_arg<Expr>::value, Expr>::type>
  field_expr_quadrature_on_sides (const Expr& expr);

// accessors:

  const space_type&  get_vf_space() const { return _expr.get_vf_space(); }
  size_type n_derivative() const             { return _expr.n_derivative(); }

// mutable modifiers:

  void initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt);
  void initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt);

  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>& omega_K,
    const geo_element&                       K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&  value) const;

  template<class Value>
  void valued_check() const {
    typedef Value A1;
    if (! is_undeterminated<A1>::value) { _expr.template valued_check<A1>(); }
  }
protected:
// data:
  mutable Expr                               _expr;
  mutable piola_on_pointset<float_type>      _pops;
  mutable bool                               _ignore_sys_coord;
// working variables:
  mutable Eigen::Matrix<float_type,Eigen::Dynamic,Eigen::Dynamic> _value_i;
  mutable std::vector<size_type>                     _dis_inod_S;
  mutable tensor_basic<float_type>                   _DF;
};
template<class Expr> struct is_field_expr_quadrature_arg <field_expr_quadrature_on_sides<Expr> > : std::true_type {};

// ---------------------------------------------------------------------------
// inlined
// ---------------------------------------------------------------------------
template<class Expr>
template<class Sfinae>
inline
field_expr_quadrature_on_sides<Expr>::field_expr_quadrature_on_sides (const Expr& expr)
 : _expr(expr),
   _pops(),
   _ignore_sys_coord(false),
   _value_i(),
   _dis_inod_S(),
   _DF()
{
}
template<class Expr>
void
field_expr_quadrature_on_sides<Expr>::initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt)
{
  _ignore_sys_coord = iopt.ignore_sys_coord;
  integrate_option new_iopt   = expr_quadrature_init_iopt (omega_K, get_vf_space(), n_derivative(), iopt);
  quadrature<float_type> quad = expr_quadrature_init_quad<float_type> (new_iopt);
  new_iopt._is_inside_on_local_sides = true; // propagated recursively in expression
  _pops.initialize (omega_K.get_piola_basis(), quad, new_iopt);
  _expr.initialize (_pops, new_iopt);
}
template<class Expr>
void
field_expr_quadrature_on_sides<Expr>::initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt)
{
  _ignore_sys_coord = iopt.ignore_sys_coord;
  integrate_option new_iopt   = expr_quadrature_init_iopt (gh.band(), get_vf_space(), n_derivative(), iopt);
  quadrature<float_type> quad = expr_quadrature_init_quad<float_type> (new_iopt);
  new_iopt._is_inside_on_local_sides = true; // propagated recursively in expression
  _pops.initialize (gh.band().get_piola_basis(), quad, new_iopt);
  _expr.initialize (gh, _pops, new_iopt);
  fatal_macro("on_local_sides: banded level set not yet supported, sorry");
}
template<class Expr>
template<class Value>
void
field_expr_quadrature_on_sides<Expr>::evaluate (
    const geo_basic<float_type,memory_type>& omega_K,
    const geo_element&                       K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&   value) const
{
  bool do_local_component_assembly = true;
  size_type sid_dim = K.dimension()-1;
  for (size_type isid = 0, nsid = K.n_subgeo(sid_dim); isid < nsid; ++isid) {
    size_type dis_isid = (K.dimension() == 1) ? K[isid] : (K.dimension() == 2) ? K.edge(isid) : K.face(isid);
    const geo_element& S = omega_K.dis_get_geo_element (sid_dim, dis_isid);
    side_information_type sid;
    K.get_side_informations (S, sid);
    _expr.evaluate_on_side (omega_K, K, sid, _value_i, do_local_component_assembly);
    const Eigen::Matrix<float_type,Eigen::Dynamic,1>& w = _pops.get_weight (omega_K, S);
    // blas2: lk(j) = sum_S sum_i phi_j(xi)*w(xi) = sum_S trans(phi)*w  ; TODO: DVT_EIGEN_BLAS2
    size_t ni = _value_i.rows();
    size_t nj = _value_i.cols();
    if (isid == 0) {
      value = Eigen::Matrix<Value,Eigen::Dynamic,1>::Zero(nj, 1);
    }
    for (size_t j = 0; j < nj; ++j) {
      Value sum = 0;
      for (size_t i = 0; i < ni; ++i) {
        sum += w[i] * _value_i(i,j);
      }
      value[j] += sum;
    }
  }
}

} // namespace details

//! @brief on_local_sides(expr): see the @ref expression_3 page for the full documentation
template<class Expr>								
inline										
typename									
std::enable_if<
  details::is_field_expr_v2_variational_arg<Expr>::value				
 ,details::field_expr_quadrature_on_sides<Expr>										
>::type										
on_local_sides (const Expr& expr)							
{
  return details::field_expr_quadrature_on_sides<Expr>(expr);
}

#ifdef TODO
// ---------------------------------------------------------------------------
// 3. unary function
// ---------------------------------------------------------------------------
// 3.1. unary node
// ---------------------------------------------------------------------------
namespace details {

template<class UnaryFunction, class Expr>
class field_expr_quadrature_unary {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename Expr::memory_type                    memory_type;
  typedef typename details::generic_unary_traits<UnaryFunction>::template result_hint<
          typename Expr::value_type>::type              result_hint;
  typedef typename details::generic_unary_traits<UnaryFunction>::template hint<
	  typename Expr::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<float_type,memory_type>           space_type;
  typedef geo_basic  <float_type,memory_type>		geo_type;
  typedef typename Expr::vf_tag_type                    vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef field_expr_quadrature_unary<UnaryFunction,Expr>           self_type;
  typedef field_expr_quadrature_unary<UnaryFunction, typename Expr::dual_self_type>
                                                        dual_self_type;

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  template<class Sfinae = typename std::enable_if<is_field_expr_quadrature_arg<Expr>::value, Expr>::type>
  field_expr_quadrature_unary (const UnaryFunction& f, const Expr& expr)
    : _f(f), _expr(expr) {}

// accessors:

  const space_type&  get_vf_space() const { return _expr.get_vf_space(); }
  size_type n_derivative() const             { return _expr.n_derivative(); }

// mutable modifiers:

  void initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt) {
    return _expr.initialize (omega_K, iopt); }
  void initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt) {
    return _expr.initialize (gh,  iopt); }

  template<class Value>
  void evaluate (const geo_element& K, Eigen::Matrix<Value,Eigen::Dynamic,1>& value) const {
    typedef Value A1; // Value is float_type in general: elementary matrix
    _expr.evaluate (K, value);
    for (size_type i = 0, ni = value.rows(); i < ni; ++i) {
    for (size_type j = 0, nj = value.cols(); j < nj; ++j) {
      value(i,j) = _f (value(i,j));
    }}
  }
  template<class Value>
  void valued_check() const {
    typedef Value A1;
    if (! is_undeterminated<A1>::value) { _expr.template valued_check<A1>(); }
  }
protected:
// data:
  UnaryFunction  _f;
  Expr           _expr;
};
template<class F, class Expr> struct is_field_expr_quadrature_arg    <field_expr_quadrature_unary<F,Expr> > : std::true_type {};

} // namespace details
// ---------------------------------------------------------------------------
// 3.2. unary calls
// ---------------------------------------------------------------------------

#define _RHEOLEF_make_field_expr_quadrature_unary(FUNCTION,FUNCTOR)		\
template<class Expr>								\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_quadrature_arg<Expr>::value				\
 ,details::field_expr_quadrature_unary<						\
    FUNCTOR									\
   ,Expr									\
  >										\
>::type										\
FUNCTION (const Expr& expr)							\
{										\
  return details::field_expr_quadrature_unary <FUNCTOR,Expr> (FUNCTOR(), expr); \
}

_RHEOLEF_make_field_expr_quadrature_unary (operator+, details::unary_plus)
_RHEOLEF_make_field_expr_quadrature_unary (operator-, details::negate)
#undef _RHEOLEF_make_field_expr_quadrature_unary

#endif // TODO
// ---------------------------------------------------------------------------
// 4. binary operators +-  between two integrated fields
// ---------------------------------------------------------------------------
// 4.1. binary node
// ---------------------------------------------------------------------------
// example: operator+ between two fields as in
//	    (u*v) + on_local_sides(u*v)

namespace details {

template<class BinaryFunction, class Expr1, class Expr2>
class field_expr_quadrature_binary {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename promote_memory<typename Expr1::memory_type,typename Expr2::memory_type>::type 
 				                   	memory_type;
  typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<
          typename Expr1::value_type
         ,typename Expr2::value_type>::type             result_hint;
  typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<float_type,memory_type>           space_type;
  typedef geo_basic  <float_type,memory_type>		geo_type;
  typedef typename details::bf_vf_tag<BinaryFunction,
	typename Expr1::vf_tag_type,
	typename Expr2::vf_tag_type>::type              vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef field_expr_quadrature_binary<BinaryFunction,Expr1,Expr2>         self_type;
  typedef field_expr_quadrature_binary<BinaryFunction,typename Expr1::dual_self_type,
                                        typename Expr2::dual_self_type>
                                                        dual_self_type;
  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:
#ifdef TODO
  template<class Sfinae 
      = typename std::enable_if<
	    is_field_expr_quadrature_arg<Expr1>::value && is_field_expr_quadrature_arg<Expr2>::value
           ,Expr1
          >::type
        >
#endif // TODO
  field_expr_quadrature_binary (const BinaryFunction& f, 
		    const Expr1&    expr1,
                    const Expr2&    expr2)
    : _f(f), _expr1(expr1), _expr2(expr2) {}

// accessors:

  const space_type&  get_vf_space() const { return _expr1.get_vf_space(); }
  size_type n_derivative() const          { return std::min(_expr1.n_derivative(), _expr2.n_derivative()); }

// mutable modifiers:
  void initialize (const geo_basic<float_type,memory_type>& omega_K, const integrate_option& iopt) {
    _expr1.initialize (omega_K, iopt);
    _expr2.initialize (omega_K, iopt);
  }
  void initialize (const band_basic<float_type,memory_type>& gh, const integrate_option& iopt) {
    _expr1.initialize (gh,  iopt);
    _expr2.initialize (gh,  iopt);
  }
  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>& omega_K,
    const geo_element&                       K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&   value) const
  {
    // Value is float_type in general: elementary matrix
    // for f=operator+ => sum of two elementary matrix of the same type
    // TODO: otherwise Value and 2 could be obtained from the hint<> helper
    typedef Value A1;
    typedef Value A2;
    Eigen::Matrix<A2,Eigen::Dynamic,1> value2 (value.size());
    _expr1.evaluate (omega_K, K, value);
    _expr2.evaluate (omega_K, K, value2);
    for (size_type i = 0, ni = value.size(); i < ni; ++i) {
      value[i] = _f (value[i], value2[i]);
    }
  }
  template<class Value>
  void valued_check() const {
    typedef Value A1;
    typedef Value A2;
    if (! is_undeterminated<A1>::value)  { _expr1.template valued_check<A1>(); }
    if (! is_undeterminated<A2>::value)  { _expr2.template valued_check<A2>(); }
  }
protected:
// data:
  BinaryFunction  _f;
  Expr1           _expr1;
  Expr2           _expr2;
};
template<class F, class Expr1, class Expr2> struct is_field_expr_quadrature_arg    <field_expr_quadrature_binary<F,Expr1,Expr2> > : std::true_type {};

} // namespace details

// ---------------------------------------------------------------------------
// 4.2. binary calls
// ---------------------------------------------------------------------------
// expr_quad := expr_quad +- expr_quad
// expr_quad := expr_var  +- expr_quad
// expr_quad := expr_quad +- expr_var

#define _RHEOLEF_field_expr_quadrature_binary(FUNCTION,FUNCTOR)			\
template <class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
     details::is_field_expr_quadrature_arg <Expr1>::value			\
  && details::is_field_expr_quadrature_arg <Expr2>::value			\
 ,details::field_expr_quadrature_binary<					\
    FUNCTOR									\
   ,Expr1									\
   ,Expr2									\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::field_expr_quadrature_binary					\
    <FUNCTOR,   Expr1, Expr2>							\
    (FUNCTOR(), expr1, expr2);							\
}										\
template <class Expr1, class ExprVar2>						\
inline										\
typename									\
std::enable_if<									\
     details::is_field_expr_quadrature_arg <Expr1>::value			\
  && details::is_field_expr_v2_variational_arg <ExprVar2>::value		\
 ,details::field_expr_quadrature_binary<					\
    FUNCTOR									\
   ,Expr1									\
   ,details::field_expr_quadrature_on_element<ExprVar2>				\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const ExprVar2& expr_var2)			\
{										\
  using Expr2 = details::field_expr_quadrature_on_element<ExprVar2>;		\
  return details::field_expr_quadrature_binary					\
    <FUNCTOR,   Expr1, Expr2>							\
    (FUNCTOR(), expr1, Expr2(expr_var2));					\
}										\
template <class ExprVar1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
     details::is_field_expr_v2_variational_arg <ExprVar1>::value		\
  && details::is_field_expr_quadrature_arg <Expr2>::value			\
 ,details::field_expr_quadrature_binary<					\
    FUNCTOR									\
   ,details::field_expr_quadrature_on_element<ExprVar1>				\
   ,Expr2									\
  >										\
>::type										\
FUNCTION (const ExprVar1& expr_var1, const Expr2& expr2)			\
{										\
  using Expr1 = details::field_expr_quadrature_on_element<ExprVar1>;		\
  return details::field_expr_quadrature_binary					\
    <FUNCTOR,   Expr1, Expr2>							\
    (FUNCTOR(), Expr1(expr_var1), expr2);					\
}										\

_RHEOLEF_field_expr_quadrature_binary (operator+, details::plus)
_RHEOLEF_field_expr_quadrature_binary (operator-, details::minus)

#undef _RHEOLEF_field_expr_quadrature_binary

// ---------------------------------------------------------------------------
// 5. binary operators */ between a integrated field and a constant
// ---------------------------------------------------------------------------
// expr_quad := k*expr_quad
// expr_quad := expr_quad*k
// expr_quad := expr_quad/k
#ifdef TODO

namespace details {
  
template<class Expr1, class Expr2, class Sfinae = void>
struct is_field_expr_quadrature_binary_multiplies_divides_constant_left : std::false_type {};

template<class Expr1, class Expr2>
struct is_field_expr_quadrature_binary_multiplies_divides_constant_left <
  Expr1
 ,Expr2
 ,typename
  std::enable_if<
       is_rheolef_arithmetic      <Expr1>::value
    && is_field_expr_quadrature_arg<Expr2>::value
  >::type
>
: std::true_type
{};

template<class Expr1, class Expr2>
struct is_field_expr_quadrature_binary_multiplies_divides_constant_right
:      is_field_expr_quadrature_binary_multiplies_divides_constant_left <Expr2,Expr1> {};

} // namespace details

#define _RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant_left(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_quadrature_binary_multiplies_divides_constant_left <Expr1,Expr2>::value \
 ,details::field_expr_quadrature_unary<						\
    details::binder_first <FUNCTOR, Expr1> 					\
   ,Expr2 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::field_expr_quadrature_unary 					\
	<details::binder_first <FUNCTOR,Expr1>,                    Expr2> 	\
	(details::binder_first <FUNCTOR,Expr1> (FUNCTOR(), expr1), expr2); 	\
}

#define _RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant_right(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_quadrature_binary_multiplies_divides_constant_right <Expr1,Expr2>::value \
 ,details::field_expr_quadrature_unary<						\
    details::binder_second <FUNCTOR, Expr2> 					\
   ,Expr1 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::field_expr_quadrature_unary 					\
	<details::binder_second <FUNCTOR,Expr2>,                    Expr1> 	\
	(details::binder_second <FUNCTOR,Expr2> (FUNCTOR(), expr2), expr1); 	\
}

#define _RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant(FUNCTION,FUNCTOR)		\
        _RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant_left  (FUNCTION,FUNCTOR) 	\
        _RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant_right (FUNCTION,FUNCTOR)


_RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant       (operator*, details::multiplies)
_RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant_right (operator/, details::divides)

#undef _RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant_right
#undef _RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant_left
#undef _RHEOLEF_make_field_expr_quadrature_binary_operator_multiplies_divides_constant

#endif // TODO

} // namespace rheolef
#endif // _RHEOLEF_FIELD_EXPR_QUADRATURE_H
