#ifndef _RHEO_ADAPT_OPTION_H
#define _RHEO_ADAPT_OPTION_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@functionfile adapt adaptive mesh generation
@addindex  command `bamg`
@addindex  command `gmsh`

Synopsis
========

        geo adapt (const field& criterion);
        geo adapt (const field& criterion, const adapt_option& aopt);

Description
===========
The `adapt` function implements an adaptive mesh procedure,
based either on the `gmsh` (isotropic) or @ref bamg_1 (anisotropic) mesh generators.
The @ref bamg_1 mesh generator is the default in two dimension.
For dimension one or three, `gmsh` is the only generator supported yet.
In the two dimensional case, the `gmsh` correspond to the 
option `aopt.generator="gmsh"`, where `aopt`
is an `adap_option` variable (see @ref adapt_3).

Criterion and metric
====================
The strategy bases on a metric determined by the Hessian of
a scalar criterion field, denoted here as `phi`,
and that is supplied by the user as the first argument of the
`adapt` function.

Let us denote by `H=Hessian(phi)` the Hessian tensor field
of the scalar field `phi`.
Then, `|H|` denotes the tensor that has the same eigenvector as `H`,
but with absolute value of its eigenvalues:
 
        |H| = Q*diag(|lambda_i|)*Qt
 
The metric `M` is determined from `|H|`.
Recall that an isotropic metric is such that `M(x)=hloc(x)^(-2)*Id`
where `hloc(x)` is the element size field
and `Id` is the identity `d*d` matrix,
and `d=1,2,3` is the physical space dimension.

Gmsh isotropic metric
=====================
 
                   max_(i=0..d-1)(|lambda_i(x)|)*Id
        M(x) = -----------------------------------------
               err*hcoef^2*(sup_y(phi(y))-inf_y(phi(y)))

Notice that the denominator involves a global (absolute) normalization
`sup_y(phi(y))-inf_y(phi(y))` of the criterion field `phi`
and the two parameters `aopt.err`, the target error,
and `aopt.hcoef`, a secondary normalization parameter (defaults to 1).

Bamg anisotropic metric
=======================
There are two approach for the normalization of the metric.
The first one involves a global (absolute) normalization:
 
                               |H(x))|
        M(x) = -----------------------------------------
               err*hcoef^2*(sup_y(phi(y))-inf_y(phi(y)))

The first one involves a local (relative) normalization:

                               |H(x))|
        M(x) = -----------------------------------------
               err*hcoef^2*(|phi(x)|, cutoff*max_y|phi(y)|)
 
Notice that the denominator involves a local value `phi(x)`.
The parameter is provided by the optional variable `aopt.cutoff`;
its default value is `1e-7`.
The default strategy is the local normalization.
The global normalization can be enforced by setting
`aopt.additional="-AbsError"`.

When choosing global or local normalization ?
 
When the governing field `phi` is bounded, 
i.e. when `err*hcoef^2*(sup_y(phi(y))-inf_y(phi(y)))`
will converge versus mesh refinement to a bounded value,
the global normalization defines a metric that is mesh-independent
and thus the adaptation loop will converge.

@addindex corner singularity

Otherwise, when `phi` presents singularities, with unbounded
values (such as corner singularity, i.e. presents picks when represented
in elevation view), then the mesh adaptation procedure
is more difficult. The global normalization
divides by quantities that can be very large  and the mesh adaptation
can diverges when focusing on the singularities.
In that case, the local normalization is preferable.
Moreover, the focus on singularities can also be controlled 
by setting `aopt.hmin` not too small.

The local normalization has been chosen as the default since it is 
more robust. When your field `phi` does not present singularities,
then you can switch to the global numbering that leads to a best
equirepartition of the error over the domain.

Options
=======
@snippet adapt.h verbatim_adapt_option

Implementation
==============
@showfromfile
*/
}// namespace rheolef

#include "rheolef/field.h"

namespace rheolef {

//! @brief adapt_option: see the @ref adapt_3 page for the full documentation
// [verbatim_adapt_option]
struct adapt_option {
    typedef std::vector<int>::size_type size_type;
    std::string generator;
    bool isotropic;
    Float err;
    Float errg;
    Float hcoef;
    Float hmin;
    Float hmax;
    Float ratio;
    Float cutoff;
    size_type n_vertices_max;
    size_type n_smooth_metric;
    bool splitpbedge;
    Float thetaquad;
    Float anisomax;
    bool clean;
    std::string additional;
    bool double_precision;
    Float anglecorner;	// angle below which bamg considers 2 consecutive edge to be part of
    			// the same spline
    adapt_option() :
        generator(""),
	isotropic(true), err(1e-2), errg(1e-1), hcoef(1), hmin(0.0001), hmax(0.3), ratio(0), cutoff(1e-7),
	n_vertices_max(50000), n_smooth_metric(1), 
	splitpbedge(true), thetaquad(std::numeric_limits<Float>::max()),
	anisomax(1e6), clean(false), additional("-RelError"), double_precision(false),
	anglecorner(0)
     {}
};
// [verbatim_adapt_option]

// [verbatim_adapt]
template <class T, class M>
geo_basic<T,M>
adapt (
  const field_basic<T,M>& phi,
  const adapt_option& options = adapt_option());
// [verbatim_adapt]

// backward compatibility:
using adapt_option_type = adapt_option;

}// namespace rheolef
#endif // _RHEO_ADAPT_OPTION_H
