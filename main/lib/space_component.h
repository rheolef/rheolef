#ifndef _RHEOLEF_SPACE_COMPONENT_H
#define _RHEOLEF_SPACE_COMPONENT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef/space.h"

namespace rheolef {

template<class T, class M>
class space_component {
public:

  typedef typename space_basic<T,M>::size_type size_type;

// allocators:

  space_component (space_constitution<T,M>& sc, size_type i_comp)
    : _constit(sc.is_hierarchical() ? sc [i_comp] : sc),
      _i_comp (sc.is_hierarchical() ? _unset      : i_comp)
  {}
  space_component (const space_component<T,M>& x) 
    : _constit(x._constit), _i_comp(x._i_comp) {}

// implicit conversion to space:

  operator space_basic<T,M>() const {
    check_macro (_i_comp == _unset, "space-component building from non-hierarchical basis: not yet supported");
    return space_basic<T,M>(_constit);
  }

// accessor:

  space_component<T,M> operator[] (size_type i_comp) const {
	space_component<T,M> sub (_constit, i_comp);
        return sub;
  }

// modifier:

  void block (std::string dom_name) {
    if (_i_comp  == _unset) {
      _constit.do_act (space_act(dom_name, space_act::block));
    } else {
      _constit.do_act (space_act(dom_name, _i_comp, space_act::block));
    }
  }
  void block (const domain_indirect_basic<M>& dom) {
    if (_i_comp  == _unset) {
      _constit.do_act (space_act(dom.name(), space_act::block));
    } else {
      _constit.do_act (space_act(dom.name(), _i_comp, space_act::block));
    }
  }

protected: 
   static const size_type _unset = std::numeric_limits<size_type>::max();
// data:
   space_constitution<T,M>& _constit; // non-const reference: block() modify the _constit
   size_type                _i_comp;  // used only when vector/tensor basis
};
// ===========================================================================
// const version
// ===========================================================================
template<class T, class M>
class space_component_const {
public:

  typedef typename space_basic<T,M>::size_type size_type;

// allocators:

  space_component_const (const space_constitution<T,M>& sc, size_type i_comp)
    : _constit(sc.is_hierarchical() ? sc [i_comp] : sc),
      _i_comp (sc.is_hierarchical() ? _unset      : i_comp)
  {}
  space_component_const (const space_component<T,M>& x) 
    : _constit(x._constit), _i_comp(x._i_comp) {}

// implicit conversion to space:

  operator space_basic<T,M>() const {
    check_macro (_i_comp == _unset, "space-component building from non-hierarchical basis: not yet supported");
    return space_basic<T,M>(_constit);
  }

// accessor:

#ifdef TODO
  space_component_const<T,M> operator[] (size_type i_comp) const {
	space_component_const<T,M> sub (_constit [i_comp]);
        return sub;
  }
#endif // TODO

protected:
   static const size_type _unset = std::numeric_limits<size_type>::max();
// data:
   const space_constitution<T,M>& _constit;
   size_type                      _i_comp;
};
// ========================================
// inlined
// ========================================
template <class T, class M>
inline
space_component<T,M>
space_base_rep<T,M>::operator[] (size_type i_comp) 
{
  space_component<T,M> Vi (_constit, i_comp);
  return Vi;
}
template <class T, class M>
inline
space_component_const<T,M>
space_base_rep<T,M>::operator[] (size_type i_comp) const
{
  return space_component_const<T,M> (_constit, i_comp);
}

} // namespace rheolef
#endif // _RHEOLEF_SPACE_COMPONENT_H
