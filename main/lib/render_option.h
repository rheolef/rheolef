#ifndef _RHEOLEF_RENDER_OPTION_H
#define _RHEOLEF_RENDER_OPTION_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// manage graphical render options
// e.g. print it in python for the rheolef_paraview.py script
//
// author: Pierre.Saramito@imag.fr
//
// date: 25 janv 2020
//
#include "rheolef/point.h"

namespace rheolef {

// ----------------------------------------------------------------------------
// option for scalar visualization with paraview
// ----------------------------------------------------------------------------
struct render_option {
  render_option();
  friend std::ostream& operator<< (std::ostream& py, const render_option&);
  void put_paraview (std::ostream& py) const;
  static std::string python (const point& x, size_t d=3);
// data:
  mutable bool showlabel, stereo, elevation, iso, cut, grid, fill, volume, view_2d,
               view_map, high_order, color, gray, black_and_white, have_opacity_bug;
  mutable size_t n_isovalue, n_isovalue_negative, branch_size;
  mutable Float scale, f_min, f_max, isovalue;
  mutable point_basic<size_t> resolution;
  mutable point xmin, xmax, origin, normal;
  mutable std::string format, mark, label, valued, style;
};

} // namespace rheolef
#endif  // _RHEOLEF_RENDER_OPTION_H
