%{
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
%}
%union  { 
        int             empty;
        size_t          string_value;
	list_type*	list;
	tree_type*	tree;
}
%type <tree>          all top_expr expr factor 
%type <list>          expr_list
%token<string_value>  IDENTIFIER GEO_NAME
/* TODO : automatically synchronize from gbasis */
/* BEGIN basis */
%type <empty>         basis scalar_name family_name index
%type <empty>         opt_d opt_list non_empty_opt_list opt_valued opt_valued_list non_empty_opt_valued_list
%token<string_value>  INTEGER FAMILY_NO_INDEX VALUED COORDINATE
%token<empty>         D
/* END basis */
%%
all     : top_expr
		{ result_ptr = $1; }
top_expr: factor
	| expr_list
		{ $$ = new_macro(tree_type("mixed",*$1)); delete_macro ($1); }
	;
factor  : basis '{' GEO_NAME '}'
                { 
                  std::string basis_name = basis_rep<Float>::standard_naming (_current_fio.family, _current_fio.index, _current_fio.option);
                  $$ = new_macro (tree_type(basis_name,symbol($3)));
                }
	;
expr_list: expr '*' expr
		{
		  $$ = new_macro (list_type);
		  $$->push_back(*$1); delete_macro ($1);
		  $$->push_back(*$3); delete_macro ($3);
		}
	| expr_list '*' expr
		{ $$ = $1; $$->push_back(*$3); delete_macro ($3); }
	;
expr	: factor
	| '(' expr_list ')'
		{ $$ = new_macro(tree_type("mixed",*$2)); delete_macro ($2); }
	;
/* TODO : automatically synchronize from gbasis */
/* BEGIN basis */
basis   : scalar_name
	| VALUED opt_valued_list '(' scalar_name ')'
		{ _current_fio.option.set_valued_tag (space_constant::valued_tag(symbol($1))); }
        ;
scalar_name: FAMILY_NO_INDEX
		{
		  _current_fio.family = symbol($1);
		  _current_fio.index = 0; /* should be -1...*/
		}
	| family_name index opt_d opt_list
	;
opt_d   : /* empty */
		{ 
		  if (_current_fio.family == "P" && _current_fio.index == 0) {
		    _current_fio.option.set_discontinuous();
                  } else {
		    _current_fio.option.set_continuous();
                  }
                }
	| IDENTIFIER
		{ check_macro (symbol($1) == "d", "unexpected identifier \""<<symbol($1)<<"\"");
		  _current_fio.option.set_discontinuous(); }
	;
family_name : IDENTIFIER
		{ _current_fio.family = symbol($1); }
	;
index  : INTEGER
		{ _current_fio.index = std::atoi (symbol($1).c_str()); }
	;
opt_list: /* empty */
		{ $$ = 0; }
	| '[' non_empty_opt_list ']'
		{ $$ = 0; }
	;
non_empty_opt_list : IDENTIFIER
		{ _current_fio.option.set (symbol($1)); }
	| non_empty_opt_list ',' IDENTIFIER
		{ _current_fio.option.set (symbol($3)); }
	;
opt_valued_list: /* empty */
		{ $$ = 0; }
	| '[' non_empty_opt_valued_list ']'
		{ $$ = 0; }
	;
non_empty_opt_valued_list: opt_valued
	| non_empty_opt_valued_list ',' opt_valued
	;
opt_valued: IDENTIFIER '=' INTEGER
		{
		  check_macro (symbol($1) == "d", "unexpected identifier \""<<symbol($1)<<"\"");
                  size_type d = std::atoi (symbol($3).c_str());
		  check_macro (d <= 3, "basis name: invalid dimension d="<<d<<". HINT: should be <= 3");
                  _current_fio.option.set_dimension (d);
                }
	| COORDINATE
                {
		  space_constant::coordinate_type sys_coord = space_constant::coordinate_system (symbol($1)); 
		  _current_fio.option.set_coordinate_system (sys_coord); 
		}
	;
/* END basis */
%%
