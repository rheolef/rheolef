#ifndef _RHEOLEF_PIOLA_ON_POINTSET_H
#define _RHEOLEF_PIOLA_ON_POINTSET_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// evaluate the Piola transformation on a full pointset,
// e.g. a quadrature node set or an interpolation node set:
//
//	F : hat_K ---> K
//          hat_x +--> x = F(hat_x)
// 
#include "rheolef/geo.h"
#include "rheolef/piola.h"
#include "rheolef/basis_on_pointset.h"
#include "rheolef/integrate_option.h"
namespace rheolef {

// ----------------------------------------------------------------------------
// representation
// ----------------------------------------------------------------------------
template<class T>
class piola_on_pointset_rep {
public: 
  typedef reference_element::size_type size_type;

// allocators:

  piola_on_pointset_rep();

// modifiers:

  void initialize (
    const basis_basic<T>&   piola_basis,
    const quadrature<T>&    quad,
    const integrate_option& iopt);

  void initialize (
    const basis_basic<T>&   piola_basis,
    const basis_basic<T>&   nodal_basis,
    const integrate_option& iopt);

// accessors:
  
  const basis_on_pointset<T>& get_basis_on_pointset() const { return _bops; }
  bool has_quadrature() const { return _bops.has_quadrature(); }
  const quadrature<T>&  get_quadrature()  const { return _bops.get_quadrature(); }
  const basis_basic<T>& get_nodal_basis() const { return _bops.get_nodal_basis(); }
  bool ignore_sys_coord() const { return _ignore_sys_coord; }

  template<class M>
  const Eigen::Matrix<piola<T>,Eigen::Dynamic,1>&
  get_piola (const geo_basic<T,M>& omega, const geo_element& K) const;

  template<class M>
  const Eigen::Matrix<T,Eigen::Dynamic,1>&
  get_weight (const geo_basic<T,M>& omega, const geo_element& K) const;

protected:
// internal:

  template<class M>
  void _update (const geo_basic<T,M>& omega, const geo_element& K) const;

// data:

  basis_on_pointset<T>                                        	_bops;
  bool                                                          _ignore_sys_coord;

// working area:

  mutable std::array<
              std::string
             ,reference_element::max_variant>           	_last_visited_geo;
  mutable std::array<
              size_type
             ,reference_element::max_variant>           	_last_visited_dis_ie;

  mutable std::vector<size_type>                                _dis_inod_K;

  mutable std::array<
              Eigen::Matrix<piola<T>,Eigen::Dynamic,1>
             ,reference_element::max_variant>                   _piola;

  mutable std::array<
              Eigen::Matrix<T,Eigen::Dynamic,1>
             ,reference_element::max_variant>                   _weight;
};
template<class T>
inline
piola_on_pointset_rep<T>::piola_on_pointset_rep()
  : _bops(),
    _ignore_sys_coord(false),
    _last_visited_geo(),
    _last_visited_dis_ie(),
    _dis_inod_K(),
    _piola(),
    _weight()
{
    _last_visited_geo.fill ("");
    _last_visited_dis_ie.fill (std::numeric_limits<size_type>::max());
}
template<class T>
template<class M>
inline
const Eigen::Matrix<piola<T>,Eigen::Dynamic,1>&
piola_on_pointset_rep<T>::get_piola (const geo_basic<T,M>& omega, const geo_element& K) const
{
  _update (omega, K);
  return _piola [K.variant()];
}
template<class T>
template<class M>
inline
const Eigen::Matrix<T,Eigen::Dynamic,1>&
piola_on_pointset_rep<T>::get_weight (const geo_basic<T,M>& omega, const geo_element& K) const
{
  _update (omega, K);
  return _weight [K.variant()];
}
// ----------------------------------------------------------------------------
// interface
// ----------------------------------------------------------------------------
template<class T>
class piola_on_pointset: public smart_pointer<piola_on_pointset_rep<T> > {
public:
  typedef piola_on_pointset_rep<T> rep;
  typedef smart_pointer<rep>       base;
  typedef typename rep::size_type  size_type;

// allocators:

  piola_on_pointset();

// modifiers:

  void initialize (
    const basis_basic<T>&   piola_basis,
    const quadrature<T>&    quad,
    const integrate_option& iopt);

  void initialize (
    const basis_basic<T>&   piola_basis,
    const basis_basic<T>&   nodal_basis,
    const integrate_option& iopt);

// accessors:

  const basis_on_pointset<T>& get_basis_on_pointset() const;
  bool has_quadrature() const;
  const quadrature<T>&  get_quadrature() const;
  const basis_basic<T>& get_nodal_basis() const;

  template<class M>
  const Eigen::Matrix<piola<T>,Eigen::Dynamic,1>&
  get_piola (const geo_basic<T,M>& omega, const geo_element& K) const;

  template<class M>
  const Eigen::Matrix<T,Eigen::Dynamic,1>&
  get_weight (const geo_basic<T,M>& omega, const geo_element& K) const;

  bool ignore_sys_coord() const { return base::data().ignore_sys_coord(); }
};
template<class T>
inline
piola_on_pointset<T>::piola_on_pointset()
 : base(new_macro(rep))
{
}
template<class T>
inline
void
piola_on_pointset<T>::initialize (
    const basis_basic<T>&   piola_basis,
    const quadrature<T>&    quad,
    const integrate_option& iopt)
{
  base::data().initialize (piola_basis, quad, iopt);
}
template<class T>
inline
void
piola_on_pointset<T>::initialize (
    const basis_basic<T>&   piola_basis,
    const basis_basic<T>&   nodal_basis,
    const integrate_option& iopt)
{
  base::data().initialize (piola_basis, nodal_basis, iopt);
}
template<class T>
inline
const basis_on_pointset<T>&
piola_on_pointset<T>::get_basis_on_pointset() const
{
  return base::data().get_basis_on_pointset();
}
template<class T>
inline
bool
piola_on_pointset<T>::has_quadrature() const
{
  return base::data().has_quadrature();
}
template<class T>
inline
const quadrature<T>&
piola_on_pointset<T>::get_quadrature() const
{
  return base::data().get_quadrature();
}
template<class T>
inline
const basis_basic<T>&
piola_on_pointset<T>::get_nodal_basis() const
{
  return base::data().get_nodal_basis();
}
template<class T>
template<class M>
inline
const Eigen::Matrix<piola<T>,Eigen::Dynamic,1>&
piola_on_pointset<T>::get_piola (const geo_basic<T,M>& omega, const geo_element& K) const
{
  return base::data().get_piola (omega, K);
}
template<class T>
template<class M>
inline
const Eigen::Matrix<T,Eigen::Dynamic,1>&
piola_on_pointset<T>::get_weight (const geo_basic<T,M>& omega, const geo_element& K) const
{
  return base::data().get_weight (omega, K);
}

}// namespace rheolef
#endif // _RHEOLEF_PIOLA_ON_POINTSET_H
