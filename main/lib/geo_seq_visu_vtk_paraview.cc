///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// paraview vtk visualization
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 may 1997  update: 23 oct 2011
//
#include "rheolef/geo.h"
#include "rheolef/piola_util.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/iofem.h"

#include "geo_seq_put_vtk.h"

using namespace std;
namespace rheolef { 


// ----------------------------------------------------------------------------
// python utils
// ----------------------------------------------------------------------------
template<class T>
static
std::string
python (const point_basic<T>& x, size_t d=3)
{
    std::ostringstream os;
    os << "[" << x[0];
    for (size_t i = 1; i < d; i++)
      os << ", " << x[i];
    os << "]" << std::flush;
    string buffer = os.str();
    return buffer;
}
// ----------------------------------------------------------------------------
// geo puts
// ----------------------------------------------------------------------------
template <class T>
odiststream&
visu_vtk_paraview (odiststream& ops, const geo_basic<T,sequential>& omega)
{
  //
  // 1) prerequises
  //
  using namespace std;
  typedef typename geo_basic<T,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  ostream& os = ops.os();
  bool verbose  = iorheo::getverbose(os);
  bool clean    = iorheo::getclean(os);
  bool execute  = iorheo::getexecute(os);
  bool fill      = iorheo::getfill(os);    // isocontours or color fill
  bool elevation = iorheo::getelevation(os);
  bool color   = iorheo::getcolor(os);
  bool gray    = iorheo::getgray(os);
  bool black_and_white = iorheo::getblack_and_white(os);
  bool stereo    = iorheo::getstereo(os);
  bool full      = iorheo::getfull(os);
  bool iso       = true;
  bool cut       = iorheo::getcut(os);
  bool volume    = iorheo::getvolume(os);
  bool grid       = iorheo::getgrid(os);
  bool lattice   = iorheo::getlattice(os);
  bool shrink    = iorheo::getshrink(os);
  bool showlabel = iorheo::getshowlabel(os);
  bool tube      = iorheo::gettube(os);
  bool ball      = iorheo::getball(os);
  string format   = iorheo::getimage_format(os);
  if (format == "tiff") format = "tif";
  if (format == "jpeg") format = "jpg";
  string basename = iorheo::getbasename(os);
  point_basic<T>          origin     = iofem::getorigin(os);
  point_basic<T>          normal     = iofem::getnormal(os);
  point_basic<size_type>  resolution = iofem::getresolution(os);
  string outfile_fmt = "";
  string tmp = get_tmpdir() + "/";
  if (!clean) tmp = "";

  size_type dim     = omega.dimension();
  size_type map_dim = omega.map_dimension();
  size_type nv      = omega.sizes().ownership_by_dimension[0].size();
  size_type nedg    = omega.sizes().ownership_by_dimension[1].size();
  size_type nfac    = omega.sizes().ownership_by_dimension[2].size();
  size_type nvol    = omega.sizes().ownership_by_dimension[3].size();
  size_type ne      = omega.sizes().ownership_by_dimension[map_dim].size();

#if (_RHEOLEF_PARAVIEW_VERSION_MAJOR >= 5) && (_RHEOLEF_PARAVIEW_VERSION_MINOR >= 5)
  // paraview version >= 5.5 has high order elements
  bool high_order = omega.order() > 1 && !cut && !shrink;
#else
  bool high_order = false;
#endif
  //
  // 2) output mesh data
  //
  string filelist;
  string filename = tmp+basename + ".vtk";
  filelist = filelist + " " + filename;
  ofstream vtk_os (filename.c_str());
  odiststream vtk (vtk_os);
  if (verbose) clog << "! file \"" << filename << "\" created.\n";
  size_type out_dim = high_order ? std::min (size_type(1),omega.map_dimension()) : omega.map_dimension();
  geo_put_vtk (vtk, omega, omega.get_piola_basis(), omega.get_nodes(), true, out_dim);
  vtk.close();
  //
  // 3) output domains data
  //
  for (size_type idom = 0, ndom = omega.n_domain(); idom < ndom; ++idom) {
      const geo_basic<T,sequential>& dom = omega.get_domain(idom);
      string filename = tmp+basename + "." + dom.name() + ".vtk";
      filelist = filelist + " " + filename;
      ofstream vtk_os_dom (filename.c_str());
      odiststream vtk_dom (vtk_os_dom);
      if (verbose) clog << "! file \"" << filename << "\" created.\n";
      out_dim = high_order && !fill ? std::min (size_type(1),dom.map_dimension()) : dom.map_dimension();
      geo_basic<T,sequential> dom_reduced_edges
         = high_order && !fill && dom.map_dimension() > 1 ?
	   compact(dom) : dom; // strip internal edges in 3D surface domain
      geo_put_vtk (vtk_dom, dom_reduced_edges, dom_reduced_edges.get_piola_basis(), dom_reduced_edges.get_nodes(), true, out_dim);
      vtk_dom.close();
  }
  //
  // 3) create python data file
  //
  std::string py_name = filename = tmp+basename + ".py";
  filelist = filelist + " " + filename;
  ofstream py (filename.c_str());
  if (verbose) clog << "! file \"" << filename << "\" created.\n";
  py << "#!/usr/bin/env paraview --script=" << endl
     << "# This is a paraview script for the visualization of " << basename << ".vtk" << endl
     << "# automatically generated by rheolef."  << endl
     << endl
     ;
  // paraview python library
  py << "from paraview.simple  import *" << endl
     << "from paraview_rheolef import *  # load rheolef specific functions" << endl
     << endl
     ;
  py << "d = [ '" << tmp+basename << "'";
  for (size_type idom = 0, ndom = omega.n_domain(); idom < ndom; ++idom) {
      const geo_basic<T,sequential>& dom = omega.get_domain(idom);;
      py << ", '" << dom.name() << "'";
  }
  py << "]" << endl;

  bool has_origin = (origin[0] != numeric_limits<Float>::max());
  bool view_2d = (omega.xmax()[2] - omega.xmin()[2] == 0);
  point xmin = omega.xmin(),
        xmax = omega.xmax();
  py << "opt = {                        \\" << endl
     << "    'format'  : '" << format   << "',          \\" << endl
     << "    'resolution' : [" << resolution[0] << "," << resolution[1] << "], \\" << endl
     << "    'showlabel': " << showlabel << "," << endl
     << "    'axis'    : 1," << endl
     << "    'view_1d' : 0," << endl
     << "    'view_2d' : " << view_2d << "," << endl
     << "    'color'   : '" << (color ? "color" : (gray ? "gray" : "black_and_white")) << "'," << endl
     << "    'stereo'  : " << stereo << ",          \\" << endl
     << "    'bbox'    : [[" << xmin[0] << "," << xmin[1] << "," << xmin[2] << "],["
                             << xmax[0] << "," << xmax[1] << "," << xmax[2] << "]],  \\" << endl
     << "    'ball'    : " << ball << ",            \\" << endl
     << "    'cut'     : " << cut  << ",            \\" << endl
     << "    'fill'    : " << fill << ",            \\" << endl
     << "    'full'    : " << full << ",            \\" << endl
     << "    'lattice' : " << lattice << ",         \\" << endl
     << "    'shrink'  : " << shrink  << ",         \\" << endl
     << "    'tube'    : " << tube    << ",         \\" << endl
     << "    'volume'  : " << 0       << ",         \\" << endl
     << "    'has_origin' : " << has_origin  << ",  \\" << endl
     << "    'origin'     : " << python(origin) << ", \\" << endl
     << "    'normal'     : " << python(normal) << ", \\" << endl
     << "    'elevation'  : 0,   \\" << endl
     << "    'high_order' : "<< high_order << "   \\" << endl
     << "  }" << endl
     << endl
     ;
  py << "paraview_geo(paraview, opt, d)" << endl
     << endl
     ;
  //
  // 3) run pyton
  //
  int status = 0;
  string command;
  if (execute) {
      string prog = (format == "") ? "paraview --script=" : "pvbatch --use-offscreen-rendering ";
      command = "LANG=C PYTHONPATH=" + string(_RHEOLEF_PKGDATADIR) + " " + prog + py_name;
      if (format != "") command = "DISPLAY=:0.0 " + command;
      if (stereo && format == "") command = command + " --stereo";
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
  //
  // 4) clear vtk data
  //
  if (clean) {
      command = "/bin/rm -f " + filelist;
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
  return ops;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template odiststream& visu_vtk_paraview<Float>  (odiststream&, const geo_basic<Float,sequential>&);

}// namespace rheolef
