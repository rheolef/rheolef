///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// vtk output
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 may 1997  update: 23 oct 2011
//
#include "rheolef/field.h"
#include "rheolef/piola_util.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/field_evaluate.h"
#include "rheolef/space_component.h"
#include "rheolef/diststream.h"
#include "rheolef/interpolate.h"

#include "geo_seq_put_vtk.h"

namespace rheolef { 
using namespace std;

// ----------------------------------------------------------------------------
// low order elements : {P0,P1,P1d}-iso-P1
// when approx = P0 or P1, P1d, paraview is more efficient with it
// than with the general nonlinear Lagrange elements
// ----------------------------------------------------------------------------
template <class T>
odiststream&
put_vtk_scalar_values (odiststream& ods, const field_basic<T,sequential>& uh, std::string name, bool put_header)
{
  typedef typename field_basic<T,sequential>::size_type size_type;
  ostream& vtk = ods.os();
  size_type degree = uh.get_space().get_basis().degree();
  vtk << setprecision(numeric_limits<T>::digits10);
  if (put_header) {
    std::string data_type = (degree == 0) ? "CELL_DATA" : "POINT_DATA";
    vtk << data_type << " " << uh.ndof() << endl;
  }
  vtk << "SCALARS " << name << " float" << endl
      << "LOOKUP_TABLE default"  << endl;
  for (size_type idof = 0, ndof = uh.ndof(); idof < ndof; idof++) {
    vtk << uh.dof(idof) << endl;
  }
  vtk << endl;
  return ods;
}
template <class T>
odiststream&
put_vtk_vector_values (odiststream& ods, const field_basic<T,sequential>& uh, std::string name, bool put_header)
{
  typedef typename field_basic<T,sequential>::size_type size_type;
  ostream& vtk = ods.os();
  // 1. output norm(uh)
  const basis_basic<T>& b_comp = uh.get_space().get_constitution().get_basis()[0];
  space_basic<T,sequential> Xh (uh.get_geo(), b_comp.name());
  field_basic<T,sequential> norm_uh = interpolate (Xh, norm(uh));
  put_vtk_scalar_values (ods, norm_uh, name+"_norm", put_header);
  // 1. output uh components
  vtk << setprecision(numeric_limits<T>::digits10)
      << "VECTORS " << name << " float" << endl;
  size_type n_comp = uh.get_space().get_basis().size();
  for (size_type i_comp_dof = 0, n_comp_dof = uh.ndof()/n_comp; i_comp_dof < n_comp_dof; i_comp_dof++) {
    for (size_type i_comp = 0; i_comp < n_comp; i_comp++) {
      size_type idof = i_comp_dof*n_comp + i_comp;
      vtk << uh.dof (idof);
      if (i_comp != 2) vtk << " ";
    }
    for (size_type i_comp = n_comp; i_comp < 3; i_comp++) {
      vtk << "0";
      if (i_comp != 2) vtk << " ";
    }
    vtk << endl;
  }
  vtk << endl;
  return ods;
}
template <class T>
odiststream&
put_vtk_tensor_values (odiststream& ods, const field_basic<T,sequential>& tau_h, std::string name, bool put_header)
{
  typedef typename field_basic<T,sequential>::size_type size_type;
  ostream& vtk = ods.os();
  const basis_basic<T>& b_comp = tau_h.get_space().get_constitution().get_basis()[0];
  space_basic<T,sequential> Xh (tau_h.get_geo(), b_comp.name());
  field_basic<T,sequential> norm_tau_h = interpolate (Xh, norm(tau_h));
  put_vtk_scalar_values (ods, norm_tau_h, name+"_norm", put_header);
  vtk << setprecision(numeric_limits<T>::digits10)
      << "TENSORS " << name << " float" << endl;
  size_type d = tau_h.get_geo().dimension();
  switch (d) {
    case 1: {
      field_basic<T,sequential> t00 = tau_h(0,0);
      for (size_type idof = 0, ndof = t00.ndof(); idof < ndof; idof++) {
        vtk << t00.dof(idof) << " 0 0" << endl
            << "0 0 0" << endl
            << "0 0 0" << endl;
      }
      break;
    }
    case 2: {
      space_constant::coordinate_type sys_coord = tau_h.get_geo().coordinate_system();
      size_type i_comp00 = space_constant::tensor_index (tau_h.valued_tag(), sys_coord, 0, 0);
      size_type i_comp01 = space_constant::tensor_index (tau_h.valued_tag(), sys_coord, 0, 1);
      size_type i_comp11 = space_constant::tensor_index (tau_h.valued_tag(), sys_coord, 1, 1);
      size_type n_comp = tau_h.get_space().get_basis().size();
      for (size_type i_comp_dof = 0, n_comp_dof = tau_h.ndof()/n_comp; i_comp_dof < n_comp_dof; i_comp_dof++) {
        size_type idof00 = i_comp_dof*n_comp + i_comp00;
        size_type idof01 = i_comp_dof*n_comp + i_comp01;
        size_type idof11 = i_comp_dof*n_comp + i_comp11;
        vtk << tau_h.dof(idof00) << " " << tau_h.dof(idof01) << " 0" << endl
            << tau_h.dof(idof01) << " " << tau_h.dof(idof11) << " 0" << endl
            << "0 0 0" << endl;
      }
      break;
    }
    default: {
      field_basic<T,sequential> t00 = tau_h(0,0);
      field_basic<T,sequential> t01 = tau_h(0,1);
      field_basic<T,sequential> t11 = tau_h(1,1);
      field_basic<T,sequential> t02 = tau_h(0,2);
      field_basic<T,sequential> t12 = tau_h(1,2);
      field_basic<T,sequential> t22 = tau_h(2,2);
      for (size_type idof = 0, ndof = t00.ndof(); idof < ndof; idof++) {
        vtk << t00.dof(idof) << " " << t01.dof(idof) << " " << t02.dof(idof) << endl
            << t01.dof(idof) << " " << t11.dof(idof) << " " << t12.dof(idof) << endl
            << t02.dof(idof) << " " << t12.dof(idof) << " " << t22.dof(idof) << endl;
      }
    }
  }
  return ods;
}
template <class T>
odiststream&
field_put_vtk (odiststream& ods, const field_basic<T,sequential>& uh, std::string name, bool put_geo)
{
  typedef typename field_basic<T,sequential>::size_type size_type;
  // 1) output geo
  basis_basic<T> my_numb  = uh.get_space().get_basis();
  basis_basic<T> my_piola = uh.get_geo().get_piola_basis();
  size_type degree    = my_numb.degree();
  size_type order     = uh.get_space().get_geo().order();
  size_type subdivide = iorheo::getsubdivide (dout.os());
  field_basic<T,sequential> vh;
  if (subdivide == 0) {
    if (degree == 0) {
      // P0
      vh = uh;
      if (put_geo) {
        geo_put_vtk (ods, vh.get_geo(), my_piola, vh.get_geo().get_nodes(), false, vh.get_geo().map_dimension());
      }
    } else if (order <= degree && uh.get_space().get_basis().family_name() == "P") {
      // Pk[d] and order <= degree
      vh = uh;
      if (put_geo) {
        geo_put_vtk (ods, vh.get_geo(), my_numb, vh.get_space().get_xdofs(), false, vh.get_geo().map_dimension());
      }
    } else {
      // not Pk[d] or order > degree 
      // => re-interpolate uh on the finer mesh lattice (eg P1d field on a P2 mesh)
      size_type k = std::max (order, degree);
      std::string approx = "P" + std::to_string(k);
      if (uh.get_space().get_basis().have_compact_support_inside_element()) approx += "d";
      space_basic<T,sequential> Vh (uh.get_geo(), approx, uh.get_space().valued());
      warning_macro ("reinterpolate \""<< approx << "\" since \"" << uh.get_approx() << "\" is not Pk[d] or mesh order > degree");
      vh = interpolate (Vh, uh);
      if (put_geo) {
        geo_put_vtk (ods, vh.get_geo(), vh.get_space().get_basis(), vh.get_space().get_xdofs(), false, vh.get_geo().map_dimension());
      }
    }
  } else { // subdivide > 0
    geo_basic<T,sequential> omega_s;
    omega_s.build_by_subdividing (uh.get_geo(), subdivide);
    space_basic<T,sequential> Vh_s (omega_s, uh.get_space().get_approx());
    vh = interpolate (Vh_s, uh);
    // TODO: interpolate: not efficient (octree...)
    //     + when discontinuous, force continuous junctions
    // => see field_seq_visu_gnuplot.cc for direct output
    if (put_geo) {
      geo_put_vtk (ods, vh.get_geo(), vh.get_space().get_basis(), vh.get_space().get_xdofs(), false, vh.get_geo().map_dimension());
    }
  }
  // 2) output values
  bool put_header = put_geo;
  if (name == "") { name = vh.get_space().valued(); }
  switch (vh.get_space().valued_tag()) {
    case space_constant::scalar: put_vtk_scalar_values (ods, vh, name, put_header); break;
    case space_constant::vector: put_vtk_vector_values (ods, vh, name, put_header); break;
    case space_constant::tensor: put_vtk_tensor_values (ods, vh, name, put_header); break;
    default: error_macro ("put_vtk: do not known how to print " << vh.valued() << "-valued field");
  }
  return ods;
}
// ----------------------------------------------------------------------------
// main call
// ----------------------------------------------------------------------------
template <class T>
odiststream&
field_put_vtk (odiststream& ods, const field_basic<T,sequential>& uh)
{
  return field_put_vtk (ods, uh, "", true);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template odiststream& field_put_vtk<Float>  (odiststream&, const field_basic<Float,sequential>&, std::string, bool);
template odiststream& field_put_vtk<Float>  (odiststream&, const field_basic<Float,sequential>&);

}// namespace rheolef
