///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/adapt.h"
#include "rheolef/form.h"
#include "rheolef/field_wdof_sliced.h"
#include "rheolef/rheostream.h"
#include "rheolef/field_expr.h"

namespace rheolef {

// extern in adapt.cc
template<class T, class M>
field_basic<T,M>
hessian_criterion (
    const field_basic<T,M>&  uh0,
    const adapt_option& opts);

// -----------------------------------------
// adapt
// -----------------------------------------
template<class T, class M>
geo_basic<T,M>
adapt_gmsh (
    const field_basic<T,M>&  uh,
    const adapt_option& opts)
{
trace_macro("adapt_gmsh...");
  using namespace std;
  typedef typename geo_basic<T,M>::size_type size_type;
  bool do_verbose  = iorheo::getverbose(clog);
  bool do_clean    = iorheo::getclean(clog);
  string command, clean_files;
  // -----------------------------------------
  // 1a) sortie du maillage : nom-<i>.geo
  // -----------------------------------------
  const geo_basic<T,M>& omega = uh.get_geo();
  size_type i = omega.serial_number();
  string i_name = omega.name();
  string geo_name = i_name + ".geo";
  odiststream out;
  if (! dis_file_exists(geo_name) &&
      ! dis_file_exists(geo_name + ".gz")) {
    out.open (geo_name, io::nogz);
    out << omega;
    check_macro (out.good(), "adapt: file \"" << geo_name << "\"creation failed");
    out.close();
    clean_files += " " + geo_name;
  }
  // -----------------------------------------
  // 2a) sortie du critere : nom-crit-<i>.field
  // -----------------------------------------
  string crit_name = i_name + "-crit.field";
  clean_files += " " + crit_name + ".gz";
  out.open (crit_name, "field");
  out << hessian_criterion(uh,opts);
  check_macro (out.good(), "adapt: file \"" << crit_name << "\"creation failed");
  out.close();
  // --------------------------------------------
  // 2b) conversion field : nom-crit-<i>.pos
  // --------------------------------------------
  string pos_name = i_name + "-crit.pos";
  clean_files += " " + pos_name;
  string bindir = string(_RHEOLEF_RHEOLEF_LIBDIR) + "/rheolef";
  command = "zcat " + crit_name + ".gz | " + bindir + "/field2gmsh_pos > " + pos_name;
  if (do_verbose) derr << "! " << command << endl;
  check_macro (dis_system (command) == 0, "adapt: unix command failed");
  // ----------------------------------------
  // 3) run gmsh in adapt mode with bamg:
  //     => nom-<i+1>.msh
  // ----------------------------------------
  // TODO: use a "cad" header entry in the .geo file
  // => get "mshcad_file" from omega.cad_filename();
  string mshcad_file = omega.familyname() + ".mshcad";
  check_macro (dis_file_exists(mshcad_file), "adapt: missing \""<<mshcad_file<<"\" file");
  string options  =  "-clmin " + ftos(opts.hmin)
                  + " -clmax " + ftos(opts.hmax)
                  + " -clscale " + ftos(opts.hcoef);
  char buffer [100];
  sprintf (buffer, "%.3d", int(i+1)); // see geo::name() in geo.cc
  string i1_name = omega.familyname() + "-" + buffer;
  size_type d = omega.dimension();
  string algo = (d <= 1) ? "meshadapt" : (d == 2) ? "bamg" : "mmg3d";
  command = "gmsh " + options
          + " " + mshcad_file
          + " -" + std::to_string(d) + " -algo " + algo
          + " -bgm " + pos_name
          + " -format msh2 -o " + i1_name + ".msh";
  if (do_verbose) derr << "! " << command << endl;
  check_macro (dis_system (command) == 0, "adapt: command failed");
  // ----------------------------------------
  // 4) conversion : nom-crit-<i>.msh
  // ----------------------------------------
  command = "msh2geo -" + omega.coordinate_system_name() 
            + " " + i1_name + ".msh | gzip -9 > " + i1_name + ".geo.gz";
  if (do_verbose) derr << "! " << command << endl;
  check_macro (dis_system (command) == 0, "adapt: command failed");
  clean_files += " " + i1_name + ".msh";
  // ----------------------------------------
  // 5) chargement  nom-<i+1>.geo
  // ----------------------------------------
  idiststream in (i1_name, "geo");
  geo_basic<T,M> new_omega;
  in >> new_omega;
  new_omega.set_name (omega.familyname());
  new_omega.set_serial_number (i+1);
  clean_files += " " + i1_name + ".geo";
  clean_files += " " + i1_name + ".geo.gz";
  // ----------------------------------------
  // 6) clean
  // ----------------------------------------
  if (do_clean) {
    command = "rm -f" + clean_files;
    if (do_verbose) derr << "! " << command << endl;
    check_macro (dis_system (command) == 0, "adapt: command failed");
  }
trace_macro("adapt_gmsh done");
  return new_omega;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M) 							\
template geo_basic<T,M> adapt_gmsh (const field_basic<T,M>&, const adapt_option&);

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
