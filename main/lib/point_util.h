#ifndef _RHEOLEF_POINT_UTIL_H
#define _RHEOLEF_POINT_UTIL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// utility: handle for pair(size_t,point)
// 
// formaly, we want to do in distributed ernvironment:
//   mpi::all_reduce (omega.comm(), disarray[dis_i,x], mpi::minimum<pair<size_t,point> >());
// where mpi::minimum<pair<size_t,point> >(a,b) takes min of idx and point coords
//

#include "rheolef/distributor.h"
#include "rheolef/point.h"

// -----------------------------------------------------------
// pair(size_t,point)
// -----------------------------------------------------------
namespace rheolef {

template <class T>
struct id_pt_t : std::pair<size_t,point_basic<T> > {
  typedef std::pair<size_t,point_basic<T> > base;
  id_pt_t () : base() {}
  id_pt_t (size_t dis_i, const point_basic<T>& x) : base(dis_i,x) {}
  template<class Archive>
  void serialize (Archive& ar, const unsigned int version) {
	ar & base::first;
        ar & base::second;
  }
};
} // namespace rheolef

#ifdef _RHEOLEF_HAVE_MPI
// Some serializable types have a fixed amount of data stored at fixed field positions.
// When this is the case, boost::mpi can optimize their serialization and transmission to avoid extraneous copy operations.
// To enable this optimization, we specialize the type trait is_mpi_datatype, e.g.:
namespace boost {
 namespace mpi {
  // TODO: when id_pt_t<T> is not a simple type, such as T=bigfloat or T=gmp, etc
  template <>
  struct is_mpi_datatype<rheolef::id_pt_t<double> > : mpl::true_ { };
 } // namespace mpi
} // namespace boost
#endif // _RHEOLEF_HAVE_MPI

namespace rheolef {

template <class T>
struct id_pt_minimum : public std::binary_function<id_pt_t<T>, id_pt_t<T>, id_pt_t<T> > {
 id_pt_minimum () {}
 id_pt_t<T> operator() (const id_pt_t<T>& a, const id_pt_t<T>& b) {
   size_t         id = std::min(a.first,    b.first);
   point_basic<T> pt  (std::min(a.second[0],b.second[0]),
                       std::min(a.second[1],b.second[1]),
                       std::min(a.second[2],b.second[2]));
   return id_pt_t<T>(id,pt);
  }
};

} // namespace rheolef
// -----------------------------------------------------------
// pair(point,point)
// -----------------------------------------------------------
namespace rheolef {

template <class T>
struct pt2_t : std::pair<point_basic<T>,point_basic<T> > {
  typedef std::pair<point_basic<T>,point_basic<T> > base;
  pt2_t () : base() {}
  pt2_t (const point_basic<T>& x, const point_basic<T>& y) : base(x,y) {}
  template<class Archive>
  void serialize (Archive& ar, const unsigned int version) {
	ar & base::first;
        ar & base::second;
  }
};
} // namespace rheolef

#ifdef _RHEOLEF_HAVE_MPI
// Some serializable types have a fixed amount of data stored at fixed field positions.
// When this is the case, boost::mpi can optimize their serialization and transmission to avoid extraneous copy operations.
// To enable this optimization, we specialize the type trait is_mpi_datatype, e.g.:
namespace boost {
 namespace mpi {
  // TODO: when pt2_t<T> is not a simple type, such as T=bigfloat or T=gmp, etc
  template <>
  struct is_mpi_datatype<rheolef::pt2_t<double> > : mpl::true_ { };
 } // namespace mpi
} // namespace boost
#endif // _RHEOLEF_HAVE_MPI

namespace rheolef {

template <class T>
struct pt2_minimum : public std::binary_function<pt2_t<T>, pt2_t<T>, pt2_t<T> > {
 pt2_minimum () {}
 pt2_t<T> operator() (const pt2_t<T>& a, const pt2_t<T>& b) {
   point_basic<T> pt1 (std::min(a.first [0],b.first [0]),
                       std::min(a.first [1],b.first [1]),
                       std::min(a.first [2],b.first [2]));
   point_basic<T> pt2 (std::min(a.second[0],b.second[0]),
                       std::min(a.second[1],b.second[1]),
                       std::min(a.second[2],b.second[2]));
   return pt2_t<T>(pt1,pt2);
  }
};

} // namespace rheolef


#endif // _RHEOLEF_POINT_UTIL_H
