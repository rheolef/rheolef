#ifndef _RHEOLEF_DOMAIN_INDIRECT_H
#define _RHEOLEF_DOMAIN_INDIRECT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

/*Class:domain_indirect
NAME: @code{domain_indirect} - a named part of a finite element mesh
@cindex  mesh boundary
@clindex domain_indirect
DESCRIPTION:
  @noindent
  The @code{domain_indirect} class defines a container for a part of a
  finite element mesh.
  This describes the connectivity of edges or faces.
  This class is useful for boundary condition setting.
IMPLEMENTATION NOTE:
  The @code{domain} class is split into two parts.
  The first one is the @code{domain_indirect} class, that contains the main
  renumbering features: it acts as a indirect on a @code{geo} class(@pxref{geo class}).
  The second one is the @code{domain} class, that simply contains two
  smart_pointers: one on a @code{domain_indirect} and the second on the
  @code{geo} where renumbering is acting.
  Thus, the domain class develops a complete @code{geo}-like interface, 
  via the @code{geo_abstract_rep} pure virtual class derivation,
  and can be used by the @code{space} class (@pxref{space class}).
  The split between domain_indirect and domain is necessary,
  because the @code{geo} class contains a list of domain_indirect.
  It cannot contains a list of @code{domain} classes, that refers
  to the geo class itself: a loop in reference counting
  leads to a blocking situation in the automatic deallocation.

DATE: 12 may 1997
End:
*/

#include "rheolef/disarray.h"
#include "rheolef/geo_element.h"
#include "rheolef/geo_element_indirect.h"
#include "rheolef/index_set.h"

namespace rheolef {

// foward declarations:
template <class U, class M> class geo_abstract_rep;
template <class U, class M> class geo_rep;
template <class U, class M> class geo_basic;

// ==================================================================================
// 1) domain_indirect
//    representation: domain_indirect_base_rep<M> and domain_indirect_rep<M>
// ==================================================================================
template <class M>
class domain_indirect_base_rep : public disarray<geo_element_indirect,M> {
public:

// typedefs:

    typedef disarray<geo_element_indirect,M> base;
    typedef typename geo_element_indirect::size_type        size_type;
    typedef typename geo_element_indirect::orientation_type orientation_type;
    typedef typename base::iterator                     iterator_ioige;
    typedef typename base::const_iterator               const_iterator_ioige;

// allocators:

    domain_indirect_base_rep();
    void resize (size_type n) { base::resize (n); }

    // build from a table of element indexes
    domain_indirect_base_rep (const std::string& name, size_type map_dim,
	const communicator&           comm,
	const std::vector<size_type>& ie_list);
    void build_from_list (const std::string& name, size_type map_dim,
	const communicator&           comm,
	const std::vector<size_type>& ie_list);

    // c := a union b with c=*this
    void build_union (
        const domain_indirect_base_rep<M>& a,
        const domain_indirect_base_rep<M>& b);

// accessors & modifiers:

    size_type             size()     const { return base::size(); }
    size_type             dis_size() const { return base::dis_size(); }

    const_iterator_ioige ioige_begin() const { return base::begin(); }
    const_iterator_ioige ioige_end()   const { return base::end(); }
          iterator_ioige ioige_begin()       { return base::begin(); }
          iterator_ioige ioige_end()         { return base::end(); }

    const geo_element_indirect& oige (size_type ioige) const { return base::operator[] (ioige); }

    std::string name ()    const { return _name; }
    size_type map_dimension () const { return _map_dim; }
    void set_name (std::string name) { _name = name; }
    void set_map_dimension (size_type map_dim) { _map_dim = map_dim; }
    bool is_broken() const { return _is_broken; }
    void set_broken(bool b) { _is_broken = b; }
protected:
// data:
    std::string           _name;
    size_type             _map_dim;
    bool 		  _is_broken; // for e.g. "sides" or "internal_sides"
};
template <class M>
inline
domain_indirect_base_rep<M>::domain_indirect_base_rep()
 : disarray<geo_element_indirect,M>(),
   _name(),
   _map_dim(0),
   _is_broken(false)
{
}
template<class M>
inline
domain_indirect_base_rep<M>::domain_indirect_base_rep (
    const std::string&            name,
    size_type                     map_dim,
    const communicator&           comm,
    const std::vector<size_type>& ie_list)
 : disarray<geo_element_indirect,M>(),
   _name(),
   _map_dim(),
   _is_broken(false)
{
  build_from_list (name, map_dim, comm, ie_list);
}
// ---------------------------------------------------------------------
template <class M> class domain_indirect_rep {};

template<>
class domain_indirect_rep<sequential> : public domain_indirect_base_rep<sequential> {
public:

// typedefs:

    typedef domain_indirect_base_rep<sequential>  base;
    typedef base::size_type              size_type;
    typedef base::iterator_ioige         iterator_ioige;
    typedef base::const_iterator_ioige   const_iterator_ioige;
    typedef base::orientation_type       orientation_type;

// allocators:

    domain_indirect_rep () : domain_indirect_base_rep<sequential>() {}

    domain_indirect_rep (
        const std::string&            name,
        size_type                     map_dim,
	const communicator&           comm,
	const std::vector<size_type>& ie_list)
     : domain_indirect_base_rep<sequential>(name, map_dim, comm, ie_list) {}

    void resize (size_type n) { base::resize (n); }

    // c := a union b with c=*this
    void build_union (
        const domain_indirect_rep<sequential>& a,
        const domain_indirect_rep<sequential>& b);

// accessors:

    size_type size()       const { return base::size(); }
    size_type dis_size()   const { return base::dis_size(); }

    const_iterator_ioige ioige_begin() const { return base::ioige_begin(); }
    const_iterator_ioige ioige_end()   const { return base::ioige_end(); }
          iterator_ioige ioige_begin()       { return base::ioige_begin(); }
          iterator_ioige ioige_end()         { return base::ioige_end(); }

    const geo_element_indirect& oige (size_type ioige) const {
	return base::oige (ioige); }

    void set_name (std::string name)   { base::set_name(name); }
    void set_map_dimension (size_type map_dim) { base::set_map_dimension(map_dim); }
    std::string name ()    const { return base::name(); }
    size_type map_dimension () const { return base::map_dimension(); }
    bool is_broken() const { return base::is_broken(); }
    void set_broken(bool b) { base::set_broken(b); }

// i/o:

    template<class U>
    void build_from_data (
        const geo_rep<U,sequential>&  omega,
        disarray<geo_element_auto<>,sequential>&
                                      d_tmp,
        std::vector<index_set>*       ball);

    template <class U>
    idiststream& get (
        idiststream&                  ips,
        const geo_rep<U,sequential>&  omega,
        std::vector<index_set>*       ball);

    odiststream& put (odiststream& ops) const;
};
// c := a union b with c=*this
inline
void
domain_indirect_rep<sequential>::build_union (
        const domain_indirect_rep<sequential>& a,
        const domain_indirect_rep<sequential>& b)
{
  base::build_union (a,b);
}
// ---------------------------------------------------------------------
#ifdef _RHEOLEF_HAVE_MPI
template<>
class domain_indirect_rep<distributed> : public domain_indirect_base_rep<distributed> {
public:

// typedefs:

    typedef domain_indirect_base_rep<distributed>          base;
    typedef base::size_type              size_type;
    typedef base::iterator_ioige         iterator_ioige;
    typedef base::const_iterator_ioige   const_iterator_ioige;
    typedef base::orientation_type       orientation_type;

// allocators:

    domain_indirect_rep();

    template<class T>
    domain_indirect_rep (
        const geo_abstract_rep<T,distributed>& omega,
        const std::string&            name,
        size_type                     map_dim,
	const communicator&           comm,
	const std::vector<size_type>& ie_list);

    template<class T>
    void init_ios (const geo_abstract_rep<T,distributed>& omega);

    // c := a union b with c=*this
    template<class T>
    void build_union (
        const geo_basic<T,distributed>& omega,
        const domain_indirect_rep<distributed>& a,
        const domain_indirect_rep<distributed>& b);

// accessors & modifiers:

    size_type size()       const { return base::size(); }
    size_type dis_size()   const { return base::dis_size(); }

    const geo_element_indirect& oige (size_type ioige) const {
	return base::oige (ioige); }

    void set_name (std::string name)   { base::set_name(name); }
    void set_map_dimension (size_type map_dim) { base::set_map_dimension(map_dim); }
    std::string name ()    const       { return base::name(); }
    size_type map_dimension () const       { return base::map_dimension(); }
    bool is_broken() const { return base::is_broken(); }
    void set_broken(bool b) { base::set_broken(b); }

// distributed specific acessors:

    const distributor& ini_ownership () const { return _ini_ioige2dis_ioige.ownership(); }
    size_type ioige2ini_dis_ioige (size_type ioige)     const { return _ioige2ini_dis_ioige [ioige]; }
    size_type ini_ioige2dis_ioige (size_type ini_ioige) const { return _ini_ioige2dis_ioige [ini_ioige]; }

// i/o:

    template <class U>
    idiststream& get (idiststream& ips, const geo_rep<U,distributed>& omega);
    template <class U>
    odiststream& put (odiststream& ops, const geo_rep<U,distributed>& omega) const;

protected:
    template <class U1,class M1> friend class geo_rep; // for geo_rep::build_from_domain
// data:
    disarray<size_type,distributed>        _ioige2ini_dis_ioige;
    disarray<size_type,distributed>        _ini_ioige2dis_ioige;
};
inline
domain_indirect_rep<distributed>::domain_indirect_rep()
  : domain_indirect_base_rep<distributed>(),
    _ioige2ini_dis_ioige(),
    _ini_ioige2dis_ioige()
{
}
template<class T>
inline
domain_indirect_rep<distributed>::domain_indirect_rep (
    const geo_abstract_rep<T,distributed>& omega,
    const std::string&            name,
    size_type                     map_dim,
    const communicator&           comm,
    const std::vector<size_type>& ie_list)
 : domain_indirect_base_rep<distributed>(name, map_dim, comm, ie_list),
   _ioige2ini_dis_ioige(),
   _ini_ioige2dis_ioige()
{
  init_ios (omega);
}
// c := a union b with c=*this
template<class T>
inline
void
domain_indirect_rep<distributed>::build_union (
        const geo_basic<T,distributed>& omega,
        const domain_indirect_rep<distributed>& a,
        const domain_indirect_rep<distributed>& b)
{
  base::build_union (a,b);
  init_ios (omega.data());
}
#endif // _RHEOLEF_HAVE_MPI
// ====================================================================
// 2) wrapper: domain_indirect_basic<M>
// ====================================================================
/// @brief the finite element boundary domain
template <class M = rheo_default_memory_model>
class domain_indirect_basic {
public:
};
typedef domain_indirect_basic<rheo_default_memory_model> domain_indirect;
// ---------------------------------------------------------------------
//<verbatim:
template <>
class domain_indirect_basic<sequential> : public smart_pointer<domain_indirect_rep<sequential> > {
public:

// typedefs:

    typedef domain_indirect_rep<sequential> rep;
    typedef smart_pointer<rep>        base;
    typedef rep::size_type            size_type;
    typedef rep::iterator_ioige       iterator_ioige;
    typedef rep::const_iterator_ioige const_iterator_ioige;

// allocators:

    domain_indirect_basic ();
 
    template <class T>
    domain_indirect_basic (
        const geo_abstract_rep<T,sequential>&  omega,
        const std::string&              name, 
        size_type                       map_dim,
	const communicator&             comm,
	const std::vector<size_type>&   ie_list);

    template <class T>
    domain_indirect_basic (
        const geo_basic<T,sequential>&  omega,
        const std::string&              name, 
        size_type                       map_dim,
	const communicator&             comm,
	const std::vector<size_type>&   ie_list);

    template <class U>
    domain_indirect_basic (
        disarray<geo_element_auto<>,sequential>&
                                        d_tmp,
	const geo_basic<U, sequential>& omega,
	std::vector<index_set>*         ball);

    void resize (size_type n);

// accessors:

    size_type size()     const;
    size_type dis_size() const;
    const distributor& ownership() const;

    const_iterator_ioige ioige_begin() const;
    const_iterator_ioige ioige_end()   const;
          iterator_ioige ioige_begin();
          iterator_ioige ioige_end();

    const geo_element_indirect& oige (size_type ioige) const;

    void set_name (std::string name);
    void set_map_dimension (size_type map_dim);
    std::string name ()    const;
    size_type map_dimension () const;
    bool is_broken() const;
    void set_broken(bool b);

// i/o:

    odiststream& put (odiststream&) const;
    template <class T>
    idiststream& get (idiststream& ips, const geo_rep<T,sequential>& omega, std::vector<index_set> *ball);
};
//>verbatim:
inline
domain_indirect_basic<sequential>::domain_indirect_basic ()
 : smart_pointer<rep> (new_macro(rep))
{
}
template<class T>
inline
domain_indirect_basic<sequential>::domain_indirect_basic (
    const geo_basic<T,sequential>& omega,
    const std::string&            name,
    size_type                     map_dim,
    const communicator&           comm,
    const std::vector<size_type>& ie_list)
 : smart_pointer<rep> (new_macro(rep(name, map_dim, comm, ie_list)))
{
}
template<class T>
inline
domain_indirect_basic<sequential>::domain_indirect_basic (
    const geo_abstract_rep<T,sequential>& omega,
    const std::string&            name,
    size_type                     map_dim,
    const communicator&           comm,
    const std::vector<size_type>& ie_list)
 : smart_pointer<rep> (new_macro(rep(name, map_dim, comm, ie_list)))
{
}
inline
void
domain_indirect_basic<sequential>::resize (size_type n)
{
  return base::data().resize (n);
}
inline
domain_indirect_basic<sequential>::size_type
domain_indirect_basic<sequential>::size() const
{
  return base::data().size();
}
inline
domain_indirect_basic<sequential>::size_type
domain_indirect_basic<sequential>::dis_size() const
{
  return base::data().dis_size();
}
inline
const distributor&
domain_indirect_basic<sequential>::ownership() const
{
  return base::data().ownership();
}
inline
const geo_element_indirect&
domain_indirect_basic<sequential>::oige (size_type ioige) const
{
  return base::data().oige (ioige);
}
inline
domain_indirect_basic<sequential>::const_iterator_ioige
domain_indirect_basic<sequential>::ioige_begin() const
{
  return base::data().ioige_begin();
}
inline
domain_indirect_basic<sequential>::const_iterator_ioige
domain_indirect_basic<sequential>::ioige_end() const
{
  return base::data().ioige_end();
}
inline
domain_indirect_basic<sequential>::iterator_ioige
domain_indirect_basic<sequential>::ioige_begin()
{
  return base::data().ioige_begin();
}
inline
domain_indirect_basic<sequential>::iterator_ioige
domain_indirect_basic<sequential>::ioige_end()
{
  return base::data().ioige_end();
}
inline
std::string
domain_indirect_basic<sequential>::name() const
{
  return base::data().name();
}
inline
domain_indirect_basic<sequential>::size_type
domain_indirect_basic<sequential>::map_dimension() const
{
  return base::data().map_dimension();
}
inline
bool
domain_indirect_basic<sequential>::is_broken() const
{
  return base::data().is_broken();
}
inline
void
domain_indirect_basic<sequential>::set_broken(bool b)
{
  base::data().set_broken(b);
}
inline
void
domain_indirect_basic<sequential>::set_name(std::string name)
{
  return base::data().set_name (name);
}
inline
void
domain_indirect_basic<sequential>::set_map_dimension (size_type map_dim)
{
  return base::data().set_map_dimension (map_dim);
}
inline
odiststream&
domain_indirect_basic<sequential>::put (odiststream& ops) const
{
  return base::data().put (ops);
}
template<class T>
inline
idiststream&
domain_indirect_basic<sequential>::get (
    idiststream&                      ips,
    const geo_rep<T,sequential>&      omega,
    std::vector<index_set>           *ball)
{
  return base::data().template get (ips, omega, ball);
}
// union:
template<class T>
inline
domain_indirect_basic<sequential>
build_union (
  const geo_basic<T,sequential>&           omega,
  const domain_indirect_basic<sequential>& a,
  const domain_indirect_basic<sequential>& b)
{
  domain_indirect_basic<sequential> c;
  c.data().build_union (a.data(), b.data());
  return c;
}
// ---------------------------------------------------------------------
#ifdef _RHEOLEF_HAVE_MPI
//<verbatim:
template <>
class domain_indirect_basic<distributed> : public smart_pointer<domain_indirect_rep<distributed> > {
public:

// typedefs:

    typedef domain_indirect_rep<distributed>  rep;
    typedef smart_pointer<rep>            base;
    typedef rep::size_type                size_type;
    typedef rep::iterator_ioige           iterator_ioige;
    typedef rep::const_iterator_ioige     const_iterator_ioige;

// allocators:

    domain_indirect_basic ();
    template<class T>
    domain_indirect_basic (
        const geo_basic<T,distributed>& omega,
        const std::string&            name,
        size_type                     map_dim,
	const communicator&           comm,
	const std::vector<size_type>& ie_list);

    template<class T>
    domain_indirect_basic (
        const geo_abstract_rep<T,distributed>& omega,
        const std::string&            name,
        size_type                     map_dim,
	const communicator&           comm,
	const std::vector<size_type>& ie_list);

// accessors/modifiers:

    size_type size()     const;
    size_type dis_size() const;
    const distributor& ownership() const;

    const geo_element_indirect& oige (size_type ioige) const;

    void set_name (std::string name);
    void set_map_dimension (size_type map_dim);
    std::string name () const;
    size_type map_dimension () const;
    bool is_broken() const;
    void set_broken(bool b);

// distributed specific acessors:

    const_iterator_ioige ioige_begin() const;
    const_iterator_ioige ioige_end()   const;
          iterator_ioige ioige_begin();
          iterator_ioige ioige_end();

    const distributor& ini_ownership() const;
    size_type ioige2ini_dis_ioige (size_type ioige) const;
    size_type ini_ioige2dis_ioige (size_type ini_ioige) const;

// i/o:

    template <class T>
    idiststream& get (idiststream& ips, const geo_rep<T,distributed>& omega);
    template <class T>
    odiststream& put (odiststream& ops, const geo_rep<T,distributed>& omega) const;
};
//>verbatim:

inline
domain_indirect_basic<distributed>::domain_indirect_basic ()
 : smart_pointer<rep> (new_macro(rep))
{
}
template<class T>
inline
domain_indirect_basic<distributed>::domain_indirect_basic (
    const geo_basic<T,distributed>& omega,
    const std::string&            name,
    size_type                     map_dim,
    const communicator&           comm,
    const std::vector<size_type>& ie_list)
 : smart_pointer<rep> (new_macro(rep(omega.data(), name, map_dim, comm, ie_list)))
{
}
template<class T>
inline
domain_indirect_basic<distributed>::domain_indirect_basic (
    const geo_abstract_rep<T,distributed>& omega,
    const std::string&            name,
    size_type                     map_dim,
    const communicator&           comm,
    const std::vector<size_type>& ie_list)
 : smart_pointer<rep> (new_macro(rep(omega, name, map_dim, comm, ie_list)))
{
}
inline
domain_indirect_basic<distributed>::size_type
domain_indirect_basic<distributed>::size() const
{
  return base::data().size();
}
inline
domain_indirect_basic<distributed>::size_type
domain_indirect_basic<distributed>::dis_size() const
{
  return base::data().dis_size();
}
inline
const distributor&
domain_indirect_basic<distributed>::ownership() const
{
  return base::data().ownership();
}
template<class T>
inline
idiststream&
domain_indirect_basic<distributed>::get (
    idiststream&                     ips,
    const geo_rep<T,distributed>&   omega)
{
  return base::data().template get (ips, omega);
}
template<class T>
inline
odiststream&
domain_indirect_basic<distributed>::put (
    odiststream&                   ops,
    const geo_rep<T,distributed>& omega) const
{
  return base::data().template put (ops, omega);
}
inline
std::string
domain_indirect_basic<distributed>::name() const
{
  return base::data().name();
}
inline
domain_indirect_basic<distributed>::size_type
domain_indirect_basic<distributed>::map_dimension() const
{
  return base::data().map_dimension();
}
inline
void
domain_indirect_basic<distributed>::set_name(std::string name)
{
  return base::data().set_name (name);
}
inline
void
domain_indirect_basic<distributed>::set_map_dimension (size_type map_dim)
{
  return base::data().set_map_dimension (map_dim);
}
inline
bool
domain_indirect_basic<distributed>::is_broken() const
{
  return base::data().is_broken();
}
inline
void
domain_indirect_basic<distributed>::set_broken(bool b)
{
  base::data().set_broken(b);
}
inline
const geo_element_indirect&
domain_indirect_basic<distributed>::oige (size_type ioige) const
{
  return base::data().oige (ioige);
}
inline
domain_indirect_basic<distributed>::const_iterator_ioige
domain_indirect_basic<distributed>::ioige_begin() const
{
  return base::data().ioige_begin();
}
inline
domain_indirect_basic<distributed>::const_iterator_ioige
domain_indirect_basic<distributed>::ioige_end() const
{
  return base::data().ioige_end();
}
inline
domain_indirect_basic<distributed>::iterator_ioige
domain_indirect_basic<distributed>::ioige_begin()
{
  return base::data().ioige_begin();
}
inline
domain_indirect_basic<distributed>::iterator_ioige
domain_indirect_basic<distributed>::ioige_end()
{
  return base::data().ioige_end();
}
inline
const distributor&
domain_indirect_basic<distributed>::ini_ownership() const
{
  return base::data().ini_ownership();
}
inline
domain_indirect_basic<distributed>::size_type
domain_indirect_basic<distributed>::ioige2ini_dis_ioige (size_type ioige) const
{
    return base::data().ioige2ini_dis_ioige (ioige);
}
inline
domain_indirect_basic<distributed>::size_type
domain_indirect_basic<distributed>::ini_ioige2dis_ioige (size_type ini_ioige) const
{
    return base::data().ini_ioige2dis_ioige (ini_ioige);
}
// union:
template<class T>
inline
domain_indirect_basic<distributed>
build_union (
  const geo_basic<T,distributed>& omega,
  const domain_indirect_basic<distributed>& a,
  const domain_indirect_basic<distributed>& b)
{
  domain_indirect_basic<distributed> c;
  c.data().build_union (omega, a.data(), b.data());
  return c;
}

#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
#endif // _RHEOLEF_DOMAIN_INDIRECT_H
