///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// call to parmetis

#include "rheolef/config.h"
#ifdef _RHEOLEF_HAVE_MPI
#include "rheolef/geo_element.h"
#include "rheolef/hack_array.h"

#if defined(_RHEOLEF_HAVE_SCOTCH)
#include "geo_partition_scotch.h"
#elif defined(_RHEOLEF_HAVE_PARMETIS)
#include <parmetis.h>
typedef idx_t idxtype; // change from previous parmetis versions
#endif // _RHEOLEF_HAVE_PARMETIS

namespace rheolef {

disarray<size_t>
geo_mpi_partition (
  const std::array<hack_array<geo_element_hack>,reference_element::max_variant>&
				  ios_geo_element,
  const distributor&              ios_ownership, // by_dimension
  size_t                          map_dim,
  size_t                          dis_nv)
{
  typedef disarray<int>::size_type size_type;
  typedef hack_array<geo_element_hack>::const_iterator const_iterator_by_variant;

  communicator comm = ios_ownership.comm();
  size_type nproc   = comm.size();
  size_type my_proc = comm.rank();

  disarray<size_type> partition (ios_ownership);
  size_type iproc = 0;
  if (partition.dis_size() <= nproc || dis_nv <= nproc) {
    // mesh is very small (nelts <= nproc): parmetis does not support it !
    for (size_type ie = 0, nie = ios_ownership.size(); ie < nie; ie++) {
      partition [ie] = my_proc;
    }
    return partition;
  }
  // here, mesh is not so small: sufficient for parmetis
  disarray<idxtype> part (ios_ownership);
  std::vector<idxtype> elmdist (nproc+1);
  std::copy (
          ios_ownership.begin(),
          ios_ownership.end(),
  	  elmdist.begin());
  std::vector<idxtype> eptr (ios_ownership.dis_size()+1);
  {
    size_type ie = 0;
    eptr [0] = 0;
    for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                   variant < reference_element:: last_variant_by_dimension(map_dim); variant++) {
      for (const_iterator_by_variant iter = ios_geo_element [variant].begin(), last = ios_geo_element [variant].end();
                               iter != last; iter++, ie++) {
        const geo_element& K = *iter;
        eptr [ie+1] = eptr[ie] + K.size();
      }
    }
  }
  std::vector<idxtype> eind (eptr [ios_ownership.size()]);
  std::vector<idxtype>::iterator iter_eind = eind.begin();
  {
    for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                   variant < reference_element:: last_variant_by_dimension(map_dim); variant++) {
      for (const_iterator_by_variant iter = ios_geo_element [variant].begin(), last = ios_geo_element [variant].end();
                               iter != last; iter++) {
        const geo_element& K = *iter;
        for (size_type iloc = 0; iloc < K.size(); iloc++, iter_eind++) {
          *iter_eind = K[iloc];
        }
      }
    }
  }
  idxtype wgtflag = 0;
  idxtype numflag = 0;
  idxtype mgcnum = map_dim;  
  idxtype ncon = 1;  
  idxtype nparts = nproc;  
  std::vector<float> tpwgts (nparts*ncon);
  std::fill (tpwgts.begin(), tpwgts.end(), 1./nparts);
  float ubvec [12];
  std::fill (ubvec, ubvec+ncon, 1.05);
  idxtype options [10];
  options[0] = 1;
  const int pvm3_option_dbglvl = 1;
  const int pvm3_option_seed   = 2;
  options[pvm3_option_dbglvl] = 0; // otherwise: timming print to stdout...
  options[pvm3_option_seed]   = 0;
  idxtype edgecut;
  MPI_Comm raw_comm = comm;
  idxtype *elmwgt = 0; 
#if defined(_RHEOLEF_HAVE_SCOTCH)
  geo_partition_scotch (
        elmdist.begin().operator->(),
	eptr.begin().operator->(),
	eind,
	elmwgt,
	&ncon,
	&mgcnum,
	&nparts, 
  	tpwgts.begin().operator->(),
	ubvec,
	&edgecut,
  	part.begin().operator->(),
	comm);
#elif defined(_RHEOLEF_HAVE_PARMETIS)
  ParMETIS_V3_PartMeshKway(
        elmdist.begin().operator->(),
	eptr.begin().operator->(),
  	eind.begin().operator->(),
	NULL,
	&wgtflag, 
        &numflag,
	&ncon,
	&mgcnum,
	&nparts,
  	tpwgts.begin().operator->(),
	ubvec,
	options,
	&edgecut,
  	part.begin().operator->(),
	&raw_comm);
#else // _RHEOLEF_HAVE_PARMETIS
# error either parmetis nor scotch partitioner founded
#endif // _RHEOLEF_HAVE_PARMETIS

  // convert int to size_type (64 bits : not the same sizeof...)
  std::copy (part.begin(), part.end(), partition.begin());
  return partition;
}

} // namespace rheolef
#endif // _RHEOLEF_HAVE_MPI
