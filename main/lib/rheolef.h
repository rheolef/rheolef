#ifndef _RHEOLEF_H
#define _RHEOLEF_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   14 december 2010

namespace rheolef {
/**
@classfile rheolef reference manual

Description
===========
The reference manual is divided into sections:

-# @ref command_page
-# @ref class_page
-# @ref function_page 
-# @ref linalgclass_page
-# @ref linalgfunction_page
-# @ref femclass_page
-# @ref utilclass_page
-# @ref internalclass_page
-# @ref internalfunction_page

Casual users probably need concern themselves with 
only the three first sections: unix commands and main classes and functions.
Advanced users would be interested by *linalg* (sparse distributed linear algebra)
and utility classes and functions,
while Rheolef's developers would be involved by internal details.

The reference manual is available both on the Rheolef web site
and as individual unix **man(1)** pages.

@ref command_page :

        man geo
        man field

@ref class_page :

	man form
	man problem
	man 2 field

Note that `field` has both a command and class entry:
the `2` section number option allows one to select the class one.
The same occurs for the `geo` and `branch` classes.

@ref function_page :

	man integrate
	man interpolate
	man damped_newton

Finally, for a fully developed table of contents:

	man -k rheolef

Implementation
==============
@showfromfile
*/
} // namespace rheolef

// for simplicity: include alls
#include "rheolef/point.h"
#include "rheolef/tensor.h"
#include "rheolef/tensor3.h"
#include "rheolef/tensor4.h"
#include "rheolef/iorheo.h"
#include "rheolef/diststream.h"
#include "rheolef/linalg.h"
#include "rheolef/cg.h"
#include "rheolef/eye.h"
#include "rheolef/geo.h"
#include "rheolef/geo_domain_indirect.h"
#include "rheolef/geo_domain.h"
#include "rheolef/space_mult.h"
#include "rheolef/space_component.h"
#include "rheolef/field_wdof_indirect.h"
#include "rheolef/field_wdof_sliced.h"
#include "rheolef/field_wdof.icc"
#include "rheolef/field_rdof.icc"
#include "rheolef/field_rdof_node.h"
#include "rheolef/field.h"
#include "rheolef/field_concat.h"
#include "rheolef/field_eigen.h"
#include "rheolef/field_valarray.h"
#include "rheolef/form.h"
#include "rheolef/form_weighted.h"
#include "rheolef/form_concat.h"
#include "rheolef/characteristic.h"
#include "rheolef/test.h"
#include "rheolef/problem_mixed.h"

#include "rheolef/field_expr.h"
#include "rheolef/form_field_expr.h"
#include "rheolef/field_vf_assembly.h"
#include "rheolef/form_vf_assembly.h"

#include "rheolef/field_lazy_node.h"
#include "rheolef/field_lazy_terminal.h"
#include "rheolef/field_lazy_form_mult.h"
#include "rheolef/form_lazy_expr.h"
#include "rheolef/form_lazy_terminal.h"
#include "rheolef/form_lazy_convert.h"

#include "rheolef/interpolate.h"
#include "rheolef/integrate.h"
#include "rheolef/compose.h"
#include "rheolef/round.h"

#include "rheolef/adapt.h"
#include "rheolef/level_set.h"
#include "rheolef/catchmark.h"
#include "rheolef/branch.h"
#include "rheolef/band.h"
#include "rheolef/newton.h"
#include "rheolef/damped_newton.h"
#include "rheolef/limiter.h"
#include "rheolef/continuation.h"

// obsolete: for backward compat.
#include "rheolef/riesz.h"
#include "rheolef/field_functor.h"

#endif // _RHEOLEF_H
