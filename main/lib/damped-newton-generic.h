# ifndef _RHEO_DAMPED_NEWTON_GENERIC_H
# define _RHEO_DAMPED_NEWTON_GENERIC_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/dis_cpu_time.h"
#include "rheolef/newton-backtrack.h"
namespace rheolef { 

//! @brief see the @ref damped_newton_3 page for the full documentation
template <class Problem, class Preconditioner, class Field, class Real, class Size>
int damped_newton (const Problem& P, const Preconditioner& T, Field& u, Real& tol, Size& max_iter, odiststream *p_derr = 0) {
  const Real delta_u_max_factor = 100;
  Real norm_delta_u_max = delta_u_max_factor*std::max(P.space_norm(u), Real(1.));
  Real lambda = 1;
  Real Tu = 0;
  double t0 = dis_wall_time();
  double c0 = dis_cpu_time();
  if (p_derr) *p_derr << "# damped-Newton: n r T lambda wall-time cpu" << std::endl << std::flush;
  for (Size n = 0; true; n++) {
    P.update_derivative (u);
    Field Fu = P.residue(u);
    Field delta_u = -P.derivative_solve (Fu);
    Tu = T(P,Fu,delta_u);
    if (p_derr) *p_derr << n << " " << P.dual_space_norm(Fu) << " " << sqrt(2.*Tu)
	<< " " << lambda << " " << dis_wall_time()-t0 << " " << dis_cpu_time()-c0 << std::endl << std::flush;
    if (2.*Tu <= sqr(tol) || n >= max_iter) {
      max_iter = n;
      tol = sqrt(2.*Tu);
      return 0;
    }
    Real slope = T.slope(P, Fu, delta_u);
    Field u_old = u;
    Real Tu_old = Tu;
    lambda = 1;
    int status = newton_backtrack (
       P, T, u_old, Tu_old, delta_u, slope, norm_delta_u_max, u, Fu, Tu, lambda, p_derr);
    if (status != 0) {
      // check if grad(T)(u) approx 0 ?
      // let T(u) = 0.5*|A*F(u)|_M
      // compute grad(T) = F(u)^T A^T M A F(u)
      // and compare to F(u) : |grad(T)(u)| / |F(u)| > tol ?
      Field Gu = T.grad(P,Fu);
      const Float eps_mach = std::numeric_limits<Float>::epsilon();
      max_iter = n;
      tol = sqrt(2.*Tu);
      if (P.space_norm(Gu) > eps_mach*P.dual_space_norm(Fu)) {
        if (p_derr) *p_derr << "# damped-Newton: warning: machine precision reached" << std::endl << std::flush;
        return 0;
      } else {
        if (p_derr) *p_derr << "# damped-Newton: warning: gradient is zero up to machine precision" << std::endl << std::flush;
        return 1;
      }
    }
  }
  tol = sqrt(2*Tu);
  return 1;
}

struct newton_identity_preconditioner {
  template <class Problem>
  Float operator() (const Problem& P, const typename Problem::value_type& MFv) const {
    return 0.5*sqr(P.dual_space_norm(MFv));
  }
  template <class Problem>
  Float operator() (const Problem& P, const typename Problem::value_type& MFu, const typename Problem::value_type& delta_u) const {
    return operator() (P, MFu);
  }
  template <class Problem>
  typename Problem::value_type grad (const Problem& P, const typename Problem::value_type& MFu) const { 
    return P.derivative_trans_mult (MFu);
  }
  template <class Problem>
  Float slope (const Problem& P, const typename Problem::value_type& MFu, const typename Problem::value_type& delta_u) const { 
    return -sqr(P.dual_space_norm(MFu));
  }
};

}// namespace rheolef
# endif // _RHEO_DAMPED_NEWTON_GENERIC_H
