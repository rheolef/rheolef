///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// Input/Output option management
// for finite element specific objects
//
// author: Pierre.Saramito@imag.fr
//
// date: 6 may 2001
//
# include "rheolef/iofem.h"
namespace rheolef {
using namespace std;

# define IO_RHEO_SCALAR(t,a) iorheobase_io_scalar_body_macro(iofem,t,a)                                
         IO_RHEO_SCALAR (field_sequential, topography)
         IO_RHEO_SCALAR (point, origin)
         IO_RHEO_SCALAR (point, normal)
         IO_RHEO_SCALAR (point_basic<size_t>, resolution)
# undef  IO_RHEO_SCALAR

// ---------------------------------------------------------------------
// constructor and destructor, copy and assignement
// ---------------------------------------------------------------------

iofem::iofem()
 :
    topography_(),
    origin_(point(numeric_limits<Float>::max(),0,0)),
    normal_(point(1,0,0)),
    resolution_(1024,768)
{
}
iofem::~iofem()
{
}
// ---------------------------------------------------------------------
// trivial memory handler instanciation
// ---------------------------------------------------------------------

// static variable initialization in template class:
template<> list<iofem*> *iorheobase_memory_handler<iofem>::pointer_list = 0;

// class instanciation:
template class iorheobase_memory_handler<iofem>;

iofem* 
iofem::get_pointer (std::ios& s)
{
    return iorheobase_memory_handler<iofem>::get_pointer(s);
}

} // namespace rheolef
