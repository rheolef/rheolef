#ifndef _RHEOLEF_VTK_CELL_TYPE_H
#define _RHEOLEF_VTK_CELL_TYPE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// vtk defines
// 
// defined in VTK/Common/DataModel/vtkCellType.h
// but avoid vtk .h dependency at compile time: copy it here
// 
namespace rheolef { 

size_t nv2vtk_cell_type (size_t map_dim, size_t nv);
size_t vtk_cell_type2variant (size_t vtk_cell_type);

#ifndef VTK_EMPTY_CELL
#define VTK_EMPTY_CELL 0
#define VTK_VERTEX 1
#define VTK_POLY_VERTEX 2
#define VTK_LINE 3
#define VTK_POLY_LINE 4
#define VTK_TRIANGLE 5
#define VTK_TRIANGLE_STRIP 6
#define VTK_POLYGON 7
#define VTK_PIXEL 8
#define VTK_QUAD 9
#define VTK_TETRA 10
#define VTK_VOXEL 11
#define VTK_HEXAHEDRON 12
#define VTK_WEDGE 13
#define VTK_PYRAMID 14

// Quadratic, isoparametric cells
#define VTK_QUADRATIC_EDGE 21
#define VTK_QUADRATIC_TRIANGLE 22
#define VTK_QUADRATIC_QUAD 23
#define VTK_QUADRATIC_POLYGON 36
#define VTK_QUADRATIC_TETRA 24
#define VTK_QUADRATIC_HEXAHEDRON 25
#define VTK_QUADRATIC_WEDGE 26
#define VTK_QUADRATIC_PYRAMID 27
#define VTK_BIQUADRATIC_QUAD 28
#define VTK_TRIQUADRATIC_HEXAHEDRON 29
#define VTK_QUADRATIC_LINEAR_QUAD 30
#define VTK_QUADRATIC_LINEAR_WEDGE 31
#define VTK_BIQUADRATIC_QUADRATIC_WEDGE 32
#define VTK_BIQUADRATIC_QUADRATIC_HEXAHEDRON 33
#define VTK_BIQUADRATIC_TRIANGLE 34

// Cubic, isoparametric cell
#define VTK_CUBIC_LINE 35

// Higher order cells in parametric form
#define VTK_PARAMETRIC_CURVE 51
#define VTK_PARAMETRIC_SURFACE 52

// from vtk-recent/common/vtkCellType.h:
#define VTK_LAGRANGE_CURVE 68
#define VTK_LAGRANGE_TRIANGLE 69
#define VTK_LAGRANGE_QUADRILATERAL 70
#define VTK_LAGRANGE_TETRAHEDRON 71
#define VTK_LAGRANGE_HEXAHEDRON 72
#define VTK_LAGRANGE_WEDGE 73
#define VTK_LAGRANGE_PYRAMID 74
#endif // VTK
}// namespace rheolef
#endif // _RHEOLEF_VTK_CELL_TYPE_H
