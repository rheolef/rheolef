//
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/fem_on_pointset.h"
#include "rheolef/piola_util.h"

namespace rheolef {

// ----------------------------------------------------------------------------
// initializers
// ----------------------------------------------------------------------------
template<class T>
void
fem_on_pointset_rep<T>::initialize (
    const basis_basic<T>&        fem_basis,
    const piola_on_pointset<T>&  pops)
{
  _pops = pops;
  if (pops.has_quadrature()) {
    _bops.set (pops.get_quadrature(),  fem_basis);
  } else {
    _bops.set (pops.get_nodal_basis(), fem_basis);
  }
}
// ----------------------------------------------------------------------------
// internal: general evaluators
// ----------------------------------------------------------------------------
// partial specialization of member class functions are not allowed,
// do, use a class-function with partial specialization:
template <class T, class M, class Value, details::differentiate_option::type Diff>
struct evaluate_internal {
  void operator() (
    const fem_on_pointset_rep<T>&                       obj,
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) 
  {
    fatal_macro("unexpected call");
  }
};
template <class T, class M, class Value, details::differentiate_option::type Diff>
struct evaluate_on_side_internal {
  void operator() (
    const fem_on_pointset_rep<T>&                       obj,
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) 
  {
    fatal_macro("unexpected call");
  }
};
// ----------------------------------------------------------------------------
// evaluators
// ----------------------------------------------------------------------------
// post-evaluation: apply a Piola transformation, e.g. for RTk
template<class T>
template<class M, class Value>
void
fem_on_pointset_rep<T>::_evaluate_post_piola (
  const geo_basic<T,M>&                                     omega_K,
  const geo_element&                                        K,
  const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& hat_phij_xi,
        Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
{
  const piola_fem<T>& pf = _bops.get_basis().get_piola_fem();
  if (! pf.transform_need_piola()) {
    // e.g. Pk element: do not need transformation
    value = hat_phij_xi;
    return;
  }
  // e.g. RTk element: need a specific Piola transformation
  const Eigen::Matrix<piola<T>,Eigen::Dynamic,1>& piola = _pops.get_piola (omega_K, K);
  size_type loc_nnod = hat_phij_xi.rows();
  size_type loc_ndof = hat_phij_xi.cols();
  value.resize (loc_nnod, loc_ndof);
  for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
    for (size_type loc_jdof = 0; loc_jdof < loc_ndof; ++loc_jdof) {
      const Value& hat_u = hat_phij_xi (loc_inod, loc_jdof);
            Value&     u =       value (loc_inod, loc_jdof);
      pf.transform (piola[loc_inod], hat_u, u);
    }
  }
}
template <class T, class M, class Value>
struct evaluate_internal<T,M,Value,details::differentiate_option::none> {
  void operator() (
    const fem_on_pointset_rep<T>&                       obj,
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& hat_phij_xi = obj._bops.template evaluate<Value> (K);
    obj._evaluate_post_piola (omega_K, K, hat_phij_xi, value);
  }
};
template <class T, class M, class Value>
struct evaluate_on_side_internal<T,M,Value,details::differentiate_option::none> {
  void operator() (
    const fem_on_pointset_rep<T>&                       obj,
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& hat_phij_xi = obj._bops.template evaluate_on_side<Value> (K, sid);
    obj._evaluate_post_piola (omega_K, K, hat_phij_xi, value);
  }
};
// ----------------------------------------------------------------------------
// grad-evaluators
// ----------------------------------------------------------------------------
// partial specialization of member class functions are not allowed,
// do, use a full function specialization:
template<class T, class M, class Value, class GradValue>
static
void
grad_evaluate_post_piola (
  const fem_on_pointset_rep<T>&				  obj,
  const geo_basic<T,M>&                                   omega_K,
  const geo_element&                                      K,
  const details::differentiate_option&                    gopt,
  const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
                                                          hat_phij_xi,
  const Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic>&
                                                          hat_grad_phij_xi,
  Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic>& value)
{
  using size_type = typename fem_on_pointset_rep<T>::size_type;
  reference_element hat_K = K;
  const Eigen::Matrix<piola<T>,Eigen::Dynamic,1>& piola = obj._pops.get_piola (omega_K, K);
  const piola_fem<T>& pf = obj._bops.get_basis().get_piola_fem();
  size_type loc_nnod = hat_grad_phij_xi.rows();
  size_type loc_ndof = hat_grad_phij_xi.cols();
  value.resize (loc_nnod,loc_ndof);
  for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
    for (size_type loc_jdof = 0; loc_jdof < loc_ndof; ++loc_jdof) {
      const Value&          hat_u =      hat_phij_xi (loc_inod, loc_jdof);
      const GradValue& hat_grad_u = hat_grad_phij_xi (loc_inod, loc_jdof);
            GradValue&     grad_u =            value (loc_inod, loc_jdof);
      pf.grad_transform (piola[loc_inod], hat_u, hat_grad_u, gopt, grad_u);
    }
  }
}
template<class T, class M>
static
void
grad_evaluate_post_piola (
  const fem_on_pointset_rep<T>&				  obj,
  const geo_basic<T,M>&                                   omega_K,
  const geo_element&                                      K,
  const details::differentiate_option&                    gopt,
  const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
                                                          hat_grad_phij_xi,
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&         value)
{
  fatal_macro("grad_evaluate: undefined scalar-valued grad");
}
template <class T, class M, class GradValue>
struct evaluate_internal<T,M,GradValue,details::differentiate_option::gradient> {
  void operator() (
    const fem_on_pointset_rep<T>&                           obj,
    const geo_basic<T,M>&                                   omega_K,
    const geo_element&                                      K,
    const details::differentiate_option&                    gopt,
    Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic>& value)
  {
    reference_element hat_K = K;
    typedef typename space_constant::rank_down<GradValue>::type Value;
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
        hat_phij_xi      = obj._bops.template evaluate<Value> (hat_K);
    const Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic>&
        hat_grad_phij_xi = obj._bops.template grad_evaluate<GradValue> (hat_K);
    grad_evaluate_post_piola (obj, omega_K, K, gopt, hat_phij_xi, hat_grad_phij_xi, value);
  }
};
template <class T, class M>
struct evaluate_internal<T,M,T,details::differentiate_option::gradient> {
  void operator() (
    const fem_on_pointset_rep<T>&                       obj,
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&     value)
  {
    fatal_macro ("undefined scalar-valued grad");
  }
};
template <class T, class M, class GradValue>
struct evaluate_on_side_internal<T,M,GradValue,details::differentiate_option::gradient> {
  void operator() (
    const fem_on_pointset_rep<T>&                           obj,
    const geo_basic<T,M>&                                   omega_K,
    const geo_element&                                      K,
    const side_information_type&                            sid,
    const details::differentiate_option&                    gopt,
    Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic>& value)
  {
    reference_element hat_K = K;
    typedef typename space_constant::rank_down<GradValue>::type Value;
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
        hat_phij_xi      = obj._bops.template evaluate_on_side<Value> (hat_K,sid);
    const Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic>&
        hat_grad_phij_xi = obj._bops.template grad_evaluate_on_side<GradValue> (hat_K, sid);
    grad_evaluate_post_piola (obj, omega_K, K, gopt, hat_phij_xi, hat_grad_phij_xi, value);
  }
};
template <class T, class M>
struct evaluate_on_side_internal<T,M,T,details::differentiate_option::gradient> {
  void operator() (
    const fem_on_pointset_rep<T>&                           obj,
    const geo_basic<T,M>&                                   omega_K,
    const geo_element&                                      K,
    const side_information_type&                            sid,
    const details::differentiate_option&                    gopt,
    Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&         value)
  {
    fatal_macro("grad_evaluate_on_side: undefined scalar-valued grad");
  }
};
// ----------------------------------------------------------------------------
// div evaluator
// ----------------------------------------------------------------------------
// partial specialization of member class functions are not allowed,
// do, use a full function specialization:
template<class T, class M, class Value, class GradValue>
static
void
div_evaluate_internal (
  const fem_on_pointset_rep<T>&				         obj,
  const geo_basic<T,M>&                                          omega_K,
  const geo_element&                                             K,
  const details::differentiate_option&                           gopt,
  const Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic>&  grad_value,
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&            value)
{
  // TODO: vector-valued div: from a tensor3 grad of a tensor2: div(sigma) -> vector
  fatal_macro("undefined "<<space_constant::valued_name(space_constant::valued_tag_traits<Value>::value)
     <<"-valued div operator");
}
// scalar-valued div:
template <class T, class M>
void
div_evaluate_internal (
  const fem_on_pointset_rep<T>&				               obj,
  const geo_basic<T,M>&                                                omega_K,
  const geo_element&                                                   K,
  const details::differentiate_option&                                 gopt,
  const Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,Eigen::Dynamic>&  grad_value,
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&                      value)
{
  using size_type = typename fem_on_pointset_rep<T>::size_type;
  value.resize (grad_value.rows(), grad_value.cols());
  for (size_type loc_inod = 0, loc_nnod = value.rows(); loc_inod < loc_nnod; ++loc_inod) {
  for (size_type loc_jdof = 0, loc_ndof = value.cols(); loc_jdof < loc_ndof; ++loc_jdof) {
    value (loc_inod,loc_jdof) = tr (grad_value(loc_inod,loc_jdof));
  }}
}
template <class T, class M, class Value>
struct evaluate_internal<T,M,Value,details::differentiate_option::divergence> {
  void operator() (
    const fem_on_pointset_rep<T>&                           obj,
    const geo_basic<T,M>&                                   omega_K,
    const geo_element&                                      K,
    const details::differentiate_option&                    gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&     value) const
  {
    typedef typename space_constant::rank_up<Value>::type     UpValue;
    typedef typename space_constant::rank_up<UpValue>::type GradValue;
    Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic> grad_value;
    obj.template evaluate<M,GradValue,details::differentiate_option::gradient> (omega_K, K, gopt, grad_value);
    div_evaluate_internal (obj, omega_K, K, gopt, grad_value, value);
  }
};
template <class T, class M, class Value>
struct evaluate_on_side_internal<T,M,Value,details::differentiate_option::divergence> {
  void operator() (
    const fem_on_pointset_rep<T>&                           obj,
    const geo_basic<T,M>&                                   omega_K,
    const geo_element&                                      K,
    const side_information_type&                            sid,
    const details::differentiate_option&                    gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&     value) const
  {
    typedef typename space_constant::rank_up<Value>::type     UpValue;
    typedef typename space_constant::rank_up<UpValue>::type GradValue;
    Eigen::Matrix<GradValue,Eigen::Dynamic,Eigen::Dynamic> grad_value;
    obj.template evaluate_on_side<M,GradValue,details::differentiate_option::gradient> (omega_K, K, sid, gopt, grad_value);
    div_evaluate_internal (obj, omega_K, K, gopt, grad_value, value);
  }
};
// ----------------------------------------------------------------------------
// curl evaluator
// ----------------------------------------------------------------------------
// partial specialization of member class functions are not allowed,
// do, use a full function specialization:
template<class T, class M, class Value>
static
void
curl_evaluate_internal (
  const fem_on_pointset_rep<T>&				  obj,
  const geo_basic<T,M>&                                   omega_K,
  const geo_element&                                      K,
  const details::differentiate_option&                    gopt,
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&     value)
{
  fatal_macro("undefined "<<space_constant::valued_name(space_constant::valued_tag_traits<Value>::value)
     <<"-valued curl operator");
}
// scalar-valued curl: for 2D geometry
template <class T, class M>
void
curl_evaluate_internal (
  const fem_on_pointset_rep<T>&				  obj,
  const geo_basic<T,M>&                                   omega_K,
  const geo_element&                                      K,
  const details::differentiate_option&                    gopt,
  Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&         value)
{
  using size_type = typename fem_on_pointset_rep<T>::size_type;
  Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,Eigen::Dynamic> value_grad;
  obj.template evaluate<M,tensor_basic<T>,details::differentiate_option::gradient> (omega_K, K, gopt, value_grad);
  value.resize (value_grad.rows(), value_grad.cols());
  for (size_type loc_inod = 0, loc_nnod = value.rows(); loc_inod < loc_nnod; ++loc_inod) {
  for (size_type loc_jdof = 0, loc_ndof = value.cols(); loc_jdof < loc_ndof; ++loc_jdof) {
    const tensor_basic<T>& g = value_grad (loc_inod,loc_jdof);
    value (loc_inod,loc_jdof) = g(1,0) - g(0,1);
  }}
}
// vector-valued curl:
//   for 3D geometry and when argument is vector-valued
// & for 2D geometry and when argument is scalar-valued
template <class T, class M>
void
curl_evaluate_internal (
  const fem_on_pointset_rep<T>&				  obj,
  const geo_basic<T,M>&                                   omega_K,
  const geo_element&                                      K,
  const details::differentiate_option&                    gopt,
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,Eigen::Dynamic>&         value)
{
  using size_type = typename fem_on_pointset_rep<T>::size_type;
  size_type d = omega_K.dimension();
  check_macro (d >= 2, "curl: unexpected dimension d="<<d<<" for mesh \""<<omega_K.name()<<"\"");
  if (d == 2) {
    // argument is scalar-valued:
    // curl(w) = [dw/dx1 ; - dw/dx0] = [g1; -g0] when g=grad(v)=dw/dxi is a vector
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,Eigen::Dynamic> value_grad;
    obj.template evaluate<M,point_basic<T>,details::differentiate_option::gradient> (omega_K, K, gopt, value_grad);
    value.resize (value_grad.rows(), value_grad.cols());
    for (size_type loc_inod = 0, loc_nnod = value.rows(); loc_inod < loc_nnod; ++loc_inod) {
    for (size_type loc_jdof = 0, loc_ndof = value.cols(); loc_jdof < loc_ndof; ++loc_jdof) {
      const point_basic<T>& g = value_grad (loc_inod,loc_jdof);
            point_basic<T>& c = value      (loc_inod,loc_jdof);
      c[0] =  g[1];
      c[1] = -g[0];
    }}
    space_constant::coordinate_type sys_coord = omega_K.coordinate_system();
    if (! obj._pops.ignore_sys_coord() && sys_coord != space_constant::cartesian && ! gopt.batchelor_curl) {
      // v = [vz,vr] = curl(w) = [dw/dr + w/r ; - dw/dz] : add w/r to the z-component
      // => add the specific vz += wr/r term in the axi case
      // Batchelor curl: do not perform this add
      size_type i_comp_r     = (sys_coord == space_constant::axisymmetric_rz) ? 0 : 1;
      size_type i_comp_z     = (sys_coord == space_constant::axisymmetric_rz) ? 1 : 0;
      size_type i_comp_theta = 2;
      reference_element hat_K = K;
      // TODO: BUG_PIOLA_RT_AXI for non-Pk basis that requires Piola : use obj.evaluate (omega_K, K, value_w); instead
      const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
        hat_phij_xi = obj._bops.template evaluate<T> (hat_K);
      const Eigen::Matrix<piola<T>,Eigen::Dynamic,1>& piola = obj._pops.get_piola (omega_K, K);
      size_type loc_nnod = value.rows();
      size_type loc_ndof = value.cols();
      for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
        T r = piola[loc_inod].F[i_comp_r];
        check_macro (1+abs(r) != 1, "curl(): singular axisymmetric (1/r) weight (HINT: avoid interpolate() or change quadrature formulae)");
        // pb: r==0 when using gauss_lobatto, e.g. for the characteristic method
        for (size_type loc_jdof = 0; loc_jdof < loc_ndof; ++loc_jdof) {
          const T& w = hat_phij_xi (loc_inod,loc_jdof);
          point_basic<T>& v = value(loc_inod,loc_jdof);
          v [i_comp_z] += w/r;
        }
      }
    }
  } else { // d == 3
    // d=3: arg is vector:
    //           [dw2/dx1 -dw1/dx2]   [ g21 - g12 ]
    // curl(w) = [dw0/dx2 -dw2/dx0] = [ g02 - g20 ] when g=grad(w)=dwi/dxj is a tensor 
    //           [dw1/dx0 -dw0/dx1]   [ g10 - g01 ]
    Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,Eigen::Dynamic> value_grad;
    obj.template evaluate<M,tensor_basic<T>,details::differentiate_option::gradient> (omega_K, K, gopt, value_grad);
    value.resize (value_grad.rows(), value_grad.cols());
    for (size_type loc_inod = 0, loc_nnod = value.rows(); loc_inod < loc_nnod; ++loc_inod) {
    for (size_type loc_jdof = 0, loc_ndof = value.cols(); loc_jdof < loc_ndof; ++loc_jdof) {
      const tensor_basic<T>& g = value_grad (loc_inod,loc_jdof);
            point_basic<T>&  c = value      (loc_inod,loc_jdof);
      c[0] = g(2,1) - g(1,2);
      c[1] = g(0,2) - g(2,0);
      c[2] = g(1,0) - g(0,1);
    }}
  } 
}
template <class T, class M, class Value>
struct evaluate_internal<T,M,Value,details::differentiate_option::curl> {
  void operator() (
    const fem_on_pointset_rep<T>&                           obj,
    const geo_basic<T,M>&                                   omega_K,
    const geo_element&                                      K,
    const details::differentiate_option&                    gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&     value) const
  {
    curl_evaluate_internal (obj, omega_K, K, gopt, value);
  }
};
template <class T, class M, class Value>
struct evaluate_on_side_internal<T,M,Value,details::differentiate_option::curl> {
  void operator() (
    const fem_on_pointset_rep<T>&                           obj,
    const geo_basic<T,M>&                                   omega_K,
    const geo_element&                                      K,
    const side_information_type&                            sid,
    const details::differentiate_option&                    gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&    value) const
  {
    fatal_macro("evaluate curl on side: not yet");
  }
};
// ----------------------------------------------------------------------------
// main call to evaluators
// ----------------------------------------------------------------------------
template <class T>
template <class M, class Value, details::differentiate_option::type Diff>
void
fem_on_pointset_rep<T>::evaluate (
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
{
  evaluate_internal<T,M,Value,Diff> eval;
  eval (*this, omega_K, K, gopt, value);
}
template <class T>
template <class M, class Value, details::differentiate_option::type Diff>
void
fem_on_pointset_rep<T>::evaluate_on_side (
    const geo_basic<T,M>&                               omega_K,
    const geo_element&                                  K,
    const side_information_type&                        sid,
    const details::differentiate_option&                gopt,
    Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
{
  evaluate_on_side_internal<T,M,Value,Diff> eval;
  eval (*this, omega_K, K, sid, gopt, value);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)	 					\
template class fem_on_pointset_rep<T>;						\

#define _RHEOLEF_instanciation_value(T,M,Value,Diff)	 			\
template void fem_on_pointset_rep<T>::evaluate<M,Value,Diff> (			\
  const geo_basic<T,M>&                               omega_K,			\
  const geo_element&                                  K,			\
  const details::differentiate_option&                gopt,			\
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;		\
template void fem_on_pointset_rep<T>::evaluate_on_side<M,Value,Diff> (		\
  const geo_basic<T,M>&                               omega_K,			\
  const geo_element&                                  K,			\
  const side_information_type&                        sid,			\
  const details::differentiate_option&                gopt,			\
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const;		\

#define _RHEOLEF_instanciation_values(T,M,Diff)					\
_RHEOLEF_instanciation_value(T,M,T,Diff)			 		\
_RHEOLEF_instanciation_value(T,M,point_basic<T>,Diff)	 			\
_RHEOLEF_instanciation_value(T,M,tensor_basic<T>,Diff)	 			\
_RHEOLEF_instanciation_value(T,M,tensor3_basic<T>,Diff)	 			\
_RHEOLEF_instanciation_value(T,M,tensor4_basic<T>,Diff)	 			\

#define _RHEOLEF_instanciation_evaluate(T,M)					\
_RHEOLEF_instanciation_values(T,M,details::differentiate_option::none)		\
_RHEOLEF_instanciation_values(T,M,details::differentiate_option::gradient)	\
_RHEOLEF_instanciation_values(T,M,details::differentiate_option::divergence)	\
_RHEOLEF_instanciation_values(T,M,details::differentiate_option::curl)		\

_RHEOLEF_instanciation(Float)
_RHEOLEF_instanciation_evaluate(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation_evaluate(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
