///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// helper for vtk file io
//
// author: Pierre.Saramito@imag.fr
//
// 29 january 2020
//
#include "rheolef/reference_element.h"

#include "vtk_cell_type.h"

namespace rheolef {

size_t vtk_cell_type2variant (size_t vtk_cell_type)
{
  switch (vtk_cell_type) {
    case VTK_VERTEX:
      return reference_element::p;
    case VTK_LINE: 
    case VTK_QUADRATIC_EDGE: 
    case VTK_CUBIC_LINE: 
    case VTK_LAGRANGE_CURVE: 
      return reference_element::e;
    case VTK_TRIANGLE:
    case VTK_QUADRATIC_TRIANGLE:
    case VTK_LAGRANGE_TRIANGLE:
      return reference_element::t;
    case VTK_QUAD:
    case VTK_BIQUADRATIC_QUAD:
    case VTK_LAGRANGE_QUADRILATERAL:
      return reference_element::t;
    case VTK_TETRA: 
    case VTK_QUADRATIC_TETRA: 
    case VTK_LAGRANGE_TETRAHEDRON: 
      return reference_element::T;
    case VTK_WEDGE: 
    case VTK_BIQUADRATIC_QUADRATIC_WEDGE: 
    case VTK_LAGRANGE_WEDGE: 
      return reference_element::P;
    case VTK_HEXAHEDRON:
    case VTK_TRIQUADRATIC_HEXAHEDRON:
    case VTK_LAGRANGE_HEXAHEDRON:
      return reference_element::H;
    case VTK_QUADRATIC_QUAD: // incomplete order 2 variants
    case VTK_QUADRATIC_WEDGE: 
    case VTK_QUADRATIC_HEXAHEDRON:
    default:
      error_macro ("unsupported vtk cell type = "<<vtk_cell_type);
  }
}
size_t nv2vtk_cell_type (size_t map_dim, size_t nv) 
{
  switch (map_dim) {
    case 0: {
      return VTK_VERTEX;
    }
    case 1: {
      check_macro(nv == 2, "unexpected 1D vtk cell with "<<nv<<" vertices");
      return VTK_LINE;
    }
    case 2: {
      check_macro(nv == 3, "unexpected 2D vtk cell with "<<nv<<" vertices");
      return VTK_TRIANGLE;
    }
    default: {
      error_macro("unexpected "<<map_dim<<"D vtk cell");
      return 0;
    }
  }
}

}// namespace rheolef
