///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/branch.h"
namespace rheolef {
using namespace std;

// --------------------------------------------------------------------------
// cstor
// --------------------------------------------------------------------------
template <class T, class M>
branch_basic<T,M>::~branch_basic() 
{
  if (_p_data_out) delete_macro (_p_data_out);
  _p_data_out = 0;
}
// --------------------------------------------------------------------------
// input
// --------------------------------------------------------------------------
template <class T, class M>
void
branch_basic<T,M>::get_header_rheolef (idiststream& in)
{
  if (_header_in_done) return;
  _header_in_done = true;
  check_macro (dis_scatch(in,"\nbranch"), "input stream does not contains a branch");
  size_type version;
  size_type sz;
  int       nval;
  in >> version >> sz >> nval >> _parameter_name;
  _n_value = ((nval == -1) ? numeric_limits<size_type>::max() : nval);
  base::resize(sz);
  for (size_type i = 0; i < sz; i++) {
    in >> base::operator[](i).first;
  }
}
template <class T, class M>
void
branch_basic<T,M>::get_event_rheolef (idiststream& in)
{
    get_header_rheolef (in);
    if (!in || !dis_scatch(in, "#"+parameter_name())) {
	// no more events
	return;
    }
    T value;
    in >> value;
    if (!in) {
	// no more events...
	return;
    }
    set_parameter(value);
    for (typename branch_basic<T,M>::size_type i = 0; in && i < base::size(); i++) {
       in >> catchmark (base::operator[](i).first) >> base::operator[](i).second;
    }
    // increment counter
    if (_count_value == numeric_limits<typename branch_basic<T,M>::size_type>::max()) {
        _count_value = 0;
    } else {
        _count_value++;
    }
}
template <class T>
void
get_header (idiststream& in, branch_basic<T,sequential>& b)
{
  iorheo::flag_type format  = iorheo::flags(in.is()) & iorheo::format_field;
       if (format [iorheo::vtk])      { get_header_vtk      (in,b); }
  else if (format [iorheo::rheo])     { b.get_header_rheolef(in); }
  else                                { error_macro ("unsupported input format="<<format); }
}
template <class T>
void
get_event (idiststream& in, branch_basic<T,sequential>& b)
{
  iorheo::flag_type format  = iorheo::flags(in.is()) & iorheo::format_field;
       if (format [iorheo::vtk])      { get_event_vtk      (in,b); }
  else if (format [iorheo::rheo])     { b.get_event_rheolef(in); }
  else                                { error_macro ("unsupported input format="<<format); }
}
#ifdef _RHEOLEF_HAVE_MPI
template <class T>
void
get_header (idiststream& in, branch_basic<T,distributed>& b)
{
  b.get_header_rheolef (in);
}
template <class T>
void
get_event (idiststream& in, branch_basic<T,distributed>& b)
{
  b.get_event_rheolef (in);
}
#endif // _RHEOLEF_HAVE_MPI
template <class T, class M>
idiststream& 
operator>> (idiststream& in, branch_basic<T,M>& b) 
{
  get_event (in,b);
  return in;
}
// --------------------------------------------------------------------------
// output
// --------------------------------------------------------------------------
template <class T, class M>
void
branch_basic<T,M>::put_header_rheolef (odiststream& out) const
{
    int nval = -1;
    if (_n_value != numeric_limits<size_type>::max()) nval = _n_value;
    out << "#!branch" << endl
        << endl
        << "branch" << endl
        << "1 " << base::size() << " " << nval << endl
        << _parameter_name;
        ;
    for (size_type i = 0; i < base::size(); i++) {
        out << " " << base::operator[](i).first;
    }
    out << endl;
}
template <class T>
void
put_header (odiststream& out, const branch_basic<T,sequential>& b)
{
    iorheo::flag_type format  = iorheo::flags(out.os()) & iorheo::format_field;
    if      (format [iorheo::gnuplot])  { put_header_gnuplot  (out, b); }
    else if (format [iorheo::paraview]) { put_header_paraview (out,b); }
    else if (format [iorheo::vtk])      { put_header_vtk      (out,b); }
    else if (format [iorheo::rheo])     { b.put_header_rheolef(out); }
    else                                { error_macro ("unsupported output/render format="<<format); }
}
#ifdef _RHEOLEF_HAVE_MPI
template <class T>
void
put_header (odiststream& out, const branch_basic<T,distributed>& b)
{
  b.put_header_rheolef (out);
}
#endif // _RHEOLEF_HAVE_MPI
template <class T, class M>
void
branch_basic<T,M>::put_header (odiststream& out) const
{
  if (_header_out_done) return;
  _header_out_done = true;
  rheolef::put_header (out, *this);
}
template <class T, class M>
void 
branch_basic<T,M>::put_event_rheolef (odiststream& out) const
{
    out << endl
        << setprecision(numeric_limits<T>::digits10)
        << "#" << _parameter_name << " " << _parameter_value << endl
        << endl
       ;
    for (typename branch_basic<T,M>::size_type i = 0; i < base::size(); i++) {
       out << catchmark (base::operator[](i).first) << base::operator[](i).second; 
    }
    out.flush(); // when gziped output, make it available as soon as computed
}
template <class T>
void
put_event (odiststream& out, const branch_basic<T,sequential>& b)
{
    b.put_header(out);
    iorheo::flag_type format  = iorheo::flags(out.os()) & iorheo::format_field;
    // increment counter
         if (format [iorheo::gnuplot]) { put_event_gnuplot  (out,b); }
    else if (format [iorheo::vtk])     { put_event_vtk      (out,b); }
    else if (format [iorheo::paraview]){ put_event_paraview (out,b); }
    else		               { b.put_event_rheolef(out); }
}
#ifdef _RHEOLEF_HAVE_MPI
template <class T>
void
put_event (odiststream& out, const branch_basic<T,distributed>& b)
{
  b.put_header(out);
  b.put_event_rheolef (out);
}
#endif // _RHEOLEF_HAVE_MPI
template <class T, class M>
odiststream& 
operator<< (odiststream& out, const branch_basic<T,M>& b)
{
  rheolef::put_event (out, b);
  return out;
}
template <class T, class M>
void
branch_basic<T,M>::put_finalize_rheolef (odiststream& out) const
{
}
template <class T>
void
put_finalize (odiststream& out, const branch_basic<T,sequential>& b)
{
  if (b._finalize_out_done) return;
  b._finalize_out_done = true;
    iorheo::flag_type format  = iorheo::flags(out.os()) & iorheo::format_field;

         if (format [iorheo::gnuplot])  { put_finalize_gnuplot  (out,b); }
    else if (format [iorheo::vtk])      { put_finalize_vtk      (out,b); }
    else if (format [iorheo::paraview]) { put_finalize_paraview (out,b); }
    else		                { b.put_finalize_rheolef(out); }
}
#ifdef _RHEOLEF_HAVE_MPI
template <class T>
void
put_finalize (odiststream& out, const branch_basic<T,distributed>& b)
{
}
#endif // _RHEOLEF_HAVE_MPI
template <class T, class M>
void
branch_basic<T,M>::put_finalize (odiststream& out) const
{
  rheolef::put_finalize (out, *this);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define rheolef_instanciate(T,M) \
  template class branch_basic<T,M>; \
  template void get_header (idiststream&, branch_basic<T,M>&); \
  template void get_event  (idiststream&, branch_basic<T,M>&); \
  template odiststream& operator<< (odiststream&, const branch_basic<T,M>&); \
  template idiststream& operator>> (idiststream&, branch_basic<T,M>&); 

rheolef_instanciate(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
rheolef_instanciate(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI
#undef rheolef_instanciate

}// namespace rheolef
