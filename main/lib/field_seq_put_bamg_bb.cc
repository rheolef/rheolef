///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// bamg .bb output, for mesh adaptation
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 may 1997  update: 23 oct 2011
//
#include "rheolef/field.h"
#include "rheolef/piola_util.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/field_evaluate.h"
#include "rheolef/space_component.h"
#include "rheolef/field_expr.h"

namespace rheolef { 
using namespace std;

template <class T>
odiststream&
field_put_bamg_bb (odiststream& ods, const field_basic<T,sequential>& uh)
{
  typedef typename field_basic<T,sequential>::size_type size_type;
  ostream& os = ods.os();
  check_macro (uh.valued() == "scalar", uh.valued() << "-valued: not yet");
  os << "2 1 " << uh.ndof() << " 2" << endl;
  for (typename field_basic<T,sequential>::const_iterator iter = uh.begin_dof(), last = uh.end_dof();
	iter != last; ++iter) {
    os << *iter << endl;
  }
  return ods;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template odiststream& field_put_bamg_bb<Float>  (odiststream&, const field_basic<Float,sequential>&);

}// namespace rheolef
