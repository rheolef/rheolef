\subsection{The linear elasticity problem}
\label{sec-elasticity-dg}
\pbindex{elasticity}%
\cindex{benchmark!embankment}%

The elasticity problem \eqref{eq-elasticity}
has been introduced in volume~1, section~\ref{sec-elasticity}, page~\pageref{sec-elasticity}.

\ \ $(P)$: \emph{find ${\bf u}$ such that}

\begin{eqnarray*}
  -{\bf div}\left(\lambda {\rm div}({\bf u}).I + 2D({\bf u})\right) &=& {\bf f} \ \mbox{ in }\ \Omega \\
  {\bf u} &=& {\bf g} \ \mbox{ on }\ \partial\Omega
\end{eqnarray*}
where $\lambda\geq -1$ is a constant
and ${\bf f},{\bf g}$ given.
This problem is a natural extension to vector-valued field
of the Poisson problem with Dirichlet boundary conditions.

The variational formulation writes:

\ \ $(FV)_h$: \emph{find ${\bf u}\in {\bf V}({\bf g})$ such that}
\begin{eqnarray*}
  a({\bf u},{\bf v}) &=& l_h({\bf v}), \ \forall {\bf v}\in {\bf V}(0)
\end{eqnarray*}
where
\begin{eqnarray*}
  {\bf V}({\bf g}) &=& \{ {\bf v}\in H^1(\Omega)^d; \ {\bf v}={\bf g}\ \mbox{ on }\ \partial\Omega \}
	\\
  a({\bf u},{\bf v})
    &=&
    \int_\Omega
      \left(
          \lambda\, {\rm div}({\bf u})\,{\rm div}({\bf v})
	+ 2D({\bf u})\!:\!D({\bf v})
      \right)
      \, {\rm d}x
    \\
  l({\bf v})
    &=& 
    \int_\Omega
      {\bf f}.{\bf v}
      \, {\rm d}x
\end{eqnarray*}
%
The discrete variational formulation writes:

\ \ $(FV)_h$: \emph{find $u_h\in {\bf X}_h$ such that}
\begin{eqnarray*}
  a_h(u_h,v_h) &=& l_h(v_h), \ \forall v_h\in {\bf X}_h
\end{eqnarray*}
where
\begin{eqnarray*}
  {\bf X}_h &=& \{ {\bf v}_h \in L^2(\Omega)^d; {\bf v}_{h|K}\in P_k^d, \ \forall K \in \mathcal{T}_h \}
	\\
  a_h({\bf u},{\bf v})
    &=&
    \int_\Omega
      \left(
          \lambda\, {\rm div}_h({\bf u})\,{\rm div}_h({\bf v})
	+ 2D_h({\bf u})\!:\!D_h({\bf v})
      \right)
      \, {\rm d}x
    \\
    &+&
    \sum_{S\in\mathscr{S}_h}
    \int_S
      \left(
	  \beta\varpi_s \jump{\bf u}.\jump{\bf v}
	- \jump{\bf u}.\average{\lambda {\rm div}_h({\bf v}){\bf n} + 2D_h({\bf v}){\bf n}}
	- \jump{\bf v}.\average{\lambda {\rm div}_h({\bf u}){\bf n} + 2D_h({\bf u}){\bf n}}
      \right)
      \, {\rm d}s
    \\
  l_h({\bf v})
    &=& 
    \int_\Omega
      {\bf f} . {\bf v}
      \, {\rm d}x
    +
    \int_{\partial\Omega}
      {\bf g}.
      \left(
	  \beta\varpi_s {\bf v}
	- \lambda {\rm div}_h({\bf v}){\bf n}
        - 2D_h({\bf v}){\bf n}
      \right)
      \, {\rm d}s
\end{eqnarray*}
where $k\geq 1$ is the polynomial degree in ${\bf X}_h$.
% where $\Gamma_D$ is the part of the boundary where homogeneous Dirichlet condition
% are imposed. On the rest of the boundary $\Gamma_N=\partial\Omega \backslash \Gamma_D$,
% homogeneous Neumann condition are imposed.

% ---------------------------------------
\myexamplelicense{elasticity_taylor_dg.cc}
% ---------------------------------------

\subsubsection*{Comments}

The data are given when $d=2$ by:
\begin{equation}
  {\bf g}(x) =
   \left( \begin{array}{r}
	- \cos(\pi x_0)\sin(\pi x_1) \\
	  \sin(\pi x_0)\cos(\pi x_1)
   \end{array} \right)
   \ \ \mbox{ and }\ \  
  {\bf f} = 2 \pi^2 {\bf g}
  \label{eq-taylor-benchmark}
\end{equation}
This choice is convenient since the exact
solution is known ${\bf u}={\bf g}$.
This benchmark solution was proposed by \citet{Tay-1923}
in the context of the Stokes problem.
Note that the solution is independent of $\lambda$ since ${\rm div}({\bf u})=0$.

% ---------------------------------------
\myexamplelicense{taylor.h}
\myexamplenoinput{elasticity_taylor_error_dg.cc}
\myexamplenoinput{taylor_exact.h}
% ---------------------------------------

As the exact solution is known, the error can be computed.
The code \code{elasticity_taylor_error_dg.cc}
and its header file~\code{taylor_exact.h}
compute the error in $L^2$, $L^\infty$ and energy norms.
These files are not listed here but are
available in the \Rheolef\  example directory.
The computation writes:
\begin{verbatim}
  make elasticity_taylor_dg elasticity_taylor_error_dg
  mkgeo_grid -t 10 > square.geo
  ./elasticity_taylor_dg square P1d | ./elasticity_taylor_error_dg
  ./elasticity_taylor_dg square P2d | ./elasticity_taylor_error_dg
\end{verbatim}

% TODO: embankment
% % ------------------------------
% \subsubsection*{Comments}
% % ------------------------------
% The code is similar to \code{dirichlet_dg.cc} and \code{neumann_dg.cc}:
% it combines homogenous Dirichlet and Neumann boundary conditions in
% a vector-valued function context.
% The \code{div_h} and \code{Dh} operators are the natual extensions
% of the boken gradient: these operators extends the \code{div}
% and \code{D} operators to the broken Sobolev spaces.
% The $\Gamma_D$ boundary depends on the physical dimension:
% it is returned by an auxilliary routine:
% 
% % ---------------------------------------
% \myexamplelicense{embankment_dg.icc}
% % ---------------------------------------
% 
% \cindex{domain concatenation}%
% Observe the union of the boundary domains of mesh
% by using the \code{+} operator.
% The whole boundary domain $\Gamma_D$ is also concatenated
% with the set of internal sides $\mathcal{S}_h^{(i)}$ 
% during the assembly process in the code~\code{embankment_dg.cc}.
% 
