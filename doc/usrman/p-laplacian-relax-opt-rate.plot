set terminal cairolatex pdf color standalone
set output "p-laplacian-relax-opt-rate.tex"

set size square 
set colors classic
set xrange [1:8]
set yrange [0:2.5]
set xtics 1
set ytics (0,0.5,1,1.5,2,2.5)
set xlabel '[c]{\large $p$}'
set  label '[r]{\large $\bar{v}_\textrm{opt}$}' at graph -0.05,0.95 
# -----------------------------------
# pas facile de fitter v_opt avec p...
# -----------------------------------
# fit p > 2
# -----------------------------------
f2(x) = a2*x+b2
#a2  = -2
#b2  = 2
#fit f2(x) 'p-laplacian-relax-opt.gdat' i 1 u (log10($1-2)):3 via a2,b2
a2              = -0.736747   #    +/- 0.03095      (4.2%)
b2              = 0.594568    #    +/- 0.01861      (3.13%)
a2 = -0.75
b2 = 0.75
#set log x
#plot \
#  'p-laplacian-relax-opt.gdat' i 1 u ($1-2):3 t '[r]{computed}' w lp pt 7 lc 1 lw 2, \
#  f2(log10(x))
#pause -1 "<retour>"
#unset log x
# -----------------------------------
# fit p < 2
# -----------------------------------
f1(x) = a1*x+b1
#a1 = 1
#b1 = 1
#fit f1(x) 'p-laplacian-relax-opt.gdat' i 0 u (log10(2-$1)):3 via a1,b1
a1              = -1.03982   #     +/- 0.03676      (3.535%)
b1              = 0.224917   #     +/- 0.02539      (11.29%)
a1 = -1
b1 = 0
#plot [0:]\
#  'p-laplacian-relax-opt.gdat' i 0 u (2-$1):3 t '[r]{computed}' w lp pt 7 lc 2 lw 2, \
#  f1(log10(x))
#pause -1 "<retour>"
# -----------------------------------
# all p
# -----------------------------------
# set samples 1000
# f(p) = (p < 2) ? f1(log10(2-p)) : f2(log10(p-2)) 
# plot f(x) t '[r]{fit $\bar{v}_\textrm{opt}$}' w l lc 0 lt 1 lw 2
plot [1:8][0:3] \
  'p-laplacian-rate-opt.gdat' \
	u 1:2 t '[r]{$\bar{v}$ when $\omega=\omega_\textrm{opt}$}' w l lt 1 lc 3 lw 2, \
  'p-laplacian-rate.gdat'  \
	t '[r]{$\bar{v}$ when $\omega=1$\ \ \ \ }' w l lc 1 lt 1 lw 2
#pause -1 "<retour>"
