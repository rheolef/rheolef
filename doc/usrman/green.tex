\section{How to write a variational formulation~?}
\label{sec-green}%
\cindex{Green formula}%

The major key-point for using \Rheolef\  is to put the problem in variational form.
Then this variational form can be efficiently translated into \code{C++} language.
This appendix is dedicated to readers who are not fluent with
variational formulations and some related functional analysis tools.

% ----------------------------------------------
\subsection{The Green formula}
% ----------------------------------------------
\label{sec-green-scalar}%
Let us come back to the model problem presented in section~\ref{sec-dirichlet}, page~\pageref{sec-dirichlet},
equations~\eqref{eq-dirichlet-1}-\eqref{eq-dirichlet-2} and
details how this problem is transformed into \eqref{eq-dirichlet-fv}.

Let $H^1_0(\Omega)$ the space of functions whose gradient square has a 
finite sum over $\Omega$ and that vanishes on $\partial\Omega$:
\[
	H^1_0(\Omega) 
	=
	\{ v\in L^2(\Omega); \ \nabla v\in L^2(\Omega)^d
	  \mbox{ and } v = 0 \mbox{ on } \partial\Omega \}
\]
We start by multiplying~\eqref{eq-dirichlet-1} by an arbitrarily
test-function $v\in H^1_0(\Omega)$ and then integrate over~$\Omega$~:
\[
	- \int_\Omega \Delta u \, v\, {\rm d}x 
	=
	\int_\Omega f \, v\, {\rm d}x 
	, \ \ \forall v\in H^1_0(\Omega)
\]
The next step is to invoke an integration by part,
the so-called Green formula:
\[
	\int_\Omega \Delta u \, v\, {\rm d}x 
	+
	\int_\Omega \nabla u . \nabla v\, {\rm d}x 
	=
	\int_{\partial \Omega} \Frac{\partial u}{\partial n} \, v\, {\rm d}s 
	, \ \ \forall u,v\in H^1(\Omega)
\]
Since our test-function $v$ vanishes on the boundary, the integral over
$\partial\Omega$ is zero and the problem becomes:
\[
	\int_\Omega \nabla u . \nabla v\, {\rm d}x 
	=
	\int_\Omega f \, v\, {\rm d}x 
	, \ \ \forall v\in H^1_0(\Omega)
\]
This is exactly the variational formulation~\eqref{eq-dirichlet-fv},
page~\pageref{eq-dirichlet-fv}.

% ----------------------------------------------
\subsection{The vectorial Green formula}
% ----------------------------------------------
\label{sec-green-vector}%
In this section, we come back to the linear elasticity problem
presented in section~\ref{sec-elasticity}, page~\pageref{sec-elasticity},
equations~\eqref{eq-elasticity-cauchy}-\eqref{eq-elasticity} and
details how this problem is transformed into \eqref{eq-elasticity-fv}.

Let~$\Gamma_d$ (resp.~$\Gamma_n$) denotes the parts of the 
boundary $\partial\Omega$ related to
the homogeneous Dirichlet boundary condition ${\bf u}=0$
(resp. the homogeneous Neumann boundary condition $\sigma({\bf u})\,{\bf n}=0$).
We suppose that \mbox{$\partial\Omega= \overline{\Gamma}_d\cap \overline{\Gamma}_n$}.
Let us introduce the following functional space:
\[
  {\bf V} = \{ {\bf v}\in H^1(\Omega)^d; \ {\bf v}=0 \mbox{ on }\Gamma_d \}
\]
Then, multiplying the first equation of~\eqref{eq-elasticity}
by an arbitrarily test-function ${\bf v}\in {\bf V}$ and then integrate over~$\Omega$~:
\[
	-\int_\Omega {\bf div}(\sigma({\bf u})).{\bf v}\, {\rm d}x
	=
	\int_\Omega {\bf f}.{\bf v}\, {\rm d}x
	, \ \ \forall {\bf v}\in {\bf V}
\]
The next step is to invoke an integration by part:
\[
	\int_\Omega {\bf div}\,\tau.{\bf v}\, {\rm d}x
	+
        \int_\Omega \tau : D({\bf v})\, {\rm d}x
	=
	\int_{\partial\Omega} \tau : ({\bf v}\otimes {\bf n}) \, {\rm d}s
	, \ \ \forall \tau\in L^2(\Omega)^{d\times d}
	, \ \forall {\bf v}\in {\bf V}
\]
Recall that ${\bf div}\,\tau$ denotes 
$\left(\sum_{j=0}^{d-1} \partial_j \tau_{i,j}\right)_{0\leq i < d}$, i.e. 
the vector whose component are the divergence of each row of $\tau$.
Also, $\sigma : \tau$ denote the double contracted product
$\sum_{i,j=0}^{d-1} \sigma_{i,j}\tau_{i,j}$ for any tensors $\sigma$ and $\tau$,
and that ${\bf u}\otimes {\bf v}$ dotes the $\tau_{i,j}=u_i\,v_j$ tensor,
vectors ${\bf u}$ and ${\bf v}$.
Remark that
\mbox{$
  \tau:({\bf u}\otimes{\bf v})
 = (\tau\,{\bf v}).{\bf u}
 = \sum_{i,j=0}^{d-1} \tau_{i,j} \, u_i \, v_j
$}.
Choosing $\tau=\sigma({\bf u})$ in the previous equation leads to:
\[
        \int_\Omega \sigma({\bf u}) : D({\bf v})\, {\rm d}x
	=
	\int_{\partial\Omega} (\sigma({\bf u})\,{\bf n}) . {\bf v} \, {\rm d}s
        +
	\int_\Omega {\bf f}.{\bf v}\, {\rm d}x
	, \ \ \forall {\bf v}\in {\bf V}
\]
Since our test-function $v$ vanishes on $\Gamma_d$ and
the solution satisfies the homogeneous Neumann boundary
condition $\sigma({\bf u})\,{\bf n}=0$ on $\Gamma_n$, the integral over
$\partial\Omega$ is zero and the problem becomes:
\[
        \int_\Omega \sigma({\bf u}) : D({\bf v})\, {\rm d}x
	=
	\int_\Omega {\bf f}.{\bf v}\, {\rm d}x
	, \ \ \forall {\bf v}\in {\bf V}
\]
From the definition of $\sigma({\bf u})$ in~\eqref{eq-elasticity-cauchy}
page~\pageref{eq-elasticity-cauchy} we have:
\begin{eqnarray*}
   \sigma({\bf u}) : D({\bf v})
  &=& \lambda \,{\rm div}({\bf u})\, (I:D({\bf v}))     + 2\mu D({\bf u}):D({\bf v})
	\\
  &=& \lambda \,{\rm div}({\bf u})\, {\rm div}({\bf v}) + 2\mu D({\bf u}):D({\bf v})
\end{eqnarray*}
and the previous relation becomes:
\[
        \int_\Omega 
  		\lambda 
		{\rm div}({\bf u})\, {\rm div}({\bf v})
        	\, {\rm d}x
        +
	\int_\Omega 
		2\mu
		D({\bf u}):D({\bf v})
        	\, {\rm d}x
	=
	\int_\Omega {\bf f}.{\bf v}\, {\rm d}x
	, \ \ \forall {\bf v}\in {\bf V}
\]
This is exactly the variational formulation~\eqref{eq-elasticity-fv},
page~\pageref{eq-elasticity-fv}.
% ----------------------------------------------
\subsection{The Green formula on a surface}
% ----------------------------------------------
\label{sec-green-surface}%
Let $\Gamma$ a closed and orientable surface of $\mathbb{R}^d$,
$d=2,3$ and ${\bf n}$ its unit normal.
From~\citep{LaaMisSar-2010}, appendix~C we have the following integration by part:
\[
   \int_\Gamma {\rm div}_s {\bf v} \, \xi \, {\rm d}s
   +
   \int_\Gamma {\bf v} . \nabla_s \xi \, {\rm d}s
   =
   \int_\Gamma {\bf v}.{\bf n} \, \xi \, {\rm div}\,{\bf n} \, {\rm d}s
\]
for all $\xi \in H^1(\Gamma)$ and ${\bf v} \in H^1(\Gamma)^d$.
\cindex{geometry!surface!curvature}%
Note that ${\rm div}\,{\bf n}$ represent the surface curvature.
Next, we choose ${\bf v}=\nabla_s \varphi$, for any $\varphi \in H^2(\Gamma)$.
Remaking that ${\bf v}.{\bf n} = 0$
and that
   ${\rm div}_s {\bf v} = \Delta_s \varphi$.
Then:
\[
   \int_\Gamma \Delta_s \, \xi \, {\rm d}s
   +
   \int_\Gamma \nabla_s \varphi . \nabla_s \xi \, {\rm d}s
   =
   0
\]
This formula is the starting point for all variational formulations
of problems defined on a surface (see chapter~\ref{sec-surface}).
