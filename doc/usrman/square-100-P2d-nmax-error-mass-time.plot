set terminal cairolatex pdf color standalone
set output "square-100-P2d-nmax-error-mass-time.tex"

gdat = 'square-100-P2d-nmax-error-mass-time.gdat'
#pi = acos(-1.)
print "pi=",pi

set log xy

set size square
set xrange [1e-3:1]

plot \
gdat \
  u (4.*pi/$1):2 \
  t 'err-l1' \
  w lp, \
gdat \
  u (4.*pi/$1):3 \
  t 'v-mass' \
  w lp

#pause -1
