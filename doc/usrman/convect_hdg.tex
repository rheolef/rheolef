% -----------------------------------------------------------------------
\subsection{Stationary diffusion-convection}
% -----------------------------------------------------------------------
We now turn to the diffusion-convection problem:

\ \ $(P)$: \emph{find $\phi$, defined in $\Omega$, such that}
\begin{eqnarray*}
        {\rm div}(\boldsymbol{u}\phi - \nu \nabla \phi) + \sigma \phi &=& f
                \ \mbox{ in } \Omega
                \\
        \phi &=& \phi_\Gamma(t)
                \ \mbox{ on } \partial\Omega
\end{eqnarray*}
where $\boldsymbol{u}$, $\nu>0$, $\sigma \geq 0$ and $\phi_\Gamma$ being known.
Note the additional $\boldsymbol{u}.\nabla$ operator.
A similar time-dependent version of this problem has been studied
in section~\ref{sec-diffusion-convection},
  page~\pageref{sec-diffusion-convection}.
Without loss of generality, we reduce here to a stationary problem:
the time-dependent case could easily be implemented from the stationary
problem e.g. by with ab implicit Euler scheme or any BFM method.

Following the methodology developped in the previous paragraph,
let us introduce the gradient $\boldsymbol{q}=\nabla u$ as an independent variable.
The problem writes equivalently (see e.g.~\cite{NguPerCoc-2011}):

\ \ $(P)$: \emph{find $\boldsymbol{q}$ and $u$, defined in $\Omega$, such that}
\begin{subequations}
\begin{eqnarray}
  \nu^{-1}\boldsymbol{q} - \nabla \phi &=& 0 
	\ \mbox{ in } \ \Omega
	\label{eq-convect-hdg-q}
	\\
  {\rm div}(\boldsymbol{q} - \boldsymbol{u}\phi) - \sigma\phi &=& -f 
	\ \mbox{ in } \ \Omega
	\label{eq-convect-hdg-u}
	\\
  \phi &=& \phi_\Gamma
	\ \mbox{ on } \ \Gamma_d
	\label{eq-convect-hdg-bc-d}
\end{eqnarray}
\end{subequations}
Let us multiply \eqref{eq-convect-hdg-q} by a test function $\boldsymbol{\tau}$
and integrate by parts on any element $K$:
\begin{eqnarray*}
  \int_K
    \left(
      \nu^{-1}
      \boldsymbol{q}.\boldsymbol{\tau}
      +
      {\rm div}(\boldsymbol{\tau})
      \, \phi
    \right)
    \,{\rm d}x
  -
  \int_{\partial K}
    (\boldsymbol{\tau}.\boldsymbol{n})
    \, \phi
    \,{\rm d}s
  &=&
  0 
\end{eqnarray*}
In the discontinuous Galerkin method, the trace of $\phi$ on $\partial K$ will
be discontinuous across the inter-element boundaries.
It is here replaced by a new independent variable, denoted as $\lambda$,
that is only definied on sides of the mesh:
\begin{eqnarray*}
  \int_K
    \left(
      \nu^{-1}
      \boldsymbol{q}.\boldsymbol{\tau}
      +
      {\rm div}(\boldsymbol{\tau})
      \, \phi
    \right)
    \,{\rm d}x
  -
  \int_{\partial K}
    (\boldsymbol{\tau}.\boldsymbol{n})
    \, \lambda
    \,{\rm d}s
  &=&
  0 
\end{eqnarray*}
The $\lambda$ variable will be uni-valued on boundary inter-elements
and accounts strongly for the boundary condition:
$\lambda=g$ on $\partial\Omega$.
%
Then, let us multiply \eqref{eq-convect-hdg-u} by a test function $\varphi$:
\begin{eqnarray*}
  \int_K
    \left(
      {\rm div}(\boldsymbol{q} - \boldsymbol{u}\phi)
      -
      \sigma\phi
    \right)
    \,\varphi
    \,{\rm d}x
  &=&
  -
  \int_K
    f 
    \,\varphi
    \,{\rm d}x
  \\
  \Longleftrightarrow\ 
  \int_K
    \left(
      \left(
        {\rm div}(\boldsymbol{q})
        -
        \sigma\phi
      \right)
      \,\varphi
      +
      \phi
      \,(\boldsymbol{u}.\nabla\varphi)
    \right)
    \,{\rm d}x
  -
  \int_{\partial K}
    (\boldsymbol{u}.\boldsymbol{n})
    \,\phi
    \,\varphi
    \,{\rm d}s
  &=&
  -
  \int_K
    f 
    \,\varphi
    \,{\rm d}x
\end{eqnarray*}
\index{penalization}%
We replace $\phi$ by $\lambda$ on $\partial K$ and
add a penalization term
in order to weakly impose the condition $\phi=\lambda$ on $\partial K$:
\begin{eqnarray*}
  \int_K
    \left(
      \left(
        {\rm div}(\boldsymbol{q})
        -
        \sigma\phi
      \right)
      \,\varphi
      +
      \phi
      \,(\boldsymbol{u}.\nabla\varphi)
    \right)
    \,{\rm d}x
  -
  \int_{\partial K}
    \left(
      (\boldsymbol{u}.\boldsymbol{n})
      \,\lambda
      +
      (
        \alpha
        +
        |\boldsymbol{u}.\boldsymbol{n}|
      )
      \,(\phi-\lambda)
    \right)
    \,\varphi
    \,{\rm d}s
  &=&
  -
  \int_K
    f 
    \,\varphi
    \,{\rm d}x
\end{eqnarray*}
We have three unknowns $\boldsymbol{q}$, $\phi$ and $\lambda$
and only two equations: for the problem to be complete, we add
an equation for $\lambda$.
\cindex{numerical flux}%
The new variable $\lambda$ interprets as a Lagrange multiplier
in order to impose the continuity of the normal component
of the numerical flux
\mbox{$
  \boldsymbol{u}\,\lambda
  -
  \boldsymbol{q}
  +
  (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
  \,(\phi-\lambda)
  \,\boldsymbol{n}
$} across any element interface
$S\in \mathscr{S}_h^{(i)}$.
Let $S=\partial K_1\cap\partial K_2$ and
$\phi_1$ (resp. $\phi_2$) and $\boldsymbol{n}_1$
(resp. $\boldsymbol{n}_2$) the value of $\phi$ and
the normal on $S\cap\partial K_1$
(resp.  $S\cap\partial K_2$).
Without loss of generality we suppose that $S$ is oriented
such that 
\mbox{$\boldsymbol{n}= \boldsymbol{n}_1= \boldsymbol{n}_2$}.
Then, the continuity of the normal component of the 
numerical flux writes on all internal sides $S$:
\begin{eqnarray*}
  &&
  \left(
    \boldsymbol{u}\lambda
    -
    \boldsymbol{q}_1
    +
    (\alpha + |\boldsymbol{u}.\boldsymbol{n}_1|)
    \,(\phi_1-\lambda)
    \,\boldsymbol{n}_1
  \right)
  .\boldsymbol{n}_1
  \ +\ 
  \left(
    \boldsymbol{u}\lambda
    -
    \boldsymbol{q}_2
    +
    (\alpha + |\boldsymbol{u}.\boldsymbol{n}_2|)
    \,(\phi_2-\lambda)
    \,\boldsymbol{n}_2
  \right)
  .\boldsymbol{n}_2
  \ = \ 
  0
  \\
  \Longleftrightarrow 
  &&
  -
  \jump{\boldsymbol{q}}.\boldsymbol{n}
  +
  2(\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
  \,(\average{\phi}-\lambda)
  \ =\ 
  0
\end{eqnarray*}
Finally, on all the boundary sides $S\subset \partial\Omega$,
the Dirichlet condition
$\lambda=\phi_\Gamma$ is imposed.

Grouping the previous relations, we obtain the 
discrete variational formulation:

\ \ $(FV)_h$: \emph{find $(\boldsymbol{q}_h,\phi_h,\lambda_h)\in T_h\times X_h\times \Lambda_h(\phi_\Gamma)$ such that}
\begin{eqnarray*}
  &&
  \int_\Omega
     \nu^{-1}
     \boldsymbol{q}_h
    .\boldsymbol{\tau}_h
    \,{\rm d}x
  +
  \int_\Omega
    {\rm div}_h(\boldsymbol{\tau}_h)
    \, \phi_h
    \,{\rm d}x
  -
  \int_{\mathscr{S}_h^{(i)}}
    (\jump{\boldsymbol{\tau}_h}.\boldsymbol{n})
    \, \lambda_h
    \,{\rm d}s
  \ =\ 
  \int_{\partial\Omega}
    (\boldsymbol{\tau}_h.\boldsymbol{n})
    \, \phi_\Gamma
    \,{\rm d}s
  \\
  &&
  \int_\Omega
    {\rm div}_h(\boldsymbol{q}_h)
    \,\varphi_h
    \,{\rm d}x
  -
  \int_\Omega
    \phi_h
    \,\left(
      \sigma
      \,\varphi_h
      -
      \,(\boldsymbol{u}.\nabla_h\varphi_h)
    \right)
    \,{\rm d}x
  -
  \sum_{K\in\mathscr{T}_h}
  \int_{\partial K}
    (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
    \,\phi_h
    \,\varphi_h
    \,{\rm d}s
  \\
  &&
  +\ 
  \int_{\mathscr{S}_h^{(i)}}
    \left(
      2
      (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
      \average{\varphi_h}
      -
      (\boldsymbol{u}.\boldsymbol{n})
      \jump{\varphi_h}
    \right)
    \,\lambda_h
    \,{\rm d}s
  \ =\ 
  -
  \int_\Omega
    f 
    \,\varphi_h
    \,{\rm d}x
  -
  \int_{\partial\Omega}
    (\alpha + |\boldsymbol{u}.\boldsymbol{n}| - \boldsymbol{u}.\boldsymbol{n})
    \,\phi_\Gamma
    \,\varphi_h
    \,{\rm d}s
  \\
  &&
  -
  \int_{\mathscr{S}_h^{(i)}}
    \jump{\boldsymbol{q}_h}.\boldsymbol{n}
    \,\mu_h
    \,{\rm d}s
  +
  \int_{\mathscr{S}_h^{(i)}}
    2
    (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
    \,\average{\phi_h}
    \,\mu_h
    \,{\rm d}s
  -
  \int_{\mathscr{S}_h^{(i)}}
    2
    (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
    \,\lambda_h
    \, \mu_h
    \,{\rm d}s
  \ =\ 
  0
\end{eqnarray*}
for all $(\boldsymbol{\tau}_h,\varphi_h,\tau_h)\in T_h\times X_h\times \Lambda_h(0)$,
where
\begin{eqnarray*}
  T_h
  &=&
  \left\{
    \boldsymbol{\tau} \in \left(L^2(\Omega)\right)^{d}
    \ ;\ 
    \boldsymbol{\tau}_{|K} \in (P_k)^d
    ,\ \forall K\in \mathscr{T}_h
  \right\}
  \\
  X_h
  &=&
  \left\{
    v\in L^2(\Omega)
    \ ;\ 
    v_{|K} \in P_k
    ,\ \forall K\in \mathscr{T}_h
  \right\}
  \\
  M_h
  &=&
  \left\{
    \mu\in L^2(\mathscr{S}_h)
    \ ;\ 
    \mu_{|S} \in P_k
    ,\ \forall S\in \mathscr{S}_h
  \right\}
  \\
  \Lambda_h(\phi_\Gamma)
  &=&
  \left\{
    \mu\in M_h
    \ ;\ 
    \mu_{|S} = \pi_h(\phi_\Gamma)
    ,\ \forall S\subset \partial\Omega
  \right\}
\end{eqnarray*}
and $k\geq 0$ is the polynomial degree.
This is a symmetric system with a mixed structure.

Let:
\begin{eqnarray*}
  a_h(
    (\boldsymbol{q},\phi);\,
    (\boldsymbol{\tau},\varphi))
  &=&
  \int_\Omega
     \left(
       \nu^{-1}
       \boldsymbol{q}
      .\boldsymbol{\tau}
      +
      {\rm div}_h(\boldsymbol{\tau})
      \, \phi
      +
      {\rm div}_h(\boldsymbol{q})
      \,\varphi
      -
      \sigma
      \,\phi
      \,\varphi
      +
      \phi
      \,(\boldsymbol{u}.\nabla_h)\varphi
    \right)
    \,{\rm d}x
  \\
  &&
  -\ 
  \sum_{K\in\mathscr{T}_h}
  \int_{\partial K}
    (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
    \,\phi\,\varphi
    \,{\rm d}s
  \\
  b_{1,h}((\boldsymbol{\tau},\varphi);\,\mu)
  &=&
  \int_{\mathscr{S}_h^{(i)}}
    \left(
      -
      \jump{\boldsymbol{\tau}}.\boldsymbol{n}
      +
      2
      (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
      \average{\varphi}
      -
      (\boldsymbol{u}.\boldsymbol{n})
      \jump{\varphi}
    \right)
    \mu
    \,{\rm d}s
  \\
  b_{2,h}((\boldsymbol{\tau},\varphi);\,\mu)
  &=&
  \int_{\mathscr{S}_h^{(i)}}
    \left(
      -
      \jump{\boldsymbol{\tau}}.\boldsymbol{n}
      +
      2
      (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
      \average{\varphi}
    \right)
    \mu
    \,{\rm d}s
  \\
  c_h(\lambda,\mu)
  &=&
  \int_{\mathscr{S}_h}
    2
    (\alpha + |\boldsymbol{u}.\boldsymbol{n}|)
    \, \lambda
    \, \mu
    \,{\rm d}s
  \\
  \ell_h(\boldsymbol{\tau},\varphi)
  &=&
  -
  \int_\Omega
    f 
    \,\varphi
    \,{\rm d}x
  +
  \int_{\partial\Omega}
    \phi_\Gamma
    \left(
      \boldsymbol{\tau}.\boldsymbol{n}
      -
      (\alpha + |\boldsymbol{u}.\boldsymbol{n}| - \boldsymbol{u}.\boldsymbol{n})
      \,\varphi
    \right)
    \,{\rm d}s
  \\
  k_h(\mu)
  &=&
  0
\end{eqnarray*}
Then, the variational formulation writes equivalently:

\ \ $(FV)_h$: \emph{find $(\boldsymbol{q}_h,\phi_h,\lambda_h)\in T_h\times X_h\times \Lambda_h(g_d)$ such that}
\begin{eqnarray*}
  a_h(
    (\boldsymbol{q}_h,\phi_h);\,
    (\boldsymbol{\tau}_h,\varphi_h))
  +
  b_{1,h}((\boldsymbol{\tau}_h,\varphi_h);\,\lambda_h)
  &=&
  \ell_h(\boldsymbol{\tau}_h,\varphi_h)
  \\
  b_{2,h}((\boldsymbol{q}_h,\phi_h);\,\mu_h)
  -
  c_h(\lambda_h,\mu_h)
  &=&
  k_h(\mu_h)
\end{eqnarray*}
for all $(\boldsymbol{\tau}_h,\varphi_h,\tau_h)\in T_h\times X_h\times \Lambda_h(0)$.
%

Let $\chi_h=(\boldsymbol{q}_h,u_h)$.
This linear symmetric system admits the following matrix structure:
\begin{eqnarray*}
  &&
  \left( \begin{array}{cc}
    A & B_1^T \\
    B_2 & -C
  \end{array} \right)
  \left( \begin{array}{c}
    \boldsymbol{\chi}_h \\
    \lambda_h
  \end{array} \right) 
  \ =\ 
  \left( \begin{array}{c}
    \ell_h \\
    k_h
  \end{array} \right) 
\end{eqnarray*}
A carreful study shows that the $A$ matrix
has a block-diagonal structure at the element level
and can be easily inverted during the assembly process.
The matrix structure writes equivalently:
\begin{eqnarray*}
  &\Longleftrightarrow&
  \left\{ \begin{array}{rcl}
	A\boldsymbol{\chi}_h + B_1^T\lambda_h &=& \ell_h \\
	B_2\boldsymbol{\chi}_h -   C\lambda_h &=& k_h
  \end{array} \right.
  \\
  &\Longleftrightarrow&
  \left\{ \begin{array}{l}
	\boldsymbol{\chi}_h = A^{-1}(F-B_1^T\lambda_h) \\
	(C+B_2A^{-1}B_1^T)\lambda_h = B_2A^{-1}\ell_h - k_h
  \end{array} \right.
\end{eqnarray*}
The second equation is solved first:
the only remaining unknown is the Lagrange multiplier $\lambda_h$.
Then, the two variables $\boldsymbol{\chi}_h=(\boldsymbol{q}_h,\phi_h)$
are simply obtained by a direct computation.

\myexamplelicense{convect_hdg.cc}

% -----------------------------------------------------------------------
\subsubsection*{Running the program}
% -----------------------------------------------------------------------

The rotating hill test case has already been introduced in
 section~\ref{sec-diffusion-convection}
page~\pageref{sec-diffusion-convection},
together with the file~\reffile{rotating-hill.h}.
Here, a stationary version of this test case
is implemented, at a fixed time step $t=0$,
and a non-zero right-hand-side 
$f=-\partial\phi/\partial t(t_0)$
is adjusted accordingly.

\myexamplelicense{rotating-hill-statio.h}

Running the one-dimensional test-case writes:
\begin{verbatim}
  make convect_hdg
  mkgeo_grid -e 500 -a -2 -b 2 > line2.geo
  ./convect_hdg line2.geo P1d  > line2.field
  field line2.field
\end{verbatim}

Conversely, running the one-dimensional test-case writes:
\begin{verbatim}
  mkgeo_grid -t 80 -a -2 -b 2 -c -2 -d 2 > square2.geo
  ./convect_hdg square2.geo P1d  > square2.field
  field square2.field -elevation
\end{verbatim}
