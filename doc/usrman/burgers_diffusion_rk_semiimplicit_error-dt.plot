set terminal cairolatex pdf color standalone
set output "burgers_diffusion_rk_semiimplicit_error-dt.tex"

set size square
set colors classic
set log
set key bottom left
set xrange [1e-5:1e-1]
set yrange [1e-5:1e-1]
graph_ratio = 4.0/4.0
set xtics (\
	'[c]{$10^{-6}$}' 1e-6, \
	'[c]{$10^{-5}$}' 1e-5, \
	'[c]{$10^{-4}$}' 1e-4, \
	'[c]{$10^{-3}$}' 1e-3, \
	'[c]{$10^{-2}$}' 1e-2, \
	'[c]{$10^{-1}$}' 1e-1)
set ytics (\
	'[r]{$10^{-5}$}' 1e-5, \
	'[r]{$10^{-4}$}' 1e-4, \
	'[r]{$10^{-3}$}' 1e-3, \
	'[r]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-1}$}' 1e-1)
set xlabel '[c]{$\Delta t$}'
set  label '[l]{$\|u-u_h\|_{L^{\infty}(0,T;L^2)}$}' at graph 0.02, 0.95
set  label '[l]{(b) $k=1$, $h=2/400$}' at graph 0.08, 0.85

# triangle a droite
slope_A = graph_ratio*1
xA =  0.34
yA =  0.40
dxA = 0.10
dyA = dxA*slope_A
set label sprintf("[l]{\\scriptsize $%.2g$}",1) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a gauche
slope_B = graph_ratio*2.0
xB =  0.69
yB =  0.30
dxB = 0.05
dyB = dxB*slope_B
set label "[r]{\\scriptsize $2$}" at graph xB-0.02, yB+0.5*dyB right
set arrow from graph xB,     yB     to graph xB,     yB+dyB nohead
set arrow from graph xB,     yB+dyB to graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
slope_C = graph_ratio*3
xC =  0.90
yC =  0.45
dxC = 0.05
dyC = dxC*slope_C
set label sprintf("[l]{\\scriptsize $%.2g$}",3) at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

plot \
  "burgers_diffusion_rk_semiimplicit_error-P1d-dt.gdat" \
        using (1./$2):3 index 0 \
	t '[r]{$p=1$}' \
	w lp lt 1 lw 2 lc 0, \
  "burgers_diffusion_rk_semiimplicit_error-P1d-dt.gdat" \
        using (1./$2):3 index 1 \
	t '[r]{$p=2$}' \
	w lp lt 1 lw 2 lc rgb "#990000", \
  "burgers_diffusion_rk_semiimplicit_error-P1d-dt.gdat" \
        using (1./$2):3 index 2 \
	t '[r]{$p=3$}' \
	w lp lt 1 lw 3 lc rgb "#006600"

#pause -1
