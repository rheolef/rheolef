set terminal cairolatex pdf color standalone
set output "yield-slip-circle-S-0,25-n-0,5-err-linf.tex"

set size square
set colors classic
set logscale xy
set key bottom
set xrange [1e-2:1e0]
set yrange [1e-9:1e-1]
set xlabel '[c]{\large $h$}'
set  label '[l]{\large $\|u-u_h\|_{0,\infty,\Omega}$}' at graph 0.08,0.92

set xtics (\
        "[c]{$10^{-2}$}" 1e-2, \
        "[c]{$10^{-1}$}" 1e-1, \
        "[c]{$1$}" 1)
set ytics (\
        "[r]{$10^{-8}$}" 1e-8, \
        "[r]{$10^{-6}$}" 1e-6, \
        "[r]{$10^{-4}$}" 1e-4, \
        "[r]{$10^{-2}$}" 1e-2 )

graph_ratio = 2.0/8.0

# triangle a droite
slope_A = graph_ratio*2.0
xA =  0.15
yA =  0.64
dxA = 0.10
dyA = dxA*slope_A
set label "[l]{\\scriptsize $2=k+1$}" at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a gauche
slope_B = graph_ratio*3.0
xB =  0.15
yB =  0.35
dxB = 0.10
dyB = dxB*slope_B
set label "[r]{\\scriptsize $3$}" at graph xB-0.02, yB+0.5*dyB right
set arrow from graph xB,     yB     to graph xB,     yB+dyB nohead
set arrow from graph xB,     yB+dyB to graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
slope_C = graph_ratio*4.0
xC =  0.25
yC =  0.14
dxC = 0.10
dyC = dxC*slope_C
set label "[l]{\\scriptsize $4$}" at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead


plot \
'yield-slip-circle-S-0,25-n-0,5-P1-sym.gdat' \
	u (1/$1):2 t '[r]{$k=1$}' w lp lt 1 lc 1 pt 1 lw 2, \
'yield-slip-circle-S-0,25-n-0,5-P2-sym.gdat' \
	u (1/$1):2 t '[r]{$k=2$}' w lp lt 1 lc 2 pt 2 lw 2, \
'yield-slip-circle-S-0,25-n-0,5-P3-sym.gdat' \
	u (1/$1):2 t '[r]{$k=3$}' w lp lt 1 lc 3 pt 3 lw 2

#pause -1 "<retour>"
