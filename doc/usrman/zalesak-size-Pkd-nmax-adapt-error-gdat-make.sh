#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
# influence of the subdivide adaptive (VTK/Tessellate)
# on the L1-error & mass-loss computation
#
branch=${1-"zalesak-250-P2d-96000.branch.gz"}
field_error=${2-"1e-12"}
subdivide=1
subdivide_max=8
loop=true
echo "# $branch"
echo "# field_error = ${field_error}"
echo "# subdivide err_linf_l1 v_max size hmin"
while test $subdivide -le $subdivide_max; do
  (zcat $branch | ./zalesak_dg_adapt ${subdivide} ${field_error} | ./zalesak_dg_error 1 ) 2>/dev/null > tmp.txt
  err_linf_l1=`grep err_linf_l1 tmp.txt | awk '{print $4}'`
  v_max=`grep v_max tmp.txt | awk '{print $4}'`
  size=`grep "omega.size" tmp.txt | awk '{print $4}'`
  hmin=`grep "hmin" tmp.txt | awk '{print $4}'`
  echo "$subdivide $err_linf_l1 $v_max $size $hmin"
  subdivide=`expr $subdivide + 1`
done
