%
% This file is part of Rheolef.
%
% Copyright (C) 2000-2009 Pierre Saramito 
%
% Rheolef is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% Rheolef is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Rheolef; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
%
% -------------------------------------------------------------------
% -------------------------------------------------------------------
\subsection{\new The time-dependent transport equation}
% -------------------------------------------------------------------
\subsubsection*{Problem statement}
% ---------------------------------
\cindex{interface!moving}%
The time-dependent transport equation is involved in
many problems of mathematical physics.
One of these problem concerns a moving domain,
transported by a velocity field.
Let us denote, at any time~\mbox{$t\geq 0$}
by~\mbox{$\omega(t)\subset\mathbb{R}^d$}, \mbox{$d=2,3$},
a bounded moving domain
and by~\mbox{$\Gamma(t)=\partial\omega(t)$} its boundary,
called the \emph{interface}.
%
Let~$\Omega\subset\mathbb{R}^d$ be the bounded computational domain.
We assume that it contains at any time~$t\geq 0$ the moving domain~$\omega(t)$.
\cindex{method!level set}%
A function
\mbox{$
  \phi:
  [0,\infty[\times\Omega\rightarrow\mathbb{R}
$}
is a \emph{level set} for
the moving domain if and only if
\mbox{$
  \Gamma(t) = \{ \boldsymbol{x}\in\Omega ~/~ \phi(t,\boldsymbol{x})=0 \}
$}
for any~$t\geq 0$.
%The level set function is then transported by~$\boldsymbol{u}$.
For a given velocity field~$\boldsymbol{u}$,
associated to the motion,
the level set function is defined as the solution of the following time dependent transport problem:

\mbox{}\ \ $(P)$:
  \emph{find~$\phi$, defined in $]0,\infty[\times \Omega$ such that}
\begin{subequations}
\begin{empheq}[left={\empheqlbrace}]{align}
    \dfrac{\partial\phi}{\partial t} + \boldsymbol{u}.\nabla\phi
    =
    0
    & \ \mbox{ in } ]0,\infty[\times \Omega 
    \label{eq-transport-level-set}
    \\
    \phi(t\!=\!0)
    =
    \phi_0
    & \ \mbox{ in } \Omega 
    \label{eq-transport-level-set-ic}
\end{empheq}
\end{subequations}
where~$\phi_0$ is given and represents the initial shape~$\omega(0)$.
Here, we assume a divergence free velocity field,
such that the volume of the moving domain is conserved.
% The normal at the boundary~$\Gamma(t)$ writes
% \mbox{$
%     \boldsymbol{\nu}
%     =
%     \nabla\phi/|\nabla\phi|
% $}
% and then deformations of~$\Gamma(t)$ are only due
% to the normal component~$\boldsymbol{u}.\boldsymbol{\nu}$
% of the velocity on~$\Gamma(t)$.
This is a linear \emph{hyperbolic problem}.
The level set method for describing moving interfaces
was introduced by \citet{OshSet-1988} (see also~\citealp{Set-1999}).

\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[width=0.8\textwidth]{zalesak-100-P2d-5000-elevation.jpg} &
      \stereoglasses
    \end{tabular}
  \end{center}
  \caption{Zalesak slotted disk:
    elevation view of the interpolated level set function
    (upside-down), together with its zero level set, in black thick line.}
  \label{fig-zalesak-elevation}
\end{figure}
%
Note that several choices are possible for the function~$\phi$:
the only requirement being that a fixed isocontour of~$\phi$ 
coincides with the front at each time~$t$.
A common choice is the signed distance from the front:
e.g. $\omega(t)$ is the part where $\phi(t,.)$ is negative and 
\mbox{$
    \phi(t,\boldsymbol{x})=-\mathrm{dist}(\Gamma(t),\boldsymbol{x})
$}
for all~$\boldsymbol{x}\in\omega(t)$.
%
    Fig.~\ref{fig-zalesak-elevation},
page~\pageref{fig-zalesak-elevation},
represents in elevation such a signed distance level set function:
the interface~$\Gamma$, the thick black line,
corresponds to the Zalesak slotted disk,
that will be our benchmark in the next section.

% ----------------------------
\subsubsection*{Approximation}
% ----------------------------
The evolution transport equation~\eqref{eq-transport-level-set}
is then discretized with respect to time.
\cindex{method!Runge-Kutta scheme}%
\citet{PieForPar-2006} and \citet{MarRemChe-2006}
used an \emph{explicit} and \emph{strong stability preserving}
Runge-Kutta method~\citep{GotShuTad-2001}.
Here, we propose to use a high order \emph{implicit} scheme
\cindex{method!BDF($p$), backward differentiation formula}%
based on the following \emph{backward differentiation formula} (BDF):
\[
    \dfrac{\partial \phi}{\partial t}(t)
    =
    \dfrac{1}{\Delta t}
    \sum_{i=0}^p
	\alpha_{p,i}
        \ \phi (t\!-\!i\Delta t)
    +
    \mathcal{O} \left( \Delta t^p \right)
\]
where $\Delta t >0$ is the time step,
$p\geq 1$ is the order of the scheme
and $(\alpha_{p,i})_{0\leq i\leq p}$ are
the $p+1$ coefficients of the formula.
When $p=1$, the scheme coincides with the usual
implicit Euler method.
For $1 \leq p \leq 2$, the scheme is unconditionally stable,
for $3 \leq p \leq 6$, the scheme is almost unconditionally stable
while when $p > 6$, the scheme is unstable and cannot be used
(see e.g.~\citealp[p.~349]{SulMay-2003}).

\begin{table}[htb]
  \begin{center}
  \[
    \begin{array}{|c||c|c|c|c|c|c|c|} \hline
	p\backslash i & 0 & 1 & 2 & 3 & 4 & 5 & 6 \\ 
	\hline
	\hline
	 1 &
	 1 & 
	-1 &
	&&&&
	\\
	\hline
	 2 &
	 3/2 & 
	-2 & 
	 1/2 &
	&&&
	\\
	\hline
	3 &
	11/6 &
	-3 &
	3/2 &
	-1/3 &
	&&
	\\
	\hline
	4 &
	25/12 &
	-4 &
	3 &
	-4/3 &
	1/4 &	
	&
	\\
	\hline
	5 &
	137/60 &
	-5 &
	5 &
	-10/3 &
	5/4 &
	-1/5 &
	\\
	\hline
	6 &
	147/60 &
	-6 &
	15/2 &
	-20/3 &
	15/4 &
	-6/5 &
	1/6
	\\
	\hline
    \end{array}
  \]
  \end{center}
  \caption{BDF($p$) schemes: the~$\alpha_{p,i}$ coefficients,
           $1\leq p\leq 6$, $1\leq i\leq p$.
  }
  \label{tab-bdf}
\end{table}

% -------------------------------------
\myexamplenoinput{bdf.icc}
% -------------------------------------

The coefficients when~$p\leq 6$ are given in table~\ref{tab-bdf}
and the file \file{bdf.icc} implements this table.
This file it is not listed here but is available in the \Rheolef\  example directory.
When the time step~\mbox{$n<p$}, the scheme is 
started by using the BDF($n$) formula instead
and we introduce~\mbox{$p_n=\min(p,n)$} for convenience.
%
The discretization with respect to space 
follows the method developed in the previous section~\ref{sec-transport-dg}.
Let us introduce the following linear and bilinear form,
defined for all $\varphi,\xi\in X_h$ by
\begin{subequations}
\begin{empheq}{align}
  a_n(\varphi,\xi)
  &=
  \int_\Omega
    \left(
      \dfrac{\alpha_{p_n,0}}{\Delta t}
      \varphi
      \, \xi
      +
      \boldsymbol{u}(t_n).\nabla_h\varphi
      \, \xi
    \right)
    \mathrm{d}x
  +
  \int_{\partial\Omega}
    \max(0,-\boldsymbol{u}(t_n).\boldsymbol{n})
    \, \varphi
    \, \xi
    \, \mathrm{d}s
  \nonumber
  \\
  & \phantom{mm}
  + \ 
  \sum_{S\in\mathscr{S}_h^{(i)}}
    \int_S
      \jump{\varphi}
      \left(
        \dfrac{|\boldsymbol{u}(t_n).\boldsymbol{n}|}{2}
	\, \jump{\xi}
        -
	\boldsymbol{u}(t_n).\boldsymbol{n}
	\, \average{\xi}
      \right)
      \, \mathrm{d}s
  \label{eq-transport-bdf-an}
  \\
  l_n(\xi)
  &=
  -
  \sum_{i=1}^{p_n}
    \dfrac{\alpha_{p_n,i}}{\Delta t}
    \int_\Omega
      \phi_h^{(n-i)}\,\xi
      \mathrm{d}x
  \label{eq-transport-bdf-ln}
\end{empheq}
\end{subequations}
When $n=0$ we set $\phi_h^{(0)}=\pi_h(\phi_0)$
and when~$n\geq 1$, 
the time approximation~$\phi_h^{(n)}$
is defined by a $p_n$-order recurrence
as the solution of the following linear problem:

\mbox{} \ 
$(P)_h$:  \emph{find $\phi_h^{(n)}\in X_h$ such that}
\begin{eqnarray*}
	a_n\left( \phi_h^{(n)},\xi_h \right)
	=
        l_n(\xi_h),
  ,\ \ \forall \xi_h\in X_h
\end{eqnarray*}
Finally, at each time step, the problem reduces to a linear system.

% ------------------------------
\subsection{\new Example: the Zalesak slotted disk}
% ------------------------------
\label{sec-zalesk-dg}%
\apindex{discontinuous}%
\cindex{problem!transport equation!evolution}%
\cindex{benchmark!Zalesak slotted disk}%

The \citet[p.~350]{Zal-1979} slotted disk in rotation
is a widely used benchmark
for comparing the performances of interface capturing methods.
The radius of the disk is~$0.15$ and its center is~$(0.5, 0.75)$
The width of the slot is~$0.05$ and its length is~$0.25$.
This slotted disk is rotated around the barycenter~$(0.5,0.5)$
of the computational domain~\mbox{$\Omega=[0,1]^2$} at velocity 
\mbox{$
  \boldsymbol{u}(x_0,x_1)=(-(x_1-1/2)/2,\, (x_0-1/2)/2)
$},
such that the angular velocity
\mbox{$
  \mathrm{curl}\,\boldsymbol{u}
  =
  1
$}
is constant.
Note that the slotted disk returns to its initial
position after a period $t_f=4\pi$.
The presentation of this section is mainly inspired
from~\citet{TaPigSar-2016-icmf}, that used \Rheolef.
%
The following code implement the previous BDF scheme
for this benchmark.

% ---------------------------------------
\myexamplenoinput{zalesak.h}

\myexamplelicense{zalesak_dg.cc}
% ---------------------------------------

\subsubsection*{Comments}
Numerical computations are performed on the time interval~$[0,t_f]$
with a time step~$\Delta t=t_f/n_{max}$ where~$n_{max}$ is the number of time steps.
%
Since~$d=2$, a direct method is preferable 
(see section~\ref{sec-solvers} and Fig.~\ref{fig-solver-P1}).
Observe that~$\boldsymbol{u}$ is here independent upon~$t$.
Then~$a_n$, as given by~\eqref{eq-transport-bdf-an},
is independent upon~$n$ when~$n\geq p$, 
and~$a_n$ can be factored one time for all~$n\geq p$.
The file \file{zalesak.h} included here implements the \code{phi0}
and \code{phi_exact} class-functions representing the
exact solution~$\phi(t)$ of level set function for the slotted disk,
together with the~\code{u} class-function for the velocity field.
This file it is not listed here but is available in the \Rheolef\  example directory.
%
Recall that the BDF($p$) scheme is
unconditionally stable when~$p\in \{1,2\}$
and \emph{almost} unconditionally stable
when~\mbox{$p\in\{3,\ldots,6\}$}
(see e.g.~\citealp[p.~349]{SulMay-2003}).
Then, when an instability is detected, an issue
for decreasing the time step is generated
and the computation is stopped.

% ------------------------------
\subsubsection*{How to run the program}
% ------------------------------
\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[height=0.4\textwidth]{zalesak-100-P2d-6000.jpg}  &
      \includegraphics[height=0.4\textwidth]{zalesak-100-P2d-6000-zoom.jpg}
    \end{tabular}
  \end{center}
  \caption{Zalesak slotted disk:
	superposition of the initial disk (black) and after a period (red).
        Polynomial degree~$k=2$, time step~$\Delta t=4\pi/6000$,
        implicit BDF($3$) scheme with a fixed mesh~$h=1/100$.
	On the right: zoom.
  }
  \label{fig-zalesak-Pkd}
\end{figure}
%
\pindexopt{branch}{-vtk}%
\pindex{paraview}%
Let us first generate a mesh for the~$\Omega=\,]0,1[^2$ geometry
with~$h=1/100$ and then run the code with a~$k=2$ discontinuous Galerkin method
and a~$BDF(3)$ scheme:
\begin{verbatim}
  mkgeo_ugrid -t 50 > square.geo
  make zalesak_dg
  time mpirun -np 8 ./zalesak_dg square P2d > square-P2d.branch
\end{verbatim}
\pindex{mpirun}%
The computation could take about ten minutes, depending upon your computer:
there are~$1000$ time steps by default~;
a prompt shows the step advancement until~$1000$.
The \code{time} and \code{mpirun -np 8} prefixes
are optional and you should omit them
if you are running a sequential installation of \Rheolef.
\pindexopt{branch}{-iso}%
\pindexopt{branch}{-bw}%
Then, the visualization writes:
\begin{verbatim}
  branch square-P2d.branch -iso 0 -bw
\end{verbatim}
The slotted disk appears at his initial position.
Then, click on the video \fbox{play} button, at the top of the window:
the disk starts to rotate.
Fig.~\ref{fig-zalesak-Pkd} 
compares the initial disk position with the final one after one period
when $h=1/100$ and~$6000$ time steps.
Observe the dramatic efficiency of the combination of the
discontinuous Galerkin method and the high-order time scheme.
The corresponding solution could be directly observed as a video
from the following link (3 Mo):
\begin{center}
  \url{http://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/zalesak-100-P2d-6000.mp4}
\end{center}
Moreover, when using higher order polynomials and schemes,
e.g. for~$k\geq 3$, the changes are no more perceptible and the disk after one period
fits the initial shape.
The error between the two shapes could also be evaluated,
and the convergence versus mesh and polynomial degree could be checked:
see~\citet{TaPigSar-2016-icmf} for more details.

