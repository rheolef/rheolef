% --------------------------------------------------------------
\subsection{\new Heterogeneous diffusion}
% --------------------------------------------------------------
\label{transmission-dg}%
\pbindex{Poisson}%
\cindex{boundary condition!Dirichlet}%

The problem with with non-constant diffusion coefficients
has already been introduced in
 section~\ref{transmission-dg},
page~\pageref{transmission-dg}
when studying continuous approximations.
It writes

{\it $(P)$: find $u$ defined in $\Omega$ such that:}
\begin{eqnarray*}
      -{\rm div}(\eta \bnabla u) &=& f \ \mbox{in}\ \Omega
		\\
      u &=& 0 \ {\rm on}\ \Gamma_{\rm left} \cup \Gamma_{\rm right}
		\\
      \Frac{\partial u}{\partial n} &=& 0 \ {\rm on}\ \Gamma_{\rm top} \cup \Gamma_{\rm bottom} \mbox{ when } d\geq 2
		\\
      \Frac{\partial u}{\partial n} &=& 0 \ {\rm on}\ \Gamma_{\rm front} \cup \Gamma_{\rm back} \mbox{ when } d = 3
\end{eqnarray*}
where $f$ is a given source term.
We consider here the important special case when
the diffusion coefficient~$\eta$ is piecewise constant:
\[
      \eta (x) = \left\{ \begin{array}{ll}
		\varepsilon   & \mbox{ when } x \in \Omega_{\rm west} \\ 
		1             & \mbox{ when } x \in \Omega_{\rm east}
      		\end{array} \right.
\]
where $(\Omega_{\rm west}, \Omega_{\rm east})$ is a partition of
$\Omega$ in two parts
(see Fig.~\ref{fig-transmission-domains},
 page~\pageref{fig-transmission-domains}).
\pbindex{transmission}%
This is the so-called {\bf transmission} problem:
the solution and the flux are continuous on the interface
$\Gamma_0=\partial\Omega_{\rm east}\cap\partial\Omega_{\rm west}$
between the two domains where the problem reduce to a constant diffusion one:
\begin{eqnarray*}
	u_{\Omega_{\rm west}} &=& u_{\Omega_{\rm east}}
		\mbox { on } \Gamma_0 \\
	\varepsilon \Frac{\partial u_{/\Omega_{\rm west}} }{\partial n}
        &=& 
	\Frac{\partial u_{\Omega_{\rm east}} }{\partial n}
		\mbox { on } \Gamma_0 \\
\end{eqnarray*}

Following \citet{Dry-2003}, we use the discontinuous Galerkin
with diffusion-dependent weights to formulate the consistency and symmetry terms
in the discrete bilinear form
and we penalize interface and boundary jumps using a diffusion-dependent parameter
scaling as the harmonic mean of the diffusion coeffcient.
As pointed out by \citet[p.~150]{PieErn-2012},
this a penalty strategy is particularly important in heterogeneous diffusion-convection
problems where the diffusion coeffcient takes locally small values leading
to advection-dominated regimes.
In this context, the exact solution exhibits sharp inner layers
which, in practice, are not resolved by the underlying meshes,
so that excessive penalty at such layers triggers spurious oscillations.
Using the harmonic mean of the diffusion coeffcient to penalize jumps
turns out to tune automatically the amount of penalty and thereby avoid such oscillations.

The \emph{weighted average} is defined
for any function~$v$
and on any side~\mbox{$S\in\mathscr{S}_h^{(i)}$}
by
\[
  \average{v}_\omega
  =
  \omega_{K_0,S}
  \, v_{|K_0}
  +
  \omega_{K_1,S}
  \, v_{|K_1}
\]
where
\mbox{$
  K_0, K_1 \in\mathscr{T}_h
$}
such that
\mbox{$
  S=K_0\cap K_1
$}
and
where the two weights~$\omega_{K_0,S}$, $\omega_{K_1,S}$ 
express
\[
  \omega_{K_0,S} 
  =
  \dfrac{\eta_{|K_0}}
        {\eta_{|K_0} + \eta_{|K_1}}
  \ \ \mbox{ and }\ \ 
  \omega_{K_1,S}
  =
  \dfrac{\eta_{|K_1}}
        {\eta_{|K_0} + \eta_{|K_1}}
\]
Note that
\mbox{$
  \omega_{K_0,S} + \omega_{K_1,S} = 1
$}.
On a boundary side~\mbox{$S\subset\partial\Omega$},,
we set
\mbox{$
  \average{v}_\omega
  =
  v
$}.

The bilinear form $a_h(.,.)$ and linear for $l_h(.)$
are obtained by simply using the weighted average~$\average{.}_\omega$
instead of~$\average{.}$ and taking into accound upon te mixed
Dirichlet and Neumann boundary condition.
For all $u,v\in H^1(\mathcal{T}_h)$,
they are given by
(see e.g.~\citealp[p.~155]{PieErn-2012}, eqn. (4.64)):
\cindex{form!{$\jump{u}\jump{v}$}}%
\cindex{form!{$\jump{u}\average{\nabla_h v.{\bf n}}_w$}}%
\begin{empheq}{align*}
  a_h(u,v)
    &=
    \int_\Omega
	\eta
	\nabla_h u .\nabla_h v
	\,{\rm d}x
    +
    \sum_{S \in \mathscr{S}_h^{(i)}\cup \Gamma_d}
    \int_S
        \left(
	      \eta_s
	      \kappa_s
              \, \jump{u} \, \jump{v}
              - 
              \average{\eta\nabla_h u.{\bf n}}_w \, \jump{v}
              -
              \average{\eta\nabla_h v.{\bf n}}_w \, \jump{u}
        \right)
        \, {\rm d}s
    \\
  l_h(v)
    &=
    \int_\Omega
	f\,u 
	\,{\rm d}x
    +
    \int_{\Gamma_d}
        \left(
	    \kappa_s 
	    \, g 
            \, v 
          -
	    g
            \,\nabla_h v.{\bf n}
        \right)
	\,{\rm d}s
	%\label{eq-transmission-dg-lh}
\end{empheq}
where~\mbox{$
 \Gamma_d
  =
 \Gamma_{\rm left} \cup \Gamma_{\rm right}
$}
denote, for convenience,
the boundary domain corresponding to the non-homogeous Dirichlet boundary condition,
\mbox{$
  \eta_s
  =
  2/(1/\eta_{|K_0} + 1/\eta_{|K_1})
$}
is the geometric mean of the diffusion coefficient on an internal side
and~\mbox{$
  \kappa_s = \beta \,\varpi_s
$}
is the stabilization, as introduced in the previous section~\ref{dirichlet-dg}.
%
Finally, the discrete variational formulation writes:

\ \ $(FV)_h$: \emph{find $u_h\in X_h$ such that}
\begin{eqnarray*}
  a_h(u_h,v_h) &=& l_h(v_h), \ \forall v_h\in X_h
\end{eqnarray*}

The following code implement this problem in the \Rheolef\  environment.

% ---------------------------------------
\myexamplelicense{transmission_dg.cc}
% ---------------------------------------

% ------------------------------
\subsubsection*{Comments}
% ------------------------------
The test-case from
 section~\ref{transmission-dg},
page~\pageref{transmission-dg},
is  considered here:
recall that the exact solution is known.
The \code{gamma_d} boundary domain represents the union~$\Gamma_d$
of the two boundary domains~$\Gamma_{\rm left}$ and~$\Gamma_{\rm right}$
pre-defined by the mesh.
Conversely, the \code{Shd} domain groups the internal sides from~$\mathscr{S}_h^{(i)}$
with those of~$\Gamma_d$.
Note the usage of the summation for merging these domains.
\clindex{solver_option}%
Note the usage of the class \blue{\code{solver_option}} for imposing a direct 
linear solver: indeed, when~$d=3$, \Rheolef\ selects by default an iterative solver,
but due to  the bad condition number of the system, it is easier to use a direct one.
Running the one-dimensional case writes:
\begin{verbatim}
  make transmission_dg
  mkgeo_grid -e 10 -region > line.geo
  ./transmission_dg line P1d | field -
  ./transmission_dg line P2d | field -
\end{verbatim}

  \begin{figure}[htb]
    \begin{center}
       \begin{tabular}{c}
          \includegraphics[height=5.5cm]{transmission-dg-1d-fig.pdf}
       \end{tabular}
    \end{center}
    \caption{The discontinuous Galerkin method for the transmission problem with
                $\varepsilon=10^{-2}$, $d=1$, $P_{1d}$ approximation.}
    \label{fig-transmission-dg-view}
  \end{figure}
Fig.~\ref{fig-transmission-dg-view}
plots the one-dimensional solution when $k=1$ for two meshes.
Observe that the jumps at inter-element nodes decreases very fast with mesh refinement
and are no more perceptible on the plots.
Recall that the Dirichlet boundary conditions at $x=0$ and $x=1$ is only
weakly imposed: the corresponding jump at the boundary is small on the 
finer mesh.
When~\mbox{$k\geq 2$}, the approximate solution coincides with the interpolation
of the exact one, since the exact solution is piecewise quadratic and the
interface of discontinuity coincides with internal mesh sides.

The two-dimensional case writes:
\begin{verbatim}
  mkgeo_grid -t 10 -region > square.geo
  ./transmission_dg square P1d | field -elevation -
  ./transmission_dg square P2d | field -elevation -
\end{verbatim}
and the three-dimensional one
\begin{verbatim}
  mkgeo_grid -T 10 -region > cube.geo
  ./transmission_dg cube P1d | field -
  ./transmission_dg cube P2d | field -
\end{verbatim}

