set terminal cairolatex pdf color standalone
set output "cvge-helmholtz-s-sphere-l2.tex"

set logscale
set colors classic
set key bottom
set size square
set xrange [5e-3:5e-1]
set yrange [1e-9:1e-1]
set xlabel '[c]{$h$}'
set  label '[l]{$\|u_h-\pi_h(u)\|_{0,2,\Omega}$}' at graph 0.05,0.9
set xtics (\
	'[l]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-1}$}' 1e-1)
set ytics (\
	'[r]{$10^{-8}$}' 1e-8, \
	'[r]{$10^{-6}$}' 1e-6, \
	'[r]{$10^{-4}$}' 1e-4, \
	'[r]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{0}$}' 1e-0)

graph_ratio = 2.0/8.0

# triangle a droite, pente +2
slope_A = graph_ratio*2.0
xA =  0.10
yA =  0.63
dxA = 0.10
dyA = dxA*slope_A
set label "[l]{\\scriptsize $2=k+1$}" at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite, pente +3
slope_B = graph_ratio*3.0
xB =  0.10
yB =  0.21
dxB = 0.10
dyB = dxB*slope_B
set label "[l]{\\scriptsize $3$}" at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite, pente +4
slope_C = graph_ratio*4
xC =  0.16
yC =  0.04
dxC = 0.10
dyC = dxC*slope_C
set label "[l]{\\scriptsize $4$}" at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

plot \
 "cvge-helmholtz_s_sphere-P1-t.gdat" u (1./$1):($2) t "[r]{$k=1$}" w lp, \
 "cvge-helmholtz_s_sphere-P2-t.gdat" u (1./$1):($2) t "[r]{$k=2$}" w lp lc rgb '#008800', \
 "cvge-helmholtz_s_sphere-P3-t.gdat" u (1./$1):($2) t "[r]{$k=3$}" w lp

#pause -1 "<retour>"
# "cvge-helmholtz_s_sphere-P4-t.gdat" u (1./$1):($2) t "[r]{$k=4$}" w lp
