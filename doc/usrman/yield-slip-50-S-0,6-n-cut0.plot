set terminal cairolatex pdf color standalone
set output 'yield-slip-50-S-0,6-n-cut0.tex'

set size square
set colors classic
set xrange [0:1]
set yrange [0:0.4]
set xtics 1
set ytics 0.1
set xlabel '[c]{\large $x_0$}'
set  label '[l]{\large $u(x_0,0)$}' at graph 0.03,0.92

plot \
'yield-slip-50-n-1,5-S-0,6-cut0.gdat' t '[r]{$n=1.5$}' w l lc 1 dt 1 lt 1 lw 4, \
'yield-slip-50-n-1-S-0,6-cut0.gdat'   t '[r]{$n=1.0$}' w l lc rgb "#008800" dt 1 lt 1 lw 4, \
'yield-slip-50-n-0,5-S-0,6-cut0.gdat' t '[r]{$n=0.5$}' w l lc 3 dt 1 lt 1 lw 4

#pause -1 "<retour>"
