set terminal cairolatex pdf color standalone
set output 'p-laplacian-fixed-point-p=1,5-res.tex'

set size square
set colors classic

# h^{-2}*residue versus n
set xlabel '[c]{\large $n$}'
set  label '[l]{$\left\|r_h^{(n)}\right\|_{-1,h}$}' at graph 0.02,0.92
set  label '[c]{$p=3/2$}' at graph 0.55,0.6

set log y
set xrange [0:50]
set yrange [1e-15:1e5]
set xtics (0,25,50)
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$1$}" 1 )

plot \
'p-laplacian-fixed-point-n=10-p=1.5.gdat' t '[r]{$h=1/10$}' w lp lw 2, \
'p-laplacian-fixed-point-n=20-p=1.5.gdat' t '[r]{$h=1/20$}' w lp lw 2 lc '#008800', \
'p-laplacian-fixed-point-n=30-p=1.5.gdat' t '[r]{$h=1/30$}' w lp lw 2, \
'p-laplacian-fixed-point-n=40-p=1.5.gdat' t '[r]{$h=1/40$}' w lp lw 2, \
'p-laplacian-fixed-point-n=50-p=1.5.gdat' t '[r]{$h=1/50$}' w lp lw 2 pt 6

#pause -1 "<retour>"
