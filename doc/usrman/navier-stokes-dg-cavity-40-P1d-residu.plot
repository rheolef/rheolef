set terminal cairolatex pdf color standalone
set output "navier-stokes-dg-cavity-40-P1d-residu.tex"

set logscale y
set colors classic
set size square 
set xrange [0:25]
set yrange [1e-12:1e+5]
set xtics (0,5,10,15,20,25)
set ytics (\
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$10^{0}$}" 1 )
set xlabel '[c]{$n$}'

set  label '[r]{$\left\|r_h^{(n)}\right\|_{L^\infty}$}' at graph -0.01,0.92
set  label '[r]{$h=1/40, k=1$}' at graph 0.98,0.23

plot \
"navier-stokes-dg-cavity-40-P1d-Re=100-residu.gdat" \
	t '[r]{$Re=100$}' w lp, \
"navier-stokes-dg-cavity-40-P1d-Re=500-residu.gdat" \
	t '[r]{$500$}' w lp lc '#008800', \
"navier-stokes-dg-cavity-40-P1d-Re=1000-residu.gdat" \
	t '[r]{$1000$}' w lp

#pause -1
