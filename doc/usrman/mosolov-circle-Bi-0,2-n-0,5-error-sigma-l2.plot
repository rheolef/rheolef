set terminal cairolatex pdf color standalone
set output "mosolov-circle-Bi-0,2-n-0,5-error-sigma-l2.tex"

set size square
set log xy
set colors classic
set key bottom
set xrange [1e-2:1e-1]
set yrange [1e-3:1e-1]
graph_ratio_xy = 1./2.

set xlabel '[c]{\Large $h$}'
set  label '[l]{\Large $\|\boldsymbol{\sigma}-\boldsymbol{\sigma}_h\|_{0,2,\Omega}$}' at graph 0.03, 0.93
set xtics add ( \
	'[l]{$10^{-2}$}'	1e-2, \
	'[r]{$10^{-1}$}'	1e-1)
set ytics add ( \
	'[r]{$10^{-1}$}'	1e-1, \
	'[r]{$10^{-2}$}'	1e-2, \
	'[r]{$10^{-3}$}'	1e-3)

# triangle a droite
slope_A = graph_ratio_xy*1.0
xA =  0.35
yA =  0.58
dxA = 0.15
dyA = dxA*slope_A
set label "[l]{\\scriptsize $1$}" at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
slope_B = graph_ratio_xy*1.0
xB =  0.35
yB =  0.09
dxB = 0.15
dyB = dxB*slope_B
set label "[l]{\\scriptsize $1$}" at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

plot \
'mosolov-circle-Bi-0,2-n-0,5-error.gdat' \
  i 0 \
  u (1/$1):6 \
  t '[r]{$k=1$}' \
  w lp lw 6 lc 1, \
'mosolov-circle-Bi-0,2-n-0,5-error.gdat' \
  i 1 \
  u (1/$1):6 \
  t '[r]{$k=2$}' \
  w lp lw 6 lc rgb "#008800", \
'mosolov-circle-Bi-0,2-n-0,5-error.gdat' \
  i 2 \
  u (1/$1):6 \
  t '[r]{$k=3$}' \
  w lp lw 6 lc 3

#pause -1 "<return>"
