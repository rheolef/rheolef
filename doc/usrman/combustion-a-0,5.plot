set terminal cairolatex pdf color standalone
set output "combustion-a-0,5.tex"

set size square
set colors classic
set key center bottom
set xrange [0:1]
set samples 1000
set xlabel '[c]{\large $x$}'
set  label '[l]{\large $u(x)$}' at graph 0.02,0.95

a=0.5
lambda=8*a*a/cosh(a)**2
u(x)=2*log(cosh(a)/(cosh(a*(1-2*x))))

plot \
  u(x) title "[r]{exact}" lc 0 lt 1, \
  "combustion-10-a-0,5.gdat" title "[r]{$h=1/10$}" with lp lc 1 lt 1

#pause -1 "<return>"
