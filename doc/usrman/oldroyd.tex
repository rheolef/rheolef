% ---------------------------------------
\subsection{The Oldroyd model}
% ---------------------------------------
\newcommand{\fxp}  {\mathop{\rm fxp}}
\newcommand{\gxp}  {\mathop{\rm gxp}}
\cindex{problem!oldroyd}%
We consider the following viscoelastic fluid flow problem
(see e.g. \citealp[chap.~4]{Sar-2016-cfma}):

\ \ $(P)$: find $\tau$, $\boldsymbol{u}$ and $p$ defined in $]0,T[\times \Omega$ such that

\begin{subequations}
\begin{eqnarray}
    We\,\Frac{\mathscr{D}_a\tau}{\mathscr{D}t}
    + \tau
    - 2\alpha D(\boldsymbol{u}) 
    &=& 0
    \ \mbox{ in }\ 
    ]0,T[\times \Omega
    \label{eq-oldroyd-tau}
    \\
    Re\left(
      \Frac{\partial \boldsymbol{u}}{\partial t}
      +
      \boldsymbol{u}.\nabla\boldsymbol{u}
    \right)
    -
    {\bf div}
    \left(
      \,\tau
      +
      2(1-\alpha)D(\boldsymbol{u})
      -
      p.I
    \right)
    &=& 0
    \ \mbox{ in }\ 
    ]0,T[\times \Omega
    \label{eq-oldroyd-u}
    \\
    -{\rm div}\,\boldsymbol{u}
    &=& 0
    \ \mbox{ in }\ 
    ]0,T[\times \Omega
    \label{eq-oldroyd-p}
    \\
    \tau &=& \tau_\Gamma
    \ \mbox{ on }\ 
    ]0,T[\times \partial\Omega_-
    \label{eq-oldroyd-bc-tau}
    \\
    \boldsymbol{u} &=& \boldsymbol{u}_\Gamma
    \ \mbox{ on }\ 
    ]0,T[\times \partial\Omega
    \label{eq-oldroyd-bc-u}
    \\
    \tau(0) = \tau_0
    \ \mbox{ and }\ 
    \boldsymbol{u}(0) &=& \boldsymbol{u}_0
    \ \mbox{ in }\ 
    \Omega
    \label{eq-oldroyd-ic-tau-u}
\end{eqnarray}
\end{subequations}
where 
$\tau_0$,
$\boldsymbol{u}_0$,
$\tau_\Gamma$
and 
$\boldsymbol{u}_\Gamma$ are given.
The first equation corresponds to a generalized Oldroyd model \citep{Old-1950}:
when $a=-1$ we obtain the Oldroyd-A model,
when $a=1$, the Oldroyd-B model,
and when $a\in\,]-1,1[$ a generalization of these two models.
The dimensionless number $We\geq 0$ is  the Weissenberg number:
this is the main parameter for this problem.
The dimensionless Reynolds number $Re\geq 0$ is often chosen small:
as such fluids are usually slow, the $\boldsymbol{u}.\nabla\boldsymbol{u}$ inertia term
is also neglected here for simplicity.
The parameter $\alpha\in [0,1]$ represent a retardation.
When $\alpha=1$ we obtain the Maxwell model, that is a reduced version
of the Oldroyd one.
The total Cauchy stress tensor is expressed by:
\begin{eqnarray}
  \boldsymbol{\sigma}_{\rm tot} 
  &=&
  -p\,\boldsymbol{I}
  + 2(1-\alpha)D(\boldsymbol{u})
  + \boldsymbol{\tau}
  \label{eq-oldroyd-sigma-tot}
\end{eqnarray}

% ---------------------------------------
\subsection{The $\theta$-scheme algorithm}
% ---------------------------------------
The $\theta$-scheme is considered for the time
discretization \citep{Sar-1994} (see also \citealp[chap.~4]{Sar-2016-cfma}):
this leads to a semi-implicit splitting algorithm that defines
a sequence $(\tau^{(n)},\boldsymbol{u}^{(n)},p^n)_{n\geq 0}$ as
\begin{itemize}
  \item $n=0$: set $(\tau^{(0)},\boldsymbol{u}^{(0)})=(\tau_0,\boldsymbol{u}_0)$ and 
             $p^0$ arbitrarily chosen.
  \item $n\geq 0$: let $(\tau^{(n)},\boldsymbol{u}^{(n)})$ being known, 
	     then $(\tau^{(n+1)},\boldsymbol{u}^{(n+1)},p^{(n+1})$ is defined 
             in three sub-steps.
  \begin{itemize}
    \item[*] sub-step~1: compute explicitly:
    \begin{eqnarray*}
	\gamma &=& \boldsymbol{u}^{(n)}.\nabla \tau^{(n)} 
                    + \tau^{(n)} M_a\left(\boldsymbol{u}^{(n)}\right)
                    + M_a^T\left(\boldsymbol{u}^{(n)}\right) \tau^{(n)}
		    \\
	\tilde{\boldsymbol{f}} &=& 
                      \lambda \boldsymbol{u}^{(n)}
                    + {\bf div}\left(
		        c_1 \tau^{(n)} 
		        +
                        c_2 \gamma
                      \right)
    \end{eqnarray*}
    where
    \begin{eqnarray*}
      c_1 = \Frac{We}
                   {We+\theta\Delta t}
        \ \ \mbox{ and }\  \ 
      c_2 = - \Frac{We \theta\Delta t}
                   {We+\theta\Delta t}
    \end{eqnarray*}
    Then determine $(\boldsymbol{u}^{(n+\theta)},p^{(n+\theta)})$ such that
    \begin{subequations}
    \begin{eqnarray}
	\lambda \boldsymbol{u}^{(n+\theta)} - {\rm div}\left(2\eta D\left(\boldsymbol{u}^{(n+\theta)}\right)\right)
        +
        \nabla p^{(n+\theta)}
        &=&
	\tilde{\boldsymbol{f}}
        \ \mbox{ in }\ 
        \Omega
	\label{eq-theta-scheme-step1-u}
        \\
	-{\rm div}\,\boldsymbol{u}^{(n+\theta)} 
        &=&
	0
        \ \mbox{ in }\ 
        \Omega
	\label{eq-theta-scheme-step1-p}
        \\
        \boldsymbol{u} &=& \boldsymbol{u}_\Gamma((n+\theta)\Delta t)
        \ \mbox{ on }\ 
        \partial\Omega
	\label{eq-theta-scheme-step1-cl}
    \end{eqnarray}
    and finaly, compute explictely
    \begin{eqnarray}
	\tau^{(n+\theta)} &=& 
		        c_1 \tau^{(n)} 
		        +
                        c_2 \gamma
		        +
                        2 c_3
	                D\left(\boldsymbol{u}^{(n+\theta)}\right)
	\label{eq-theta-scheme-step1-tau}
    \end{eqnarray}
    \end{subequations}
    where
    \begin{eqnarray*}
      \lambda = \Frac{Re}
                     {\theta\Delta t}
        ,\ \ 
      \eta = \Frac{(1-\alpha)We + \theta\Delta t}
                   {We+\theta\Delta t}
        \ \ \mbox{ and }\  \ 
      c_3 = \Frac{\alpha\theta\Delta t}
                 {We+\theta\Delta t}
    \end{eqnarray*}
    \item[*] sub-step~2: $(\tau^{(n+\theta)},\boldsymbol{u}^{(n+\theta)})$ being known, 
    compute explictely
    \begin{eqnarray*}
	\boldsymbol{u}^{(n+1-\theta)}
	&=&
        \Frac{1-\theta}{\theta}
	\boldsymbol{u}^{(n+\theta)}
        -
        \Frac{1-2\theta}{\theta}
	\boldsymbol{u}^{(n)}
	\\
	\xi
	&=&
	c_4
        \tau_{n+\theta}
        +
        2 c_5
	D\left(\boldsymbol{u}^{(n+\theta)}\right)
    \end{eqnarray*}
    and then find $\tau^{(n+1-\theta)}$ such that
    \begin{subequations}
    \begin{eqnarray}
	&&
	\boldsymbol{u}^{(n+1-\theta)}.\nabla
        \tau^{(n+1-\theta)}
        + \tau^{(n+1-\theta)} M_a\left(\boldsymbol{u}^{(n+1-\theta)}\right)
        + M_a^T\left(\boldsymbol{u}^{(n+1-\theta)}\right) \tau^{(n+1-\theta)}
        \nonumber 
        \\
        && \phantom{aaaaaa}
        + \nu \tau^{(n+1-\theta)}
	=
	\xi
        \ \mbox{ in }\ 
        \Omega
        	\phantom{\left(\boldsymbol{u}^{(n+1-\theta)}\right)}
	\label{eq-theta-scheme-step2-tau}
        \\
	&&
        \tau^{(n+1-\theta)} = \tau_\Gamma((n+1-\theta)\Delta t)
        \ \mbox{ on }\ 
        \partial\Omega_-
	\label{eq-theta-scheme-step2-cl}
    \end{eqnarray}
    \end{subequations}
    where
    \begin{eqnarray*}
	\nu
	=	
	\Frac{1}{(1-2\theta)\Delta t}
	,\ \ 
	c_4
	=
	\Frac{1}{(1-2\theta)\Delta t}
	-
	\Frac{1}{We}
        \ \ \mbox{ and }\  \ 
	c_5
	=
	\Frac{\alpha}{We}
    \end{eqnarray*}
    \item[*] sub-step~3 is obtained by replacing $n$ and $n+\theta$ by
	$n+1-\theta$ and $n+1$, respectively.
  \end{itemize}
\end{itemize}
Thus, sub-step~1 and~2 reduces to two similar generalized Stokes problems
while sub-step~3 involves a stress transport problem.
Here $\Delta t>0$ and $\theta\in ]0,1/2[$ are numerical parameters. 
A good choice is $\theta=1-1/\sqrt{2}$ \citep{Sar-1997-b}.
This algorithm was first proposed by \citet{Sar-1990-these}
and extended \citep{Sar-1995} to Phan-Thien and Tanner viscoelastic models.
See also \citep{SinLea-1993} for another similar approach in the context
of FENE viscoelastic models.
\citet{Scu-2005} presented some benchmarks of this algorithm
while \citet{ChrErvJen-2009} presented a numerical analysis of its
convergence properties.
The main advantage of this time-depend algorithm is its flexibility:
while most time-dependent splitting algorithms for viscoelastic are limited to $\alpha \leq 1/2$
(see e.g. \citealp{PanHaoGlo-2009}), here the full range $\alpha\in ]0,1]$ is available.

Let us introduce the finite element spaces:
\begin{eqnarray*}
  T_h &=& \{
	\boldsymbol{\tau}_h \in (L^2(\Omega))^{d\times d}_s \ ;\ 
	\boldsymbol{\tau}_{h|K} \in (P_1)^{d\times d}_s
	,\ \forall K\in\mathscr{T}_h \}
	\\
  X_h &=& \{
	\boldsymbol{v}_h \in (H^1(\Omega))^{d} \ ;\ 
	\boldsymbol{\tau}_{h|K} \in (P_2)^{d}
	,\ \forall K\in\mathscr{T}_h \}
	\\
  Q_h &=& \{
	q_h \in L^2(\Omega) \ ;\ 
	q_{h|K} \in P_1
	,\ \forall K\in\mathscr{T}_h \}
\end{eqnarray*}
\apindex{P2-P1d, Scott-Vogelius}%
Note the discontinuous approximation of pressure:
it presents a major advantage,
as ${\rm div}(X_h)\subset Q_h$,
it leads to an exact \emph{divergence-free} approximation of the velocity:
for any field $\boldsymbol{v}_h\in X_h$ satisfying
\mbox{$
  \int_\Omega
    q_h
    \,{\rm div}(\boldsymbol{v}_h)
    \,{\rm d}x
  =
  0
$}
for all $q_h\in Q_h$,
we have ${\rm div}\,{\bf v}_h=0$ point-wise, everywhere in $\Omega$.
The pair $(X_h,Q_h)$ is known as the \citet*{ScoVog-1985} lowest-order finite
element approximation.
This is a major advantage when dealing with a transport equation.
The only drawback is that the pair $(X_h,Q_h)$ does not satisfy
the inf-sup condition for an arbitrary mesh.
There exists a solution to this however:
\citet*{ArnQin-1992} proposed
a macro element technique applied
to the mesh that allows satisfying the inf-sup condition:
for any triangular finite element mesh, it is sufficient
to split each triangle in three elements from 
its barycenter (see also \citealp{Sar-2014}).
Note that the macro element technique extends
to quadrilateral meshes~\citep{ArnQin-1992}
and to the three-dimensional case \citep{Zan-2005}.
%
By this way, the approximate velocity field satisfies exactly
the incompressibility constraint: this is an essential property for the
operator splitting algorithm to behave correctly,
combining stress transport equation with a divergence-free
velocity approximation.

The tensor transport term is discretized as in
the previous chapter, by using the~$t_h$ trilinear form introduced 
in~\eqref{eq-transport-tensor-def-th}
page~\pageref{eq-transport-tensor-def-th}.
The bilinear forms $b$, $c$, $d$ are defined by:
\begin{eqnarray*}
  b(\boldsymbol{\sigma}_h,\boldsymbol{v}_h)
  &=&
  \int_\Omega
    \boldsymbol{\sigma}_h
    \!:\!
    D(\boldsymbol{v}_h)
    \,{\rm d}x
  \\
  c(\boldsymbol{u}_h,\boldsymbol{v}_h)
  &=&
  \int_\Omega
    D(\boldsymbol{u}_h)
    \!:\!
    D(\boldsymbol{v}_h)
    \,{\rm d}x
  \\
  d(\boldsymbol{u}_h,\boldsymbol{q}_h)
  &=&
  \int_\Omega
    {\rm div}(\boldsymbol{u}_h)
    \,q_h
    \,{\rm d}x
\end{eqnarray*}
Let $T$, $B$, $C$, $D$ and $M$ be the discrete operators (i.e. the matrix)
associated to the forms $t_h$, $b$, $c$, $d$ and the $L^2$ scalar
product in $T_h$.
Assume that a stationnary state is reached for the discretized algorithm.
Then, \eqref{eq-theta-scheme-step1-u}-\eqref{eq-theta-scheme-step1-tau} writes
\begin{eqnarray*}
  We\,T\tau_h + M\tau_h - 2\alpha B^Tu_h &=& 0
  \\
  B\left( c_1 \tau_h + M^{-1}T\tau_h\right)
  + 2\eta C u_h
  + D^Tp_h
  &=&
  0
  \\
  D u_h &=& 0
\end{eqnarray*}
Note that 
\eqref{eq-theta-scheme-step2-tau}-\eqref{eq-theta-scheme-step2-cl}
reduces also to the first equation of the previous system.
Expanding the coefficients,
combining the two previous equations,
and using $C=BM^{-1}B^T$,
we obtain the system characterizing the stationary solution of the
discrete version of the algorithm:
\begin{eqnarray*}
  We\, T\tau_h + M\tau_h - 2\alpha B^Tu_h &=& 0
  \\
  B\tau_h 
  + 2(1-\alpha)C u_h
  + D^T p_h
  &=&
  0
  \\
  D u_h &=& 0
\end{eqnarray*}
Note that this is exactly the discretized version of the stationary equation%
\footnote{The original $\theta$-scheme presented by \citet{Sar-1994}
  has an additional relaxation parameter $\omega$. The present version correspond
  to $\omega=1$. When $\omega \neq 1$, the stationary solution still depends slightly
  upon $\Delta t$, as the stationary system do not simplifies completely.}%
.
In order to check that the solution reaches a stationary state,
the residual terms of this stationary equations are computed at each iteration,
together with the relative error between two iterates.
\vfill
% ---------------------------------------
\myexamplelicense{oldroyd_theta_scheme.h}

\myexamplelicense{oldroyd_theta_scheme1.h}

\myexamplelicense{oldroyd_theta_scheme2.h}

\myexamplelicense{oldroyd_theta_scheme3.h}
% ---------------------------------------

% -----------------------------------
\subsection{Flow in an abrupt ontraction}
% -----------------------------------
\cindex{geometry!contraction}%
\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=\textwidth]{contraction-fig.pdf}
  \end{center}
  \caption{
    The Oldroyd problem in the abrupt contraction:
    shematic view of the flow domain.
  }
  \label{fig-oldroyd-contraction}
\end{figure}
Fig.~\ref{fig-oldroyd-contraction} represents the contraction flow geometry.
Let us denote by
$\Gamma_{u}$,
$\Gamma_{d}$,
$\Gamma_{w}$ and
$\Gamma_{s}$
the upstream, downstream, wall and symmetry axis
boundary domains, respectively.
This geometry has already been studied in section~\ref{sec-p1bp1},
in the context of a Newtonian fluid.
Here, the fluid is more complex and additional boundary conditions
on the upstream domain are required for the extra-stress tensor$~\boldsymbol{\tau}$.

For the geometry,
we assume that the lengths $L_u$ and $L_d$ are sufficiently large
for the Poiseuille flows to be fully developped
at upstream and downstream.
Assuming also $a=1$, i.e. the Oldroyd-B model,
the boundary conditions at upstream are explicitely known
as the solution of the fully Poiseuille flow
for a plane pipe with half width $c$:
\begin{eqnarray*}
  u_0(x_1) &=& \bar{u}\left(1-\left(\Frac{x_1}{c}\right)^2\right)
	\\
  \dot{\gamma}(x_1) &=& u'_0(x_1) \ =\ - \Frac{2 \bar{u}\,x_1}{c^2}
	\\
  \tau_{00}(x_1) &=& 2\alpha We \, \dot{\gamma}^2(x_1)
	\\
  \tau_{01}(x_1) &=& \tau_{10}(x_1) \ =\ \alpha \, \dot{\gamma}(x_1)
	\\
  \tau_{11}(x_1) &=& 0
\end{eqnarray*}
where $\bar{u}$ denotes the maximal velocity
of the Poiseuille flow.
Without loss of generality, thanks to a dimensional analysis,
it can be adjused with the contraction ratio $c$
for obtaining a flow rate equal to one:
\begin{eqnarray*}
  \bar{u}
  &=&
  \left\{ \begin{array}{ll}
    3/(2c) & \mbox{for a planar geometry} \\
    4/c^2 & \mbox{for an axisymmetric one}
  \end{array} \right.
\end{eqnarray*}
%ICI

% ---------------------------------------
\exindex{contraction.h}%
\myexamplelicense{oldroyd_contraction.h}
% ---------------------------------------

\cindex{boundary condition!weakly imposed}%
The class \code{contraction}, already used for Newtonian
fluids, is here reused and extended with the boundary
condition function \code{tau_upstream}, as a derived
class \code{oldroyd_contraction}.
We are now able to write the \code{main} program
for solving a viscoelastic fluid flow problem
in a contraction.

% ---------------------------------------
\myexamplelicense{oldroyd_contraction.cc}%
% ---------------------------------------

\pindex{mkgeo_contraction}%
\pindexopt{mkgeo_contraction}{-split}%
The splitting element technique for the Scott-Vogelius element 
is implemented as an option by the command \code{mkgeo_contraction}.
This file it is not listed here but is available in the \Rheolef\  example directory.
The mesh generation for an axisymmetric contraction writes:
\begin{verbatim}
  mkgeo_contraction 3 -c 4 -zr -Lu 20 -Ld 20 -split
  geo contraction.geo
\end{verbatim}
This command generates a mesh for the axisymmetric 4:1 abrupt contraction
with upstream and downstream length $L_u=L_d=20$.
Such high lengths are required for the Poiseuille flow
to be fully developped at upstream and downstream for large values of $We$.
The $3$ first argument of \code{mkgeo_contraction}
is a number that characterizes the mesh density:
when increasing, the average edge length decreases.
Then, the program is started:
\begin{verbatim}
  make oldroyd_contraction
  mpirun -np 8 ./oldroyd_contraction contraction.geo 0.1 10 0.01 > contraction.branch
\end{verbatim}
The program computes stationnary solutions by increasing $We$ with the
time-dependent algorithm.
It performs a continuation algorithm,
using solution at a lowest $We$ as initial condition.
The reccurence starts from an the Newtonian solution associated to $We=0$.
The others model parameters for this classical benchmark
are fixed here as $\alpha=8/9$ and $a=1$ (Oldroyd-B model).
The computation can take a while as there are two loops:
one outer, on $We$, and the other inner on time,
and there are two generalized
Stokes subproblem and one tensorial transport one to solve at each
time iteration.
The inner time loop stops when the relative error is small enough.
\pindex{mpirun}%
\cindex{distributed computation}%
\cindex{parallel computation}%
Thus, the parallel run, when available, is a major advantage:
it is obtained by adding~\code{mpirun -np 8} 
at the begining of the command line.
Recall that the time scheme is conditionnaly stable:
the time step should be small enough for the algorithm
to converge to a stationnary solution.
When the solver fails, it restarts with a smaller time step.
Note that the present solver can be dramatically improved:
by using a Newton method, as shown by \citet{Sar-2014},
it is possible to directly reach the stationnary solution,
but such more sophisticated implementation
is out of the scope of the present documentation.

\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{ll}
      \begin{minipage}[b][4.5cm][t]{2cm} \mbox{$We=0.1$} \end{minipage}
	& \includegraphics[width=9cm]{{oldroyd-contraction3-zr-We-0.1}.png}
        \\
      \begin{minipage}[b][4.5cm][t]{2cm} \mbox{$We=0.3$} \end{minipage}
	& \includegraphics[width=9cm]{{oldroyd-contraction3-zr-We-0.3}.png}
        \\
      \begin{minipage}[b][4.5cm][t]{2cm} \mbox{$We=0.7$} \end{minipage}
	& \includegraphics[width=9cm]{{oldroyd-contraction3-zr-We-0.7}.png}
    \end{tabular}
  \end{center}
  \caption{
	The Oldroyd problem in the axisymmetric contraction:
	stream function for $We=0.1$, $0.3$ and~$0.7$, from top to bottom.
  }
  \label{fig-oldroyd-contraction-psi}
\end{figure}

\pindexopt{field}{-n-iso-negative}%
\pindexopt{branch}{-toc}%
\pindexopt{branch}{-extract}%
\pindexopt{branch}{-branch}%
The visualization of the stream function writes:
\begin{verbatim}
  branch contraction.branch -toc
  branch contraction.branch -extract 3 -branch > contraction-We-0.3.field 
  make streamf_contraction
  field -mark u contraction-We-0.3.field -field | ./streamf_contraction > psi.field
  field psi.field -n-iso 15 -n-iso-negative 10
  field psi.field -n-iso 15 -n-iso-negative 10 -bw
\end{verbatim}
\exindex{streamf_contraction.cc}%
The file \file{streamf_contraction.cc} has already
been studied in section~\ref{sec-p1bp1},
in the context of a Newtonian fluid.
The result is shown on Fig.~\ref{fig-oldroyd-contraction-psi}.
The vortex growths with $We$: this is the major effet observed
on this problem.
Observe the two color maps representation:
positive values of the stream functions are associated to
the vortex and negatives values to the main flow region.
The last command is a black-and-white variant view.

\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=7cm]{oldroyd-contraction-zr-psi-max.pdf}
  \end{center}
  \caption{
	The Oldroyd-B problem in the axisymmetric contraction:
	vortex activity vs $We$ ($\alpha=8/9$).
  }
  \label{fig-oldroyd-contraction-psi-max}
\end{figure}
%
The vortex activity is obtained by:
\begin{verbatim}
  field psi.field -max
\end{verbatim}
Recall that the minimal value of the stream function
is $-1$, thanks to the dimensionless procedure used here.


\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{ll}
      \includegraphics[width=0.45\textwidth]{oldroyd-contraction3-zr-u0-axis.pdf}    &
      \includegraphics[width=0.45\textwidth]{oldroyd-contraction3-zr-tau00-axis.pdf} \\
      \includegraphics[width=0.45\textwidth]{oldroyd-contraction3-zr-tau11-axis.pdf} &
      \includegraphics[width=0.45\textwidth]{oldroyd-contraction3-zr-tau22-axis.pdf}
    \end{tabular}
  \end{center}
  \caption{
	The Oldroyd problem in the axisymmetric contraction:
	velocity and stress components along the axis.
  }
  \label{fig-oldroyd-contraction-axis}
\end{figure}
%
Cuts along the axis of symmetry are obtained by:
\begin{verbatim}
  field contraction-We-0.3.field -domain axis -mark u   -comp 0  -elevation -gnuplot
  field contraction-We-0.3.field -domain axis -mark tau -comp 00 -elevation -gnuplot
  field contraction-We-0.3.field -domain axis -mark tau -comp 11 -elevation -gnuplot
  field contraction-We-0.3.field -domain axis -mark tau -comp 22 -elevation -gnuplot
\end{verbatim}
These cuts are plotted on Fig.~\ref{fig-oldroyd-contraction-axis}.
Observe the overshoot of the velocity along the axis
when $We>0$ while this effect is not perceptible when $We=0$.
Also, the normal extra stress $\tau_{zz}$ growth dramatically
in the entry region.

