set terminal cairolatex pdf color standalone
set output "obstacle-slip-ux-xaxis.tex"

set size square
set xrange [0:4]
set yrange [0:1]
set xtics 1
set ytics 1
set xlabel '[r]{$x_0$}' 
set  label '[l]{$1\!-\!u_0(x_1)$}' at graph 0.015, 0.7

plot \
"obstacle-slip-ux-xaxis.gdat" \
  u 1:(1-$3) \
  t 'cylinder' \
  with l lw 2 lc rgb '#000000', \
"obstacle-zr-slip-ux-xaxis.gdat" \
  u 1:(1-$3) \
  t 'sphere' \
  with l lw 2 lc rgb '#ff0000'

#pause -1 "<return>"
