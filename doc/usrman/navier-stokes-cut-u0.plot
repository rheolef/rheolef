set terminal cairolatex pdf color standalone
set output 'navier-stokes-cut-u0.tex'
#
# comparaison NS cavite avec [GhiGhiShi-1982], table 1
# USAGE:
# ./new-navier-stokes-cavity-cahcha square-Re=100.geo 100 0.2 5 
# mfield square-Re=100-5.mfield.gz -u0 |field -cut -normal -1 0 -origin 0.5 0 -noclean -gnuplot -

set size square
set colors classic
set xlabel '[c]{\Large $u_0(0.5,x_1)$}'
set  label '[r]{\Large $x_1$}' at graph -0.02,0.75
set xtics (-0.5,0,0.5,1)
set ytics (0,0.5,1)
set key right bottom at graph 0.99,0.25

plot [-0.5:1.0][0:1] \
  'navier-stokes-Re=100-dt=0.05-t2-2-cut-u0.gdat' u 2:1 \
	title '[r]{$Re=100\phantom{0}$}' w l dt 1 lc 1 lw 2, \
  'navier-stokes-bench-cut-u0.gdat' u 3:2 \
	title '[r]{data: $Re=100\phantom{0}$}' w p ps 0.5 pt 7 lc 1, \
  'navier-stokes-Re=400-dt=0.05-t2-2-cut-u0.gdat' u 2:1 \
	title '[r]{$Re=400\phantom{0}$}' w l dt 2 lc rgb '#008800' lw 2, \
  'navier-stokes-bench-cut-u0.gdat' u 4:2 \
	title '[r]{data: $Re=40\phantom{0}0$}' w p ps 0.5 pt 9 lc rgb '#008800', \
  'navier-stokes-Re=1000-dt=0.02-t2-2-cut-u0.gdat' u 2:1 \
	title '[r]{$Re=1000$}' w l dt 3 lc 3 lw 2, \
  'navier-stokes-bench-cut-u0.gdat' u 5:2 \
	title '[r]{data: $Re=1000$}' w p ps 0.5 pt 11 lc 3

#pause -1 '<return>'
