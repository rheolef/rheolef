set terminal cairolatex pdf color standalone
set output "p-laplacian-newton-square-r2.tex"

set logscale y
set colors classic
set size square 
set xtics (0,25)
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$10^{0}$}" 1 )
set xlabel '[c]{$n$}'
set  label '[r]{$\left\|r_h^{(n)}\right\|_{-1,h}$}' at graph -0.01,0.92

plot [0:25][1e-15:1e5] \
"p-laplacian-newton-square-p=2.5.gdat" title "[r]{$p=2.5$}" w lp lw 2, \
"p-laplacian-newton-square-p=3.0.gdat" title "[r]{$p=3.0$}" w lp lw 2 lc rgb '#008800', \
"p-laplacian-newton-square-p=3.5.gdat" title "[r]{$p=3.5$}" w lp lw 2

#pause -1 "<retour>"
