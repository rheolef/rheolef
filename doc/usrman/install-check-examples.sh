#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
# special files that are documented and installed with another name
MAIN_TEX="
rheolef
"

FORCE_IDX="
Makefile.demo
"
FORCE_EXF="
Makefile
"

# -----------------------------------------
# uniq list
# -----------------------------------------
uniq_list () {
  tmp="/tmp/uniq-list-$$"
  rm -f $tmp
  for x in $*; do
    echo $x >> $tmp
  done
  list=`sort -u $tmp`
  rm -f $tmp
  echo $list
}
# -----------------------------------------
# get installed examples from command line
# -----------------------------------------
EXF=`uniq_list $FORCE_EXF $*`

# -----------------------------------------
# get documented files from example indexes
# -----------------------------------------
IDX="$FORCE_IDX"
for f in $MAIN_TEX; do
  if test ! -f $f.exx; then
    echo "$0: missing $f.exx file" 1>&2
    if test $f = rheolef; then
      exit 1
    fi
  fi
  tmp="tmp-exx-$f.log"
  #tmp="/tmp/exx-$$"
  grep '^\\indexentry' $f.exx |    \
    sed -e 's/{[0-9][0-9]*}//' 	   \
       -e 's/}.*$//' 		   \
       -e 's/^.*{//'          |    \
    sort -u                        \
    > $tmp
  IDX="$IDX `cat $tmp`"
  rm -f $tmp
done
IDX=`uniq_list $IDX`;

#echo "IDX=$IDX"
#echo "EXF=$EXF"
# ------------------
# idx not in exf 
# ------------------
status=0
for x in $IDX; do
  #echo "doc file $x..." 1>&2
  founded=false
  for e in $EXF; do
    if test $x = $e; then
      #echo "doc file $e FOUNDED in examples" 1>&2
      founded=true
      break;
    fi
  done
  if $founded; then  
     continue; 
  fi
  echo "doc file $x is missing in examples dir" 1>&2
  status=1
done
# ------------------
# exf not in idx 
# ------------------
for e in $EXF; do
  #echo "example file $e..." 1>&2
  founded=false
  for x in $IDX; do
    if test $x = $e; then
      founded=true
      break;
    fi
  done
  if $founded; then 
    #echo "example file $e FOUNDED in doc" 1>&2
    continue;
  fi
  echo "example file $e is missing in doc" 1>&2
  status=1
done
# ------------------
# epilogue
# ------------------
if test $status -ne 0; then
  echo "$0: problem in example documentation or installation" 1>&2
fi
exit $status
