set terminal cairolatex pdf color standalone
set output "combustion_arc_umax.tex"

set size square
set colors classic
set xrange [0:4]
set yrange [0:4]
set xtics (0,1,2,3,4)
set ytics (0,1,2,3,4)
set xlabel '[c]{\large $\lambda$}'
set  label '[l]{\Large $\|u_h\|_{0,\infty,\Omega}$}' at graph 0.02,0.90

plot \
'line-10-arc-umax.gdat' u 2:3 title '[r]{$h=1/10$}' w lp lc 1 dt 2 lw 2, \
'line-80-arc-umax.gdat' u 2:3 title '[r]{$h=1/80$}' w lp lc 0 dt 1 lw 2

#pause  -1 "<retour>"
