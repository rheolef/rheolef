set terminal cairolatex pdf color standalone
set output "convect_cvge_Pk_err_linf_linf.tex"

set size square 
set colors classic
set log xy
set xrange [4e-4/3:4e-0/3]
set yrange [5e-4:5]
graph_ratio = 4.0/4.0

#set label '[r]{$\|\phi_h-\pi_h(\phi)\|_{L^2(L^2)}$}' at graph -0.05,0.9
set xlabel '[r]{$h$}'
set label  '[l]{\small $\Delta t=2\pi/50$}'  at graph 0.02,0.20
set label  '[l]{\small $\Delta t=2\pi/100$}' at graph 0.02,0.13
set label  '[l]{\small $\Delta t=2\pi/200$}' at graph 0.02,0.06

plot \
"convect_cvge_Pk.gdat" \
	i 0 u (8.0/$1):3 \
	not "P1 err_linf_linf dt=50" \
	w lp lc 1 lt 1 pt 1, \
"convect_cvge_Pk.gdat" \
	i 1 u (8.0/$1):3 \
	not "P1 err_linf_linf dt=100" \
	w lp lc 1 lt 1 pt 2, \
"convect_cvge_Pk.gdat" \
	i 2 u (8.0/$1):3 \
	t '[c]{$P_1$}' \
	w lp lc 1 lt 1 pt 3, \
"convect_cvge_Pk.gdat" \
	i 3 u (8.0/$1):3 \
	not "P2 err_linf_linf dt=50" \
	w lp lc 3 lt 1 pt 1, \
"convect_cvge_Pk.gdat" \
	i 4 u (8.0/$1):3 \
	not "P2 err_linf_linf dt=100" \
	w lp lc 3 lt 1 pt 2, \
"convect_cvge_Pk.gdat" \
	i 5 u (8.0/$1):3 \
	t '[c]{$P_2$}' \
	w lp lc 3 lt 1 pt 3

#pause -1 "<retour>"
