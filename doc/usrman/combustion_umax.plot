set terminal cairolatex pdf color standalone
set output "combustion_umax.tex"

lambda_c = 3.51383071912516
set xlabel '[c]{\large $\lambda$}'
set  label '[l]{\Large $\|u_h\|_{0,\infty,\Omega}$}' at graph 0.02,0.95

set key top center
set colors classic
set xrange [0:4]
set yrange [0:1.5]
set xtics (0,1,2,3,4)
set ytics (0,1)
set xtics add ('[c]{$\lambda_c$}' lambda_c)
plot \
  'combustion_umax.gdat' i 2 u 2:3 t '[r]{$h=1/160$}' w lp lw 3 dt 1 lc 0, \
  'combustion_umax.gdat' i 0 u 2:3 t '[r]{$h=1/10$}'  w lp lw 3 dt 2 lc 1

#pause -1 "<retour>"
