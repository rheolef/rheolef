\section{Migrating to \Rheolef\  version 6.0}

Due to its new distributed memory and computation support,
\Rheolef\  version 6.0 presents some backward incompatibilities
with previous versions: codes using previous versions of the 
library should be slightly modified.
This appendix presents some indications for migrating existing code.

% ===========================================================================
\subsection{What is new in \Rheolef\  6.0 ?}
% ===========================================================================

The major main features are:
\cindex{distributed computation}%
\pindexopt{library}{MPI, message passing interface}%
\begin{itemize}
  \item support {\bf distributed achitectures}: the code looks sequential, is easy to read and write
    but can be run massively parallel and distributed, based on the MPI library.

\apindex{high-order}%
\apindex{Pk}%
  \item {\bf high order polynomial} approximation: 
    $P_k$ basis are introduced in this version, for $k\geq 0$.
    This feature will be improved in the future developments.

\cindex{method!characteristic}%
\cindex{mesh!adaptation}%
  \item {\bf mesh adaptation} and the {\bf charateristic method} are now available 
        for {\bf three-dimensional} problems.
\end{itemize}
In order to evoluate in these directions, internal data structures inside the library
are completely rewritten in a different way, and thus this version is a completely new library.

Conversely, the library and unix command interfaces was as less as possible modified.

Nevertheless, the user will find some few backward incompatibilities:
5.93 based codes will not directly compile with the 6.0 library version.
Let us review how to move a code from 5.93 to 6.0 version.

% ===========================================================================
\subsection{What should I have to change in my 5.x code~?}
% ===========================================================================

\subsubsection*{1. Namespace}
% ---------------------------
\cindex{namespace!rheolef}%
The \code{namespace rheolef} was already introduced in last 5.93 version.
Recall that a code usually starts with:
\begin{lstlisting}[numbers=none,frame=none]
  #include "rheolef.h"
  using namespace rheolef;
\end{lstlisting}

% ----------------------------
\subsubsection*{2. Environment}
% ---------------------------
\clindex{environment}%
\pindexopt{library}{boost}%
\cindex{argc, argv, command line arguments}%
The MPI library requires initialisation and the two command line arguments.
This initialisation is performed via the \code{boost::mpi} class \code{environment}:
The code entry point writes:
\begin{lstlisting}[numbers=none,frame=none]
  int main (int argc, char** argv) {
  environment rheolef (argc,argv);
  ...
\end{lstlisting}
% --------------------------------------------------
\subsubsection*{3. Fields and forms data accessors}
% -------------------------------------------------
\clindex{field}%
The accesses to unknown and blocked data was of a \code{field uh} 
was direct, as \code{uh.u} and \code{uh.b}.
This access is no more possible in a distributed environment, as
non-local value requests may be optimized and thus, read and 
write access may be controled thought accessors.
These accessors are named \code{uh.u()} and \code{uh.b()}
for read access, and \code{uh.set_u()} and \code{uh.set_b()}
for write access.
Similarly, a \code{form a} has accessors as \code{a.uu()}.

A typical 5.93 code writes:
\begin{lstlisting}[numbers=none,frame=none]
  ssk<Float> sa = ldlt(a.uu);
  uh.u = sa.solve (lh.u - a.ub*uh.b);
\end{lstlisting}
and the corresponding 6.0 code is:
\begin{lstlisting}[numbers=none,frame=none]
  solver sa (a.uu());
  uh.set_u() = sa.solve (lh.u() - a.ub()*uh.b());
\end{lstlisting}

This major change in the library interface induces the most important
work when porting to the 6.0 version.

\clindex{solver}%
Note also that the old \code{ssk<Float>} class has been
supersetted by the \code{solver} class, that manages
both direct and iterative solvers in a more effective way.
For three-dimensional problems, the iterative solver is the default
while direct solvers are used otherwise.
\clindex{solver_abtb}%
In the same spirit, a \code{solver_abtb} has been introduced,
for Stokes-like mixed problem.
These features facilitate the dimension-independent coding style
provided by the \Rheolef\  library.

% ---------------------------------------------------------------------------
\subsubsection*{4. Distributed input and output streams}
% --------------------------------------------------------------------------
\cindex{variable!\code{din}}%
\cindex{variable!\code{dout}}%
\cindex{variable!\code{derr}}%
Input and output {\em sequential} standard streams \code{cin}, \code{cout} and \code{cerr}
may now replaced by {\em distributed} \Rheolef\  streams \code{din}, \code{dout} and \code{derr} as:
\begin{lstlisting}[numbers=none,frame=none]
  din >> omega;
  dout << uh;
\end{lstlisting}
\clindex{idiststream}%
\clindex{odiststream}%
These new streams are available togeher with the \code{idiststream} and \code{odiststream}
classes of the \Rheolef\  library.

% ---------------------------------------------------------------------------
\subsubsection*{5. File formats \filesuffix{.geo} and \filesuffix{.field} have changed}
% --------------------------------------------------------------------------
\fiindex{\filesuffix{.geo} mesh}%
\fiindex{\filesuffix{.field} field}%
\fiindex{\filesuffix{.field} multi-component field}%
The \filesuffix{.geo} and \filesuffix{.field} file formats have changed.
The \filesuffix{.mfield} is now obsolete: it has been merged into
the \filesuffix{.field} format that supports now multi-component fields.
\pindex{field}%
Also, the corresponding \code{mfield} unix command is obsolete,
as these features are integrated in the \code{field} unix command.

%TODO: relire les anciens formats en sequentiel et les convertir
At this early stage of the 6.0 version, it is not yet possible to read
the old \filesuffix{.geo} format, but this backward compatibility will be
assured soon.

% ---------------------------------------------------------------------------
\subsubsection*{6. Space on a domain}
% --------------------------------------------------------------------------
\clindex{space}%
A space defined on a domain~\code{"boundary"} of a mesh \code{omega}
was defined in the 5.93 version as:
\begin{lstlisting}[numbers=none,frame=none]
  space Wh (omega["boundary"], omega, "P1");
\end{lstlisting}
It writes now:
\begin{lstlisting}[numbers=none,frame=none]
  space Wh (omega["boundary"], "P1");
\end{lstlisting}
as the repetition of \code{omega} is no more required.

% ---------------------------------------------------------------------------
\subsubsection*{7. Nonlinear expressions involving fields}
% --------------------------------------------------------------------------
Non-linear operations, such as \code{sqrt(uh)} or \code{1/uh} was directly supported in \Rheolef-5.x.
\begin{lstlisting}[numbers=none,frame=none]
  space Xh (omega, "P1");
  field uh (Xh, 2.);
  field vh = 1/uh;
\end{lstlisting}
Note that non-linear operations as \code{1/uh} do not returns in general picewise polynomials
while \code{uh*uh} is piecewise quadratic.
In \Rheolef-5.x, the returned value was implicitly the Lagrange interpolant of the nonlinear expression in 
space \code{Xh}.
\findex{interpolate}%
For more clarity, \Rheolef-6.x requires an explicit call to the \code{interpolate} function
and the code should write:
\begin{lstlisting}[numbers=none,frame=none]
  field vh = interpolate (Xh, 1/uh);
\end{lstlisting}
Note that when the expression is linear, there is no need to call \code{interpolate}.

% ===========================================================================
\subsection{New features in \Rheolef\  6.4}
% ===========================================================================

The \Rheolef-6.x code is in active developments.
While backward compatibility s maintained since 6.0,
some styles and idioms evoluates in order to
increase the expressivity and the flexibility of
the interface library.
Here is the summary of these evolutions.
% ---------------------------------------------------------------------------
\subsubsection*{1. Nonlinear expressions}
% --------------------------------------------------------------------------
\cindex{functor}%
Nonlinear expressions has been extended since \Rheolef-6.4 to expression mixing
\code{field} and functions or functors.
For instance, when \code{u_exact} is a functor, an
$L^2$ error could be computed using a nonlinear expression submitted to
the \code{integrate} function:
\begin{lstlisting}[numbers=none,frame=none]
    Float err_l2  = sqrt (integrate (omega, sqr (uh - u_exact()), qopt));
\end{lstlisting}
\clindex{field_functor}%
Until Rheolef version 6.6, the class \code{field_functor} was used
to mark such functors. From version 6.7,
the \code{field_functor} class is obsolete and any function or functor
that is callable with a \code{point} as argument is valid.
% ---------------------------------------------------------------------------
\subsubsection*{2. Right-hand-side specification}
% --------------------------------------------------------------------------
For specifiying a right-hand-side involving \code{f},
previous code style, from \Rheolef-6.0 to 6.3 was using:
\findex{riesz}%
\begin{lstlisting}[numbers=none,frame=none]
    field lh = riesz (Xh, f());
\end{lstlisting}
\findex{integrate}%
\Rheolef-6.4 introduces:
\begin{lstlisting}[numbers=none,frame=none]
    test v (Xh);
    field lh = integrate (f()*v);
\end{lstlisting}
This feature opens new possibilities of right-hand-side specifications,
e.g. expressions involving some derivatives of the test-function \code{v}.
The \code{riesz} function is no more needed: it is maintained for
backward compatibility purpose.
% ---------------------------------------------------------------------------
\subsubsection*{3. Form specification}
% --------------------------------------------------------------------------
For specifiying a bilinear form,
previous code style, from \Rheolef-6.0 to 6.3 was using
a specification based on a name:
\begin{lstlisting}[numbers=none,frame=none]
    form a (Xh, Xh, "grad_grad");
\end{lstlisting}
\findex{integrate}%
\Rheolef-6.4 introduces:
\begin{lstlisting}[numbers=none,frame=none]
    trial u (Xh); test v (Xh);
    form a = integrate (dot(grad(u),grad(v)));
\end{lstlisting}
This feature opens new possibilities for form specifications
and more flexibility.
The \code{form} specification based on a name is no more needed: it is maintained for
backward compatibility purpose.

\vfill
