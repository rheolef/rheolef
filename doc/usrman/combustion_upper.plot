set terminal cairolatex pdf color standalone
set output "combustion_upper.tex"

# line-10:
#lambda = 0.012158390911425
#alpha  = 5.67353669534372

# line-20:
lambda = 0.0103965533859998
alpha  = 5.76839201515163

u(a,x)=2*log(cosh(a)/(cosh(a*(1-2*x))))

set size square
set colors classic
set key center bottom
set xrange [0:1]
set samples 1000
set xlabel '[c]{\large $x$}'
set  label '[l]{\large $u(x)$}' at graph 0.02,0.95

plot \
u(alpha,x) t '[r]{exact}' w l lc 0 lw 1, \
'line-20-upper.gdat' t '[r]{$h=1/20$}' w lp lc 1 lt 1 lw 1

#pause -1 "<retour>"
