set terminal cairolatex pdf color standalone
set output "p-laplacian-relax-1.tex"

set size square 
set colors classic
set key left at graph 0.1,0.95
set xrange [0:2]
set yrange [0:0.6]
set xtics 0.5
set ytics (0,0.1,0.2,0.3,0.4)

set xlabel '[c]{\large $\omega$}'
set  label '[r]{\Large $\bar{v}$}' at graph -0.05,0.95


plot \
'p-laplacian-relax-p=1.2.gdat' t '[r]{$p=1.2$}' w lp lw 2, \
'p-laplacian-relax-p=1.3.gdat' t '[r]{$p=1.3$}' w lp lw 2 lc rgb '#008800', \
'p-laplacian-relax-p=1.4.gdat' t '[r]{$p=1.4$}' w lp lw 2, \
'p-laplacian-relax-p=1.5.gdat' t '[r]{$p=1.5$}' w lp lw 2

#pause -1 "<retour>"
