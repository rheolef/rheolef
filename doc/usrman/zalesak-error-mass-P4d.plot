set terminal cairolatex pdf color standalone
set output "zalesak-error-mass-P4d.tex"

set size square
set key bottom
set xrange [0:4*pi]
set yrange [-0.05:0.05]
set xlabel '[c]{$t_n$}'
set  label '[l]{$V_h^{(n)}$}' at graph 0.05,0.88
set  label '[r]{$k=4$}' at graph 0.95,0.32
set xtics (\
	0, \
	'[r]{$4\pi$}' 4*pi)
set ytics (-0.05, 0, 0.05)

plot \
'zalesak-error-mass-P4d.gdat' \
	 i 0 every 15 \
	 t '[r]{$h=0.04$}' \
	 w lp lt 1 lw 1 lc rgb '#008800', \
'zalesak-error-mass-P4d.gdat' \
	 i 1 every 15 \
	 t '[r]{$h=0.02$}' \
	 w lp lt 1 lw 1 lc rgb '#0000ff', \
'zalesak-error-mass-P4d.gdat' \
	 i 2 t '[r]{$h=0.01$}' \
	 w l lt 1 lw 1 lc rgb '#ff00ff'

#pause -1 '<retour>'
