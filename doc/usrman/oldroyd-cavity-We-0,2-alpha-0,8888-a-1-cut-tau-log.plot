set terminal cairolatex pdf color standalone
set output "oldroyd-cavity-We-0,2-alpha-0,8888-a-1-cut-tau-log.tex"

set log x
set key bottom
set size square 
set xrange [1e-2:1e+2]
set yrange [0:1]
set xtics (\
        "[c]{$10^{-2}$}" 1e-2, \
        "[c]{$10^{-1}$}" 1e-1, \
        "[c]{$1$}" 1, \
        "[c]{$10$}" 10,\
        "[c]{$100$}" 100 )
set ytics       (0, 0.5, 1)
set xlabel '[c]{$\log\tau_{00}(1/2,x_1)$}'
set  label '[r]{$x_1$}' at graph -0.02, 0.75


plot \
"oldroyd-cavity-40-We-0,2-alpha-0,8888-a-1-cut-tau.gdat" \
  u 2:1 \
  t '[r]{$h=1/40$}' \
  w l, \
"oldroyd-cavity-20-We-0,2-alpha-0,8888-a-1-cut-tau.gdat" \
  u 2:1 \
  t '[r]{$h=1/20$}' \
  w p, \
"oldroyd-cavity-10-We-0,2-alpha-0,8888-a-1-cut-tau.gdat" \
  u 2:1 \
  t '[r]{$h=1/10$}' \
  w p

#pause -1 "<retour>"
