set terminal cairolatex pdf color standalone
set output 'p-laplacian-square-newton-p=3-Pk.tex'

set size square
set colors classic
set log y
set xrange [0:25]
set xtics (0,5,10,15,20,25)
set yrange [1e-15:1e5]
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$1$}" 1 )
set xlabel '[c]{\large $n$}'
set  label '[l]{$\left\|r_h^{(n)}\right\|_{-1,h}$}' at graph 0.01,0.92
set  label '[c]{$p=3$}' at graph 0.5,0.60

plot \
'p-laplacian-square-newton-p=3-P1.gdat' \
	t '[r]{$k=1$}' w lp lw 2, \
'p-laplacian-square-newton-p=3-P2.gdat' \
	t '[r]{$k=2$}' w lp lw 2 lc rgb '#008800', \
'p-laplacian-square-newton-p=3-P3.gdat' \
	t '[r]{$k=3$}' w lp lw 2, \
'p-laplacian-square-newton-p=3-P4.gdat' \
	t '[r]{$k=4$}' w lp lw 2, \
'p-laplacian-square-newton-p=3-P5.gdat' \
	t '[r]{$k=5$}' w lp lw 2

#pause -1 "<retour>"
