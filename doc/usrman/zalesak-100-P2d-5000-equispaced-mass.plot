set terminal cairolatex pdf color standalone
set output "zalesak-100-P2d-5000-equispaced-mass.tex"

gdat = 'zalesak-100-P2d-5000-equispaced.gdat' 

set size square
set xrange [0:100]
set yrange [0:1e-2]
set xlabel '[c]{$r$}'
set  label '[l]{$V_h^{(n_{\max})}$}' at graph 0.09, 0.9
set xtics 20
set ytics (\
    '[r]{$10^{-2}$}'             1e-2, \
    '[r]{$5\!\times\! 10^{-3}$}' 5e-3, \
    '[r]{$0$}'                   0)

plot \
gdat \
  every 2 u 1:3 \
  not 'v-tf' \
  w lp pt 7 ps 0.25 lw 2 lc 0

#pause -1
