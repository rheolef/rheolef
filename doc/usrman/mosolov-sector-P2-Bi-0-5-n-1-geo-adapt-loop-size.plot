set terminal cairolatex pdf color standalone
set output "mosolov-sector-P2-Bi-0-5-n-1-geo-adapt-loop-size.tex"

set size square
set colors classic
set xlabel '[c]{adaptation iteration}'
set  label '[l]{mesh size}' at graph 0.03, 0.93
set ytics 1000
set yrange [0:5000]
set xtics 5
plot 'mosolov-sector-P2-Bi-0.5-n-1-geo-adapt-loop.gdat' u 1:2 notitle w lp lc 0 lw 4
#pause -1
