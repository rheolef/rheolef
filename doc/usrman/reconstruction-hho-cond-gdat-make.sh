#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
e=${1-"e"}
case $e in
 e)   kmax=3; L="3 4 8 16 32 64 128 256 512 1024";;
 t|q) kmax=3; L="1 2 4 8 16 32";;
 *)   kmax=3; L="5 10 20 40";;
esac
k=0
while test $k -le $kmax; do
  for l in $k; do
    echo "# P${k}d P${l}d $e"
    echo "# 1/h cond_As tr_a norm_b2"
    for m in $L; do
      command="mkgeo_grid -$e $m > tmp.geo"
      #echo "! $command" 1>&2
      eval $command
      rm -f a.m s.m
      command="./reconstruction_hho tmp.geo P${k}d P${l}d 2>/dev/null >/dev/null"
      #echo "! $command" 1>&2
      eval $command
      command="octave -q a.m > tmp.txt 2>/dev/null"
      eval $command
      cond_As=`grep cond_As tmp.txt | gawk '{print $3}'`
      tr_a=`grep tr_a tmp.txt | gawk '{print $3}'`
      norm_b2=`grep norm_b2 tmp.txt | gawk '{print $3}'`
      echo "$m $cond_As $tr_a $norm_b2"
    done
    echo; echo
    k=`expr $k + 1`
  done
done
rm -f tmp.geo tmpa.txt tmps.txt
