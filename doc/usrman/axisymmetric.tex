% -----------------------------
\subsection{Axisymmetric geometries}
% -----------------------------
\label{sec-axi}

\cindex{polar coordinate system}
\cindex{coordinate system!axisymmetric}
Axisymmetric geometries are fully supported in \Rheolef:
the coordinate system is associated to the geometry description,
stored together with the mesh in the \filesuffix{.geo} and this information
is propagated in spaces, forms and fields without any change in the code.
Thus, a code that works in plane a 2D plane geometry is able to support
a 3D axisymmetric one without changes.
A simple axisymmetric geometry writes:
\fiindex{\filesuffix{.geo} mesh}
\pindexopt{mkgeo_grid}{-zr}
\begin{verbatim}
        mkgeo_grid -t 10 -zr > square-zr.geo
        more square-zr.geo
\end{verbatim}
Remark the additional line in the header:
\begin{verbatim}
        coordinate_system zr
\end{verbatim}
The axis of symmetry is denoted as $z$ while the polar coordinates
are $(r,\theta)$. By symmetry, the problem is supposed to be independent
upon $\theta$ and the computational domain is described by $(x_0,x_1)=(z,r)$.
Conversely, in some cases, it could be convenient to swap the order of the coordinates
and use $(r,z)$: this feature is obtained by the \code{-rz} option:
\begin{verbatim}
        mkgeo_grid -t 10 -rz > square-rz.geo
        more square-rz.geo
\end{verbatim}
\cindex{space!weighted (axisymmetric)}
Axisymmetric problems uses $L^2$ functional space equipped with the
following weighted scalar product
\[
	(f,g) = \int_\Omega f(z,r) \, g(z,r) \, r \, dr dz
\]
and all usual bilinear forms support this weight.
Thus, the {\bf coordinate system can be chosen at run time} and
we can expect an efficient source code reduction.

% ----------------------------------------------------------
\subsection{The axisymmetric stream function and stress tensor}
% ----------------------------------------------------------
\cindex{stream function!axisymmetric}%
  In the axisymmetric case, the velocity field ${\bf u}=(u_z,u_r)$ 
  can be expressed in terms of the Stokes stream function $\psi$
  by (see~Batchelor~\citealp[p.453]{Bat-1967-6ed} and~\citealp{Wik-2012-stokes-streamf}):
  \begin{equation}
    {\bf u} = (u_z,u_r)
     = \left( \Frac{1}{r}\Frac{\partial \psi}{\partial r},
        \ \ - \Frac{1}{r}\Frac{\partial \psi}{\partial z} \right)
    \label{eq-axi-streamf}
  \end{equation}
  Recall that in the axisymmetric case:
  \[
    {\bf curl}\,\psi
     = \left( \Frac{1}{r}\Frac{\partial (r\psi)}{\partial r},
        \ \ - \Frac{\partial \psi}{\partial z} \right)
  \]
  Thus, from this definition, in axisymmetric geometries ${\bf u}\neq {\bf curl}\,\psi$
  and the definition of $\psi$ differs from the 2D plane or 3D cases
  (see section~\ref{sec-streamf}, page~\pageref{sec-streamf}).

  Let us turn to a variational formulation in order to compute $\psi$ from ${\bf u}$.
  For any $\xi\in H^1(\Omega)$, let us multiply \eqref{eq-axi-streamf}
  by ${\bf v}=(\partial_r\xi,\ -\partial_z\xi)$ and then integrate over $\Omega$
  with the $r\,{\rm d}r\,{\rm d}z$ weight.
  For any known ${\bf u}$ velocity field, the problem writes:

  \ \ {\em (P): find $\psi \in \Psi(\psi_\Gamma)$ such that}
  \[
     a(\psi,\xi) = l(\xi),
     \ \ \forall\xi\in \Psi(0)
  \]
  where we have introduced the following bilinear forms:
  \begin{eqnarray*}
     a(\psi,\xi)
     &=&
     \int_\Omega 
       \left(
           \Frac{\partial \psi}{\partial r}
           \Frac{\partial \xi }{\partial r}
	+
           \Frac{\partial \psi}{\partial z}
           \Frac{\partial \xi }{\partial z}
        \right)
  	\ {\rm d}r\,{\rm d}z
	\\
     l(\xi)
     &=&
     \int_\Omega 
       \left(
           \Frac{\partial \xi }{\partial r}
    	   u_z 
	   -
           \Frac{\partial \xi }{\partial z}
	   u_r
        \right)
  	\ r\, {\rm d}r\,{\rm d}z
  \end{eqnarray*}
\cindex{geometry!contraction}%
\cindex{benchmark!flow in an abrupt contraction}%
\exindex{streamf_contraction.cc}
These forms are defined in \reffile{streamf_contraction.cc} as:
\clindex{integrate_option}%
\begin{lstlisting}[numbers=none,frame=none]
  integrate_option iopt;
  iopt.ignore_sys_coord = true;
  form a = integrate (dot(grad(psi), grad(xi)), iopt);
\end{lstlisting}
and
\begin{lstlisting}[numbers=none,frame=none]
  field lh = integrate (dot(uh,bcurl(xi)));
\end{lstlisting}
The \code{iopt.ignore_sys_coord} alows us to drops the $r$ integration weight,
i.e. replace $r\,{\rm d}r\,{\rm d}z$ by ${\rm d}r\,{\rm d}z$ when computing
the $a(.,.)$ form.
%
Conversely, $l$ involves the {\bf bcurl} operator defined as:
  \[
    {\bf bcurl}\,\xi
     = \left( \Frac{\partial \xi}{\partial r},
        \ \ - \Frac{\partial \xi}{\partial z} \right)
  \]
It is is closely related but differs from the standard {\bf curl} operator:
  \[
    {\bf curl}\,\xi
    = \left( \Frac{1}{r}\Frac{\partial (r\xi)}{\partial r},
        \ \ - \Frac{\partial \xi}{\partial z} \right)
  \]
The ${\bf bcurl}$ operator is a specific notation introduced in \Rheolef: 
it coincides with the usual ${\bf curl}$ operator except for axisymmetric
geometries. In tht case, it refers to the Batchelor trick, suitable for
the computation of the stream function.

As an example, let us reconsider the contraction
geometry (see section~\ref{sec-p1bp1}, page~\pageref{sec-p1bp1}),
extended in the axisymmetric case.
  In that case, the functional space is defined by:
  \begin{eqnarray*}
    \Psi(\psi_\Gamma)
	&=&
	\{ 
	  \varphi \in H^1(\Omega); \ 
		\varphi = \psi_\Gamma
		\ \mbox{ on }
		\Gamma_{\rm upstream}
		\cup \Gamma_{\rm wall}
		\cup \Gamma_{\rm axis}
	\}
  \end{eqnarray*}
  with
  \[
    \psi_\Gamma
    =
    \left\{ \begin{array}{cl}
		\psi_{\rm poiseuile} & \mbox{ on } \ \Gamma_{\rm upstream}
			\\
		\ 0 & \mbox{ on } \ \Gamma_{\rm wall}
			\\
		-1 & \mbox{ on } \ \Gamma_{\rm axis}
    \end{array} \right.
  \]
\cindex{boundary condition!mixed}%
  This space corresponds to the imposition of Dirichlet boundary conditions on 
		$\Gamma_{\rm upstream}$,
		$\Gamma_{\rm wall}$
		and
		$\Gamma_{\rm axis}$
  and a Neumann boundary condition on $\Gamma_{\rm downstream}$.

  \begin{figure}[htb]
     \begin{center}
       \begin{tabular}{ll}
          \includegraphics[height=4.5cm]{contraction-zr-P2-psi.png} 
		& $\psi_{\rm max}=1.84\times 10^{-3}$ \\
          \includegraphics[height=4.5cm]{contraction-cmp-P2-psi.png}
       \end{tabular}
     \end{center}
     \caption{Solution of the axisymmetric Stokes problem in the abrupt contraction:
       (top) the $P_2$ stream function associated to the $P2-P_1$ element;
       (bottom) comparison with the 2D Cartesian solution (in red).
       }
     \label{fig-contraction-zr}
  \end{figure}
\pindex{gmsh}%
\cindex{mesh!generation}%
\pindexopt{msh2geo}{-zr}%
\fiindex{\filesuffix{.msh} gmsh mesh}%
\fiindex{\filesuffix{.mshcad} gmsh geometry}%
\myexamplenoinput{contraction.mshcad}%
  The following unix commands generate the axisymmetric geometry:
\begin{verbatim}
  gmsh -2 contraction.mshcad -format msh2 -o contraction.msh
  msh2geo -zr contraction.msh > contraction-zr.geo
  more contraction-zr.geo
  geo contraction-zr.geo
\end{verbatim}
  The previous code \code{stokes_contraction.cc}
  and \code{streamf_contraction.cc} are both reused as:
\pindexopt{field}{-n-iso-negative}
\pindexopt{field}{-bw}
\begin{verbatim}
  ./stokes_contraction contraction-zr.geo > contraction-zr-P2.field
  ./streamf_contraction < contraction-zr-P2.field > contraction-zr-P2-psi.field
  field contraction-zr-P2-psi.field -n-iso-negative 10 -bw
\end{verbatim}
  The solution is represented on~Fig.~\ref{fig-contraction-zr}:
  it slightly differs from the 2D Cartesian solution, as computed in the previous section
  (see Fig.~\ref{fig-contraction-bubble}).
\cindex{vortex}%
  The vortex size is smaller but its intensity $\psi_{\rm max}=1.84\times 10^{-3}$ is higher.
  \begin{figure}[htb]
     \begin{center}
       \begin{tabular}{cc}
        \includegraphics[height=6cm]{contraction-zr-P2-cut.pdf} &
        \includegraphics[height=6cm]{contraction-zr-P1-tau22-cut.pdf} 
       \end{tabular}
     \end{center}
     \caption{Solution of the plane and axisymmetric Stokes problem in the abrupt contraction:
       cut along the axis of symmetry: 
       (left): $u_0$; (right) $\tau_{\theta\theta}$.
     }
     \label{fig-contraction-cut}
  \end{figure}
  Despite the stream functions looks like similar, the plane solutions are really different,
  as we can observe from a cut of the first component of the velocity along the axis
  (see Fig.~\ref{fig-contraction-cut}):
\pindexopt{field}{-comp}%
\pindexopt{field}{-cut}%
\pindexopt{field}{-normal}%
\pindexopt{field}{-origin}%
\pindexopt{field}{-gnuplot}%
\begin{verbatim}
  field contraction-P2.field    -comp 0 -cut -normal 0 1 -origin 0 1e-15 -gnuplot
  field contraction-zr-P2.field -comp 0 -cut -normal 0 1 -origin 0 1e-15 -gnuplot
\end{verbatim}
  The \code{1e-15} argument replace the zero value, as the mesh intersection
  cannot yet be done exactly on the boundary.
  Note that the \code{stokes_contraction_bubble.cc} can be also reused 
  in a similar way:
\begin{verbatim}
  ./stokes_contraction_bubble contraction-zr.geo > contraction-zr-P1.field
  ./streamf_contraction < contraction-zr-P1.field > contraction-zr-P1-psi.field
  field contraction-zr-P1-psi.field -n-iso-negative 10 -bw
\end{verbatim}
\cindex{tensor!rate of deformation}%
  There is another major difference with axisymmetric problems:
  the rate of deformation tensor writes:
  \[
    \tau=2D({\bf u})
    =\left(\begin{array}{ccc}
	\tau_{zz} & \tau_{rz} & 0 \\
	\tau_{rz} & \tau_{rr} & 0 \\
	0         &  0        & \tau_{\theta\theta}
    \end{array} \right)
  \]
\cindex{tensor!Cauchy stress}%
  Thus, there is an additional non-zero component $\tau_{\theta\theta}$ that is
  automatically integrated into the computations in \Rheolef.
  The incompressibility relation leads to ${\rm tr}(\tau)=\tau_{zz}+\tau_{rr}+\tau_{\theta\theta}=0$.
  Here $\sigma_{\rm tot}=-p.I + \tau$ is the total Cauchy stress tensor
  (by a dimensionless procedure, the viscosity can be taken as one).
\exindex{stress.cc}%
  By reusing the \code{stress.cc} code (see page~\pageref{stress.cc})
  we are able to compute the tensor components:
\begin{verbatim}
  make stress
  ./stress < contraction-zr-P1.field  > contraction-zr-P1-tau.field
\end{verbatim}
  The visualization along the axis of symmetry for the
  $\tau_{\theta\theta}$ component is obtained by
     (see Fig.~\ref{fig-contraction-cut}):
\pindexopt{field}{-comp}%
\pindexopt{field}{-cut}%
\pindexopt{field}{-normal}%
\pindexopt{field}{-origin}%
\pindexopt{field}{-gnuplot}%
\begin{verbatim}
  field contraction-zr-P1-tau.field -comp 22 -proj -cut -normal 0 1 -origin 0 1e-15 -gnuplot
\end{verbatim}
  Recall that the $\tau_{zz}$ and $\tau_{rr}$ components are obtained by the
  \code{-comp 00} and \code{-comp 11} options, respectively.
  The non-zero values along the axis of symmetry expresses the elongational 
  effects in the entry region of the abrupt contraction.
