set terminal cairolatex pdf color standalone
set output "transport-tensor-err.tex"

set log xy
set size square
set colors classic
set xrange [1e-3:1e-1]
set yrange [1e-8:1e0]
graph_ratio_xy = 2./8.
set xlabel '[c]{$h$}'
set  label '[l]{$\|\boldsymbol{\sigma}_h-\boldsymbol{\sigma}\|_{0,2,\Omega}$}' at graph 0.03, 0.93
set xtics add (\
	'[l]{$10^{-3}$}' 1e-3, \
	'[c]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-1}$}' 1e-1)
set ytics add (\
	'[r]{$10^{-8}$}' 1e-8, \
	''               1e-7, \
	'[r]{$10^{-6}$}' 1e-6, \
	''               1e-5, \
	'[r]{$10^{-4}$}' 1e-4, \
	''               1e-3, \
	'[r]{$10^{-2}$}' 1e-2, \
	''               1e-1, \
	'[r]{$1$}'       1)

# triangle a droite
alpha_A = 1.0
slope_A = graph_ratio_xy*alpha_A
xA =  0.27
yA =  0.59
dxA = 0.10
dyA = dxA*slope_A
set label sprintf("[l]{\\scriptsize $%g=k+1$}",alpha_A) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
alpha_B = 2.0
slope_B = graph_ratio_xy*alpha_B
xB =  0.27
yB =  0.34
dxB = 0.10
dyB = dxB*slope_B
set label sprintf("[l]{\\scriptsize $%g$}",alpha_B) at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
alpha_C = 3.0
slope_C = graph_ratio_xy*alpha_C
xC =  0.27
yC =  0.07
dxC = 0.10
dyC = dxC*slope_C
set label sprintf("[l]{\\scriptsize $%g$}",alpha_C) at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

# triangle a droite
alpha_D = 4.0
slope_D = graph_ratio_xy*alpha_D
xD =  0.57
yD =  0.10
dxD = 0.10
dyD = dxD*slope_D
set label sprintf("[l]{\\scriptsize $%g$}",alpha_D) at graph xD+dxD+0.02, yD+0.5*dyD right
set arrow from graph xD,     yD to     graph xD+dxD, yD     nohead
set arrow from graph xD+dxD, yD to     graph xD+dxD, yD+dyD nohead
set arrow from graph xD+dxD, yD+dyD to graph xD,     yD     nohead

plot \
'transport-tensor-err.gdat' \
  i 0 \
  u (1./$1):2 \
  t '[r]{$k=0$}' \
  w lp lw 4, \
'transport-tensor-err.gdat' \
  i 1 \
  u (1./$1):2 \
  t '[r]{$k=1$}' \
  w lp lw 4 lc rgb '#008800', \
'transport-tensor-err.gdat' \
  i 2 \
  u (1./$1):2 \
  t '[r]{$k=2$}' \
  w lp lw 4 lc 3, \
'transport-tensor-err.gdat' \
  i 3 \
  u (1./$1):2 \
  t '[r]{$k=3$}' \
  w lp lw 4 lc 4

#pause -1
