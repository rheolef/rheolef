% ---------------------------------------
\subsection{Example: the Burgers equation}
% ---------------------------------------
\label{sec-burger-dg}%
\cindex{problem!Burgers equation}
As an illustration, let us consider now the
test with the one-dimensional ($d=1$)
Burgers equation for a propagating slant step (see e.g.~\citealp[p.~87]{CarJia-1988})
in $\Omega=]0,1[$. 
We have
\mbox{$
  {\bf f}(u) = u^2/2
$},
for all $u\in\mathbb{R}$.
In that case, the Godunov
flux~\eqref{eq-dg-hyp-flux-godunov},
introduced page~\pageref{eq-dg-hyp-flux-godunov},
can be computed explicitly
for any $\boldsymbol{\nu}=(\nu_0) \in\mathbb{R}^d$
and $a, b \in\mathbb{R}$:
% TODO: verifier le calcul !!
\begin{eqnarray*}
  \Phi(\boldsymbol{\nu};\,a,b)
  =
  \left\{
    \begin{array}{ll}
      \nu_0 \min\left(a^2,b^2\right)/2
      & \mbox{ when } \nu_0 \geq 0 \mbox{ and } a \leq b 
        \mbox{ or }   \nu_0 \leq 0 \mbox{ and } a \geq b \\
      \nu_0 \max\left(a^2,b^2\right)/2
      & \mbox{ otherwise }
    \end{array}
  \right.
\end{eqnarray*}

% ---------------------------------------
\myexamplelicense{burgers.icc}

\myexamplelicense{burgers_flux_godunov.icc}
% ---------------------------------------

% -----------------------------------------------------------
\subsubsection*{Computing an exact solution}
% -----------------------------------------------------------
An exact solution is useful for testing numerical methods.
The computation of such an exact solution for the one dimensional Burgers
equation is described by \citet{HarEngOshCha-1987}.
The authors consider first the problem with a periodic boundary condition:

\ \ $(P)$: find $u:~]0,T[\times ]\!-1\!,1[ \longmapsto \mathbb{R}$ such that
%\begin{subequations}
\begin{eqnarray*}
	\Frac{\partial u}{\partial t}
	+
	\Frac{\partial}{\partial x}
        \left(
		\Frac{u^2}{2}
	\right)
	&=&
        0
	\ \mbox{ in } ]0,T[\times ]-1,1[
	\\
	u(t\!=\!0,x) &=& \alpha + \beta\sin(\pi x +\gamma)
	,\ \mbox{ a.e. } x\in ]-1,1[
	\\
	u(t,x\!=\!-1) &=& u(t,x\!=\!1)
	\ \mbox{ a.e. } t\in ]0,T[
\end{eqnarray*}
where $\alpha,\beta$ and $\gamma$ are real parameters.
%\end{subequations}
Let us denote $w$ the solution of the problem when $\beta=1$
and $\alpha=\gamma=0$; i.e. with the initial condition
\mbox{$
	w(t\!=\!0,x) = \sin(\pi x)
	,\ \mbox{ a.e. } x\in ]-1,1[
$}.
For any $x\in [0,1[$ and $t>0$, the solution $\bar{w}=w(t,x)$ satisfies
the characteristic relation
\begin{eqnarray*}
	\bar{w} = \sin(\pi(x-\bar{w}t))
\end{eqnarray*}
This nonlinear relation can be solved by a Newton algorithm.
Then, for $x\in ]\!-\!1,0[$, the solution is completed by symmetry:
$w(t,x)=-w(t,-x)$.
Finally, the general solution for any $\alpha,\beta$ and $\gamma=0$
writes
\mbox{$
	u(t,x) = \alpha + w(\beta t,\, x-\alpha t + \gamma)
$}.
File~\file{harten.h} implements this approach. 

% ---------------------------------------
% TODO: clean this long file
\myexamplenoinput{harten0.h}

\myexamplelicense{harten.h}

\myexamplelicense{harten_show.cc}
% ---------------------------------------

The included file~\file{harten0.h} is not shown here
but is available in the example directory.
\begin{figure}[htb]
     \begin{center}
       \begin{tabular}{cc}
          \includegraphics[height=6cm]{harten-anim-t-0.pdf} &
          \includegraphics[height=6cm]{harten-anim-t-0,5.pdf} \\
          \includegraphics[height=6cm]{harten-anim-tcrit.pdf} &
          \includegraphics[height=6cm]{harten-anim-t-0,75.pdf} \\
          \includegraphics[height=6cm]{harten-anim-t-1.pdf} &
          \includegraphics[height=6cm]{harten-anim-t-1,75.pdf}
       \end{tabular}
     \end{center}
     \caption{Harten's exact solution of the Burgers equation
		($\alpha=1, \beta=1/2, \gamma=0$).
	Comparison with the $P_0$ approximation ($h=1/100$, RK-SSP($3$)).
     }
   \label{fig-harten}
\end{figure}
% -----------------------------------------------------------
\subsubsection*{Comments}
% -----------------------------------------------------------
Note that the constant $M$, used by the limiter
in~\eqref{eq-dg-limiter-M}, can be explicitly
computed for this solution:
$M=\beta\pi^2$.
% For $\beta=1/2$, we use $M=\pi^2/2\approx 5$.

The animation of this exact solution 
is performed by the following commands:
\begin{verbatim}
  make harten_show
  mkgeo_grid -e 2000 -a -1 -b 1 > line2.geo
  ./harten_show line2 P1 1000 2.5 > line2-exact.branch
  branch line2-exact -gnuplot
\end{verbatim}
Fig.~\ref{fig-harten} shows the solution $u$ for
$\alpha=1, \beta=1/2$ and $\gamma=0$.
It is regular until $t=2/\pi$
(Fig.~\ref{fig-harten}.c)
and then develops a chock for $t>2/\pi$
(Fig.~\ref{fig-harten}.d).
After its apparition, this chock interacts with the expansion
wave in $]-1,1[$: this brings about a fast decay of the solution
(Figs.~\ref{fig-harten}.e and~f).
Fig.~\ref{fig-harten} plots also a numerical solution:
its computation is the aim of the next section.

% -----------------------------------------------------------
\subsubsection*{Numerical resolution}
% -----------------------------------------------------------
When replacing the periodic boundary condition with a
inflow one, associated with the boundary data $g$, we choose
$g$ to be the value of the exact solution of the problem
with periodic boundary conditions:
\mbox{$
	g(t,x) = \alpha + w(\beta t,\, x-\alpha t)
$} for $x\in \{-1,1\}$.

% TODO: le code c++ de l'algo en temps arrive bien apres sa 
%       presentation latex
% ---------------------------------------
\myexamplelicense{burgers_dg.cc}
% ---------------------------------------

% -----------------------------------------------------------
\subsubsection*{Comments}
% -----------------------------------------------------------
The Runge-Kutta time discretization combined
with the discontinuous Galerkin space discretization
is implemented for this test case.

The $P_0$ approximation is performed by the following commands:
\begin{verbatim}
  make burgers_dg
  mkgeo_grid -e 200 -a -1 -b 1 > line2-200.geo
  ./burgers_dg line2-200.geo P0 1000 2.5 > line2-200-P0.branch
  branch line2-200-P0.branch -gnuplot
\end{verbatim}
The two last commands compute the $P_0$ approximation
of the solution, as shown on Fig.~\ref{fig-harten}.
Observe the robust behavior of the solution at the vicinity of the chock.
By replacing \code{P0} by \code{P1d} in the previous commands,
we obtain the $P_1$-discontinuous approximation.
\begin{verbatim}
  ./burgers_dg line2-200.geo P1d 1000 2.5 > line2-200-P1d.branch
  branch line2-200-P1d.branch -gnuplot
\end{verbatim}

\begin{figure}[htb]
     \begin{center}
       \begin{tabular}{ll}
          \includegraphics[height=6cm]{harten-Pk-err-before-choc.pdf} &
          \includegraphics[height=6cm]{harten-Pk-err.pdf}
       \end{tabular}
     \end{center}
     \caption{Burgers equation: error between the $P_0$ approximation
		and the exact solution of the Harten's problem
		($\alpha=1, \beta=1/2, \gamma=0$):
		(a) before chock, with $T=1/\pi$;
		(b) after chock, with $T=5/2$.
     }
   \label{fig-harten-Pk-err}
\end{figure}
Fig.~\ref{fig-harten-Pk-err} plots the error vs $h$ for $k=0$ and $k=1$.
Fig.~\ref{fig-harten-Pk-err}.a plots in a time interval $[0,T]$ with 
$T=1/\pi$, before the chock that occurs at $t=2/\pi$.
In that interval, the solution is regular and the error approximation behaves
as $\mathcal{O}(h^{k+1})$. 
The time interval has been chosen sufficiently small for the error to depend
only upon $h$.
Fig.~\ref{fig-harten-Pk-err}.b plots in a larger time interval $[0,T]$ with 
$T=5/2$, that includes the chock.
Observe that the error behaves as $\mathcal{O}(h)$ for both $k=0$ and~$1$.
This is optimal when $k=0$ but not when $k=1$.
This is due to the loss of regularity of the exact solution that presents
a discontinuity;
A similar observation can be found in~\citet{ZhoShu-2013}, table~4.1.
% Also in~\citet{ZhuZhoShuQiu-2013},

%% % -----------------------------------------------------------
%% \subsection{Slanted step test}
%% % -----------------------------------------------------------
%% 
%% We consider the inviscid Burgers equation for a propagating slant step.
%% Compilation and run write:
%% \begin{verbatim}
%%   make burgers_limiter_dg merge
%%   mkgeo_grid -e 100 > line-100.geo
%%   ./burgers_limiter_dg line-100.geo P0  1000 > line-100-P0.branch
%%   ./burgers_limiter_dg line-100.geo P1d 1000 > line-100-P1d.branch
%%   ./merge line-100-P0.branch line-100-P1d.branch > all.branch 
%%   branch all.branch -gnuplot -umin -0.1 -umax 1.1
%% \end{verbatim}
%%
%% \begin{figure}[htb]
%%   \begin{center}
%%     \includegraphics[width=0.5\textwidth]{burgers-200-P0.pdf}
%%   \end{center}
%%   \caption{Burgers equation with $k=0$, $h=1/200$, $p=3$ and $\Delta t=1/400$ at various times.}
%%   \label{fig-dg-burgers-P0}
%% \end{figure}
%% %
%% The initial profile and solutions at $t=0.16$ are shown in Fig.~\ref{fig-dg-burgers-P0}.
%% The nonlinearity causes the front to steepen in time to form (eventually) a chock.
%% At $t = 0.3$ when the front is close to vertical.
%% The $k=0$ approximation is well behaved, but for higher order space, e.g. with $k=1$
%% approximation there is a problem and the computations fail:
%% the sharp solution gradient approximating the chock causes the oscillations to grow, and the
%% approximation becomes nonlinearly unstable. 
%% A forthcoming study will present some slope limiters for space hight order polynomials approximations.
