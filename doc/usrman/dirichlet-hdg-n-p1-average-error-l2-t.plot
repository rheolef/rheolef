set terminal cairolatex pdf color standalone
set output "dirichlet-hdg-n-p1-average-error-l2-t.tex"
gdat     = "dirichlet-hdg-n-p1-average-error-t.gdat"

set log xy
set size square
set key at graph 0.38, 0.27
set colors classic
set xrange [1e-3:1e-0]
set yrange [1e-18:1e2]
graph_ratio_xy = 3./20.
set xlabel '[c]{$h$}'
set  label '[l]{$\|\bar{\pi}_h(u)-\bar{u}_h\|_{0,2,\Omega}$}' at graph 0.03, 0.93
set  label '[l]{$n=1$}' at graph 0.04, 0.36
set xtics add (\
	'[c]{$1$}'       1, \
	'[c]{$10^{-3}$}' 1e-3, \
	'[c]{$10^{-2}$}' 1e-2, \
	'[c]{$10^{-1}$}' 1e-1)
set ytics (\
	'[r]{$10^{-16}$}' 1e-16, \
	'[r]{$10^{-12}$}' 1e-12, \
	'[r]{$10^{-8}$}' 1e-8, \
	'[r]{$10^{-4}$}' 1e-4, \
	'[r]{$1$}'       1)

# triangle a droite
alpha_A = 2.0
slope_A = graph_ratio_xy*alpha_A
xA =  0.20
yA =  0.62
dxA = 0.10
dyA = dxA*slope_A
set label sprintf("[l]{\\scriptsize $%g$}",alpha_A) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
alpha_B = 3.0
slope_B = graph_ratio_xy*alpha_B
xB =  0.20
yB =  0.42
dxB = 0.10
dyB = dxB*slope_B
set label sprintf("[l]{\\scriptsize $%g$}",alpha_B) at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
alpha_C = 4.0
slope_C = graph_ratio_xy*alpha_C
xC =  0.30
yC =  0.28
dxC = 0.10
dyC = dxC*slope_C
set label sprintf("[l]{\\scriptsize $%g$}",alpha_C) at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

# triangle a droite
alpha_D = 5.0
slope_D = graph_ratio_xy*alpha_D
xD =  0.50
yD =  0.28
dxD = 0.10
dyD = dxD*slope_D
set label sprintf("[l]{\\scriptsize $%g$}",alpha_D) at graph xD+dxD+0.02, yD+0.5*dyD right
set arrow from graph xD,     yD to     graph xD+dxD, yD     nohead
set arrow from graph xD+dxD, yD to     graph xD+dxD, yD+dyD nohead
set arrow from graph xD+dxD, yD+dyD to graph xD,     yD     nohead


# triangle a droite
alpha_E = 6.0
slope_E = graph_ratio_xy*alpha_E
xE =  0.60
yE =  0.23
dxE = 0.10
dyE = dxE*slope_E
set label sprintf("[l]{\\scriptsize $%g=k+2$}",alpha_E) at graph xE+dxE+0.02, yE+0.5*dyE right
set arrow from graph xE,     yE to     graph xE+dxE, yE     nohead
set arrow from graph xE+dxE, yE to     graph xE+dxE, yE+dyE nohead
set arrow from graph xE+dxE, yE+dyE to graph xE,     yE     nohead


plot \
gdat \
  i 0 \
  u (1./$1):2 \
  t '[r]{$k=0$}' \
  w lp lw 4, \
gdat \
  i 1 \
  u (1./$1):2 \
  t '[r]{$k=1$}' \
  w lp lw 4 lc rgb '#008800', \
gdat \
  i 2 \
  u (1./$1):2 \
  t '[r]{$k=2$}' \
  w lp lw 4 lc 3, \
gdat \
  i 3 \
  u (1./$1):2 \
  t '[r]{$k=3$}' \
  w lp lw 4 lc 4, \
gdat \
  i 4 \
  u (1./$1):2 \
  t '[r]{$k=4$}' \
  w lp lw 4 lc 5

#pause -1
