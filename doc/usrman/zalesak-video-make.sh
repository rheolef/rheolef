#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# in paraview, "save animation":
#    image resolution: 1024x768
#    file name       : "name", as first arg in the script
#    file of type    : *.jpg
#  & choose a new output directory
#
# then, run in it this scipt
# see https://askubuntu.com/questions/610903/how-can-i-create-a-video-file-from-a-set-of-jpg-images
#
name=${1-"zz"}
suffix=jpeg
tmp="tmp" ; # prefix for symlinks
rm -f ${tmp}.*.${suffix}
# --------------------------
# 1) get the number of files
# --------------------------
# scenario: move and then final stop
move_duration=10 # integer, in seconds
stop_duration=3  # integer, in seconds
framerate=25 # image per second
n_move_img=`expr $move_duration \* $framerate`
n_stop_img=`expr $stop_duration \* $framerate`
echo "n_move_img = $n_move_img"
n_tot=`ls ${name}.*.${suffix} | wc -l`
n_tot=`expr $n_tot - 1`
echo "n_tot = ${n_tot}"
#exit 0
i=0
k=0
while test $i -lt $n_tot; do
  ii=`echo $i | awk '{printf("%.04d",$1)}'`
  kk=`echo $k | awk '{printf("%.04d",$1)}'`
  ln -s ${name}.${ii}.${suffix} ${tmp}.${kk}.${suffix}
  i=`echo $i $n_tot $n_move_img | awk '{printf("%d", int($1 + 1.0*$2/$3))}'`
  k=`expr $k + 1`
done
j=1
nn_tot=`echo ${n_tot} | awk '{printf("%.04d",$1)}'`
while test $j -le $n_stop_img; do
  kk=`echo $k | awk '{printf("%.04d",$1)}'`
  ln -s $L ${name}.${nn_tot}.${suffix} ${tmp}.${kk}.${suffix}
  j=`expr $j + 1`
  k=`expr $k + 1`
done
#ls -l ${tmp}.*.${suffix}
#exit 0

rm -f output.mp4
old_opts="-pix_fmt yuv420p -i ${name}.%04d.${suffix}"
ffmpeg \
  -framerate $framerate \
  -i "${tmp}.%04d.${suffix}" \
  -c:v libx264 \
  -profile:v high \
  -crf 20 \
  output.mp4

