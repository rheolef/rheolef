#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
BINDIR="../../../rheolef-dis/doc/pexamples"
k=${1-"1"}
Bi=0.2
n=0.5
echo "# n $n"
echo "# approx P$k"
echo "# Bi $Bi"
echo "# 1/h err_u_linf err_u_l2 err_u_h1 err_s_linf err_s_l2"
for N in 10 20 30 40 50; do
  name="mosolov-circle-$N-P$k-Bi-$Bi-n-$n"
  gzip -d < $name.field.gz | $BINDIR/mosolov_error > tmp.txt 2>/dev/null
  err_u_linf=`grep err_u_linf tmp.txt | gawk '{print $3}'`
  err_u_l2=`grep err_u_l2 tmp.txt | gawk '{print $3}'`
  err_u_h1=`grep err_u_h1 tmp.txt | gawk '{print $3}'`
  err_s_linf=`grep err_s_linf tmp.txt | gawk '{print $3}'`
  err_s_l2=`grep err_s_l2 tmp.txt | gawk '{print $3}'`
  echo "$N $err_u_linf $err_u_l2 $err_u_h1 $err_s_linf $err_s_l2"
done 
