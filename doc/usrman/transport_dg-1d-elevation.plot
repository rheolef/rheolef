set terminal cairolatex pdf color standalone
set output "transport_dg-1d-elevation.tex"

set sample 1000
set colors classic
set size square 
set xrange [0:1]
set yrange [0:1.1]
set xtics (0,0.5,1)
set ytics (0,0.5,1)
set xlabel '[c]{\large $x$}'
set  label '[l]{$h=1/20$}' at graph 0.10,0.90
plot \
  exp(-3*x) \
	title '[r]{$\phi(x)$}' \
	w l lw 1 lc 0 lt 1, \
  "transport_dg-1d-elevation.gdat" \
	title '[r]{$\phi_h(x)$}' \
	with lines lc 1 lw 3 lt 1

#pause -1
