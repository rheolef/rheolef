\label{sec-transmission}
This section is related to the so-called transmission problem.
We introduce some new concepts: 
problems with non-constant coefficients,
regions in the mesh,
weighted forms
and discontinuous approximations.

%
% NOTE: u_h = pi_h(u)
%	1d: maillages non-uniformes aussi
%	2d: uniforme ok, non-unif non !
%	    mais super-convergence ?
%  =>	3d: ?
%	P1d-P2 : ?
%	quadrature et P1d-P2 ?
%  =>   voir whalbin ! et book E.F. zienkiewicz
%	proj P1-C0 pi_h(gh) du gradient P1d gh : superconvergence...?
%	estimateur d'erreur gh-pi_h(gh) directionnel ?
%

% -------------------------------------
\subsection*{Formulation}
% -------------------------------------

Let us consider a diffusion problem
with a non-constant diffusion coefficient $\eta$
in a domain bounded $\Omega \subset \mathbb{R}^d$, $d=1,2,3$:

{\it $(P)$: find $u$ defined in $\Omega$ such that:}
\begin{eqnarray}
      -{\rm div}(\eta \bnabla u) &=& f \ \mbox{in}\ \Omega
		\label{eq-trans-1}
		\\
      u &=& 0 \ {\rm on}\ \Gamma_{\rm left} \cup \Gamma_{\rm right}
		\label{eq-trans-2}
		\\
      \Frac{\partial u}{\partial n} &=& 0 \ {\rm on}\ \Gamma_{\rm top} \cup \Gamma_{\rm bottom} \mbox{ when } d\geq 2
		\label{eq-trans-3}
		\\
      \Frac{\partial u}{\partial n} &=& 0 \ {\rm on}\ \Gamma_{\rm front} \cup \Gamma_{\rm back} \mbox{ when } d = 3
		\label{eq-trans-4}
\end{eqnarray}
where $f$ is a given source term.
\begin{figure}[htb]
     \begin{center}
          \includegraphics{square-region-fig.pdf}
     \end{center}
     \caption{Transmission problem:
		the domain $\Omega$ partition: $(\Omega_{\rm west}$ and $\Omega_{\rm east})$.}
      \label{fig-transmission-domains}
\end{figure}
We consider here the important special case when $\eta$ is 
piecewise constant:
\[
      \eta (x) = \left\{ \begin{array}{ll}
		\varepsilon   & \mbox{ when } x \in \Omega_{\rm west} \\ 
		1             & \mbox{ when } x \in \Omega_{\rm east}
      		\end{array} \right.
\]
where $(\Omega_{\rm west}, \Omega_{\rm east})$ is a partition of
$\Omega$ in two parts (see Fig.~\ref{fig-transmission-domains}).
\pbindex{transmission}%
This is the so-called {\bf transmission} problem:
the solution and the flux are continuous on the interface
$\Gamma_0=\partial\Omega_{\rm east}\cap\partial\Omega_{\rm west}$
between the two domains where the problem reduce to a constant diffusion one:
\begin{eqnarray*}
	u_{\Omega_{\rm west}} &=& u_{\Omega_{\rm east}}
		\mbox { on } \Gamma_0 \\
	\varepsilon \Frac{\partial u_{/\Omega_{\rm west}} }{\partial n}
        &=& 
	\Frac{\partial u_{\Omega_{\rm east}} }{\partial n}
		\mbox { on } \Gamma_0 \\
\end{eqnarray*}
\cindex{region}%
  It expresses the transmission of the quantity $u$ and its flux 
  across the interface $\Gamma_0$ 
  between two regions that have different diffusion properties:
  Note that the more classical problem, with constant diffusion $\eta$
  on $\Omega$ is obtained by simply choosing when $\varepsilon=1$.

  The variational formulation of this problem expresses:
  
  {\it $(VF)$: find $u\in V$ such that:}
  $$
      a(u,v) = l(v),
      \ \forall v \in V
  $$
  where the bilinear forms $a(.,.)$ and the linear one $l(.)$ are defined by
  \begin{eqnarray*}
          a(u,v) &=& \int_\Omega \eta \, \nabla u . \nabla v \, dx,
	  \ \ \forall u,v \in H^1(\Omega)
	      \\
          l(v) &=& \int_\Omega f \, v \, dx,
	  \ \ \forall v \in L^2(\Omega)
	      \\
          V &=& \{ v \in H^1(\Omega); \ 
		v = 0 \ {\rm on}\ \Gamma_{\rm left} \cup \Gamma_{\rm right} \}
  \end{eqnarray*}

\cindex{form!energy}%
  The bilinear form $a(.,.)$ defines a scalar product
  in $V$ and is related to the {\it energy} form.
  This form is associated to the $-{\rm div}\,\eta\bnabla$ operator.
  
  The approximation of this problem could performed by
  a standard Lagrange $P_k$ continuous approximation.

% -------------------------------------
\myexamplelicense{transmission.cc}
% -------------------------------------

% -------------------------------------
\subsection*{Comments}
% -------------------------------------
This file is quite similar to those studied in
the first sections of this book.
Let us comment only the changes.
\cindex{boundary condition!Dirichlet}%
\cindex{boundary condition!Neumann}%
The Dirichlet boundary condition applies no more
on the whole boundary $\partial\Omega$ but on
two parts $\Gamma_{\rm left}$ and $\Gamma_{\rm right}$.
On the other boundary parts, an homogeneous Neumann
boundary condition is used: since these conditions does not
produce any additional terms in the variational formulation,
there are also nothing to write in the \code{C++} code for
these boundaries.
We choose $f=1$: this leads to a convenient test-problem,
since the exact solution is known when $\Omega=]0,1[^d$:
\[
   u(x) = \left\{
	   \begin{array}{ll}
     		\Frac{x_0}{2\varepsilon}
 	 	\left( \Frac{1+3\varepsilon}{2(1+\varepsilon)} - x_0 \right)
 	       & \mbox{ when } x_0 < 1/2 \\
 	       & \\
     		\Frac{1-x_0}{2}
 		\left(x_0 + \Frac{1-\varepsilon}{2(1+\varepsilon)} \right)
 	       & \mbox{ otherwise }
 	   \end{array} \right.
\]
\apindex{discontinuous}%
The field $\eta$ belongs to a discontinuous finite element $P_{k-1}$ space
denoted by $Q_h$:
\begin{lstlisting}[numbers=none,frame=none]
  string eta_approx = "P" + to_string(Xh.degree()-1) + "d";
  space Qh (omega, eta_approx);
  field eta (Qh);
\end{lstlisting}
For instance, when \code{argv[2]} contains \code{"P2"}, i.e.  $k=2$,
then the string \code{eta_approx} takes value \code{"P1d"}.
Then $\eta$ is initialized by:
\begin{lstlisting}[numbers=none,frame=none]
  eta["east"]  = 1;
  eta["weast"] = epsilon;
\end{lstlisting}
\cindex{form!weighted}%
\cindex{form!{$\eta\nabla u.\nabla v$}}%
The energy form $a$ is then constructed with $\eta$ as additional parameter
that acts as a integration \emph{weight}:
\begin{lstlisting}[numbers=none,frame=none]
  form a = integrate (eta_h*dot(grad(u),grad(v)));
\end{lstlisting}
Such forms with a additional {\em weight} function are
called {\em weighted forms} in \Rheolef.

% -------------------------------------
\subsection*{How to run the program ?}
% -------------------------------------
Build the program as usual: \code{make transmission}.
Then, creates a one-dimensional geometry with two regions:
\pindex{gnuplot}%
\pindexopt{mkgeo_grid}{-region}%
\begin{verbatim}
        mkgeo_grid -e 100 -region > line.geo
        geo line.geo
\end{verbatim}
\cindex{region}%
The trivial mesh generator \code{mkgeo_grid}, defines two
regions \code{east} and \code{west} when used with the \code{-region} option.
This correspond to the situation:
\[
	\Omega = [0,1]^d ,
	   \ \ \ 
	\Omega_{\rm west} = \Omega \cap \{ x_0 < 1/2 \}
	   \ \mbox{ and } \ 
	\Omega_{\rm east} = \Omega \cap \{ x_0 > 1/2 \}.
\]
\cindex{coordinate system!Cartesian}%
In order to avoid mistakes with the \code{C++} style indexes,
we denote by $(x_0,\ldots,x_{d-1})$ the Cartesian coordinate system in $\mathbb{R}^d$.

Finally, run the program and look at the solution:
\begin{verbatim}
        make transmission
        ./transmission line.geo P1 > line.field
        field line.field
\end{verbatim}
Since the exact solution is a piecewise second order polynomial
and the change in the diffusion coefficient value fits the element boundaries,
we obtain the exact solution for all the degrees of freedom of any $P_k$ approximation,
$k\geq 1$, as shown on Fig.~\ref{fig-transmission} when $k=1$.
Moreover, when $k\geq 2$ then $u_h=u$ since $X_h$ contains the exact solution $u$.
\begin{figure}[htb]
     \begin{center}
          \includegraphics{transmission-1d-fig.pdf}
     \end{center}
     \caption{Transmission problem with
	$\varepsilon=10^{-2}$, $d=1$, $P_1$ approximation.}
      \label{fig-transmission}
\end{figure}
The two dimensional case corresponds to the commands:
\begin{verbatim}
        mkgeo_grid -t 10 -region > square.geo
        geo square.geo
        ./transmission square.geo P1 > square.field
        field square.field -elevation
\end{verbatim}
while the tridimensional to
\begin{verbatim}
        mkgeo_grid -T 10 -region > cube.geo
        ./transmission cube.geo P1 > cube.field
        field cube.field
\end{verbatim}
\apindex{high-order}%
\cindex{element shape}%
\pindex{mpirun}%
As for all the others examples, you can replace
\code{P1} by higher-order approximations,
change elements shapes, such as \code{q}, \code{H} or \code{P},
and run distributed computations computations with \code{mpirun}.

% % -------------------------------------
% \section{Mixed Formulation}
% % -------------------------------------
% \cindex{mixed formulation}
%   We switch here to a mixed formulation that implements easily.
% 
%    Let us introduce the following vector-valued field:
%    \[
% 	{\bf p} = \eta \, \bnabla u
%    \]
%    Problem \eqref{eq-trans-1}-\eqref{eq-trans-4} can be rewritten as:
% 
%   {\it (M): find ${\bf p}$ and $u$ defined in $\Omega$ such that:}
%   \[ \begin{array}{rlrlrl}
%       \Frac{1}{\eta} \, {\bf p} &-& \bnabla u &=& 0  & \mbox{ in } \Omega \\
%       {\rm div}\, {\bf p}    & &           &=& -f & \mbox{ in } \Omega \\
%       &&                          u        &=& 0  & 
% 		  \mbox{ on } \Gamma_{\rm left} \cup \Gamma_{\rm right}
% 		\\
%       && \Frac{\partial u}{\partial n}     &=& 0  & 
% 		  \mbox{ on } \Gamma_{\rm top} \cup \Gamma_{\rm bottom} \mbox{ when } d \geq 2
% 		\\
%       && \Frac{\partial u}{\partial n}     &=& 0 & 
% 		  \mbox{ on } \Gamma_{\rm front} \cup \Gamma_{\rm back} \mbox{ when } d = 3
%     \end{array}
%   \]
%   The variational formulation writes:
% 
%   {\it $(VF)$: find ${\bf p}\in L^2(\Omega)^d$ and $u\in V$ such that:}
%   \[ \begin{array}{llllll}
%       m({\bf p},{\bf q}) &+& b({\bf q},u) &=& 0,  & \forall {\bf q}\in (L^2(\Omega))^d \\
%       b({\bf p},v)       & &              &=& -l(v), & \forall v\in V
%     \end{array}
%   \]
%   where
%   \begin{eqnarray*}
%           m({\bf p},{\bf q}) &=& \int_\Omega \Frac{1}{\eta} {\bf p}. {\bf q}\, dx,
% 	  	\ \ \forall {\bf p}, {\bf q} \in L^2(\Omega)^d,
% 	      \\
%           b({\bf q},v) &=& -\int_\Omega \bnabla v . {\bf q} \, dx,
% 	  	\ \ \forall {\bf q} \in L^2(\Omega)^d, \forall v \in H^1(\Omega)
% 	      \\
%           l(v) &=& \int_\Omega f \, v \, dx,
% 	  	\ \ \forall v \in L^2(\Omega)
% 	      \\
%           V &=& \{ v \in H^1(\Omega); \ 
% 		v = 0 \ {\rm on}\ \Gamma_{\rm left} \cup \Gamma_{\rm right} \}
%   \end{eqnarray*}
%   This problem appears as more complex than the previous one,
%   since there is two unknown instead of one.
%   Nevertheless, the ${\bf p}$ unknown could be eliminated
%   by inverting the operator associated to the $m$ form.
%   This can be done easily since ${\bf p}$ is approximated by
%   discontinuous piecewise polynomial functions.
% 
% % -------------------------------------
% \subsection*{Mixed approximation}
% % -------------------------------------
% 
%   Let $k\geq 1$ and
%   \begin{eqnarray*}
% 	{\bf Q}_h &=& \{ {\bf q} \in (L^2(\Omega))^d; \ {\bf q}_{/K} \in (P_{k-1})^d \}
% 		\\
% 	X_h &=& \{ v \in H^1(\Omega); \ v_{/K} \in P_k \}
% 		\\
% 	V_h &=& X_h \cap V
%   \end{eqnarray*}
%   The mixed finite element approximation writes:
% 
%   {\it $(VF)_h$: find ${\bf p}_h\in {\bf Q}_h$ and $u_h\in V_h$ such that:}
%   \[ \begin{array}{llllll}
%       m({\bf p}_h,{\bf q}) &+& b({\bf q},u_h) &=& 0,  & \forall {\bf q}\in {\bf Q}_h \\
%       b({\bf p}_h,v)       & &                &=& -l(v), & \forall v\in V_h
%     \end{array}
%   \]
%   Here the matrix associated to $m$ is block-diagonal
%   and can be inverted at the element level. Then, by identifying
%   the matrix and its corresponding bilinear forms:
%   \[
% 	{\bf p}_h = - m^{-1} \, b^T \, u_h
%   \]
%   and the second equation reduces to:
%   \[
% 	a _, u_h = l_h
%   \]
%   where $a=b\,m^{-1}\,b^T$.
%   The problem reduced to a standard linear system.
% 
% % -------------------------------------
% \myexamplelicense{transmission-mixed.cc}
% % -------------------------------------
% \clindex{form_diag}
% \foindex{inv_mass}
% \foindex{grad}
% \foindex{mass}
% 
% 
% TODO : compilation and run.
% 
% % -------------------------------------
% \subsection*{Comments}
% % -------------------------------------
% \cindex{diffusion tensor}
% 
% The code begins as usual.
% In the second part, the space ${\bf Q}_h$ is defined.
% For convenience, the diffusion coefficient $\eta$ is introduced
% diagonal diffusion tensor operator $d$, i.e. for $d=2$:
% \[
% 	d = \left( \begin{array}{cc}
% 			\eta & 0 \\ 0 & \eta
% 	    \end{array} \right)
% \]
% It is first defined as an element of ${\bf Q}_h$ i.e. a vector-valued field:
% \begin{verbatim}
% 	field eta (Qh);
% \end{verbatim}
% and then converted to a diagonal form representing the
% diffusion tensor operator:
% \begin{verbatim}
%         form_diag d (eta);
% \end{verbatim}
% Note that, by this way, an anisotropic diffusion operator
% could be considered.
% \findex{catchmark}%
% The statement
% \begin{verbatim}
%         cout << setprecision(numeric_limits<Float>::digits10)
%              << catchmark("epsilon") << epsilon << endl
%              << catchmark("u")       << uh;
% \end{verbatim}
% writes $\varepsilon$ and $u_h$
% in a labeled format, suitable for post-traitement,
% visualization and error analysis.
% \clindex{Float}%
% \cindex{floating point precision}%
% The number of digits printed depends upon the \code{Float}
% type. When \code{Float} is a \code{double}, this is roughly
% $15$ digits; this number depends upon the machine precision.
% Here, we choose to print all the relevant digits: this
% choice is suitable for the following error analysis.
% 
% 
% % -------------------------------------
% \subsection*{How to run the program ?}
% % -------------------------------------
% \cindex{region}
% \pindex{gnuplot}
% Build the program as usual: \code{make transmission}.
% Then, creates a one-dimensional geometry with two
% regions:
% \begin{verbatim}
%         mkgeo_grid -e 100 -region > line-region.geo
%         geo -gnuplot line-region.geo
% \end{verbatim}
% The trivial mesh generator \code{mkgeo_grid},
% defines two
% regions \code{east} and \code{west} when used with the
% \code{-region} option.
% This correspond to the situation:
% \[
% 	\Omega = [0,1]^d ,
% 	   \ \ \ 
% 	\Omega_{\rm west} = \Omega \cap \{ x_0 < 1/2 \}
% 	   \ \mbox{ and } \ 
% 	\Omega_{\rm east} = \Omega \cap \{ x_0 > 1/2 \}.
% \]
% In order to avoid mistakes with the {\tt C++} style indexes,
% we denote by $(x_0,\ldots,x_{d-1})$ the Cartesian coordinate
% system in $\mathbb{R}^d$.
% 
% Finally, run the program and look at the solution:
% \begin{verbatim}
%         ./transmission line-region.geo > transmission1d.mfield
%         mfield transmission1d.mfield -u | field -
% \end{verbatim}
% The two dimensional case corresponds to the commands:
% \begin{verbatim}
%         mkgeo_grid -t 10 -region > square-region.geo
%         geo square-region.geo
%         ./transmission square-region.geo > transmission2d.mfield
%         mfield transmission2d.mfield -u | field -
% \end{verbatim}
% while the tridimensional to
% \begin{verbatim}
%         mkgeo_grid -T 10 -region > cube-region.geo
%         ./transmission cube-region.geo > transmission3d.mfield
%         mfield transmission3d.mfield -u | field -
% \end{verbatim}
% % See also apendix~\ref{sec-region} for building an unstructured
% % mesh on a general geometry with regions.
% 
