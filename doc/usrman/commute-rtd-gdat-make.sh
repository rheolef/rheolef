#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
e=${1-"t"}
case $e in
 t|q) kmax=3; L="1 2 4 8 16 32 64 128 256";;
 *)   kmax=3; L="5 10 20 40";;
esac
k=0
while test $k -le $kmax; do
  echo "# RT${k}d $e"
  echo "# n err_p_l2 err_p_div_l2 err_i_l2 err_i_div_l2"
  for n in $L; do
    command="mkgeo_grid -$e $n > tmp.geo"
    #echo "! $command" 1>&2
    eval $command
    command="./commute_rtd_v2 tmp.geo ${k} 2>/dev/null | ./commute_rtd_error > tmp.txt 2>/dev/null"
    #echo "! $command" 1>&2
    eval $command
    err_p_l2=` grep err_p_l2 tmp.txt | gawk '{print $3}'`
    err_i_l2=` grep err_i_l2 tmp.txt | gawk '{print $3}'`
    err_p_div_l2=` grep err_p_div_l2 tmp.txt | gawk '{print $3}'`
    err_i_div_l2=` grep err_i_div_l2 tmp.txt | gawk '{print $3}'`
    echo "$n $err_p_l2 $err_p_div_l2 $err_i_l2 $err_i_div_l2"
  done
  echo; echo
  k=`expr $k + 1`
done
