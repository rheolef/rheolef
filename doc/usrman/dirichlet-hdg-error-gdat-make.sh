#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
n=${1-"1"}
e=${2-"t"}
case $e in
 e)   kmax=5; L="3 4 8 16 32 64 128 256 512 1024";;
 t|q) kmax=3; L="1 2 4 8 16 32 64 128 256";;
 *)   kmax=3; L="5 10 20 40";;
esac
k=0
while test $k -le $kmax; do
  echo "# n = $n"
  echo "# P${k}d $e"
  echo "# 1/h err_u_l2 err_u_linf err_u_h1 err_lambda_l2 err_lambda_linf err_sigma_l2 err_sigma_linf"
  for m in $L; do
    command="mkgeo_grid -$e $m > tmp.geo"
    #echo "! $command" 1>&2
    eval $command
    command="./dirichlet_hdg tmp.geo P${k}d $n 2>/dev/null | ./sinusprod_error_hdg 2> tmp.txt >/dev/null"
    #echo "! $command" 1>&2
    eval $command
    err_u_l2=` grep err_u_l2 tmp.txt | gawk '{print $3}'`
    err_u_linf=` grep err_u_linf tmp.txt | gawk '{print $3}'`
    err_u_h1=` grep err_u_h1 tmp.txt | gawk '{print $3}'`
    err_lambda_l2=` grep err_lambda_l2 tmp.txt | gawk '{print $3}'`
    err_lambda_linf="none"
    err_sigma_l2=` grep err_sigma_l2 tmp.txt | gawk '{print $3}'`
    err_sigma_linf=` grep err_sigma_linf tmp.txt | gawk '{print $3}'`
    echo "$m $err_u_l2 $err_u_linf $err_u_h1 $err_lambda_l2 $err_lambda_linf $err_sigma_l2 $err_sigma_linf"
  done
  echo; echo
  k=`expr $k + 1`
done
