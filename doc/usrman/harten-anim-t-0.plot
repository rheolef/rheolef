set terminal cairolatex pdf color standalone
set output "harten-anim-t-0.tex"

set size square 
set colors classic
set xrange [-1:1]
set yrange [0.5:1.5]
set xtics (-1,0,1)
set ytics (0.5,1,1.5)
set arrow from -1,1 to 1,1 nohead lt 1 lw 0.5 lc 0
set xlabel '[c]{$x$}'
set  label '[l]{$u(t,x)$}' at graph 0.02, 0.95
set  label '[c]{(a) $t=0$}' at graph 0.5, 0.95
plot \
  "harten-anim-t-0.gdat" \
	notitle \
	w l lt 1 lw 2 lc 0

