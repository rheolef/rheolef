set terminal cairolatex pdf color standalone
set output "zalesak-error-l1-P2d-time.tex"

set size square
set log xy
set key bottom
set xlabel '[c]{$\Delta t$}'
set  label '[l]{$E_h^{(n_{max})}$}' at graph 0.03,0.92
set  label '[l]{$k=2$, $p=3$}' at graph 0.6,0.30
set xrange [1e-4:1.]
set yrange [1e-4:10]
set xtics (\
	'[l]{$10^{-4}$}' 1e-4, \
	'[c]{$10^{-3}$}' 1e-3, \
	'[c]{$10^{-2}$}' 1e-2, \
	'[c]{$10^{-1}$}' 1e-1, \
	'[c]{$1$}'       1)
set ytics (\
	'[r]{$10^{-4}$}' 1e-4, \
	'[r]{$10^{-3}$}' 1e-3, \
	'[r]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-1}$}' 1e-1, \
	'[r]{$1$}'       1)
plot \
'zalesak-error-l1-P2d-time-mass.gdat' \
	i 0 u ((4*pi)/$1):2 \
	t '[r]{$h=1/25$}' \
	w lp lt 1 pt 1 lw 2 lc rgb '#ff0000', \
'zalesak-error-l1-P2d-time.gdat' \
	i 1 u ((4*pi)/$1):2 \
	t '[r]{$h=0.04$}' \
	w lp lt 1 pt 2 dt 2 lw 2 lc rgb '#008800', \
'zalesak-error-l1-P2d-time.gdat' \
	i 2 u ((4*pi)/$1):2 \
	t '[r]{$h=0.02$}' \
	w lp lt 1 pt 3 dt 2 lw 2 lc rgb '#0000ff', \
'zalesak-error-l1-P2d-time.gdat' \
	i 3 u ((4*pi)/$1):2 \
	t '[r]{$h=0.01$}' \
	w lp lt 1 pt 8 dt 2 lw 2 lc rgb '#ff00ff'

#pause -1 "<retour>"
