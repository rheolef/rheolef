set terminal cairolatex pdf color standalone
set output "speedup-2d-solve-iter.tex"

# dirichlet_time.cc
set key Left left

set size square
set colors classic
set xrange [0:32]
set yrange [0:32]
set xtics (0,  8,16,24,32)
set ytics (0,1,8,16,24,32)
set xlabel '[c]{$p$}'
set  label '[r]{$S(p)$}' at graph -0.02, 0.87
set  label '[l]{\em iterative solve $d=2$}' at graph 0.05, 0.60

plot \
'speedup-dirichlet-square-40-P1-iter.gdat'   \
  u 1:(0.00282979/$4) \
  t 'square-40'   \
  w lp lc 1 pt 1, \
'speedup-dirichlet-square-80-P1-iter.gdat'   \
  u 1:(0.0149441/$4) \
  t 'square-80'   \
  w lp lc rgb '#008800' pt 2, \
'speedup-dirichlet-square-160-P1-iter.gdat'  \
  u 1:(0.108872/$4) \
  t 'square-160'  \
  w lp lc 3 pt 3, \
'speedup-dirichlet-square-320-P1-iter.gdat'  \
  u 1:(0.860483/$4) \
  t 'square-320'  \
  w lp lc 4 pt 4, \
'speedup-dirichlet-square-640-P1-iter.gdat'  \
  u 1:(8.78795/$4) \
  t 'square-640'  \
  w lp lc 5 pt 5, \
'speedup-dirichlet-square-1280-P1-iter.gdat' \
  u 1:(75.0865/$4)  \
  t 'square-1280' \
  w lp lc 7 pt 7, \
x notitle \
  w l dt 2 lc 0, \
1 notitle \
  w l dt 2 lc 0

#pause -1 "<retour>"
