%
% This file is part of Rheolef.
%
% Copyright (C) 2000-2009 Pierre Saramito 
%
% Rheolef is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% Rheolef is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Rheolef; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
%
% -------------------------------------------------------------------

% =============================================================================
\subsubsection*{Formulation}
% =============================================================================
\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{fig-obstacle-slip-quarter.pdf}
  \end{center}
  \caption{Slip boundary conditions for the flow around an obstacle.}
  \label{fig-obstacle-slip-quarter}
\end{figure}

\cindex{boundary condition!slip}%
\cindex{geometry!obstacle}%
\cindex{problem!Stokes}%
We consider an approximation of
the Stokes problem with slip boundary conditions.
As a test case, we consider the flow around a circular obstacle,
as represented on Fig.~\ref{fig-obstacle-slip-quarter}.
We assume a permanent flow and: due to the symmetries
versus upstream/downstream and with respect to the 
horizontal axis, the computational domain
reduces to the quarter of the geometry.

This problem writes~\citep{Ver-1987-slip}:

\mbox{} $(P)$:
\emph{
  find $\boldsymbol{u}$
  and $p$,
  defined in $\Omega$,
  such that
}
\begin{subequations}
\begin{empheq}[left={\empheqlbrace}]{align}
  \boldsymbol{\sigma} 
  =
  -p\boldsymbol{I} + 2D(\boldsymbol{u}) 
  &\ \mbox{ in } \Omega
  \\
  - \mathbf{div}\,\boldsymbol{\sigma}
  =
  0
  &\ \mbox{ in } \Omega
  \\
  - \mathrm{div}\,\boldsymbol{u}
  =
  0
  &\ \mbox{ in } \Omega
  \\
  \boldsymbol{u}.\boldsymbol{n}
  =
  0
  \mbox{ and }
  \boldsymbol{\sigma}_{nt}
  =
  0
  &\ \mbox{ on } \Gamma_{wall}\cup\Gamma_{axis}\cup\Gamma_{obstacle}
  \label{eq-stokes-slip-bc-slip}%
  \\
  \boldsymbol{u}_t
  =
  0
  \mbox{ and }
  \sigma_{nn}
  =
  0
  &\ \mbox{ on } \Gamma_{vaxis}
  \\
  \boldsymbol{u}
  =
  \boldsymbol{e}_0
  &\ \mbox{ on } \Gamma_{downstream}
\end{empheq}
\end{subequations}
with the notation
\mbox{$
  \boldsymbol{v}_{t}
  =
  \boldsymbol{v}
  -
  (\boldsymbol{v}.\boldsymbol{n})
  \boldsymbol{n}
$},
\mbox{$
  \tau_{nn}
  =
  (\boldsymbol{\tau} \boldsymbol{n}).\boldsymbol{n}
$}
and
\mbox{$
  \boldsymbol{\tau}_{nt}
  =
  \boldsymbol{\tau} \boldsymbol{n}
  -
  \tau_{nn}
  \boldsymbol{n}
$}
for any vector~$\boldsymbol{v}$ and tensor~$\boldsymbol{\tau}$.
%
Observe that $\Gamma_{wall}$, $\Gamma_{axis}$
and $\Gamma_{vaxis}$ are parallel to the coordinate axis:
the corresponding slip boundary condition writes also
\begin{empheq}{align*}
  u_{d-1}
  =
  0
  \mbox{ and }
  \sigma_{i,d\!-\!1}
  =
  0
  &
  \ \mbox{ on } \Gamma_{wall}\cup\Gamma_{axis}
  ,\ \ 1\leq i\leq d-2
  \\
  u_i 
  =
  0
  \mbox{ and }
  \sigma_{0,0}
  =
  0
  &
  \ \mbox{ on } \Gamma_{vaxis}
  ,\ \ 1\leq i\leq d-1
\end{empheq}
It remains one slip boundary condition on the
curved boundary domain~$\Gamma_{obstacle}$:
imposing such a boundary condition is the main
difficulty of this problem.
%
There are mainly three approaches for the imposition of this
boundary condition:
(i)~the regularization~;~
(ii)~the Lagrange multiplier weak imposition~;~
(iii)~the strong imposition.
%
The main drawback of the Lagrange multiplier
weak imposition approach
is the discretization of the Lagrange multiplier~$\lambda$,
that should satisfy the inf-sup condition~\citep{Ver-1987-slip,Ver-1991,Lia-2001,CagLia-2009}.
An alternative is to introduce
stabilization terms (\citealp[p.~621]{Ver-1991}, eqn~(4.1)),
but the resulting problem is no more symmetric.
The strong imposition~\citep{Ver-1985-b} requires 
some modifications of the finite element basis along the slip boundary:
this promising feature is in development
in the \Rheolef\  library.
The rest of this section focuses on the regularization approach.

\cindex{boundary condition!Robin}%
The main idea of the regularization approach is
to replace the slip boundary condition~\eqref{eq-stokes-slip-bc-slip}
on the curved boundary domain~$\Gamma_{obstacle}$
by Robin one:
\begin{empheq}{align*}
  \boldsymbol{\sigma}_{\boldsymbol{nt}}
  +
  \varepsilon^{-1}
  \boldsymbol{u}.\boldsymbol{n}
  =
  0
  &\ \mbox{ on } \Gamma_{obstacle}
\end{empheq}
\cindex{method!regularization}%
where $\varepsilon>0$ is the regularization parameter.
It leads to the following variational formulation:

\mbox{} $(FV)_\varepsilon$:
\emph{
  find $\boldsymbol{u}\in V(1)$
  and $p\in L^2(\Omega)$ 
  such that
}
\begin{empheq}[left={\empheqlbrace}]{align*}
  &
  a(\boldsymbol{u},\boldsymbol{v})
  +
  b(\boldsymbol{v},p)
  =
  0 
  ,\ \ \forall \boldsymbol{v}\in V(0)
  \\
  &
  b(\boldsymbol{u},q)
  =
  0
  ,\ \ \forall q\in L^2(\Omega)
\end{empheq}
with
\begin{empheq}{align*}
  a(\boldsymbol{u},\boldsymbol{v})
  &=
  \int_\Omega
    2D(\boldsymbol{u})
    \!:\!
    2D(\boldsymbol{v})
    \,\mathrm{d}x
  +
  \varepsilon^{-1}
  \int_{\partial\Omega}
    (\boldsymbol{u}.\boldsymbol{n})
    (\boldsymbol{v}.\boldsymbol{n})
    \,\mathrm{d}s
  \\
  b(\boldsymbol{v},q)
  &=
  -
  \int_\Omega
    q\,
    \mathrm{div}\,\boldsymbol{v}
    \,\mathrm{d}x
  \\
  V(\alpha)
  &=
  \left\{
     \boldsymbol{v} \in \left( H^1(\Omega)\right)^d ~/~
     \boldsymbol{v} = \alpha\boldsymbol{e}_0
     \mbox{ on } \Gamma_{downstream}
  \right.
  \\
  & \phantom{mmmmmm}
  \left.
     \ \mbox{ and }
     u_i
     =
     0
     \mbox{ on } \Gamma_{vaxis}
     ,\ \ 1\leq i \leq d-1
  \right.
  \\
  & \phantom{mmmmmm}
  \left.
     \ \mbox{ and }
     u_{d-1}
     =
     0
     \mbox{ on } \Gamma_{wall}\cup\Gamma_{axis}
  \right\}
\end{empheq}

% =============================================================================
\subsubsection*{Approximation}
% =============================================================================
\cindex{Babu\v{s}ka paradox}%
The curved boundary~$\partial\Omega$ is replaced
polynomial approximation~$\partial\mathscr{T}_h$.
Then, a natural procedure would be to replace the
the normal~$\boldsymbol{n}$ on~$\partial\Omega$
by the normal~$\boldsymbol{n}_h$ on~$\partial\mathscr{T}_h$
in the previous expression of the bilinear form~$a$.
This approach do not converge in general and this
counter-intuitive feature is called
the Babu\v{s}ka paradox, see e.g. \citet[p.~473]{Ver-1985-b}.
We have to deal with either the exact normal~$\boldsymbol{n}$
or a more accurate approximation~$\widetilde{\boldsymbol{n}}_h$
of~$\boldsymbol{n}$.
In the present case, as the exact normal~$\boldsymbol{n}$
is known, we use it.

Otherwise, the space $V(\alpha)$ is completely standard,
for any~$\alpha\in\mathbb{R}$ and can be approximated
by classical Lagrange finite elements.
\apindex{P2-P1, Taylor-Hood}%
Thus, the \citet*{hood-taylor-73} finite element approximation
of the Stokes problem is considered.
The present implementation
\file{stokes_obstacle_slip_regul.cc}
supports both the 2D Cartesian
and the 3D axisymmetric cases,
so the 3D Cartesian case is not implemented here.
Note that the 2D Cartesian case corresponds to a 3D cylindrical obstacle
while the 3D axisymmetric case corresponds to a spherical
obstacle.
The \file{streamf_obstacle_slip_move.cc}
computes the stream function the modified velocity field
\mbox{$
  \widetilde{\boldsymbol{u}}_h
  =
  \boldsymbol{e}_0
  -
  \boldsymbol{u}_h
$}
relative to the moving obstacle.

% ----------------------------
\myexamplelicense{stokes_obstacle_slip_regul.cc}

\myexamplelicense{streamf_obstacle_slip_move.cc}
% ----------------------------

% =============================================================================
\subsubsection*{How to run the program}
% =============================================================================
\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{rl}
      \includegraphics[width=0.45\textwidth]{obstacle-slip-streamf.jpg}&
      \includegraphics[width=0.45\textwidth]{obstacle-zr-slip-streamf.jpg}
    \end{tabular}
  \end{center}
  \caption{Slip boundary conditions for the flow around a
	cylinder (left) and sphere (right):
	isovalues of the stream function.}
  \label{fig-obstacle-visu}
\end{figure}
The run is detailed in the axisymmetric case.
\pindex{mkgeo_obstacle}%
The \code{mkgeo_obstacle} script generates the mesh of the geometry:
\begin{verbatim}
  mkgeo_obstacle -zr -name obstacle-zr
  geo obstacle-zr.geo
\end{verbatim}
Then the compilation and run writes:
\begin{verbatim}
  make stokes_obstacle_slip_regul ./streamf_obstacle_slip_move
  ./stokes_obstacle_slip_regul obstacle-zr > obstacle-zr.field
  field -velocity obstacle-zr.field
  ./streamf_obstacle_slip_move < obstacle-zr.field | field -bw -n-iso 25 -
\end{verbatim}
Observe on Fig.~\ref{fig-obstacle-visu} that the trajectories,
as represented by the stream function, differ
in the Cartesian an axisymmetric cases.

\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{rl}
      \includegraphics[height=0.35\textwidth]{obstacle-slip-ux-yaxis.pdf}&
      \includegraphics[height=0.35\textwidth]{obstacle-slip-ux-xaxis.pdf}
    \end{tabular}
  \end{center}
  \caption{Slip boundary conditions for the flow around an obstacle:
	horizontal relative velocity along the two axis:
        (left) along~$Ox_1$ on the top of the obstacle~;
        (right) along~$Ox_0$ on the front.
  }
  \label{fig-obstacle-slip-ux-axis}
\end{figure}
Let us turn to the cut of the relative velocity along the
horizontal and vertical axis, named respectively
\code{axis} and \code{vaxis} by the mesh:
\begin{verbatim}
  field obstacle-zr.field -domain vaxis -comp 0 -gnuplot -elevation
  field obstacle-zr.field -domain  axis -comp 0 -gnuplot -elevation
\end{verbatim}
Observe on Fig.~\ref{fig-obstacle-slip-ux-axis}.left
that the relative velocity is negative on the top
of the obstacle.
Indeed, when the obstacle moves right,
the fluid moves from the front of the obstacle,
rotates around the obstacle and goes to the back.
Thus, the fluid moves in the negatives direction
when it is on the top of the obstacle.
Also, observe that the fluid is more accelerated
when it flows around a cylinder than around a sphere.
Fig.~\ref{fig-obstacle-slip-ux-axis}.right
shows that the perturbation caused by the fluid decreases
faster for a sphere than for a cylinder.

% TODO: Cx trainee vs option "-c" : Cartesian & axi
