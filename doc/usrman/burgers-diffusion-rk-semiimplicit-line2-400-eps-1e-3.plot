set terminal cairolatex pdf color standalone
set output "burgers-diffusion-rk-semiimplicit-line2-400-eps-1e-3.tex"

set size square 
set sample 1000
set key at graph 0.7, 0.99
#set key at graph 0.1,0.1
set xrange [-1:1]
set yrange [-0.2:2.6]
set xtics (-1,0,1) nomirror
set ytics (0,1,2)
set arrow from -1,0 to 1,0 nohead dt 2 lc 0 lw 1
set xlabel '[c]{$x$}'
set  label '[l]{$u(t,x)$}' at graph 0.02,0.95

eps = 1e-1
x0=-0.5
u(t,x) = 1 - tanh((x-x0-t)/(2*eps))

plot \
 "burgers-diffusion-rk-semiimplicit-line2-400-eps-1e-3-init.gdat" \
   t '[r]{$t=0$}' \
   w l lw 2 lc 0, \
 "burgers-diffusion-rk-semiimplicit-line2-400-eps-1e-3-final.gdat" \
   t '[r]{$t=1$}'\
   w l lw 2 lc rgb "#990000"

#pause -1
