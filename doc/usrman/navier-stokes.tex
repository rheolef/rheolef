% - 2nd order scheme presentation: [FouPip-2004,BouMadMetRaz-1997]
% - Fig. compare ux and uy cuts [GhiGhiShi-1982]
% - PCG convergence versus Re/delta_t and meshes
% - converence in time to stationary solution
% => a newton method ?
% QUESTIONS :
% - adapt criteria ? see freefem...
\section{The Navier-Stokes equations}
\label{sec-navier-stokes}
\pbindex{Navier-Stokes}%
\pbindex{nonlinear}%
\pbindex{Stokes}%
\cindex{boundary condition!Dirichlet}%
\cindex{benchmark!driven cavity flow}%
\apindex{P1}%
\apindex{P2}%

\subsubsection*{Formulation}
  This longer example combines most functionalities presented
  in the previous examples.

  Let us consider the Navier-Stokes problem for the driven cavity
  in $\Omega = ]0,1[^d$, $d = 2,3$.
  Let $Re>0$ be the Reynolds number,
  and $T>0$ a final time.
  The problem writes:

  \ \ \ $(NS)$: {\it find ${\bf u}=(u_0,\ldots,u_{d-1})$ 
  	and $p$ defined in $\Omega \times ]0,T[$ such that:}
  \[
    \begin{array}{cccccl}
      Re\left( \Frac{\partial {\bf u}}{\partial t} + {\bf u}.\nabla {\bf u} \right)
         &-\ {\bf div}(2 D({\bf u})) 
         &+& \bnabla p &=& 0 \ {\rm in}\ \Omega \times ]0,T[, \\
      &-\ {\rm div}\,{\bf u}     &&            &=& 0 \ {\rm in}\ \Omega \times ]0,T[, \\
      &{\bf u}(t\!=\!0) && &=& 0 \ {\rm in}\ \Omega \times \{0,T\}, \\
      &{\bf u} && &=& (1,0) \ {\rm on}\ \Gamma_{\rm top} \times ]0,T[, \\
      &{\bf u} && &=& 0
         \ {\rm on}\ (\Gamma_{\rm left} \cup \Gamma_{\rm right} \cup \Gamma_{\rm bottom})
	 \times ]0,T[, \\
      &\Frac{\partial u_0}{\partial {\bf n}} &=&
         \Frac{\partial u_1}{\partial {\bf n}} &=&
         u_2 = 0 \ {\rm on}\ (\Gamma_{\rm back} \cup \Gamma_{\rm front})\times ]0,T[
	 \ \mbox{ when } d=3,
    \end{array}
  \]
  where $D({\bf u}) = (\bnabla {\bf u} + \bnabla {\bf u}^T)/2$.
  This nonlinear problem is the natural extension of the linear Stokes
  problem, as presented in paragraph~\ref{sec-navier-stokes},
  page~\pageref{sec-navier-stokes}.
  The boundaries are represented on 
  Fig.~\ref{fig-square-cube}, page~\pageref{fig-square-cube}.

\subsubsection*{Time approximation}
% ------------------------------
\cindex{method!characteristic}%

Let $\Delta t > 0$.
Let us consider the following backward second
order scheme, for all
$\phi\in C^2([0,T])$~:
\[
  \Frac{{\rm d}\phi}{{\rm d}t}(t)
  =
  \Frac{3\phi(t) - 4\phi(t-\Delta t) + \phi(t-2\Delta t)}
       {2\Delta t}
  +
  \mathcal{O}(\Delta t^2)
\]
\cindex{method!BDF2 scheme}%
The problem is approximated by the following
second-order implicit scheme (BDF2):
\[
    \begin{array}{cccccl}
       Re
       \Frac{
          3{\bf u}^{n+1}
         -4{\bf u}^{n} \circ X^n
         + {\bf u}^{n-1} \circ X^{n-1}
       }{
         2\Delta t
       } 
       &-\ {\bf div}(2 D({\bf u}^{n+1}))
       &+& \bnabla p^{n+1} &=& 0 \ {\rm in}\ \Omega, \\
      &-\ {\rm div}\,{\bf u}^{n+1}     &&            &=& 0 \ {\rm in}\ \Omega, \\
      &{\bf u}^{n+1} && &=& (1,0) \ {\rm on}\ \Gamma_{\rm top}, \\
      &{\bf u}^{n+1} && &=& 0
        \ {\rm on}\ \Gamma_{\rm left} \cup \Gamma_{\rm right} \cup \Gamma_{\rm bottom}, \\
      &\Frac{\partial u_0^{n+1}}{\partial {\bf n}} =
      \Frac{\partial u_1^{n+1}}{\partial {\bf n}}
      &=&
      u_2^{n+1} 
      &=& 0 
      \ {\rm on}\ \Gamma_{\rm back} \cup \Gamma_{\rm front} \ \mbox{ when } d=3,
    \end{array}
\]
where, following~\citep{BouMadMetRaz-1997,FouPip-2004}: 
\begin{eqnarray*}
   X^n(x) &=& x - \Delta t\, {\bf u}^*(x) \\
   X^{n-1}(x) &=& x - 2\Delta t\, {\bf u}^*(x) \\
   {\bf u}^* &=& 2{\bf u}^n - {\bf u}^{n-1}
\end{eqnarray*}
It is a second order extension of
the method previously introduced in
paragraph~\ref{characteristic-method}
page~\pageref{characteristic-method}.
The scheme defines a second order recurrence 
for the sequence $({\bf u}^{n})_{n\geq -1}$,
that  starts with ${\bf u}^{-1}={\bf u}^{0}=0$.

\subsubsection*{Variational formulation}
% ------------------------------------
The variational formulation of this problem expresses:
 
  \ \ \ $(NS)_{\Delta t}$: \ {\it find ${\bf u}^{n+1}\in {\bf V}(1)$ and $p^{n+1} \in L^2_0(\Omega)$ such that:}
  \[
     \begin{array}{lcccll}
      a({\bf u}^{n+1},{\bf v}) &+& b({\bf v}, p^{n+1}) 
      	&=& m({\bf f}^{n},{\bf v}),
	& \forall {\bf v}\in {\bf V}(0), \\
	&&&&\\
      b({\bf u}^{n+1},q) && &=& 0, & \forall q \in L^2_0(\Omega),
     \end{array}
  \]
  where
  \[
	{\bf f}^n
	=
      	\Frac{Re}{2\Delta t} 
        \left(
          4\, {\bf u}^{n}\circ X^n
          -
          {\bf u}^{n-1}\circ X^n
        \right)
  \]
\cindex{form!{$2D({\bf u}):D({\bf v})+{\bf u}.{\bf v}$}}%
  and
  \begin{eqnarray*}
       a({\bf u},{\bf v}) &=& \Frac{3 Re}{2\Delta t}  \int_\Omega {\bf u} . {\bf v} \, dx
       		+ \int_\Omega 2 D({\bf u}) : D({\bf v}) \, dx
  \end{eqnarray*}
  and $b(.,.)$ and ${\bf V}(\alpha)$
  was already introduced in paragraph~\ref{sec-stokes},
  page~\pageref{sec-stokes}, while studying the Stokes problem.

\subsubsection*{Space approximation}
% ------------------------------------
  The \citet*{hood-taylor-73} finite element approximation
  of this generalized Stokes problem was also considered
  in paragraph~\ref{sec-stokes},  page~\pageref{sec-stokes}.
  We introduce a mesh ${\cal T}_h$ of $\Omega$
  and the finite dimensional spaces
  ${\bf X}_h$, ${\bf V}_h(\alpha)$ and $Q_h$.
  The approximate problem writes:

  \ \ \ $(NS)_{\Delta t,h}$: {\it find ${\bf u}_h^{n+1} \in {\bf V}_h(1)$ and $p^{n+1}\in Q_h$ such that:}
  \begin{equation}
    \begin{array}{lcccll}
      a({\bf u}_h^{n+1},{\bf v}) &+& b({\bf v}, p_h^{n+1}) &=& 
      	m({\bf f}_h^{n},{\bf v}),
        & \forall {\bf v}\in {\bf V}_h(0), \\
      b({\bf u}_h^{n+1},q) && &=& 0, & \forall q \in Q_h.
    \end{array}
    \label{eq-fvh-navier-stokes}
  \end{equation}
  where
  \[
	{\bf f}_h^n
	=
      	\Frac{Re}{2\Delta t} 
        \left(
          4\, {\bf u}_h^{n}\circ X^n
          -
          {\bf u}_h^{n-1}\circ X^n
        \right)
  \]
  The problem reduces to a sequence resolution
  of a generalized Stokes problems.

% ----------------------------------
\myexamplelicense{navier_stokes_solve.icc}
% ----------------------------------

\subsubsection*{Comments}
% ----------------------------------
  The \code{navier_stokes_solve} function is similar to the
  \reffile{stokes_cavity.cc}.
  It solves here a generalized Stokes problem
  and manages a right-hand side ${\bf f}_h$:
\begin{lstlisting}[numbers=none,frame=none]
  characteristic X1 (    -delta_t*uh_star);
  characteristic X2 (-2.0*delta_t*uh_star);
  field l1h = integrate (compose(uh1,X1)*v, iopt);
  field l2h = integrate (compose(uh2,X2)*v, iopt);
  field lh  = l0h + (Re/delta_t)*(2*l1h - 0.5*l2h);
\end{lstlisting}
\exindex{convect.cc}%
  This last computation is similar to those done in the
  \reffile{convect.cc} example.
\clindex{solver_abtb}%
  The generalized Stokes problem is solved by the \code{solver_abtb} class.
  The stopping criterion is related to the stationary solution or the maximal
  iteration number.
% TODO: 3D case
%\cindex{preconditioner!Cahouet-Chabart}%
%  The generalized Stokes problem is solved by the \code{solver_abtb} class.
%  The preconditioner is here the Cahouet and Chabart
%  one~\citep{CahCha-1988}.
%  As showed in~\citep{KobOls-2000-precond-uzawa-stokes-gen},
%  the number of iterations need by the conjugate gradient algorithm
%  to reach a given precision is then independent of the mesh size.
%  This preconditioner leads to the
%  resolution of the following subproblem:
%  \[
%	(M_h^{-1} + \lambda A_h^{-1})q_h = r_h
%  \]
%\pbindex{Poisson}%
%\cindex{boundary condition!Neumann}%
%  where $\lambda=Re/\Delta t$,
%  $r_h\in Q_h$ is given and $M$ and $A$ are respectively the
%  the mass matrix and the discrete Poisson operator
%  with Neumann boundary condition.
%  The resolution of this subproblem
%  has been previously developed in section~\ref{sec-neumann-laplace},
%  page~\pageref{sec-neumann-laplace}.
%
%\myexamplelicense{cahouet-chabart.h} TODO

% ----------------------------------
\myexamplelicense{navier_stokes_cavity.cc}
\myexamplelicense{navier_stokes_criterion.icc}
% ----------------------------------
\findex{norm2}%

\subsubsection*{Comments}
% ----------------------------------
\cindex{mesh!adaptation!anisotropic}%
\findex{compose}%
  The code performs a computation by
  using adaptive mesh refinement, in order to capture
  recirculation zones.
  The \code{adapt_option} declaration is used by \code{rheolef} to
  send options to the mesh generator.
\exindex{cavity.h}%
  The code reuse the file \reffile{cavity.h}
  introduced page~\pageref{cavity.h}.
  This file contains two functions that defines
  boundary conditions associated to the cavity driven problem.

  The \code{criteria} function computes the
  adaptive mesh refinement criteria:
  \[
     c_h = (Re|{\bf u}_h|^2 + 2|D({\bf u}_h)|^2)^{1/2}
  \]
  The \code{criteria} function is similar to those presented
  in the \reffile{embankment_adapt.cc} example.

\subsubsection*{How to run the program}
% ----------------------------------

  \begin{figure}[htb]
      \mbox{} 
      \hspace{-0cm}
      \mbox{} 
      \begin{tabular}{cc}
        $Re=100$: 4804 elements, 2552 vertices & 
	$\psi_{max} =  9.5\times 10^{-6}, \psi_{min} = -0.103 $ \\
	&\\
	\includegraphics[width=7cm]{navier-stokes-Re=100-geo.pdf} &
        \includegraphics[width=7cm]{navier-stokes-Re=100-psi.pdf} \\
	&\\
        $Re=400$: 5233 elements, 2768 vertices & 
	$\psi_{max} =  6.4\times 10^{-4}, \psi_{min} = -0.111 $ \\
	&\\
        \includegraphics[width=7cm]{navier-stokes-Re=400-geo.pdf} &
        \includegraphics[width=7cm]{navier-stokes-Re=400-psi.pdf}
      \end{tabular}
    \caption{Meshes and stream functions associated to the solution of the
    	Navier-Stokes equations for $Re=100$ (top) and $Re=400$ (bottom).}
    \label{fig-navier-stokes}
  \end{figure}
  \begin{figure}[htb]
      \mbox{} 
      \hspace{-0cm}
      \mbox{} 
      \begin{tabular}{cc}
        $Re=1000$: 5873 elements, 3106 vertices & 
	$\psi_{max} =  1.64\times 10^{-3}, \psi_{min} = -0.117 $ \\
	&\\
	\includegraphics[width=7cm]{navier-stokes-Re=1000-geo.pdf} &
        \includegraphics[width=7cm]{navier-stokes-Re=1000-psi.pdf} \\
      \end{tabular}
    \caption{Meshes and stream functions associated to the solution of the
    	Navier-Stokes equations for $Re=1000$.}
    \label{fig-navier-stokes-2}
  \end{figure}
  The mesh loop adaptation is initiated from a \code{bamg} mesh
  (see also appendix~\ref{sec-bamg}).
\pindex{bamg}%
\exindex{square.bamgcad}%
\exindex{square.dmn}%
\begin{verbatim}
  bamg -g square.bamgcad -o square.bamg
  bamg2geo square.bamg square.dmn > square.geo
\end{verbatim}
  Then, compile and run the Navier-Stokes solver for the driven cavity
  for $Re=100$:
\begin{verbatim}
  make navier_stokes_cavity
  time mpirun -np 8 ./navier_stokes_cavity square.geo 100
\end{verbatim}
  The program performs a computation with $Re=100$.
  By default the time step is $\Delta t = 0.05$
  and the computation loops for five mesh adaptations.
  At each time step, the program prints an approximation of the time derivative,
  and stops when a stationary solution is reached.
\pindex{mpirun}%
\pindex{time}%
\cindex{speedup}%
  The \mbox{\code{mpirun -np 8}} prefix allows a parallel and distributed run
  while the \code{time} one returns the \code{real}
  and the \code{user} times used by the computation.
  The speedup could be estimated here by the ratio \code{user}/\code{real}:
  it is ideally close to the number of processors.
  These prefixes are optional and you can omit the \code{mpirun} one if you are running with
  a sequential installation of \Rheolef.
  Then, we visualize the \file{square-005.geo} adapted mesh and its associated solution:
\pindexopt{field}{-velocity}%
\begin{verbatim}
  geo   square-005.geo
  field square-005.field.gz -velocity
\end{verbatim}
\exindex{streamf_cavity.cc}%
\pindexopt{field}{-bw}%
\pindexopt{field}{-n-iso-negative}%
\pindex{zcat}%
\cindex{stream function}%
  The representation of the stream function writes:
\begin{verbatim}
  make streamf_cavity
  zcat square-005.field.gz | ./streamf_cavity | field -bw -n-iso-negative 10 -
\end{verbatim}
  The programs \reffile{streamf_cavity.cc},
  already introduced page~\pageref{streamf_cavity.cc}, is here reused.
  The last options of the \code{field} program draws isocontours
  of the stream function using lines, as shown on Fig.~\ref{fig-navier-stokes}.
  The zero isovalue separates the main flow from recirculations,
  located in corners at the bottom of the cavity.

  For $Re=400$ and $1000$ the computation writes:
\begin{verbatim}
  ./navier_stokes_cavity square.geo  400
  ./navier_stokes_cavity square.geo 1000
\end{verbatim}

  \begin{figure}[htb]
   \begin{tabular}{ll}
	\hspace{-0cm} \includegraphics{navier-stokes-cut-u0.pdf}& 
	\hspace{-0cm} \includegraphics{navier-stokes-cut-u1.pdf}
   \end{tabular}
    \caption{Navier-Stokes: velocity profiles along lines passing
	thought the center of the cavity, compared 
	with data from~\citet{GhiGhiShi-1982}:
	(a) $u_0$ along the vertical line;
	(b) $u_1$ along the horizontal line line.}
    \label{fig-navier-stokes-cut}
  \end{figure}
  The visualization of the cut of the horizontal velocity
  along the vertical median line writes:
\pindexopt{field}{-comp}%
\pindexopt{field}{-cut}%
\pindexopt{field}{-normal}%
\pindexopt{field}{-origin}%
\pindexopt{field}{-gnuplot}%
\begin{verbatim}
  field square-005.field.gz -comp 0 -cut -normal -1 0 -origin 0.5 0   -gnuplot
  field square-005.field.gz -comp 1 -cut -normal  0 1 -origin 0   0.5 -gnuplot
\end{verbatim}
  Fig.~\ref{fig-navier-stokes-cut} compare the cuts with data
  from~\citet{GhiGhiShi-1982}, table~1 and~2
  (see also~\citealp{GupKal-2005}).
  Observe that the solution is in good agreement with
  these previous computations.

  \begin{figure}[htb]
    \begin{center}
     \begin{tabular}{lllllc}
     \hline
      $Re$ & & $x_c$ & $y_c$ & $-\psi_{\rm min}$ & $\psi_{\rm max}$ \\
     \hline
      100  & present
	   & 0.613 & 0.738 & 0.103 & $9.5\times 10^{-6}$ \\
           & {\citet*{LabWel-2007}}
	   & 0.608 & 0.737 & 0.104 & - \\
           & {\citet*{DonHue-2003}}
	   & 0.62 & 0.74 & 0.103   & - \\
     \hline
      400  & present 
           & 0.554 & 0.607 & 0.111 & $5.6 \times 10^{-4}$ \\
           & {\citet*{LabWel-2007}}
           & 0.557 & 0.611 & 0.115 & - \\
           & {\citet*{DonHue-2003}}
           & 0.568 & 0.606 & 0.110 & - \\
     \hline
     1000  & present 
           & 0.532 & 0.569 & 0.117 & $1.6\times 10^{-3}$ \\
           & {\citet*{LabWel-2007}}
           & 0.524 & 0.560 & 0.121 & - \\
           & {\citet*{DonHue-2003}}
           & 0.540 & 0.573 & 0.110 & - \\
     \hline
     \end{tabular}
    \end{center}
    \caption{Cavity flow: primary vortex position and stream
	function value.}
    \label{tab-vortex}
  \end{figure}
  Finally, table~\ref{tab-vortex} compares the
  primary vortex position and its associated stream function value.
  Note also the good agreement with previous simulations.
  The stream function extreme values are obtained by:
\pindexopt{field}{-min}%
\pindexopt{field}{-max}%
\begin{verbatim}
  zcat square-005.field.gz | ./streamf_cavity | field -min -
  zcat square-005.field.gz | ./streamf_cavity | field -max -
\end{verbatim}
  The maximal value has not yet been communicated to our knowledge and
  is provided in table~\ref{tab-vortex} for cross validation purpose.
  The small program that computes the primary vortex position is showed below.
\begin{verbatim}
  make vortex_position
  zcat square-005.field.gz | ./streamf_cavity | ./vortex_position
\end{verbatim}

% -----------------------------
\myexamplelicense{vortex_position.cc}
% -----------------------------

  For higher Reynolds number,
  \citet{shen-1991} showed in 1991 that the flow converges
  to a stationary state
  for Reynolds numbers up to $10\,000$; for Reynolds numbers larger
  than a critical value \mbox{$10\,000<Re_1<10\,500$} and less than another critical
  value \mbox{$15\,000<Re_2<16\,000$}, these authors 
  founded that the flow becomes periodic in time which
  indicates a Hopf bifurcation; the flow loses time periodicity for
  $Re\ge Re_2$.
  \citet{Oul-1998} founded a loss of stationnarity
  between $10\,000$ and $20\,000$.
  \citet*{AutParQua-2002} estimated
  the critical value for the apparition of the first instability
  to $Re_1\approx 8018$.
  \citet*{ErtCorGok-2005} computed
  steady driven cavity solutions up to $Re \leq 21\,000$.
  This result was infirmed by~\citet*{GelLubOlsSta-2005}:
  these authors
  estimated $Re_1$ close to $8000$, in agreement with~\citet*{AutParQua-2002}.
  The 3D driven cavity has been investigated in~\citet*{MinEth-1998}
  by the method of characteristic (see also~\citealp*{MelLegDooWat-2012}
  for 3D driven cavity computations).
  In conclusion, the exploration of the driven cavity at large
  Reynolds number is a fundamental challenge in computational
  fluid dynamics.

  Note that, instead of using a time-dependent scheme, that requires many time
  steps, it is possible to
  directly compute the stationary solution of the Navier-Stokes problem,
  thanks to a nonlinear solver.
  This alternative approach is presented in section~\ref{dg-sec-navier-stokes},
  page~\pageref{dg-sec-navier-stokes}, based on the discontinuous Galerkin method.
  The discontinuous Galerkin method is much more robust and accurate
  than the method of characteristics and is recomanded for serious problems.

% ------------------------------------------------
% pb de stationarite discrete Re=1000
% ------------------------------------------------
% test d'arret |du/dt| < eps
% pour Re=1000 et les maillages adaptes 4 & 5,
% c'est periodique autour de 0.1 et ne % tends pas vers zero !
% => comment arreter les calculs autrement que avec n >= iter_max ?
%
% ----------------------------------------------------------------------
% schema d'ordre 3:
% ----------------------------------------------------------------------
%  du/dt(n) = (11/6)u(n) - u(n-1) + (3/2)*u(n-2) - (1/3)*u(n-3)
%  u* = 3*u(n-1) - 3*u(n-2) + u(n-3)
% REFS: BotForPasPeySab-2001
%      O. Botella, M. Y. Forestier, R. Pasquetti, R. Peyret, C. Sabbah
%      Chebyshev methods for the Navier-Stokes equations: algorithms and applications
%      Nonlinear Analysis, Volume 47, Issue 6, August 2001, Pages 4157-4168
% ----------------------------------------------------------------------
% autres comparaisons:
% ----------------------------------------------------------------------
% The reader can compare the result shown on Fig.~\ref{fig-navier-stokes}
% to any available computation on this standard benchmark
% (e.g.~\citealp[p.~526]{choe-kim-kim-kim-2001} for $Re=100$,
%     \citealp[p.~144]{Pir-1988} for $Re=500$).
% http://www.nag.co.uk/simulation/Fastflo/Documents/ToolBox/html/node22.htm
% Re=100-s05f02.gif (manque les recirculations)
% Re=1000-s05f04.gif 
%
% paper-30604.ps.gz : coupes u1 et u2 
%
% http://cs.engr.uky.edu/~jzhang/colleague.html
% Re=10000
% n128c.eps
% n256c.eps
%
% http://aimsciences.org/DCDS-B/Volume1-4/Choi.pdf
% Choi.pdf
% page 32 : figures Re=1 et 100
%
% http://www.csc.fi/elmer/examples/cavity/
% Re=1000
%
% http://www.math.temple.edu/~jxu/cfd.html
% Re=1000, 7500, 10000
% http://calvino.polito.it/~sberrone/Publications/Papers/Report_07_1999.ps.gz
% Re=5000 : p 43
% EF stab + adapt
%
% Hopf Bifurcation of the Unsteady Regularized Driven Cavity Flow, J. Comp. Phys. Vol. 95, 228-245 (1991)
% Author: Jie Shen
% It is found that the flow
% converges to a stationary state
% for Reynolds numbers (Re) up to 10000; for Reynolds numbers larger
% than a critical value $10000<Re1<10500$ and less than another critical
% value $15000<Re2<16000$, the flow becomes periodic in time which
% indicates a Hopf bifurcation; the flow loses time periodicity for
% $Re\ge Re2$.
% Cavity_abs.txt
% Cavity.ps
%
% http://www.fem.gr.jp/fem/fluid/dcf/dcf7.html
% animation : Re=0..1000
%

