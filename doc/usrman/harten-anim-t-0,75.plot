set terminal cairolatex pdf color standalone
set output "harten-anim-t-0,75.tex"

set size square 
set colors classic
set key bottom
set xrange [-1:1]
set yrange [0.5:1.5]
set xtics (-1,0,1)
set ytics (0.5,1,1.5)
set arrow from -1,1 to 1,1 nohead lt 1 lw 0.5 lc 0
set xlabel '[c]{$x$}'
set  label '[l]{$u(t,x)$}' at graph 0.02, 0.95
set  label '[r]{(d) $t=0,75$}' at graph 0.95, 0.95
plot \
  "harten-anim-t-0,75.gdat" \
	t '[r]{exact}' \
	w l lt 1 lw 2 lc 0, \
  "harten-anim-t-0,75-P0-h-200-n-1000.gdat" \
	t '[r]{$P_0$}' \
	w l lt 1 lw 2 lc 1

