% --------------------------------------------------------------
\subsection{The Poisson problem with Dirichlet boundary conditions}
% --------------------------------------------------------------
\label{dirichlet-dg}%
\pbindex{Poisson}%
\cindex{boundary condition!Dirichlet}%

The Poisson problem with
non-homogeneous Dirichlet boundary conditions has been already
introduced in volume~1, section~\ref{dirichlet-nh}, page~\pageref{dirichlet-nh}:

\ \ $(P)$: \emph{find $u$, defined in $\Omega$, such that}
\begin{eqnarray*}
      -\Delta u &=& f \ {\rm in}\ \Omega        
        \\
      u &=& g \ {\rm on}\ \partial \Omega
\end{eqnarray*}
where $f$ and $g$ are given.

The \emph{discontinuous} finite element space is defined by:
\[ 
   X_h = \{ v_h \in L^2(\Omega); v_{h|K}\in P_k, \ \forall K \in \mathcal{T}_h \}
\]
where $k\geq 1$ is the polynomial degree.
\cindex{broken Sobolev space {$H^1(\mathcal{T}_h)$}}%
As elements of $X_h$ do not belongs to $H^1(\Omega)$, due to discontinuities
at inter-elements, we introduce the broken Sobolev space:
\[
   H^1(\mathcal{T}_h) = \{ v \in L^2(\Omega); \ v_{|K}\in H^1(K), \ \forall K\in\mathcal{T}_h \}
\]
such that $X_h\subset H^1(\mathcal{T}_h)$.
We introduce the folowing bilinear form $a_h(.,.)$ and linear for $l_h(.)$,
defined for all $u,v\in H^1(\mathcal{T}_h)$ by
(see e.g.~\citealp[p.~125~and~127]{PieErn-2012}, eqn. (4.12)):
\cindex{form!{$\jump{u}\jump{v}$}}%
\cindex{form!{$\jump{u}\average{\nabla_h v.{\bf n}}$}}%
\begin{eqnarray}
  a_h(u,v)
    &=&
    \int_\Omega
	\nabla_h u .\nabla_h v
	\,{\rm d}x
    +
    \sum_{S \in \mathscr{S}_h}
    \int_S
        \left(
	      \kappa_s
              \, \jump{u} \, \jump{v}
              - 
              \average{\nabla_h u.{\bf n}}
              \, \jump{v}
              -
              \jump{u}
              \, \average{\nabla_h v.{\bf n}}
        \right)
        \, {\rm d}s
	\label{eq-dirichlet-dg-ah}
    \\
  l_h(v)
    &=&
    \int_\Omega
	f\,u 
	\,{\rm d}x
    +
    \int_{\partial\Omega}
        \left(
	    \kappa_s 
	    \, g 
            \, v 
          -
	    g
            \,\nabla_h v.{\bf n}
        \right)
	\,{\rm d}s
	\label{eq-dirichlet-dg-lh}
\end{eqnarray}
The last term involves a sum over $\mathscr{S}_h$, the set
of \emph{all sides} of the mesh $\mathcal{T}_h$,
i.e. the internal sides and the boundary sides.
% TODO: extends the code to this definition:
\cindex{operator!\code{jump}, across sides}%
\cindex{operator!\code{average}, across sides}%
By convenience, the definition of the jump and average are extended
to all boundary sides as $\jump{u}=\average{u}=u$.
\cindex{boundary condition!weakly imposed}%
Note that, as for the previous transport problem,
the Dirichlet boundary condition $u=g$ is weakly imposed
on $\partial\Omega$ via the integrals on the boundary.
Finally, $\kappa_s > 0$ is a stabilization parameter on a side $S$.
The stabilization term associated to $\kappa_s$ is present in order to
achieve coercivity: it penalize interface and boundary jumps.
A common choice is $\kappa_s = \beta\,h_s^{-1}$ where
$\beta > 0$ is a constant and $h_s$ is a local length scale associated to the current side $S$.
One drawnback to this choice is that it requires the end user to
specify the numerical constant $\beta$.
From one hand, if the value of this parameter is not sufficiently large, the form $a_h(.,.)$ is not coercive
and the approximate solution develops instabilities an do not converge \citep{EpsRiv-2007}.
From other hand, if the value of this parameter is too large, 
its affect the overall efficiency of the iterative solver of the linear system:
the spectral condition number of the matrix associated to $a_h(.,.)$ grows linearly with 
this paramater \citep{Cas-2002}.
\cindex{penalty parameter}%
An explicit choice of penalty parameter is proposed \citep{Sha-2005}:
\mbox{$
  \kappa_s = \beta \,\varpi_s
$}
where
\mbox{$
  \beta = (k+1)(k+d)/d
$}
and
\begin{eqnarray*}
  \varpi_s &=&
    \left\{
    \begin{array}{ll}
	\Frac{{\rm meas}(\partial K)}{{\rm meas}(K)}
	& \mbox{ when } S = K \cap \partial \Omega \ \mbox{ is a boundary side}
	\\
	{\rm max}\left(
            \Frac{{\rm meas}(\partial K_0)}{{\rm meas}(K_0)}, \ 
            \Frac{{\rm meas}(\partial K_1)}{{\rm meas}(K_1)}
        \right)
	& \mbox{ when } S = K_0 \cap K_1 \ \mbox{ is an internal side}
    \end{array}
    \right.
\end{eqnarray*}
Note that $\varpi_s$ scales as $h_s^{-1}$.
Now, the computation of the penalty parameter is fully automatic
and the convergence of the method is always guaranted to converge.
Moreover, this choice has been founded to be sharp and it preserves
the optimal efficiency of the iterative solvers.
%
%
Finally, the discrete variational formulation writes:

\ \ $(FV)_h$: \emph{find $u_h\in X_h$ such that}
\begin{eqnarray*}
  a_h(u_h,v_h) &=& l_h(v_h), \ \forall v_h\in X_h
\end{eqnarray*}

The following code implement this problem in the \Rheolef\  environment.

% ---------------------------------------
\myexamplelicense{dirichlet_dg.cc}
% ---------------------------------------

% ------------------------------
\subsubsection*{Comments}
% ------------------------------
The \code{penalty()} pseudo-function implements the computation of $\varpi_s$ in \Rheolef.
The right-hand side $f$ and $g$ are given by \eqref{eq-cosinusprod},
volume~1, page~\pageref{eq-cosinusprod}.
In that case, the exact solution is known.
Running the one-dimensional case writes:
\begin{verbatim}
  make dirichlet_dg
  mkgeo_grid -e 10 > line.geo
  ./dirichlet_dg line P1d | field -
  ./dirichlet_dg line P2d | field -
\end{verbatim}

  \begin{figure}[htb]
    \begin{center}
       \begin{tabular}{cc}
          \includegraphics[height=5.5cm]{dirichlet_dg-1d-elevation.pdf} &
          \includegraphics[height=5.5cm]{dirichlet_dg-1d-elevation2.pdf}
       \end{tabular}
    \end{center}
    \caption{The discontinuous Galerkin method for
	the Poisson problem when $k=1$ and $d=1$.}
    \label{fig-dirichlet-dg-view}
  \end{figure}
Fig.~\ref{fig-dirichlet-dg-view}
plots the one-dimensional solution when $k=1$ for two meshes.
Observe that the jumps at inter-element nodes decreases very fast with mesh refinement
and are no more perceptible on the plots.
Recall that the Dirichlet boundary conditions at $x=0$ and $x=1$ is only
weakly imposed: the corresponding jump at the boundary is also not perceptible.

The two-dimensional case writes:
\begin{verbatim}
  mkgeo_grid -t 10 > square.geo
  ./dirichlet_dg square P1d | field -elevation -
  ./dirichlet_dg square P2d | field -elevation -
\end{verbatim}
and the three-dimensional one
\begin{verbatim}
  mkgeo_grid -T 10 > cube.geo
  ./dirichlet_dg cube P1d | field -
  ./dirichlet_dg cube P2d | field -
\end{verbatim}

% ------------------------------
\subsubsection*{Error analysis}
% ------------------------------
\cindex{convergence!error!versus mesh}%

  \begin{figure}[htb]
    \begin{center}
       \begin{tabular}{cc}
          \includegraphics[height=6.5cm]{dirichlet_dg-cv-l2-2d-t-grid.pdf} &
          \includegraphics[height=6.5cm]{dirichlet_dg-cv-linf-2d-t-grid.pdf} \\
          \includegraphics[height=6.5cm]{dirichlet_dg-cv-h1-2d-t-grid.pdf} &
       \end{tabular}
    \end{center}
    \caption{The discontinuous Galerkin method
	for the Poisson problem: convergence when $d=2$.}
    \label{fig-dirichlet-dg-cv}
  \end{figure}

The space $H^1(\mathcal{T}_h)$
is equiped with the norm $\|.\|_{1,h}$, defined for all
$v\in H^1(\mathcal{T}_h$ by~\citet[p.~128]{PieErn-2012}:
\[
   \|v\|_{1,h}^2
   =
   \| \nabla_h v \|_{0,\Omega}^2
   +
   \sum_{S \in \mathscr{S}_h}
   \int_S
	h_s^{-1} \, \jump{v}^2
        \, {\rm d}s
\]
%The folowing code compute the error in these norms.
% ---------------------------------------
%\myexamplelicense{cosinusprod_error_dg.cc}
% ---------------------------------------
\rawexindex{cosinusprod_error_dg.cc}%
The code \code{cosinusprod_error_dg.cc} compute the error in these norms.
This code it is not listed here but is available in the \Rheolef\  example directory.
The computation of the error writes:
\begin{verbatim}
  make cosinusprod_error_dg
  ./dirichlet_dg square P1d | cosinusprod_error_dg
\end{verbatim}
Fig.~\ref{fig-dirichlet-dg-cv} plots the error $u-u_h$ in
$L^2$, $L^\infty$ and the $\|.\|_{1,h}$ norms.
The $L^2$ and $L^\infty$ error norms
behave as $\mathcal{O}\left(h^{k+1}\right)$ for all $k\geq 0$,
while the $\|.\|_{1,h}$ one behaves as $\mathcal{O}\left(h^{k}\right)$,
which is optimal.

