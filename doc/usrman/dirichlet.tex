%\section{Getting started with \Rheolef}

  For obtaining and installing \Rheolef, see the installation instructions on the \Rheolef\  home page:
  \begin{quote}
	\url{http://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef}
  \end{quote}
  Before to run examples, please check your \Rheolef\  installation with:
\pindexopt{rheolef-config}{--check}%
\begin{verbatim}
  rheolef-config --check
\end{verbatim}
  The present book is available in 
  the documentation directory of the \Rheolef\  distribution.
  This documentation directory is given by the following unix command:
\pindex{rheolef-config}%
\pindexopt{rheolef-config}{--docdir}%
\begin{verbatim}
  rheolef-config --docdir
\end{verbatim}
  All examples presented along the present book
  are also available in the \code{example/} directory 
  of the \Rheolef\  distribution.
  This directory is given by the following unix command:
\cindex{directory of example files}%
\pindexopt{rheolef-config}{--exampledir}%
\begin{verbatim}
  rheolef-config --exampledir
\end{verbatim}
  This command returns you a path, something like \code{/usr/share/doc/rheolef-doc/examples}
  and you should make a copy of these files:
\begin{verbatim}
  cp -a /usr/share/doc/rheolef-doc/examples .
  cd examples
\end{verbatim}

% --------------------------------------
\subsection{Problem statement}
% --------------------------------------
\label{sec-dirichlet}%
\pbindex{Poisson}%
\cindex{boundary condition!Dirichlet}%
  Let us consider the classical Poisson problem
  with homogeneous Dirichlet boundary conditions
  in a domain bounded $\Omega \subset \mathbb{R}^d$,
  $d=1,2,3$:

\cindex{operator!Laplace}%
  {\em (P): find $u$, defined in $\Omega$, such that:}
  \begin{eqnarray}
      -\Delta u &=& 1 \ {\rm in}\ \Omega	
	\label{eq-dirichlet-1}
	\\
      u &=& 0 \ {\rm on}\ \partial \Omega
	\label{eq-dirichlet-2}
  \end{eqnarray}
  where $\Delta$ denotes the Laplace operator.
  The variational formulation of this problem expresses
  (see appendix~\ref{sec-green-scalar} for details):
  
  {\em (VF): find $u\in H^1_0(\Omega)$ such that:}
  \begin{eqnarray}
      a(u,v) = l(v),
      \ \forall v \in H^1_0(\Omega)
		\label{eq-dirichlet-fv}
  \end{eqnarray}
\cindex{form!energy}%
\cindex{form!{$\nabla u.\nabla v$}}%
  where the bilinear form $a(.,.)$ and the linear form $l(.)$ are defined by
  \begin{eqnarray*}
          a(u,v) &=& \int_\Omega \nabla u . \nabla v \, {\rm d}x,
	  \ \ \forall u,v \in H^1_0(\Omega)
	  \\
          l(v) &=& \int_\Omega v \, {\rm d}x,
	  \ \ \forall v \in L^2(\Omega)
  \end{eqnarray*}
  The bilinear form $a(.,.)$ defines a scalar product
  in $H^1_0(\Omega)$ and is related to the {\em energy} form.
  This form is associated to the $-\Delta$ operator.
% The $m(.,.)$ is here the classical scalar product on $L^2(\Omega)$,
% and is related to the {\em mass} form.
  
% --------------------------------------
\subsection{Approximation}
% --------------------------------------
\cindex{approximation} %
  Let us introduce a mesh ${\cal T}_h$ of $\Omega$
  and the finite dimensional space $X_h$ of continuous
  piecewise polynomial functions.
  $$
      X_h = \{ v \in H^1(\Omega); \ 
          v_{/K} \in P_k, \ 
	  \forall K \in {\cal T}_h \}
  $$
  where $k=1$ or $2$.
  Let $V_h=X_h \cap H^1_0(\Omega)$ be the functions of $X_h$ that
  vanishes on the boundary of $\Omega$.
  The approximate problem expresses:

  {\em $(VF)_h$: find $u_h\in V_h$ such that:}
  $$
      a(u_h,v_h) = l(v_h),
      \ \forall v_h \in V_h
  $$
  By developing $u_h$ on a basis of $V_h$, this 
  problem reduces to a linear system.
  The following C++ code
  implement this problem in the \Rheolef\  environment.

% --------------------------------------
\myexamplelicense{dirichlet.cc}
% --------------------------------------
 
% --------------------------------------
\subsection{Comments}
% --------------------------------------
\cindex{mesh}%
\clindex{geo}%
\clindex{space}%
\clindex{form}%
\clindex{field}%
  This code applies for both one, two or three
  dimensional meshes and
  for both piecewise linear or quadratic
  finite element approximations.
  Four major classes are involved, namely: 
  {\color{rheoleftypcolor} \code{geo}},
  {\color{rheoleftypcolor} \code{space}},
  {\color{rheoleftypcolor} \code{form}} and 
  {\color{rheoleftypcolor} \code{field}}.

  Let us now comment the code, line by line.
\begin{lstlisting}[numbers=none,frame=none]
  #include "rheolef.h"
\end{lstlisting}
  The first line includes the \Rheolef\  header file \file{rheolef.h}.
\cindex{namespace!std}%
\cindex{namespace!rheolef}%
\begin{lstlisting}[numbers=none,frame=none]
  using namespace rheolef;
  using namespace std;
\end{lstlisting}
  By default, in order to avoid possible name conflicts when using another library,
  all class and function names are prefixed by \code{rheolef::}, as in~\code{rheolef::space}.
  This feature is called the name space.
  Here, since there is no possible conflict, and in order to simplify the syntax, we drop all the
  \code{rheolef::} prefixes, and do the same with the standard \code{c++} library classes and
  variables, that are also prefixed by \code{std::}.
\cindex{argc, argv, command line arguments}%
\begin{lstlisting}[numbers=none,frame=none]
  int main(int argc, char**argv) {
\end{lstlisting}
  The entry function of the program is always called \code{main} and accepts arguments
  from the unix command line: \code{argc} is the counter of command line arguments
  and \code{argv} is the table of values. The character string \code{argv[0]}
  is the program name and \code{argv[i]}, for $i=1$ to \code{argc-1}, are the
  additional command line arguments.
\begin{lstlisting}[numbers=none,frame=none]
  environment rheolef (argc, argv);
\end{lstlisting}
\pindexopt{library}{boost}%
\pindexopt{library}{MPI, {\rm message passing interface}}%
\cindex{distributed computation}%
\cindex{parallel computation}%
  These two command line parameters are immediately furnished to the 
  distributed environment initializer of the \code{boost::mpi} library, that is
  a \code{c++} library based on the usual message passing interface ({\sc mpi}) library.
  Note that this initialization is required, even when you run with only one processor.
\begin{lstlisting}[numbers=none,frame=none]
  geo omega (argv[1]);
\end{lstlisting}
  This command get the first unix command-line argument {\tt argv[1]}
  as a mesh file name and store the corresponding mesh in the variable {\tt omega}.
\begin{lstlisting}[numbers=none,frame=none]
  space Xh (omega, argv[2]);
\end{lstlisting}
  Build the finite element space {\tt Xh} contains all the piecewise polynomial
  continuous functions. The polynomial type
  is the second command-line arguments
\apindex{P1}%
\apindex{P2}%
\apindex{Pk}%
\apindex{high-order}%
  {\tt argv[2]}, and could be either \code{P1}, \code{P2} or any \code{P}$k$, where $k\geq 1$.

\begin{lstlisting}[numbers=none,frame=none]
  Xh.block ("boundary");
\end{lstlisting}
  The homogeneous Dirichlet conditions are declared on the boundary.
\findex{integrate}%
\begin{lstlisting}[numbers=none,frame=none]
  trial u (Xh); test v (Xh);
  form a = integrate (dot(grad(u),grad(v)));
\end{lstlisting}
  The bilinear form $a(.,.)$ is the energy form: it is defined for all
  functions $u$ and $v$ in $X_h$.
\begin{lstlisting}[numbers=none,frame=none]
  field lh = integrate (v);
\end{lstlisting}
  The linear form $lh(.)$ is associated to the constant right-hand side $f=1$ of the problem.
  It is defined for all $v$ in $X_h$.
\begin{lstlisting}[numbers=none,frame=none]
  field uh (Xh);
\end{lstlisting}
  The field {\tt uh} contains the the degrees of freedom.
\begin{lstlisting}[numbers=none,frame=none]
  uh ["boundary"] = 0;
\end{lstlisting}
  Some degrees of freedom are prescribed as zero on the boundary.
\clindex{problem}%
\begin{lstlisting}[numbers=none,frame=none]
  problem p (a);
  p.solve (lh, uh);
\end{lstlisting}
Finally, the problem related to the bilinear form~$a$
and the right-hand-side~{\tt lh} is solved
and~{\tt uh} contains the solution.
The field is printed to standard output:
\begin{lstlisting}[numbers=none,frame=none]
  dout << uh;
\end{lstlisting}
  The {\color{rheolefvarcolor}\tt dout} stream is a specific variable defined in the \Rheolef\  library:
  it is a distributed and parallel extension
  of the usual {\tt cout} stream in {\tt C++}

\cindex{Lagrange!node}%
Let us study with more details the linear system.
Let $(\varphi_i)_{0\leq i< {\rm dim}(X_h)}$ be the basis 
  of $X_h$ associated to the Lagrange nodes, e.g. the vertices
  of the mesh for the $P_1$ approximation and the vertices and the
  middle of the edges for the $P_2$ approximation.
  The approximate solution $u_h$ expresses as a linear combination
  of the continuous piecewise polynomial functions $(\varphi_i)$:
  $$
        u_h = \sum_{i} u_i \varphi_i
  $$
\cindex{unknown and blocked components}%
\label{field-u-b}%
  Thus, the field $u_h$ is completely represented by its coefficients $(u_i)$.
  The coefficients $(u_i)$ of this combination are grouped into
  to sets: some have zero values, from the boundary condition
  and are related to {\em blocked} coefficients,
  and some others are {\em unknown}.
  Blocked coefficients are stored into the \code{uh.b} array
  while unknown one are stored into \code{uh.u}.
  Thus, the restriction of the bilinear form $a(.,.)$ to $X_h\times X_h$ 
  can be conveniently represented by a block-matrix structure:
\cindex{matrix!block structure}%
  $$
        a(u_h, v_h) = 
	\left ( \begin{array}{cc}
	    {\tt vh.u} & {\tt vh.b}
	\end{array} \right)
	\left ( \begin{array}{cc}
	    {\tt a.uu} & {\tt a.ub} \\
	    {\tt a.bu} & {\tt a.bb}
	\end{array} \right)
	\left ( \begin{array}{c}
	    {\tt uh.u} \\
	    {\tt uh.b}
	\end{array} \right)
  $$
  This representation also applies for the
  linear form $l(.)$:
  $$
        l(v_h) = 
	\left ( \begin{array}{cc}
	    {\tt vh.u} & {\tt vh.b}
	\end{array} \right)
	\left ( \begin{array}{c}
	    {\tt lh.u} \\
	    {\tt lh.b}
	\end{array} \right)
  $$
  Thus, the problem $(VF)_h$ writes now:
  $$
	\left ( \begin{array}{cc}
	    {\tt vh.u} & {\tt vh.b}
	\end{array} \right)
	\left ( \begin{array}{cc}
	    {\tt a.uu} & {\tt a.ub} \\
	    {\tt a.bu} & {\tt a.bb}
	\end{array} \right)
	\left ( \begin{array}{c}
	    {\tt uh.u} \\
	    {\tt uh.b}
	\end{array} \right)
     =
	\left ( \begin{array}{cc}
	    {\tt vh.u} & {\tt vh.b}
	\end{array} \right)
	\left ( \begin{array}{c}
	    {\tt lh.u} \\
	    {\tt lh.b}
	\end{array} \right)
  $$
  for any {\tt vh.u} and where {\tt vh.b} = 0.
  After expansion, the problem reduces to 
  {\em find} {\tt uh.u} {\em such that}:
  $$
    {\tt a.uu*uh.u} = {\tt l.u - a.ub*uh.b}
  $$
\cindex{matrix!factorization!Choleski}%
\clindex{pbl em}%
\clindex{solver}%
The resolution of this linear system for the {\tt a.uu} matrix is
then performed via the \code{solver} class:
this call is performed by the \code{problem} class.
\clindex{reference manual}%
\clindex{man}%
For more details, see the Rheolef reference manual related to  
the \code{problem} and \code{solver} classes,
available on the web site and via the unix command:
\begin{verbatim}
  man problem
  man solver
\end{verbatim}

%% A preliminary step build the $LDL^T$ factorization:
%% \begin{lstlisting}[numbers=none,frame=none]
%%   solver sa (a.uu());
%% \end{lstlisting}
%%   Then, the second step solves the {\em unknown part}:
%% \begin{lstlisting}[numbers=none,frame=none]
%%   uh.set_u() = sa.solve (lh.u() - a.ub()*uh.b());
%% \end{lstlisting}
%% \cindex{method!conjugate gradient algorithm}%
%% \cindex{preconditioner!Choleski incomplete factorization}%
%%   When $d>3$, a faster iterative strategy is automatically preferred
%%   by the {\tt solver} class for solving
%%   the linear system: in that case, the preliminary step build 
%%   an incomplete Choleski factorization preconditioner,
%%   while the second step runs an iterative method:
%%   the preconditioned conjugate gradient algorithm.
% --------------------------------------
\subsection{How to compile the code}
% --------------------------------------
\cindex{compilation}%
\cindex{Makefile}%
\pindex{make}%
\label{makefile}%
  First, create a file~\reffile{Makefile} as follow:
  \label{Makefile}%
  \rawexindex{Makefile}%
  \begin{quote}
    \verbatiminput{Makefile.demo}
  \end{quote}
  Then, enter:
\begin{verbatim}
  make dirichlet
\end{verbatim}
  Now, your program, linked with \Rheolef,
  is ready to run on a mesh.

% --------------------------------------
\subsection{How to run the program}
% --------------------------------------
\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[height=6.0cm]{dirichlet-2d-P1-paraview.png} &
      \includegraphics[height=6.0cm]{dirichlet-2d-P1-gnuplot.png} 
    \end{tabular}
  \end{center}
  \caption{Solution of the model problem for $d=2$ with the $P_1$ element: visualization 
	(left)  with \code{paraview} as filled isocontours~;
	(right) with \code{gnuplot} as unfilled isocontours.
  } 
  \label{fig-dirichlet-2d}
\end{figure}
\cindex{geometry!square}%
\pindex{geo}%
\pindex{mkgeo_grid}%
\pindexopt{mkgeo_grid}{-t}%
\fiindex{\filesuffix{.geo} mesh}%
\pindexopt{visualization}{mesh}%
  Enter the commands:
\begin{verbatim}
  mkgeo_grid -t 10 > square.geo
  geo square.geo
\end{verbatim}
  The first command generates a simple 10x10 bidimensional mesh
  of $\Omega=]0,1[^2$
  and stores it in the file \code{square.geo}. 
  The second command shows the mesh.
\pindex{paraview}%
  It uses \code{paraview} visualization program by default.

\pindex{field}%
\fiindex{\filesuffix{.field} field}%
  The next commands perform the computation and visualization:
\begin{verbatim}
  ./dirichlet square.geo P1 > square.field
  field square.field
\end{verbatim}
The result is hown on Fig.~\ref{fig-dirichlet-2d}.
\pindex{paraview}%
By default, the visualization appears in a \code{paraview} window.
\pindex{gnuplot}%
If you are in trouble with this software, you can switch to
the simpler \code{gnuplot} visualization mode:
\pindexopt{field}{-gnuplot}%
\begin{verbatim}
  field square.field -gnuplot
\end{verbatim}
% ----------------------------------------------------------------
\subsection{Advanced and stereo visualization}
% ----------------------------------------------------------------
  \begin{figure}[htb]
%    \begin{center}
     \mbox{}\hspace{-1cm}
       \begin{tabular}{lll}
          \includegraphics[height=6.5cm]{dirichlet-2d-bw-gnuplot.pdf} &
          \includegraphics[height=6.5cm]{dirichlet-2d-elevation.png} &
          \stereoglasses
       \end{tabular}
%    \end{center}
     \caption{Alternative representations
	of the solution of the model problem ($d=2$ and the
        $P_1$ element):
       (left) in black-and-white;
       (right) in elevation and stereoscopic anaglyph mode.}
     \label{fig-dirichlet-2d-alt}
  \end{figure}
  We could explore some graphic rendering modes
  (see Fig.~\ref{fig-dirichlet-2d-alt}):
\pindex{paraview}%
\pindexopt{field}{-bw}%
\pindexopt{field}{-gray}%
\pindexopt{field}{-stereo}%
\pindexopt{field}{-nofill}%
\pindexopt{field}{-elevation}%
\cindex{visualization!elevation view}%
\begin{verbatim}
  field square.field -bw
  field square.field -gray
  field square.field -elevation
  field square.field -elevation -gray
  field square.field -elevation -nofill -stereo
\end{verbatim}
\cindex{visualization!stereoscopic anaglyph}%
  The last command shows the solution in elevation
  and in stereoscopic anaglyph
  mode (see Fig.~\ref{fig-dirichlet-3d}, left).
  The anaglyph mode requires red-cyan glasses:
  red for the left eye and cyan for the right one,
  as shown on Fig.~\ref{fig-anaglyph-glasses}.
\begin{figure}[htb]
     \begin{center}
          \includegraphics{red-cyan-glasses.png}
     \end{center}
     \caption{Red-cyan anaglyph glasses for the stereoscopic visualization.}
     \label{fig-anaglyph-glasses}
\end{figure}
In the book, stereo figures are indicated by the \mbox{\stereoglasses} logo
in the right margin.
See~\url{http://en.wikipedia.org/wiki/Anaglyph_image}
for more and
% broken: \url{http://www.alpes-stereo.com/lunettes.html} 
\url{https://www.ecolofrance.com/56-lunettes-anaglyphe-rouge-cyan-carton-pas-cher} 
for how to find anaglyph red-cyan glasses.
For simplicity, it would perhaps prefer to switch to 
the \code{gnuplot} render:
\pindex{gnuplot}%
\pindexopt{field}{-gnuplot}%
\begin{verbatim}
  field square.field -gnuplot
  field square.field -gnuplot -bw
  field square.field -gnuplot -gray
\end{verbatim}
  Please, consult the \Rheolef\  reference manual page for more 
  on the unix commands \code{field}, \code{geo} and \code{mkgeo_grid}.
  The manual is available both on the web site:
\begin{verbatim}
  firefox https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef
\end{verbatim}
and as individual unix man pages:
\index{reference manual}
\pindex{man}
\begin{verbatim}
  man mkgeo_grid
  man geo
  man field
\end{verbatim}
See also
\begin{verbatim}
  man rheolef
\end{verbatim}
The complete list of \Rheolef\  man page is obtained by:
\begin{verbatim}
  man -k rheolef
\end{verbatim}
% ----------------------------------------------------------------
\subsection{High-order finite element methods}
% ----------------------------------------------------------------
\apindex{P2}%
\apindex{Pk}%
\apindex{high-order}%
  Turning to the \code{P2} or \code{P3} approximations simply writes:
\begin{verbatim}
  ./dirichlet square.geo P2 > square-P2.field
  field square-P2.field
\end{verbatim}
  You can replace the \code{P2} command-line argument by any \code{P}$k$, where $k\geq 1$.
\cindex{geometry!line}%
  Now, let us consider a mono-dimensional problem
  $\Omega=]0,1[$: 
\pindexopt{field}{-}
\pindexopt{mkgeo_grid}{-e}%
\begin{verbatim}
  mkgeo_grid -e 10 > line.geo
  geo line.geo
  ./dirichlet line.geo P1 | field -
\end{verbatim}
  The first command generates a subdivision containing ten edge elements.
\pindex{gnuplot}%
  The last two lines show the mesh and the solution
  via \code{gnuplot} visualization, respectively.

  Conversely, the \code{P2} case writes:
\begin{verbatim}
  ./dirichlet line.geo P2 | field -
\end{verbatim}
% ----------------------------------------------------------------
\subsection{Tridimensional computations}
% ----------------------------------------------------------------
Let us consider a three-dimensional problem $\Omega=]0,1[^3$.
First, let us generate a mesh:
\cindex{geometry!cube}%
\pindexopt{mkgeo_grid}{-T}%
\pindexopt{geo}{-fill}%
\pindexopt{geo}{-cut}%
\pindexopt{geo}{-shrink}%
\pindexopt{geo}{-full}%
\pindexopt{geo}{-stereo}%
\begin{verbatim}
  mkgeo_grid -T 10 > cube.geo
  geo cube.geo 
  geo cube.geo -fill
  geo cube.geo -cut
  geo cube.geo -shrink
  geo cube.geo -shrink -cut
\end{verbatim}
\pindex{paraview}%
The 3D visualization bases on the \code{paraview} render.
These commands present some cuts (\code{-cut})
inside the internal mesh structure:
a simple click on the central arrow draws the cut plane normal vector
or its origin, while the red square allows a translation.
The following command draws the mesh with all internal edges (\code{-full}),
together with the stereoscopic anaglyph (\code{-stereo}):
\begin{verbatim}
  geo cube.geo -stereo -full
\end{verbatim}
  \begin{figure}[htb]
     \mbox{}\hspace{-1cm}
     \begin{tabular}{ccc}
	 \includegraphics[scale=0.30]{cube-3d-stereo-fig.png} &
         \includegraphics[scale=0.35]{dirichlet-3d-mayavi-mix-fig.png}  &
          \stereoglasses
     \end{tabular}
     \caption{Solution of the model problem for $d=3$ and the $P_1$ element~:
     	(left) mesh;
	(right) isovalue, cut planes and stereo anaglyph renderings.}
     \label{fig-dirichlet-3d}
  \end{figure}
Then, we perform the computation and the visualization:
\begin{verbatim}
  ./dirichlet cube.geo P1 > cube.field
  field cube.field
\end{verbatim}
\pindex{paraview}%
  The visualization presents an isosurface.
  Also here, you can interact with the cutting plane.
  On the \code{Properties} of the \code{paraview} window,
  select \code{Contour}, change the value of the isosurface
  and click on the green \code{Apply} button.
  Finally exit from the visualization and explore
  the stereoscopic anaglyph mode
  (see Fig.~\ref{fig-dirichlet-3d}, right):
\pindexopt{field}{-stereo}%
\begin{verbatim}
  field cube.field -stereo
\end{verbatim}
It is also possible to add a second isosurface (\code{Contour})
or a cutting plane (\code{Slice}) to this scene by using the corresponding 
\code{Properties} menu.
\pindexopt{field}{-volume}%
Finally, the following command, with the \code{-volume} option,
allows a 3D color light volume graphical rendering:
\begin{verbatim}
  field cube.field -volume
\end{verbatim}
After this exploration of the 3D visualization capacities of our environment,
let us go back to the Dirichlet problem and
perform the \code{P2} approximation:
\begin{verbatim}
  ./dirichlet cube.geo P2 | field -
\end{verbatim}
% ----------------------------------------------------------------
\subsection{Quadrangles, prisms and hexahedra}
% ----------------------------------------------------------------
Quadrangles and hexahedra are also supported in meshes:
\pindexopt{mkgeo_grid}{-q}%
\pindexopt{mkgeo_grid}{-H}%
\begin{verbatim}
  mkgeo_grid -q 10 > square.geo
  geo square.geo
  mkgeo_grid -H 10 > cube.geo
  geo cube.geo
\end{verbatim}
Note also that the one-dimensional exact solution
expresses explicitly:
  $$
 	u(x) = \Frac{x(1-x)}{2}
  $$
In the two-and three dimensional cases, 
an explicit expression of the solution,
based on Fourier expansion, is also known
(see e.g. \citealp{SarRoq-2001}, annex~B, page~5411).

% --------------------------------------
\subsection{Direct versus iterative solvers}
% --------------------------------------
\label{sec-solvers}%
  \begin{figure}[htb]
     \begin{center}
       \begin{tabular}{rr}
          \includegraphics[height=5.5cm]{timing_cmp_direct_iter_2d.pdf} &
          \includegraphics[height=5.5cm]{timing_cmp_direct_iter_3d.pdf}
       \end{tabular}
     \end{center}
     \caption{Compared performance between direct and iterative solvers:
	(left) $d=2$;
	(right) $d=3$.}
     \label{fig-solver-P1}
  \end{figure}
In order to measure the performances of the solver, the \code{dirichlet.cc}
(page~\pageref{dirichlet.cc}) has been modified as:
\findex{dis_wall_time}%
\clindex{solver}%
\clindex{solver_option}%
\begin{lstlisting}[numbers=none,frame=none]
  double t0 = dis_wall_time();
  solver_option sopt;
  sopt.iterative = false; // or true
  sopt.tol       = 1-5;   // when iterative
  problem p (a, sopt);
  double t_factorize = dis_wall_time() - t0;
  p.solve (lh, uh);
  double t_solve = dis_wall_time() - t0 - t_factorize;
  derr << "time " << t_factorize << " " << t_solve << endl;
\end{lstlisting}
The \code{dis_wall_time} function returns the wall-clock time in seconds,
while the \code{solver_option} \code{sopt}
enables to choose between a direct or an iterative solver
method: by default \Rheolef\  selects a direct method when~$d\leq 2$ and
an iterative one when~$d=3$. 
For a 3D mesh, the compilation and run writes:
\begin{verbatim}
  make dirichlet
  mkgeo_grid -T 60 > cube-60.geo
  ./dirichlet cube-60.geo P1 > cube-60.field
\end{verbatim}
Fig.~\ref{fig-solver-P1} plots the performances of the direct and iterative solvers
used in \Rheolef. The computing time $T(n)$ is represented versus
size $n$ of the linear system, says $Ax=b$.
Note that for a {\tt square-$k$.geo} or {\tt cube-$k$.geo} mesh,
the size of the linear system is~$n=(k-1)^d$.
For the direct method, two times are represented:
first, the time spend to \emph{factorize} $A=LDL^T$,
where $L$ is lower triangular and $D$ is diagonal,
and second, the time used to \emph{solve} $LDL^T=x$
(in three steps: solving $Lz=b$, then $Dy=z$ and finally $L^Tx=y$).
For the iterative method, the conjugate gradient algorithm is considered,
without building any preconditioner, so there is nothing to initialize,
and only one time is represented. The tolerance on the residual term is set to $10^{-5}$.

In the bidimensional case, the iterative solver presents asymptotically,
for large $n$, a computing time similar to the factorization time of the 
direct solver, roughly $\mathcal{O}(n^{3/2})$
while the time to solve by the direct method is dramatically lower,
roughly $\mathcal{O}(n)$. 
As the factorization can be done one time for all, the direct method
is advantageous most of the time.

In the three dimensional case, the situation is different.
The factorization time is very time consuming
roughly $\mathcal{O}(n^{2})$, while the 
time to solve for both direct and iterative methods
behave as $\mathcal{O}(n^{4/3})$.
Thus, the iterative method is clearly advantageous for three-dimensionnal problems.
Future works will improve the iterative approach by building preconditioners.

The asymptotic behaviors of direct methods strongly depends upon the ordering strategy
used for the factorization.
\pindexopt{library}{scotch, {\rm mesh partition library}}%
\pindexopt{library}{mumps, {\rm linear system direct solver}}%
For the direct solver, \Rheolef\  was configured with the
\code{mumps}~\citep{AmeDufExcKos-2001,AmeGueExcPra-2006} library
and \code{mumps} was configured with the parallel \code{scotch}~\citep{Pel-2010-mpi} ordering library.
For a regular grid and in the bidimensional case,
there exists a specific ordering called
nested disection~\citep{HofMarRos-1973,Geo-1973}
that minimize the fillin of the sparse matrix during the factorization.
For three-dimensional case
this ordering is called nested multi-section~\citep{AshLiu-1998}.
Asymptotic computing time for these regular grid are then explicity known
versus the grid size $n$:
\[
  \begin{array}{|c||l|l||l|} \hline
        d & \mbox{direct/factorize} & \mbox{direct/solve}& \mbox{iterative} \\ \hline
        1 & n                & n           & n^2      \phantom{\Frac{1}{2}} \\ \hline
        2 & n^{3/2}          & n\,\log\,n  & n^{3/2}  \phantom{\Frac{1}{2}} \\ \hline
        3 & n^2              & n^{4/3}     & n^{4/3}  \phantom{\Frac{1}{2}} \\ \hline
  \end{array}
\]
The last column gives the asymptotic computing time
for the conjugate gradient on a general mesh~\citep{Sar-2013-cours-cxx}.
Remark that these theoretical results are consistent with
numerical experiments presented on ~Fig.~\ref{fig-solver-P1}.
In conclusion, the best strategy is to choose
a direct method when~$d\leq 2$ and
an iterative one when~$d=3$:
this is the default behavior with \Rheolef.
% --------------------------------------
\subsection{Distributed and parallel runs}
% --------------------------------------
  \begin{figure}[htb]
     \begin{center}
       \begin{tabular}{rr}
          \includegraphics[height=5.5cm]{speedup-2d-assembly.pdf} &
          \includegraphics[height=5.5cm]{speedup-3d-assembly.pdf} \\
          \includegraphics[height=5.5cm]{speedup-2d-solve-direct.pdf} &
          \includegraphics[height=5.5cm]{speedup-3d-solve-direct.pdf} \\
          \includegraphics[height=5.5cm]{speedup-2d-solve-iter.pdf} &
          \includegraphics[height=5.5cm]{speedup-3d-solve-iter.pdf}
       \end{tabular}
     \end{center}
     \caption{Distributed and massively parallel resolution of the model problem with $P_1$ element:
	speedup $S(p)$ versus the number of processors $p$ during :
	(left-right) for $d=2$ and $3$, respectively ;
	(top) the assembly phase ;
	(center-bottom) the solve phase, direct and iterative solvers, respectively.
     }
     \label{fig-speedup-P1}
  \end{figure}
\pindex{mpirun}%
\cindex{distributed computation}%
\cindex{parallel computation}%
For large meshes, a computation in a distributed and parallel environment 
is profitable:
\begin{verbatim}
  mpirun -np  8 ./dirichlet cube-60.geo P1 > cube-60.field
  mpirun -np 16 ./dirichlet cube-60.geo P1 > cube-60.field
\end{verbatim}
\cindex{speedup}%
The computing time~$T(n,p)$ depends now upon the linear system size~$n$
and the number of processes~$p$.
For a fixed size~$n$, the speedup $S(p)$ when using $p$ processors 
is defined by the ratio of the time required by a sequential computation with
the time used by a parallel one: $S(p)=T(n,1)/T(n,p)$.
The speedup is presented on Fig~\ref{fig-speedup-P1}
for the two phases of the computation:
the assembly phase and the solve one,
and for $d=2$ (direct solver) and $3$ (iterative solver).
The ideal speedup $S(p)=p$ and the null speedup $S(p)=1$ are represented by dotted lines.
Observe on Fig~\ref{fig-speedup-P1} that for too small meshes, using too much processes
is not profitable, as more time is spend by communications rather by computations,
especially for the solve phase.
Conversely, when the mesh size increases, using more processes
leads to a remarkable speedup for both $d=2$ and $3$.
The largest mesh used here contains about three millions of elements.
The speedup behavior is roughly linear up to a critical number of processor denotes as $p_c$.
Then, there is a transition to a plateau (the Amdahl's law), where communications dominate.
Note that $p_c$ increases with the mesh size: larger problems lead to a higher speedup.
Also $p_c$ increases also with the efficiency of communications.

Present computation times are measured on a BullX DLC supercomputer (Bull Newsca)
composed of nodes having two intel sandy-bridge processors 
and connected to a FDR infiniband non-blocking low latency network.
The assembly phase corresponds to \code{dirichlet.cc}
(page~\pageref{dirichlet.cc}) line~7 to~13 and
the solve phase to lines~14 and~15.
%\footnote{%
%	Input and output are poorly paralleliazed yet and the
%	corresponding speedup is not presented here. 
%	Future version of \Rheolef\  will consider \code{mpi\_io}.
%}.
