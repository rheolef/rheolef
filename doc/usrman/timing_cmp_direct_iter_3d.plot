set terminal cairolatex pdf color standalone
set output "timing_cmp_direct_iter_3d.tex"

d=3
am=2.0
as=1.33
ai=1.33
#fm(n) = am*n+bm; fit fm(x) 'timing_cmp_direct_iter_3d.gdat' u (log10(($1-1)**d)):(log10($3)) via am,bm
#fs(n) = as*n+bs; fit fs(x) 'timing_cmp_direct_iter_3d.gdat' u (log10(($1-1)**d)):(log10($4)) via as,bs
#fi(n) = ai*n+bi; fit fi(x) 'timing_cmp_direct_iter_3d.gdat' u (log10(($1-1)**d)):(log10($2)) via ai,bi 
# also umfpack and cholmod, but less efficient
#fu(n) = au*n+bu; fit fu(x) 'timing_cmp_direct_iter_3d.gdat' u (log10(($1-1)**d)):(log10($5)) via au,bu
#fc(n) = ac*n+bc; fit fc(x) 'timing_cmp_direct_iter_3d.gdat' u (log10(($1-1)**d)):(log10($6)) via ac,bc

set size square
set colors classic
set key bottom
set log xy
set xrange [5e2:5e6]
set yrange [1e-3:1e3]
graph_ratio = 4.0/6.0

set xlabel '[c]{\large $n$}'
set  label '[l]{\large $T(n)$}' at graph 0.08,0.92
set  label '[l]{$d=3$}'         at graph 0.08,0.85

set xtics (\
        "[c]{$10^{3}$}" 1e3, \
        "[c]{$10^{4}$}" 1e4, \
        "[c]{$10^{5}$}" 1e5, \
        "[c]{$10^{6}$}" 1e6, \
        "[c]{$10^{7}$}" 1e7)
set ytics (\
        "[r]{$10^{-3}$}" 1e-3, \
        "[r]{$10^{0}$}"  1e-0, \
        "[r]{$10^{3}$}"  1e+3)

# triangle a droite
slope_A = graph_ratio*am
xA =  0.55
yA =  0.80
dxA = 0.10
dyA = dxA*slope_A
set label sprintf("[l]{\\scriptsize $%.3g$}",am) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead lc 0
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead lc 0
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead lc 0

# triangle a droite
slope_B = graph_ratio*as
xB =  0.55
yB =  0.38
dxB = 0.10
dyB = dxB*slope_B
set label sprintf("[l]{\\scriptsize $%.3g$}",as) at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead lc 0
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead lc 0
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead lc 0

plot \
"timing_cmp_direct_iter_3d.gdat" \
  u (($1-1)**d):3 t 'direct/factorize'  \
  w lp lt 1 lc 1, \
"timing_cmp_direct_iter_3d.gdat" \
  u (($1-1)**d):4 t 'direct/solve'      \
  w lp lt 1 lc rgb '#008800', \
'timing_cmp_direct_iter_3d.gdat' \
  u (($1-1)**d):2 t 'iterative/solve'   \
  w lp lt 1 lc 3

#"timing_cmp_direct_iter_3d.gdat" u (($1-1)**d):5 t sprintf('umfpack=%g',au)  w lp lc 4 lt 1,\
#10**fu(log10(x)) notitle  w l lc 4 lt 1 lw 2, \
#"timing_cmp_direct_iter_3d.gdat" u (($1-1)**d):6 t sprintf('cholmod=%g',ac)  w lp lc 5 lt 1, \
#10**fc(log10(x)) notitle  w l lc 5 lt 1 lw 2

#pause -1 "<retour>"
