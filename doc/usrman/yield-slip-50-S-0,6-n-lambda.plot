set terminal cairolatex pdf color standalone
set output 'yield-slip-50-S-0,6-n-lambda.tex'

set size square
set colors classic
set key at graph 0.60, 0.29
set xrange [0:1]
set yrange [-0.1:1]
set xtics 0.5
set ytics (0,0.6,1)
set arrow from 0,0.6 to 1,0.6 nohead lc 0 lw 1 dt 3
set xlabel '[c]{\large $x_0$}'
set  label '[l]{\large $\lambda(x_0,1)=-{\displaystyle \frac{\partial u}{\partial x_1}}(x_0,1)$}' at graph 0.03,0.90

# filters only for the {y==1} top domain
lambda(x,y,value) = (y == 1) ? value : 0/0

plot \
'yield-slip-50-n-1,5-S-0,6-lambda.gdat' u 1:(lambda($1,$2,$3)) t '[r]{$n=1.5$}' w l lc 1 dt 1 lw 3, \
'yield-slip-50-n-1-S-0,6-lambda.gdat'   u 1:(lambda($1,$2,$3)) t '[r]{$n=1.0$}' w l lc rgb "#008800" dt 1 lw 3, \
'yield-slip-50-n-0,5-S-0,6-lambda.gdat' u 1:(lambda($1,$2,$3)) t '[r]{$n=0.5$}' w l lc 3 dt 1 lw 3

#pause -1 "<retour>"
