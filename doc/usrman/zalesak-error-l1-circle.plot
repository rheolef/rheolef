set terminal cairolatex pdf color standalone
set output "zalesak-error-l1-circle.tex"

set size square
set log xy
set key bottom
set xlabel '[c]{\large $h$}'
set  label '[l]{$E_h^{(n_{max})}=\mathcal{O}(h^\alpha)$}' at graph 0.03,0.93
set  label '[l]{$\alpha=\frac{k+3}{2}$}'                  at graph 0.23,0.80
set xrange [1e-2:1e-1]
set yrange [1e-6:1e-1]
graph_ratio_xy = 1.0/5.0
set xtics (\
	'[c]{$10^{-3}$}' 1e-3, \
	'[c]{$10^{-2}$}' 1e-2, \
	'[c]{$10^{-1}$}' 1e-1, \
	'[c]{$1$}'       1, \
	'[c]{$10$}'      10)
set ytics (\
        '[r]{$10^{-6}$}' 1e-6, \
        '[r]{$10^{-5}$}' 1e-5, \
	'[r]{$10^{-4}$}' 1e-4, \
	'[r]{$10^{-3}$}' 1e-3, \
	'[r]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-1}$}' 1e-1, \
	'[r]{$1$}'       1, \
	'[r]{$10$}'      1e+1)

# triangle a droite
slope_A = graph_ratio_xy*2.0
xA =  0.18
yA =  0.54
dxA = 0.10
dyA = dxA*slope_A
set label '[l]{\scriptsize $2$}' at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
slope_B = graph_ratio_xy*2.5
xB =  0.18
yB =  0.23
dxB = 0.10
dyB = dxB*slope_B
set label '[l]{\scriptsize $2.5$}' at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
slope_C = graph_ratio_xy*3.0
xC =  0.18
yC =  0.10
dxC = 0.10
dyC = dxC*slope_C
set label '[l]{\scriptsize $3$}' at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

plot \
'zalesak-error-l1-circle.gdat' \
	i 0 u (1e-2*$1):2 \
	t '[r]{$k=1$}' \
	w lp lt 1 pt 1 dt 2 lw 2 lc rgb '#ff0000', \
'zalesak-error-l1-circle.gdat' \
	i 1 u (1e-2*$1):2 \
	t '[r]{$2$}' \
	w lp lt 1 pt 2 dt 2 lw 2 lc rgb '#008800', \
'zalesak-error-l1-circle.gdat' \
	i 2 u (1e-2*$1):2 \
	t '[r]{$3$}' \
	w lp lt 1 pt 3 dt 2 lw 2 lc rgb '#0000ff'

#pause -1 "<retour>"
