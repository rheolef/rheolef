%------------------------------------
\subsection{A tensor transport equation}
%------------------------------------
\label{sec-oldroyd-transport}%
\apindex{discontinuous}%
The aim of this chapter is to introduce to the numerical
resolution of equations involving tensor derivatives
by using discontinuous approximations.
See \citet[chap.~4]{Sar-cours-non-newtonien}
for an introduction to tensor derivatives
and in section~\ref{sec-transport-dg} of the present book,
page~\pageref{sec-transport-dg},
for discontinuous Galerkin methods.

\cindex{problem!transport equation!tensor}
The tensor derivative of a symmetric tensor $\boldsymbol{\sigma}$
is defined by:
\begin{equation}
  \Frac{\mathscr{D}_a\boldsymbol{\sigma}}{\mathscr{D}t}
            =
            \Frac{\partial\boldsymbol{\sigma}}{\partial t}
            +
            ({\bf u}.\nabla)\boldsymbol{\sigma}
            +
            \boldsymbol{\sigma} \boldsymbol{g}_a({\bf u})
            +
            \boldsymbol{g}_a^T({\bf u}) \boldsymbol{\sigma},
          \label{eq-gordon-schowalter-derivative}
\end{equation}
where ${\bf u}$ is a given velocity field,
\begin{equation}
            \boldsymbol{g}_a({\bf u}) =
               \left( (1-a) \ \bnabla {\bf u}
                  - (1+a) \ \bnabla {\bf u}^T
               \right)/2
          \label{eq-oldroyd-def-ma}
\end{equation}
is a generalized velocity grandient
and $a\in\,[-1,1]$ is the parameter of the tensorderivative.
Problems involving tensor derivatives appear
in viscoelasticity
(polymer solution and polymer melt, see e.g. \citealp{Sar-2016-cfma}),
in fluid-particle suspension modeling
(see e.g. \citealp{OzeSarCha-2018}),
in turbulence modeling ($R_{ij}\!-\!\epsilon$ models)
or
in liquid crystals modeling.
Let $\Omega\subset\mathbb{R}^d$ be a bounded open domain.

The time-dependent tensor transport problem writes:

\ \ $(P)$: \emph{find $\boldsymbol{\sigma}$, defined in $]0,T[\times \Omega$, such that}
\begin{eqnarray*}
  \Frac{\mathscr{D}_a\boldsymbol{\sigma}}{\mathscr{D}t}
  + \nu \boldsymbol{\sigma} &=& \boldsymbol{\chi} \ \mbox{ in } ]0,T[\times \Omega \\
  \boldsymbol{\sigma} &=& \boldsymbol{\sigma}_\Gamma \ \mbox{ on } ]0,T[\times \partial\Omega_{-} \\
  \boldsymbol{\sigma}(0) &=& \boldsymbol{\sigma}_0 \ \mbox{ in } \Omega
\end{eqnarray*}
where $\boldsymbol{\sigma}$ is the tensor valued unknown and
$\nu > 0$ is a constant that represents the inverse of the Weissenberg number.
Also $T>0$ is a given final time,
the data $\boldsymbol{\chi}$, $\boldsymbol{\sigma}_\Gamma$ and $\boldsymbol{\sigma}_0$ are known
and $\partial\Omega_-$ denotes the upstream boundary
(see section~\ref{sec-transport-dg}, page~\pageref{sec-transport-dg}).

The steady version of the tensor transport problem writes:

\ \ $(S)$: \emph{find $\boldsymbol{\sigma}$, defined in $\Omega$, such that}
\begin{eqnarray*}
  \left(
            {\bf u}.\nabla)\boldsymbol{\sigma}
            +
            \boldsymbol{\sigma} \boldsymbol{g}_a({\bf u}
            +
            \boldsymbol{g}_a^T({\bf u}) \boldsymbol{\sigma}
  \right)
  + \nu \boldsymbol{\sigma} &=& \boldsymbol{\chi} \mbox{ in } \Omega \\
  \boldsymbol{\sigma} &=& \boldsymbol{\sigma}_\Gamma \ \mbox{ on } \partial\Omega_{-}
\end{eqnarray*}
A sufficient condition this problem to be well posed
is \citep{Sar-1994,Sar-cours-non-newtonien}:
\[
	{\bf u}\in W^{1,\infty}(\Omega)^d
	\ \mbox{ and }\ 
	2\nu
	-
	\|{\rm div}\,{\bf u}\|_{0,\infty,\Omega}
	-
        2a
        \|D({\bf u})\|_{0,\infty,\Omega}
        >
        0
\]
Note that this condition is always satisfied when
${\rm div}\,{\bf u}=0$ and $a=0$.
%
We introduce the space:
\[ 
   X = \{ \boldsymbol{\tau} \in L^2(\Omega)^{d\times d}_s; \ ({\bf u}.\nabla) \boldsymbol{\tau} \in L^2(\Omega)^{d\times d}_s \}
\]
and, for all $\boldsymbol{\sigma},\boldsymbol{\tau}\in X$ 
\begin{equation*}
  \begin{array}{rcccl}
  a(\boldsymbol{\sigma},\boldsymbol{\tau}) &=& 
	{\displaystyle
          \int_\Omega 
	    \left(
              ({\bf u}.\nabla) \boldsymbol{\sigma}
              +
              \boldsymbol{\sigma} \boldsymbol{g}_a({\bf u})
              +
              \boldsymbol{g}_a^T({\bf u}) \boldsymbol{\sigma}
	      +
	      \nu \,
              \boldsymbol{\sigma}
	    \right)
            \!:\! \boldsymbol{\tau}
	    \, {\rm d}x
        }
	&+&
	{\displaystyle
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n}\right)
	    \boldsymbol{\sigma} \!:\! \boldsymbol{\tau} \, {\rm d}s
        }
	\\
  l(\boldsymbol{\tau}) &=& 
	{\displaystyle
  	  \int_\Omega 
	    \boldsymbol{\chi} \!:\! \boldsymbol{\tau} \, {\rm d}x
        }
        &+&
	{\displaystyle
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n}\right)
	    \boldsymbol{\sigma}_\Gamma \!:\! \boldsymbol{\tau} \, {\rm d}s
        }
 \end{array}
\end{equation*}
Then, the variational formulation of the steady problem
writes:

\ \ $(FV)$: \emph{find $\boldsymbol{\sigma}\in X$ such that}
\begin{eqnarray*}
  a(\boldsymbol{\sigma},\boldsymbol{\tau}) &=& l(\boldsymbol{\tau}), \ \forall \boldsymbol{\tau}\in X
\end{eqnarray*}
Note that the term 
\mbox{$
  \max(0,-{\bf u}.{\bf n})
  =
  (|{\bf u}.{\bf n}|-{\bf u}.{\bf n})/2
$}
is positive and vanishes everywhere except on $\partial\Omega_{-}$.
\cindex{boundary condition!weakly imposed}%
Thus, the boundary condition $\phi=\phi_\Gamma$ is weakly imposed 
on $\partial\Omega_{-}$ via the integrals on the boundary.
%
We aim at adapting the discontinuous Galerkin method to this problem.
The \emph{discontinuous} finite element space is defined by:
\[ 
   X_h = \{ \boldsymbol{\tau}_h \in L^2(\Omega)^{d\times d}_s; \boldsymbol{\tau}_{h|K}\in P_k, \ \forall K \in \mathcal{T}_h \}
\]
where $k\geq 0$ is the polynomial degree.
Note that $X_h \not\subset X$
and that the $\nabla\boldsymbol{\tau}_h$ term has no more sense for discontinuous functions $\boldsymbol{\tau}_h \in X_h$.
We introduce the
\emph{broken gradient} $\nabla_h$ as a convenient notation:
\[
	(\nabla_h \boldsymbol{\tau}_h)_{|K} = \nabla (\boldsymbol{\tau}_{h|K})
   	, \ \forall K \in \mathcal{T}_h
\]
Thus
\[
        \int_\Omega
	    (({\bf u}.\nabla_h) \boldsymbol{\sigma}_h) \!:\! \boldsymbol{\tau}_h
	    \, {\rm d}x
	=
        \sum_{K \in \mathcal{T}_h}
          \int_K
	    (({\bf u}.\nabla) \boldsymbol{\sigma}_h) \!:\! \boldsymbol{\tau}_h
	    \, {\rm d}x
   	, \ \forall \boldsymbol{\sigma}_h, \boldsymbol{\tau}_h \in X_h
\]
This leads to a discrete version $a_h$ of the bilinear form $a$,
defined for all $\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h\in X_h$ by:
\begin{eqnarray}
  a_h(\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h)
    	&=& 
    	t_h(\boldsymbol{u};\,\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h)
	+
	\nu
        \int_\Omega
	    \boldsymbol{\sigma}_h
            \!:\!\boldsymbol{\tau}_h
	    \, {\rm d}x
	\nonumber
  \\
  t_h(\boldsymbol{u};\,\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h)
  	&=& 
        \int_\Omega
	    \left(
              ({\bf u}.\nabla_h) \boldsymbol{\sigma}_h
              +
              \boldsymbol{\sigma} \boldsymbol{g}_a({\bf u})
              +
              \boldsymbol{g}_a^T({\bf u}) \boldsymbol{\sigma}
	    \right)
            \!:\!\boldsymbol{\tau}_h
	    \, {\rm d}x
        +
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n} \right)
	    \boldsymbol{\sigma}_h \!:\! \boldsymbol{\tau}_h
	    \, {\rm d}s
	\nonumber
	\\
	&& + \ 
        \sum_{S \in \mathscr{S}_h^{(i)}}
          \int_S
	    \left(
	      -\ 
	      {\bf u}.{\bf n} \,
	      \jump{\boldsymbol{\sigma}_h} \!:\! \average{\boldsymbol{\tau}_h}
              +
	      \Frac{\alpha}{2}
	      |{\bf u}.{\bf n}| \,
	      \jump{\boldsymbol{\sigma}_h} \!:\! \jump{\boldsymbol{\tau}_h}
            \right)
	    \, {\rm d}s
	\label{eq-transport-tensor-def-th}
\end{eqnarray}

\ \ $(FV)_h$: \emph{find $\boldsymbol{\sigma}_h\in X_h$ such that}
\begin{eqnarray*}
  a_h(\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h) &=& l(\boldsymbol{\tau}_h), \ \forall \boldsymbol{\tau}_h\in X_h
\end{eqnarray*}

The following code implement this problem in the \Rheolef\  environment.

% ---------------------------------------
\myexamplelicense{transport_tensor_dg.cc}
% ---------------------------------------

% ---------------------------------------
\subsubsection*{Running the program}
% ---------------------------------------
Let $d=2$ and $\Omega = ]-1/2,\,1/2[^2$.
We consider the rotating field ${\bf u}=(-x_2,x_1)$.
A particular solution of the
time-dependent problem with zero right-hand side
is given by:
\[
       \boldsymbol{\sigma}(x,t)
        =
        \Frac{1}{2} \,
	\exp\left\{
		-\nu t
		-\Frac{(x_1-x_{1,c}(t))^2 + (x_2-x_{2,c}(t))^2}
		      {r_0^2}	
		\right\}
	\times 
        \left( \begin{array}{cc}
                1+\cos(2t) & \sin(2t) \\
                \sin(2t) & 1-\cos(2t)
        \end{array} \right)
\]
where
$
	x_{1,c}(t) = \bar{x}_{1,c} \cos(t) - \bar{x}_{2,c} \sin(t) 
$
and
$
	x_{2,c}(t) = \bar{x}_{1,c} \sin(t) + \bar{x}_{2,c} \cos(t)
$
with $r_0>0$ and $(\bar{x}_{1,c},\,\bar{x}_{2,c})\in\mathbb{R}^2$.
The initial condition is chosen as $\boldsymbol{\sigma}_0(x)=\boldsymbol{\sigma}(0,x)$.
%
% ---------------------------------------
\myexamplenoinput{transport_tensor_exact.icc}
% ---------------------------------------
This exact solution is implemented in the file \file{transport_tensor_exact.icc}.
This file it is not listed here but is available in the \Rheolef\  example directory.
%
For the steady problem, the right-hand side could be chosen
as $\boldsymbol{\chi}=-\Frac{\partial \boldsymbol{\sigma}}{\partial t}$ and then $t=t_0$ is fixed.
%
The numerical tests correspond to
$\nu\!=\!3$,
$r_0\!=\!1/10$,
$(\bar{x}_{1,c},\,\bar{x}_{2,c})=(1/4,\,0)$
and a fixed time $t_0=\pi/8$.
\begin{verbatim}
  make transport_tensor_dg
  mkgeo_grid -t 80 -a -0.5 -b 0.5 -c -0.5 -d 0.5 > square2.geo
  ./transport_tensor_dg square2 P1d > square2.field
  field square2.field -comp 00 -elevation
  field square2.field -comp 01 -elevation
\end{verbatim}
The computation could also be performed with
any \code{Pkd}, with $k\geq 0$.
% ---------------------------------------
\subsubsection*{Error analysis}
% ---------------------------------------

  \begin{figure}[htb]
     \begin{center}
          \includegraphics[height=7cm]{transport-tensor-err.pdf}
     \end{center}
     \caption{Tranport tensor problem: convergence versus mesh size.}
     \label{fig-transport-tensor-err}
  \end{figure}
% ---------------------------------------
\myexamplenoinput{transport_tensor_error_dg.cc}
% ---------------------------------------
The file \file{transport_tensor_error_dg.cc} implement the computation of the error
between the approximate solution $\boldsymbol{\sigma}_h$ and the exact one $\boldsymbol{\sigma}$.
This file it is not listed here but is available in the \Rheolef\  example directory.
The computation of the error is obtained by:
\begin{verbatim}
  make transport_tensor_error_dg
  ./transport_tensor_error_dg < square2.field
\end{verbatim}
The error is plotted on Fig.~\ref{fig-transport-tensor-err} for various
mesh size~$h$ and polynomial order~$k$: observe the optimality of the
convergence properties.
For $k=4$ and on the finest mesh, the error saturates at about $10^{-8}$,
due to finite machine precision effects.
In the next chapter, transport tensor approximation are applied
to viscoelastic fluid flow computations.

