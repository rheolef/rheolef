set terminal cairolatex pdf color standalone
set output "dirichlet-hho-k-k-t-us-l2.tex"
gdat     = 'dirichlet-hho-k-k-t.gdat'

set size square
set log xy
set key at graph 0.47, 0.85
set xrange [1e-3:1]
set yrange [1e-10:1e2]
graph_ratio_xy = 3.0/12.0
set xlabel '[c]{$h$}'
set  label '[l]{\large $\|u_h^* - u\|_{0,2,\Omega}$}' at graph 0.05, 0.92
set xtics (\
        '[c]{$10^{-3}$}' 1e-3, \
        '[c]{$10^{-2}$}' 1e-2, \
        '[c]{$10^{-1}$}' 1e-1, \
        '[c]{$1$}'       1)
set ytics (\
        '[r]{$1$}'        1,     \
        '[r]{$10^{-5}$}'  1e-5,  \
        '[r]{$10^{-10}$}' 1e-10)

# triangle a droite
alpha_A = 2
slope_A = graph_ratio_xy*alpha_A
xA =  0.08
yA =  0.34
dxA = 0.10
dyA = dxA*slope_A
set label sprintf("[l]{\\scriptsize $%g$}",alpha_A) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
alpha_B = 3
slope_B = graph_ratio_xy*alpha_B
xB =  0.08
yB =  0.07
dxB = 0.10
dyB = dxB*slope_B
set label sprintf("[l]{\\scriptsize $%g$}",alpha_B) at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
alpha_C = 4
slope_C = graph_ratio_xy*alpha_C
xC =  0.29
yC =  0.02
dxC = 0.10
dyC = dxC*slope_C
set label sprintf("[l]{\\scriptsize $%g$}",alpha_C) at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

# triangle a droite
alpha_D = 5
slope_D = graph_ratio_xy*alpha_D
xD =  0.47
yD =  0.02
dxD = 0.10
dyD = dxD*slope_D
set label sprintf("[l]{\\scriptsize $%g=k+2$}",alpha_D) at graph xD+dxD+0.02, yD+0.5*dyD right
set arrow from graph xD,     yD to     graph xD+dxD, yD     nohead
set arrow from graph xD+dxD, yD to     graph xD+dxD, yD+dyD nohead
set arrow from graph xD+dxD, yD+dyD to graph xD,     yD     nohead

plot \
gdat \
  i 0 u (1./($1)):6 \
  t '[r]{$k=0$}' \
  w lp lw 2 lc '#ff0000', \
gdat \
  i 1 u (1./($1)):6 \
  t '[r]{$k=1$}' \
  w lp lw 2 lc '#008800', \
gdat \
  i 2 u (1./($1)):6 \
  t '[r]{$k=2$}' \
  w lp lw 2 lc '#0000ff', \
gdat \
  i 3 u (1./($1)):6 \
  t '[r]{$k=3$}' \
  w lp lw 2 lc '#ff00ff'

#pause -1
