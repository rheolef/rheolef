set terminal cairolatex pdf color standalone
set output "speedup-3d-assembly.tex"

# dirichlet_time.cc
set key Left left

set size square
set colors classic
set xrange [0:32]
set yrange [0:32]
set xtics (0,  8,16,24,32)
set ytics (0,1,8,16,24,32)
set xlabel '[c]{$p$}'
set  label '[r]{$S(p)$}' at graph -0.02, 0.87
set  label '[l]{\em assembly $d=3$}' at graph 0.05, 0.60

plot \
x notitle w l dt 2 lc 0, \
1 notitle w l dt 2 lc 0, \
'speedup-dirichlet-cube-10-P1-iter.gdat' \
  u 1:(0.116039/$3) \
  t 'cube-10' \
  w lp lc 1 pt 1, \
'speedup-dirichlet-cube-20-P1-iter.gdat' \
  u 1:(0.83648/$3)  \
  t 'cube-20' \
  w lp lc rgb '#008800' pt 2, \
'speedup-dirichlet-cube-40-P1-iter.gdat' \
  u 1:(6.63793/$3)  \
  t 'cube-40' \
  w lp lc 3 pt 3, \
'speedup-dirichlet-cube-60-P1-iter.gdat' \
  u 1:(22.223/$3)   \
  t 'cube-60' \
  w lp lc 7 pt 7
#pause -1 "<retour>"
