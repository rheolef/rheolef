set terminal cairolatex pdf color standalone
set output "combustion-ac.tex"

set size square
set colors classic
set key center bottom
set xrange [0:1]
set samples 1000
set xlabel '[c]{\large $x$}'
set  label '[l]{\large $u(x)$}' at graph 0.02,0.95

a=1.19967864025773
#lambda=8*a*a/cosh(a)**2
lambda=3.35979473291221
print "lambda=",lambda
# h=1/10 :
lambda_10=3.55
# h=1/100:
lambda_100=3.51
u(x)=2*log(cosh(a)/(cosh(a*(1-2*x))))

plot \
  u(x) title '[r]{exact, $\lambda=\lambda_c$}' lc 0 dt 1, \
  "combustion-10-ac.gdat"  title sprintf('[r]{$h=1/10, \lambda=%g$}', lambda_10)  with lp lc 1 dt 1, \
  "combustion-100-ac.gdat" title sprintf('[r]{$h=1/100, \lambda=%g$}',lambda_100) with l  lc 3 dt 2

#pause -1 "<return>"
