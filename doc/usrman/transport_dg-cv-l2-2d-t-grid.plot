set terminal cairolatex pdf color standalone
set output "transport_dg-cv-l2-2d-t-grid.tex"

set size square
set colors classic
set log
set xrange [5e-3:0.5]
set yrange [1e-15:1]

set xtics (\
        "[r]{$10^{-3}$}" 1e-3, \
        "[r]{$10^{-2}$}" 1e-2, \
        "[r]{$10^{-1}$}" 1e-1, \
        "[r]{$1$}" 1 )
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$1$}" 1 )
set xlabel '[c]{\large $h$}'
set  label '[l]{$\left\|\phi - \phi_h\right\|_{L^2}$}' at graph 0.05,0.92
set key bottom

graph_ratio = 2.0/15.0

# triangle a droite, pente +1
slope_A = graph_ratio*1.0
xA =  0.15
yA =  0.805
dxA = 0.10
dyA = dxA*slope_A
set label "[l]{\\scriptsize $1=k+1$}" at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# placian-fixed-point-p=1,5-err-linf.plot:#triangle a droite, pente +5
slope_B = graph_ratio*5.0
xB =  0.25
yB =  0.14
dxB = 0.10
dyB = dxB*slope_B
set label "[l]{\\scriptsize $5=k+1$}" at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

plot \
 "transport_dg-cv-2d-t-grid.gdat" \
    i 0 u (sqrt(2.)/$1):2 \
    t '[r]{$k=0$}' \
    w lp, \
 "transport_dg-cv-2d-t-grid.gdat" \
    i 1 u (sqrt(2.)/$1):2 \
    t '[r]{$k=1$}' \
    w lp lc rgb '#008800', \
 "transport_dg-cv-2d-t-grid.gdat" \
    i 2 u (sqrt(2.)/$1):2 \
    t '[r]{$k=2$}' \
    w lp, \
 "transport_dg-cv-2d-t-grid.gdat" \
    i 3 u (sqrt(2.)/$1):2\
    t '[r]{$k=3$}' \
    w lp, \
 "transport_dg-cv-2d-t-grid.gdat" \
    i 4 u (sqrt(2.)/$1):2\
    t '[r]{$k=4$}' \
    w lp

#pause -1 "<retour>"
