set terminal cairolatex pdf color standalone
set output "contraction-zr-P1-tau22-cut.tex"

#set noborder
set size square
set xtics (-8,-4,0,2)
set ytics (-2,-1,0)
set xlabel '[c]{\Large $z$}'
set  label '[l]{\Large $\tau_{\theta\theta}(z,0)$}' at graph 0.05, 0.95
plot [-8:2][:0] \
     "contraction-zr-P1-tau22-cut.gdat" notitle with lines lc 0 lt 1 lw 3
#pause -1 "<return>"
