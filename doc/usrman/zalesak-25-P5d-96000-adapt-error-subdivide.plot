set terminal cairolatex pdf color standalone
set output "zalesak-25-P5d-96000-adapt-error-subdivide.tex"

k    = 5
gdat = 'zalesak-25-Pkd-96000-adapt-error-subdivide.gdat'

set log xy
set size square
set xrange [1e-7:1e-2]
set yrange [1e-4:1e-2]
set xlabel '[c]{$h_{min}$}'
set xtics add (\
  '[r]{$10^{-2}$}' 1e-2, \
  '[c]{$10^{-3}$}' 1e-3, \
  '[c]{$10^{-4}$}' 1e-4, \
  '[c]{$10^{-5}$}' 1e-5, \
  '[c]{$10^{-6}$}' 1e-6, \
  '[c]{$10^{-7}$}' 1e-7)
set ytics add (\
  '[r]{$10^{-2}$}' 1e-2, \
  '[r]{$10^{-3}$}' 1e-3, \
  '[r]{$10^{-4}$}' 1e-4)
set label sprintf('[r]{\scriptsize $k=%d$}',k) at graph 0.45, 0.93

#abscisea(fied_error,mesh_size,hmin) = 1./sqrt(mesh_size)
#abscisea(fied_error,mesh_size,hmin) = fied_error
abscisea(fied_error,mesh_size,hmin) = hmin

plot \
gdat \
  i k u (abscisea($1,$4,$5)):2 \
  t '[r]{\scriptsize $E_h^{(n_{max})}$}' \
  w lp lc rgb '#ff0000' lw 2, \
gdat \
  i k u (abscisea($1,$4,$5)):3 \
  t '[r]{\scriptsize $V_h^{(n_{max})}\ $}' \
  w lp lc rgb '#008800' lw 2, \
gdat \
  i k u (abscisea($1,$4,$5)):(1./sqrt($4)) \
  t '[r]{\scriptsize $\mathrm{card}(\mathscr{T}_h)^{-\frac{1}{2}}$}' \
  w lp lc rgb '#0000ff' lw 2

#pause -1
