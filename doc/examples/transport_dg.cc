///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile transport_dg.cc The transport benchmark by the discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  Float alpha   = (argc > 3) ? atof(argv[3]) : 1;
  Float sigma   = (argc > 4) ? atof(argv[4]) : 3;
  point u (1,0,0);
  trial phi (Xh); test psi (Xh);
  form ah = integrate (dot(u,grad_h(phi))*psi + sigma*phi*psi)
          + integrate ("boundary", max(0., -dot(u,normal()))*phi*psi)
          + integrate ("internal_sides", 
	 	- dot(u,normal())*jump(phi)*average(psi)
                + 0.5*alpha*abs(dot(u,normal()))*jump(phi)*jump(psi));
  field lh = integrate ("boundary", max(0, -dot(u,normal()))*psi);
  field phi_h(Xh);
  problem p (ah);
  p.solve (lh, phi_h);
  dout << catchmark("sigma") << sigma << endl
       << catchmark("phi")   << phi_h;
}
