#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${TOP_SRCDIR}/main/tst"
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "    not yet (skiped)"
#exit 0

status=0
# --------------------------------------------------------------------
# do meshes
# --------------------------------------------------------------------
run "${SBINDIR}/mkgeo_grid_2d -v4 10 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-2d.geo 2>/dev/null"
if test $? -ne 0; then exit 1; fi

run "${SBINDIR}/mkgeo_grid_3d -v4 3 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-3d.geo 2>/dev/null"
if test $? -ne 0; then exit 1; fi

if false; then 
#TODO: stokes axi
run "${SBINDIR}/mkgeo_grid_2d -v4 10 -rz 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-rz.geo 2>/dev/null"
if test $? -ne 0; then exit 1; fi
fi
# --------------------------------------------------------------------
# run tests on embankment incompressible elasticity
# --------------------------------------------------------------------

for mesh in mesh-2d mesh-3d; do
 for inv_lambda in 0 1; do
  loop_mpirun "./incompressible-elasticity $mesh $inv_lambda 2>/dev/null| \$RUN ./incompressible-elasticity-check >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
 done
done
# --------------------------------------------------------------------
# clean
# --------------------------------------------------------------------
run "/bin/rm -f mesh-2d.geo mesh-3d.geo mesh-rz.geo"

exit $status

