///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile cosinus_vector.h The cosinus vector function
struct u_exact {
  point operator() (const point& x) const {
    return point(cos(w*(x[0]+2*x[1])),sin(w*(x[0]-2*x[1]))); }
  u_exact(size_t d) : w(1) {}
  Float w;
};
struct div_u_exact {
  Float operator() (const point& x) const {
    return -w*(sin(w*(x[0]+2*x[1])) + 2*cos(w*(-x[0]+2*x[1]))); }
  div_u_exact(size_t d) : w(1) {}
  Float w;
};
