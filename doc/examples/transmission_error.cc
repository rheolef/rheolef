///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
size_t d;
Float epsilon;
Float u (const point& x) {
  Float x0 = (d <= 2) ? x[0] : x[1];
  if (x0 < Float(0.5)) 
    return x0*((1+3*epsilon)/(2*(1+epsilon)) - x0)/(2*epsilon);
  else
    return (1-x0)*(x0 + (1-epsilon)/(2*(1+epsilon)))/2;
}
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float error_linf_expected = (argc > 1) ? atof(argv[1]) : 1e+38;
  field uh;
  din >> catchmark("epsilon") >> epsilon
      >> catchmark("u")       >> uh;
  d = uh.get_geo().dimension();
  space Xh = uh.get_space();
  field pi_h_u = lazy_interpolate(Xh, u);
  field eh = pi_h_u - uh;
  trial u (Xh); test v (Xh);
  form m = integrate (u*v);
  derr << "error_linf "  << eh.max_abs() << endl
       << "error_l2   "  << sqrt(m(eh,eh)) << endl;
  return (eh.max_abs() < error_linf_expected) ? 0 : 1;
}
