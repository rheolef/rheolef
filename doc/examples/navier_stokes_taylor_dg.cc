///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile navier_stokes_taylor_dg.cc The Navier-Stokes equations for the Taylor benchmark with fixed-point and discontinuous Galerkin methods -- di Pietro & Ern variant
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "taylor.h"
#include "stokes_dirichlet_dg.icc"
#include "inertia.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2], "vector");
  space Qh (omega, argv[2]);
  Float  Re       = (argc > 3) ? atof(argv[3]) : 1;
  size_t max_iter = (argc > 4) ? atoi(argv[4]) : 1;
  form a, b, c, mp;
  field lh, kh;
  stokes_dirichlet_dg (Xh, Qh, a, b, c, mp, lh, kh);
  field uh (Xh, 0), ph (Qh, 0);
  problem_mixed stokes (a, b, c);
  stokes.set_metric (mp);
  stokes.solve (lh, kh, uh, ph);
  trial u (Xh); test v (Xh);
  form a1 = a + Re*inertia (uh, u, v);
  lh += Re*inertia_fix_rhs (v);
  derr << "#k r as" << endl;
  for (size_t k = 0; k < max_iter; ++k) {
    stokes = problem_mixed (a1, b, c);
    stokes.set_metric (mp);
    stokes.solve (lh, kh, uh, ph);
    form th = inertia (uh, u, v);
    a1 = a + Re*th;
    field rh = a1*uh + b.trans_mult(ph) - lh;
    derr << k << " " << rh.max_abs() << " " << th(uh,uh) << endl;
  }
  dout << catchmark("Re") << Re << endl
       << catchmark("u")  << uh
       << catchmark("p")  << ph;
}
