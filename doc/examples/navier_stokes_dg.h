///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile navier_stokes_dg.h The Navier-Stokes equations with the discontinuous Galerkin method -- class header
struct navier_stokes_dg {
  typedef Float                    float_type;
  typedef Eigen::Matrix<field,2,1> value_type;
  navier_stokes_dg (Float Re, const geo& omega, string approx);
  value_type initial (string restart) const;
  value_type residue     (const value_type& uh) const;
  void update_derivative (const value_type& uh) const;
  value_type derivative_solve      (const value_type& mrh) const;
  value_type derivative_trans_mult (const value_type& mrh) const;
  Float space_norm       (const value_type& uh) const;
  Float dual_space_norm  (const value_type& mrh) const;
  Float Re;
  space Xh, Qh;
  integrate_option iopt;
  form  a0, b, c, mu, mp;
  field lh0, lh, kh;
  problem pmu, pmp;
  mutable form a1;
  mutable problem_mixed stokes1;
};
#include "navier_stokes_dg1.icc"
#include "navier_stokes_dg2.icc"
