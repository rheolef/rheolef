///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile mosolov_exact_circle.h The Mossolov problem for a circular pipe -- exact solution
struct u {
  Float operator() (const point& x) const {
    return (pow(1-Bi,1+1/n) - pow(max(Float(0),norm(x)-Bi),1+1/n))/(1+1/n);
  }
  u (Float Bi1, Float n1) : Bi(Bi1), n(n1) {}
  protected: Float Bi, n;
};
struct grad_u {
  point operator() (const point& x) const {
    Float r = norm(x);
    return (r <= Bi) ? point(0,0) : -pow(r-Bi, 1/n)*(x/r);
  }
  grad_u (Float Bi1, Float n1) : Bi(Bi1), n(n1) {}
  protected: Float Bi, n;
};
struct sigma {
  point operator() (const point& x) const { return -x; }
  sigma (Float=0, Float=0) {}
};
