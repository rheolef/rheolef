///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile laplace_s.cc The Poisson problem on a surface
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "torus.icc"
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  geo gamma (argv[1]);
  size_t d = gamma.dimension();
  space Wh (gamma, argv[2]);
  trial u (Wh); test v (Wh);
  form  a = integrate (dot(grad_s(u),grad_s(v)));
  field b = integrate(v);
  field lh = integrate (f(d)*v);
  form  A = {{    a,     b },
             { trans(b), 0 }};
  field Bh = {   lh,     0 };
  field Uh (Bh.get_space(), 0);
  A.set_symmetry(true);
  problem pa (A);
  pa.solve (Bh, Uh);
  dout << Uh[0];
}
