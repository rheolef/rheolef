///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile taylor_exact.h The Taylor benchmark -- the exact solution of the Stokes problem
#include "taylor.h"
typedef g u_exact;
struct p_exact {
  Float operator() (const point& x) const {
    return - Re*(cos(2*pi*x[0]) + cos(2*pi*x[1]))/4 
           - (!have_kinetic_energy ? 0 : Re*(norm2(u(x))/2 - 0.25));
  }
  p_exact(Float Re1=0, bool have_kinetic_energy1=false)
  : u(), pi(acos(Float(-1.0))), Re(Re1), have_kinetic_energy(have_kinetic_energy1) {}
  u_exact u; const Float pi, Re; bool have_kinetic_energy;
};
