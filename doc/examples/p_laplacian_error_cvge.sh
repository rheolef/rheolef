#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# utility
# ------------------------------------------
verbose=false
my_eval () {
  command="$*"
  if test "$verbose" = true; then echo "! $command" 1>&2; fi
  eval $command
  if test $? -ne 0; then
    echo "$0: error on command: $command"
    exit 1
  fi
}
# ------------------------------------------
k=${1-"1"}
p="1.5"
eps="1e-14"
method="newton"
#method="fixed_point"
L="3 5 10 20 30 40 50"
echo "# p-laplacian: convergence with h=1/n"
echo "# method = $method"
echo "# p      = $p"
echo "# approx = P$k"
echo "# tol    = $eps"
echo "# n err_linf err_lp err_w1p"
for n in $L; do
  geo="circle-$n.geo"
  my_eval "mkgeo_ball -t $n -order $k > $geo"
  if test $method = "fixed_point"; then
    my_eval "./p_laplacian_fixed_point   $geo P$k $p - $eps 2>/dev/null | ./p_laplacian_error 1e10 > tmp.log 2>/dev/null"
  else
    my_eval "./p_laplacian_damped_newton $geo P$k $p   $eps 2>/dev/null | ./p_laplacian_error 1e10 > tmp.log 2>/dev/null"
  fi
  err_linf=`grep err_linf tmp.log | gawk '{print $3}'`
  err_lp=`grep err_lp     tmp.log | gawk '{print $3}'`
  err_w1p=`grep err_w1p   tmp.log | gawk '{print $3}'`
  echo "$n $err_linf $err_lp $err_w1p"
done
