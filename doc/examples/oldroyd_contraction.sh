#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR=${SBINDIR-"../../main/sbin"}
BINDIR=${BINDIR-"../../main/bin"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

psi_max="0.00139176663546753"
tol="1e-4"

loop_mpirun "./oldroyd_contraction $SRCDIR/icontraction-small3-zr.geo 0.05 0.05 0.1 2>/dev/null | RHEOPATH=$SRCDIR $BINDIR/field -mark u -field - 2>/dev/null | RHEOPATH=$SRCDIR ./streamf_contraction 2>/dev/null | RHEOPATH=$SRCDIR $BINDIR/field -max - 2>/dev/null | gawk '{err = int((\$1-${psi_max})/${tol}); have_value=1;} END { exit (have_value == 1 && err == 0)?0:1;}'"
if test $? -ne 0; then status=1; fi

run "rm -f tmp.log"

exit $status
