///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile stokes_contraction.cc The Stokes problem on the contraction benchmark -- the Taylor-Hood element
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "contraction.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo  omega (argv[1]);
  space Xh = contraction::velocity_space (omega, "P2");
  space Qh  (omega, "P1");
  trial u (Xh), p (Qh);
  test  v (Xh), q (Qh);
  form a  = integrate (2*ddot(D(u),D(v)));
  form b  = integrate (-div(u)*q);
  field uh = contraction::velocity_field (Xh);
  field ph (Qh, 0);
  problem_mixed stokes (a, b);
  stokes.solve (field(Xh,0), field(Qh,0), uh, ph);
  dout << catchmark("inv_lambda") << 0 << endl
       << catchmark("u")  << uh
       << catchmark("p")  << ph;
}
