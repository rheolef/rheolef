///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace std;
using namespace rheolef;
#include "sphere.icc"
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  Float tol    = (argc > 1) ? atof(argv[1]) : 1e-7;
  bool do_dump = (argc > 2) ? true : false;
  field phi_h;
  din >> catchmark("phi") >> phi_h;
  const space& Xh = phi_h.get_space();
  band gh (phi_h);
  space Bh (gh.band(), "P1");
  size_t d = Bh.get_geo().dimension();
  field uh(Bh);
  din >> catchmark("u")   >> uh;
  trial u (Bh); test v (Bh);
  form m = lazy_integrate (gh, u*v);
  form a = lazy_integrate (gh, dot(grad_s(u),grad_s(v)));
  field pi_h_u = lazy_interpolate(Bh, u_exact(d));
  field eh = pi_h_u - uh;
  field phi_h_band = phi_h [gh.band()];
  // TODO: loop on each cc(i)
  Float c = - m(eh,phi_h_band)/m(phi_h_band,phi_h_band);
  eh = eh + c*phi_h_band;
  Float err_l2 = sqrt(m(eh,eh));
  Float err_h1 = sqrt(a(eh,eh));
  derr << "err_l2 = " << err_l2 << endl
       << "err_h1 = " << err_h1 << endl;
  if (do_dump) {
    dout << catchmark ("u")  << uh
         << catchmark ("ue") << pi_h_u
         << catchmark ("eh") << eh;
  }
  return (err_l2 < tol && err_h1 < tol) ? 0 : 1;
}
