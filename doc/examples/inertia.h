///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile inertia.h The inertia term of the Navier-Stokes equation with the discontinuous Galerkin method -- di Pietro & Ern variant
template<class W, class U, class V>
form inertia (W w, U u, V v, 
  integrate_option iopt = integrate_option())
{
  return
    integrate (dot(grad_h(u)*w,v) + 0.5*div_h(w)*dot(u,v), iopt)
  + integrate ("boundary", - 0.5*dot(w,normal())*dot(u,v), iopt)
  + integrate ("internal_sides",
     - dot(average(w),normal())*dot(jump(u),average(v))
     - 0.5*dot(jump(w),normal())
          *(dot(average(u),average(v)) + 0.25*dot(jump(u),jump(v))), iopt);
}
field inertia_fix_rhs (test v, 
  integrate_option iopt = integrate_option())
{
  return integrate("boundary", - 0.5*dot(g(),normal())*dot(g(),v), iopt);
}
