///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile stokes_contraction_bubble.cc The Stokes problem on the driven cavity benchmark -- the P1-bubble element
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "contraction.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo  omega (argv[1]);
  space X1h = contraction::velocity_space (omega, "P1");
  space Bh  (omega, "bubble", "vector");
  space Qh  (omega, "P1");
  trial u1 (X1h), ub (Bh), p (Qh);
  test  v1 (X1h), vb (Bh), q (Qh);
  form b1 = integrate (-div(u1)*q);
  form bb = integrate (-div(ub)*q);
  form a1 = integrate (2*ddot(D(u1),D(v1)));
  integrate_option iopt;
  iopt.invert = true;
  form inv_ab = integrate (2*ddot(D(ub),D(vb)), iopt);
  form c = bb*inv_ab*trans(bb);
  field u1h = contraction::velocity_field (X1h);
  field ph (Qh, 0);
  problem_mixed stokes (a1, b1, c);
  stokes.solve (field(X1h,0), field(Qh,0), u1h, ph);
  dout << catchmark("inv_lambda") << 0 << endl
       << catchmark("u")  << u1h
       << catchmark("p")  << ph;
}
