#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR=${SBINDIR-"../../main/sbin"}
BINDIR=${BINDIR-"../../main/bin"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

tol="1e-7"
lambda="1.57289546593186"
u_max="0.23929044237744"

run "$SBINDIR/mkgeo_grid_1d -e 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-1d.geo 2>/dev/null"

loop_mpirun "./combustion_newton mesh-1d.geo P1 $lambda 2>/dev/null | $BINDIR/field -max - 2>/dev/null | gawk '{ exit ((int(\$1-${u_max})/${tol}) == 0)?0:1;}'"
if test $? -ne 0; then status=1; fi

run "rm -f mesh-1d.geo"

exit $status
