#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${TOP_SRCDIR}/main/tst"
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "    not yet (skiped)"
#exit 0

# BUG_P2_P1D fails with mpi
NPROC_MAX=1

status=0
# --------------------------------------------------------------------
# do meshes
# --------------------------------------------------------------------
run "${SBINDIR}/mkgeo_grid_2d -v4 -t 10 2>/dev/null | ${BINDIR}/geo -upgrade - 2>/dev/null | ${SBINDIR}/geo_split | ${BINDIR}/geo -upgrade - 2>/dev/null > mesh-2d.geo"
if test $? -ne 0; then exit 1; fi

# --------------------------------------------------------------------
# run tests
# --------------------------------------------------------------------
for mesh in mesh-2d; do
  loop_mpirun "./stokes_cavity_incompressible $mesh 2>/dev/null >/dev/null"
  if test $? -ne 0; then status=1; fi
done
# --------------------------------------------------------------------
# clean
# --------------------------------------------------------------------
run "/bin/rm -f mesh-2d.geo"

exit $status
