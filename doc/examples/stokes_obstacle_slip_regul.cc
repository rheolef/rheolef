///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile stokes_obstacle_slip_regul.cc The Stokes problem on the obstacle benchmark with slip boundary condition -- the Taylor-Hood element
#include "rheolef.h"
using namespace rheolef;
using namespace std;
point n_exact (const point& x) { return -x; }
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo  omega (argv[1]);
  Float eps = (argc > 2) ? atof(argv[2]) : 1e-7;
  space Xh (omega, "P2", "vector");
  Xh.block ("downstream");
  Xh[1].block ("wall");
  Xh[1].block ("axis");
  Xh[1].block ("vaxis");
  space Qh  (omega, "P1");
  trial u (Xh), p (Qh);
  test  v (Xh), q (Qh);
  form a  = integrate (2*ddot(D(u),D(v)))
          + (1/eps)*integrate("obstacle",dot(u,n_exact)*dot(v,n_exact));
  form b  = integrate (-div(u)*q);
  field uh (Xh,0);
  uh[0]["downstream"] = 1;
  field ph (Qh, 0);
  problem_mixed stokes (a, b);
  stokes.solve (field(Xh,0), field(Qh,0), uh, ph);
  dout << catchmark("u")  << uh
       << catchmark("p")  << ph;
}
