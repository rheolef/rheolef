///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile transmission.cc The transmission problem
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1d";
  Float epsilon = (argc > 3) ? atof(argv[3]) : 1e-2;
  space Xh (omega, approx);
  Xh.block ("left");
  Xh.block ("right");
  string eta_approx = "P" + to_string(Xh.degree()-1) + "d";
  space Qh (omega, eta_approx);
  field eta_h (Qh);
  eta_h ["east"] = 1;
  eta_h ["west"] = epsilon;
  trial u (Xh); test v (Xh);
  form  a  = integrate (eta_h*dot(grad(u),grad(v)));
  field lh = integrate (v);
  field uh (Xh);
  uh["left"] = 0; uh["right"] = 0;
  problem p (a);
  p.solve (lh, uh);
  dout << catchmark("epsilon") << epsilon << endl
       << catchmark("u")       << uh;
}
