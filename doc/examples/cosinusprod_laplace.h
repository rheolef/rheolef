///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile cosinusprod_laplace.h The cosinus product function -- right-hand-side and boundary condition for the Laplace problem
struct f {
  Float operator() (const point& x) const { 
    return d*pi*pi*cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  f(size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; const Float pi;
};
struct g {
  Float operator() (const point& x) const { 
    return cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[2]); }
  g(size_t d1) : pi(acos(Float(-1))) {}
  const Float pi;
};
