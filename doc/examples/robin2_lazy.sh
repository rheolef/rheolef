#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
DATADIR=$TOP_SRCDIR/main/tst
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

echo "      not yet (skipped)"
exit 0

status=0

# geo			err_P1	err_P2	err_P3
L="
line-bdry-v2		2e-2	2e-4	6e-6
carre-bamg-v2		4e-2	2e-3	4e-5
carre-bamg-q-dom-v2	9e-3	2e-4	4e-6
carre-tq-10-dom-v2	3e-2	5e-4	3e-5
cube-dom-v2		2e-1	3e-2	6e-3
cube-H-6-dom-v2		6e-2	2e-3	2e-4
cube-P-5-dom-v2		2e-1	6e-3	6e-4
my_cube_PH-5-v2         2e-1	6e-3	6e-4
my_cube_TP-5-v2         2e-1	2e-2	4e-3
my_cube_TPH-5-v2        2e-1	2e-2	3e-3
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2 P3; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./robin2_lazy $DATADIR/$geo $Pk 2>/dev/null | RHEOPATH=$DATADIR \$RUN ./sinusprod_error $err 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done

exit $status
