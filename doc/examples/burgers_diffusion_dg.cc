///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile burgers_diffusion_dg.cc The diffusive Burgers equation by the discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "burgers.icc"
#include "burgers_flux_godunov.icc"
#include "runge_kutta_semiimplicit.icc"
#include "burgers_diffusion_exact.h"
#undef  NEUMANN
#include "burgers_diffusion_operators.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  size_t k = Xh.degree();
  Float epsilon = (argc > 3) ? atof(argv[3]) : 0.1;
  size_t nmax   = (argc > 4) ? atoi(argv[4]) : 500;
  Float  tf     = (argc > 5) ? atof(argv[5]) : 1;
  size_t p      = (argc > 6) ? atoi(argv[6]) : min(k+1,rk::pmax);
  Float delta_t = tf/nmax;
  size_t d = omega.dimension();
  Float beta = (k+1)*(k+d)/Float(d);
  trial u (Xh); test v (Xh);
  form m = integrate (u*v);
  integrate_option iopt;
  iopt.invert = true;
  form inv_m = integrate (u*v, iopt);
  form a = epsilon*(
              integrate (dot(grad_h(u),grad_h(v)))
#ifdef  NEUMANN
            + integrate ("internal_sides", 
#else //  NEUMANN
            + integrate ("sides", 
#endif //  NEUMANN
                  beta*penalty()*jump(u)*jump(v)
	 	- jump(u)*average(dot(grad_h(v),normal()))
	 	- jump(v)*average(dot(grad_h(u),normal()))));
  vector<problem> pb (p+1);
  for (size_t i = 1; i <= p; ++i) {
    form ci = m + delta_t*rk::alpha[p][i][i]*a;
    pb[i] = problem(ci);
  }
  vector<field> uh(p+1, field(Xh,0));
  uh[0] = lazy_interpolate (Xh, u_init(epsilon));
  branch even("t","u");
  dout << catchmark("epsilon") << epsilon << endl
       << even(0,uh[0]);
  for (size_t n = 0; n < nmax; ++n) {
    Float tn = n*delta_t;
    Float t  = tn + delta_t;
    field uh_next = uh[0] - delta_t*rk::tilde_beta[p][0]*(inv_m*gh(epsilon, tn, uh[0], v));
    for (size_t i = 1; i <= p; ++i) {
      Float ti = tn + rk::gamma[p][i]*delta_t;
      field rhs = m*uh[0] - delta_t*rk::tilde_alpha[p][i][0]*gh(epsilon, tn, uh[0], v);
      for (size_t j = 1; j <= i-1; ++j) {
        Float tj = tn + rk::gamma[p][j]*delta_t;
        rhs -= delta_t*( rk::alpha[p][i][j]*(a*uh[j] - lh(epsilon,tj,v))
                       + rk::tilde_alpha[p][i][j]*gh(epsilon, tj, uh[j], v));
      }
      rhs += delta_t*rk::alpha[p][i][i]*lh (epsilon, ti, v);
      pb[i].solve (rhs, uh[i]);
      uh_next -= delta_t*(inv_m*( rk::beta[p][i]*(a*uh[i] - lh(epsilon,ti,v))
                                + rk::tilde_beta[p][i]*gh(epsilon, ti, uh[i], v)));
    }
    uh_next = limiter(uh_next);
    dout << even(tn+delta_t,uh_next);
    uh[0] = uh_next;
  }
}
