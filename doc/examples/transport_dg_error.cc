///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct phi {
 Float operator() (const point& x) const { return exp(-sigma*x[0]); }
 phi(Float sigma1) : sigma(sigma1) {}
 Float sigma;
};
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-10;
  bool dump = (argc > 2);
  Float sigma;
  field phi_h;
  din >> catchmark("sigma") >> sigma
      >> catchmark("phi")   >> phi_h;
  size_t k = phi_h.get_space().degree();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(2*k+1);
  Float err_l2   = sqrt(integrate (phi_h.get_geo(), sqr(phi_h - phi(sigma)), iopt));
  space Th1 (phi_h.get_geo(), "P"+to_string(k+1)+"d");
  field eh = lazy_interpolate(Th1, phi_h - phi(sigma));
  Float err_linf = eh.max_abs();
  derr << "err_l2   = " << err_l2   << endl
       << "err_linf = " << err_linf << endl;
  if (dump) {
    dout << catchmark("phi_h")    << phi_h
         << catchmark("phi")      << lazy_interpolate(phi_h.get_space(), phi(sigma))
         << catchmark("e")        << eh;
  }
  return (err_l2 < tol) ? 0 : 1;
}
