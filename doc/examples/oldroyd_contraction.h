///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile oldroyd_contraction.h The Oldroyd problem on the contraction benchmark -- boundary conditions
#include "contraction.h"
struct oldroyd_contraction: contraction {
  struct tau_upstream: base {
    tau_upstream (geo omega, Float We1, Float alpha1)
      : base(omega), We(We1), alpha(alpha1) {}
    tensor operator() (const point& x) const {
      tensor tau;
      Float dot_gamma = - 2*base::umax*x[1]/sqr(base::c);
      tau(0,0) =     2*alpha*We*sqr(dot_gamma);
      tau(0,1) = tau(1,0) = alpha*dot_gamma;
      tau(1,1) = 0;
      return tau;
    }
    Float We, alpha;
  };
};
