///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile dirichlet_hdg_average.cc The Poisson problem by the hybrid discontinuous Galerkin method -- local averaging
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "dirichlet_hdg_average.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  field uh, lambda_h;
  din >> catchmark("u")      >> uh
      >> catchmark("lambda") >> lambda_h;
  field bar_uh = dirichlet_hdg_average (uh, lambda_h);
  dout << catchmark("u")     << uh
       << catchmark("bar_u") << bar_uh;
}
