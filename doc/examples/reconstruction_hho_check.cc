///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile reconstruction_hho_check.cc residue check
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod.h"
#include "diffusion_isotropic.h"
int main(int argc, char**argv) {
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-15;
  environment rheolef (argc, argv);
  field us_h, zeta_h;
  din >> catchmark("us")     >> us_h
      >> catchmark("zeta")   >> zeta_h;
  const space& Xhs =     us_h.get_space();
  const space& Zh  =   zeta_h.get_space();
  const geo& omega = Xhs.get_geo();
  check_macro (Xhs.degree() > 0, "invalid Xhs degree");
  size_t k = Xhs.degree() - 1, d = omega.dimension();
  string Pkd = "P"+to_string(k)+"d",
         Pld = (argc > 2) ? argv[2] : Pkd;
  space  Xh (omega, Pld),
         Mh (omega["sides"], Pkd);
  trial u(Xh), lambda(Mh), us(Xhs), zeta (Zh);
  test  v(Xh), mu    (Mh), vs(Xhs), xi   (Zh);
  integrate_option iopt;
  iopt.invert = true;
  form as = integrate (dot(grad_h(us),A(d)*grad_h(vs)));
  form bs = integrate (us*xi);
  form inv_m  = integrate (u*v, iopt);
  form ms = integrate (lambda*mu);
  field llh = integrate(u_exact(d)*v);
  field rhs = integrate(u_exact(d)*mu);
  field pi_Xh_u = inv_m*llh;
  field pi_Mh_lambda(Mh);
  problem pms (ms);
  pms.solve (rhs, pi_Mh_lambda);
  field lh = integrate (dot(grad_h(pi_Xh_u),A(d)*grad_h(vs))
                    + on_local_sides((pi_Mh_lambda-pi_Xh_u)
                                   *dot(normal(),A(d)*grad_h(vs))));
  field kh = integrate (pi_Xh_u*xi);
  field rus = as*us_h - lh;
  field rz  = bs*us_h - kh;
  Float err_us = rus.max_abs();
  Float err_z  =  rz.max_abs();
  derr << "err_us   = " << err_us << endl;
  derr << "err_z    = " << err_z  << endl;
  derr << "|zeta_h| = " << zeta_h.max_abs() << endl;
  return (max(max(err_us,err_z), zeta_h.max_abs()) < tol) ? 0 : 1;
}
