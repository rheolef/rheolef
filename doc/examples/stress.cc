///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile stress.cc The stress tensor for the linear elasticity and Stokes problems
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char** argv) {
  environment rheolef (argc,argv);
  Float inv_lambda;
  field uh;
  din >> catchmark("inv_lambda") >> inv_lambda
      >> catchmark("u")          >> uh;
  const geo& omega = uh.get_geo();
  const space& Xh  = uh.get_space();
  string grad_approx = "P" + to_string(Xh.degree()-1) + "d";
  space Th  (omega, grad_approx, "tensor");
  size_t d = omega.dimension();
  tensor I = tensor::eye (d);
  field sigma_h;
  if (inv_lambda == 0)
       sigma_h = lazy_interpolate (Th, 2*D(uh));
  else sigma_h = lazy_interpolate (Th, 2*D(uh) + (1/inv_lambda)*div(uh)*I);
  dout << catchmark("s") << sigma_h;
}
