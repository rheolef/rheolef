///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile elasticity_taylor_dg.cc The elasticity problem with the Taylor benchmark and discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "taylor.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2], "vector");
  Float lambda = (argc > 3) ? atof(argv[3]) : 1;
  size_t d = omega.dimension();
  size_t k = Xh.degree();
  Float beta = (k+1)*(k+d)/Float(d);
  trial u (Xh); test v (Xh);
  form  a = integrate (lambda*div_h(u)*div_h(v) + 2*ddot(Dh(u),Dh(v)))
          + integrate (omega.sides(),
                  beta*penalty()*dot(jump(u),jump(v))
                - lambda*dot(jump(u),average(div_h(v)*normal()))
                - lambda*dot(jump(v),average(div_h(u)*normal()))
	 	- 2*dot(jump(u),average(Dh(v)*normal()))
	 	- 2*dot(jump(v),average(Dh(u)*normal())));
  field lh = integrate (dot(f(),v))
           + integrate (omega.boundary(),
                  beta*penalty()*dot(g(),jump(v))
                - lambda*dot(g(),average(div_h(v)*normal()))
                - 2*dot(g(),average(Dh(v)*normal())));
  field uh(Xh);
  problem p (a);
  p.solve (lh, uh);
  dout << uh;
}
