///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile zalesak.h The Zalesak slotted disk benchmark -- the exact solution
struct u {
  point operator() (const point& x) const {
    return point(-0.5*(x[1]-yc), 0.5*(x[0]-xc));
  }
  static Float period() {
    Float pi = acos (Float(-1));
    return 4*pi;
  }
  u() {}
protected:
  static constexpr Float
    xc = 0.5,  // rotation center
    yc = 0.5;
};
struct phi_exact {
  Float operator() (const point& x) const {
    Float xm = xc + cos(0.5*t)*(x[0]-xc) + sin(0.5*t)*(x[1]-yc);
    Float ym = yc - sin(0.5*t)*(x[0]-xc) + cos(0.5*t)*(x[1]-yc);
    Float d0 = sqrt((xm-x0)*(xm-x0) + (ym-y0)*(ym-y0)) - r;
    if (d0 >= 0) {
      Float d2 = sqrt(sqr(xm-x2) + sqr(ym-y2));
      Float d3 = sqrt(sqr(xm-x3) + sqr(ym-y3));
      if (fabs(xm-x0) < w/2 && ym < y2) {
        return min(d2,d3);
      } else {
        return min(d2,min(d3,d0));
      }
    }
    else {
      if (ym < y4) {
        if (xm < x2) {
	  Float d1 = xm - x2;
	  return max(d0,d1);  
        } else if (xm > x3) {
	  Float d1 = x3 - xm;
	  return max(d0,d1);  
        } else {
          Float d1 = xm - x2;
          Float d2 = x3 - xm;
          Float d3 = y4 - ym;
          return min(d1,min(d2,d3));
        }
      } else {
        Float d1 = sqrt(sqr(xm-x4) + sqr(ym-y4));
        Float d2 = sqrt(sqr(xm-x5) + sqr(ym-y5));
        if (xm < x2) {
	  return max(d0,-d1);
        } else if (xm > x3) {
	  return max (d0,-d2);
        } else {
  	  Float d3 = y4 - ym;
	  return max(d3,d0);
        }
      }
    }
  }
  phi_exact (Float t1);
  Float perimeter() const {
    Float pi = acos(Float(-1));
    Float d24 = sqrt(sqr(x4-x2) + sqr(y4-y2));
    Float d45 = w;
    Float d53 = d24;
    Float theta = asin(0.5*w/r);
    Float arc32 = (2*pi - theta)*r;
    return d24 + d45 + d53 + arc32;
  }
protected:
  Float t, x2, y2, x3, y3, x4, y4, x5, y5;
  static constexpr Float
    xc = 0.5,  // rotation center
    yc = 0.5,
    x0 = 0.50, // circle center
    y0 = 0.75,
    r  = 0.15, // circle radius
    w  = 0.05, // slot width
    l  = 0.25; // slot length
};
inline
phi_exact::phi_exact (Float t1)
  : t  (t1),
    x2 (x0 - w/2),
    y2 (y0 - sqrt(r*r - (w/2)*(w/2))), 
    x3 (x0 + w/2),
    y3 (y2),
    x4 (x2),
    y4 (y0 + l - r),
    x5 (x3),
    y5 (y4)
{}
struct phi0 {
  Float operator() (const point& x) const { return _phi(x); }
  phi0() : _phi(0) {}
  phi_exact _phi;
};
