///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile d_projection_dx.h The projection for yield-stress rheology -- its derivative
#include "projection.h"
struct d_projection_dx {
  Float operator() (const Float& x) const {
    if (fabs(x) <= a) return 0;
    if (n == 1) return 1/(c + r);
    if (r == 0) return pow(fabs(x)-a,-1+1/n)/(n*pow(c,1/n));
    return 1/(r + n*c*pow(_phi(fabs(x)-a),-1+n));
  }
  d_projection_dx (Float a1, Float n1=1, Float c1=1, Float r1=0)
   : a(a1), n(n1), c(c1), r(r1), _phi(n1,c1,r1) {}
  Float a,n,c,r;
  phi _phi;
};
