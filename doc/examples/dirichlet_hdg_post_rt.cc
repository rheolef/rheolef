///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile dirichlet_hdg_post_rt.cc The Poisson problem by the hybrid discontinuous Galerkin method -- post-treatment with the Raviart-Thomas element
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float n, beta;
  field sigma_h, uh, lambda_h;
  din >> catchmark("n")      >> n
      >> catchmark("beta")   >> beta
      >> catchmark("u")      >> uh
      >> catchmark("lambda") >> lambda_h
      >> catchmark("sigma")  >> sigma_h;
  const geo& omega = uh.get_geo();
  size_t d = omega.dimension();
  size_t k = uh.get_space().degree();
  string approx = (k == 0) ? "empty" : "P"+to_string(k-1)+"d";
  space Tht(omega, "RT"+to_string(k)+"d");
  space Wht(omega, approx, "vector");
  space Mht(omega, "trace_n(RT"+to_string(k)+"d)");
  space Sht = Wht*Mht;
  trial sigma_t (Tht); test tau (Sht);
  auto tau_internal = tau[0], tau_n = tau[1];
  auto coef = beta*pow(h_local(),n);
  form  aht = integrate (dot(sigma_t, tau_internal)
                 + on_local_sides (dot(sigma_t,normal())*tau_n));
  field lht = integrate(dot(sigma_h, tau_internal)
                 + on_local_sides((dot(sigma_h,normal())
                                  + coef*(lambda_h - uh))*tau_n));
  field sigma_ht (Tht);
  problem p (aht);
  p.solve (lht, sigma_ht);
  dout << catchmark("n")      << n << endl
       << catchmark("beta")   << beta << endl
       << catchmark("u")      << uh
       << catchmark("lambda") << lambda_h
       << catchmark("sigma")  << sigma_h
       << catchmark("sigmat") << sigma_ht;
}
