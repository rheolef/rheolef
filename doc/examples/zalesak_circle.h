///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile zalesak_circle.h The Zalesak full disk benchmark -- the exact solution
struct u {
  point operator() (const point& x) const {
    return point(-0.5*(x[1]-yc), 0.5*(x[0]-xc));
  }
  static Float period() {
    Float pi = acos (Float(-1));
    return 4*pi;
  }
  u() {}
protected:
  static constexpr Float
    xc = 0.5,  // rotation center
    yc = 0.5;
};
struct phi_exact {
  Float operator() (const point& x) const {
    Float xm = xr + cos(0.5*t)*(x[0]-xr) + sin(0.5*t)*(x[1]-yr);
    Float ym = yr - sin(0.5*t)*(x[0]-xr) + cos(0.5*t)*(x[1]-yr);
    return sqrt(sqr(xm-x0) + sqr(ym-y0)) - r;
  }
  Float perimeter() const { return 2*acos(Float(-1))*r; }
  phi_exact (float t1) : t(t1) {}
protected:
  Float t;
  static constexpr Float xr = 0.5,  // rotation center
                         yr = 0.5,
                         x0 = 0.50, // circle center
                         y0 = 0.75,
                         r  = 0.15;  // circle radius
};
struct phi0 {
  Float operator() (const point& x) const { return _phi(x); }
  phi0() : _phi(0) {}
  phi_exact _phi;
};
