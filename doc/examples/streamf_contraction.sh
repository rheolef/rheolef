#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${TOP_SRCDIR}/main/tst"
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
ROUNDER="$BINDIR/field - -field -round"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0
# --------------------------------------------------------------------
# run tests on abrupt contraction: axi case
# --------------------------------------------------------------------
# psi_max=0.00128681006039726	with contraction-small-zr.geo
# psi_max=0.00183864440058968	with contraction-zr.geo

loop_mpirun "./stokes_contraction ${SRCDIR}/contraction-small-zr 2>/dev/null | RHEOPATH=${SRCDIR} ./streamf_contraction 2>/dev/null | RHEOPATH=${SRCDIR} $ROUNDER 1e-5 2>/dev/null | diff ${SRCDIR}/streamf-contraction-small-zr.field - >/dev/null"
if test $? -ne 0; then status=1; fi

exit $status
