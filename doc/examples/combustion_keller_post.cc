///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile combustion_keller_post.cc The combustion problem by Keller continuation -- post-treatment
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "combustion.h"
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  string metric;
  din >> catchmark("metric") >> metric;
  keller<combustion> F (combustion(), metric);
  keller<combustion>::value_type xh, dot_xh;
  dout << noverbose
       << setprecision(numeric_limits<Float>::digits10)
       << "# metric " << metric << endl
       << "# s lambda umax det(mantissa,base,exp) |u| |grad(u)| |residue|"
       << endl;
  for (size_t n = 0; F.get(din,xh); ++n) {
    problem::determinant_type det;
    if (n > 0 || metric != "spherical") det = F.update_derivative(xh);
    const space& Xh = xh.second.get_space();	
    trial u (Xh); test v (Xh);
    form a = integrate(dot(grad(u),grad(v))),
         m = integrate(u*v);
    const combustion& F0 = F.get_problem();
    field mrh = F0.residue(xh.second);
    dout << F.parameter() << " " << xh.first
         << " " << xh.second.max_abs()
         << " " << det.mantissa 
         << " " << det.base 
         << " " << det.exponant 
         << " " << sqrt(m(xh.second,xh.second))
         << " " << sqrt(a(xh.second,xh.second))
         << " " << sqrt(F0.dual_space_dot (mrh,mrh))
         << endl;
    dot_xh = F.direction (xh);
    F.refresh (F.parameter(), xh, dot_xh);
  }
}
