///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "eta.h"
void usage(string name) {
  derr << name << ": usage: "
       << name << " "
       << "[-|file[.field[.gz]]] "
       << "[-check] "
       << "[-residue] "
       << "[-criteria] "
       << endl;
  exit (1);
}
field residue (Float p, const field& uh) {
  const space& Xh = uh.get_space();
  trial u (Xh); test v (Xh);
  field lh = integrate (v);
  form  m  = integrate (u*v);
  form a  = integrate (compose (eta(p), norm2(grad(uh)))*dot(grad(u),grad(v)));
  field mrh = a*uh - lh;
  mrh["boundary"] = 0;
  field rh (Xh);
  problem pm (m);
  pm.solve(mrh, rh);
  rh["boundary"] = 0;
  return rh;
}
void check (Float p, const field& uh) {
  field rh = residue(p, uh);
  const space& Xh = rh.get_space();
  trial u (Xh); test v (Xh);
  form m = integrate (u*v);
  Float res = sqrt(m(rh,rh));
  derr << "check: residue = " << res << endl;
  check_macro (res < 1e-3, "unexpected residue");
}
field criteria (Float p, const field& uh) {
  const space& Xh = uh.get_space();
  if (uh.get_approx() == "P1") return lazy_interpolate (Xh, abs(uh));
  return lazy_interpolate (Xh, pow(norm(grad(uh)), p/2));
}
void
load (idiststream& in, Float& p, field& uh) {
    in >> catchmark("p")     >> p
       >> catchmark("u")     >> uh;
}
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  if (argc < 3) usage(argv[0]);
  Float p;
  field uh;
  if (string (argv[1]) == "-") {
    load (din, p, uh);
  } else {
    idiststream in;
    in.open(argv[1], "field");
    load (in, p, uh);
  }
  for (int i = 2; i < argc; i++) {
         if (strcmp (argv[i], "-check") == 0) { check (p, uh); }
    else if (strcmp (argv[i], "-residue") == 0) { dout << catchmark("ru")   << residue (p, uh); }
    else if (strcmp (argv[i], "-criteria") == 0) { dout << catchmark("c")   << criteria (p, uh); }
    else {
       derr << argv[0] << ": unknown option \'" << argv[i] << "'" << endl;
       usage(argv[0]);
    }
  }
}
