///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile combustion_continuation.cc The combustion problem by continuation
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "combustion.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  cin >> noverbose;
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1";
  Float eps = numeric_limits<Float>::epsilon();
  continuation_option opts;
  opts.ini_delta_parameter = 0.1;
  opts.max_delta_parameter = 1; 
  opts.min_delta_parameter = 1e-7; 
  opts.tol              = eps;
  derr << setprecision(numeric_limits<Float>::digits10)
       << "# continuation in lambda:" << endl
       << "# geo          = " << omega.name() << endl
       << "# approx       = " << approx << endl
       << "# dlambda_ini  = " << opts.ini_delta_parameter << endl
       << "# dlambda_min  = " << opts.min_delta_parameter << endl
       << "# dlambda_max  = " << opts.max_delta_parameter << endl
       << "# tol          = " << opts.tol << endl;
  combustion F (omega, approx);
  field uh = F.initial();
  F.put (dout, uh);
  continuation (F, uh, &dout, &derr, opts);
}
