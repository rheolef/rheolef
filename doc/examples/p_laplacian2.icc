///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile p_laplacian2.icc The p-Laplacian problem by the Newton method -- class body
field p_laplacian::derivative_trans_mult (const field& mrh) const {
  field rh (Xh, 0);
  pm.solve (mrh, rh);
  field mgh = a1*rh;
  mgh.set_b() = 0;
  return mgh;
}
Float p_laplacian::space_norm (const field& uh) const {
  return sqrt (m(uh,uh));
}
Float p_laplacian::dual_space_norm (const field& mrh) const {
  field rh (Xh, 0);
  pm.solve (mrh, rh);
  return sqrt (dual(mrh, rh));
}
