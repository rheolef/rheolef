#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR=${SBINDIR-"../../main/sbin"}
BINDIR=${BINDIR-"../../main/bin"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

# BUG_DEBIAN_MUMPS_BUSTER : declared in debian/buster for mumps-ptscocth segfault
#echo "      not yet (skipped)"
#exit 0

status=0

if test x"`bash ../../config/rheolef-config --float`" != x"float128"; then
  n=80
  # geo          		approx  nu    t0    	alpha	tolerance (err_linf)
  L="
  mesh-2d-t		P0	3     0		1	2e-1
  mesh-2d-t		P1d	3     0         1	2e-2
  mesh-2d-t		P2d	3     0         1	5e-4
  mesh-2d-t		P0	3     0.7854    1	7e-3
  mesh-2d-t		P1d	3     0.7854    1	5e-4
  mesh-2d-t		P2d	3     0.7854    1	2e-5
  "
else
  n=15; # too much duration, otherwise
  # geo          	approx  nu    t0    	alpha	tolerance (err_linf)
  L="
  mesh-2d-t		P0	3     0		1	2e-1
  mesh-2d-t		P1d	3     0         1	6e-3
  mesh-2d-t		P2d	3     0         1	9e-4
  mesh-2d-t		P0	3     0.7854    1	3e-3
  mesh-2d-t		P1d	3     0.7854    1	6e-4
  mesh-2d-t		P2d	3     0.7854    1	9e-5
  "
fi

run "${SBINDIR}/mkgeo_grid_2d -v4 -t $n -a -0.5 -b 0.5 -c -0.5 -d 0.5 -boundary  2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-2d-t.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  approx=`echo $L | gawk '{print $2}'`
  nu=`echo $L | gawk '{print $3}'`
  t0=`echo $L | gawk '{print $4}'`
  alpha=`echo $L | gawk '{print $5}'`
  tol=`echo $L | gawk '{print $6}'`
  L=`echo $L | gawk '{for (i=7; i <= NF; i++) print $i}'`
  loop_mpirun "./transport_tensor_dg $geo $approx $alpha $nu $t0 2>/dev/null | \$RUN ./transport_tensor_error_dg $tol >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done
run "rm -f mesh-1d.geo mesh-2d-t.geo mesh-2d-q.geo"
exit $status
