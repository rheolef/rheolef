///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile contraction.h The contraction geometry: boundary conditions
struct contraction {
  struct base {
     base (geo omega) : c(0), umax(0), cartesian(true) {
       c = omega.xmax()[1];
       string sys_coord = omega.coordinate_system_name();
       cartesian = (sys_coord == "cartesian");
       umax = cartesian ? 3/(2*c) : 4/sqr(c);
     }
     Float c, umax;
     bool cartesian;
  };
  struct u_upstream: base {
    u_upstream (geo omega) : base(omega) {}
    Float operator() (const point& x) const {
      return base::umax*(1-sqr(x[1]/base::c)); }
  };
  static space velocity_space (geo omega, string approx) {
    space Xh (omega, approx, "vector");
    Xh.block ("wall");
    Xh.block ("upstream");
    Xh[1].block ("axis");
    Xh[1].block ("downstream");
    return Xh;
  }
  static field velocity_field (space Xh) {
    geo omega = Xh.get_geo();
    string approx = "P" + to_string(Xh.degree());
    space Wh  (omega["upstream"], approx);
    field uh (Xh, 0.);
    uh[0]["upstream"] = lazy_interpolate (Wh, u_upstream(omega));
    return uh;
  }
  static space streamf_space (geo omega, string approx) {
    space Ph (omega, approx);
    Ph.block("upstream");
    Ph.block("wall");
    Ph.block("axis");
    return Ph;
  }
  struct psi_upstream: base {
    psi_upstream (geo omega) : base(omega) {}
    Float operator() (const point& x) const {
      Float y = (x[1]/base::c);
      if (base::cartesian) {
        return  (base::umax*base::c)*(y*(1-sqr(y)/3) - 2./3);
      } else {
        return 0.5*(base::umax*sqr(base::c))*(sqr(y)*(1-sqr(y)/2) - 0.5);
      }
    }
  };
  static field streamf_field (space Ph) {
    geo omega = Ph.get_geo();
    space Wh (omega["upstream"], Ph.get_approx());
    field psi_h (Ph, 0);
    psi_upstream psi_up (omega);
    psi_h["upstream"] = lazy_interpolate (Wh, psi_up);
    psi_h["wall"] =  0;
    psi_h["axis"] = -1;
    return psi_h;
  }
};
