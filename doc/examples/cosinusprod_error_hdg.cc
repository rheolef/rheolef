///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile cosinusprod_error_hdg.cc The sinus product function -- error analysis for the hybrid discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "cosinusprod.h"
#include "cosinusprod_grad.h"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  Float err_linf_expected = (argc > 1) ? atof(argv[1]) : 1e+38;
  bool with_u     =  (argc <= 2) || argv[2] != string("-us");
  bool with_sigma = ((argc <= 2) || argv[2] != string("-no-sigma")) && with_u;
  field uh, lambda_h, sigma_h;
  if (with_u) {
    din >> catchmark("u")  >> uh; // hdg or hho
  } else {
    din >> catchmark("us") >> uh; // hho
  }
  din >> catchmark("lambda") >> lambda_h;
  if (with_sigma) {
    din >> catchmark("sigma") >> sigma_h;
  }
  space Xh = uh.get_space();
  geo omega = Xh.get_geo();
  size_t k = Xh.degree();
  size_t d = omega.dimension();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(3*(k+1)+4);
  Float err_u_l2 = sqrt(integrate (omega, sqr(uh-u_exact(d)), iopt));
  string opts = Xh.get_basis().option().stamp();
  space Xh1 (omega, "P"+to_string(k+1)+"d"+opts);
  field euh = lazy_interpolate (Xh1, uh-u_exact(d));
  Float err_u_linf = euh.max_abs();
  Float err_u_h1 = sqrt(integrate (omega, norm2(grad_h(euh)), iopt)
                    + integrate (omega.sides(), (1/h_local())*sqr(jump(euh)), iopt));
  derr << "err_u_l2   = " << err_u_l2 << endl
       << "err_u_linf = " << err_u_linf << endl
       << "err_u_h1   = " << err_u_h1 << endl;
  if (with_sigma) {
    Float err_sigma_l2 = sqrt(integrate (omega, norm2(sigma_h-grad_u(d)), iopt));
    space Th1 (omega, "P"+to_string(k+1)+"d"+opts, "vector");
    field esh = lazy_interpolate (Th1, sigma_h-grad_u(d));
    Float err_sigma_linf = esh.max_abs();
    derr << "err_sigma_l2   = " << err_sigma_l2 << endl
         << "err_sigma_linf = " << err_sigma_linf << endl;
  }
  if (!lambda_h.get_space().get_basis().option().is_trace_n()) {
    space Mh = lambda_h.get_space();
    trial lambda(Mh); test mu(Mh);
    form ms = integrate(lambda*mu);
    field kh = integrate(u_exact(d)*mu, iopt);
    field ph_lambda(Mh);
    problem pms (ms);
    pms.solve (kh, ph_lambda);
    Float err_lambda_l2 = sqrt(integrate (omega["sides"], h_local()*sqr(lambda_h-ph_lambda), iopt));
    derr << "err_lambda_l2   = " << err_lambda_l2 << endl;
  }
  return (err_u_linf <= err_linf_expected) ? 0 : 1;
}
