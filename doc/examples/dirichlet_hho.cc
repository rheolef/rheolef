///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile dirichlet_hho.cc The Poisson problem by the hybrid high order method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod_dirichlet.h"
#include "diffusion_isotropic.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string Pkd = (argc > 2) ? argv[2] : "P1d",
         Pld = (argc > 3) ? argv[3] :  Pkd;
  space  Xh (omega, Pld),
         Mh (omega["sides"], Pkd);
  Mh.block("boundary");
  size_t k = Mh.degree(), l = Xh.degree(), dim = omega.dimension();
  Float beta  = (argc > 4) ? atof(argv[4]) : 10*(k+1)*(k+dim)/Float(dim);
  check_macro(l == k-1 || l == k || l == k+1, "invalid (k,l)");
  space Xhs(omega, "P"+to_string(k+1)+"d"),
        Zh (omega, "P0"),
        Mht(omega, "trace_n(RT"+to_string(max(k,l))+"d)");
  trial us(Xhs), u(Xh), zeta(Zh), deltat(Mht), lambda(Mh);
  test  ws(Xhs), w(Xh),   xi(Zh),   phit(Mht),     mu(Mh);
  auto lh = lazy_integrate (f(dim)*w);
  auto m  = lazy_integrate (u*w);
  auto as = lazy_integrate (dot(grad_h(us),A(dim)*grad_h(ws)));
  auto cs = lazy_integrate (pow(h_local(),2)*zeta*xi);
  auto mt = lazy_integrate (on_local_sides(deltat*phit));
  auto ct = lazy_integrate (on_local_sides(beta*pow(h_local(),-1)*deltat*phit));
  auto bs = lazy_integrate (us*xi);
  auto d  = lazy_integrate (u*xi);
  auto ds = lazy_integrate (us*w);
  auto dt = lazy_integrate (on_local_sides(u*phit));
  auto dst= lazy_integrate (on_local_sides(us*phit));
  auto ac = lazy_integrate (dot(grad_h(u),A(dim)*grad_h(ws))
                - on_local_sides(u*dot(A(dim)*grad_h(ws),normal())));
  auto et = lazy_integrate (on_local_sides(mu*deltat));
  auto es = lazy_integrate (on_local_sides(mu*dot(A(dim)*grad_h(us),normal())));
  auto inv_cs = inv(cs);
  auto inv_Ss = inv(as + trans(bs)*inv_cs*bs);
  auto inv_T  = inv(as*inv_Ss*as + trans(bs)*inv_cs*bs);
  auto R = as*inv_Ss*trans(bs)*inv_cs*d - ac;
  auto Ac  = trans(R)*inv_T*R;
  auto D = ct*inv(mt)*(dst - dt*inv(m)*ds);
  auto M0 = inv_Ss - inv_Ss*as*inv_T*as*inv_Ss;
  auto inv_M = inv(ct + D*M0*trans(D));
  auto E = trans(dt)*inv(mt)*ct
         + trans(ac)*inv_T*as*inv_Ss*trans(D)
         + trans(d)*inv_cs*bs*M0*trans(D);
  auto As = E*inv_M*trans(E);
  auto inv_A = inv(Ac + As);
  auto F = es*inv_T*as*inv_Ss*trans(D)
         - et*inv(mt)*ct;
  auto C = es*inv_T*trans(es) + F*inv_M*trans(F);
  auto B = F*inv_M*trans(E) - es*inv_T*R;
  form S = C - B*inv_A*trans(B);
  problem pS (S);
  field rhs = -B*(inv_A*lh);
  field lambda_h(Mh, 0);
  pS.solve (rhs, lambda_h);
  auto uh = inv_A*(lh - B.trans_mult(lambda_h));
  auto deltat_h = inv_M*(E.trans_mult(uh) + F.trans_mult(lambda_h));
  auto vs_h = inv_T*(-as*inv_Ss*D.trans_mult(deltat_h) + R*uh - es.trans_mult(lambda_h));
  field us_h = inv_Ss*(-as*vs_h - D.trans_mult(deltat_h) + trans(bs)*inv_cs*d*uh);
  dout << catchmark("us")     << us_h
       << catchmark("u")      << field(uh)
       << catchmark("lambda") << lambda_h;
}
