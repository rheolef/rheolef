///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile sinusrad_laplace.h The sinus radius function -- right-hand-side and boundary condition for the Poisson problem with Neumann boundary condition
struct f {
  Float operator() (const point& x) const {
    Float r = sqrt(sqr(x[0])+sqr(x[1])+sqr(x[2]));
    return (n == 2) ? 4 : n*n*pow(r,n-2);
  }
  f(size_t=0) : n(2) {}
  size_t n;
};
struct g {
  Float operator() (const point& x) const { 
    Float r = sqrt(sqr(x[0])+sqr(x[1])+sqr(x[2]));
    return 1-pow(r,n);
  }
  g(size_t=0) : n(2) {}
  size_t n;
};
