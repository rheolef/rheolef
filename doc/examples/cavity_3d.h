///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile cavity_3d.h The driven cavity benchmark: 3D boundary conditions
struct cavity {
  static space velocity_space (const geo& omega, string approx) {
    space Xh (omega, approx, "vector");
    Xh.block("top");  Xh.block("bottom");
    Xh.block("left"); Xh.block("right");
    if (omega.dimension() == 3) {
      Xh.block("back"); Xh.block("front");
    }
    return Xh;
  }
  static field velocity_field (const space& Xh, Float alpha=1) {
    field uh (Xh, 0.);
    uh[0]["top"] = alpha;
    return uh;
  }
  static space streamf_space (geo omega, string approx) {
    string valued = (omega.dimension() == 3) ? "vector" : "scalar";
    space Ph (omega, approx, valued);
    Ph.block("top");  Ph.block("bottom");
    Ph.block("left"); Ph.block("right");
    if (omega.dimension() == 3) {
      Ph.block("back"); Ph.block("front");
    }
    return Ph;
  }
  static field streamf_field (space Ph) {
    return field(Ph, 0);
  }
};
