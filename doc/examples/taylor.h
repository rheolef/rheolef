///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile taylor.h The Taylor benchmark -- right-hand-side and boundary condition
struct g {
  point operator() (const point& x) const { 
    return point(-cos(pi*x[0])*sin(pi*x[1]),
                  sin(pi*x[0])*cos(pi*x[1])); }
  g() : pi(acos(Float(-1.0))) {}
  const Float pi;
};
struct f {
  point operator() (const point& x) const { return 2*sqr(pi)*_g(x); }
  f() : pi(acos(Float(-1.0))), _g() {}
  const Float pi; g _g;
};
