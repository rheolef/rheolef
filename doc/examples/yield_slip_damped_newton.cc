///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile yield_slip_damped_newton.cc The yield slip problem by the damped Neton method
#include "rheolef.h"
using namespace std;
using namespace rheolef;
#include "yield_slip.h"
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  geo omega (argv[1]);
  string approx  = (argc > 2) ?      argv[2]  : "P1";
  Float  S       = (argc > 3) ? atof(argv[3]) : 0.6;
  Float  n       = (argc > 4) ? atof(argv[4]) : 1;
  Float  Cf      = (argc > 5) ? atof(argv[5]) : 1;
  Float  r = (argc > 6) ? atof(argv[6]) : 1;
  domain boundary = omega["boundary"];
  yield_slip F (S, n, Cf, r, omega, boundary, approx);
  field beta_h = F.initial();
  Float tol = 10*numeric_limits<Float>::epsilon();
  size_t max_iter = 10000;
  int status = damped_newton (F, beta_h, tol, max_iter, &derr);
  field uh, lambda_h;
  F.post (beta_h, uh, lambda_h);
  dout << setprecision(numeric_limits<Float>::digits10)
       << catchmark("S") << S << endl
       << catchmark("n") << n << endl
       << catchmark("Cf") << Cf << endl
       << catchmark("r") << r << endl
       << catchmark("u") << uh
       << catchmark("lambda") << lambda_h;
  return status;
}
