///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile combustion.h The combustion problem: class header for the Newton method
struct combustion {
  typedef Float float_type;
  typedef field value_type;
  combustion(const geo& omega=geo(), string approx="");
  void reset(const geo& omega, string approx);
  field initial (std::string restart="");
  idiststream& get (idiststream& is, field& uh);
  odiststream& put (odiststream& os, const field& uh) const;
  string parameter_name() const { return "lambda"; }
  float_type parameter() const { return lambda; }
  void set_parameter(float_type lambda1) { lambda = lambda1; }
  bool stop (const field& xh) const { return xh.max_abs() > 10; }
  field residue (const field& uh) const;
  form derivative (const field& uh) const;
  field derivative_versus_parameter (const field& uh) const;
  problem::determinant_type update_derivative (const field& uh) const;
  field derivative_solve (const field& mrh) const;
  field derivative_trans_mult (const field& mrh) const;
  field massify   (const field& uh) const { return m*uh; }
  field unmassify (const field& uh) const;
  float_type space_dot  (const field& xh, const field& yh) const;
  float_type dual_space_dot  (const field& mrh, const field& msh) const;
protected:
  float_type lambda;
  space Xh;
  form m;
  problem pm;
  mutable form a1;
  mutable problem pa1;
  mutable branch event;
};
#include "combustion1.icc"
#include "combustion2.icc"
