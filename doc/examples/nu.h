///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile nu.h The p-Laplacian problem -- the nu function
template<class Function>
struct nu {
  tensor operator() (const point& grad_u) const {
    Float x2 = norm2 (grad_u);
    Float a = f(x2);
    Float b = 2*f.derivative(x2);
    tensor value;
    for (size_t i = 0; i < d; i++) {
      value(i,i)   = a + b*grad_u[i]*grad_u[i];
      for (size_t j = 0; j < i; j++)
        value(j,i) = value(i,j) = b*grad_u[i]*grad_u[j];
    }
    return value;
  }
  nu (const Function& f1, size_t d1) : f(f1), d(d1) {}
  Function f;
  size_t   d;
};
