///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile stokes_taylor_dg.cc The Stokes problem for the Taylor benchmark by the discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "taylor.h"
#include "stokes_dirichlet_dg.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2], "vector");
  space Qh (omega, argv[2]);
  form a, b, c, mp;
  field lh, kh;
  stokes_dirichlet_dg (Xh, Qh, a, b, c, mp, lh, kh);
  field uh (Xh, 0), ph (Qh, 0);
  problem_mixed stokes (a, b, c);
  stokes.set_metric (mp);
  stokes.solve (lh, kh, uh, ph);
  dout << catchmark("u")  << uh
       << catchmark("p")  << ph;
}
