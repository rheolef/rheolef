#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR=${TOP_SRCDIR}/main/tst
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"2"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skipped)"
#exit 0

status=0
run "$SBINDIR/mkgeo_grid_1d -e 1000 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-1d.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "$SBINDIR/mkgeo_grid_2d -t   10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-t.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "$SBINDIR/mkgeo_grid_2d -q   10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-q.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "$SBINDIR/mkgeo_grid_3d -T    5 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-3d-T.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

# geo          		approx  sigma alpha         tolerance (err_linf)
L="
mesh-1d			P0	300   0		4e-4
mesh-1d			P0	300   1		4e-3
mesh-1d			P1d	300   0		9e-4
mesh-1d			P1d	300   1		2e-4
mesh-1d			P2d	300   0		5e-7
mesh-1d			P2d	300   1		4e-6
mesh-2d-t		P0	3     0		2e-2
mesh-2d-t		P0	3     1		2e-2
mesh-2d-t		P1d	3     0		3e-3
mesh-2d-t		P1d	3     1		2e-3
mesh-2d-t		P2d	3     0		4e-5
mesh-2d-t		P2d	3     1		4e-5
mesh-2d-q		P0	3     0		5e-3
mesh-2d-q		P0	3     1		4e-2
mesh-2d-q		P1d	3     0		9e-3
mesh-2d-q		P1d	3     1		2e-3
mesh-2d-q		P2d	3     0		6e-6
mesh-2d-q		P2d	3     1		4e-5
mesh-3d-T		P0	3     0		2e-2
$GEODIR/square_tq_bamg	P0	3     0		3e-2
$GEODIR/square_tq_bamg	P0	3     1		3e-2
$GEODIR/square_tq_bamg	P1d	3     0		6e-3
$GEODIR/square_tq_bamg	P1d	3     1		2e-3
$GEODIR/square_tq_bamg	P2d	3     0		6e-5
$GEODIR/square_tq_bamg	P2d	3     1		4e-5
"

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  approx=`echo $L | gawk '{print $2}'`
  sigma=`echo $L | gawk '{print $3}'`
  alpha=`echo $L | gawk '{print $4}'`
  tol=`echo $L | gawk '{print $5}'`
  L=`echo $L | gawk '{for (i=6; i <= NF; i++) print $i}'`
  loop_mpirun "./transport_dg $geo $approx $alpha $sigma 2>/dev/null | RHEOPATH=.:$GEODIR \$RUN ./transport_dg_error $tol >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done
run "rm -f mesh-1d.geo mesh-2d-t.geo mesh-2d-q.geo"
exit $status
