///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile oldroyd_cavity.h The Oldroyd problem on the driven cavity benchmark -- boundary conditions
#include "cavity.h"
struct oldroyd_cavity: cavity {
  static Float u0_top (const point& x) { return 16*sqr(x[0])*sqr(1-x[0]); }
  static field velocity_field (const space& Xh) {
    geo omega = Xh.get_geo();
    space Wh (omega["top"], Xh.get_approx());
    field uh (Xh, 0.);
    uh[0]["top"] = lazy_interpolate (Wh, u0_top);
    return uh;
  }
  struct tau_upstream {
    tau_upstream (geo, Float, Float) {}
    tensor operator() (const point& x) const { return tensor(); }
  };
};
