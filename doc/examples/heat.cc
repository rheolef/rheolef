///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile heat.cc The heat equation
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  size_t n_max = (argc > 2) ? atoi(argv[2]) : 100;
  Float delta_t = 0.5/n_max;
  space Xh (omega, "P1");
  Xh.block ("boundary");
  trial u (Xh); test v (Xh);
  form a = integrate (u*v + delta_t*dot(grad(u),grad(v)));
  problem p (a);
  field uh (Xh, 0);
  branch event ("t","u");
  dout << event (0, uh);
  for (size_t n = 1; n <= n_max; n++) {
    field rhs = uh + delta_t;
    field lh = integrate (rhs*v);
    p.solve (lh, uh);
    dout << event (n*delta_t, uh);
  }
}
