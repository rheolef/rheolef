#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR=${GEODIR-"$TOP_SRCDIR/main/tst"}
SBINDIR=${SBINDIR-"../../main/sbin"}
BINDIR=${BINDIR-"../../main/bin"}
NPROC_MAX=${NPROC_MAX-"1"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

# geo                   err_P1  	err_P2		err_P3
#			tol_u tol_s	tol_u tol_s	tol_u tol_s
L="
circle-10-fix           7e-3  7e-2  	4e-3  8e-3
"

#BUG_A_BTB : Pk, k>=3 the solution depends upon r and error increases with r
L_TODO="
circle-10-fix           7e-3  7e-2  	4e-3  8e-3	9e-4 5e-3
"

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2; do
    tol_u=`echo $L | gawk '{print $1}'`
    tol_s=`echo $L | gawk '{print $2}'`
    L=`echo $L | gawk '{for (i=3; i <= NF; i++) print $i}'`
    loop_mpirun "./mosolov_augmented_lagrangian $GEODIR/$geo-$Pk $Pk 0.2 1 2>/dev/null && gzip -d < $geo-$Pk.field.gz | RHEOPATH=$GEODIR \$RUN ./mosolov_error $tol_u $tol_s 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
    run "rm -f $geo-$Pk.field.gz"
  done
done

exit $status
