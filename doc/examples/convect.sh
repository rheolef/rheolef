#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR="${TOP_SRCDIR}/doc/examples"
GEODIR="${TOP_SRCDIR}/main/tst"
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

run "${SBINDIR}/mkgeo_grid_1d 500 -a -4 -b 4 -v4 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-1d.geo 2>/dev/null"
run "${SBINDIR}/mkgeo_grid_2d  20 -a -2 -b 2 -c -2 -d 2 -v4 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-2d.geo 2>/dev/null"
run "${SBINDIR}/mkgeo_grid_3d   9 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2 -v4 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-3d.geo 2>/dev/null"

loop_mpirun "./convect mesh-1d.geo > tmp.branch 2>/dev/null && \$RUN ./convect_error 1.6e-3 < tmp.branch >/dev/null 2>/dev/null"
if test $? -ne 0; then status=1;  fi

loop_mpirun "./convect mesh-2d.geo > tmp.branch 2>/dev/null && \$RUN ./convect_error 0.33 >/dev/null < tmp.branch 2>/dev/null"
if test $? -ne 0; then status=1;  fi

# TODO:
# loop_mpirun "./convect mesh-3d.geo > tmp.branch 2>/dev/null && \$RUN ./convect_error 0.49 < tmp.branch >/dev/null 2>/dev/null"
# if test $? -ne 0; then status=1;  fi

run "/bin/rm -f mesh-1d.geo mesh-2d.geo mesh-3d.geo tmp.log tmp.branch"

exit $status
