///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile convect_hdg_error.cc Convection-diffusion equation by the hybrid discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "rotating-hill-statio.h"
int main (int argc, char **argv) {
  environment rheolef (argc,argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e+38;
  bool dump = (argc > 2);
  Float nu;
  field phi_h;
  din >> catchmark("nu")     >> nu
      >> catchmark("phi")    >> phi_h;
  space Xh = phi_h.get_space();
  geo omega = phi_h.get_geo();
  size_t d = Xh.get_geo().dimension();
  size_t k = Xh.degree();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(2*k+1);
  Float err_l2 = sqrt(integrate (omega, sqr(phi_h-phi(d,nu)), iopt));
  basis b1 = Xh.get_basis();
  b1.reset_family_index (k+1);
  space Xh1 (omega, b1);
  field eh = lazy_interpolate (Xh1, phi_h-phi(d,nu));
  Float err_linf = eh.max_abs();
  Float err_h1 = sqrt(integrate (omega, norm2(grad_h(eh)), iopt)
                    + integrate (omega.sides(), (1/h_local())*sqr(jump(eh)), iopt));
  derr << "err_l2   = " << err_l2 << endl
       << "err_linf = " << err_linf << endl
       << "err_h1   = " << err_h1 << endl;
  if (dump) {
    field pi_h_phi = lazy_interpolate (Xh, phi(d,nu));
    dout << catchmark("phi") << phi_h
         << catchmark("phi_e") << pi_h_phi;
  }
  return (err_linf <= tol) ? 0 : 1;
}
