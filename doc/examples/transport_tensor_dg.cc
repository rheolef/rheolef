///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile transport_tensor_dg.cc The tensorial transport benchmark by the discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "transport_tensor_exact.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2], "tensor");
  Float alpha = (argc > 3) ? atof(argv[3]) : 1;
  Float nu    = (argc > 4) ? atof(argv[4]) : 3;
  Float t0    = (argc > 5) ? atof(argv[5]) : acos(-1.)/8;
  Float a = 0;
  trial sigma (Xh); test tau (Xh);
  tensor ma = 0.5*((1-a)*grad_u - (1+a)*trans(grad_u));
  auto beta_a = sigma*ma + trans(ma)*sigma;
  form  ah = integrate (ddot(grad_h(sigma)*u + beta_a + nu*sigma,tau))
           + integrate ("boundary",
		max(0, -dot(u,normal()))*ddot(sigma,tau))
           + integrate ("internal_sides", 
	 	- dot(u,normal())*ddot(jump(sigma),average(tau))
                + 0.5*alpha*abs(dot(u,normal()))
                           *ddot(jump(sigma),jump(tau)));
  field lh = integrate (ddot(chi(nu,t0),tau))
           + integrate ("boundary", 
                max(0, -dot(u,normal()))*ddot(sigma_g(nu,t0),tau));
  field sigma_h(Xh);
  problem p (ah);
  p.solve (lh, sigma_h);
  dout << catchmark("nu")    << nu << endl
       << catchmark("t0")    << t0 << endl
       << catchmark("sigma") << sigma_h;
}
