#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR=${SBINDIR-"../../main/sbin"}
BINDIR=${BINDIR-"../../main/bin"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test "`bash ../../config/rheolef-config --float`" != "double"; then
   echo "      not yet (skipped, since unsym direct solver not available for QD arithmetic)"
   exit 0
fi

status=0

# TODO: check P2
err_h1="2.9"

run "$SBINDIR/mkgeo_grid_1d -e 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-1d.geo 2>/dev/null"

for metric in orthogonal spherical; do
  loop_mpirun "./combustion_keller mesh-1d.geo P1 $metric > mesh-1d.branch 2>/dev/null ; i=\`$BINDIR/branch -toc mesh-1d 2>/dev/null | tail -1 | gawk '{print \$1}'\` && $BINDIR/branch -extract \$i mesh-1d -branch 2>/dev/null | \$RUN ./combustion_error 1 $err_h1 2>/dev/null >/dev/null"
  if test $? -ne 0; then status=1; fi
done

run "rm -f mesh-1d.geo mesh-1d.branch"

exit $status
