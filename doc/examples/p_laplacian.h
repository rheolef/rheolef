///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile p_laplacian.h The p-Laplacian problem by the Newton method -- class header
class p_laplacian {
public:
  typedef field value_type;
  typedef Float float_type;
  p_laplacian (Float p, const geo& omega, string approx);
  field initial() const;
  field residue          (const field& uh) const;
  void update_derivative (const field& uh) const;
  field derivative_solve      (const field& mrh) const;
  field derivative_trans_mult (const field& mrh) const;
  Float space_norm       (const field& uh) const;
  Float dual_space_norm  (const field& mrh) const;
  Float p;
  space Xh;
  field lh;
  form  m;
  problem pm;
  mutable form a1;
  mutable problem pa1;
};
#include "p_laplacian1.icc"
#include "p_laplacian2.icc"
