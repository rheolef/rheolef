///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "harten.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float err_expected = (argc > 1) ? atof(argv[1]) : 1;
  branch even("t","u");
  Float t=0; field uh;
  Float err_linf_l1 = 0;
  dout << "# t err_l1(t)" << endl;
  while (din >> even(t,uh)) {
    const geo& omega = uh.get_geo();
    integrate_option iopt;
    iopt.set_order (2*uh.get_space().degree()+1);
    Float err_l1 = integrate (omega, fabs(uh - harten(t)), iopt);
    err_linf_l1 = max(err_linf_l1, err_l1);
    dout << t << " " << err_l1 << endl;
  }
  dout << "# err_linf_l1 = " << err_linf_l1 << endl;
  return (err_linf_l1 <= err_expected) ? 0 : 1;
}
