///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile transmission-mixed.cc The transmission problem -- mixed formulation
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  const Float epsilon = 0.01;
  geo omega (argv[1]);
  space Xh (omega, "P1");
  Xh.block ("left");
  Xh.block ("right");
  space Qh (omega, "P0", "vector");
  test v (Xh);
  field eta (Qh);
  eta ["east"] = 1;
  eta ["west"] = epsilon;
  field lh = integrate (v);
  form b = -form(Xh, Qh, "grad"); 
  form inv_m (Qh, Qh, "inv_mass", 1/eta); 
  form a = trans(b)*inv_m*b;
  field uh (Xh, 0.);
  problem p (a);
  p.solve (lh, uh);
  dout << catchmark("epsilon") << epsilon << endl
       << catchmark("u")       << uh;
}
