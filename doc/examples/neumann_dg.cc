///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile neumann_dg.cc The Helmholtz problem with Neumann boundary conditions by the discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod_helmholtz.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  size_t d = omega.dimension();
  size_t k = Xh.degree();
  Float beta = (k+1)*(k+d)/Float(d);
  trial u (Xh); test v (Xh);
  form  a = integrate (u*v + dot(grad_h(u),grad_h(v)))
          + integrate ("internal_sides", 
                  beta*penalty()*jump(u)*jump(v)
	 	- jump(u)*average(dot(grad_h(v),normal()))
	 	- jump(v)*average(dot(grad_h(u),normal())));
  field lh = integrate (f(d)*v) + integrate ("boundary", g(d)*v);
  field uh(Xh);
  problem p (a);
  p.solve (lh, uh);
  dout << uh;
}
