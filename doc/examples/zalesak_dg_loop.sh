#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# batch: run all computations for zalesak test in one loop
# save only the first and last time iteration in a .branch
#
name="zalesak"
verbose="true"
execute="true"

LH="25 50 100 200"
#   => compare with MarRemChe-2006, page 350, table 2
LK="P1d P2d P3d P4d P5d"
#   => p=k+1 <= 6 : almost stable
RUN="mpirun -np 15"
LN="13 25 49 94 187 375 750 1500 3000 6000 12000 24000 48000 96000"
#   => 4*pi/n in [1e-4:1]

if test `hostname` = hawaii; then
  RUN=""
else
  RUN="mpirun -np 14"
fi
rm -f run.LOG
for ih in $LH; do
  if test ! -f ${name}-$ih.geo.gz; then
    command="mkgeo_ugrid -t $ih | gzip -9 > ${name}-$ih.geo.gz"
    $verbose && echo $command | tee -a run.LOG
    $execute && eval $command | tee -a run.LOG
  fi
  for approx in $LK; do
    for n_max in $LN; do
      command="$RUN ./zalesak_dg ${name}-$ih.geo $approx $n_max true | gzip -9 > ${name}-$ih-$approx-$n_max.branch.gz"
      echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" | tee -a run.LOG
      $verbose && echo $command | tee -a run.LOG
      $execute && eval $command | tee -a run.LOG
    done
  done
done

