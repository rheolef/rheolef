#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR=${TOP_SRCDIR}/main/tst
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active due to missing iter&dir unsym solver_abtb)"
  exit 0
fi

status=0
run "$SBINDIR/mkgeo_grid_2d -t 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-t.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "$SBINDIR/mkgeo_grid_2d -q 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-q.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

Re=10
max_iter=3
not_yet=1e38
# geo          		approx  err_u_linf 	err_p_linf
L="
mesh-2d-t		P1d	3e-2		4e-1
mesh-2d-t		P2d	2e-3		4e-2
mesh-2d-t		P3d	6e-5		3e-3
mesh-2d-q		P1d	2e-2		2e-0
mesh-2d-q		P2d	6e-4		5e-1
mesh-2d-q		P3d	2e-5		9e-3
"

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  approx=`echo $L | gawk '{print $2}'`
  tol_u=`echo $L | gawk '{print $3}'`
  tol_p=`echo $L | gawk '{print $4}'`
  L=`echo $L | gawk '{for (i=5; i <= NF; i++) print $i}'`
  loop_mpirun "./navier_stokes_taylor_dg $geo $approx $Re $max_iter 2>/dev/null | RHEOPATH=.:$GEODIR \$RUN ./navier_stokes_taylor_error_dg $tol_u $tol_p >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done
run "rm -f mesh-2d-t.geo mesh-2d-q.geo"
exit $status
