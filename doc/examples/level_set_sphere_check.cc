///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sphere.icc"
int main (int argc, char**argv) {
  environment rheolef (argc,argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e+38;
  geo gamma_h;
  din >> gamma_h;
  space Wh (gamma_h, "P1");
  trial u (Wh); test v (Wh);
  form m  = integrate (u*v);
  field one (Wh, 1);
  Float meas_gamma_h = m(one,one);
  size_t d = gamma_h.dimension();
  Float pi = acos(Float(-1));
  Float meas_gamma = (d == 3) ? 4*pi : 2*pi;
  Float err = fabs(meas_gamma - meas_gamma_h);
  dout << "meas(gamma_h) = " << meas_gamma_h << endl
       << "meas(gamma)   = " << meas_gamma   << endl
       << "err           = " << err          << endl;
  return (err < tol) ? 0 : 1;
}
