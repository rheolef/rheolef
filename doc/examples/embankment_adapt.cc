///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile embankment_adapt.cc The elasticity problem for the embankment geometry with adaptive mesh
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "elasticity_solve.icc"
#include "elasticity_criterion.icc"
#include "embankment.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  const Float lambda = 1;
  geo  omega (argv[1]);
  adapt_option options;
  string approx  = (argc > 2) ? argv[2] : "P1";
  options.err    = (argc > 3) ? atof(argv[3]) : 5e-3;
  size_t n_adapt = (argc > 4) ? atoi(argv[4]) : 5;
  options.hmin   = 0.004;
  for (size_t i = 0; true; i++) {
    space Xh = embankment_space (omega, approx);
    field uh = elasticity_solve (Xh, lambda);
    odiststream of (omega.name(), "field");
    of << catchmark("lambda")  << lambda << endl
       << catchmark("u")       << uh;
    if (i == n_adapt) break;
    field ch = elasticity_criterion (lambda,uh);
    omega = adapt(ch, options);
    odiststream og (omega.name(), "geo");
    og << omega;
  }
}
