///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile cosinusprod_error_hdg_post_rt.cc The sinus product function -- error analysis for the hybrid discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "cosinusprod_dirichlet.h"
#include "cosinusprod_grad.h"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  Float err_linf_expected = (argc > 1) ? atof(argv[1]) : 1e+38;
  field sigmat_h;
  din >> catchmark("sigmat") >> sigmat_h;
  space Tth = sigmat_h.get_space();
  geo omega = Tth.get_geo();
  size_t k = Tth.degree() - 1;
  size_t d = omega.dimension();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(2*(k+1)+2);
  Float err_sigmat_l2 = sqrt(integrate (omega, norm2(sigmat_h-grad_u(d)), iopt));
  space Th1 (omega, "P"+to_string(k+1)+"d", "vector");
  field esth = lazy_interpolate (Th1, sigmat_h-grad_u(d));
  Float err_sigmat_linf = esth.max_abs();
  derr << "err_sigmat_l2   = " << err_sigmat_l2 << endl
       << "err_sigmat_linf = " << err_sigmat_linf << endl;
  Float err_div_sigmat_l2 = sqrt(integrate (omega, sqr(div_h(sigmat_h)+f(d)), iopt));
  space Lh1 (omega, "P"+to_string(k+2)+"d");
  field edsth = lazy_interpolate (Lh1, div_h(sigmat_h)+f(d));
  Float err_div_sigmat_linf = edsth.max_abs();
  derr << "err_div_sigmat_l2   = " << err_div_sigmat_l2 << endl
       << "err_div_sigmat_linf = " << err_div_sigmat_linf << endl;
  return (err_sigmat_linf <= err_linf_expected) ? 0 : 1;
}
