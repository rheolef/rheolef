///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile p_laplacian_error.cc The p-Laplacian problem on a circular geometry -- error analysis
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "p_laplacian_circle.h"
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-15;
  Float p;
  field uh;
  din >> catchmark("p") >> p
      >> catchmark("u") >> uh;
  const geo& omega = uh.get_geo();
  const space& Xh = uh.get_space();
  field pi_h_u = lazy_interpolate (Xh, u_exact(p));
  field eh = pi_h_u - uh;
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(2*Xh.degree());
  Float err_lp  = pow(integrate (omega,
    pow(fabs(uh - u_exact(p)), p), iopt), 1./p);
  Float err_w1p = pow(integrate (omega,
    pow(norm(grad(uh) - grad_u(p)), p), iopt), 1./p);
  Float err_linf = eh.max_abs();
  dout << "err_linf = " << err_linf << endl
       << "err_lp   = " << err_lp << endl
       << "err_w1p  = " << err_w1p << endl;
  return (err_linf < tol) ? 0 : 1;
}
