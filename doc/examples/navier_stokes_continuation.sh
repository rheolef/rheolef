#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
approx="P1d"
n=80
name="square-$n"
mkgeo_grid -t $n > $name.geo

Re0=0

L="
100
400
1000
1500
2000
2500
3000
3500
4000
4500
5000
5500
6000
6500
7000
7500
8000
8500
9000
9500
10000
"


for Re in $L; do
  if test $Re0 -eq 0; then
    restart=""
  else
    restart="$name-P1d-Re\=${Re0}-upw.field"
  fi
  command="(./navier_stokes_cavity_newton_upw_dg $name $approx $Re 1e-15 100 $restart > $name-P1d-Re\=${Re}-upw.field ) 2>&1|tee ../pusrman/navier-stokes-dg-cavity-$n-P1d-Re=${Re}-upw-residu.gdat"
  echo $command
  eval $command
  status=$?
  if test $status -ne 0; then
    echo "comand failed" 1>&2
    exit 1
  fi
  Re0=$Re
done
