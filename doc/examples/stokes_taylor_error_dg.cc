///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile stokes_taylor_error_dg.cc The Stokes problem for the Taylor benchmark by the discontinuous Galerkin method -- error analysis
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "taylor_exact.h"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  Float err_linf_expected = (argc > 1) ? atof(argv[1]) : 1e+38;
  bool dump = (argc > 2);
  field uh;
  din >> uh;
  space Xh = uh.get_space();
  geo omega = Xh.get_geo();
  space Qh (omega, Xh.get_approx());
  field ph(Qh);
  din >> ph;
  size_t k = Xh.degree();
  size_t d = omega.dimension();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(2*k+1);
  Float meas_omega = integrate (omega);
  Float moy_ph = integrate (omega, ph, iopt);
  ph = ph - moy_ph/meas_omega; // ph with zero average value
  string high_approx = "P"+to_string(k+1)+"d";
  space Xh1 (omega, high_approx, "vector"),
        Qh1 (omega, high_approx);
  field euh = lazy_interpolate (Xh1, uh-u_exact());
  field eph = lazy_interpolate (Qh1, ph-p_exact());
  Float err_u_l2 = sqrt(integrate (omega, norm2(uh-u_exact()), iopt));
  Float err_u_linf = euh.max_abs();
  Float err_u_h1 = sqrt(integrate (omega, norm2(grad_h(euh)), iopt)
                    + integrate (omega.sides(), (1/h_local())*norm2(jump(euh)), iopt));
  Float err_p_l2 = sqrt(integrate (omega, sqr(ph-p_exact()), iopt));
  Float err_p_linf = eph.max_abs();
  derr << "err_u_l2   = " << err_u_l2 << endl
       << "err_u_linf = " << err_u_linf << endl
       << "err_u_h1   = " << err_u_h1 << endl
       << "err_p_l2   = " << err_p_l2 << endl
       << "err_p_linf = " << err_p_linf << endl;
  if (dump) {
    dout << catchmark("uh")  << uh 
         << catchmark("u")   << lazy_interpolate (Xh, u_exact())
         << catchmark("eu") << euh 
         << catchmark("ph")  << ph 
         << catchmark("p")   << lazy_interpolate (Qh, p_exact())
         << catchmark("ep") << eph;
  }
  return (max(err_u_linf,err_p_linf) <= err_linf_expected) ? 0 : 1;
}
