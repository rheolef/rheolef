///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile mosolov_augmented_lagrangian.h The Mossolov problem by the augmented Lagrangian method -- solver class header
struct mosolov_augmented_lagrangian: adapt_option {
  mosolov_augmented_lagrangian();
  void reset (geo omega, string approx);
  void initial (field& sigma_h, field& uh) const;
  int  solve   (field& sigma_h, field& uh) const;
  void put (odiststream& out, field& sigma_h, field& uh) const;
// data:
  Float Bi, n, r, tol;
  size_t max_iter;
  mutable space Xh, Th;
  mutable field lh;
  mutable form a, b, inv_mt;
  mutable problem pa;
};
#include "mosolov_augmented_lagrangian1.icc"
#include "mosolov_augmented_lagrangian2.icc"
