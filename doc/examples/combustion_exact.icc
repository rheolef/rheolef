///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile combustion_exact.icc The combustion problem -- its exact solution
#include "lambda2alpha.h"
struct u_exact {
  Float operator() (const point& x) const {
    return 2*log(cosh(a)/cosh(a*(1-2*x[0]))); }
  u_exact (Float lambda, bool is_upper)
   : a(lambda2alpha(lambda,is_upper)) {}
  u_exact (Float a1) : a(a1) {}
  Float a;
};
struct grad_u {
  point operator() (const point& x) const {
    return point(4*a*tanh(a*(1-2*x[0]))); }
  grad_u (Float lambda, bool is_upper)
   : a(lambda2alpha(lambda,is_upper)) {}
  grad_u (Float a1) : a(a1) {}
  Float a;
};
