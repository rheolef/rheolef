#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
DATADIR=$TOP_SRCDIR/main/tst
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

# -------------------------------------------------------------
# check for low degrees and equispaced node set
# -------------------------------------------------------------
# geo			err_P1	err_P2  err_P3
L="
line-bdry-v2		2e-2	2e-4	6e-6
carre-bamg-v2		3e-2	2e-3	4e-5
carre-bamg-q-dom-v2	9e-3	2e-4	3e-6
carre-tq-10-dom-v2	4e-2	6e-4	4e-5
cube-dom-v2		2e-1	3e-2	3e-3
cube-H-6-dom-v2		9e-2	2e-3	9e-5
cube-P-5-dom-v2		2e-1	6e-3	6e-4
my_cube_TP-5-v2         2e-1	3e-2	3e-3
my_cube_PH-5-v2         2e-1	7e-3	4e-4
my_cube_TPH-5-v2        2e-1	2e-2	2e-3
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2 P3; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./dirichlet-nh $DATADIR/$geo $Pk 2>/dev/null | RHEOPATH=$DATADIR \$RUN ./cosinusprod_error $err 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# -------------------------------------------------------------
# check for higher degrees and others node set:
# -------------------------------------------------------------
eps="1e-11"; # when reaches machine precision, up to rounding  effects

# geo			err_P6	err_P10 err_P16 / [equispaced, warburton]
L="
line-1-bdry		9e-5	5e-9	2e-10
			1e-4	5e-9	2e-10
line-bdry-v2		2e-11	2e-11	2e-8
			2e-11	$eps	$eps
triangle_p1-bdry-v2	7e-4	6e-7	3e-10
			1e-3	5e-7	$eps
carre-tq-10-dom-v2	4e-10	4e-10	3e-2
			4e-10	$eps	$eps
tetra2_p1-bdry-v2	2e-3	3e-6	none
			2e-2	2e-2	none
prism_p1-bdry-v2	2e-3	2e-6	none
			2e-3	2e-6	none
hexa_p1-bdry-v2		2e-4	none	none
			2e-4	none	none
"

raw_poly="dubiner"

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for node in equispaced warburton; do
    for k in 6 10 16; do
      err=`echo $L | gawk '{print $1}'`
      L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
      if test "$err" = "none"; then continue; fi
      loop_mpirun "./dirichlet-nh $DATADIR/$geo P${k}[$node,$raw_poly] 2>/dev/null | RHEOPATH=$DATADIR \$RUN ./cosinusprod_error $err 2>/dev/null >/dev/null"
      if test $? -ne 0; then status=1; fi
    done
  done
done

exit $status
