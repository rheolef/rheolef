///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile incompressible-elasticity.cc The incompressible elasticity problem for the embankment geometry
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "embankment.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  Float inv_lambda = (argc > 2 ? atof(argv[2]) : 0);
  size_t d = omega.dimension();
  space Xh = embankment_space(omega, "P2");
  space Qh (omega, "P1");
  point f (0,0,0);
  f [d-1] = -1;
  trial u (Xh), p (Qh); test v (Xh), q (Qh);
  field lh = integrate (dot(f,v));
  form  a  = integrate (2*ddot(D(u),D(v)));
  form  b  = integrate (-div(u)*q);
  form  c  = integrate (inv_lambda*p*q);
  field uh (Xh, 0), ph (Qh, 0);
  problem_mixed elasticity (a, b, c);
  elasticity.solve (lh, field(Qh,0), uh, ph);
  dout << catchmark("inv_lambda")  << inv_lambda << endl
       << catchmark("u")  << uh
       << catchmark("p")  << ph;
}
