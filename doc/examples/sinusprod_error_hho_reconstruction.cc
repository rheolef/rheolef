///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile sinusprod_error_hho_reconstruction.cc The sinus product function -- reconstruction for the hybrid high order method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod.h"
#include "sinusprod_grad.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float err_us_s_h1_expected = (argc > 1) ? atof(argv[1]) : 1e+38;
  field uhs; 
  din >> uhs;
  geo omega = uhs.get_geo();
  space Xhs = uhs.get_space();
  size_t d = omega.dimension(), kp1 = Xhs.degree();
  integrate_option iopt;
  iopt.invert = true;
  iopt.set_order (2*kp1+2);
  Float err_us_l2 = sqrt(integrate(omega, sqr(uhs-u_exact(d)), iopt)),
        err_us_h1 = sqrt(integrate(omega, norm2(grad_h(uhs)-grad_u(d)), iopt));
  derr << "err_us_l2   = " << err_us_l2 << endl
       << "err_us_h1   = " << err_us_h1 << endl;
#ifdef TODO
  // on_local_sides in non-variational exprs:
  Float err_us_s_l2 = sqrt(integrate(omega, on_local_sides(sqr(uhs-u_exact(d))), iopt)),
        err_us_s_h1 = sqrt(integrate(omega, on_local_sides(norm2(grad_h(uhs)-grad_u(d))), iopt));
  derr << "err_us_s_l2 = " << err_us_s_l2 << endl
       << "err_us_s_h1 = " << err_us_s_h1 << endl;
#endif // TODO
  return (err_us_h1 < err_us_s_h1_expected) ? 0 : 1;
}
