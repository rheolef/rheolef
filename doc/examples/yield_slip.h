///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile yield_slip.h The yield slip problem -- class header
class yield_slip {
public:
  typedef field value_type;
  typedef Float float_type;
  yield_slip (Float S, Float n, Float Cf, Float r,
    const geo& omega, const geo& boundary, string approx = "P1");
  field residue          (const field& beta_h) const;
  void update_derivative (const field& beta_h) const;
  field derivative_solve      (const field& mrh) const;
  field derivative_trans_mult (const field& mrh) const;
  Float space_norm      (const field&) const;
  Float dual_space_norm (const field&) const;
  field initial () const;
  void post (const field& beta_h, field& uh, field& lambda_h) const;
protected:
  Float S, n, Cf, r;
  geo boundary;
  space Xh, Wh, Yh;
  field lh, mkh;
  form m, mb, a, b;
  mutable form c1;
  problem pmb, pa;
  mutable problem pA;
};
#include "yield_slip1.icc"
#include "yield_slip2.icc"
