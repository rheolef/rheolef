#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${TOP_SRCDIR}/main/tst"
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

# avoid division by zero:
geo="$GEODIR/carre-bamg-splitbedge-v2.geo"

progs="p_laplacian_newton"
approxs="P1 P2 P3"
pvalues="1.5 2.5" ;# 1.5 => singular matrix for P1 with mumps(np>1) and P2, P3 with mumps and pastix
for prog in $progs; do
  for approx in $approxs; do
    for p in $pvalues; do
      # Pk approx diverges for p < 2 && k >= 2
      #if test $p = "1.5" && test $approx = "P2" -o $approx = "P3"; then continue; fi
      loop_mpirun "./$prog $geo $approx $p 1e-10 2>/dev/null | RHEOPATH=$GEODIR \$RUN ./p_laplacian_post - -check >/dev/null 2>/dev/null"
      if test $? -ne 0; then status=1; fi
    done
  done
done

exit $status
