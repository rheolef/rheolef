#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR=${TOP_SRCDIR}/main/tst
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

echo "      no more (skipped)"
exit 0

run "$SBINDIR/mkgeo_grid_1d -e 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-1d.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "$SBINDIR/mkgeo_grid_2d -t 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-t.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "$SBINDIR/mkgeo_grid_2d -q 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-q.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

run "$SBINDIR/mkgeo_grid_3d -T  5 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-3d-T.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

# -------------------------------------------------------------
# check for low degrees and equispaced node set
# -------------------------------------------------------------
# geo          		approx  tolerance (err_linf)
L="
mesh-1d			P0 	3e-1
mesh-1d			P1d 	2e-2
mesh-1d			P2d 	8e-3
mesh-1d			P3d 	4e-3
"
L_TODO="
mesh-2d-t		P0	3e-2
mesh-2d-t		P1d	2e-3
mesh-2d-t		P2d	4e-5
mesh-2d-t		P3d	1e-6
mesh-2d-q		P0	3e-2
mesh-2d-q		P1d	3e-4
mesh-2d-q		P2d	1e-5
mesh-2d-q		P3d	7e-8
mesh-3d-T		P0d	2e-1
mesh-3d-T		P1d	4e-2
"
# un peu long pour l'instant (l'assemblage domine)
L_TODO="
mesh-3d-T		P2d	4e-3
"

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  approx=`echo $L | gawk '{print $2}'`
  d=`grep dimension $geo.geo | gawk '{print $2}'`
  k=`echo $approx | sed -e 's/P//' -e 's/d//'`
  tol=`echo $L | gawk '{print $3}'`
  L=`echo $L | gawk '{for (i=4; i <= NF; i++) print $i}'`
  if test $d = 3 -a $k -ge 2; then continue; fi
  loop_mpirun "./dirichlet_hho_v2 $geo $approx $approx 2>/dev/null | \$RUN ./sinusprod_error_hdg $tol -us >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done
run "rm -f mesh-1d.geo mesh-2d-t.geo mesh-2d-q.geo mesh-3d-T.geo"

exit $status
