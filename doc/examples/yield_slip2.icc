///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile yield_slip2.icc The yield slip problem -- class body
#include "poisson_robin.icc"
field yield_slip::derivative_solve (const field& mrh) const {
  field mryh(Yh, 0.);
  mryh[1] = -mrh;
  field delta_yh(Yh);
  pA.solve(mryh, delta_yh);
  return delta_yh[1];
}
field yield_slip::derivative_trans_mult (const field& mrh) const {
  field rh (Wh);
  pmb.solve (mrh, rh);
  field rhs = b.trans_mult(rh);
  field delta_vh (Xh, 0.);
  pa.solve (rhs, delta_vh);
  field mgh = b*delta_vh + c1*rh;
  field gh (Wh);
  pmb.solve (mgh, gh);
  return gh;
}
Float yield_slip::space_norm (const field& rh) const {
  return sqrt (mb(rh,rh));
}
Float yield_slip::dual_space_norm (const field& mrh) const {
  field rh (Wh,0);
  pmb.solve (mrh, rh);
  return sqrt (dual (mrh,rh));
}
field yield_slip::initial () const {
  field uh = poisson_robin (Cf, boundary, lh);
  return (Cf+r)*uh["boundary"];
}
void yield_slip::post (const field& beta_h, field& uh, field& lambda_h) const {
  field rhs = lh - b.trans_mult(beta_h);
  uh = field (Xh, 0.);
  pa.solve (rhs, uh);
  lambda_h = beta_h - r*uh["boundary"];
}
