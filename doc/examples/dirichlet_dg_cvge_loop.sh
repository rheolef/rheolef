#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
uniform=true
Pkd=${1-"P1d"}
L="10 20 40 80 160"

prog="dirichlet_dg"
prog_error="cosinusprod_error_dg"

name="$prog-cv-$Pkd"
if $uniform; then
  mkgeo="mkgeo_grid -t";  # uniform grid
  name="$name-grid"
else
  mkgeo="mkgeo_ugrid -q"; # unstructured grid
  name="$name-gmsh"
fi
gdat="$name.gdat"
plot="$name.plot"
tmp="tmp.log"
echo "# $prog $Pkd"
echo "# n err_l2 err_linf err_h1" | tee $gdat
for n in $L; do
  command="$mkgeo $n > square-$n.geo 2>/dev/null"
  #echo $command
  eval $command
  command="./$prog square-$n $Pkd 2>/dev/null | ./${prog_error} 2> $tmp"
  #echo $command
  eval $command
  err_l2=`grep err_l2 $tmp | gawk '{print $3}'`
  err_linf=`grep err_linf $tmp | gawk '{print $3}'`
  err_h1=`grep err_h1 $tmp | gawk '{print $3}'`
  echo "$n $err_l2 $err_linf $err_h1" | tee -a $gdat
done
echo "! file $gdat created" 1>&2

k=`echo $Pkd | sed -e 's/P//' -e 's/d//'`
cat > $plot << EOF
set log
plot \
"$gdat" u (1./\$1):2 w lp, \
"$gdat" u (1./\$1):3 w lp, \
"$gdat" u (1./\$1):4 w lp, \
0.2*x**($k+1), \\
2*x**($k)
pause -1 "<retour>"
EOF
echo "! file $plot created" 1>&2

gnuplot $plot
