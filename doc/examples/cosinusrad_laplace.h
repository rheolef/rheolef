///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile cosinusrad_laplace.h The cosinus radius function -- right-hand side and boundary condition
struct f {
  Float operator() (const point& x) const { 
    Float r = sqrt(sqr(x[0])+sqr(x[1])+sqr(x[2]));
    Float sin_over_ar = (r == 0) ? 1 : sin(a*r)/(a*r);
    return sqr(a)*((d-1)*sin_over_ar + cos(a*r)); }
  f(size_t d1) : d(d1), a(acos(Float(-1.0))) {}
  size_t d; Float a;
};
struct g {
  Float operator() (const point& x) const { 
    return cos(a*sqrt(sqr(x[0])+sqr(x[1])+sqr(x[2]))); }
  g(size_t=0) : a(acos(Float(-1.0))) {}
  Float a;
};
