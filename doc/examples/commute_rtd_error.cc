///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile commute_rtd_error.cc Discontinuous Raviart-Thomas L2 projection -- error analysis
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "cosinus_vector.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float err_p_l2_valid     = (argc > 1) ? atof(argv[1]) : 1;
  Float err_p_div_l2_valid = (argc > 2) ? atof(argv[2]) : 1;
  field  p_Vh_u,   pi_h_u;
  din >> p_Vh_u >> pi_h_u;
  const geo& omega = p_Vh_u.get_geo();
  const space& Vh  = p_Vh_u.get_space();
  size_t d = omega.dimension();
  size_t k = Vh.get_basis().family_index();
  integrate_option iopt;
  iopt.set_order (2*k+2);
  Float err_p_l2     = sqrt(integrate(omega,norm2(p_Vh_u-u_exact(d)),iopt)),
        err_i_l2     = sqrt(integrate(omega,norm2(pi_h_u-u_exact(d)),iopt)),
        err_p_div_l2 = sqrt(integrate(omega,norm2(div_h(p_Vh_u)-div_u_exact(d)),iopt)),
        err_i_div_l2 = sqrt(integrate(omega,norm2(div_h(pi_h_u)-div_u_exact(d)),iopt));
  dout << "err_p_l2     = " << err_p_l2     << endl
       << "err_i_l2     = " << err_i_l2     << endl
       << "err_p_div_l2 = " << err_p_div_l2 << endl
       << "err_i_div_l2 = " << err_i_div_l2 << endl;
  return (err_p_l2 <= err_p_l2_valid && err_p_div_l2 <= err_p_div_l2_valid) ? 0: 1;
}
