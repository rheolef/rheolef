///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile mosolov_error_yield_surface.cc The Mossolov problem for a circular pipe -- error analysis for the yield surface
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "mosolov_exact_circle.h"
Float delta (Float f, Float g) { return (f*g >= 0) ? 0 : 1; }
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-15;
  Float Bi;
  field sigma_h;
  din >> catchmark("Bi")    >> Bi 
      >> catchmark("sigma") >> sigma_h;
  space Th = sigma_h.get_space();
  geo omega = Th.get_geo();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(4*(Th.degree()+1));
  Float err_ys_l1 = integrate (omega,
     compose(delta, norm(sigma_h)-Bi, norm(sigma())-Bi), iopt);
  dout << "err_ys_l1 = " << err_ys_l1 << endl;
  return err_ys_l1 < tol ? 0 : 1;
}
