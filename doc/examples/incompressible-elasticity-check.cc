///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char** argv) {
  environment rheolef (argc, argv);
  Float inv_lambda;
  field uh, ph;
  din  >> catchmark("inv_lambda")  >> inv_lambda
       >> catchmark("u")  >> uh
       >> catchmark("p")  >> ph;
  const space& Xh = uh.get_space();
  const space& Qh = ph.get_space();
  size_t d = Xh.get_geo().dimension();
  point f (0,0,0);
  f [d-1] = -1;
  trial u (Xh), p (Qh); test v (Xh), q (Qh);
  field lh = integrate (dot(f,v));
  form  a  = integrate (2*ddot(D(u),D(v)));
  form  b  = integrate (-div(u)*q);
  form  mp = integrate (p*q);
  form  c  = inv_lambda*mp;
  field m_ru = a*uh + b.trans_mult(ph) - lh;
  field m_rp = b*uh - c*ph;
  for (size_t i_comp = 0; i_comp < d; i_comp++) {
    m_ru[i_comp]["bottom"] = 0;
    m_ru[i_comp]["left"] = 0;
  }
  if (d == 3) {
    for (size_t i_comp = 0; i_comp < d; i_comp++) {
      m_ru[i_comp]["right"] = 0;
      m_ru[i_comp]["back"] = 0; 
    }
  }
  field rp (Qh);
  problem pmp (mp);
  pmp.solve(m_rp, rp);
  Float res_u = m_ru.u().max_abs();
  Float res_p = sqrt(mp(rp,rp));
  Float res = max(res_u, res_p);
  derr << "check: residue(uh) = " << res_u << endl 
       << "check: residue(ph) = " << res_p << endl;
  if (argc > 1) {
    dout << catchmark ("mru") << m_ru << endl
         << catchmark ("mrp") << m_rp << endl;
  }
  check_macro (res < 1e-6, "unexpected residue");
}
