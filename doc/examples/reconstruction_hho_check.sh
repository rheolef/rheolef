#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR=${TOP_SRCDIR}/main/tst
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skipped)"
#exit 0

run "$SBINDIR/mkgeo_grid_2d -t 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-t.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

# -------------------------------------------------------------
# geo          		approx  err_us_h1
# -------------------------------------------------------------
L="
mesh-2d-t		P0	3e-1
mesh-2d-t		P1d	2e-2
mesh-2d-t		P2d	9e-4
mesh-2d-t		P3d	3e-5
"

tol="1e-10"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  approx=`echo $L | gawk '{print $2}'`
  L=`echo $L | gawk '{for (i=4; i <= NF; i++) print $i}'`
  for prog in reconstruction_hho; do
    loop_mpirun "./$prog $geo $approx $approx 2>/dev/null | \$RUN ./reconstruction_hho_check $tol >/dev/null 2>/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
run "rm -f mesh-2d-t.geo"

exit $status
