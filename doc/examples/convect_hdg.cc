///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile convect_hdg.cc Convection-diffusion equation by the hybrid discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "rotating-hill-statio.h"
int main (int argc, char **argv) {
  environment rheolef (argc,argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1d";
  Float  nu     = (argc > 3) ? atof(argv[3]) : 1e-2;
  Float  beta   = (argc > 4) ? atof(argv[4]) : 1;
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh,
        Mh (omega["sides"], approx);
  Mh.block("boundary");
  space Wh(Mh.get_geo()["boundary"],approx);
  size_t d = omega.dimension();
  trial x(Yh);
  trial lambda(Mh);
  test  y(Yh);
  test  mu(Mh);
  integrate_option iopt;
  iopt.invert = true;
  form inv_a = integrate((1/nu)*dot(x[0],y[0]) + x[1]*div_h(y[0])
          + y[1]*div_h(x[0]) - phi::sigma(d,nu)*x[1]*y[1]
          + x[1]*dot(u(d),grad_h(y[1]))
  	  - on_local_sides((beta+abs(dot(u(d),normal())))*x[1]*y[1]), iopt);
  form b1 = integrate("internal_sides", (-dot(jump(x[0]),normal())
          + 2*(beta+abs(dot(u(d),normal())))*average(x[1])
	  - dot(u(d),normal())*jump(x[1]))*mu);
  form b2 = integrate("internal_sides", (-dot(jump(x[0]),normal())
          + 2*(beta+abs(dot(u(d),normal())))*average(x[1]))*mu);
  form c = integrate("internal_sides",
                    2*(beta+abs(dot(u(d),normal())))*lambda*mu);
  field lh = integrate(omega, -f(d,nu)*y[1])
           + integrate("boundary", phi(d,nu)*(dot(y[0],normal())
              - (beta+abs(dot(u(d),normal()))-dot(u(d),normal()))*y[1]));
  field kh(Mh,0), lambda_h(Mh,0);
  lambda_h["boundary"] = lazy_interpolate(Wh,phi(d,nu));
  form s = c + b2*inv_a*trans(b1);
  field rh = b2*(inv_a*lh) - kh;
  problem p(s);
  p.solve (rh, lambda_h);
  field xh = inv_a*(lh - b1.trans_mult(lambda_h));
  dout << catchmark("nu")     << nu << endl
       << catchmark("phi")    << xh[1]
       << catchmark("sigma")  << xh[0]
       << catchmark("lambda") << lambda_h;
}
