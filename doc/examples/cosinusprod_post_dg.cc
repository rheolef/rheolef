///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile cosinusprod_post_dg.cc The cosinus product function -- post-treatment with the discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "cosinusprod.h"
#include "cosinusprod_grad.h"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  field uh; din >> uh;
  space Xh = uh.get_space();
  geo omega = Xh.get_geo();
  size_t k = Xh.degree();
  size_t d = omega.dimension();
  space Vh (omega, "P"+to_string(k));
  Vh.block("boundary");
  space Wh (omega["boundary"], "P"+to_string(k));
  test u (Vh); trial v (Vh);
  integrate_option fopt;
  fopt.lump = true;
  fopt.set_family(integrate_option::gauss_lobatto);
  fopt.set_order(k);
  form m = integrate (u*v, fopt);
  field lh = integrate (uh*v, fopt);
  field uh_star (Vh, 0);
  uh_star ["boundary"] = lazy_interpolate(Wh, u_exact(d));
  problem pm (m);
  pm.solve (lh, uh_star);
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(2*k+1);
  space Xh1 (omega, "P"+to_string(k+1)+"d");
  field eh      = lazy_interpolate (Xh1, uh-u_exact(d));
  field eh_star = lazy_interpolate (Xh1, uh_star-u_exact(d));
  field eta_h   = eh_star - eh;
  Float err_linf      = eh.max_abs();
  Float err_star_linf = eh_star.max_abs();
  Float eta_linf      = eta_h.max_abs();
  Float err_l2       = sqrt(integrate (omega, sqr(uh-u_exact(d)), iopt));
  Float err_star_l2  = sqrt(integrate (omega, sqr(uh_star-u_exact(d)), iopt));
  Float eta_l2       = sqrt(integrate (omega, sqr(eta_h), iopt));
  Float err_h1       = sqrt(integrate (omega, norm2(grad_h(uh)-grad_u(d)), iopt));
  Float err_star_h1  = sqrt(integrate (omega, norm2(grad(uh_star)-grad_u(d)), iopt));
  Float eta_h1       = sqrt(integrate (omega, norm2(grad_h(eta_h)), iopt));
  // gradient as P0
  space G0h (omega, "P0", "vector");
  field grad_eh    = lazy_interpolate (G0h, grad_h(eh));
  field grad_eta_h = lazy_interpolate (G0h, grad_h(eta_h));
  // proj gradient as P1
  space G1h (omega, "P1", "vector");
  G1h.block("boundary");
  test uu (G1h); trial vv (G1h);
  form m1v = integrate(dot(uu,vv), fopt);
  field l1h = integrate (dot(grad_eh,vv));
  field grad_eh_p1 (G1h, 0);
  problem pm1v (m1v);
  pm1v.solve(l1h, grad_eh_p1);
  field l2h = integrate (dot(grad_eta_h,vv));
  field grad_eta_h_p1 (G1h, 0);
  pm1v.solve(l2h, grad_eta_h_p1);
  // calcul de s_eta_h = sqrt(int_K eta_h^2 dx) : P0
  space X0h (omega, "P0");
  test v0 (X0h);
  field s_eta_h2 = integrate (sqr(eta_h)*v0);
  field s_eta_h = lazy_interpolate (X0h, sqrt(s_eta_h2));
  field s_eh2 = integrate (sqr(uh-u_exact(d))*v0);
  field s_eh = lazy_interpolate (X0h, sqrt(s_eh2));
  derr << "err_l2        = " << err_l2      << endl
       << "eta_l2        = " << eta_l2      << endl
       << "err_star_l2   = " << err_star_l2 << endl
       << "err_linf      = " << err_linf      << endl
       << "eta_linf      = " << eta_linf      << endl
       << "err_star_linf = " << err_star_linf << endl
       << "err_h1        = " << err_h1      << endl
       << "eta_h1        = " << eta_h1      << endl
       << "err_star_h1   = " << err_star_h1 << endl;
  dout << catchmark ("grad_eta_h")   << grad_eta_h_p1
       << catchmark ("grad_eh")      << grad_eh_p1
       << catchmark ("eta_h")   << eta_h
       << catchmark ("eh")      << eh
       << catchmark ("uh")      << uh
       << catchmark ("uh_star") << uh_star
       << catchmark ("s_eta_h") << s_eta_h
       << catchmark ("s_eh")    << s_eh
    ;
}
