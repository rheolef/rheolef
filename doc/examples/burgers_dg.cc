///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile burgers_dg.cc The Burgers equation by the discontinous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "harten.h"
#include "burgers.icc"
#include "burgers_flux_godunov.icc"
#include "runge_kutta_ssp.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  Float cfl = 1;
  limiter_option lopt;
  size_t nmax = (argc > 3) ? atoi(argv[3]) : numeric_limits<size_t>::max();
  Float  tf   = (argc > 4) ? atof(argv[4]) : 2.5;
  size_t p    = (argc > 5) ? atoi(argv[5]) : ssp::pmax;
  lopt.M      = (argc > 6) ? atoi(argv[6]) : u_init().M();
  if (nmax == numeric_limits<size_t>::max()) {
      nmax = (size_t)floor(1+tf/(cfl*omega.hmin()));
  }
  Float delta_t = tf/nmax;
  integrate_option iopt;
  iopt.invert = true;
  trial u (Xh); test v (Xh);
  form inv_m = integrate (u*v, iopt);
  vector<field> uh(p+1, field(Xh,0));
  uh[0] = lazy_interpolate (Xh, u_init());
  branch even("t","u");
  dout << catchmark("delta_t") << delta_t << endl
       << even(0,uh[0]);
  for (size_t n = 1; n <= nmax; ++n) {
    for (size_t i = 1; i <= p; ++i) {
      uh[i] = 0;
      for (size_t j = 0; j < i; ++j) {
        field lh = 
         - integrate (dot(compose(f,uh[j]),grad_h(v)))
         + integrate ("internal_sides",
             compose (phi, normal(), inner(uh[j]), outer(uh[j]))*jump(v))
         + integrate ("boundary",
             compose (phi, normal(), uh[j], g(n*delta_t))*v);
        uh[i] += ssp::alpha[p][i][j]*uh[j] - delta_t*ssp::beta[p][i][j]*(inv_m*lh);
      }
      uh[i] = limiter(uh[i], g(n*delta_t)(point(-1)), lopt);
    }
    uh[0] = uh[p];
    dout << even(n*delta_t,uh[0]);
  }
}
