///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "contraction.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo  omega (argv[1]);
  space X1h = contraction::velocity_space (omega, "P1");
  space Bh  (omega, "bubble", "vector");
  space Xh = X1h * Bh;
  space Qh  (omega, "P1");
  trial u1 (X1h), ub (Bh), p (Qh);
  test  v1 (X1h), vb (Bh), q (Qh);
  form a1 = integrate (2*ddot(D(u1),D(v1)));
  form ab = integrate (2*ddot(D(ub),D(vb)));
  form b1 = integrate (-div(u1)*q);
  form bb = integrate (-div(ub)*q);
  form a = {{a1,  0},
            { 0, ab}};
  form b =  {b1, bb};
  a.set_uu().set_symmetry(true);
  field uh (Xh, 0), ph (Qh, 0);
  uh[0] = contraction::velocity_field (X1h);
  problem_mixed stokes (a, b);
  stokes.solve (field(Xh,0), field(Qh,0), uh, ph);
  dout << catchmark("inv_lambda") << 0 << endl
       << catchmark("u")  << uh[0]
       << catchmark("ub") << uh[1]
       << catchmark("p")  << ph;
}
