#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# print the solution of the mosolov problem as
#  - the sheared zones in gray
#  - some velocity isovalues
#  - the geo boundaries as lines
#  - the mirroited mesh, when using a sector
#
# usage:
#  mosolov_view [-[no]clean] [-[no]verbose] file[.field[.gz]]
#
name=""
do_png=false
verbose=false
clean=true
while test $# -ne 0; do
  case $1 in
  -png)       do_png=true;;
  -verbose)   verbose=true;;
  -noverbose) verbose=false;;
  -clean)     clean=true;;
  -noclean)   clean=false;;
  *)    name=$1;;
  esac
  shift
done
if test "$name" = ""; then
  echo "usage: $0 [-png] file[.field[.gz]]" >&2
  exit 1
fi
#echo "avant : name=$name"
name=`expr $name : '\(.*\).gz'    \| $name`
name=`expr $name : '\(.*\).field' \| $name`
#echo "apres : name=$name"
if test -f $name.field; then
  input="cat $name.field"
else
  input="zcat $name.field.gz"
fi
to_clean=""
auxfiles=""
# --------------------------------------
# 1. sheared zones in gray
# --------------------------------------
command="$input | ./mosolov_yield_surface 2>/dev/null | tee tmp1.field | field - -name tmp1 -proj P1 -gray -n-iso-negative 1 -n-iso 2 -paraview -noclean -noexecute 2>/dev/null"
$verbose && echo "! $command" 1>&2
eval   "$command"
phi_min=`field tmp1 -min 2>/dev/null`
phi_max=`field tmp1 -max 2>/dev/null`
to_clean="$to_clean tmp1.vtk"
auxfiles="$auxfiles tmp1.field tmp1.py"
# --------------------------------------
# 2. velocities as isovalues
# --------------------------------------
command="$input | field - -name tmp2 -catchmark u -bw -n-iso 10 -paraview -noclean -noexecute 2>/dev/null"
$verbose && echo "! $command" 1>&2
eval   "$command"
umax=`field $input -catchmark u -max 2>/dev/null`
to_clean="$to_clean tmp2.vtk"
auxfiles="$auxfiles tmp2.py"
# --------------------------------------
# 3. add boundaries
# --------------------------------------
geoname=`$input | field -get-geo - 2>/dev/null`
if test -f $geoname.geo; then
  geoinput="cat $geoname.geo"
else
  geoinput="zcat $geoname.geo.gz"
fi
# file "tmp3.axis.vtk" created.
# file "tmp3.boundary.vtk" created.
# file "tmp3.bisector.vtk" created.
rm -f tmp3.bisector.vtk tmp3.boundary.vtk tmp3.axis.vtk tmp3.vtk
command="$geoinput | geo - -name tmp3 -paraview -noclean -noexecute 2>/dev/null"
$verbose && echo "! $command" 1>&2
eval   "$command"
if test -f tmp3.bisector.vtk; then
 is_sector=true
 domlist="[ 'tmp3', 'axis', 'boundary', 'bisector']"
else
 domlist="[ 'tmp3', 'boundary']"
 is_sector=false
fi
to_clean="$to_clean tmp3.vtk tmp3.axis.vtk tmp3.boundary.vtk tmp3.bisector.vtk"
auxfiles="$auxfiles tmp3.py"
# --------------------------------------
# 4. python script
# --------------------------------------
format=""
py_output="tmp.py"
if $do_png; then
  format="png"
  pv_run="pvbatch --use-offscreen-rendering $py_output"
else
  pv_run="paraview --script=$py_output"
fi
to_clean="$to_clean $py_output"
cat > $py_output << EOF
#!/usr/bin/env paraview --script=
# -------------------------------------------------------------------------
# main
# -------------------------------------------------------------------------
from paraview.simple  import *
from paraview_rheolef import *  # load rheolef specific functions

opt = {                 	\
}
opt1 = {                 	\
    'format'  : '',		\
    'resolution' : [1024,768], \
    'mark'    : 'scalar',   \
    'label'   : '',   \
    'showlabel': 1,   \
    'axis'    : 1,			\
    'color'   : 'gray',
    'have_opacity_bug'   : 0,
    'stereo'  : 0,		\
    'bbox'    : [[-1,-1,0],[1,1,0]],  \
    'scale'   : 1,		\
    'elevation'  : 0,		\
    'range'      : [$phi_min, $phi_max],  \
    'iso'        : 1,		\
    'isovalue'   : 0,		\
    'n_isovalue'          : 2,		\
    'n_isovalue_negative' : 1,		\
    'cut'        : 0,		\
    'origin'     : (1.79769e+308, 0, 0), 		\
    'normal'     : (1, 0, 0),  		\
    'grid'    : 0,		\
    'fill'    : 1,		\
    'volume'  : 0,		\
    'view_1d' : 0,		\
    'view_2d' : 1,		\
    'view_map' : 0,		\
    'high_order' : 0,   	\
    'valued'  : 'scalar',   	\
    'style'   : 'none'   	\
    }

paraview_branch_2d_scalar_bicolor(paraview, "tmp1", "tmp1.vtk", opt1)

opt2 = {                 	\\
    'outline'  : 0,             \\
    'format'  : '',		\\
    'resolution' : [1024,768], \\
    'mark'    : 'scalar',   \\
    'label'   : 'u',   \\
    'showlabel': 0,   \\
    'axis'    : 1,	\\
    'color'   : 'black_and_white',  \\
    'have_opacity_bug'   : 0,
    'stereo'  : 0,		\\
    'bbox'    : [[0,0,0],[1,1,0]],  \\
    'scale'   : 1,		\\
    'elevation'  : 0,		\\
    'range'      : [1e-15, $umax],  \\
    'iso'        : 0,		\\
    'isovalue'   : 0,		\\
    'n_isovalue' : 10,		\\
    'n_isovalue_negative' : 0,		\\
    'cut'        : 0,		\\
    'origin'     : (1.79769e+308, 0, 0), 		\\
    'normal'     : (1, 0, 0),  		\\
    'grid'    : 0,		\\
    'fill'    : 0,		\\
    'volume'  : 0,		\\
    'view_1d' : 0,		\\
    'view_2d' : 1,		\\
    'view_map' : 0,		\\
    'high_order' : 0,   	\\
    'valued'  : 'scalar',   	\\
    'style'   : 'none',   	\\
    'high_order' : 0	  	\\
    }

paraview_field_scalar(paraview, "tmp2", opt2)

d = $domlist 
opt3 = {                        \\
    'boundary_only'  : 1,          \\
    'format'  : '$format',          \\
    'resolution' : [1024,768], \\
    'showlabel'   : 1,
    'axis'    : 1,
    'view_1d' : 0,
    'view_2d' : 1,
    'color'   : 'black_and_white',
    'stereo'  : 0,          \\
    'bbox'    : [[0,0,0],[1,1,0]],  \\
    'ball'    : 0,            \\
    'cut'     : 0,            \\
    'fill'    : 0,            \\
    'full'    : 0,            \\
    'lattice' : 1,         \\
    'shrink'  : 0,         \\
    'tube'    : 0,         \\
    'volume'  : 0,         \\
    'has_origin' : 0,  \\
    'origin'     : [1.79769e+308, 0, 0], \\
    'normal'     : [1, 0, 0], \\
    'elevation'  : 0   \\
  }

paraview_geo(paraview, opt3, d)
EOF

if $is_sector; then
cat >> $py_output << EOF2
def sector_mirror_mesh (paraview, vtkfile):
  reader = LegacyVTKReader(FileNames=vtkfile);
  render = GetActiveViewOrCreate('RenderView');
  mirror = Transform(Input=reader)
  mirror.Transform = 'Transform'
  mirror.Transform.Rotate = [0.0, 0.0, 90.0]
  mirror.Transform.Scale = [1.0, -1.0, 1.0]
  show_mirror = Show(mirror, render)
  show_mirror.Representation     = 'Wireframe'
  show_mirror.DiffuseColor       = [0,0,0]
  show_mirror.AmbientColor       = [0,0,0]
  show_mirror.ColorArrayName = ''
  show_mirror.SelectionPointFieldDataArrayName = 'mesh'
  show_mirror.SetScalarBarVisibility(render, True)
# show_mirror.SetShowBox (False);
  Hide(reader, render)
  render.Update()

sector_mirror_mesh (paraview, "tmp3.vtk");
EOF2
fi
$verbose && echo "! file $py_output created" 1>&2

command="(LANG=C PYTHONPATH=`rheolef-config --pkgdatadir` ${pv_run} 2>&1) > tmp.log"
to_clean="$to_clean tmp.log"
$verbose && echo "! $command" 1>&2
eval   "$command"
status=$?
if test $status -ne 0; then
  if $verbose; then true; else echo "! $command" 1>&2; fi
  cat tmp.log
  echo "$0: command failed" 1>&2
  exit 1
fi
if $do_png; then
  command="convert tmp3.png -trim $name.png"
  $verbose && echo "! $command" 1>&2
  eval   "$command"
  echo "! file $name.png created" 1>&2
fi
to_clean="$to_clean $py_output"
if $clean; then
  command="rm -f $to_clean"
  $verbose && echo "! $command" 1>&2
  eval   "$command"
fi
command="rm -f $auxfiles"
$verbose && echo "! $command" 1>&2
eval   "$command"

