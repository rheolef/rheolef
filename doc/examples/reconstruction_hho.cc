///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile reconstruction_hho.cc The hybrid high order method -- reconstruction operator
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod.h"
#include "diffusion_isotropic.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string Pkd = (argc > 2) ? argv[2] : "P0",
         Pld = (argc > 3) ? argv[3] :  Pkd;
  space  Xh (omega, Pld),
         Mh (omega["sides"], Pkd);
  size_t l = Xh.degree(), k = Xh.degree(), d = omega.dimension();
  check_macro(l == k-1 || l == k || l == k+1,
    "invalid (k,l) = ("<<k<<","<<l<<")");
  space Xhs(omega, "P"+to_string(k+1)+"d"),
        Zh (omega, "P0");
  trial u(Xh), lambda(Mh), us(Xhs), zeta (Zh);
  test  v(Xh), mu    (Mh), vs(Xhs), xi   (Zh);
  auto as = lazy_integrate (dot(grad_h(us),A(d)*grad_h(vs)));
  auto bs = lazy_integrate (us*xi);
  auto cs = lazy_integrate (pow(h_local(),2)*zeta*xi);
  auto m  = lazy_integrate (u*v);
  form ms = lazy_integrate (lambda*mu);
  auto inv_cs = inv(cs);
  auto inv_m  = inv(m);
  auto inv_S  = inv(as + trans(bs)*inv_cs*bs);
  auto llh = lazy_integrate (u_exact(d)*v);
  field rhs = lazy_integrate (u_exact(d)*mu);
  field pi_Xh_u = inv_m*llh;
  field pi_Mh_lambda(Mh); 
  problem pms (ms);
  pms.solve (rhs, pi_Mh_lambda);
  field lh = lazy_integrate (dot(grad_h(pi_Xh_u),A(d)*grad_h(vs))
                    + on_local_sides((pi_Mh_lambda-pi_Xh_u)
                                   *dot(normal(),A(d)*grad_h(vs))));
  auto kh = lazy_integrate (pi_Xh_u*xi);
  auto rh = lh + bs.trans_mult(inv_cs*kh);
  field us_h = inv_S*rh;
  field zeta_h = inv_cs*(bs*us_h - kh);
  dout << catchmark("us")   << us_h
       << catchmark("zeta") << zeta_h;
}
