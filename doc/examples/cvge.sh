#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
if test $# -eq 0; then
  echo "usage: $0 name Pk elt" 
  echo "example: $0 dirichlet-nh P1 e" 
  echo "         $0 dirichlet-nh P3 ut" 
  echo "         $0 dirichlet2-ball P3 ut" 
  echo "         $0 dirichlet-nh-ball P3 ut" 
  echo "         $0 helmholtz_s_sphere P3 t" 
  exit 0
fi
fix=true; # when fixing gmsh internal nodes
name=${1-"dirichlet"}
Pk=${2-"P1"}
order=`echo $Pk | sed -e 's/P//'`
echo "Pk $Pk" >&2
echo "order $order" >&2
e=${3-"e"}
np=2
if $fix; then
  ball_arg=""
else
  ball_arg="-nofix"
fi
case $e in
 e|t|q) 
      if test $name != "helmholtz_s_sphere"; then 
	#n_set="10 20 40 80"
	n_set="8 11 16 23 32 45 64 92 128"
      else
	#n_set="8 12 16 24 32 48 64"
	n_set="8 11 16 23 32 45 64 92 128"
      fi
      ;;
 ut|uq|utq)
	#n_set="  10 20 40 80 160"
	#n_set="8 12 16 24 32 48 64"
	n_set="8 11 16 23 32 45 64 92 128"
	;;
 *) case $Pk in
    P1)
	#n_set="5 10 15 20"
	n_set="5 6 8 10 13 16 20 25 32 40"
	;;
    *)  
	#n_set="3 5 7 10";
	n_set="5 6 8 10 13 16 20 25 32 40"
    esac
esac
RUN="mpirun -np $np"
#RUN=""
ball=false
case $name in
 dirichlet2|robin2|neumann-nh)   errname="sinusprod";;
 neumann|robin|dirichlet-nh)     errname="cosinusprod";;
 dirichlet2-ball)  	   	 errname="sinusrad"; ball=true;;
 dirichlet-nh-ball)     	 errname="cosinusrad"; ball=true;;
 helmholtz_s_sphere)     	 errname="helmholtz_s_sphere"; ball=true; ball_arg="$ball_arg -s";;
 *)                              echo "unexpected $name"; exit 1;;
esac 
verbose=true
# ------------------------------------------
# utility
# ------------------------------------------
my_eval () {
  command="$*"
  if test "$verbose" = true; then echo "! $command" 1>&2; fi
  eval $command
  if test $? -ne 0; then
    echo "$0: error on command: $command"
    exit 1
  fi
}
# ------------------------------------------
# plot
# ------------------------------------------
gdat="cvge-$name-$Pk-${e}${ball_arg}.gdat"
plot="cvge-$name-$Pk-${e}${ball_arg}.plot"
cat << EOF > $plot
set logscale
set size square

a_l2 = 1.0
c_l2 = 1.0
f_l2(log10_h) = a_l2*log10_h + c_l2
g_l2(h) = 10.0**(f_l2(log10(h)))
fit f_l2(x) "$gdat" using (log10(1./\$1)):(log10(\$2)) via a_l2, c_l2
titre_l2 = sprintf("pente = %g", a_l2)

a_h1 = 1.0
c_h1 = 1.0
f_h1(log10_h) = a_h1*log10_h + c_h1
g_h1(h) = 10.0**(f_h1(log10(h)))
fit f_h1(x) "$gdat" using (log10(1./\$1)):(log10(\$3)) via a_h1, c_h1
titre_h1 = sprintf("pente = %g", a_h1)

a_linf = 1.0
c_linf = 1.0
f_linf(log10_h) = a_linf*log10_h + c_linf
g_linf(h) = 10.0**(f_linf(log10(h)))
fit f_linf(x) "$gdat" using (log10(1./\$1)):(log10(\$4)) via a_linf, c_linf
titre_linf = sprintf("pente = %g", a_linf)

plot \
	"$gdat" u (1./\$1):(\$2) t "$Pk($e) err_l2" w lp, \
	g_l2(x) t titre_l2, \
	"$gdat" u (1./\$1):(\$4) t "$Pk($e) err_linf" w lp, \
	g_linf(x) t titre_linf, \
	"$gdat" u (1./\$1):(\$3) t "$Pk($e) err_h1" w lp, \
	g_h1(x) t titre_h1

print "pente_l2   = ", a_l2
print "pente_h1   = ", a_h1
print "pente_linf = ", a_linf

pause -1 "<retour>"
EOF

# ------------------------------------------
# pente file = silent plot
# ------------------------------------------
tmp_pente="tmp.plot"
cat << EOF2 > $tmp_pente
set logscale
set size square

a_l2 = 1.0
c_l2 = 1.0
f_l2(log10_h) = a_l2*log10_h + c_l2
g_l2(h) = 10.0**(f_l2(log10(h)))
fit f_l2(x) "$gdat" using (log10(1./\$1)):(log10(\$2)) via a_l2, c_l2
titre_l2 = sprintf("pente = %g", a_l2)

a_h1 = 1.0
c_h1 = 1.0
f_h1(log10_h) = a_h1*log10_h + c_h1
g_h1(h) = 10.0**(f_h1(log10(h)))
fit f_h1(x) "$gdat" using (log10(1./\$1)):(log10(\$3)) via a_h1, c_h1
titre_h1 = sprintf("pente = %g", a_h1)

a_linf = 1.0
c_linf = 1.0
f_linf(log10_h) = a_linf*log10_h + c_linf
g_linf(h) = 10.0**(f_linf(log10(h)))
fit f_linf(x) "$gdat" using (log10(1./\$1)):(log10(\$4)) via a_linf, c_linf
titre_linf = sprintf("pente = %g", a_linf)

print "pente_l2 = ", a_l2
print "pente_h1 = ", a_h1
print "pente_linf = ", a_linf
EOF2

# ------------------------------------------
# gdat
# ------------------------------------------
geo_upgrade="../../main/bin/geo -upgrade -"
rm -f $gdat
echo "# cvgce $Pk $e"    | tee -a $gdat
echo "# n err_l2 err_h1 err_linf" | tee -a $gdat
for n in $n_set; do
  geo="tmp-$n.geo"
  log="tmp-$n.log"
  ue=`echo $e | sed -e 's/u//'`
  if test $ball != true; then
    case $e in
     u*) ue=`echo $e | sed -e 's/u//'`
         my_eval "mkgeo_ugrid -$ue $n 2>/dev/null > $geo" ;;
     *)  my_eval "mkgeo_grid  -$e  $n 2>/dev/null > $geo" ;;
    esac
  else
    my_eval "mkgeo_ball ${ball_arg} -$ue $n -order $order > $geo"
  fi
  my_eval "$RUN ./${name} $geo ${Pk} 2>/dev/null | $RUN ./${errname}_error > $log 2>/dev/null"
  err_l2=`cat $log | grep l2 | gawk '{print $2}'`
  err_h1=`cat $log | grep h1 | gawk '{print $2}'`
  err_linf=`cat $log | grep linf | gawk '{print $2}'`
  echo "$n $err_l2 $err_h1 $err_linf" | tee -a $gdat
  rm -f $geo $log
done
# ------------------------------------------
# pente
# ------------------------------------------
log="tmp.log"
rm -f $log
(gnuplot $tmp_pente 2>&1) > $log
pente_l2=`cat $log | grep pente_l2 | gawk '{printf ("%8.2f\n",$3)}'`
pente_h1=`cat $log | grep pente_h1 | gawk '{printf ("%8.2f\n",$3)}'`
pente_linf=`cat $log | grep pente_linf | gawk '{printf ("%8.2f\n",$3)}'`

gnuplot $plot

echo "! file \"$gdat\" created" 1>&2
echo "! file \"$plot\" created" 1>&2

echo "# $name $Pk : pente"
echo "# elt	pente_l2	pente_h1	pente_linf"
echo "$e	$pente_l2	$pente_h1	$pente_linf"

