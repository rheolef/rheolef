namespace rheolef { /**
@page forum_page Mailing list

- mail to the list: mailto:rheolef@framalistes.org
- suscribe: https://framalistes.org/sympa/subscribe/rheolef
- unsuscribe: https://framalistes.org/sympa/sigrequest/rheolef
- archive: https://framalistes.org/sympa/arc/rheolef
