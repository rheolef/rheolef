#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
html=${1-"html"}

# browse files: detail level=2 by default
# see https://stackoverflow.com/questions/33518424/configure-class-list-view-to-use-level-2-by-default
sed -e '/init_search/s,init_search();,init_search();  toggleLevel(1);,' \
  < $html/files.html \
  > $html/files.html.new && \
mv $html/files.html.new $html/files.html

