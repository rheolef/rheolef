@page changes_page Changelogs

<!--
future developments
--------------------
* slip boundary conditions strongly imposed (TODO)
* periodic boundary conditions (TODO)
* Bernstein & Dubiner elements (TODO)
Download via git.
-->

30 may 2022 (version 7.2, stable)
---------------------------------
- HDG and HHO methods improved
- Raviart-Thomas element
- tensor diffusion and weighted DG examples

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-7.2.tar.gz"><B>rheolef-7.2.tar.gz</B></A> (44 mb).

22 march 2020 (version 7.1, oldstable)
-----------------------------------
- reference manual renewed and new web site with doxygen
- hybrid discontinuous Galerkin (HDG & HHO) methods
- user's manual introduces new examples:
  * HDG and HHO examples
  * zalesak & leveque examples: discontinuous Galerkin
  * slip boundary conditions by regularization
- visualization:
  * branch: run animation with paraview
  * high order FEM and curved boundaries: with paraview and gmsh
- eigen sparse solver (sequential)
- mac-osx porting with clang
- minor bug fixes

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-7.1.tar.gz"><B>rheolef-7.1.tar.gz</B></A> (35 mb).

19 february 2018 (version 7.0)
-----------------------------------------
- complex fluids applications: viscoplaticity, viscoelasticity
- continuation nonlinear solver and path following methods
- bug fixes related to the parallel/distributed case:
    - mesh adaptation with bamg and gmsh 
    - reinterpolate between two meshes 
    - memory management improved

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-7.0.tar.gz"><B>rheolef-7.0.tar.gz</B></A> (36 mb).

6 june 2016 (version 6.7)
-------------------------
- discontinuous Galerkin method: limiters and documentation for nonlinear
  hyperbolic problems
- direct distributed solver: new mumps+scotch combined support
- float128 support : quadruple precision
- debian packaging and distribution (administrative upgrade)
- minor bug fixes

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-6.7.tar.gz"><B>rheolef-6.7.tar.gz</B></A> (35 mb).

14 sept 2015 (version 6.6)
--------------------------
- direct solver:
  * enhanced umfpack/cholmod support
  * sparse matrix determinant computations in solver (mumps and umfpack)
- visualization: paraview support
- variadic support for i/o with branch class and compose(f,u1..uN) expressions
- Q2-P1d element for Stokes problem on quadrilateral meshes
- piola inverse transformation in nonlinear cases (quadrangles, curved)
  with damped_newton algorithms
- write in append mode for gziped parallel output files (branch; etc)

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-6.6.tar.gz"><B>rheolef-6.6.tar.gz</B></A> (35 mb).

17 sept 2013 (version 6.5)
--------------------------
- discontinuous Galerkin method

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-6.5.tar.gz"><B>rheolef-6.5.tar.gz</B></A> (35 mb).

8 may 2013 (version 6.4)
------------------------
- flexible expressions for linear and bilinear forms construction

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-6.4.tar.gz"><B>rheolef-6.4.tar.gz</B></A> (32 mb).

19 october 2012 (version 6.3)
-----------------------------
- porting improved on various massively parallel machines
- minor bug fixes
- debian packaging and distribution (administrative upgrade)

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-6.3.tar.gz"><B>rheolef-6.3.tar.gz</B></A> (32 mb).

2 july 2012 (version 6.2)
-------------------------
- improvments for nonlinear solvers (see p-laplacian example)
- sparse matrix-matrix product in distributed memory mode
- minor bug fixes

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-6.2.tar.gz"><B>rheolef-6.2.tar.gz</B></A> (32 mb).

15 may 2012 (version 6.1)
-------------------------
- high order Pk Lagrange interpolation rewritten
- improvments for high order approximations
- portage: intel c++ 12.0 and gnu c++ 4.7 supports

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-6.1.tar.gz"><B>rheolef-6.1.tar.gz</B></A> (31 mb).

10 april 2012 (version 6.0)
---------------------------
- distributed memory and computation support (MPI based)
- support for high order approximations and curved domain
- library code has been completely rewritten

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-6.0.tar.gz"><B>rheolef-6.0.tar.gz</B></A> (27 mb).


23 march 2011 (version 5.93)
----------------------------
- debian packaging and distribution (administrative upgrade)

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-5.93.tar.gz"><B>rheolef-5.93.tar.gz</B></A> (13 mb).

8 february 2011 (version 5.92)
------------------------------
- debian packaging and distribution (administrative upgrade)

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-5.92.tar.gz"><B>rheolef-5.92.tar.gz</B></A> (13 mb).

29 november 2010 (version 5.91)
-------------------------------
- namespace `rheolef` added
- debian packaging and distribution (administrative upgrade)

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-5.91.tar.gz"><B>rheolef-5.91.tar.gz</B></A> (13 mb).

8 november 2010 (version 5.90)
------------------------------
- official debian packaging started

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-5.90.tar.gz"><B>rheolef-5.90.tar.gz</B></A> (13 mb).

16 march 2010 (version 5.89)
----------------------------
- mayavi2 support for 2d and 3d rendering (stereo vision, etc)
- generic damped newton method (documented with the p-laplacian)
- new improved preconditioned conjugate gradient for Navier-Stokes & incompressible elasticity (Cahouet-Chabart type)
- gmsh support for unstructured mesh input-output.

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-5.89.tar.gz"><B>rheolef-5.89.tar.gz</B></A> (13 mb).

7 october 2009 (version 5.76)
-----------------------------
- paraview support for easy animations
- new preconditioned conjugate gradient and minres for navier-stokes & incompressible elasticity
- debian i386 binary distribution

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-5.76.tar.gz"><B>rheolef-5.76.tar.gz</B></A> (12 mb).

20 march 2009 (version 5.62)
----------------------------
- mayavi support: see user manual (vtk support replacement)
- advanced support for tensorial fields and visualization as ellipsoids
- fixes for GNU C++ 4.1, 4.2 and 4.3 compilers

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-5.62.tar.gz"><B>rheolef-5.62.tar.gz</B></A> (8849 kb).

17 january 2006 (version 5.18)
------------------------------
- umfpack multifrontal solver library support
- new elementary matrix code, based on GiNaC C++ library
- debian binary and cvs source distributions
- fixes for GNU C++ 4.0 compiler

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-5.18.tar.gz"><B>rheolef-5.18.tar.gz</B></A> (5508 kb).

27 march 2003 (version 4.74)
----------------------------
- characteristic method for convection-difusion, time-dependent problems
  and Navier-Stokes equations.
- animation for time-dependent bidimensional problems
- support for taucs out-of-core sparse direct solver
- support for qmg mesh generator (2d, 3d)
- support for doxygen documentation
- elementary forms uses dynamic loadable modules
- port on compacq/dec alpha 64 bits workstations
- source code honnors ansi c++ standards (headers, namespace std, ...)
- elementary forms uses dynamic loadable modules
- support for dmalloc (runtime memory debug)

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-4.74.tar.gz"><B>rheolef-4.74.tar.gz</B></A> (5360 kb).

22 january 2002 (version 3.105)
-------------------------------
- linear elasticity in 2D and 3D, with P1 and P2 elements,
  including the incompressible and nearly incompressible elasticity
- auto-adaptive mesh based for 2D problems
- axisymetric problems
- multi-regions and non-constant coefficients

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-3.105.tar.gz"><B>rheolef-3.105.tar.gz</B></A> (3492 kb).

21 february 2001 (version 3.50)
-------------------------------
- users guide developped; stokes solver documented;
- stokes 3d; vector visualization (plotmtv,vtk);
- bug fix for some vector-valued field operators.

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-3.50.tar.gz"><B>rheolef-3.50.tar.gz</B></A> (2616 kb).

30 january 2001 (version 3.33)
------------------------------
- first public release;
- Poisson problem for 2d and 3d geometries;
- support for spooles library version 2.2;
- configuration fixes on linuxes for shared libraries.

Download <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef-3.33.tar.gz"><B>rheolef-3.33.tar.gz</B></A> (2164 kb).
