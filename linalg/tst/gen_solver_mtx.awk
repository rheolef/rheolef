#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
BEGIN {
    # 1d laplace = tridiag(-1,2,-1)[n], n >= 0
    if (n > 1) {
      nnz = n + 2*(n-1);
    } else {
      nnz = n;
    }
    print "%%MatrixMarket matrix coordinate real general"
    print n " " n " " nnz;
    if (n == 1) {
      print "1 1 2";
    } else if (n > 1) {
      print "1 1 2";
      print "1 2 -1";
      for (i = 2; i <= n-1; i++) {
        print i " " i-1 " -1";
        print i " " i   " 2";
        print i " " i+1 " -1";
      }
      print n " " n-1 " -1";
      print n " " n   " 2";
    }
  }
