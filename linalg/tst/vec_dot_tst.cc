///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/disarray.h"
#include "rheolef/diststream.h"
#include "rheolef/dis_accumulate.h"
#include "rheolef/dis_inner_product.h"
#include "rheolef/environment.h"
using namespace rheolef;
int main(int argc, char**argv)
{
    environment rheolef(argc, argv);
    disarray<Float> x(100, Float(2.0)/Float(3.0));
    Float sum_x    = dis_accumulate    (x.begin(),x.size(),x.comm()); 
    Float norme2_x = dis_inner_product (x.begin(),x.begin(),x.size(),x.comm()); 
    dout << std::setprecision(10) 
          << "dot(x,1) = " << sum_x    << std::endl
          << "dot(x,x) = " << norme2_x << std::endl;
    return 0;
}
