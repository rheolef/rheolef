///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// Bug fixed in C=A-B csr when different sparse structure:
//  when A(i,j)=0 and B(i,j)!=0 then C(i,j) was B(i,j) instead of -C(i,j) 
//
#include "rheolef/linalg.h"
using namespace rheolef;
using namespace std;
#include "rounder2.h"

bool do_trunc = false;

Float tol = 1e-7;

template<class M>
void check (const csr<Float,M>& b, const char* expr, int sub_prec = 1)
{
    static int i = 0;
    dout << "e1=" << expr    << ";\n";
    dout << setbasename("e2") << matlab << csr_apply (rounder_type<Float>(tol),b);

    if (!do_trunc) {
        dout << "error" << ++i << "=norm(e1-e2)\n\n";
    } else {
	if (sub_prec == 1) {
            dout << "error" << ++i << "=eps1*round(norm(e1-e2)/eps1)\n\n";
	} else {
	    dout << "eps2=eps1^(1.0/" << sub_prec << ");\n";
            dout << "error" << ++i << "=eps2*round(norm(e1-e2)/eps2)\n\n";
	}
    }
}
template<class M>
void
run ()
{
    csr<Float,M> a,b;
    din >> a >> b;

    dout << matlab
         << setbasename("a") << csr_apply (rounder_type<Float>(tol),a)
         << setbasename("b") << csr_apply (rounder_type<Float>(tol),b);

    csr<Float,M> c = a-b;
    check (c, "a-b");
}
// usage: prog -ndigit n {-seq|-dis}
int main (int argc, char* argv[])
{
    environment rheolef (argc, argv);
    if (argc >= 3) {
        // avoid machine-dependent output digits in non-regression mode:
	do_trunc = true;
        int digits10 = atoi(argv[2]);
        tol = pow(Float(10.0),-digits10);
	dout << "eps1=sqrt(10^(-" << digits10 << "));\n";
    }
    communicator comm;
    if ((argc > 3 && string(argv[3]) != "-seq") || comm.size() > 1) {
#ifdef _RHEOLEF_HAVE_MPI
      warning_macro ("distributed...");
      run<distributed>();
#endif // _RHEOLEF_HAVE_MPI
    } else {
      warning_macro ("sequential...");
      run<sequential>();
    }
}
