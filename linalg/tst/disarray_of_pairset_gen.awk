#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
BEGIN{
    if (n == 0) n = 100;
    if (m == 0) m = 20;
    print n;
    for (i = 1; i <= n; i++) {
      printf ("%d\t", m);
      for (j = 1; j <= m; j++) {
        k = (i-1) + 10000*j;
        val = k/10000.0;
        printf ("  %d %g", k, val);
      }
      printf ("\n");
    }
  }

