///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// test of vec cstor from initializer list (c++ 2011)
//
#include "rheolef/vec_concat.h"
#include "rheolef/csr_concat.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;
int main (int argc, char **argv)
{ 
  environment rheolef (argc, argv);
  csr<Float> a;
  din >> a;
  distributor ownership = a.row_ownership();
  vec<Float> x (ownership);
  size_t first_i = ownership.first_index();
  for (size_t i = 0, n = x.size(); i < n; i++) {
    x[i] = first_i + i;
  }
  csr<Float> A = {{a, x},
                  {a, x}}; 
  dout << "A = "<< endl << A; // note: the result depends upon nproc !
}
