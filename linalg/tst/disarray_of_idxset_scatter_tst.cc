///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI
#include "rheolef/disarray.h"
#include "rheolef/index_set.h"
#include "rheolef/environment.h"
using namespace rheolef;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  communicator comm;
  check_macro (comm.size() == 2 || comm.size() == 3, "expect np=2 or 3");
  size_t dis_n;
  din >> dis_n;
  disarray<index_set> x (dis_n);
  x.get_values (din);
  std::set<size_t> ext_idx_set;
  switch (comm.rank()) {
       case 0:
		ext_idx_set.insert (3);
		ext_idx_set.insert (5);
		break;
       case 1:
		ext_idx_set.insert (0);
		ext_idx_set.insert (1);
		break;
       case 2:
		ext_idx_set.insert (0);
		ext_idx_set.insert (3);
		break;
       default: error_macro ("unsupported np >= 4");
  }
  std::map <size_t,index_set> ext_idx_map;
  x.get_dis_entry (ext_idx_set, ext_idx_map);
  // print in rank-dependent files:
  std::string filename = "disarray_of_idxset_scatter_tst-" + std::to_string(comm.rank());
  std::ofstream out (filename.c_str());
  for (std::map<size_t,index_set>::const_iterator iter = ext_idx_map.begin(), last = ext_idx_map.end();
        iter != last; iter++) {
      out << "proc " << comm.rank() << ": x[" << (*iter).first << "] = " << (*iter).second << std::endl;
  }
}
#endif // _RHEOLEF_HAVE_MPI
