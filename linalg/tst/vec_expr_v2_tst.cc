///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/vec_expr_v2.h"
#include "rheolef/diststream.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;

#ifdef TO_CLEAN
// check SFINAE type filtering
struct bug_expr_t {};
#endif // TO_CLEAN

int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  size_t n = (argc < 2) ? 5 : atoi(argv[1]);
  vec<Float> u(n, 1), w(n, 0);
  vec<Float> v = -u;
  w = u-v;
  w = 1-v;
  w = u+1;
  w = 2*u;
  w = u*2;
  w = u/2;
  w += u;
  w *= 2;
  w /= 3;
  Float value = w.max();
  Float expected = 1;
  Float err1 = max(fabs(w.min()-expected), fabs(w.max()-expected));
  dout << "min = " << w.min() << " max = " << w.max() << endl;
  dout << "expected = " << expected << endl;
  dout << "err1 = " << err1 << endl;
  // w.put_values (dout);

  Float s = dot (1, 2*u);
  Float expected2 = 2*Float(int(u.dis_size()));
  Float err2 = fabs(s - expected2);
  dout << "s = " << s << endl;
  dout << "expected2 = " << expected2 << endl;
  dout << "err2 = " << err2 << endl;

  Float s2 = dot (u-v, u);
  Float err3 = fabs(s2 - expected2);
  dout << "s2 = " << s << endl;
  dout << "expected2 = " << expected2 << endl;
  dout << "err3 = " << err3 << endl;

  Float err = max(err1, max(err2,err3));
  return (err < 1e-10) ? 0 : 1;

#ifdef TO_CLEAN
  // check that the Sfinae technoique filters some invalid exprs
  // => this should not compile:
  vec<double> bug(n,0);
  bug_expr_t bug_expr;
  dout << "typedecl(bug) = " << pretty_typename_macro(decltype(bug)) << endl;
  dout << "typedecl(bug_expr) = " << pretty_typename_macro(decltype(bug_expr)) << endl;
  bug = bug_expr;
  dout << "bug.dis_size = " << bug.dis_size() << endl;
  bug.put_values (dout);
  vec<double> bug2 = bug_expr;
  dout << "bug2.dis_size = " << bug2.dis_size() << endl;
  bug2.put_values (dout);
#endif // TO_CLEAN
}
