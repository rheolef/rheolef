///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// test of the solver
//
#include "rheolef/linalg.h"
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) { 
  environment rheolef (argc, argv);
  // read the matrix
  csr<Float> a;
  din >> a;

  // scan command line options:
  solver_option opt;
  bool use_precond = false;
  for (size_t i = 1; i < size_t(argc); ++i) {
         if (argv[i] == string("-direct"))     { opt.iterative = false; }
    else if (argv[i] == string("-eigen"))      { opt.preferred_library += " eigen";   opt.iterative = false; }
    else if (argv[i] == string("-suitesparse")){ opt.preferred_library += " suitesparse"; opt.iterative = false; }
    else if (argv[i] == string("-mumps"))      { opt.preferred_library += " mumps";   opt.iterative = false; }
    else if (argv[i] == string("-iterative"))  { opt.iterative = true; }
    else if (argv[i] == string("-precond"))    { use_precond = true;  opt.iterative = true; }
    else if (argv[i] == string("-noprecond"))  { use_precond = false; opt.iterative = true; }
    else if (argv[i] == string("-sdp"))        { a.set_symmetry(true);  a.set_definite_positive(true); }
    else if (argv[i] == string("-nosdp"))      { a.set_symmetry(false); a.set_definite_positive(false); }
    else error_macro ("invalid option: " << argv[i]);
  }
  Float tol = sqrt(numeric_limits<Float>::epsilon());

  // convert csr a to csc trans(a) pastix format (with a transpose)
  opt.verbose_level = 5;
  opt.compute_determinant = true;
warning_macro("main(1)");
  solver sa (a, opt);
warning_macro("main(2)");
  if (!opt.iterative) {
    dis_warning_macro ("direct library used: " << sa.name());
    dis_warning_macro ("det(a)="<< sa.det().mantissa<<"*"<<sa.det().base<<"^("<<sa.det().exponant<<")");
  } else if (use_precond) {
    if (a.is_symmetric()) {
      sa.set_preconditioner (mic(a));
    } else {
      sa.set_preconditioner (ilut(a));
    }
    dis_warning_macro ("iterative uses preconditioner");
  }
  // set x(dis_i) = dis_i
  vec<Float> x_ex (a.row_ownership());
  for (size_t i = 0; i < x_ex .size(); i++) {
    x_ex [i] = i + x_ex .ownership().first_index();
  }
  // compute the rhs and solve
  vec<Float> b = a*x_ex ;
warning_macro("main(3)");
  vec<Float> x = sa.solve (b);
warning_macro("main(4)");

  // check the result
  Float err = sqrt(dot (x-x_ex,x-x_ex));
  dis_warning_macro ("error=|x-x_ex|="<< setprecision(15) << err);

  // build another matrix with the same sparse structure
  csr<Float> a2 = a+a;
  if (a.is_symmetric() &&  a.is_definite_positive()) {
    a2.set_symmetry(true);  a2.set_definite_positive(true);
  } else {
    a2.set_symmetry(false); a2.set_definite_positive(false);
  }
  sa.update_values (a2); 
  vec<Float> b2 = a2*x_ex ;
  x = sa.solve (b2);
  Float err2 = sqrt(dot (x-x_ex,x-x_ex));
  dis_warning_macro ("error2=|x-x_ex|="<< setprecision(15) << err2);
  if (max(err,err2) >= tol) dis_warning_macro ("solver failed"); 
  return max(err,err2) < tol ? 0 : 1;
}
