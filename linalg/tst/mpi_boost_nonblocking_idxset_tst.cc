///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI

#include "rheolef/index_set.h"
#include "rheolef/rheostream.h"
#include <boost/mpi.hpp>
using namespace std;
using namespace rheolef;
int main(int argc, char** argv) {
  mpi::environment rheolef(argc, argv);
  mpi::communicator comm;
  check_macro (comm.size() == 2, "expect np=2");
  index_set msg;
  if (comm.rank() == 0) {
    mpi::request reqs[2];
    index_set out_msg;
    out_msg += 100;
    out_msg += 200;
    reqs[0] = comm.isend(1, 0, out_msg);
    reqs[1] = comm.irecv(1, 1, msg);
    mpi::wait_all(reqs, reqs + 2);
  } else {
    mpi::request reqs[2];
    index_set out_msg;
    out_msg += 101;
    out_msg += 201;
    reqs[0] = comm.isend(0, 1, out_msg);
    reqs[1] = comm.irecv(0, 0, msg);
    mpi::wait_all(reqs, reqs + 2);
  }
  std::string filename = "mpi_boost_nonblocking_idxset_tst-" + std::to_string(comm.rank());
  std::ofstream out (filename.c_str());
  out << "proc " << comm.rank() << ": " << msg << std::endl;
}
#endif // _RHEOLEF_HAVE_MPI
