///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// asembly for proc=0 and distribute to others ; then print
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI

#include "rheolef/asr.h"
#include "rheolef/environment.h"
using namespace std;
using namespace rheolef;
int main(int argc, char**argv)
{
  environment rheolef(argc, argv);
  communicator comm;
  check_macro (comm.size() == 2, "expect np=2");
  size_t dis_n = 3;
  distributor ownership (dis_n, comm, (comm.rank() == 0) ? 2 : 1);
  asr<Float> a(ownership,ownership);
  if (comm.rank() == 0) {
    a.dis_entry (0, 0) += 1;
    a.dis_entry (1, 0) += 1;
    a.dis_entry (1, 1) += 1;
    a.dis_entry (0, 1) += 1;
  } else {
    a.dis_entry (1, 1) += 1;
    a.dis_entry (2, 1) += 1;
    a.dis_entry (2, 2) += 1;
    a.dis_entry (1, 2) += 1;
  }
  a.dis_entry_assembly();
  // check that a.dis_at(1,1) == 2
  dout << a;
}
#endif // _RHEOLEF_HAVE_MPI
