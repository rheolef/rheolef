#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
SRCDIR=${SRCDIR-"."}
TOP_SRCDIR=${TOP_SRCDIR-"../.."}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0
#loop_mpirun "./pcg_tst < solver_tst.mtx 2>/dev/null >/dev/null"
#if test $? -ne 0; then status=1; fi

#TODO: also check small matrices, but pastix is buggy
#exit $status

i=5;
while test $i -ge 0; do
    run "echo | LANG=C gawk -v n=$i -f ${SRCDIR}/gen_solver_mtx.awk > solver-$i.mtx"
    if test $? -ne 0; then status=1; break; fi

    loop_mpirun "./pcg_tst < solver-$i.mtx 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi

    run "rm -f solver-$i.mtx"
    if test $? -ne 0; then status=1; break; fi

    i=`expr $i - 1`
done

exit $status
