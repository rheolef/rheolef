///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check whether boost::mpi is abble to send efficiently pair<size_t,double>
//
// it was a bug with pair<const size_t,double> that was not coorectly treated
// while it appears in std::map<size_t,double> with pair_set
//
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI
#include "rheolef/diststream.h"
#include "rheolef/mpi_pair_datatype.h"
#include "rheolef/pretty_name.h"
#include "rheolef/environment.h"
using namespace rheolef;
#define check_type(name,T) \
    dout << name<<" = "<<typename_macro(T)<<" -> " << typename_macro (boost::mpi::is_mpi_datatype<T>::type) << std::endl;

// ======================================================
int main(int argc, char**argv) {
// ======================================================
    environment rheolef(argc, argv);

    typedef unsigned int T1;
    typedef unsigned int const T2;
    typedef double T3;
    typedef std::pair<T1,T3> P13;
    typedef std::pair<T2,T3> P23;
    typedef std::pair<T1, P23> P123;
    typedef std::pair<T1, P13> P113;

    check_type ("T1",T1);
    check_type ("T2",T2);
    check_type ("T3",T3);
    check_type ("P13",P13);
    check_type ("P23",P23);
    check_type ("P113",P113);
    check_type ("P123",P123);
}
#endif // _RHEOLEF_HAVE_MPI
