///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/vec_expr_v2.h"
#include "rheolef/diststream.h"
#include "rheolef/pretty_name.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;

struct phi : unary_function<Float,Float> {
  Float operator() (const Float& x) { return 2*x; }
};
int main(int argc, char**argv) {
    environment rheolef(argc, argv);
    size_t n = (argc < 2) ? 5 : atoi(argv[1]);
    vec<Float> x(n, 2.5);
    vec<Float> y(n, 4.);

    // set size and then assign:
    vec<Float> z(n); 
    z = 2*x - y;
    dout << "z = 2*x-y : z.dis_size = " << z.dis_size() << endl;
    z.put_values (dout);
    
    // size is zero and then assign:
    vec<Float> z1; 
    z1 = 2*x - y;
    dout << "z1.dis_size = " << z1.dis_size() << endl;
    z1.put_values (dout);
    
    // copy cstor:
    vec<Float> z2 = 2*x - y;
    dout << "z2.dis_size = " << z2.dis_size() << endl;
    z2.put_values (dout);

    // dot with expression (lazy evaluation)
    dout << "dot(x, z) = " << dot(x, z) << endl;
    dout << "dot(x, 2*x-y) = " << dot(x, 2*x - y) << endl;
    dout << "dot(2*x-y, x) = " << dot(2*x-y, x) << endl;
    dout << "dot(3*x-y, 2*x-y) = " << dot(3*x-y, 2*x-y) << endl;

#ifdef TO_CLEAN    
    // set bad size and then assign: it may exit(1)
    vec<Float> z3(n+1); 
    z3 = 2*x - y;
    dout << "z3 = " << z3.size() << endl;
    z3.put_values (dout);
#endif // TO_CLEAN    
}
