///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() {return 0;}
#else // _RHEOLEF_HAVE_MPI

#include "rheolef/distributor.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;
// -------------------------------------------------------------
// send a pair<size_t,T> via mpi & serialization
// -------------------------------------------------------------
// non-intrusive version of serialization for pair<size_t,T> 
namespace boost {
 namespace serialization {
  template<class T, class Archive>
  void serialize (Archive & ar, std::pair<size_t, T>& g, const unsigned int version) {
    ar & g.first;
    ar & g.second;
  }
 } // namespace serialization
} // namespace boost
// -------------------------------------------------------------
// usage
// -------------------------------------------------------------
typedef pair<size_t,double> pair_t;
int main(int argc, char* argv[]) {
  environment rheolef(argc, argv);
  mpi::communicator comm;
  if (comm.size() != 2) return 0;
  if (comm.rank() == 0) {
    pair_t p_send (1,1.1);
    comm.send(1, 0, p_send);
    pair_t p_recv;
    comm.recv(1, 1, p_recv);
    cout << "[0]{" << p_recv.first << "," << p_recv.second << "}" << endl << flush;
  } else {
    pair_t p_recv;
    comm.recv(0, 0, p_recv);
    cout << "[1]{" << p_recv.first << "," << p_recv.second << "}" << endl << flush;
    pair_t p_send (2,2.2);
    comm.send(0, 1, p_send);
  }
}
#endif // _RHEOLEF_HAVE_MPI
