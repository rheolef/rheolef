///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/pair_set.h"
#include "rheolef/environment.h"
using namespace rheolef;
using std::make_pair;
// ======================================================
int main(int argc, char**argv) {
// ======================================================
    environment rheolef(argc, argv);
    pair_set<Float> a;
    a += make_pair(101, 1.2);
    a += make_pair(201, 2.2);
    a += make_pair(301, 3.3);
    a += make_pair(301, 3.3);
    std::cout << "a = " << a << std::endl;
    pair_set<Float> b;
    b += make_pair(101, 1.2);
    std::cout << "b = " << b << std::endl;
    a += b;
    std::cout << "a = " << a << std::endl;
}
