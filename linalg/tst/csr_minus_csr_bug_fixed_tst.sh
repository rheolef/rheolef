#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
SRCDIR=${SRCDIR-"."}
TOP_SRCDIR=${TOP_SRCDIR-"../.."}
NPROC_MAX=${NPROC_MAX-"7"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test "$QD_EXT" = ".float128"; then
  ndigit="8"; # rounding problems
else
  ndigit="10";
fi

run "./csr_minus_csr_bug_fixed_tst -ndigit $ndigit -seq < $SRCDIR/csr_minus_csr_bug_fixed.mtx 2>/dev/null | diff -Bw ${SRCDIR}/csr_minus_csr_bug_fixed${QD_EXT}.m.valid - > /dev/null"
if test $? -ne 0; then status=1; fi

if test "$MPIRUN" != ""; then
  loop_mpirun "./csr_minus_csr_bug_fixed_tst -ndigit $ndigit -dis < $SRCDIR/csr_minus_csr_bug_fixed.mtx 2>/dev/null | diff -Bw ${SRCDIR}/csr_minus_csr_bug_fixed${QD_EXT}.m.valid - > /dev/null"
  if test $? -ne 0; then status=1; fi
fi

exit $status
