#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/linalg/tst"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test "$QD_EXT" = ".float128"; then QD_EXT=""; fi

status=0

run "./vec_concat_tst 2>/dev/null | diff $SRCDIR/vec_concat_tst${QD_EXT}.1.valid - >/dev/null"
if test $? -ne 0; then status=1; fi

if test "$MPIRUN" != "" -a "$NPROC_MAX" -ge 2; then
  run "$MPIRUN -np 2 ./vec_concat_tst 2>/dev/null | diff $SRCDIR/vec_concat_tst${QD_EXT}.2.valid - >/dev/null"
  if test $? -ne 0; then status=1; fi
fi

if test "$MPIRUN" != "" -a "$NPROC_MAX" -ge 3; then
  run "$MPIRUN -np 3 ./vec_concat_tst 2>/dev/null | diff $SRCDIR/vec_concat_tst${QD_EXT}.3.valid - >/dev/null"
  if test $? -ne 0; then status=1; fi
fi

exit $status
