#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/linalg/tst"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

echo "      BUG_REGRESSION_MPI (skipped))"
exit 0

status=0
if test "$MPIRUN" = ""; then
  echo "      (skipped)"
else
  run "gawk -f $SRCDIR/disarray_of_idxset_gen.awk > tmp.valid" 
  loop_mpirun "./disarray_of_idxset_io_tst < tmp.valid 2> /dev/null | diff -w -B tmp.valid - >/dev/null" 
  status=$?
  run "rm -f tmp.valid" 
fi
exit $status
