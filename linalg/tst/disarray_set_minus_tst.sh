#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/linalg/tst"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test "$MPIRUN" = "" || test "$NPROC_MAX" -lt 3; then
  echo "      skipped (requiers np >= 3)"
else
  run "$MPIRUN -np 3 ./disarray_set_minus_tst 2> /dev/null | diff -w -B $SRCDIR/disarray_set_plus_tst.valid - >/dev/null" 
  status=$?
  exit $sattus
fi
