#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
SRCDIR=${SRCDIR-"."}
TOP_SRCDIR=${TOP_SRCDIR-"../.."}
NPROC_MAX=${NPROC_MAX-"7"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test "$QD_EXT" = ".float128"; then QD_EXT=""; fi

status=0

L="asr_tst jgl009 bcspwr01"
for matrix in $L; do
    loop_mpirun "./csr_trans_mult_tst < ${SRCDIR}/${matrix}${QD_EXT}.mtx.valid 2>/dev/null | diff ${SRCDIR}/${matrix}${QD_EXT}.trans_mult.valid - >/dev/null"
    if test $? -ne 0; then status=1; fi
done

i=5;
while test $i -ge 0; do
    run "echo | LANG=C gawk -v n=$i -f ${SRCDIR}/gen2_amux${QD_EXT}.awk > dns-$i.trans_mult.valid"
    if test $? -ne 0; then status=1; break; fi

    run "echo | LANG=C gawk -v n=$i -f ${SRCDIR}/gen2_mtx.awk > dns-$i.mtx"
    if test $? -ne 0; then status=1; break; fi

    loop_mpirun "./csr_trans_mult_tst < dns-$i.mtx 2>/dev/null | diff dns-$i.trans_mult.valid - >/dev/null"
    if test $? -ne 0; then status=1; fi

    run "rm -f dns-$i.mtx dns-$i.trans_mult.valid"
    if test $? -ne 0; then status=1; break; fi

    i=`expr $i - 1`
done

exit $status
