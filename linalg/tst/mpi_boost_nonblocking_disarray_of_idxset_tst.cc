///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI

#include "rheolef/index_set.h"
#include "rheolef/environment.h"
#include <boost/mpi.hpp>
#include <boost/serialization/vector.hpp>
using namespace std;
using namespace rheolef;
int main(int argc, char** argv) {
  environment rheolef(argc, argv);
  mpi::communicator world;
  check_macro (world.size() == 2, "expect np=2");
  mpi::request reqs[2];
  vector<index_set> msg    (2, index_set());
  vector<index_set> msg_out(2, index_set());
  if (world.rank() == 0) {
    msg_out[0] += 100;
    msg_out[0] += 200;
    msg_out[1] += 110;
    msg_out[1] += 210;
    reqs[0] = world.isend(1, 0, msg_out);
    reqs[1] = world.irecv(1, 1, msg);
  } else {
    msg_out[0] += 101;
    msg_out[0] += 201;
    msg_out[1] += 111;
    msg_out[1] += 211;
    reqs[0] = world.isend(0, 1, msg_out);
    reqs[1] = world.irecv(0, 0, msg);
  }
  mpi::wait_all(reqs, reqs + 2);
  // print in rank-dependent files:
  std::string filename = "mpi_boost_nonblocking_disarray_of_idxset_tst-" + std::to_string(world.rank());
  std::ofstream out (filename.c_str());
  out << "proc "<<world.rank()<<" : msg[0]=" << msg[0] << std::endl;
  out << "proc "<<world.rank()<<" : msg[1]=" << msg[1] << std::endl;
  return 0;
}
#endif // _RHEOLEF_HAVE_MPI
