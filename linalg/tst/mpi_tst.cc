///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// mpi basic check

#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#include <mpi.h>
#pragma GCC diagnostic pop

#include <iostream>
#include <unistd.h>
int main(int argc, char**argv) {
  MPI_Init (&argc,&argv);
  char hostname [1000];
  gethostname(hostname, 1000);
  int myrank;
  MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
  std::cerr << "coucou from proc " << myrank << " on " << hostname << " !" << std::endl;
  MPI_Finalize ();
}
#endif // _RHEOLEF_HAVE_MPI
