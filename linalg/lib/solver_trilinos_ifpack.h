#ifndef _RHEOLEF_SOLVER_TRILINOS_IFPACK_H
#define _RHEOLEF_SOLVER_TRILINOS_IFPACK_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// solver implementation: interface
//

#include "rheolef/config.h"

#if defined(_RHEOLEF_HAVE_TRILINOS) && defined(_RHEOLEF_HAVE_MPI)

#include "rheolef/solver.h"

#ifdef _RHEOLEF_HAVE_MPI
#  include "mpi.h"
#  include "Epetra_MpiComm.h"
#else // _RHEOLEF_HAVE_MPI
#  include "Epetra_SerialComm.h"
#endif // _RHEOLEF_HAVE_MPI
#include "Epetra_CrsMatrix.h"
#include "Ifpack_Preconditioner.h"

namespace rheolef {

// =======================================================================
// rep
// =======================================================================
template<class T, class M>
class solver_trilinos_ifpack_rep : public solver_abstract_rep<T,M> {
public:

// allocator:

  solver_trilinos_ifpack_rep()
    : solver_abstract_rep<T,M>(solver_option()),
      _ilu_ptr(0),
      _petra_ownership_ptr(0)
    {}
  explicit solver_trilinos_ifpack_rep (const csr<T,M>& a, const solver_option& opt = solver_option());
  void update_values (const csr<T,M>& a);
  bool initialized() const { return true; }
  ~solver_trilinos_ifpack_rep ();

// accessors:

  vec<T,M> trans_solve (const vec<T,M>& rhs) const;
  vec<T,M> solve       (const vec<T,M>& rhs) const;

protected:
// internal:
  void destroy_values ();
  void solve (const vec<T,M>& rhs, bool transpose, vec<T,M>& x) const;
// data:
  Ifpack_Preconditioner*   _ilu_ptr;
  Epetra_Map*              _petra_ownership_ptr;
};

} // namespace rheolef
#endif // defined(_RHEOLEF_HAVE_TRILINOS) && defined(_RHEOLEF_HAVE_MPI)
#endif // _RHEOLEF_SOLVER_TRILINOS_IFPACK_H
