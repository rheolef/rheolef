#ifndef _RHEOLEF_SOLVER_EIGEN_H
#define _RHEOLEF_SOLVER_EIGEN_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// solver implementation: interface
//

#include "rheolef/config.h"

#ifdef _RHEOLEF_HAVE_EIGEN

#include "rheolef/solver.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#include <Eigen/Sparse>
#pragma GCC diagnostic pop

namespace rheolef {

// =======================================================================
// rep
// =======================================================================
template<class T, class M>
class solver_eigen_rep : public solver_abstract_rep<T,M> {
public:
// typedef:

  typedef solver_abstract_rep<T,M>        base;
  typedef typename base::size_type        size_type;
  typedef typename base::determinant_type determinant_type;

// allocator:

  explicit solver_eigen_rep (const csr<T,M>& a, const solver_option& opt = solver_option());
  solver_eigen_rep (const solver_eigen_rep<T,M>&);
  solver_abstract_rep<T,M>* clone() const;
  void update_values (const csr<T,M>& a);
  bool initialized() const { return true; }

// accessors:

  vec<T,M> trans_solve (const vec<T,M>& rhs) const;
  vec<T,M> solve       (const vec<T,M>& rhs) const;
  determinant_type det() const { return _det; }

protected:
// data:
  csr<T,M>           _a;
  Eigen::SparseLU<Eigen::SparseMatrix<T,Eigen::ColMajor>, Eigen::COLAMDOrdering<int> >
	             _superlu_a;
  Eigen::SimplicialLDLT<Eigen::SparseMatrix<T,Eigen::ColMajor>, Eigen::Lower, Eigen::AMDOrdering<int> > 
		     _ldlt_a;
  determinant_type   _det;
};
// -----------------------------------------------------------------------------
// inlined
// -----------------------------------------------------------------------------
template<class T, class M>
inline
solver_eigen_rep<T,M>::solver_eigen_rep (const csr<T,M>& a, const solver_option& opt)
 : solver_abstract_rep<T,M>(opt),
   _a(a),
   _superlu_a(),
   _ldlt_a(),
   _det()
{
  update_values (a);
}
template<class T, class M>
inline
solver_eigen_rep<T,M>::solver_eigen_rep (const solver_eigen_rep<T,M>& x)
 : solver_abstract_rep<T,M>(x.option()),
   _a(x._a),
   _superlu_a(),
   _ldlt_a(),
   _det()
{
  // Eigen::SuperLU copy cstor is non-copyable, so re-initialize for a copy
  update_values (_a);
}
template <class T, class M>
inline
solver_abstract_rep<T,M>*
solver_eigen_rep<T,M>::clone() const
{
  typedef solver_eigen_rep<T,M> rep;
  return new_macro (rep(*this));
}

} // namespace rheolef
#endif // _RHEOLEF_HAVE_EIGEN
#endif // _RHEOLEF_SOLVER_EIGEN_H
