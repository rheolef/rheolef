#ifndef _ASR_TO_CSR_DIST_LOGICAL_H
#define _ASR_TO_CSR_DIST_LOGICAL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

namespace rheolef {
/*F:
NAME: asr_to_csr_dist_logical -- sparse matrix convertion (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Logical pass for the
  distributed "asr" to the distributed "csr" sparse matrix format
  conversion.
  Build the set of column indexes out of
  the current processor range, and return the
  size of this set.
ALGORITHM:
  asr_to_csr_dist_logical

  "input": the sparse asr matrix
  |   ia(0:2*nrow+1), a(0:nnz-1), is_dia(.)
  "output": the external column set
  |   colext(0:ncolext-1)
  begin
  |   for i := 0 to nrow-1 do
  |     for p := ia(2*i) to ia(2*i+1)-1 do
  |       if not is_dia(a(p)) then
  |         colext(ncolext) := a(p)
  |         ncolext := ncolext + 1
  |       endif
  |     endfor
  |   endfor
  end
NOTE:
  Practical implementation build the sorted set
  "colext" by using STL set.

  The predicate "is_dia" returns true when the matrix entry
  has its column in the local column range of the processor.
COMPLEXITY:
  Time and memory complexity is O(nnz + ncolext*log(ncolext)).
METHODS: @asr_to_csr_dist_logical 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   22 march 1999
END:
*/

//<asr_to_csr_dist_logical:
template <
    class InputPtrIterator,
    class Predicate,
    class Set>
typename Set::value_type
asr_to_csr_dist_logical (
    InputPtrIterator iter_ptr_a,
    InputPtrIterator last_ptr_a,
    Predicate        is_dia,
    Set&             colext)
{
    typedef typename std::iterator_traits<InputPtrIterator>::value_type  Row;
    typedef typename Row::const_iterator                                 InputDataIterator;
    typedef typename Set::value_type                                     Size;

    Size nnzext = 0;
    while (iter_ptr_a != last_ptr_a) {
        InputDataIterator iter_data_a = (*iter_ptr_a).begin();
        InputDataIterator last_data_a = (*iter_ptr_a).end();
	iter_ptr_a++;
	while (iter_data_a != last_data_a) {
	    if (!is_dia(*iter_data_a)) {
		colext.insert((*iter_data_a).first);	
		nnzext++;
            }
	    iter_data_a++;
        }
    }
    return nnzext;
}
template <class Size, class Pair>
struct is_dia_t : public std::unary_function<Pair, bool> {
    bool operator()(const Pair& x) const { 
	return x.first >= j1 && x.first < j2; }
    is_dia_t(Size k1, Size k2) : j1(k1), j2(k2) {}
    Size j1, j2;
};
} // namespace rheolef
//>asr_to_csr_dist_logical:
#endif // _ASR_TO_CSR_DIST_LOGICAL_H
