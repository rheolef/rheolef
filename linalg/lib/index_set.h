#ifndef _RHEOLEF_INDEX_SET_H
#define _RHEOLEF_INDEX_SET_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
#include "rheolef/communicator.h"
#include "rheolef/pretty_name.h"
#include "rheolef/msg_util.h"

#ifdef _RHEOLEF_HAVE_MPI
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#include <boost/serialization/set.hpp>
#include <boost/serialization/base_object.hpp>
#pragma GCC diagnostic pop
#endif // _RHEOLEF_HAVE_MPI

#include "rheolef/index_set_header.icc"
#endif // _RHEOLEF_INDEX_SET_H
