#ifndef _RHEO_MSG_UTIL_H
#define _RHEO_MSG_UTIL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// utilities for message exchange basic algorithms
//   implemented with MPI
//
// author: Pierre.Saramito@imag.fr
//
// date: 17 december 1998
//
# include "rheolef/communicator.h"
namespace rheolef {

// there is a non-standard SGI extension like that:
template <class T1, class T2>
struct select1st : std::unary_function<std::pair<T1,T2>, T1> {
  T1 operator() (const std::pair<T1,T2>& x) const { return x.first; }
};
template <class T1, class T2>
struct select2nd : std::unary_function<std::pair<T1,T2>, T2> {
  T2 operator() (const std::pair<T1,T2>& x) const { return x.second; }
};

// always true predicate
template <class T>
struct always_true : std::unary_function<T,bool> {
    bool operator()(const T& x) const { return true; }
};
// index iterator, simulates array[i] = i
template <class Size, class Distance = std::ptrdiff_t>
class index_iterator : public std::iterator<std::input_iterator_tag, Size, Distance, const Size*, const Size&> {
public:
	index_iterator& operator++() { _i++; return *this; }
	index_iterator operator++(int) { 
	    index_iterator<Size,Distance> tmp = *this;
            _i++;
            return tmp;
	}
	const Size& operator*() const { return _i; }
	const Size& operator[](const Size& i) const { return i; }
	bool operator==(const index_iterator<Size,Distance>& x) const{
	    return x._i == _i; }
	bool operator!=(const index_iterator<Size,Distance>& x) const{
	    return !(x._i == _i); }
	index_iterator(Size i0 = 0) : _i(i0) {}
protected:
	Size _i;
};

// f1(pair x) = x.first
template <class Pair>
struct first_op
: public std::unary_function<Pair,typename Pair::first_type> {
    typename std::unary_function<Pair,typename Pair::first_type>::result_type
    operator()(const Pair& x) const {
	return x.first; }
};
// f2(pair x) = x.second
template <class Pair>
struct second_op
  : public std::unary_function<Pair, typename Pair::second_type> {
    typename std::unary_function<Pair,typename Pair::second_type>::result_type
    operator()(const Pair& x) const {
	return x.second; }
};
// pair<const uint, T> and pair<uint, T> are not compatible
// for some C++; so convert it explicitly:
template<class Pair1, class Pair2>
struct pair_identity : public std::unary_function<Pair1, Pair2> {
    Pair2 operator()(const Pair1& x) const {
	return Pair2(x.first, x.second); }
};
// wrapper iterator class that applies an operator
template <class Iterator, class Operator>
class apply_iterator : public std::iterator_traits<Iterator> {
public:
    typedef typename Operator::result_type value_type;
    apply_iterator(Iterator i1, Operator op1)
      : i(i1), op(op1) {}
    apply_iterator& operator++() { 
	i++; return *this; }
    apply_iterator operator++(int) { 
	apply_iterator t = *this; i++; return t; }
    value_type operator*() const { return op(*i); }
    bool operator== (apply_iterator<Iterator,Operator> b) const{ return (i == b.i); }
    bool operator!= (apply_iterator<Iterator,Operator> b) const{ return (i != b.i); }
protected:
    Iterator i;
    Operator op;
};
template <class Iterator, class Operator>
inline
apply_iterator<Iterator,Operator>
make_apply_iterator(Iterator i, Operator op) {
    return apply_iterator<Iterator,Operator>(i,op); 
}
// some c++ cannot convert pair<const I,T> to pair<I,T>:
template <class InputIterator, class OutputIterator>
OutputIterator
msg_pair_copy(InputIterator input, InputIterator last,
OutputIterator result) {
    while (input != last) {
	(*result).first = (*input).first;
  	(*result++).second = (*input++).second;
    }
    return result;
}
} // namespace rheolef
// ----------------------------------------------------------------------------
// generic set_op for the operator= += -= definition
// ----------------------------------------------------------------------------
// author: Pierre.Saramito@imag.fr
// date: 13 april 2020
namespace rheolef { namespace details {

// concept of class_reference
// such as disarray::dis_reference
template<class T>
struct is_class_reference : std::false_type {};

// generic "set_op" used to define
//   operator= += -=
// should work at least when
// - T1=T2=double
// - Reference=disarray::dis_reference and T=double
// - IndexSet=index_set and T=int or size_t
// - PairSet=index_set and T=double and Size=size_t
//
#define _RHEOLEF_generic_set_xxx_op(NAME,OP)			\
struct NAME {							\
  template <class T1, class T2>					\
  typename std::enable_if<					\
    std::is_convertible<T1,T2>::value				\
   ,T2&>::type							\
  operator() (T2& x, const T1& y) const { x OP y; return x; }	\
								\
  template <class T, class Reference>				\
  typename std::enable_if<					\
    !std::is_convertible<T,Reference>::value &&			\
    is_class_reference<Reference>::value &&			\
    std::is_member_function_pointer<				\
      decltype(static_cast<Reference& (Reference::*)(T)>	\
                              (&Reference::operator OP))	\
    >::value							\
   ,Reference>::type						\
  operator() (Reference x, const T& y) const { x OP y; return x; }	\
								\
  template <class T, class Reference>				\
  typename std::enable_if<					\
    !std::is_convertible<T,Reference>::value &&			\
    std::is_class<Reference>::value &&				\
    std::is_member_function_pointer<				\
      decltype(static_cast<Reference& (Reference::*)(const T&)>	\
                              (&Reference::operator OP))	\
    >::value							\
   ,Reference>::type						\
  operator() (Reference x, const T& y) const { x OP y; return x; }	\
								\
  template <class T, class IndexSet>				\
  typename std::enable_if<					\
    std::is_convertible<T,size_t>::value &&			\
    std::is_class<IndexSet>::value &&				\
    std::is_member_function_pointer<				\
      decltype(static_cast<IndexSet& (IndexSet::*)(size_t)>	\
                              (&IndexSet::operator OP))		\
    >::value							\
   ,IndexSet&>::type						\
  operator() (IndexSet& x, const T& y) const { x OP y; return x; }	\
								\
  template <class T, class PairSet, class Size>			\
  typename std::enable_if<					\
    std::is_convertible<Size,size_t>::value &&			\
    std::is_class<PairSet>::value				\
   ,PairSet&>::type						\
  operator() (PairSet& x, const std::pair<Size,T>& y) const { x OP y; return x; }	\
};

       _RHEOLEF_generic_set_xxx_op(generic_set_op,=)
       _RHEOLEF_generic_set_xxx_op(generic_set_plus_op,+=)
       _RHEOLEF_generic_set_xxx_op(generic_set_minus_op,-=)
#undef _RHEOLEF_generic_set_xxx_op

#define _RHEOLEF_generic_set_xxx_op(NAME,FUN)			\
struct NAME {							\
  template <class T1, class T2>					\
  typename std::enable_if<					\
    std::is_convertible<T1,T2>::value				\
   ,T2&>::type							\
  operator() (T2& x, const T1& y) const { return x = FUN(x,T2(y)); } \
};

       _RHEOLEF_generic_set_xxx_op(generic_set_min_op,std::min)
       _RHEOLEF_generic_set_xxx_op(generic_set_max_op,std::max)
#undef _RHEOLEF_generic_set_xxx_op

// this traits will be overloaded by index_set and pair_set
// as generic_set_plus_op:
template <class T>
struct default_set_op_traits {
  using type = generic_set_op;
};

}} // namespace rheolef::details
// ----------------------------------------------------------------------------
// container traits
// ----------------------------------------------------------------------------
#include <boost/serialization/utility.hpp>
#ifdef _RHEOLEF_HAVE_MPI
#include <boost/mpi/datatype.hpp>
#endif // _RHEOLEF_HAVE_MPI

namespace rheolef { namespace details {

template<class T>
struct is_container : std::false_type {
  typedef std::false_type type;
};
#ifdef _RHEOLEF_HAVE_MPI
template <class T>
struct is_container_of_mpi_datatype : std::false_type {
  typedef std::false_type type;
};
#endif // _RHEOLEF_HAVE_MPI
}} // namespace rheolef::details
#endif // _RHEO_MSG_UTIL_H
