#ifndef _RHEO_DIS_INNER_PRODUCT_H
#define _RHEO_DIS_INNER_PRODUCT_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

#include "rheolef/promote.h"
#include "rheolef/communicator.h"
#include <iterator>
namespace rheolef {

/*D:dis_inner_product
NAME:  dis_inner_product - distributed inner product algorithm (@PACKAGE@-@VERSION@)
DESCRIPTION:       
 STL-like inner product for distributed containers
 environment.
SYNOPSIS:
  @example
  template<class InputIterator1, class InputIterator2, class Size>
  T dis_inner_product (
      	InputIterator1 first1, InputIterator2 first2,
        Size n);
  @end example
EXAMPLE:
   A sample usage writes:
   @example
     # include "rheolef/disarray.h"
     # include "rheolef/dis_inner_product.h"
     int main(int argc, char**argv) {
          environment distributed(argc, argv);
         unsigned int n = 100;
         disarray<double> x(n, 2.0);
         double norme2_x = dis_inner_product(x.begin(),x.begin(),x.size(),x.comm());
         dout << "dot(x,x) = " << norme2_x << endl;
     }
   @end example
IMPLEMENTATION NOTE: 
   The std::inner_product(first1,first2,last1) function is similar but not used here.
   Use here both two "first" iterators and the size "n", since
   expression template approach generates iterators as expression
   tree and a comparison like "first1 != last1" becomes complex and
   requires a recursive inspection.
SEE ALSO: "disarray"(1)
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
   | Pierre.Saramito@imag.fr
DATE:   24 november 1998
End:
*/
template <class InputIterator1, class InputIterator2, class Size>
typename promote<
  typename std::iterator_traits<InputIterator1>::value_type,
  typename std::iterator_traits<InputIterator2>::value_type
>::type
dis_inner_product (
    	InputIterator1 first1,
    	InputIterator2 first2,
	Size n,
	const distributor::communicator_type& comm,
	sequential /* memory_model */)
{
    typedef typename std::iterator_traits<InputIterator1>::value_type T;
    T sum = T(0);
    for (Size i = 0; i < n; ++i, ++first1, ++first2) {
        sum = sum + (*first1)*(*first2);
    }
    return sum;
}
#ifdef _RHEOLEF_HAVE_MPI
template <class InputIterator1, class InputIterator2, class Size>
inline
typename promote<
  typename std::iterator_traits<InputIterator1>::value_type,
  typename std::iterator_traits<InputIterator2>::value_type
>::type
dis_inner_product (
    	InputIterator1 first1,
    	InputIterator2 first2,
	Size n,
	const distributor::communicator_type& comm,
	distributed /* memory_model */)
{
    typedef typename std::iterator_traits<InputIterator1>::value_type T;
    T local_sum = dis_inner_product (first1, first2, n, comm, sequential());
    return mpi::all_reduce (comm, local_sum, std::plus<T>());    
}
#endif // _RHEOLEF_HAVE_MPI

template <class InputIterator1, class InputIterator2, class Size>
inline
typename promote<
  typename std::iterator_traits<InputIterator1>::value_type,
  typename std::iterator_traits<InputIterator2>::value_type
>::type
dis_inner_product (
    	InputIterator1 first1,
    	InputIterator2 first2,
	Size n,
	const distributor::communicator_type& comm)
{
    return dis_inner_product (first1, first2, n, comm, rheo_default_memory_model());
}

} // namespace rheolef
#endif // _RHEO_DIS_INNER_PRODUCT_H
