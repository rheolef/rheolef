#ifndef RHEO_MSG_FROM_CONTEXT_INDICES_H
#define RHEO_MSG_FROM_CONTEXT_INDICES_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

# include "rheolef/msg_util.h"
namespace rheolef {

/*F:
NAME: msg_from_context_indices -- gather (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Computes the receive compressed message pattern for gather
  and scatter.
  Suppose indexes are sorted by increasing order.
ALGORITHM:
  msg_from_context_indices

  "input": the owner and indice arrays, the current process
  |   owner(0:nidx-1), idy(0:nidx-1), 
  |   proc2from_proc(0:nproc-1), my_proc
  "input-output": the pointer array, used for accumulation
  |   ptr(0:nidx)
  "output": the receive context (from) indice array
  |   from_idx(0:nidx-1)
  begin
  | for k := 0 to nidx-1 do
  |   iproc := owner(k)
  |   if iproc <> my_proc then
  |     i := proc2from_proc(iproc)
  |     p := ptr(i)
  |     ptr(i) := p + 1
  |     from_idx(p) := idy(k)
  |   endif
  | endfor 
  end
COMPLEXITY:
  Complexity is O(nidx).
TODO:
  Uses input iterators.
METHODS: @msg_from_context_indices 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   6 january 1999
END:
*/

//<msg_from_context_indices:
template <
    class InputIterator1,
    class InputIterator2,
    class InputRandomIterator,
    class Proc,
    class Size,
    class MutableRandomIterator,
    class OutputIterator>
void
msg_from_context_indices (
    InputIterator1 		owner,			// nidx
    InputIterator1		last_owner,	
    InputIterator2 		idy,			// nidx
    InputRandomIterator		proc2from_proc,		// nproc
    Proc			my_proc,
    Size			idy_maxval,
    MutableRandomIterator	ptr,			// send_nproc+1
    OutputIterator      	from_idx)		// nidx
{
    Size nidx = distance(owner,last_owner);
    for (Size i = 0; i < nidx; i++) {
        if (owner[i] != my_proc) {
            assert_macro (idy[i] < idy_maxval, "Scattering past end of TO vector: idy="
                << idy[i] << " out of range 0.." << idy_maxval-1);
            Size p = ptr[proc2from_proc[owner[i]]]++;
            from_idx[p] = idy[i];
        }
    }
}
//>msg_from_context_indices:
} // namespace rheolef
#endif // RHEO_MSG_FROM_CONTEXT_INDICES_H
