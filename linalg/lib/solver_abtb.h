#ifndef _SKIT_SOLVER_ABTB_H
#define _SKIT_SOLVER_ABTB_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   19 january 2012

namespace rheolef {
/**
@linalgclassfile solver_abtb mixed systems solver
@addindex mixed linear problem
@addindex conjugate gradient algorithm
@addindex finite element method
@addindex stabilized mixed finite element method
@addindex Stokes problem
@addindex incompressible elasticity

Synopsis
========
  
        solver_abtb stokes     (a,b,mp); 
        solver_abtb elasticity (a,b,c,mp); 
  
Description
===========
This class provides both direct and iterative algorithms for solving mixed problem:

       [ A  B^T ] [ u ]    [ Mf ]
       [        ] [   ]  = [    ]
       [ B  -C  ] [ p ]    [ Mg ]

where A is symmetric positive definite and C is symmetric positive.
By default, iterative algorithms are considered for tridimensional problems
and direct methods otherwise.
A @ref solver_option_4 argument can change this default behavior.
Mixed linear problems appears for instance with the discretization
of Stokes and elasticity problems.
The C matrix can be zero and then the corresponding argument can be omitted
when invoking the constructor.
Non-zero C matrix appears for of Stokes problems with stabilized P1-P1 element,
or for nearly incompressible elasticity problems.

Recall that, for 1D and 2D problems,
the direct method is default, since it is more efficient:
see e.g. @ref usersguide_page.
The @ref solver_option_4 allows one to change this default behavior.

Direct algorithm
================
When the kernel of `B^T` is reduced to zero, the global system is non-singular
and the @ref solver_4 method could be applied.
Otherwise, when the kernel of `B^T` is not reduced to zero,
then the pressure p is defined up to a constant
and the system is singular.
This is a major difficulty for any direct method.
Thus, the system is first completed by the imposition
of an additional constraint on the pressure term:
it should have a zero average value.
By this way, the system becomes non-singular 
and the @ref solver_4 method could be applied.

Iterative algorithm
===================
The @ref cg_5 preconditionned conjugate gradient algorithm
or the @ref gmres_5 one is used, depending on the symmetry of the matrix.
The `mp` matrix is used as preconditionner:
it can be customized by the `set_preconditionner` member function.
The linear sub-systems related to the `A` matrix are also solved by an inner solver.
This inner solver is automatically defined by default:
it can be customized by the `set_inner_solver` member function
and e.g. uses it own inner iterative algorithm and preconditionner.

Example
=======
See the @ref usersguide_page for practical examples for the nearly incompressible
elasticity, the Stokes and the Navier-Stokes problems.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/solver.h"
#include "rheolef/mixed_solver.h"
#include "rheolef/csr.h"

namespace rheolef {
//<solver_abtb:
template <class T, class M = rheo_default_memory_model>
class solver_abtb_basic {
public:

// typedefs:

  typedef typename csr<T,M>::size_type size_type;

// allocators:

  solver_abtb_basic ();
  solver_abtb_basic (const csr<T,M>& a, const csr<T,M>& b, const csr<T,M>& mp,
       const solver_option& opt     = solver_option());
  solver_abtb_basic (const csr<T,M>& a, const csr<T,M>& b, const csr<T,M>& c, const csr<T,M>& mp,
       const solver_option& opt     = solver_option());

// accessors:

  void solve (const vec<T,M>& f, const vec<T,M>& g, vec<T,M>& u, vec<T,M>& p) const;
  bool initialized() const;
  const solver_option& option() const { return _opt; }
  void set_inner_solver (const solver_basic<T,M>& sa)     { _sa = sa; }
  void set_preconditioner (const solver_basic<T,M>& smp) { _smp = smp; }
  std::string name()       const { return _sA.name(); }
  std::string inner_name() const { return _sa.name(); }
  std::string preconditionner_name() const { return _sa.name(); }
//>solver_abtb:

protected:
// internal
  void init();
// data:
  mutable solver_option _opt;
  csr<T,M>          _a;
  csr<T,M>          _b;
  csr<T,M>          _c;
  csr<T,M>          _mp;
  solver_basic<T,M> _sA;
  mutable solver_basic<T,M> _sa;
  mutable solver_basic<T,M> _smp;
  bool              _need_constraint;
};
//<solver_abtb:
typedef solver_abtb_basic<Float,rheo_default_memory_model> solver_abtb;
//>solver_abtb:

} // namespace rheolef
#endif // _SKIT_SOLVER_ABTB_H
