#ifndef _RHEOLEF_SOLVER_MUMPS_H
#define _RHEOLEF_SOLVER_MUMPS_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// solver implementation: interface
//

#include "rheolef/config.h"

#ifdef _RHEOLEF_HAVE_MUMPS

#include "rheolef/solver.h"
#include "dmumps_c.h"

namespace rheolef {

// =======================================================================
// rep
// =======================================================================
template<class T, class M>
class solver_mumps_rep : public solver_abstract_rep<T,M> {
public:
// typedef:

  typedef solver_abstract_rep<T,M>        base;
  typedef typename base::size_type        size_type;
  typedef typename base::determinant_type determinant_type;

// allocator:

  explicit solver_mumps_rep (const csr<T,M>& a, const solver_option& opt = solver_option());
  solver_abstract_rep<T,M>* clone() const;
  bool initialized() const { return true; }
  void update_values (const csr<T,M>& a);
  ~solver_mumps_rep ();

// accessors:

  vec<T,M> trans_solve (const vec<T,M>& rhs) const;
  vec<T,M> solve       (const vec<T,M>& rhs) const;
  determinant_type det() const { return _det; }

protected:
// data:
  bool                    _has_mumps_instance;
  bool                    _drop_ext_nnz; // when building schur complement
  mutable DMUMPS_STRUC_C  _mumps_par;
  std::vector<MUMPS_INT>  _row; // (i,j) matrix sparse tructure for mumps
  std::vector<MUMPS_INT>  _col;
  std::vector<double>     _val;
  double                  _a00; // circumvent a bug when matrix size <= 1...
  determinant_type        _det;
};
template <class T, class M>
inline
solver_abstract_rep<T,M>*
solver_mumps_rep<T,M>::clone() const
{
  typedef solver_mumps_rep<T,M> rep;
  return new_macro (rep(*this));
}

} // namespace rheolef
#endif // MUMPS
#endif // _RHEOLEF_SOLVER_MUMPS_H
