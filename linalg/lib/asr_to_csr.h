#ifndef _ASR_TO_CSR_H
#define _ASR_TO_CSR_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "get_pointer_from_iterator.h"
namespace rheolef {
/*F:
NAME: asr_to_csr -- sequential sparse matrix convertion (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Convert sequential "asr" to sequential "csr" sparse matrix format.
ALGORITHM:
  asr_to_csr

  "input": the sparse asr matrix, predicate and operator
  |   ia(0:2*nrow+1), a(0:nnz-1), 
  |   pred(.), op(.)
  "output": the sparse csr matrix
  |   ib(0:nrow-1), b(0:nnz-1)
  begin
  |   ib(0) := q := 0
  |   for i := 0 to nrow-1 do
  |     for p := ia(2*i) to ia(2*i+1)-1 do
  |       if pred(a(p)) then
  |         b(q) := op(a(p))
  |         q := q + 1
  |       endif
  |     endfor
  |     ib(i+1) := q
  |   endfor
  end
COMPLEXITY:
  Time and memory complexity is O(nnz).
NOTE:
  The output arrays are supposed to be coorectly allocated,
  i.e. large enough.
METHODS: @asr_to_csr 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   22 march 1999
END:
*/
//<asr_to_csr:
template<
    class InputPtrIterator,
    class Predicate,
    class Operation,
    class OutputPtrIterator,
    class OutputDataIterator>
OutputPtrIterator
asr_to_csr (
    InputPtrIterator   iter_ptr_a,
    InputPtrIterator   last_ptr_a,
    Predicate          pred,
    Operation          op,
    OutputPtrIterator  iter_ptr_b,
    OutputDataIterator iter_data_b)
{
    typedef typename std::iterator_traits<InputPtrIterator>::value_type Row;
    typedef typename Row::const_iterator DataIterator;

    (*iter_ptr_b++) = get_pointer_from_iterator(iter_data_b);
    while (iter_ptr_a != last_ptr_a) {
        const Row& row_i = *iter_ptr_a;
        DataIterator iter_data_a = row_i.begin();
        DataIterator last_data_a = row_i.end();
	while (iter_data_a != last_data_a) {
	    if (pred(*iter_data_a))
	        (*iter_data_b++) = op(*iter_data_a);
            iter_data_a++;
        }
	(*iter_ptr_b++) = get_pointer_from_iterator(iter_data_b);
	iter_ptr_a++;
    }
    return iter_ptr_b;
}
//>asr_to_csr:
} // namespace rheolef
#endif // _ASR_TO_CSR_H
