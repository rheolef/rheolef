#ifndef _RHEO_MPI_PAIR_DATA_TYPE_H
#define _RHEO_MPI_PAIR_DATA_TYPE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// send a pair<T1,T2> via mpi & serialization

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
// -------------------------------------------------------------
// 1) non-intrusive version of serialization for pair<T1,T2> 
// -------------------------------------------------------------
#include <boost/serialization/utility.hpp>
// ---------------------------------------------------------------------------
// 2) Some serializable types, like pair<size_t,double>, have a fixed amount of
// data stored at fixed field positions.
// When this is the case, boost::mpi can optimize their serialization and
// transmission to avoid extraneous copy operations.
// To enable this optimization, we specialize the type trait is_mpi_datatype
// this is done in an header file.
// ---------------------------------------------------------------------------
#include <boost/mpi/datatype.hpp>
#pragma GCC diagnostic pop

// ---------------------------------------------------------------------------
// 3) remove constness
// when is_mpi_datatype<unsigned int>        is true
// then is_mpi_datatype<unsigned int const>  is false
// => remove constness is not performed by is_mpi_datatype<T>
//    we do it here
#ifdef _RHEOLEF_HAVE_MPI
namespace boost {
 namespace mpi {
  template <class T>
  struct is_mpi_datatype<T const> : is_mpi_datatype<T> {};
 } // namespace mpi
} // namespace boost
#endif // _RHEOLEF_HAVE_MPI

#endif // _RHEO_MPI_PAIR_DATA_TYPE_H
