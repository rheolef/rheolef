# ifndef _SKIT_DIAG_H
# define _SKIT_DIAG_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
# include "rheolef/dia.h"

namespace rheolef { 

/*Class:diag
NAME: @code{diag} - get diagonal part of a matrix 
@clindex dia
@clindex vec
@clindex csr
@cindex diagonal matrix
DESCRIPTION:       
  This function get the diagonal part of a matrix.
  @example
     	csr<Float> a;
     	dia<Float> d = diag(a);
  @end example
TODO:
  Build a csr matrix from a diagonal one or from a vector:
  @example
     	dia<Float> d;
     	csr<Float> a = diag(d);
     	vec<Float> u;
     	csr<Float> b = diag(u);
  @end example
End:
*/
//<diag:
template<class T, class M>
dia<T,M> diag (const csr<T,M>& a) 
//>diag:
{
  return dia<T,M>(a);
}

}// namespace rheolef
# endif /* _SKIT_DIAG_H */
