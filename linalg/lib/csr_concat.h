#ifndef _RHEOLEF_CSR_CONCAT_H
#define _RHEOLEF_CSR_CONCAT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// build csr from initializer list
//
#include "rheolef/csr.h"
#include "rheolef/vec_concat.h" // for constraint_process_rank()

namespace rheolef { namespace details {

// -----------------------------------------------------------------------------
// 1) implementation
// -----------------------------------------------------------------------------
//
// 1rst case : one-line matrix initializer
//  A = {a, u};			// matrix & vector
//  A = {trans(u), 3.2};	// trans(vector) & scalar
//
// -----------------------------------------------------------------------------
template <class T, class M>
struct vec_trans {
  vec<T,M> v;
  vec_trans (const vec<T,M>& w) : v(w) {}
};
template <class T, class M>
struct vector_vec_trans {
  std::vector<vec<T,M> > v;
  vector_vec_trans (const std::vector<vec<T,M> >& w) : v(w) {}
};

template <class T, class M>
class csr_concat_value {
public:

// typedef:

  typedef typename csr<T,M>::size_type     size_type;
  typedef std::pair<size_type,size_type>   sizes_type;       // [size,dis_size]
  typedef std::pair<sizes_type,sizes_type> sizes_pair_type;  // [nrow,ncol] = [[nrow,dis_nrow],[ncol,dis_ncol]]

  static constexpr size_type undef = std::numeric_limits<size_type>::max();
  static constexpr size_type zero  = 0;

  typedef enum { empty, scalar, vector, vector_transpose, vector_vec, vector_vec_transpose, matrix} variant_type;

// allocators:

  csr_concat_value (const sizes_pair_type& x)        : e(x), s(),  v(),     vv(),     m(),  variant(empty) {}
  template <class U,
            class Sfinae
                  = typename std::enable_if<
                      is_rheolef_arithmetic<U>::value
                     ,void
                    >::type
           >
  csr_concat_value (const U& x)                      : e(),  s(x), v(),     vv(),     m(),  variant(scalar) {}
  csr_concat_value (const vec<T,M>& x)               : e(),  s(),  v(x),    vv(),     m(),  variant(vector) {}
  csr_concat_value (const vec_trans<T,M>& vt)        : e(),  s(),  v(vt.v), vv(),     m(),  variant(vector_transpose) {}
  csr_concat_value (const std::vector<vec<T,M> >& x) : e(),  s(),  v(),     vv(x),    m(),  variant(vector_vec) {}
  csr_concat_value (const vector_vec_trans<T,M>& vt) : e(),  s(),  v(),     vv(vt.v), m(),  variant(vector_vec_transpose) {}
  csr_concat_value (const csr<T,M>& x)               : e(),  s(),  v(),     vv(),     m(x), variant(matrix) {}

// data:
public:
  sizes_pair_type        e;
  T                      s;
  vec<T,M>               v;
  std::vector<vec<T,M> > vv;
  csr<T,M>               m;
  variant_type           variant;
};

template <class T, class M>
class csr_concat_line {
public:

// typedef:

  typedef typename csr_concat_value<T,M>::size_type       size_type;
  typedef typename csr_concat_value<T,M>::sizes_type      sizes_type;      // [size,dis_size]
  typedef typename csr_concat_value<T,M>::sizes_pair_type sizes_pair_type; // [nrow,ncol] = [[nrow,dis_nrow],[ncol,dis_ncol]]

  static constexpr size_type undef = std::numeric_limits<size_type>::max();
  static constexpr size_type zero  = 0;

  typedef csr_concat_value<T,M>          value_type;

// allocators:

  csr_concat_line () : _l() {}

  csr_concat_line (const std::initializer_list<value_type>& il) : _l() {
    typedef typename std::initializer_list<value_type>::const_iterator const_iterator;
    for (const_iterator iter = il.begin(); iter != il.end(); ++iter) {
        _l.push_back(*iter);
    }
  }
  void push_back (const value_type& x) { _l.push_back (x); }

  friend std::ostream& operator<< (std::ostream& o, const csr_concat_line<T,M>& x) {
    std::cout << "{";
    for(typename std::list<value_type>::const_iterator iter = x._l.begin(); iter != x._l.end(); ++iter) {
        std::cout << *iter << " ";
    }
    return std::cout << "}";
  }
// internals:
  static void set_sizes (
    std::pair<size_type,size_type>&         row_sizes,
    std::pair<size_type,size_type>&         col_sizes,
    const std::pair<size_type,size_type>&   new_row_sizes,
    const std::pair<size_type,size_type>&   new_col_sizes);

  static void finalize_sizes (
    std::pair<size_type,size_type>&         sizes,
    const communicator&                     comm);

  static void finalize_sizes (
    std::vector<std::pair<size_type,size_type> >&  sizes,
    const communicator&                            comm);

  void build_csr_pass0 (
    std::pair<size_type,size_type>&                row_sizes,
    std::vector<std::pair<size_type,size_type> >&  col_sizes,
    communicator&                                  comm) const;

  void build_csr_pass1 (
    const std::pair<size_type,size_type>&                row_sizes,
    const std::vector<std::pair<size_type,size_type> >&  col_sizes,
    const communicator&                                  comm,
    distributor&                                         row_ownership,
    distributor&                                         col_ownership,
    std::vector<distributor>&                            col_start_by_component) const;

  void build_csr_pass2 (
    asr<T,M>&                                            a,
    const std::pair<size_type,size_type>&                row_sizes,
    const distributor&                                   row_start_by_component,
    const std::vector<std::pair<size_type,size_type> >&  col_sizes,
    const std::vector<distributor>&                      col_start_by_component) const;

  csr<T,M> build_csr () const;

// data:
protected:
  std::list<value_type> _l;
};
// -----------------------------------------------------------------------------
//
// 2nd case : multi-line matrix initializer
//  A = { {a,        u  },
//        {trans(u), 3.2} };
//
// -----------------------------------------------------------------------------
template <class T, class M>
class csr_concat {
public:

// typedef:

  typedef typename csr_concat_value<T,M>::size_type       size_type;
  typedef typename csr_concat_value<T,M>::sizes_type      sizes_type;      // [size,dis_size]
  typedef typename csr_concat_value<T,M>::sizes_pair_type sizes_pair_type; // [nrow,ncol] = [[nrow,dis_nrow],[ncol,dis_ncol]]

  static constexpr size_type undef = std::numeric_limits<size_type>::max();
  static constexpr size_type zero  = 0;

  typedef csr_concat_line<T,M>           line_type;
  typedef csr_concat_value<T,M>          value_type;

// allocators:

  csr_concat () : _l() {}

  csr_concat (const std::initializer_list<line_type>& il) : _l() {
    typedef typename std::initializer_list<line_type>::const_iterator const_iterator;
    for(const_iterator iter = il.begin(); iter != il.end(); ++iter) {
        _l.push_back(*iter);
    }
  }
  void push_back (const line_type& line) { _l.push_back(line); }

// internals:
  csr<T,M> build_csr () const;

// data:
protected:
  std::list<line_type> _l;
};

} // namespace details

// -----------------------------------------------------------------------------
// 2) interface with the csr<T,M> class
// -----------------------------------------------------------------------------

template <class T, class M>
inline
details::vec_trans<T,M>
trans (const vec<T,M>& w) {
  return details::vec_trans<T,M> (w);
}
template <class T, class M>
inline
details::vector_vec_trans<T,M>
trans (const std::vector<vec<T,M> >& w) {
  return details::vector_vec_trans<T,M> (w);
}
// -------------------------------
// csr cstor from std::initializer
// -------------------------------
#define RHEOLEF_csr_cstor(M)						\
template <class T>							\
inline									\
int									\
csr<T,M>::constraint_process_rank() const				\
{									\
  return details::constraint_process_rank (row_ownership().comm());	\
}									\
template <class T>							\
inline									\
csr<T,M>::csr (const std::initializer_list<details::csr_concat_value<T,M> >& init_list)	\
{									\
  details::csr_concat_line<T,M> cc (init_list);				\
  csr<T,M>::operator= (cc.build_csr());					\
}									\
template <class T>							\
inline									\
csr<T,M>::csr (const std::initializer_list<details::csr_concat_line<T,M> >& init_list) \
{									\
  details::csr_concat<T,M> cc (init_list);				\
  csr<T,M>::operator= (cc.build_csr());					\
}

RHEOLEF_csr_cstor(sequential)

#ifdef _RHEOLEF_HAVE_MPI
RHEOLEF_csr_cstor(distributed)
#endif // _RHEOLEF_HAVE_MPI

#undef RHEOLEF_csr_cstor

} // namespace rheolef
#endif // _RHEOLEF_CSR_CONCAT_H
