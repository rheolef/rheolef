///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
# include "rheolef/dis_cpu_time.h"
# include "rheolef/diststream.h"
// 
// wall time: real time
// cpu  time: number of instructions
//
#ifdef _RHEOLEF_HAVE_CLOCK_GETTIME
# include <time.h>
#elif defined(_RHEOLEF_HAVE_GETTIMEOFDAY) || defined(_RHEOLEF_HAVE_WIERDGETTIMEOFDAY)
# include <sys/types.h>
# include <sys/time.h>
#elif defined(_WIN32)
#  include <Windows.h>
# else
#  include <ctime>
#endif

namespace rheolef {
// ----------------------------------------------------------------------------
// dis_cpu_time
// ----------------------------------------------------------------------------
# ifndef _RHEOLEF_HAVE_MPI
double dis_cpu_time()  { return seq_cpu_time(); }
# else  // _RHEOLEF_HAVE_MPI
double dis_cpu_time() {
  double cpu = seq_cpu_time();
  if (! (mpi::environment::initialized() && !mpi::environment::finalized())) {
    return cpu;
  }
  return rheolef::mpi::all_reduce (rheolef::communicator(), cpu, std::plus<double>());
}
# endif // _RHEOLEF_HAVE_MPI

// ----------------------------------------------------------------------------
// seq_wall_time
// ----------------------------------------------------------------------------
double seq_wall_time()
{
#if defined(_RHEOLEF_HAVE_CLOCK_GETTIME)
    // high precision clock
#ifdef CLOCK_HIGHRES
#define SAMPLED_CLOCK CLOCK_HIGHRES
#else
#define SAMPLED_CLOCK CLOCK_REALTIME
#endif
    struct timespec tp;
    if (clock_gettime(SAMPLED_CLOCK, &tp) != 0) {
      return 0; // failure
    }
    return double(tp.tv_sec) + 1e-9*double(tp.tv_nsec);
#elif defined(_RHEOLEF_HAVE_WIERDGETTIMEOFDAY)
    // This is for Solaris, where they decided to change the CALLING SEQUENCE OF gettimeofday
    struct timeval tp;
    gettimeofday(&tp);
    return double(tp.tv_sec) + 1e-6*double(tp.tv_usec);
#elif defined(_RHEOLEF_HAVE_GETTIMEOFDAY)
    struct timeval tp;
    struct timezone tzp;
    gettimeofday(&tp,&tzp);
    return double(tp.tv_sec) + 1e-6*double(tp.tv_usec);
#elif defined(_RHEOLEF_HAVE_BSDGETTIMEOFDAY)
    struct timeval tp;
    struct timezone tzp;
    BSDgettimeofday(&tp,&tzp);
    return double(tp.tv_sec) + 1e-6*double(tp.tv_usec);
#elif defined(_WIN32)
    LARGE_INTEGER time, freq;
    if (!QueryPerformanceFrequency(&freq)){
        //  Handle error
        return 0;
    }
    if (!QueryPerformanceCounter(&time)){
        //  Handle error
        return 0;
    }
    return (double)time.QuadPart / freq.QuadPart;
#else
    // fall back to low resolution time function
    tfreqime_t tp, zero = 0;
    time (&tp);
    return difftime(time(&tp), zero);
#endif
}
// 
// NOTE: the c
//
double seq_cpu_time() {
#if !defined(_WIN32)
    return (double)clock() / CLOCKS_PER_SEC;
#else // _WIN32
    FILETIME a,b,c,d;
    if (GetProcessTimes(GetCurrentProcess(),&a,&b,&c,&d) != 0) {
        //  Returns total user time.
        //  Can be tweaked to include kernel times as well.
        return
            (double)(d.dwLowDateTime |
            ((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
    } else {
        //  Handle error
        return 0;
    }
#endif
}
// ----------------------------------------------------------------------------
// dis_wall_time
// ----------------------------------------------------------------------------
# ifndef _RHEOLEF_HAVE_MPI
double dis_wall_time() { return seq_wall_time(); }
# else  // _RHEOLEF_HAVE_MPI
double dis_wall_time() {
  if (! (mpi::environment::initialized() && !mpi::environment::finalized())) {
    return seq_wall_time();
  }
  double t = 0;
  communicator comm;
  comm.barrier();
  typedef std::size_t size_type;
  size_type io_proc = idiststream::io_proc();
  size_type my_proc = comm.rank();
  if (my_proc == io_proc) {
    t = seq_wall_time();
  }
  mpi::broadcast (comm, t, io_proc);
  return t;
}
# endif // _RHEOLEF_HAVE_MPI

}// namespace rheolef
