# ifndef _RHEO_ENVIRONMENT_H
# define _RHEO_ENVIRONMENT_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   29 september 2015

namespace rheolef {
/**
@classfile environment initialization of distributed computations

Description
===========
This class is mainly used to initialize the MPI library:
it should be called juste after the `main(argc,argv)` declaration:

        #include "rheolef.h"
        using namespace rheolef;
        int main (int argc, char**argv) {
          environment rheolef (argc, argv);
          ...
        }

An optional third argument of the `environment` constructor
allows one to set the MPI thread feature.
By default, its value is `MPI_THREAD_MULTIPLE`,
as defined in `mpi.h`.
Other possible values are related to MPI multi-threaded:
see the MPI documentation for more details.

When compiling in a non-distributed @ref configuration_page
of the Rheolef library, this is a do-nothing class.

Note
====
The interface of this class is inspirated from
the `boost::mpi::environment` class.
Instead of the `boost` implementation that calls
`MPI_Init()`, here this class calls
`MPI_Init_thread()` instead.
This extension allows one to support the `scotch` 
library when it has been compiled with the threads feature.

Implementation
==============
@showfromfile
@snippet environment.h verbatim_environment
@snippet environment.h verbatim_environment_cont
@snippet environment.h verbatim_environment_option
@snippet environment.h verbatim_environment_option_cont
@snippet environment.h verbatim_environment_option_cont2
*/
} // namespace rheolef

#include "rheolef/communicator.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#include <boost/optional.hpp>
#pragma GCC diagnostic pop

namespace rheolef {

// [verbatim_environment_option]
struct environment_option_type {
  static const int no_thread = 100;
// [verbatim_environment_option]
#ifdef _RHEOLEF_HAVE_MPI
// [verbatim_environment_option_cont]
  static const int default_thread = MPI_THREAD_MULTIPLE;
// [verbatim_environment_option_cont]
#else // _RHEOLEF_HAVE_MPI
  static const int default_thread = 0;
#endif // _RHEOLEF_HAVE_MPI
// [verbatim_environment_option_cont2]
  environment_option_type() : thread_level (default_thread) {}
  int thread_level ;
};
// [verbatim_environment_option_cont2]

#ifndef _RHEOLEF_HAVE_MPI

// [verbatim_environment]
class environment {
public:
  environment (int& argc, char**& argv, const environment_option_type& opt = environment_option_type());
  ~environment();
// [verbatim_environment]
protected:
  unsigned int _oldcw;
// [verbatim_environment_cont]
};
// [verbatim_environment_cont]
#else
class environment {
public:
  explicit environment (int& argc, char**& argv, const environment_option_type& opt = environment_option_type());
  ~environment();
  static bool initialized();
  static bool finalized();
  static void abort(int errcode);
  static int max_tag();
  static int collectives_tag();
  static boost::optional<int> host_rank();
  static boost::optional<int> io_rank();
  static std::string processor_name();
private:
  bool             _rheolef_has_init;
  unsigned int     _oldcw;
  static const int _num_reserved_tags = 1;
};
#endif // _RHEOLEF_HAVE_MPI
} // namespace rheolef
#endif // _RHEO_ENVIRONMENT_H
