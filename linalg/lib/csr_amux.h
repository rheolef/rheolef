# ifndef _SKIT_CSR_AMUX_H
# define _SKIT_CSR_AMUX_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

# include "rheolef/compiler.h"

namespace rheolef {
/*F:
NAME: csr_amux -- mat-vec product (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Performs the product "A*x" where "A" is a sparse matrix in
  CSR format and "x" a dense vector.
ALGORITHM:

  csr_amux

  "input": the matrix in CSR format and the dense vector
  |   ia(0:nrow), ja(0:nnz-1), a(0:nnz-1),
  |   x(0:ncol-1)
  "output": the result as dense vector
  |   y(0:nrow-1)
  begin
  | for i := 0 to nrow-1 do
  |   sum := 0
  |   for p := ia(i) to ia(i+1)-1 do
  |     sum := sum + a(p) * x(ja(p))
  |   endfor
  |   y(i) := sum
  end
COMPLEXITY:
  Complexity is "O(nz)" where "nz" is the number of non-zero
  entries of "A".
IMPLEMENTATION:
  "ja" and "a" arrays are merged in an array of pair.
  
  The "ia" is an array of pointer in the array of pair.
  
  Arrays are abstractred by using STL iterators.
  
  The operation "y := A*x" is extended to "y += A*x" and so
  by using and abstract set-operator in
  the instruction "y(i) := sum.
METHODS: @csr_amux 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   14 may 1999
END:
*/

//<csr_amux:
template <
    class InputIterator,
    class InputRandomAcessIterator,
    class SetOperator,
    class OutputIterator>
void
csr_amux (
    InputIterator            ia,
    InputIterator            last_ia,
    InputRandomAcessIterator x,
    SetOperator 	     set_op,
    OutputIterator           y)
{
    typedef typename std::iterator_traits<InputIterator>::value_type InputIterator2;
    typedef typename std::iterator_traits<OutputIterator>::value_type T;
    InputIterator2 a = (*ia++);
    while (ia != last_ia) {
	T sum = 0;
        InputIterator2 last_a = (*ia++);
	while (a != last_a) {
	    sum += (*a).second * x[(*a).first];
	    ++a;
	}
	set_op(*y++, sum);
    }
}
//>csr_amux:
//@!\vfill
} // namespace rheolef
#endif // _SKIT_CSR_AMUX_H
