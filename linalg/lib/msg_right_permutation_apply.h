#ifndef _RHEO_MSG_RIGHT_PERMUTATION_APPLY_H
#define _RHEO_MSG_RIGHT_PERMUTATION_APPLY_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
namespace rheolef {
/*F:
NAME: msg_right_permutation_apply -- sequentail apply (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Applies a permutation to an array.
ALGORITHM:
  msg_right_permutation_apply

  "input": the length array
  |   perm(0:n-1), x(0:nx-1)
  "output": the pointer array and the total size
  |   y(0:n)
  begin
  |   for i := 0 to n-1 do
  |     y(i) := x(perm(i))
  |   endfor
  end
COMPLEXITY:
  Time and memory complexity is O(n).
METHODS: @msg_right_permutation_apply 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   6 january 1999
END:
*/

//<msg_right_permutation_apply:
template <
    class InputIterator,
    class InputRandomIterator,
    class OutputIterator,
    class SetOp>
OutputIterator
msg_right_permutation_apply (
    InputIterator	        perm,
    InputIterator	        last_perm,
    const InputRandomIterator&	x,
    OutputIterator	        y,
    SetOp                       set_op)
{
    for (; perm != last_perm; y++, perm++) {
	// something like: (*y++) = x[(*perm++)];
	set_op (y, x, *perm);
    }
    return y;
}
//>msg_right_permutation_apply:

// set(rhs,lhs,i) <==> *rhs = lhs[i] or *rhs = pair(i,rhs[i])

// 1) used by csr<Float>:
template<class OutputIterator, class InputRandomIterator, class Size>
struct msg_right_permutation_set_default {
  void operator() (OutputIterator rhs, const InputRandomIterator& lhs, Size i) {
	*rhs = lhs [i];
  }
};
template <
    class InputIterator,
    class InputRandomIterator,
    class OutputIterator>
inline
OutputIterator
msg_right_permutation_apply (
    InputIterator	        perm,
    InputIterator	        last_perm,
    const InputRandomIterator&	x,
    OutputIterator	        y)
{
    typedef typename std::iterator_traits<InputIterator>::value_type size_type;
    msg_right_permutation_set_default<OutputIterator, InputRandomIterator, size_type> set_op;
    return msg_right_permutation_apply (perm, last_perm, x, y, set_op);
}
#ifdef TO_CLEAN
// used by polymorphic_array<geo_element> in geo<Float>:
template<class OutputIterator, class InputRandomIterator, class Size>
struct msg_right_permutation_set_pair {
  typedef typename std::iterator_traits<OutputIterator>::value_type T;
  void operator() (OutputIterator rhs, const InputRandomIterator& lhs, Size i) {
	*rhs = std::pair<Size,T>(i,lhs [i]);
  }
};
#endif // TO_CLEAN


} // namespace rheolef
#endif // _RHEO_MSG_RIGHT_PERMUTATION_APPLY_H
