#ifndef _RHEO_MSG_LEFT_PERMUTATION_APPLY_H
#define _RHEO_MSG_LEFT_PERMUTATION_APPLY_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
namespace rheolef {
/*F:
NAME: msg_left_permutation_apply -- sequentail apply (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Applies a left permutation to an array.
ALGORITHM:
  msg_left_permutation_apply

  "input": the length array
  |   x(0:nx-1), py(0:n-1)
  "output": the pointer array and the total size
  |   y(0:n)
  begin
  |   for i := 0 to n-1 do
  |     y(py(i)) := x(i)
  |   endfor
  end
COMPLEXITY:
  Time and memory complexity is O(n).
METHODS: @msg_left_permutation_apply 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   6 january 1999
END:
*/

//<msg_left_permutation_apply:
template <
    class InputIterator1,
    class InputIterator2,
    class SetOp,
    class OutputRandomIterator>
void
msg_left_permutation_apply (
    InputIterator1		x,
    SetOp			op,
    InputIterator2		py,
    InputIterator2		last_py,
    OutputRandomIterator	y)
{
    while (py != last_py)
	op(y[*py++], *x++);
}
//>msg_left_permutation_apply:
} // namespace rheolef
#endif // _RHEO_MSG_LEFT_PERMUTATION_APPLY_H
