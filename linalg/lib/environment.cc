///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// boost::mpi::environment-like interface, with thread options for scotch lib

#include "rheolef/environment.h"
#ifndef _RHEOLEF_HAVE_MPI

namespace rheolef {

environment::environment (int& argc, char**& argv, const environment_option_type& opt)
 : _oldcw(0)
{
}
environment::~environment()
{
}

} // namespace rheolef

#else // _RHEOLEF_HAVE_MPI

// support MPI-2 (with warnings, but changed to error by the compiler flags)
#ifdef MPI_VERSION
# if (MPI_VERSION >= 2)
#  ifndef MPI_Errhandler_set
#   define MPI_Errhandler_set	MPI_Comm_set_errhandler
#  endif
#  ifndef MPI_Attr_get
#   define MPI_Attr_get		MPI_Comm_get_attr
#  endif
# endif
#endif

namespace rheolef {

bool environment::initialized() {
  int flag;
  MPI_Initialized(&flag);
  return flag != 0;
}
bool environment::finalized() {
  int flag;
  MPI_Finalized(&flag);
  return flag != 0;
}
environment::environment (int& argc, char**& argv, const environment_option_type& opt)
 : _rheolef_has_init(false), _oldcw(0)
{
  if (!initialized()) {
    int status = 0;
    if (opt.thread_level == environment_option_type::no_thread) {
      status = MPI_Init (&argc, &argv);
    } else {
      int obtained_thread_level;
      status = MPI_Init_thread (&argc, &argv, opt.thread_level, &obtained_thread_level);
      if (obtained_thread_level != opt.thread_level) {
        warning_macro ("mpi_init: obtained thread level="<<obtained_thread_level <<" while asking for thread level="<<opt.thread_level);
      }
    }
    check_macro (status == MPI_SUCCESS, "mpi init failed");
  }
  if (!_rheolef_has_init) {
#ifdef _RHEOLEF_HAVE_OBSOLETE_MPI_V1
    MPI_Errhandler_set      (MPI_COMM_WORLD, MPI_ERRORS_RETURN);
#else
    MPI_Comm_set_errhandler (MPI_COMM_WORLD, MPI_ERRORS_RETURN);
#endif
    _rheolef_has_init = true;
  }
}
environment::~environment()
{
  if (_rheolef_has_init && !finalized()) {
    boost::mpi::detail::mpi_datatype_cache().clear();
    MPI_Finalize();
  }
}
void environment::abort(int errcode)
{
  MPI_Abort(MPI_COMM_WORLD, errcode);
}
int environment::max_tag()
{
  int* max_tag_value;
  int found = 0;
  MPI_Attr_get(MPI_COMM_WORLD, MPI_TAG_UB, &max_tag_value, &found);
  assert_macro (found != 0, "MPI_Attr_get: error");
  return *max_tag_value - _num_reserved_tags;
}
int environment::collectives_tag()
{
  return max_tag() + 1;
}
boost::optional<int> environment::host_rank()
{
  int* host;
  int found = 0;
  MPI_Attr_get(MPI_COMM_WORLD, MPI_HOST, &host, &found);
  if (!found || *host == MPI_PROC_NULL) {
    return boost::optional<int>();
  } else {
    return *host;
  }
}
boost::optional<int> environment::io_rank()
{
  int* io;
  int found = 0;
  MPI_Attr_get(MPI_COMM_WORLD, MPI_IO, &io, &found);
  if (!found || *io == MPI_PROC_NULL) {
    return boost::optional<int>();
  } else {
    return *io;
  }
}
std::string environment::processor_name()
{
  char name[MPI_MAX_PROCESSOR_NAME];
  int len;
  MPI_Get_processor_name(name, &len);
  return std::string(name, len);
}

} // namespace rheolef
#endif // _RHEOLEF_HAVE_MPI
