#ifndef _RHEOLEF_LOAD_CHUNK_H
#define _RHEOLEF_LOAD_CHUNK_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
namespace rheolef {
template <class RandomIterator>
inline
bool
load_chunk (std::istream& s, RandomIterator iter, RandomIterator last)
{
    while (iter != last)
        if (!(s >> *iter++)) return false;
    return s.good();
}
template <class RandomIterator, class GetFunction>
inline
bool
load_chunk (std::istream& s, RandomIterator iter, RandomIterator last, GetFunction get_element)
{
    while (iter != last)
        if (! get_element (s, *iter++)) return false;
    return s.good();
}
} // namespace rheolef
#endif // _RHEOLEF_LOAD_CHUNK_H
