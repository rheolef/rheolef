#ifndef _RHEO_VECTOR_OF_ITERATOR_H
#define _RHEO_VECTOR_OF_ITERATOR_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// vector_of_iterator returns:
//  iterator when begin()
//  const_iterator when begin() const
// and so.
//
#include "get_pointer_from_iterator.h"
namespace rheolef {

template<class T>
class vector_of_iterator
: public std::vector<T*>
{
public:
    typedef          std::vector<T*>         V;
    typedef          std::vector<const T*>   CONST_V;
    typedef          T*                      value_type;
    typedef          const T*                const_value_type;
    typedef          value_type*             iterator;
    typedef          const_value_type*const  const_iterator;
    typedef          value_type&             reference;
    typedef          const value_type&       const_reference;
#ifdef TODO
    // pointer do not have rbegin and rend with p++ that goes back...
    typedef          value_type*             reverse_iterator;
#endif // TODO
    typedef typename V::size_type            size_type;
    typedef typename V::difference_type      difference_type;
#ifdef TODO
    typedef typename CONST_V::const_reference  const_reference;
#endif // TODO

    explicit vector_of_iterator (size_type n = 0, const value_type& value = value_type ());

#ifdef TODO
    vector_of_iterator (const_iterator first, const_iterator last);
#endif // TODO

    size_type size() const;
    iterator begin();
    const_iterator begin() const;
    const_iterator end () const;
    iterator end ();
    const_reference operator[] (size_type i) const;
    reference operator[] (size_type i);
#ifdef TODO
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
    reverse_iterator rend();
public:
    const_reference at (size_type n) const;
    reference at (size_type n);
    const_reference front () const;
    reference front ();
    const_reference back () const;
    reference back ();
#endif // TODO
};
template<class T>
inline
vector_of_iterator<T>::vector_of_iterator (
    size_type n, 
    const value_type& value)
  : std::vector<value_type>(n,value)
{
}
#ifdef TODO
template<class T>
inline
vector_of_iterator<T>::vector_of_iterator (
    const_iterator first, 
    const_iterator last)
  : std::vector<T*>(first,last)
{
}
#endif // TODO
template<class T>
inline
typename vector_of_iterator<T>::size_type
vector_of_iterator<T>::size() const
{
	return std::vector<value_type>::size(); 
}
template<class T>
inline
typename vector_of_iterator<T>::iterator
vector_of_iterator<T>::begin()
{
    return get_pointer_from_iterator(std::vector<value_type>::begin()); 
}
template<class T>
inline
typename vector_of_iterator<T>::const_iterator
vector_of_iterator<T>::begin() const
{
    return const_iterator(get_pointer_from_iterator(std::vector<value_type>::begin())); 
}
template<class T>
inline
typename vector_of_iterator<T>::iterator
vector_of_iterator<T>::end()
{ 
    return begin()+size();
}
template<class T>
inline
typename vector_of_iterator<T>::const_iterator
vector_of_iterator<T>::end() const
{
    return begin()+size();
}
#ifdef TODO
template<class T>
inline
typename vector_of_iterator<T>::reverse_iterator
vector_of_iterator<T>::rbegin()
{
    return get_pointer_from_iterator(std::vector<T>::rbegin()); 
}
template<class T>
inline
typename vector_of_iterator<T>::const_reverse_iterator
vector_of_iterator<T>::rbegin() const
{
    return get_pointer_from_iterator(std::vector<T>::rbegin()); 
}
template<class T>
inline
typename vector_of_iterator<T>::reverse_iterator
vector_of_iterator<T>::rend()
{
    return get_pointer_from_iterator(std::vector<T>::rend());
}
template<class T>
inline
typename vector_of_iterator<T>::const_reverse_iterator
vector_of_iterator<T>::rend() const
{
    return get_pointer_from_iterator(std::vector<T>::rend());
}
#endif // TODO

template<class T>
inline
typename vector_of_iterator<T>::const_reference 
vector_of_iterator<T>::operator[] (size_type i) const
{
    return *(begin()+i);
}
template<class T>
inline
typename vector_of_iterator<T>::reference 
vector_of_iterator<T>::operator[] (size_type i)
{
    return *(begin()+i);
}
#ifdef TODO
template<class T>
inline
typename vector_of_iterator<T>::const_reference 
vector_of_iterator<T>::at (size_type n) const
{
    return const_reference(std::vector<value_type>::operator[] (n));
}
template<class T>
inline
typename vector_of_iterator<T>::reference 
vector_of_iterator<T>::at (size_type n)
{
    return const_reference(std::vector<value_type>::operator[] (n));
}
template<class T>
inline
typename vector_of_iterator<T>::const_reference 
vector_of_iterator<T>::front () const
{
    return const_reference(std::vector<value_type>::front());
}
template<class T>
inline
typename vector_of_iterator<T>::reference 
vector_of_iterator<T>::front ()
{
    return const_reference(std::vector<value_type>::front());
}
template<class T>
inline
typename vector_of_iterator<T>::const_reference 
vector_of_iterator<T>::back () const
{
    return const_reference(std::vector<value_type>::back());
}
template<class T>
inline
typename vector_of_iterator<T>::reference 
vector_of_iterator<T>::back ()
{
    return const_reference(std::vector<value_type>::back());
}
#endif // TODO
} // namespace rheolef
#endif // _RHEO_VECTOR_OF_ITERATOR_H
