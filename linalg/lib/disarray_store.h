#ifndef _RHEOLEF_ARRAY_STORE_H
#define _RHEOLEF_ARRAY_STORE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
namespace rheolef {
//
// implementation for dense disarray
//
template <class OutputRandomIterator, class SetOp, class Size, class IsContainer = std::false_type>
struct disarray_store {};

// dense disarray of simple type, i.e. T is a mpi_datatype
template <class OutputRandomIterator, class SetOp, class Size>
struct disarray_store<OutputRandomIterator, SetOp, Size, std::false_type> {
    typedef typename std::iterator_traits<OutputRandomIterator>::value_type T;
    typedef typename std::pair<Size,T>                                      data_type;
    void operator()(const std::pair<Size,T>& val) { _op(_x [val.first], val.second); }
    Size n_new_entry() const { return 0; }
    disarray_store(OutputRandomIterator x, SetOp op) : _x(x), _op(op) {}
    OutputRandomIterator _x;
    SetOp                _op;
};
// dense disarray of more complex type, i.e. T is a container of mpi_datatype
template <class OutputRandomIterator, class SetOp, class Size>
struct disarray_store<OutputRandomIterator, SetOp, Size, std::true_type> {
    typedef typename std::iterator_traits<OutputRandomIterator>::value_type T;
    typedef typename T::value_type                                          U;
    typedef typename std::pair<Size,U>                                      data_type;
    void operator()(const std::pair<Size,U>& val) {
	_op (_x [val.first], val.second);
    }
    Size n_new_entry() const { return 0; }
    disarray_store(OutputRandomIterator x, SetOp op) : _x(x), _op(op) {}
    OutputRandomIterator _x;
    SetOp                _op;
};

// a simple wrapper, for convenience:
template <class OutputRandomIterator, class SetOp, class Size, class IsContainer>
inline
disarray_store<OutputRandomIterator, SetOp, Size, IsContainer>
disarray_make_store(OutputRandomIterator x, SetOp op, Size, IsContainer)
{
    return disarray_store<OutputRandomIterator, SetOp, Size, IsContainer>(x,op);
}
} // namespace rheolef
#endif // _RHEOLEF_ARRAY_STORE_H
