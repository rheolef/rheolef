#ifndef _RHEOLEF_MPI_SCATTER_MAP_H
#define _RHEOLEF_MPI_SCATTER_MAP_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/mpi_scatter_init.h"
#include "rheolef/mpi_scatter_begin.h"
#include "rheolef/mpi_scatter_end.h"

namespace rheolef {

/*F:
NAME: mpi_scatter_map -- gather/scatter and build map (@PACKAGE@ @VERSION@)
DESCRIPTION:
  This is a high-level communication function for application
  to finite-element problems.
  The input is a stl::set of external degrees of freedom (e.g. vertices, edges, faces)
  referenced by localy managed elements and required by the local processor.
  These degrees of freedom are on the boundary of the mesh partition
  and are managed by a neighbour processor.
  The ouput is the same index set, but associated with the required values, in
  a stl::map associative container.
  The input also furnish an iterator to the locally managed data and 
  the distributor associated to these data.
COMPLEXITY:
  Time, communication and memory complexity is O(nidx+nproc).
  For finite-element problems in d dimenion
  @example
    nidx ~ N^((d-1)/d)
  @end example
  where N is the number of degrees of freedom (vertices or elements).

AUTHORS: Pierre.Saramito@imag.fr
DATE:   23 march 1999
END:
*/
//<verbatim:
template <class InputIterator, class InputSet, class OutputMap>
void
mpi_scatter_map (
// input:
    const distributor& 	ownership,
    InputIterator 	local_data,
    const InputSet& 	ext_idx_set,
// output:
    OutputMap& 		ext_idx_map)
//>verbatim:
{
    typedef typename std::iterator_traits<InputIterator>::value_type data_type;
    typedef typename distributor::size_type size_type;

    // 0) declare the local context
    distributor::tag_type tag = distributor::get_new_tag();
    scatter_message<std::vector<data_type> > from;
    scatter_message<std::vector<data_type> > to;

    // 1) convert set to vector, for direct acess:
    std::vector<size_type> ext_idx (ext_idx_set.size());
    std::copy (ext_idx_set.begin(), ext_idx_set.end(), ext_idx.begin());

    // 2) declare id[i]=i for scatter
    std::vector<size_type> id (ext_idx.size());
    for (size_type i = 0; i < id.size(); i++) id[i] = i;

    // 3) init scatter
    mpi_scatter_init(
        ext_idx.size(),
        ext_idx.begin().operator->(),
        id.size(),
        id.begin().operator->(),
        ownership.dis_size(),
        ownership.begin().operator->(),
        tag,
        ownership.comm(),
        from,
        to);

    // 4) begin scatter: send local data to others and get ask for missing data
    std::vector<data_type> buffer (ext_idx.size());
    mpi_scatter_begin (
        get_pointer_from_iterator(local_data),
        buffer.begin().operator->(),
        from,
        to,
        details::generic_set_op(),
        tag,
        ownership.comm());

    // 5) end scatter: receive missing data
    mpi_scatter_end (
        get_pointer_from_iterator(local_data),
        buffer.begin(),
        from,
        to,
        details::generic_set_op(),
        tag,
        ownership.comm());

    // 6) build the associative container: pair (ext_idx ; data)
    ext_idx_map.clear();
    for (size_type i = 0; i < buffer.size(); i++) {
        ext_idx_map.insert (std::make_pair (ext_idx[i], buffer[i]));
    }
}

} // namespace rheolef
#endif // _RHEOLEF_MPI_SCATTER_MAP_H
