///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*
  setenv DMALLOC_OPTIONS "debug=0x14f47d83,log=dtest.log"
  c++ dtest.cc -ldmallocxx -o dtest
  ./dtest
  more dtest.log
 */

#include <dmalloc.h>

#define TODO
#ifdef TODO
extern void *operator new(size_t, const char *const, int);
extern void *operator new[](size_t,  const char *const, int);
#define D_NEW new (__FILE__ , __LINE__)
#else // TODO
//#define D_NEW new 
#endif // TODO

int main()
{
  char *fred;
  char *test;

  test = D_NEW char;
  test = D_NEW char;
  delete test;

  fred =D_NEW char[20];
  fred =D_NEW char[20];
  delete [] fred;
  return 0;
}
