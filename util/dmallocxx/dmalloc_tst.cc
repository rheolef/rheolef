///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/compiler.h"
/*
 usage :
   setenv DMALLOC_OPTIONS 'debug=0x14f47d83,log=a.log'
   
   g++-3.2 -DNO_BUG try-bug.cc dmallocc.cc -ldmalloc
   ./a.out
   more a.log

   g++-3.2 -DBUG try-bug.cc dmallocc.cc -ldmalloc
   rm -f a.log
   ./a.out
   more a.log
*/

int main () {
    char *p = (char*)dmalloc_malloc(__FILE__, __LINE__, 10*sizeof(char), DMALLOC_FUNC_NEW_ARRAY, 0 /* no alignment */, 0 /* no xalloc messages */);

#ifdef NO_BUG
    p [9] = 'a';
#else
    p [10] = 'a';
#endif
    delete_tab_macro(p);
    dmalloc_free(__FILE__, __LINE__, p, DMALLOC_FUNC_NEW_ARRAY);
}
