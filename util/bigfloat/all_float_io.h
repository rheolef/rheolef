#ifndef _RHEO_ALL_FLOAT_IO_H
#define _RHEO_ALL_FLOAT_IO_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// generic inputs/outputs for floats
//
// author: Pierre.Saramito@imag.fr
//
// date: 25 january 2000
//
// IMPLEMENTATION NOTE:
//  requires some minimal math library
//  on generic type T : log10(), ...
// 
// TODO: ostream
//   knows setprecision(n) but
// does not respond to
//   ios::scientific 
//   ios::fixed
//   ios::showpoint
//
// cf.  glibc-2.1.2/stdio-common/printf_fp.c
//
//      gcc-2.95.2/libio/iostream.c 
//      gcc-2.95.2/libio/outfloat.c 
//      gcc-2.95.2/libio/floatconv.c
//
template<class T>
std::istream&
all_float_read (std::istream& s, T& x)
{
  int sign = 1;
  char c;
  s >> std::ws; // eat whitespace
  s >> c;
  // check for stream
  if (s.eof())  s.setstate (std::ios::failbit);
  if (s.fail()) return s;

  if (c == '-') { 
    sign = -1; 
    s >> c;
  } else if (c == '+') {
    s >> c;
  }
  // get digits before decimal point
  int n  = c - '0';
  int ex = 0;
  T y = 0;
  while (n >= 0 && n < 10) {
    y = 10*y + n;
    s.get(c);  // cannot use s>>c; as that will eat whitespace
    if (isspace(c)) { goto fixup; }
    n = c - '0'; 
  }
  if (c == '.') {  // get digits after decimal point
     s.get(c);  // cannot use s>>c; as that will eat whitespace
     if (isspace(c)) { 
       goto fixup;
     }
     n = c - '0';
     while (n >= 0 && n < 10) { 
       y = 10*y + n; 
       s.get(c);
       n = c - '0';
       --ex; 
       if (isspace(c)) { 
         goto fixup;
       }
     }
  }
  n = 0;
  if (c == 'e' || c == 'E' || 
      c == 'd' || c == 'D' || c == 'L') {

     s >> n; 
     ex += n; // get exponent
  } else {
     s.putback(c);
  }
fixup:
  if (sign < 0) {
    y = -y;
  }
  // exponent adjustment
  while (ex-- > 0) y *= 10;
  while (++ex < 0) y /= 10;
  x = y;
  return s;
}
template <class T>
std::ostream&
all_float_write (std::ostream& s, const T& x)
{
  if (numeric_flags<T>::output_as_double()) {
    // i.e. for non-regression tests purpose
    return s << double(x);
  }
  if (x == 0) { 
    s << "0"; 
    return s;
  }
  T ten = 10;
  T y   = fabs(x); 
  T q   = log10(y);
  int n = int(floor(q));
  if (n < 0) {
    n++;
  }
  T l = pow(ten,n);
  y = y/l;
#ifdef TO_CLEAN
  long showpos = (s.flags() & std::ios::showpos);
  s.unsetf(std::ios::showpos);
#endif // TO_CLEAN
  if (x < 0) {
    s  <<  "-";
#ifdef TO_CLEAN
  } else if (showpos) {
    s  <<  "+";
#endif // TO_CLEAN
  }
  // num of decimals in 3..N
  int prec = s.precision();
  int digits10 = std::numeric_limits<T>::digits10;
  int d = ((prec > digits10) ? digits10 : prec);
  d = ((d < 3) ? 3 : d);
  for (int i = 1; i <= d; i++) {
    if (i == 2) {
      s << ".";
    }
    int m = int(floor(y));
    if (m < 0 || m > 9) {
      std::cerr << "fatal(genfloat_io): digit = " << m 
       << " founded in ostream& operator <<" << std::endl;
      exit (1);
    }
    s << m;
    y = (y - T(m))*ten;
    if (y <= 0) break; // x must be an integer
  }
  // restaure optional showpos flag (used for exponent part)
#ifdef TO_CLEAN
  s.setf(std::ios::showpos);
#endif // TO_CLEAN
  if (n != 0) {
      char E = ((s.flags() & std::ios::uppercase) ? 'E' : 'e');
      s << E << n;
  } else {
      s << "";
  }
  s << "";
  return s;
}
#endif // _RHEO_ALL_FLOAT_IO_H
