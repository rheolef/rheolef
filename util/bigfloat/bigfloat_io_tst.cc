///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "bigfloat.h"
#include <iomanip>
#include <sstream>
#include <string>
using namespace std;

//
// N=digits10 is an argument of the bigfloat type
//
#ifdef DIGITS10
const int N = DIGITS10;
#else
const int N = 40;
#endif

int main(){

    cout << setprecision(N); // digit10 == N;

    bigfloat<N> x;

    x = 9;
    cout << "x = " << x << endl;
    cout << "floor(x) = " << floor(x) << endl;

    x = 10;
    cout << "x = " << x << endl;
    cout << "floor(x) = " << floor(x) << endl;

    cin  >> x;
    cout << "x = " << x << endl;

    numeric_flags<bigfloat<N> >::output_as_double(true);
    cout << "x = " << x << endl;

    cout.setf(ios::uppercase);
    cout << "x = " << x << endl;
    cout.unsetf(ios::uppercase);
    cout << "x = " << x << endl;
    cout <<  1 << endl;
    cout << -1 << endl;
    cout.setf(ios::showpos);
    cout <<  1 << endl;
    cout << -1 << endl;
    // +1.00000000000000e+100
    // +1.00000000000000e-100
    // -1.00000000000000e-100
    // -1.00000000000000e+100
    cout << bigfloat<N>(+1e+100) << endl;
    cout << bigfloat<N>(+1e-100) << endl;
    cout << bigfloat<N>(-1e-100) << endl;
    cout << bigfloat<N>(-1e+100) << endl;
}

