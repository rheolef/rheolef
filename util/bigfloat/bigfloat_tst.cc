///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "bigfloat.h"
#include <iomanip>
#include <sstream>
#include <string>
using namespace std;
//
// N=digits10 is an argument of the bigfloat type
//
#ifdef DIGITS10
const int N = DIGITS10;
#else
const int N = 40;
#endif

int main(){

    cout << setprecision(N); // digit10 == N;

    bigfloat<N> zero;
    cout << "zero			= " << zero << endl;

    // cstor, math library
    bigfloat<N> sqrt5 = sqrt(bigfloat<N>(5));
    cout << "sqrt(5)			= " << sqrt5 << endl;
    bigfloat<N> sqrt5_0 = sqrt(bigfloat<N>(5.0));
    cout << "sqrt(5.0)		= " << sqrt5_0 << endl;

    // type conv & assignment
    bigfloat<N> y;
    y = "2.236067977499789696409173668731276235440618359612L0";
    cout << "y = string		= " << y << endl;
    y = double(sqrt5);
    cout << "y = double(sqrt(5))	= " << y << endl;
    y = int(sqrt5);
    cout << "y = int(sqrt(5))	= " << y << endl;

    // do not send any warning !!!
    int iy = y;

    // operators
    y = sqrt(bigfloat<N>(5));
    cout << "-y			= " << -y << endl;
    bigfloat<N> z = y+y;
    cout << "z = y+y			= " << z << endl;
    z = z/2;
    cout << "z = z/2			= " << z << endl;

    // computed assignment
    z += y;
    cout << "z += y			= " << z << endl;
    
    // comparator
    cout << "z == y			= " << (z==y) << endl;
    cout << "z != y			= " << (z!=y) << endl;
    
    // to string
    ostringstream os;
    os << setprecision(N) << y;
    string s = os.str();
    cout << "string(y) 		= " << s << endl;

    // math
    bigfloat<N> t = log10(y);
    cout << "t = log10(y)		= " << t << endl;

    cout << "Pi			= " << bigfloat<N>::compute_pi() << endl;
    cout << "digits 			= " << numeric_limits<bigfloat<N> >::digits << endl;
    cout << "digits10 		= " << numeric_limits<bigfloat<N> >::digits10 << endl;
    cout << "epsilon 		= " << numeric_limits<bigfloat<N> >::epsilon() << endl;
}

