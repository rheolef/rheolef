///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// test custom allocators, with std:map
//
#include "rheolef/stack_allocator.h"
using namespace rheolef;
using namespace std;

int main() { return 0; }

#ifdef TODO
// TODO: heap_alloc problem with g++-8.1 : compil error with map & custom allocator
struct object {
  double data;
  object (double x) : data(x) {
	warning_macro ("object cstor: "<<data);
  }
  object (const object& x) : data(x.data) {
	warning_macro ("object copy : "<<data);
  }
  ~object () {
	warning_macro ("object dstor: "<<data);
  }
};
ostream& operator<< (ostream& os, const object& x) { 
  return cout << x.data;
}
int main()
{
  {
    typedef map<size_t, object>::value_type value_type;
    typedef stack_allocator<value_type>        alloc_t;
    typedef map <size_t, object, less<size_t>, alloc_t>  map_type;
    const size_t stack_size = 1024;
    vector<unsigned char> stack (stack_size);
    alloc_t stack_alloc (stack.begin().operator->(), stack.size());
    {
        map_type a (less<size_t>(), stack_alloc);
        a.insert (make_pair (0, object(3.14)));
        a.insert (make_pair (1, object(1.17)));
        for (map_type::iterator iter = a.begin(), last = a.end(); iter != last; iter++) {
            cout << (*iter).first << " " << (*iter).second << endl;
        }
	warning_macro ("end of map block: will destroy map...");
    }
    warning_macro ("end of map block: destroy map done");
  }
}
#endif // TODO
