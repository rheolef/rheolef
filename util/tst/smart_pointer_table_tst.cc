///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// maquette for a hash table associated to a class
// this will be used by the geo class in order to avoid 
// several loads on file of the same mesh: its name is unique
#include "rheolef/smart_pointer.h"
using namespace rheolef;
using namespace std;
// --------------------------------------------------------------
// objects representation (could be file "object.h")
// --------------------------------------------------------------
static map<string,void*> table;

class A_rep {
public:
 explicit A_rep (string name);
 ~A_rep();
 string name() const { return _name; }
protected:
 string _name;
};
// --------------------------------------------------------------
// object interface
// --------------------------------------------------------------
class A: public smart_pointer<A_rep> {
  typedef smart_pointer<A_rep> base;
public:
  explicit A(string name);
  string name() const { return data().name(); }
};
// --------------------------------------------------------------
// implementation
// --------------------------------------------------------------
A_rep::A_rep (string name)
  : _name(name)
{
  cout << "A_rep::cstor("<<_name<<")" << endl;
}
A_rep::~A_rep()
{
  cout << "A_rep::destor("<<_name<<")" << endl;
  table.erase (_name);
}
A::A(string name)
  : base()
{
  auto iter = table.find(name);
  if (iter == table.end()) {
    cout << "A::A: NEW("<<name<<")..." << endl;
    base::operator= (new_macro(A_rep(name)));
    void* p_count = base::get_count();
    table.insert (make_pair (name, p_count));
    return;
  }
  cout << "A::A REUSE("<<name<<")..." << endl;
  void* p_count = (*iter).second;
  base::operator= (base(p_count,base::internal()));
}
// --------------------------------------------------------------
// test program
// --------------------------------------------------------------
int main()
{
  cout << "main: BEGIN..." << endl;
  {
    A A1("a");
    cout << "A1.n=" << A1.reference_counter() << endl;
    A B1("b");
    cout << "B1.n=" << B1.reference_counter() << endl;
    A A2("a");
    cout << "A2.n=" << A2.reference_counter() << endl;
    A B2("b");
    cout << "B2.n=" << B2.reference_counter() << endl;
  }
  cout << "main: RESTART..." << endl;
  {
    A A1("a");
    cout << "A1.n=" << A1.reference_counter() << endl;
    A B1("b");
    cout << "B1.n=" << B1.reference_counter() << endl;
    A A2("a");
    cout << "A2.n=" << A2.reference_counter() << endl;
    A B2("b");
    cout << "B2.n=" << B2.reference_counter() << endl;
  }
  cout << "main: END..." << endl;
}
