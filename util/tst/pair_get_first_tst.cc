///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// iterator adaptator demo: on the first (or second) of a container<pair>
#include "rheolef/pair_util.h"

#include <iostream>
#include <vector>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#include <boost/functional.hpp>
#include <boost/iterator/transform_iterator.hpp>
#pragma GCC diagnostic pop

using namespace rheolef;
using namespace std;

int main() {
  vector<pair<size_t,string> > x;
  x.push_back(make_pair(1,"hello"));
  x.push_back(make_pair(2," "));
  x.push_back(make_pair(3,"world"));
  x.push_back(make_pair(4," "));
  x.push_back(make_pair(5,"!"));
  const int N = x.size();
  
  
  cout << "get the first:" << endl;
  typedef boost::transform_iterator<get_first<size_t,string>, vector<pair<size_t,string> >::iterator>
  	get_first_iterator;
  get_first_iterator i    (x.begin(), get_first<size_t,string>()),
                     i_end(x.end(),   get_first<size_t,string>());
  while (i != i_end) cout << *i++ << " ";
  cout << endl;
  
  cout << "get the second:" << endl;
  typedef boost::transform_iterator<get_second<size_t,string>, vector<pair<size_t,string> >::iterator>
  	get_second_iterator;
  get_second_iterator j    (x.begin(), get_second<size_t,string>()),
                      j_end(x.end(),   get_second<size_t,string>());
  while (j != j_end) cout << *j++;
  cout << endl;
  
  cout << "get all by this iterator:" << endl;
  get_first_iterator k    (x.begin(), get_first<size_t,string>());
  k++;
  k++;
  vector<pair<size_t,string> >::iterator p = k.base();
  cout << "base = [" << (*p).first << "," << (*p).second << "]" << endl;
  return 0; 
}
