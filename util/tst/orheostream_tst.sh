#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/util/tst"}
export TMPDIR="."
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0
for file in in-animal-s2; do
  run "rm -f toto.hb toto.hb.gz"
  run "gzip -dc < ${TOP_SRCDIR}/util/tst/$file.hb.gz | ./orheostream_tst toto hb 2>/dev/null"
  if test $? -ne 0; then status=1; fi
  run "gzip -dc < toto.hb.gz > tmp1.hb"
  run "gzip -dc < ${TOP_SRCDIR}/util/tst/$file.hb.gz > tmp2.hb"
  run "diff -B tmp1.hb tmp2.hb"
  if test $? -ne 0; then status=1; fi
  run "rm -f toto.hb toto.hb.gz toto.txt toto.txt.gz tmp1.hb tmp2.hb"
  run "echo abc | ./orheostream_tst toto txt 2>/dev/null && echo def | ./orheostream_tst toto txt app 2>/dev/null && test \`gzip -d < toto.txt.gz | grep -v '^$' | wc -l\` -eq 2"
  if test $? -ne 0; then status=1; fi
  run "rm -f toto.txt toto.txt.gz"
done
exit $status
