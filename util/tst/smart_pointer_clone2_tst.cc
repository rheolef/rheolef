///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// maquette for a hierarchy of classes: two concrete classes + 1 abstrct one
// the second concrete class has additionnal members
// the final object implements a fat interface: check whether we 
// have the 2nd object before calling the extra members.
//
#include "rheolef/smart_pointer.h"
using namespace rheolef;
using namespace std;
// --------------------------------------------------------------
// objects representation (could be file "object.h")
// --------------------------------------------------------------
class object_abstract_rep {
public:
 virtual object_abstract_rep* clone () const = 0;
 virtual ~object_abstract_rep() {}
 virtual int value () const = 0;
 virtual void set_value (int i) = 0;
};
class object1_rep : public object_abstract_rep {
 int _i;
public:
 object1_rep (int i) : object_abstract_rep(), _i(i) {}
 object_abstract_rep* clone () const { 
 	warning_macro ("object1::clone: i="<<_i);
	return new_macro(object1_rep(*this));
 }
 ~object1_rep() { warning_macro ("object1::destroy: i="<<_i); }
 int value () const { return _i; }
 void set_value (int i)  { _i = i; }
};
class object2_rep : public object_abstract_rep {
 int         _i;
 string      _name;
public:
 object2_rep (int i, string s) : object_abstract_rep(), _i(i), _name(s) {}
 object_abstract_rep* clone () const {
 	warning_macro ("object2::clone: i="<<_i<<", name="<<_name);
	return new_macro(object2_rep(*this));
 }
 ~object2_rep() { warning_macro ("object2::destroy: i="<<_i<<", name="<<_name); }
 int value () const { return _i; }
 void set_value (int i)  { _i = i; }
 string name () const { return _name; }
 void set_name (string s)  { _name = s; }
};
// --------------------------------------------------------------
// object interface (fat interface, for object2)
// --------------------------------------------------------------
class object : public smart_pointer_clone<object_abstract_rep> {
  typedef smart_pointer_clone<object_abstract_rep> base;
public:
// allocator:

  explicit object(int i) : base(new_macro(object1_rep(i))) {}
  explicit object(int i, string s) : base(new_macro(object2_rep(i,s))) {}

// accessors & modifiers:

  int value () const { return data().value(); }
  void set_value (int i)  { data().set_value (i); }
  string name () const;
  void set_name (string s);
};
// here are accessors specific to object2 representation
// => check whether we have an object2
string
object::name() const
{
  const object_abstract_rep* abstract_ptr = base::pointer();
  const object2_rep* ptr = dynamic_cast<const object2_rep*>(abstract_ptr);
  check_macro (ptr != 0, "object is not an object2");
  return ptr->name();
}
void
object::set_name(string s)
{
  object_abstract_rep* abstract_ptr = base::pointer();
  object2_rep* ptr = dynamic_cast<object2_rep*>(abstract_ptr);
  check_macro (ptr != 0, "object is not an object2");
  ptr->set_name(s);
}
// --------------------------------------------------------------
// test program
// --------------------------------------------------------------
int main()
{
  object A(10);
  object B(12,"tutu");
  A = B;
  object C = B;
  warning_macro ("A.count="<<A.reference_counter());
  A.set_name ("toto");
  std::cout << "A.name = " << A.name() << std::endl;
  std::cout << "B.name = " << B.name() << std::endl;
  std::cout << "C.name = " << C.name() << std::endl;
  check_macro (A.name() != B.name(), "It is not a true copy semantic");
  check_macro (C.name() == B.name(), "It is not a true copy semantic");
  std::cout << "It seems to be a true copy semantic." << std::endl;
  warning_macro ("A.count="<<A.reference_counter());
  warning_macro ("B.count="<<B.reference_counter());
  warning_macro ("C.count="<<C.reference_counter());
}
