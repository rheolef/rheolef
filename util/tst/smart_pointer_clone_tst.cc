///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/smart_pointer.h"
using namespace rheolef;
using namespace std;
// --------------------------------------------------------------
// object representation (could be file "object.h")
// --------------------------------------------------------------
class object_abstract_rep {
public:
 virtual object_abstract_rep* clone () const = 0;
 virtual ~object_abstract_rep() {}
 virtual int value () const = 0;
 virtual void set_value (int i) = 0;
};
class object_rep : public object_abstract_rep {
 int _i;
public:
 object_rep (int i) : object_abstract_rep(), _i(i) {}
 object_abstract_rep* clone () const { return new_macro(object_rep(*this)); }
 ~object_rep() { warning_macro ("object::destroy: i="<<_i); }
 int value () const { return _i; }
 void set_value (int i)  { _i = i; }
};
// --------------------------------------------------------------
// object interface
// --------------------------------------------------------------
class object : public smart_pointer_clone<object_abstract_rep> {
  typedef smart_pointer_clone<object_abstract_rep> base;
public:
// allocator:

  explicit object(int i) : base(new_macro(object_rep(i))) {}

// accessors & modifiers:

  int value () const { return data().value(); }
  void set_value (int i)  { data().set_value (i); }
};
// --------------------------------------------------------------
// test program
// --------------------------------------------------------------
int main()
{
    object A(10);
    object B = A;
    B.set_value (12);
    std::cout << "A = " << A.value() << std::endl;
    std::cout << "B = " << B.value() << std::endl;
    check_macro (A.value() != B.value(), "It is not a true copy semantic");
    std::cout << "It seems to be a true copy semantic." << std::endl;
}
