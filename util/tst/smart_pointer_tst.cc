///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#define _RHEO_SMART_POINTER_TST_CC
#include "rheolef/smart_pointer.h"

#ifdef TO_CLEAN
using namespace rheolef;
using namespace std;

// data representation (could be file "container_data.h")
typedef int T;
class container_data {
    private:  
        T *values;
        int n;
    public: 
	//!      IMPORTANT: 
	//! the copy constructor 
        //!       **MAY** 
	//! performs a complete copy
	//!
        container_data (const container_data& x)
         : values(new T[x.n]), n(x.n)
        { for (int i=0; i<n;i++) values[i]=x.values[i];}
        container_data& operator= (const container_data& x) {
          n = x.n;
          values = new T[n];
          for (int i=0; i<n;i++) values[i]=x.values[i];
	  return *this;
        }
        // a customized constructor
        explicit container_data(int n1)
         : values(new T[n1]), n(n1) {}

        ~container_data() { delete [] values; }

        // read and write accessors are separated
        const T& operator[](int i) const 
		    { return values[i]; }
              T& operator[](int i)       
		    { return values[i]; }
};
// an interface to data via the Objet class
//        that count occurrence (could be "container.h")
//
class container : private smart_pointer<container_data> {
public:
    // the customized cstor
    explicit container(int n = 0);

    // read/write accessors
    const T&  operator[](int i) const;
          T&  operator[](int i);
};
// here is the implementation of the interface
//  (could be "container.c")
//
container::container (int n)
: smart_pointer<container_data> (new container_data(n))
{}
const T&
container::operator[] (int i) const {
    // use read access data()
    return data().operator[] (i);
}
T&
container::operator[] (int i) {
    // use write access data() that check occurrence count
    return data().operator [] (i);
}
// test program
int main() {
    container A(10);
    A[1] = 1;
    container B = A;
    B[1] = 2;
    if (A[1] == B[1]) {
	    std::cerr << "fatal: It is not a true copy semantic." << std::endl;
	exit(1);
    }
    std::cerr << "It seems to be a true copy semantic." << std::endl;
}
#endif // TO_CLEAN
