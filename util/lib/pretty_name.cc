///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef/pretty_name.h"

#include <cxxabi.h>
#include <cstdlib>

// inspirated from: http://stackoverflow.com/questions/2885597/c-template-name-pretty-print
//   If you've got minor improvements, put them in or in a comment.
//   If you've got a quite different version you can always put it in another answer.
//   @aaa - Georg Fritzsche May 22 at 3:21

namespace rheolef {

std::string
indent (std::string str, const std::string &indent = "  ")
{
    std::string indent_ = std::string("\n");
    size_t token = 0;

    bool one_line = false;
    while ((token = str.find_first_of("<>,", token)) != std::string::npos) {
        size_t size = str.size();
        size_t close, open, comma;

        switch(str[token]) {
        case '<':
            close = str.find(">", token+1);
            open = str.find("<", token+1);
            comma = str.find(",", token+1);
            one_line = !(close > open) && !(comma < close);

            if (one_line) break;
            indent_.append(indent);

        case ',':
            str.insert(token + 1, indent_);
            break;

        case '>':
            if (!one_line) {
                indent_.erase(indent_.size() - indent.size());
                str.insert(token, indent_);
            }
            one_line = false;
        }

        token += 1 + str.size() - size;

        const size_t nw = str.find_first_not_of(" ", token);
        if(nw != std::string::npos) {
            str.erase(token, nw-token);
        }
    }

    return str;
}
std::string
typeid_name (const char* name, bool do_indent)
{
// #ifdef HAVE_CXA_DEMANGLE
    size_t size;
    int status;
    char *buf = abi::__cxa_demangle (name, NULL, &size, &status);
    if (status  != 0) throw status;
    std::string string(buf);
    free(buf);
    if (do_indent) return indent(string); else return string;
// #else
//     return name;
// #endif
}

} // namespace rheolef
