#ifndef _RHEOLEF_HEAP_OBJECT_H
#define _RHEOLEF_HEAP_OBJECT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef/compiler.h"
namespace rheolef { 

template<class T>
class heap_object {
public:
    typedef size_t size_type;
    heap_object(size_type sizeof_bucket = sizeof(T));
    ~heap_object();
    T* new_bucket() const;
#ifdef TO_CLEAN
    T* malloc() const { return new_bucket(); } // for boost::object_pool compatibility
#endif // TO_CLEAN
    size_type size() const { return _counter; }
    void reinitialize (size_type sizeof_bucket = sizeof(T));
    void clear ();
protected:
    mutable      std::list<std::vector<char> > _heap;
    mutable      size_type _heap_block_size;
    mutable      size_type _heap_block_last_free;
                 size_type _sizeof_bucket;
    mutable      size_type _counter;
    static const size_type _heap_block_size_init = 10;
};
template<class T>
inline
heap_object<T>::heap_object(size_type sz)
  : _heap(),
    _heap_block_size(_heap_block_size_init),
    _heap_block_last_free(0),
    _sizeof_bucket(sz),
    _counter(0)
{
    _heap.push_front(std::vector<char>(_heap_block_size*_sizeof_bucket));
}
template<class T>
inline
void
heap_object<T>::reinitialize (size_type sz)
{
    clear();
    _heap_block_size = heap_object<T>::_heap_block_size_init;
    _heap_block_last_free = 0;
    _sizeof_bucket = sz;
    _counter = 0;
    _heap.push_front(std::vector<char>(_heap_block_size*_sizeof_bucket));
}
template<class T>
inline
T*
heap_object<T>::new_bucket() const
{
    if (_heap_block_last_free == _heap_block_size) {
      _heap_block_size *= 2;
      _heap.push_front(std::vector<char>(_heap_block_size*_sizeof_bucket));
      _heap_block_last_free = 0;
    }
    std::vector<char>& block = *(_heap.begin());
    char* p  = &block [_heap_block_last_free*_sizeof_bucket];
    _heap_block_last_free++;
    new ((void*) p) T(); // call default T constructor at p
    _counter++;
    return (T*)p;
}
template<class T>
inline
heap_object<T>::~heap_object()
{
    clear();
}
template<class T>
void
heap_object<T>::clear()
{
    size_type n = _heap_block_size_init;
    for (std::list<std::vector<char> >::reverse_iterator i = _heap.rbegin();
	_counter != 0 && i != _heap.rend(); i++, n *= 2) {
        std::vector<char>& block = *i;
	char* p = &(block[0]);
	for (size_type c = 0; _counter != 0 && c < n; c++, p += _sizeof_bucket) {
	      ((T*)p)->~T();
	      _counter--;
	}
    }
    _heap.erase(_heap.begin(), _heap.end());
}
}// namespace rheolef
#endif // _RHEOLEF_HEAP_OBJECT_H
