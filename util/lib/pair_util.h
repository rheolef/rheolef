#ifndef _RHEOLEF_PAIR_UTIL_H
#define _RHEOLEF_PAIR_UTIL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// useful class-functions for scanning containers of std::pair<T1,T2>
#include <utility>    // for std::pair
#include <functional> // for std::unary_function
#include <iostream>   // for debug

namespace rheolef {

template <class T1, class T2>
struct get_first : std::unary_function<std::pair<T1,T2>, T1> {
  T1 operator() (const std::pair<T1,T2>& x) const { return x.first; }
};
template <class T1, class T2>
struct get_second : std::unary_function<std::pair<T1,T2>, T2> {
  T2 operator() (const std::pair<T1,T2>& x) const { return x.second; }
};

template<typename InputPairIterator, typename OutputPairIterator, typename UnaryOperation>
OutputPairIterator
pair_transform_second (
    InputPairIterator  first,
    InputPairIterator  last,
    OutputPairIterator result, 
    UnaryOperation     unary_op)
{
  for (; first != last; ++first, ++result)
        (*result).second = unary_op ((*first).second);
  return result;
}
#ifndef TO_CLEAN
template <class T1, class T2>
std::ostream&
operator<< (std::ostream& out, const std::pair<T1,T2>& x)
{
  return out << "pair("<<x.first<<","<<x.second<<")";
}
#endif // TO_CLEAN

} // namespace rheolef
#endif // _RHEOLEF_PAIR_UTIL_H
