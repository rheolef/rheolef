#ifndef _RHEO_VECTOR_H
#define _RHEO_VECTOR_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

/*Class:csr
NAME:  @code{Vector} - STL @code{vector<T>} with reference counting 
@clindex Vector
DESCRIPTION:       
 @noindent
 The class implement a reference counting wrapper for
 the STL @code{vector<T>} container class, with shallow copies.
 See also:
 @emph{The standard template library}, by Alexander Stephanov and Meng Lee.

 @noindent
 This class provides the full @code{vector<T>}
 interface specification
 an could be used instead of @code{vector<T>}.
NOTE:
 @noindent
 The write accessors 
 @example
	T& operator[](size_type)
 @end example
 @noindent
 as in @code{v[i]}
 may checks the reference count for each access.
 For a loop, a better usage is:
 @example
      Vector<T>::iterator i = v.begin();
      Vector<T>::iterator last = v.end();
      while (i != last) @{ ...@}
 @end example
 @noindent
 and the reference count check step occurs only two time,
 when accessing via @code{begin()} and @code{end()}.

 @noindent
 Thus, in order to encourage users to do it, we declare private
 theses member functions. A synonym of @code{operator[]} is @code{at}.

AUTHOR: 
     Pierre Saramito
   | Pierre.Saramito@imag.fr
    LMC-IMAG, 38041 Grenoble cedex 9, France
DATE:   14 september 1997
End:
*/

/*
//<Vector:
template<class T>
class Vector : private smart_pointer<vector_rep<T> > {
 
public:

// typedefs:

    typedef iterator;
    typedef const_iterator;
    typedef pointer;
    typedef reference;
    typedef const_reference;
    typedef size_type;
    typedef difference_type;
    typedef value_type;
    typedef reverse_iterator;
    typedef const_reverse_iterator;

// allocation/deallocation:

    explicit Vector (size_type n = 0, const T& value = T ());
    Vector (const_iterator first, const_iterator last);
    void reserve (size_type n);
    void swap (Vector<T>& x) ;

// accessors:

    iterator                 begin ();
    const_iterator           begin () const;
    iterator                 end ();
    const_iterator           end ()   const;
    reverse_iterator         rbegin();
    const_reverse_iterator   rbegin() const;
    reverse_iterator         rend();
    const_reverse_iterator   rend() const;
    size_type size () const;
    size_type max_size () const;
    size_type capacity () const;
    bool empty () const;
    void resize (size_type sz, T v = T ()); // non-standard ?
private:
    const_reference operator[] (size_type n) const;
    reference operator[] (size_type n);
public:
    const_reference at (size_type n) const; // non-standard ?
    reference at (size_type n);
    reference         front ();
    const_reference   front () const;
    reference         back ();
    const_reference   back ()  const;

// insert/erase:

    void push_back (const T& x);
    iterator insert (iterator position, const T& x = T ());
    void insert (iterator position, size_type n, const T& x);
    void insert (iterator position, const_iterator first, const_iterator last);
    void pop_back ();
    void erase (iterator position);
    void erase (iterator first, iterator last);
};
//>Vector:
*/

#include "rheolef/compiler.h"
#include "rheolef/smart_pointer.h"

namespace rheolef { 
template<class T>
struct vector_rep : public std::vector<T> {

// typedefs:
    typedef std::vector<T>  DATA;

    typedef typename DATA::iterator                   iterator;
    typedef typename DATA::const_iterator             const_iterator;
    typedef typename DATA::pointer                    pointer;
    typedef typename DATA::reference                  reference;
    typedef typename DATA::const_reference            const_reference;
    typedef typename DATA::size_type                  size_type;
    typedef typename DATA::difference_type            difference_type;
    typedef          T                                value_type;
    typedef typename DATA::reverse_iterator           reverse_iterator;
    typedef typename DATA::const_reverse_iterator     const_reverse_iterator;

// allocators/deallocators:

    vector_rep (const vector_rep& x) 
    : std::vector<T>(x)
    {}

    explicit
    vector_rep (size_type n = 0, const T& value = T ())
    : std::vector<T>(n,value)
    {}

#ifdef TODO
    vector_rep (const_iterator first, const_iterator last)
    : std::vector<T>(first,last)
    {}
#endif // TODO
};

template<class T>
struct Vector : smart_pointer<vector_rep<T> > 
{
    typedef vector_rep<T>                    DATA;

public:

// typedefs:

    typedef typename DATA::iterator                   iterator;
    typedef typename DATA::const_iterator             const_iterator;
    typedef typename DATA::pointer                    pointer;
    typedef typename DATA::reference                  reference;
    typedef typename DATA::const_reference            const_reference;
    typedef typename DATA::size_type                  size_type;
    typedef typename DATA::difference_type            difference_type;
    typedef          T                                value_type;
    typedef typename DATA::reverse_iterator           reverse_iterator;
    typedef typename DATA::const_reverse_iterator     const_reverse_iterator;

// allocation/deallocation:

    explicit
    Vector (size_type n = 0, const T& value = T ())
    : smart_pointer<DATA>(new_macro(DATA(n,value)))
    {}

#ifdef TODO
    Vector (const_iterator first, const_iterator last)
    : smart_pointer<DATA>(new_macro(DATA(first,last)))
    {}
#endif // TODO

    void reserve (size_type n)
    { 
	smart_pointer<vector_rep<T> >::data().reserve(n);
    }
    void swap (Vector<T>& x) 
    {
	smart_pointer<vector_rep<T> >::data().swap(*x.p);
    }
// accessors:

    iterator                 begin ()       
    { 
	return smart_pointer<vector_rep<T> >::data().begin();
    }
    const_iterator           begin () const 
    {
	return smart_pointer<vector_rep<T> >::data().begin();
    }
    iterator                 end ()         
    { 
	return smart_pointer<vector_rep<T> >::data().end();
    }
    const_iterator           end ()   const 
    { 
	return smart_pointer<vector_rep<T> >::data().end();
    }
    reverse_iterator rbegin()
    {
	return reverse_iterator(smart_pointer<vector_rep<T> >::data().end());
    }
    const_reverse_iterator rbegin() const
    {
	return const_reverse_iterator(smart_pointer<vector_rep<T> >::data().end());
    }
    reverse_iterator rend()
    {
	return reverse_iterator(smart_pointer<vector_rep<T> >::data().begin());
    }
    const_reverse_iterator rend() const
    {
	return const_reverse_iterator(smart_pointer<vector_rep<T> >::data().begin());
    }
    size_type size ()     const 
    {
	return smart_pointer<vector_rep<T> >::data().size();
    }
    size_type max_size () const
    {
	return smart_pointer<vector_rep<T> >::data().max_size ();
    }
    size_type capacity () const
    {
	return smart_pointer<vector_rep<T> >::data().capacity ();
    }
    bool empty () const
    {
	return smart_pointer<vector_rep<T> >::data().empty();
    }
    // non-standatd ?
    void resize (size_type sz, T v = T ())
    {
        if (sz > (smart_pointer<vector_rep<T> >::data().size ())) {
           smart_pointer<vector_rep<T> >::data().insert (smart_pointer<vector_rep<T> >::data().end(), sz- (smart_pointer<vector_rep<T> >::data().size ()), v);
        } else if (sz < (smart_pointer<vector_rep<T> >::data().size())) {
           smart_pointer<vector_rep<T> >::data().erase (smart_pointer<vector_rep<T> >::data().begin()+sz, smart_pointer<vector_rep<T> >::data().end());
        }
    }
    const_reference operator[] (size_type n) const
    {
        return smart_pointer<vector_rep<T> >::data().operator[] (n);
    }
    reference operator[] (size_type n)
    {
        return smart_pointer<vector_rep<T> >::data().operator[] (n);
    }
public:
    const_reference at (size_type n) const
    {
        return smart_pointer<vector_rep<T> >::data().operator[] (n);
    }
    reference at (size_type n) 
    {
        return smart_pointer<vector_rep<T> >::data().operator[] (n);
    }
    reference         front ()        
    {
        return smart_pointer<vector_rep<T> >::data().front();
    }
    const_reference   front () const
    {
        return smart_pointer<vector_rep<T> >::data().front();
    }
    reference         back ()
    {
        return smart_pointer<vector_rep<T> >::data().back();
    }
    const_reference   back ()  const
    {
        return smart_pointer<vector_rep<T> >::data().back();
    }
// insert/erase:

    void push_back (const T& x)
    {
        smart_pointer<vector_rep<T> >::data().push_back(x);
    }
    iterator insert (iterator position, const T& x = T ())
    {
        return smart_pointer<vector_rep<T> >::data().insert(position,x);
    }
    void insert (iterator position, size_type n, const T& x)
    {
        smart_pointer<vector_rep<T> >::data().insert(position,n,x);
    }
#ifdef TODO
    void insert (iterator position, const_iterator first, const_iterator last)
    {
        smart_pointer<vector_rep<T> >::data().insert(position,first,last);
    }
#endif // TODO
    void pop_back ()
    {
        smart_pointer<vector_rep<T> >::data().pop_back();
    }
    void erase (iterator position)
    {
        smart_pointer<vector_rep<T> >::data().erase(position);
    }
    void erase (iterator first, iterator last)
    {
        smart_pointer<vector_rep<T> >::data().erase(first,last);
    }
};
template<class IteratorValue, class ConstIteratorValue>
struct VectorOfIterators : Vector<IteratorValue>
{
    typedef          Vector<IteratorValue>         V;
    typedef          Vector<ConstIteratorValue>    CONST_V;

    typedef          IteratorValue                 value_type;
    typedef          ConstIteratorValue            const_value_type;

    typedef typename V::iterator                   iterator;
    typedef typename CONST_V::const_iterator       const_iterator;
    typedef typename V::pointer                    pointer;
    typedef typename V::reference                  reference;
    typedef typename CONST_V::const_reference      const_reference;
    typedef typename V::size_type                  size_type;
    typedef typename V::difference_type            difference_type;
    typedef typename V::reverse_iterator           reverse_iterator;
    typedef typename CONST_V::const_reverse_iterator const_reverse_iterator;

    explicit
    VectorOfIterators (size_type n, const IteratorValue& value = IteratorValue ())
    : V(n,value)
    {}

#ifdef TODO
    VectorOfIterators (const_iterator first, const_iterator last)
    : V(first,last)
    {}
#endif // TODO

    iterator begin()
    {
	return Vector<IteratorValue>::data().begin(); 
    }
    const_iterator begin() const
    {
	return const_iterator (Vector<IteratorValue>::data().begin()); 
    }
    const_iterator end ()   const 
    { 
	return const_iterator (Vector<IteratorValue>::data().end()); 
    }
    iterator end ()
    { 
	return Vector<IteratorValue>::data().end(); 
    }
    const_reverse_iterator rbegin() const
    {
	return const_reverse_iterator(Vector<IteratorValue>::data().end());
    }
    reverse_iterator rbegin()
    {
	return reverse_iterator(Vector<IteratorValue>::data().end());
    }
    const_reverse_iterator rend() const
    {
	return const_reverse_iterator(Vector<IteratorValue>::data().begin());
    }
    reverse_iterator rend()
    {
	return reverse_iterator(Vector<IteratorValue>::data().begin());
    }
private:
    const_reference operator[] (size_type n) const
    {
        return const_reference(Vector<IteratorValue>::data().operator[] (n));
    }
    reference operator[] (size_type n)
    {
        return reference(Vector<IteratorValue>::data().operator[] (n));
    }
public:
    const_reference at (size_type n) const
    {
        return const_reference(Vector<IteratorValue>::data().operator[] (n));
    }
    reference at (size_type n)
    {
        return Vector<IteratorValue>::data().operator[] (n);
    }
    const_reference   front () const
    {
        return const_reference(Vector<IteratorValue>::data().front());
    }
    reference   front ()
    {
        return Vector<IteratorValue>::data().front();
    }
    const_reference   back ()  const
    {
        return const_reference(Vector<IteratorValue>::data().back());
    }
    reference   back ()
    {
        return Vector<IteratorValue>::data().back();
    }
};
}// namespace rheolef
#endif /* _RHEO_VECTOR_H */

