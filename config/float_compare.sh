#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
float_compare() {
  tol=${3-"1e-7"}
  cmp_status=`echo $1 $2 | gawk -v tol=$tol '
	($1 != "") {	
	     abs_a1 = ($1 > 0 ? $1 : -$1);
	     abs_a2 = ($2 > 0 ? $2 : -$2);
	     max_abs_12 = (abs_a1 > abs_a2 ? abs_a1 : abs_a2);
	     abs_diff = ($1-$2 > 0 ? $1-$2 : $2-$1);
             print !(abs_diff < tol*max_abs_12);
	  }
'`
  #echo "cmp_status=$cmp_status"
  return $cmp_status
}
float_differ() {
  float_compare $1 $2 $3
  test "$?" != 0
}
