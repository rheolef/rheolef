#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# plot2pdf: run gnuplot and output pdf for pdflatex
#
# authors: Ibrahim Cheddadi and Pierre Saramito
#
# changelog:
# * 16 dec 2013 : force option
# * 25 fev 2013 : handle gnuplot terminals: pdf, latex with or without standalone
# *  3 jan 2014 : merge the two previous versions
# * 17 fev 2015 : add various ams fonts, also in not-standalone mode
# * 27 nov 2015 : detect when gnuplot retuns an unix error status
# *  8 fev 2016 : option -gnuplot4 : backward compat. when gnuplot=gnuplot5
# * 18 fev 2018 : add "cairolatex pdf" terminal support
#                 add check_fit_name between name.plot and output name
#                 add support for both -gnuplot4 and -gnuplot5 options
# * 16 may 2019 : fix pdfcrop with "--resolution 300"
# * 14 may 2022 : handle dest dir same as src dir
#
if test $# -eq 0; then
  echo "usage: $0 [-gnuplot4|-gnuplot5][-eps|-pdf][-force][-[no]amsmath][-[no]clean] file[.plot]" 1>&2
  exit 1
fi
wish_gnuplot=""
output_format="pdf"
force=false
add_amsmath=true
clean=true
GNUPLOT="gnuplot"
while test $# -ne 0; do
 case $1 in
 -gnuplot4)   wish_gnuplot="gnuplot4";;
 -gnuplot5)   wish_gnuplot="gnuplot5";;
 -eps)        output_format="eps";;
 -pdf)        output_format="pdf";;
 -force)      force=true;;
 -amsmath)    add_amsmath=true;;
 -noamsmath)  add_amsmath=false;;
 -clean)      clean=true;;
 -noclean)    clean=false;;
 *)           name=$1;;
 esac
 shift
done
if test x"$wish_gnuplot" != x""; then 
  for d in `echo $PATH | sed -e 's/:/ /g'`; do
    if test -x $d/$wish_gnuplot; then
      GNUPLOT="$wish_gnuplot"
      break
    fi
  done
fi
name=`expr $name : '\(.*\).plot' \| $name`
if test ! -f $name.plot; then
  echo "$0: file \"$name.plot\" not found" 1>&2
  exit 1
fi
dir=`expr $name : '\(.*\)/.*' \| .`
#echo "dir=$dir"; exit 0
# -------------------------------------
# check fit name:
# -------------------------------------
basename=`basename $name`
liste=""
for mot in $(cat $name.plot); do
  if [ "${mot:(-5)}" = ".tex\"" ]; then
	mot=${mot#\"}
	mot=${mot%\"}
	liste="$liste $mot"
  elif [ "${mot:(-5)}" = ".tex'" ]; then
	mot=${mot#\'}
	mot=${mot%\'}
	liste="$liste $mot"
  else
	continue
  fi
  basename2=${mot%.tex}
  if test $basename != $basename2; then
    echo "$0: error: basename mismatch \"$basename.plot\" and output \"$basename2.tex\""
    exit 1
  fi
done
# -------------------------------------
# is generated latex file standalone ?
# -------------------------------------
to_clean=""
if `grep '^set[ \t][ \t]*terminal[ \t].*latex' $name.plot 2>/dev/null >/dev/null`; then
  run_latex=true
  if `grep '^set[ \t][ \t]*terminal[ \t].*standalone' $name.plot 2>/dev/null >/dev/null`; then
    standalone=true
    wrapname="${basename}"
  else
    standalone=false
    wrapname="${basename}_wrap"
  fi
  to_clean="$to_clean $wrapname.tex $wrapname.aux"
  if `grep '^set[ \t][ \t]*terminal[ \t].*pdf' $name.plot 2>/dev/null >/dev/null`; then
    latex="pdflatex"
    suffix="pdf"
    if $standalone; then
      to_clean="$to_clean $basename-inc.pdf"
    fi
  else
    latex="latex"
    suffix="dvi"
    to_clean="$to_clean $wrapname.dvi $basename.ps"
    if $standalone; then
      to_clean="$to_clean $basename-inc.eps"
    fi
    if test "$output_format" != "eps"; then
      to_clean="$to_clean $basename.eps"
    fi
  fi
  if test $wrapname != $basename; then
    to_clean="$to_clean $basename.tex $wrapname.$output_format"
  fi
else
  run_latex=false
  standalone=true
fi
output="$basename.$output_format"
#echo "VERBOSE run_latex  $run_latex"
#echo "VERBOSE latex  $latex"
#echo "VERBOSE suffix $suffix"
#echo "VERBOSE standalone $standalone"
#echo "VERBOSE output_format $output_format"
# ---------------------------------------
# checked runs
# ---------------------------------------
clean_and_exit () {
  status=${1-"0"}
  if test $status -ne 0; then
    to_clean="$to_clean $output"
  fi
  if $clean; then
    command="/bin/rm -f ${to_clean}"
    echo $command
    eval $command
  fi
  if test $status -eq 0; then
    echo "! output written to \"$output\""
  fi
  exit $status
}
run () {
   command=$*
   echo $command
   eval $command
   status=$?
   return $status
}
checked_run () {
   run $*
   status=$?
   if test $status -ne 0; then
     echo "$0: ERROR: command failed" 1>&2
     clean_and_exit $status;
   fi
}
# -----------
# run gnuplot
# -----------
run "/bin/rm -f ${to_clean} $output"
checked_run "$GNUPLOT $name.plot 2>&1 | tee $name.glog"
if test $force != "true"; then
  if test `grep -i warning $name.glog | \
	      grep -v -i 'empty cb range' | \
	      grep -v -i 'difficulty making room for' | \
	      grep -v -i 'Cannot contour non grid data' | \
	      grep -v -i 'to convert degree sign' | \
	      wc -l` \
	     -ne 0; then
    echo "$0: gnuplot warning changed to error" 1>&2
    clean_and_exit 1
  fi
fi
# -------------------------------------
# need a wrapper ?
# -------------------------------------
if test $standalone = false; then
  # makes a little latex wrapper:
  cat > ${wrapname}.tex << EOF1
% GNUPLOT: LaTeX picture with Postscript
\documentclass{minimal}
% Set font size
\makeatletter
\def\@ptsize{1}
\InputIfFileExists{size11.clo}{}{%
   \GenericError{(gnuplot) \space\space\space\@spaces}{%
      Gnuplot Error: File size11.clo not found! Could not set font size%
   }{See the gnuplot documentation for explanation.%
   }{For using a font size a file size<fontsize>.clo has to exist.
        Falling back ^^Jto default fontsize 10pt.}%
  \def\@ptsize{0}
  \input{size10.clo}%
}%
\makeatother
% Load packages
\usepackage{graphicx}
\usepackage{color}
\makeatletter
% Select an appropriate default driver (from TeXLive graphics.cfg)
\begingroup
  \chardef\x=0 %
  % check pdfTeX
  \@ifundefined{pdfoutput}{}{%
    \ifcase\pdfoutput
    \else
      \chardef\x=1 %
    \fi
  }%
  % check VTeX
  \@ifundefined{OpMode}{}{%
    \chardef\x=2 %
  }%
\expandafter\endgroup
\ifcase\x
  % default case
  \PassOptionsToPackage{dvips}{geometry}
\or
  % pdfTeX is running in pdf mode
  \PassOptionsToPackage{pdftex}{geometry}
\else
  % VTeX is running
  \PassOptionsToPackage{vtex}{geometry}
\fi
\makeatother
% Set papersize
\usepackage[papersize={252.00bp,176.40bp},text={252.00bp,176.40bp}]{geometry}
% No page numbers and no paragraph indentation
\pagestyle{empty}
\setlength{\parindent}{0bp}%
% Load configuration file
\InputIfFileExists{gnuplot.cfg}{%
  \typeout{Using configuration file gnuplot.cfg}%
}{%
 \typeout{No configuration file gnuplot.cfg found.}%
}%
%
\begin{document}
\thispagestyle{empty}
\input{${basename}.tex}
\end{document}
EOF1
fi
# -----------
# extra: amsmath ?
# -----------
if $add_amsmath; then
  cat > gnuplot.cfg << EOF0
\usepackage{amssymb,amsmath,amsfonts,mathrsfs}
\usepackage[T1]{fontenc}
EOF0
else
  echo > gnuplot.cfg 
fi
# -----------
# run latex
# -----------
if $run_latex; then
  #echo "VERBOSE run_latex..."
  # small fix for some file names containing '=' such as "evp-shear-eta-a=1-r=1.5.plot" :
  if test $suffix = "dvi"; then
    command="sed -e 's/includegraphics{\(.*\)}}/includegraphics{{\1}.eps}}/' < $basename.tex > $basename.tex.new && mv $basename.tex.new $basename.tex"
  else
    command="sed -e 's/includegraphics{\(.*\)}}/includegraphics{{\1}.pdf}}/' < $basename.tex > $basename.tex.new && mv $basename.tex.new $basename.tex"
  fi
  checked_run $command
  run "(echo R | $latex $wrapname 2>&1) >> $basename.glog"
  status=$?
  nerr=`grep -i "Latex Error" $wrapname.log | wc -l`
  test -f $wrapname.$suffix; status_exists=$?
  if test $status -ne 0 || test $nerr -ne 0 || test $status_exists -ne 0; then
    cat $basename.glog	
    echo "! $0: $latex ERROR with $name.plot" 1>&2
    #echo "VERBOSE status=$status"
    #echo "VERBOSE file $wrapname.$suffix : $status_exists"
    #echo "VERBOSE nerr=$nerr"
    grep -i "Latex Error" $wrapname.log
    clean_and_exit 1
  fi
  to_clean="$to_clean $basename.glog $wrapname.log"
  if test $suffix = "dvi"; then
    #echo "VERBOSE dvi..."
    checked_run "dvips -E $wrapname.dvi -o $basename.ps"
    checked_run "ps2epsi $basename.ps $basename.eps"
    if test "$output_format" = "pdf"; then
      checked_run "epstopdf $basename.eps"
    fi
  else
    checked_run "pdfcrop --resolution 300 $wrapname.pdf $basename-new.pdf && mv $basename-new.pdf $basename.pdf"
  fi
else # not run_latex
  checked_run "pdfcrop --resolution 300 $basename.pdf $basename-new.pdf && mv $basename-new.pdf $basename.pdf"
fi
if test "$dir" != "."; then
  checked_run "mv $basename.pdf $dir"
fi
clean_and_exit 0
