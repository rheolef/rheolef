#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# --------------------------------------------------------------------------

#------------------------------------------------------------------------------
# rules for documentation: unix man pages & reference manual
#------------------------------------------------------------------------------
# produce file suffixes:
#     .Nrheolef  : unix man pages, N=section number in {1,..,9}
#     .manN      : links, used as marks for automatic TOC in html doc

# the shell-script that produces .Nrheolef files (by using doxygen):
SRC2MAN      = ${top_builddir}/config/src2man.sh
SRC2MAN_DEPS = \
	${SRC2MAN} \
	${top_srcdir}/config/Doxyfile-man.in \
	${top_builddir}/config/src2dox_filter_man 
# SRC2MAN_FLAGS : defined by the configure script, e.g. -warning-as-error
SRC2MAN_OPTS = $(SRC2MAN_FLAGS)

# for rheolef-config.in : is in config/ and need a special dependency
rheolef-config.1rheolef: $(srcdir)/rheolef-config.in src2man.sh Doxyfile-man.in src2dox_filter_man
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 1 $< $@
rheolef-config.man1: $(srcdir)/rheolef-config.in
	rm -f $@ && ln -s $< $@

# for Float.h : is in config/ and need a special dependency
Float.2rheolef: $(srcdir)/Float.h src2man.sh Doxyfile-man.in src2dox_filter_man
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 2 $< $@
Float.man2: $(srcdir)/Float.h
	rm -f $@ && ln -s $< $@

# for .sh shell scripts that are unix commands:
%.1rheolef: $(srcdir)/%.sh ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 1 $< $@
%.man1: $(srcdir)/%.sh
	rm -f $@ && ln -s $< $@

# for .cc that are unix commands:
%.1rheolef: $(srcdir)/%.cc ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 1 $< $@
%.man1: $(srcdir)/%.cc
	rm -f $@ && ln -s $< $@

# for .h that are classes:
%.2rheolef: $(srcdir)/%.h ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 2 $< $@
%.man2: $(srcdir)/%.h
	rm -f $@ && ln -s $< $@

# for .h that are functions:
%.3rheolef: $(srcdir)/%.h ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 3 $< $@
%.man3: $(srcdir)/%.h
	rm -f $@ && ln -s $< $@

# for .h that are linalg classes:
%.4rheolef: $(srcdir)/%.h ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 4 $< $@
%.man4: $(srcdir)/%.h
	rm -f $@ && ln -s $< $@

# for .h that are linalg functions:
%.5rheolef: $(srcdir)/%.h ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 5 $< $@
%.man5: $(srcdir)/%.h
	rm -f $@ && ln -s $< $@

# for .h & .icc that are fem classes & functions:
%.6rheolef: $(srcdir)/%.h ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 6 $< $@
%.6rheolef: $(srcdir)/%.icc ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 6 $< $@
%.man6: $(srcdir)/%.h
	rm -f $@ && ln -s $< $@
%.man6: $(srcdir)/%.icc
	rm -f $@ && ln -s $< $@

# for .h that are util classes & functions:
%.7rheolef: $(srcdir)/%.h ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 7 $< $@
%.man7: $(srcdir)/%.h
	rm -f $@ && ln -s $< $@

# for .h that are internal classes:
%.8rheolef: $(srcdir)/%.h ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 8 $< $@
%.man8: $(srcdir)/%.h
	rm -f $@ && ln -s $< $@

# for .h that are internal functions:
%.9rheolef: $(srcdir)/%.h ${SRC2MAN_DEPS}
	bash ${SRC2MAN} ${SRC2MAN_OPTS} -section 9 $< $@
%.man9: $(srcdir)/%.h
	rm -f $@ && ln -s $< $@

#------------------------------------------------------------------------------
# rules for parsers: flex & bison
#------------------------------------------------------------------------------
%.cc: %.flex
	@if test x"$(FLEX)" != x""; then \
	    echo "$(FLEX) -t -+ -i $< | ${SHELL} ${top_srcdir}/config/flexfix.sh $(FLEXLEXER_H) > $(srcdir)/$*.cc"; \
	    (  echo "#pragma GCC diagnostic push"; \
	       echo "#pragma GCC diagnostic ignored \"-Weffc++\" // yyFlexLexer has uninitialized members"; \
	       $(FLEX) -t -+ -i $< | ${SHELL} ${top_srcdir}/config/flexfix.sh $(FLEXLEXER_H); \
	       echo "#pragma GCC diagnostic pop"; \
            ) > $(srcdir)/$*.cc; \
	else \
	    test -f $@ && touch $@; \
	fi

# bison rule
$(srcdir)/%.cc: $(srcdir)/%.y
	@if [ x"$(BISON)" != x"" ]; then \
	    echo "$(BISON) -d $*.y -o ${srcdir}/$*.cc"; \
	    $(BISON) ${srcdir}/$*.y -o ${srcdir}/$*.cc; \
	else \
	  if test -f ${srcdir}/$*.cc; then \
	    echo "BISON: warning: "${srcdir}/$*.cc" not updated (BISON not available)."; \
	  else \
	    echo "BISON: error: "${srcdir}/$*.cc" not created (BISON not available)."; \
	    exit 1; \
	  fi \
	fi

#------------------------------------------------------------------------------
# rules for documentation: figures
#------------------------------------------------------------------------------
# figures latex + postscript -> pdf
%.pdf: %.fig
	@if test x"${FIG2DEV}" != x""; then				\
	  rm -f $*.tex $*.pdf;						\
	  bash $(top_srcdir)/config/fig2pdf.sh $(srcdir)/$*.fig;	\
	else								\
	  echo "warning: fig2dev missing";				\
	  echo "touch $*.pdf";						\
	  touch $*.pdf;							\
	fi

#------------------------------------------------------------------------------
# rules for includes: symbolic links
#------------------------------------------------------------------------------
MKSYMLINK = /bin/sh ${top_srcdir}/config/mk-symlink.sh \
		${top_srcdir} \
		${top_builddir} \
		${srcdir}

${addprefix ${top_builddir}/include/rheolef/, ${pkginclude_HEADERS}}:
	$(MKSYMLINK) $(pkginclude_HEADERS)
${addprefix ${top_builddir}/include/rheolef/, ${machine_DATA}}:
	$(MKSYMLINK) $(machine_DATA)

#------------------------------------------------------------------------------
# rules for various checks
#------------------------------------------------------------------------------
# check for unexpected special characters in sources
check-ascii:
	@bash ${top_srcdir}/config/check-non-ascii.sh ${srcdir}

# check for missing license in sources
check-license:
	@if test -f $(CVSMKROOT)/git.mk; then 				\
	   all=`${MAKE} mk-license`; 					\
	   bash ${top_srcdir}/config/check-license.sh -d $(srcdir) $$all; \
	 fi
show-license:
	@if test -f $(CVSMKROOT)/git.mk; then 				\
	   all=`${MAKE} mk-license`; 					\
	   cd $(srcdir) ; 						\
	   grep -ih "copyrigh[ \t][ \t]*(c)" $$all | sed -e 's/^.*opyright/Copyright/' | sort -u; \
	 fi
