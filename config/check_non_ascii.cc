//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
#include<iostream>
#include<fstream>
#include<cstdio>
using namespace std;
void check_non_ascii (
  bool debug,
  bool dump,
  string valid_set,
  string filename,
  istream& in)
{
  if (debug) {
    cerr << "valid_set["<<valid_set.size()<<"]=\""<<valid_set<<"\""<<endl;
    for (size_t i = 0; i < valid_set.size(); ++i) {
      char c = valid_set[i];
      cerr << "valid_set["<<i<<"]=`"<<char(c)<<"' code="<<int(c)<<" : ascii" <<endl;
    }
  }
  string line = "";
  for (int line_no = 1; getline(in,line); line_no++) {
    if (dump) cout << line << endl;
    int iwc = 0; 
    for (size_t ic = 0; ic < line.size(); ++ic) {
      char c = line[ic];
      if (int(c) != -61) iwc++; // utf8 are two-char : first is -61 and second has info
      bool valid = true;
      if (c >= 0) {
        valid = true;
        // cerr << "c=`"<<char(c)<<"' code="<<int(c)<<" : ascii" <<endl;
      } else {
        valid = false;
        for (size_t i = 0; i < valid_set.size(); ++i) {
          if (c == valid_set[i]) {
	    valid = true;
            // cerr << "c=`"<<char(c)<<"' code="<<int(c)<<" : non-ascii("<<i<<") founded" <<endl;
	    break;
          }
        }
      }
      if (!valid) { 
        char c0 = (ic > 1 && int(line[ic-1]) < 0) ? line[ic-1] : 0;
        if (debug) cerr<< "prec char(ic="<<ic<<")=`"<<char(c0)<<"' (code="<<int(c0)<<")" << endl;
        cerr << filename << "("<<line_no<<"): "
             << "invalid character `"<<char(c0)<<char(c)<<"' (code="<<int(c)<<")"
             << " at position " << iwc <<endl;
        if (line.size() > 0) { 
          cerr << line << endl;
          for (int jwc = 0; jwc + 1 < iwc; ++jwc) {
	    cerr << " ";
          }
	  cerr << "^" << endl;
        }
        exit(1);
      }
    }
    line = "";
  }
}
int main(int argc, char**argv) {
  bool debug = false;
  bool dump = false;
  bool ascii = (argc == 1 || (argc > 1 && string(argv[1]) == "-ascii")); 
  bool fr_utf8  = (argc > 1 && string(argv[1]) == "-fr-utf8");
  string fr_utf8_set = "àâäÀÂÄçÇéèêëÉÈÊËîïÎÏôöÔÖùÙûÛüÜœ";
  string valid_set = ascii ? "" : (fr_utf8 ? fr_utf8_set : "");
  string filename = (argc > 1 && argv[1][0] != '-') ? argv[1] : 
                    (argc > 2 && argv[2][0] != '-') ? argv[2] : "-";
  if (filename == "-") {
    check_non_ascii (debug, dump, valid_set, "sdtin", cin);
  } else {
    ifstream in (filename);
    check_non_ascii (debug, dump, valid_set, filename, in);
  }
}
