dnl
dnl This file is part of Rheolef.
dnl
dnl Copyright (C) 2000-2009 Pierre Saramito 
dnl
dnl Rheolef is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl 
dnl Rheolef is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with Rheolef; if not, write to the Free Software
dnl Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
dnl 
dnl -----------------------------------------------------------------------------
dnl
dnl NOTE: lines starting with 'dnl #' are filetered for documentation
dnl
dnl -----------------------------------------------------------------------------
dnl #F:acinclude
dnl #NAME: @code{acinclude} -- autoconf macros
dnl #@cindex porting the code
dnl #@toindex @code{autoconf}
dnl #DESCRIPTION:
dnl #    These macros test for particular system featutres that 
dnl #    rheolef uses. These tests print the messages telling
dnl #    the user which feature they are looking for and what
dnl #    they find. They cache their results for future
dnl #    @code{configure} runs.
dnl #    Some of these macros
dnl #    @dfn{set} some shell variable,
dnl #    @dfn{defines} some output variables for the @file{config.h} header,
dnl #    or performs Makefile macros @dfn{subsitutions}.
dnl #    See @code{autoconf} documentation for how to use such
dnl #    variables.
dnl #SYNOPSIS:
dnl #  Follows a list of particular check required for a
dnl #  successful installation.
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_GINAC}
dnl #
dnl #    Check to see if GiNaC library exists.
dnl #    If so, set the shell variable @code{rheo_have_ginac}
dnl #    to "yes", defines HAVE_GINAC and
dnl #    substitues INCLUDES_GINAC and LADD_GINAC
dnl #    for adding in CFLAGS and LIBS, respectively,
dnl #    If not, set the shell variable rheo_have_ginac to "no".
dnl #
AC_DEFUN([RHEO_CHECK_GINAC],
    rheo_have_ginac=no
    [AH_TEMPLATE([HAVE_GINAC], Defines if you have ginac library)]
    [AC_SUBST(INCLUDES_GINAC)]
    [AC_SUBST(LDADD_GINAC)]
    [AC_PATH_PROG(PKG_CONFIG, pkg-config, no)]
    if test x"$PKG_CONFIG" != x"no" ; then
      GINAC_VERSION=`pkg-config ginac --modversion 2>/dev/null`
      if test x"$GINAC_VERSION" != x""; then
        LDADD_GINAC=`pkg-config ginac --libs 2>/dev/null`
        INCLUDES_GINAC=`pkg-config ginac --cflags 2>/dev/null`
        rheo_have_ginac=yes
      fi
    fi
    if test x"$rheo_have_ginac" != x"yes" ; then
      [AC_PATH_PROG(GINACLIB_CONFIG, ginac-config, no)]
      if test x"$GINACLIB_CONFIG" != x"no" ; then
        LDADD_GINAC=`ginac-config --cppflags`
        INCLUDES_GINAC=`ginac-config --libs`
        rheo_have_ginac=yes
      fi
    fi
    [AC_MSG_CHECKING(for GiNaC)]
    if test x"$rheo_have_ginac" != x"no" ; then
      [AC_DEFINE(HAVE_GINAC)]
    fi
    [AC_MSG_RESULT($rheo_have_ginac)]
)
dnl #
dnl #    @code{RHEO_CHECK_CLN}
dnl #
dnl #    Check to see if library @code{-lcln} exists.
dnl #    If so, set the shell variable @code{rheo_have_cln}
dnl #    to "yes", defines HAVE_CLN and
dnl #    substitues INCLUDES_CLN and LADD_CLN
dnl #    for adding in CFLAGS and LIBS, respectively,
dnl #    If not, set the shell variable no "no".
dnl #    Includes and libraries path are searched from a
dnl #    given shell variable @code{rheo_dir_cln}.
dnl #    This shell variable could be set for instance
dnl #    by an appropriate @code{--with-cln}=@var{value_dir_cln} option.
dnl #    The default value is @code{/usr/local/math}.
dnl #
AC_DEFUN([RHEO_CHECK_CLN],
    [AC_MSG_CHECKING(for libcln)]
    [AH_TEMPLATE([HAVE_CLN], Defines if you have cln library)]
    if test -f ${rheo_dir_cln}/lib/libcln.la; then
        rheo_have_cln=yes
        [AC_DEFINE(HAVE_CLN)]
        [AC_SUBST(INCLUDES_CLN)]
        [AC_SUBST(LDADD_CLN)]
        INCLUDES_CLN="-I${rheo_dir_cln}/include"
        LDADD_CLN="${rheo_dir_cln}/lib/libcln.la"
        echo ${ac_n} "${LDADD_CLN}${ac_c}... "
    else
        rheo_have_cln=no
    fi
    [AC_MSG_RESULT(${rheo_have_cln})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_SPOOLES_2_0}
dnl #
dnl #    Check to see if spooles library has old version 2.0
dnl #    since @code{FrontMtx_factorInpMtx} profile has changed
dnl #    in version 2.2.
dnl #    If so, defines HAVE_SPOOLES_2_0.
dnl #    This macro is called by RHEO_CHECK_SPOOLES.
dnl #
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_SPOOLES_2_0],
    [AC_MSG_CHECKING(for libspooles version 2.0)]
    [AH_TEMPLATE([HAVE_SPOOLES_2_0], Defines if you have spooles library)]
    [AC_SUBST(INCLUDES_SPOOLES_2_0)]
    PREV_CXXFLAGS=$CXXFLAGS
    CXXFLAGS="$CXXFLAGS -I${rheo_incdir_spooles}"
    [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
		extern "C" {
		#include <misc.h>
		#include <FrontMtx.h>
		#include <SymbFac.h>
		}
	    %%,
	    %%
		FrontMtx      *frontmtx;
		InpMtx *mtxA = InpMtx_new() ;
		double tau = 100;
		ChvManager *chvmanager = ChvManager_new() ;
		double cpus[10] ;
		int stats[20] ;
		int msglvl = 1;
		FILE *msgFile = stdout;
		Chv *rootchv = FrontMtx_factorInpMtx(
			frontmtx, mtxA, tau, 0.0, chvmanager,
			cpus, stats, msglvl, msgFile) ;
	    %%,
	    changequote([, ])dnl
	    rheo_have_spooles_2_0=yes,
	    rheo_have_spooles_2_0=no
        )]
    CXXFLAGS=$PREV_CXXFLAGS
    if test x"${rheo_have_spooles_2_0}" = x"yes"; then
	[AC_DEFINE(HAVE_SPOOLES_2_0)]
    fi
    [AC_MSG_RESULT(${rheo_have_spooles_2_0})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_TAUCS}
dnl #
dnl #    Check to see if taucs library and headers exists.
dnl #    If so, set the shell variable "rheo_have_taucs"
dnl #    to "yes", defines HAVE_TAUCS and
dnl #    substitues INCLUDES_TAUCS and LADD_TAUCS
dnl #    for adding in CXXFLAGS and LIBS, respectively,
dnl #    If not, set the shell variable to "no".
dnl #    Includes and libraries options are
dnl #    given shell variable $rheo_ldadd_taucs and $rheo_incdir_taucs.
dnl #    These shell variables could be set for instance
dnl #    by appropriates "--with-taucs-ldadd="'rheo_ldadd_taucs' and
dnl #    "--with-taucs-includes="'rheo_incdir_taucs' options.
dnl #
AC_DEFUN([RHEO_CHECK_TAUCS],
    [AH_TEMPLATE([HAVE_TAUCS], Defines if you have taucs library)]
    [AC_SUBST(INCLUDES_TAUCS)]
    [AC_SUBST(LDADD_TAUCS)]

    rheo_have_taucs=yes
    PREV_CPPFLAGS=$CPPFLAGS
    PREV_LIBS=$LIBS
    INCLUDES_TAUCS=${rheo_incdir_taucs}
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_TAUCS}"
    [AC_CHECK_HEADER(taucs.h, true, rheo_have_taucs=no)]

    if test x"$rheo_have_taucs" = x"yes"; then
        LDADD_TAUCS=${rheo_ldadd_taucs}
        LIBS="${LIBS} ${LDADD_TAUCS}"
        [AC_MSG_CHECKING(for libtaucs)]
        [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
	        extern "C" {
		#include <taucs.h>
		}
	    %%,
	    %%
	        taucs_dccs_create(1,1,1);
	    %%,
	    changequote([, ])dnl
	    true,
	    rheo_have_taucs=no
        )]
        [AC_MSG_RESULT(${rheo_have_taucs})]
    fi
    if test x"$rheo_have_taucs" = x"yes"; then
        [AC_DEFINE(HAVE_TAUCS)]
    else
        INCLUDES_TAUCS=""
        LDADD_TAUCS=""
    fi
    CPPFLAGS=$PREV_CPPFLAGS
    LIBS=$PREV_LIBS
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_BOOST_UBLAS}
dnl #
dnl #    Check to see if boost headers exists.
dnl #    If so, set the shell variable "rheo_have_boost"
dnl #    to "yes", defines HAVE_BOOST and
dnl #    substitues INCLUDES_BOOST for adding in CXXFLAGS,
dnl #    and LDADD_BOOST for adding in LIBS.
dnl #    If not, set the shell variable to "no".
dnl #    Includes options are
dnl #    given in the shell variables $rheo_incdir_boost and $rheo_libdir_boost.
dnl #    These shell variables could be set for instance
dnl #    by appropriates "--with-boost-includes="'rheo_incdir_boost'
dnl #    and "--with-boost-libdir="'rheo_libdir_boost' options.
dnl #
AC_DEFUN([RHEO_CHECK_BOOST_UBLAS],
    [AH_TEMPLATE([HAVE_BOOST], Defines if you have boost library)]
    [AC_SUBST(INCLUDES_BOOST)]
    [AC_SUBST(LDADD_BOOST)]
    PREV_CPPFLAGS=$CPPFLAGS

    rheo_have_boost=no
    [AC_CHECK_HEADER(boost/numeric/ublas/matrix.hpp, rheo_have_boost=yes)]
    if test x"$rheo_have_boost" = x"no"; then
      # MAC OS 10 : fink install boost1.33
      # -> /sw/include/boost-1_33_1/boost/numeric/ublas/matrix.hpp
      macdir="/sw/include"
      if test -d $macdir; then
        f=`find $macdir -name matrix.hpp 2>/dev/null`
        if test x"$f" = x""; then
          macincdir=""
        else
          macincdir=`expr $f : '\(.*\)/boost/numeric/ublas/matrix.hpp'`
        fi
      fi
      inc_path_boost="${rheo_incdir_boost} /usr/include ${macdir} ${prefix}/include /usr/local/include"
      for dir in ${inc_path_boost}; do
        [AC_MSG_CHECKING(for boost/numeric/ublas/matrix.hpp presence in $dir...)]
        if test -f $dir/boost/numeric/ublas/matrix.hpp; then
	  [AC_MSG_RESULT(yes)]
	  rheo_have_boost=yes
          if test "$dir" != "/usr/include"; then
              INCLUDES_BOOST="-I${dir}"
          fi
          if test x"${rheo_libdir_boost}" != x""; then
              LDADD_BOOST="-L${rheo_libdir_boost}"
          fi
	  CPPFLAGS="$CPPFLAGS $INCLUDES_BOOST"
	  break
        else
	  [AC_MSG_RESULT(no)]
	fi
      done
    fi
    if test x"$rheo_have_boost" = x"yes"; then
        [AC_MSG_CHECKING(for boost/numeric/ublas/matrix.hpp with c++ compiler)]
        [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
		#include <boost/numeric/ublas/matrix.hpp>
	        namespace ublas = boost::numeric::ublas;
	    %%,
	    %%
		ublas::matrix<double> a (2,2);
	    %%,
	    changequote([, ])dnl
	    true,
	    rheo_have_boost=no
        )]
        [AC_MSG_RESULT(${rheo_have_boost})]
    fi
    if test x"$rheo_have_boost" = x"yes"; then
        [AC_DEFINE(HAVE_BOOST)]
    else
        INCLUDES_BOOST=""
    fi
    CPPFLAGS=$PREV_CPPFLAGS
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_EIGEN}
dnl #
dnl #    Check to see if eigen headers exists.
dnl #    If so, set the shell variable "rheo_have_eigen"
dnl #    to "yes", defines HAVE_EIGEN and
dnl #    substitues INCLUDES_EIGEN for adding in CXXFLAGS,
dnl #    and LDADD_EIGEN for adding in LIBS.
dnl #    If not, set the shell variable to "no".
dnl #    Includes options are
dnl #    given in the shell variables $rheo_incdir_eigen and $rheo_libdir_eigen.
dnl #    These shell variables could be set for instance
dnl #    by appropriates "--with-eigen-includes="'rheo_incdir_eigen'
dnl #    and "--with-eigen-libdir="'rheo_libdireigen' options.
dnl #
AC_DEFUN([RHEO_CHECK_EIGEN],
    [AH_TEMPLATE([HAVE_EIGEN], Defines if you have eigen library)]
    [AC_SUBST(INCLUDES_EIGEN)]
    [AC_SUBST(LDADD_EIGEN)]
    PREV_CPPFLAGS=$CPPFLAGS
    rheo_have_eigen=no
    [AC_CHECK_HEADER(Eigen/Dense, rheo_have_eigen=yes)]
    # try to autodetect...
    if test x"$rheo_have_eigen" = x"no"; then
      inc_path_eigen="${rheo_incdir_eigen} /usr/include ${prefix}/include /usr/local/include/eigen3"
      for dir in ${inc_path_eigen}; do
        [AC_MSG_CHECKING(Eigen/Dense presence in $dir...)]
        if test -f $dir/Eigen/Dense; then
	  [AC_MSG_RESULT(yes)]
	  rheo_have_eigen=yes
          if test "$dir" != "/usr/include"; then
              INCLUDES_EIGEN="-I${dir}"
          fi
          if test x"${rheo_libdir_eigen}" = x""; then
              LDADD_EIGEN=""
          fi
	  CPPFLAGS="$CPPFLAGS $INCLUDES_EIGEN"
	  break
        else
	  [AC_MSG_RESULT(no)]
	fi
      done
    fi
    [AC_MSG_CHECKING(for libeigen)]
    if test x"${rheo_libs_eigen}" != x"yes" -a x"${rheo_libs_eigen}" != x""; then
      LDADD_EIGEN="${rheo_libs_eigen}"
    else
      LDADD_EIGEN=""
    fi
    if test x"${rheo_libdir_eigen}" != x"yes" -a x"${rheo_libdir_eigen}" != x""; then
      LDADD_EIGEN="-L${rheo_libdir_eigen} ${LDADD_EIGEN}"
    fi
    [AC_MSG_RESULT($LDADD_EIGEN)]
    if test x"$rheo_have_eigen" = x"yes"; then
        PREV_CPPFLAGS=$CPPFLAGS
        PREV_LIBS=$LIBS
        CPPFLAGS="${CPPFLAGS} ${INCLUDES_EIGEN}"
        LIBS="${LIBS} ${LDADD_EIGEN}"
        [AC_MSG_CHECKING(for eigen with c++ compiler)]
        [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
                #pragma GCC diagnostic ignored "-Weffc++"
		#include <Eigen/Dense>
	    %%,
	    %%
		Eigen::Matrix<double,2,2> a;
  	        a(0,0)=a(1,1)=1;
  	        a(0,1)=a(1,0)=0;
	    %%,
	    changequote([, ])dnl
	    true,
	    rheo_have_eigen=no
        )]
        [AC_MSG_RESULT(${rheo_have_eigen})]
        CPPFLAGS=$PREV_CPPFLAGS
        LIBS=$PREV_LIBS
    fi
    if test x"$rheo_have_eigen" = x"yes"; then
        [AC_DEFINE(HAVE_EIGEN)]
    else
        INCLUDES_EIGEN=""
        LDADD_EIGEN=""
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_ZLIB}
dnl #
dnl #    Check to see if zlib library and headers exists.
dnl #    If so, set the shell variable "rheo_have_zlib"
dnl #    to "yes", defines HAVE_ZLIB and
dnl #    substitues INCLUDES_ZLIB and LADD_ZLIB
dnl #    for adding in CXXFLAGS and LIBS, respectively,
dnl #    If not, set the shell variable to "no".
dnl #    Includes and libraries path are searched from
dnl #    given shell variable $rheo_dir_zlib/lib and $rheo_incdir_zlib.
dnl #    Default value for $rheo_incdir_zlib is $rheo_dir_zlib/include.
dnl #    These shell variables could be set for instance
dnl #    by appropriates "--with-zlib="'dir_zlib' and
dnl #    "--with-zlib-includes="'incdir_zlib' options.
dnl #
AC_DEFUN([RHEO_CHECK_ZLIB],
    [AH_TEMPLATE([HAVE_ZLIB], Defines if you have zlib library)]
    [AC_SUBST(INCLUDES_ZLIB)]
    [AC_SUBST(LDADD_ZLIB)]

    rheo_have_zlib=yes
    if test x"${rheo_dir_zlib}" != x""; then
        rheo_libdir_zlib="$rheo_dir_zlib/lib"
    fi
    if test x"${rheo_dir_zlib}" != x"" -a x"${rheo_incdir_zlib}" = x""; then
        rheo_incdir_zlib="${rheo_dir_zlib}/include"
    fi
    PREV_CPPFLAGS=$CPPFLAGS
    PREV_LIBS=$LIBS
    if test x"${rheo_incdir_zlib}" != x""; then
        INCLUDES_ZLIB="-I${rheo_incdir_zlib}"
        CPPFLAGS="${CPPFLAGS} ${INCLUDES_ZLIB}"
    else
        INCLUDES_ZLIB=""
    fi
    [AC_CHECK_HEADER(zlib.h, true, rheo_have_zlib=no)]

    if test x"$rheo_have_zlib" = x"yes"; then
        if test x"${rheo_libdir_zlib}" != x""; then
            LDADD_ZLIB="-L${rheo_libdir_zlib} -lz"
        else
            LDADD_ZLIB="-lz"
        fi
        LIBS="${LIBS} ${LDADD_ZLIB}"
        [AC_MSG_CHECKING(for -lz)]
        [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
	        extern "C" {
		#include <zlib.h>
		}
	    %%,
	    %%
	        deflate(0,0);
	    %%,
	    changequote([, ])dnl
	    true,
	    rheo_have_zlib=no
        )]
        [AC_MSG_RESULT(${rheo_have_zlib})]
    fi
    if test x"$rheo_have_zlib" = x"yes"; then
        [AC_DEFINE(HAVE_ZLIB)]
    else
        INCLUDES_ZLIB=""
        LDADD_ZLIB=""
    fi
    CPPFLAGS=$PREV_CPPFLAGS
    LIBS=$PREV_LIBS
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_SPOOLES}
dnl #
dnl #    Check to see if spooles library and headers exists.
dnl #    If so, set the shell variable "rheo_have_spooles"
dnl #    to "yes", defines HAVE_SPOOLES and
dnl #    substitues INCLUDES_SPOOLES and LADD_SPOOLES
dnl #    for adding in CXXFLAGS and LIBS, respectively,
dnl #    If not, set the shell variable to "no".
dnl #    Includes and libraries path are searched from
dnl #    given shell variable "rheo_libdir_spooles" and "rheo_incdir_spooles".
dnl #    These shell variables could be set for instance
dnl #    by appropriates "--with-spooles="'libdir_spooles' and
dnl #    "--with-spooles-includes="'incdir_spooles' options.
dnl #
AC_DEFUN([RHEO_CHECK_SPOOLES],
    [AC_MSG_CHECKING(for libspooles)]
    [AH_TEMPLATE([HAVE_SPOOLES], Defines if you have spooles library)]
    [AC_SUBST(INCLUDES_SPOOLES)]
    [AC_SUBST(LDADD_SPOOLES)]
    # get absolute path name:
    path_spooles="${prefix} /usr/local/math /usr/local /usr /"
    if test x"${rheo_libdir_spooles}" != x""; then
        dir="`expr ${rheo_libdir_spooles} : '\./\(.*\)' \| ${rheo_libdir_spooles}`"
        path_spooles="${dir} ${path_spooles}"
    fi
    default_rheo_libdir_spooles=${rheo_libdir_spooles-"/usr/local/math"}
    rheo_libdir_spooles=""
    for sym_dir in ${path_spooles}; do
        dir=`eval "echo ${sym_dir}"`
	for lib in libspooles.so libspooles.sl libspooles.a spooles.a; do
          if test -f "${dir}/${lib}" -o -f "${dir}/lib/${lib}"; then
	    rheo_dir_spooles=${dir}
            if test -f "${dir}/${lib}"; then
	      rheo_libdir_spooles=${dir}
	    else
	      rheo_libdir_spooles=${dir}/lib
	    fi
            [AC_MSG_RESULT(${rheo_libdir_spooles}/${lib})]
            LDADD_SPOOLES=${rheo_libdir_spooles}/${lib}
    	    rheo_have_spooles=yes
	    break
	  fi
	done
	if test x"${rheo_libdir_spooles}" != x""; then
	    break
        fi
    done
    if test x"${rheo_have_spooles}" = x"yes"; then
        [AC_MSG_CHECKING(for spooles/FrontMtx.h)]
        if test x"${rheo_dir_spooles}" != x""; then
     	    if test -f "${rheo_dir_spooles}/FrontMtx.h"; then
                rheo_incdir_spooles="${rheo_dir_spooles}"
	    elif test -f "${rheo_dir_spooles}/include/FrontMtx.h"; then
                rheo_incdir_spooles="${rheo_dir_spooles}/include"
	    elif test -f "${rheo_dir_spooles}/include/spooles/FrontMtx.h"; then
                rheo_incdir_spooles="${rheo_dir_spooles}/include/spooles"
	    fi
        fi
     	if test -f "${rheo_incdir_spooles}/FrontMtx.h"; then
            [AC_MSG_RESULT(${rheo_incdir_spooles}/FrontMtx.h)]
	    INCLUDES_SPOOLES="-I${rheo_incdir_spooles}"
	else
    	    rheo_have_spooles=no
        fi
    fi
    if test x"${rheo_have_spooles}" != x"yes"; then
	INCLUDES_SPOOLES=""
	LDADD_SPOOLES=""
        [AC_MSG_RESULT($rheo_have_spooles)]
	[AC_MSG_RESULT(** WARNING: recommended spooles library not found)]
    else
	[AC_DEFINE(HAVE_SPOOLES)]
	[RHEO_CHECK_SPOOLES_2_0]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_CHOLMOD}
dnl #
dnl #    Check to see if cholmod library and headers exists.
dnl #    If so, set the shell variable "rheo_have_cholmod"
dnl #    to "yes", defines HAVE_CHOLMOD and
dnl #    substitues INCLUDES_CHOLMOD and LADD_CHOLMOD
dnl #    for adding in CXXFLAGS and LIBS, respectively,
dnl #    If not, set the shell variable to "no".
dnl #    Includes and libraries path are searched from
dnl #    given shell variable "rheo_libdir_cholmod" and "rheo_incdir_cholmod".
dnl #    These shell variables could be set for instance
dnl #    by appropriates "--with-cholmod="'libdir_cholmod' and
dnl #    "--with-cholmod-includes="'incdir_cholmod' options.
dnl #
AC_DEFUN([RHEO_CHECK_CHOLMOD],
    [AC_MSG_CHECKING(for libcholmod)]
    [AH_TEMPLATE([HAVE_CHOLMOD], Defines if you have cholmod library)]
    [AC_SUBST(INCLUDES_CHOLMOD)]
    [AC_SUBST(LDADD_CHOLMOD)]
   
    # assume debian style :
    rheo_have_cholmod=yes
    if test x"${rheo_libs_cholmod}" != x"yes" -a x"${rheo_libs_cholmod}" != x""; then
      LDADD_CHOLMOD="${rheo_libs_cholmod}"
    else
      LDADD_CHOLMOD="-lcholmod -lamd"
    fi
    if test x"${rheo_libdir_cholmod}" != x"yes" -a x"${rheo_libdir_cholmod}" != x""; then
      LDADD_CHOLMOD="-L${rheo_libdir_cholmod} ${LDADD_CHOLMOD}"
    fi
    [AC_MSG_RESULT($LDADD_CHOLMOD)]
    if test x"${rheo_incdir_cholmod}" != x""; then
      incdir_list="${rheo_incdir_cholmod}"
    else
      incdir_list="/usr/include/cholmod /usr/include/ufsparse /usr/include/suitesparse"
    fi
    [AC_MSG_CHECKING(for cholmod.h)]
    rheo_incdir_cholmod=""
    for d in ${incdir_list}; do
      if test -f "${d}/cholmod.h"; then
        rheo_incdir_cholmod="${d}"; break
      fi
    done
    if test -f "${rheo_incdir_cholmod}/cholmod.h"; then
            [AC_MSG_RESULT(${rheo_incdir_cholmod}/cholmod.h)]
            INCLUDES_CHOLMOD="-I${rheo_incdir_cholmod}"
    else
    	    rheo_have_cholmod=no
    fi
    if test x"${rheo_have_cholmod}" != x"yes"; then
	INCLUDES_CHOLMOD=""
	LDADD_CHOLMOD=""
        [AC_MSG_RESULT(${rheo_have_cholmod})]
	[AC_MSG_RESULT(** WARNING: recommended cholmod library not found)]
    else
	[AC_DEFINE(HAVE_CHOLMOD)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_UMFPACK}
dnl #
dnl #    Check to see if umfpack library and headers exists.
dnl #    If so, set the shell variable "rheo_have_umfpack"
dnl #    to "yes", defines HAVE_UMFPACK and
dnl #    substitues INCLUDES_UMFPACK and LADD_UMFPACK
dnl #    for adding in CXXFLAGS and LIBS, respectively,
dnl #    If not, set the shell variable to "no".
dnl #    Includes and libraries path are searched from
dnl #    given shell variable "rheo_libdir_umfpack" and "rheo_incdir_umfpack".
dnl #    These shell variables could be set for instance
dnl #    by appropriates "--with-umfpack="'libdir_umfpack' and
dnl #    "--with-umfpack-includes="'incdir_umfpack' options.
dnl #
AC_DEFUN([RHEO_CHECK_UMFPACK],
    [AC_MSG_CHECKING(for libumfpack)]
    [AH_TEMPLATE([HAVE_UMFPACK], Defines if you have umfpack library)]
    [AC_SUBST(INCLUDES_UMFPACK)]
    [AC_SUBST(LDADD_UMFPACK)]
   
    # assume debian style :
    rheo_have_umfpack=yes
    if test x"${rheo_libs_umfpack}" != x"yes" -a x"${rheo_libs_umfpack}" != x""; then
      LDADD_UMFPACK="${rheo_libs_umfpack}"
    else
      LDADD_UMFPACK="-lumfpack -lamd"
    fi
    if test x"${rheo_libdir_umfpack}" != x"yes" -a x"${rheo_libdir_umfpack}" != x""; then
      LDADD_UMFPACK="-L${rheo_libdir_umfpack} ${LDADD_UMFPACK}"
    fi
    [AC_MSG_RESULT($LDADD_UMFPACK)]
    if test x"${rheo_incdir_umfpack}" != x""; then
      incdir_list="${rheo_incdir_umfpack}"
    else
      incdir_list="/usr/include/umfpack /usr/include/ufsparse /usr/include/suitesparse"
    fi
    [AC_MSG_CHECKING(for umfpack.h)]
    rheo_incdir_umfpack=""
    for d in ${incdir_list}; do
      if test -f "${d}/umfpack.h"; then
        rheo_incdir_umfpack="${d}"; break
      fi
    done
    if test -f "${rheo_incdir_umfpack}/umfpack.h"; then
            [AC_MSG_RESULT(${rheo_incdir_umfpack}/umfpack.h)]
            INCLUDES_UMFPACK="-I${rheo_incdir_umfpack}"
    else
    	    rheo_have_umfpack=no
    fi
    if test x"${rheo_have_umfpack}" != x"yes"; then
	INCLUDES_UMFPACK=""
	LDADD_UMFPACK=""
        [AC_MSG_RESULT(${rheo_have_umfpack})]
	[AC_MSG_RESULT(** WARNING: recommended umfpack library not found)]
    else
	[AC_DEFINE(HAVE_UMFPACK)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_MALLOC_DBG}
dnl #
dnl #    Check to see if malloc debug library -lmalloc_dbg
dnl #    and corresponding header <malloc_dbg.h> exists.
dnl #    If so, set the shell variable @code{rheo_have_malloc_dbg}
dnl #    to "yes", defines HAVE_MALLOC_DBG,
dnl #    add @code{-I}@var{dir_malloc_dbg}@code{/include} to CFLAGS,
dnl #    add @var{dir_malloc_dbg}@code{/lib/libmalloc_dbg.a} to LIBS.
dnl #    Here, @var{dir_malloc_dbg} is the directory such that
dnl #    @var{dir_malloc_dbg}@code{/bin} appears in PATH and
dnl #    the command @var{dir_malloc_dbg}@code{/bin/malloc_dbg} exists.
dnl #    If not, set the variable to "no".
dnl #	 Set also LIBS_MALLOC_DBG to these flags.
AC_DEFUN([RHEO_CHECK_MALLOC_DBG],
    rheo_have_malloc_dbg=no
    [AC_PATH_PROG(malloc_dbg, malloc_dbg)]
    [AC_SUBST(LIBS_MALLOC_DBG)]
    if test x${malloc_dbg} != x; then
        [AC_MSG_CHECKING(for libmalloc_dbg)]
	[AH_TEMPLATE([HAVE_MALLOC_DBG], Defines if you have malloc_dbg library)]
        rheo_bindir_malloc_dbg=`expr ${malloc_dbg} : '\(.*\)/[/]*malloc_dbg' `
        rheo_dir_malloc_dbg=`expr ${rheo_bindir_malloc_dbg} : '\(.*\)/.*' `
        PREC_CFLAGS="$CFLAGS"
        CFLAGS="$CFLAGS -I${rheo_dir_malloc_dbg}/include"
        PREC_LIBS=${LIBS}
        LIBS_MALLOC_DBG="-L${rheo_dir_malloc_dbg}/lib -lmalloc_dbg"
        LIBS="${LIBS} ${LIBS_MALLOC_DBG}"
        [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
	        extern "C" {
		#include <malloc_dbg.h>
		}
	    %%,
	    %%
	        malloc_verify(0);
	    %%,
	    changequote([, ])dnl
	    rheo_have_malloc_dbg=yes,
	    rheo_have_malloc_dbg=no
        )]
        if test x"${rheo_have_malloc_dbg}" != x"yes"; then
	    CFLAGS=${PREC_CFLAGS}
	    LIBS=${PREC_LIBS}
            LIBS_MALLOC_DBG=""
	else
	    rheo_have_malloc_dbg=yes
	    [AC_DEFINE(HAVE_MALLOC_DBG)]
	fi
        [AC_MSG_RESULT($rheo_have_malloc_dbg)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_DMALLOC}
dnl #
dnl #    @pindex dmalloc
dnl #    @toindex dmalloc, debug runtime library
dnl #    @cindex  debugging
dnl #
dnl #    Check whether the dmalloc
dnl #    package exists and set the corresponding
dnl #    shell value "rheo_have_dmalloc" and 
dnl #    HAVE_DMALLOC (in Makefile.am and config.h) accordingly,
dnl #    create LDADD_DMALLOC and LDADD_DMALLOCXX Makefile.am variables.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_DMALLOC], [
    AH_TEMPLATE([HAVE_DMALLOC], Defines if you use dmalloc library)
    rheo_have_dmalloc=no
    AC_CHECK_PROGS(DMALLOC, dmalloc)
    if test x"${DMALLOC}" != x""; then
      rheo_have_dmalloc=yes
    fi
    if test x"${rheo_have_dmalloc}" = xyes; then
      AC_DEFINE(HAVE_DMALLOC)
      AC_SUBST(LDADD_DMALLOCXX)
      AC_SUBST(LDADD_DMALLOC)
      AC_SUBST(INCLUDES_DMALLOC)
      AC_SUBST(INCLUDES_DMALLOCXX)
      LDADD_DMALLOCXX="\${top_builddir}/util/dmallocxx/libdmallocxx.la"
      LDADD_DMALLOC="-ldmalloc"
    fi
])
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_NAMESPACE}
dnl #
dnl #    Check whether the namespace feature
dnl #    is supported by the C++ compiler.
dnl #    value. So, try to compile
dnl #    the following code:
dnl #    @example
dnl #	   namespace computers @{
dnl #  	      struct keyboard @{ int getkey() const @{ return 0; @} @};
dnl #	   @}
dnl #	   namespace music @{
dnl #  	      struct keyboard @{ void playNote(int note); @};
dnl #	   @}
dnl #	   namespace music @{
dnl #  	      void keyboard::playNote(int note) @{ @}
dnl #	   @}
dnl #	   using namespace computers;
dnl #      int main() @{
dnl #         keyboard x;
dnl #         int z = x.getkey();
dnl #         music::keyboard y;
dnl #         y.playNote(z);
dnl #         return 0;
dnl #      @}
dnl #    @end example
dnl #    If it compile, set the corresponding 
dnl #    shell variable "rheo_have_namespace"
dnl #    to "yes" and defines HAVE_NAMESPACE.
dnl #    If not, set the variable no "no".
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_NAMESPACE],
    [AC_MSG_CHECKING(for namespace)]
    [AH_TEMPLATE([HAVE_NAMESPACE], Defines if you have C++ namespace)]
    [AC_TRY_COMPILE(
	changequote(%%, %%)dnl
	%%
	    namespace computers {
  	        struct keyboard { int getkey() const { return 0; } };
	    }
	    namespace music {
  	        struct keyboard { void playNote(int note); };
	    }
	    namespace music {
  		void keyboard::playNote(int note) { }
	    }
	    using namespace computers;
	%%,
	%%
    	    keyboard x;
    	    int z = x.getkey();
    	    music::keyboard y;
    	    y.playNote(z);
    	    return 0;
	%%,
	changequote([, ])dnl
	rheo_have_namespace=yes
	AC_DEFINE(HAVE_NAMESPACE),
	rheo_have_namespace=no
    )]
    [AC_MSG_RESULT($rheo_have_namespace)]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_STD_NAMESPACE}
dnl #
dnl #    Some compilers (e.g. GNU C++ 2.7.2) does not support
dnl #    the full namespace feature. Nevertheless, they support
dnl #    the "std:" namespace for the C++ library.
dnl #    is supported by the C++ compiler. The following code
dnl #    is submitted to the compiler:
dnl #    @example
dnl #         #include<vector.h>
dnl #         extern "C" void exit(int);
dnl #         int main() @{
dnl #             std::vector<int> x(3);
dnl #         	  return 0;
dnl #         @}
dnl #    @end example
dnl #    If it compile, set the corresponding 
dnl #    shell variable "rheo_have_std_namespace"
dnl #    to "yes" and defines HAVE_STD_NAMESPACE.
dnl #    If not, set the variable no "no".
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_STD_NAMESPACE],
    [AC_MSG_CHECKING(for namespace std::)]
    [AH_TEMPLATE([HAVE_STD_NAMESPACE], Defines if you have namespace std::)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
	%%
	    #include<vector.h>
	    extern "C" void exit(int);
	%%,
	%%
    	    std::vector<int> x(3);
    	    return 0;
	%%,
	changequote([, ])dnl
	rheo_have_std_namespace=yes
	[AC_DEFINE(HAVE_STD_NAMESPACE)],
	rheo_have_std_namespace=no
    )]
    [AC_MSG_RESULT($rheo_have_std_namespace)]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_PROG_GNU_MAKE}
dnl #
dnl #    Find command make and check whether make is GNU make.
dnl #    If so, set the corresponding 
dnl #    shell variable "rheo_prog_gnu_make"
dnl #    to "yes" and substitues no_print_directory_option
dnl #    to "--no-print-directory".
dnl #    If not, set the shell variable no "no".
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_PROG_GNU_MAKE],
    [AC_CHECK_PROGS(MAKE, make gmake gnumake, make)]
    [AC_MSG_CHECKING(whether make is GNU make)]
    echo "all:" > conftest.mk
    if $MAKE --no-print-directory -f conftest.mk 2>/dev/null 1>/dev/null; then
       rheo_prog_gnu_make=yes
       [AC_SUBST([no_print_directory_option])]
       no_print_directory_option="--no-print-directory"
    else
        rheo_prog_gnu_make=no
    fi
    rm -f conftest.mk
    [AC_MSG_RESULT(${rheo_prog_gnu_make})]
    if test x"${rheo_prog_gnu_make}" != x"yes"; then
       [AC_MSG_ERROR(rheolef requires gnu make)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_ISTREAM_RDBUF}
dnl #
dnl #    @code{RHEO_CHECK_IOS_BP}
dnl #
dnl #    Check to see if "iostream::rdbuf(void*)"
dnl #    function exists, that set the "ios" buffer
dnl #    of a stream. Despite this function is standard,
dnl #    numerous compilers does not furnish it. 
dnl #    a common implementation is to set directly
dnl #    the buffer variable. For instance, the CRAY C++
dnl #    compiler implements this variable as "ios::bp".
dnl #    These two functions set the shell variables
dnl #    "rheo_have_istream_rdbuf" and "rheo_have_ios_bp"
dnl #    and define HAVE_ISTREAM_RDBUF and HAVE_IOS_BP
dnl #    respectively.
dnl -----------------------------------------------------------------------------
dnl istream::rdbuf(ptr)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_ISTREAM_RDBUF],
    [AC_MSG_CHECKING(for iostream::rdbuf(void*))]
    [AH_TEMPLATE([HAVE_ISTREAM_RDBUF], Defines if you have iostream::rdbuf(void*) in <iostream>)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            #include <iostream>
            extern "C" void exit(int);
            struct A : istream {
                A() : istream(0) { rdbuf(0); }
            };  
        %%,
        %%
            A a();
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_ISTREAM_RDBUF)]
        rheo_have_istream_rdbuf=yes, 
        rheo_have_istream_rdbuf=no,
        rheo_have_istream_rdbuf=undef
        false
    )]
    [AC_MSG_RESULT(${rheo_have_istream_rdbuf})]
)
dnl -----------------------------------------------------------------------------
dnl istream::bp when ios::rdbuf() failed
dnl (non-standard: e.g. CRAY C++)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_IOS_BP],
    [AC_MSG_CHECKING(for ios::bp)]
    [AH_TEMPLATE([HAVE_IOS_BP], Defines if you have ios::bp in <iostream>)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            extern "C" void exit(int);
            #include <iostream>
            struct A : istream {
            A() : istream(0) { bp = 0; }
            };  
        %%,
        %%
            A a();
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_IOS_BP)]
        rheo_have_ios_bp=yes, 
        rheo_have_ios_bp=no,
        rheo_have_ios_bp=undef
        false
    )]
    [AC_MSG_RESULT(${rheo_have_ios_bp})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_IOS_SETSTATE}
dnl #
dnl #    Check to see if "ios::setstate(long)"
dnl #    function exists, that set the "ios" state variable
dnl #    of a stream. Despite this function is standard,
dnl #    numerous compilers does not furnish it. 
dnl #    a common implementation is to set directly
dnl #    the buffer variable. For instance, the CRAY C++
dnl #    compiler does not implements it.
dnl #    This function set the shell variables
dnl #    "rheo_have_ios_setstate"
dnl #    and define HAVE_IOS_SETSTATE.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_IOS_SETSTATE],
    [AC_MSG_CHECKING(for ios::setstate(long))]
    [AH_TEMPLATE([HAVE_IOS_SETSTATE], Defines if you have ios::setstate(long) in <iostream>)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            extern "C" void exit(int);
            #include <iostream>
        %%,
        %%
            cout.setstate(ios::goodbit);
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_IOS_SETSTATE)]
        rheo_have_ios_setstate=yes, 
        rheo_have_ios_setstate=no,
        rheo_have_ios_setstate=undef
        false
    )]
    [AC_MSG_RESULT(${rheo_have_ios_setstate})]
)
dnl -----------------------------------------------------------------------------
dnl ios::bitalloc() when ios::pwords() failed
dnl (non-standard: e.g. CRAY C++)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_IOS_BITALLOC],
    [AC_MSG_CHECKING(for ios::bitalloc())]
    [AH_TEMPLATE([HAVE_IOS_BITALLOC], Defines if you have ios::bitalloc() in <iostream>)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            #include <iostream>
            extern "C" void exit(int);
        %%,
        %%
            long b = ios::bitalloc();
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_IOS_BITALLOC)]
        have_ios_bitalloc=yes, 
	have_ios_bitalloc=no,
	have_ios_bitalloc=undef
	false
    )]
    [AC_MSG_RESULT(${have_ios_bitalloc})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_FILEBUF_INT}
dnl #
dnl #    @code{RHEO_CHECK_FILEBUF_FILE}
dnl #
dnl #    @code{RHEO_CHECK_FILEBUF_FILE_MODE}
dnl #
dnl #    Check whether "filebuf::filebuf(int fileno)",
dnl #    "filebuf::filebuf(FILE* fd)" exist,
dnl #    or "filebuf::filebuf(FILE* fd, ios::openmode)" exist,
dnl #    respectively.
dnl #    If so, set the corresponding 
dnl #    shell variable "rheo_have_filebuf_int"
dnl #    (resp. "rheo_have_filebuf_file")
dnl #    to "yes" and defines HAVE_FILEBUF_INT,
dnl #    (resp. HAVE_FILEBUF_FILE).
dnl #    If not, set the variable no "no".
dnl #    Notes that there is no standardisation 
dnl #    of this function in the "c++" library.
dnl #    Nevertheless, this fonctionality is
dnl #    useful to open a pipe stream class,
dnl #    as "pstream(3)".
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_FILEBUF_INT],
    [AC_MSG_CHECKING(for filebuf::filebuf(int))]
    [AH_TEMPLATE([HAVE_FILEBUF_INT], Defines if you have filebuf(int*) in <fstream.h>)]
    [AC_TRY_COMPILE(
	changequote(%%, %%)dnl
	%%
	    #include <stdio.h>
	    #include <fstream.h>
	    extern "C" void exit(int);
	    extern "C" int open(const char*, const char*);
	%%,
	%%
	    int fileno = open("/dev/null", "r");
	    filebuf fbuf(fileno);
	%%,
	changequote([, ])dnl
	[AC_DEFINE(HAVE_FILEBUF_INT)]
	rheo_have_filebuf_int=yes, 
	rheo_have_filebuf_int=no,
	rheo_have_filebuf_int=undef
	false
    )]
    [AC_MSG_RESULT(${rheo_have_filebuf_int})]
)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_FILEBUF_FILE],
    [AC_MSG_CHECKING(for filebuf::filebuf(FILE*))]
    [AH_TEMPLATE([HAVE_FILEBUF_FILE], Defines if you have filebuf(FILE*) in <fstream.h>)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            #include <stdio.h>
            #include <fstream.h>
            extern "C" void exit(int);
        %%,
        %%
            FILE* fd = fopen("/dev/null", "r");
            filebuf fbuf(fd);
        %%,
        changequote([, ])dnl
	[AC_DEFINE(HAVE_FILEBUF_FILE)]
	rheo_have_filebuf_file=yes, 
	rheo_have_filebuf_file=no,
	rheo_have_filebuf_file=undef
	false
    )]
    [AC_MSG_RESULT(${rheo_have_filebuf_file})]
)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_FILEBUF_FILE_MODE],
    [AC_MSG_CHECKING(for filebuf::filebuf(FILE*,ios::openmode))]
    [AH_TEMPLATE([HAVE_FILEBUF_FILE_MODE], Defines if you have filebuf(FILE*,ios::openmode) in <fstream.h>)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            #include <stdio.h>
            #include <fstream.h>
            extern "C" void exit(int);
        %%,
        %%
            FILE* fd = fopen("/dev/null", "r");
            filebuf fbuf(fd,std::ios::in);
        %%,
        changequote([, ])dnl
	[AC_DEFINE(HAVE_FILEBUF_FILE_MODE)]
	rheo_have_filebuf_file_mode=yes, 
	rheo_have_filebuf_file_mode=no,
	rheo_have_filebuf_file_mode=undef
	false
    )]
    [AC_MSG_RESULT(${rheo_have_filebuf_file_mode})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CXX_CLOCK_GETTIME}
dnl #
dnl #    Tries to determine whether clock_gettime is useable.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CXX_CLOCK_GETTIME], [
  AC_MSG_CHECKING([for clock_gettime useability])
  AC_LANG_PUSH(C++)
  AC_COMPILE_IFELSE([AC_LANG_SOURCE([
    #include <time.h>
    int main() {
      struct timespec tv;
      return clock_gettime(CLOCK_REALTIME, &tv);
    }])], [rheo_have_cxx_clock_gettime="yes"], [rheo_have_cxx_clock_gettime="no"])
  AC_LANG_POP(C++)
  AC_MSG_RESULT([$rheo_have_cxx_clock_gettime])
])
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_GETTIMEOFDAY}
dnl #
dnl #    Check whether the "gettimeofday(timeval*, timezone*)"
dnl #    function exists and set the corresponding
dnl #    shell value "rheo_have_gettimeofday" and 
dnl #    define HAVE_GETTIMEOFDAY accordingly.
dnl --------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_GETTIMEOFDAY],
    [AC_MSG_CHECKING(for gettimeofday(timeval*, timezone*))]
    [AH_TEMPLATE([HAVE_GETTIMEOFDAY], Defines if you have gettimeofday(timeval*, timezone*) in <sys/time.h>)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            #include <sys/types.h>
            #include <sys/time.h>
        %%,
        %%
            struct timeval tp;
            struct timezone tzp;
            gettimeofday(&tp,&tzp);
            double x = ((double) tp.tv_sec + .000001 * (double) tp.tv_usec);
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_GETTIMEOFDAY)]
        rheo_have_gettimeofday=yes, 
        rheo_have_gettimeofday=no,
        rheo_have_gettimeofday=undef
        false
    )]
    [AC_MSG_RESULT(${rheo_have_gettimeofday})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_WIERDGETTIMEOFDAY}
dnl #
dnl #    This is for Solaris, where they decided 
dnl #    to change the CALLING SEQUENCE OF gettimeofday!
dnl #    Check whether the "gettimeofday(timeval*)"
dnl #    function exists and set the corresponding
dnl #    shell value "rheo_have_wierdgettimeofday" and 
dnl #    define HAVE_WIERDGETTIMEOFDAY accordingly.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_WIERDGETTIMEOFDAY],
    [AC_MSG_CHECKING(for solaris gettimeofday(timeval*))]
    [AH_TEMPLATE([HAVE_WIERDGETTIMEOFDAY], Defines if you have solaris gettimeofday(timeval*) in <sys/time.h>"")]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            #include <sys/types.h>
            #include <sys/time.h>
        %%,
        %%
       	    struct timeval tp;
            gettimeofday(&tp);
            double x = ((double) tp.tv_sec + .000001 * (double) tp.tv_usec);
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_WIERDGETTIMEOFDAY)]
        rheo_have_wierdgettimeofday=yes, 
        rheo_have_wierdgettimeofday=no,
        rheo_have_wierdgettimeofday=undef
        false
    )]
    [AC_MSG_RESULT(${rheo_have_wierdgettimeofday})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_BSDGETTIMEOFDAY}
dnl #
dnl #    For BSD systems,
dnl #    check whether the "BSDgettimeofday(timeval*, timezone*)"
dnl #    function exists and set the corresponding
dnl #    shell value "rheo_have_bsdgettimeofday" and 
dnl #    define HAVE_BSDGETTIMEOFDAY accordingly.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_BSDGETTIMEOFDAY],
    [AC_MSG_CHECKING(for BSDgettimeofday(timeval*, timezone*))]
    [AH_TEMPLATE([HAVE_BSDGETTIMEOFDAY], Defines if you have BSDgettimeofday(timeval*, timezone*) in <sys/time.h>)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
            #include <sys/types.h>
            #include <sys/time.h>
        %%,
        %%
    	    struct timeval tp;
	    struct timezone tzp;
	    BSDgettimeofday(&tp,&tzp);
	    double x = ((double) tp.tv_sec + .000001 * (double) tp.tv_usec);
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_BSDGETTIMEOFDAY)]
        rheo_have_bsdgettimeofday=yes, 
        rheo_have_bsdgettimeofday=no,
        rheo_have_bsdgettimeofday=undef
        false
    )]
    [AC_MSG_RESULT(${rheo_have_bsdgettimeofday})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_AMICCLK}
dnl #
dnl #    Check whether the clock "amicclk()"
dnl #    function exists and set the corresponding
dnl #    shell value "rheo_have_amicclk" and 
dnl #    define HAVE_AMICCLK accordingly.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_AMICCLK],
    [AC_MSG_CHECKING(for amicclk)]
    [AH_TEMPLATE([HAVE_AMICCLK], Defines if you have amicclk() in <sys/time.h>)]
    [AC_TRY_RUN(
        changequote(%%, %%)dnl
        %%
            #include <sys/types.h>
            #include <sys/time.h>
	    int main() {
	        double x = amicclk()*1.0e-6;
	        return 0;
	    }
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_AMICCLK)]
        rheo_have_amicclk=yes, 
        rheo_have_amicclk=no,
        rheo_have_amicclk=undef
        false
    )]
    [AC_MSG_RESULT(${rheo_have_amicclk})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_TEMPLATE_FULL_SPECIALIZATION}
dnl #
dnl #    Check whether the template specialization syntax "template<>"
dnl #    is supported by the compiler
dnl #    value. So, try to compile, run
dnl #    and check the return value for the following code:
dnl #    @example
dnl #      template<class T> struct toto @{
dnl #          int tutu() const @{ return 1; @}
dnl #      @};
dnl #      template<> struct toto<float> @{
dnl #          int tutu() const @{ return 0; @}
dnl #      @};
dnl #      main() @{
dnl #       toto<float> x;
dnl #       return x.tutu();
dnl #      @}
dnl #    @end example
dnl #    If so, set the corresponding 
dnl #    shell variable "rheo_have_template_full_specialization"
dnl #    to "yes" and defines HAVE_TEMPLATE_FULL_SPECIALIZATION.
dnl #    If not, set the variable no "no".
dnl --------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_TEMPLATE_FULL_SPECIALIZATION],
    [AC_MSG_CHECKING(for template<> full specialization syntax)]
    [AH_TEMPLATE([HAVE_TEMPLATE_FULL_SPECIALIZATION], Defines if you have template<> full specialization syntax")]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
        %%
   	    template<class T> struct toto {
       	        int tutu() const { return 1; }
   	    };
   	    template<> struct toto<float> {
       	        int tutu() const { return 0; }
   	    };
        %%,
        %%
	    toto<float> x;
	    return x.tutu();
        %%,
        changequote([, ])dnl
        rheo_have_template_full_specialization=yes
        [AC_DEFINE(HAVE_TEMPLATE_FULL_SPECIALIZATION)],
        rheo_have_template_full_specialization=no
    )]
    [AC_MSG_RESULT(${rheo_have_template_full_specialization})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_ISNAN_DOUBLE}
dnl #
dnl #    @code{RHEO_CHECK_ISINF_DOUBLE}
dnl #
dnl #    @code{RHEO_CHECK_FINITE_DOUBLE}
dnl #
dnl #    @code{RHEO_CHECK_INFINITY}
dnl #
dnl #    @code{RHEO_CHECK_ABS_DOUBLE}
dnl #
dnl #    @code{RHEO_CHECK_SQR_DOUBLE}
dnl #
dnl #    Check whether the functions 
dnl #    @example
dnl #            bool isnan(double);
dnl #            bool isinf(double);
dnl #            bool finite(double);
dnl #            double infinity();
dnl #            double abs();
dnl #            double sqr();
dnl #    @end example
dnl #    are supported by the compiler, respectively.
dnl #    If so, set the corresponding 
dnl #    shell variable "rheo_have_xxx"
dnl #    to "yes" and defines HAVE_XXX.
dnl #    If not, set the variable no "no".
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_ISNAN_DOUBLE],
    [AC_MSG_CHECKING(for isnan(double))]
    [AC_TRY_RUN(
	[
	    #include <math.h>
	    extern "C" void exit(int);
	    int main() { exit (!isnan(log(-1))); }
	],
	[AC_DEFINE(HAVE_ISNAN_DOUBLE)]
	rheo_have_isnan_double=yes, 
	rheo_have_isnan_double=no,
	rheo_have_isnan_double=undef
	false
    )]
    [AC_MSG_RESULT(${rheo_have_isnan_double})]
)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_ISINF_DOUBLE],
    [AC_MSG_CHECKING(for isinf(double))]
    [AH_TEMPLATE([HAVE_ISINF_DOUBLE], Defines if you have isinf(double) in <math.h>)]
    [AC_TRY_RUN(
	[
	    #include <math.h>
	    extern "C" void exit(int);
	    int main() { exit (!isinf(1/log(1))); }
	],
	[AC_DEFINE(HAVE_ISINF_DOUBLE)]
	rheo_have_isinf_double=yes, 
	rheo_have_isinf_double=no,
	rheo_have_isinf_double=undef
	false
    )]
    [AC_MSG_RESULT(${rheo_have_isinf_double})]
)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_FINITE_DOUBLE],
    [AC_MSG_CHECKING(for finite(double))]
    [AH_TEMPLATE([HAVE_FINITE_DOUBLE], Defines if you have finite(double) in <fstream.h>)]
    [AC_TRY_RUN(
	[
	    #include <math.h>
	    extern "C" void exit(int);
	    int main() { exit (finite(0) && !finite(1/log(1))); }
	],
	[AC_DEFINE(HAVE_FINITE_DOUBLE)]
	rheo_have_finite_double=yes, 
	rheo_have_finite_double=no,
	rheo_have_finite_double=undef
	false
    )]
    [AC_MSG_RESULT(${rheo_have_finite_double})]
)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_INFINITY],
    [AC_MSG_CHECKING(for infinity())]
    [AC_TRY_RUN(
	[
	    #include <math.h>
	    extern "C" void exit(int);
	    int main() { double x = infinity(); exit(0); }
	],
	[AC_DEFINE(HAVE_INFINITY)]
	rheo_have_infinity=yes, 
	rheo_have_infinity=no,
	rheo_have_infinity=undef
	false
    )]
    [AC_MSG_RESULT(${rheo_have_infinity})]
)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_ABS_DOUBLE],
    [AC_MSG_CHECKING(for abs(double))]
    [AH_TEMPLATE([HAVE_ABS_DOUBLE], Defines if you have abs(double))]
    [AC_TRY_RUN(
	[
	    #include <math.h>
	    extern "C" void exit(int);
	    int main() { exit (abs(double(1.2)) != 1.2); }
	],
	[AC_DEFINE(HAVE_ABS_DOUBLE)]
	rheo_have_abs_double=yes, 
	rheo_have_abs_double=no,
	rheo_have_abs_double=undef
	false
     )]
     [AC_MSG_RESULT(${rheo_have_abs_double})]
)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_SQR_DOUBLE],
    [AC_MSG_CHECKING(for sqr(double))]
    [AH_TEMPLATE([HAVE_SQR_DOUBLE], Defines if you have sqr(double) in <math.h>)]
    [AC_TRY_RUN(
	[
	    #include <math.h>
	    extern "C" void exit(int);
	    int main() { exit (sqr(double(1.2)) != 1.2*1.2); }
	],
	[AC_DEFINE(HAVE_SQR_DOUBLE)]
	rheo_have_sqr_double=yes, 
	rheo_have_sqr_double=no,
	rheo_have_sqr_double=undef
	false
     )]
     [AC_MSG_RESULT(${rheo_have_sqr_double})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_FLEX}
dnl #
dnl #    Check to see if the "flex" command and the
dnl #    corresponding header "FlexLexer.h" are available.
dnl #    If so, set the shell variable "rheo_have_flex"
dnl #    to "yes" and substitues FLEX to "flex"
dnl #    and FLEXLEXER_H to the full path for FlexLexer.h
dnl #    If not, set the shell variable no "no".
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_FLEX],
    rheo_have_flex=no
    [AC_SUBST(FLEX)]
    [AC_SUBST(FLEXLEXER_H)]
    FLEX=""
    FLEXLEXER_H=""
    [AC_PATH_PROG(PATH_FLEX, flex)]
    if test x"${PATH_FLEX}" != x""; then
        FLEX=`expr ${PATH_FLEX} : '.*/\(.*\)' \| ${PATH_FLEX}`
        rheo_bindir_flex=`expr ${PATH_FLEX} : '\(.*\)/[/]*flex' `
        rheo_dir_flex=`expr ${rheo_bindir_flex} : '\(.*\)/.*' `
        [AC_MSG_CHECKING(for FlexLexer.h)]
        listdir="${rheo_dir_flex} /usr/local/opt/flex"
        for dir in $listdir; do
    	  FLEXLEXER_H="${dir}/include/FlexLexer.h"
	  if test -f "$FLEXLEXER_H"; then
    	    [AC_MSG_RESULT(${FLEXLEXER_H})]
	    rheo_have_flex=yes
            break
          fi
        done
        if test "${rheo_have_flex}" != yes; then
    	    FLEXLEXER_H="";
    	    [AC_MSG_RESULT("no")]
	fi
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_PROG_CC_INTEL}
dnl #
dnl #    Check whether we are using INTEL C++ compiler.
dnl #    If so, set the shell variable "ac_cv_prog_icc"
dnl #    to "yes" and define HAVE_INTEL_CXX.
dnl #    If not, set the shell variable no "no".
dnl #    The shell variable is also exported for sub-shells,
dnl #    such as ltconfig from libtool.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_PROG_CC_INTEL],
    [AH_TEMPLATE([HAVE_INTEL_CXX], Defines if you have INTEL C++ compiler)]
    [AC_MSG_CHECKING(whether we are using INTEL C++ compiler)] 
    ac_cv_prog_icc=no
    export ac_cv_prog_icc
    [cat > conftest.cc <<EOF_ICC
        #ifdef __ICC
            yes;
        #endif
EOF_ICC
    if AC_TRY_COMMAND(${CXX-icpc} -E conftest.cc) | egrep yes >/dev/null 2>&1; then
      ac_cv_prog_icc=yes
    else
      ac_cv_prog_icc=no
    fi
    rm -f conftest.cc
    ]
    if test x"${ac_cv_prog_icc}" = x"yes"; then
      [AC_DEFINE(HAVE_INTEL_CXX)]
    fi
    [AC_MSG_RESULT(${ac_cv_prog_icc})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_PROG_CC_CLANG}
dnl #
dnl #    Check whether we are using CLANG C++ compiler.
dnl #    If so, set the shell variable "ac_cv_prog_clang"
dnl #    to "yes" and define HAVE_INTEL_CLANG.
dnl #    If not, set the shell variable no "no".
dnl #    The shell variable is also exported for sub-shells,
dnl #    such as ltconfig from libtool.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_PROG_CC_CLANG],
    [AH_TEMPLATE([HAVE_CLANG_CXX], Defines if you have CLANG C++ compiler)]
    [AC_MSG_CHECKING(whether we are using CLANG C++ compiler)] 
    ac_cv_prog_clang=no
    export ac_cv_prog_clang
    [cat > conftest.cc <<EOF_CLANG
        #ifdef __clang__
            yes;
        #endif
EOF_CLANG
    if AC_TRY_COMMAND(${CXX-clang++} -E conftest.cc) | egrep yes >/dev/null 2>&1; then
      ac_cv_prog_clang=yes
    else
      ac_cv_prog_clang=no
    fi
    rm -f conftest.cc
    ]
    if test x"${ac_cv_prog_clang}" = x"yes"; then
      [AC_DEFINE(HAVE_CLANG_CXX)]
    fi
    [AC_MSG_RESULT(${ac_cv_prog_clang})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CLANG_LINK_LIBSTDCXX}
dnl #
dnl #    Check whether CLANG C++ compiler links correctly with std c++ lib.
dnl #    If not, exit with error.
dnl #    This is still a bug in debian/testing (stretch) in june 2016.
dnl #        https://llvm.org/bugs/show_bug.cgi?id=24844
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CLANG_LINK_LIBSTDCXX],
    [AC_MSG_CHECKING(whether CLANG C++ compiler links correctly with the std C++ library)] 
    [AC_TRY_RUN(
        changequote(%%, %%)dnl
        %%
        #include <iostream>
        #include <string>
        int main() {
          std::string msg = "hello world";
          std::ios_base::failure f(msg);
          std::cout << msg << std::endl;
          return 0;
        }
        %%,
        changequote([, ])dnl
        rheo_have_clang_link=yes, 
        rheo_have_clang_link=no,
        rheo_have_clang_link=undef
        false
    )]
    if test x"${rheo_have_clang_link}" != x"yes"; then
      [AC_MSG_RESULT(${rheo_have_clang_link})]
      [AC_MSG_ERROR(CLANG compiler problem: $CXX is unable to link with the std C++ library)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl recognize C++
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_RECOGNIZE_CXX}
dnl #
dnl #    Check whether we are able to recognize the C++ compiler.
dnl #    Tested compilers:
dnl #    @example
dnl #         The GNU   C++ compiler that defines: __GNUC__  (egcs-1.1.1)
dnl #         The INTEL C++ compiler that defines: __ICC     (ICPC-12)
dnl #    @end example
dnl #
dnl #    If so, substitue RECOGNIZED_CXX to a specific
dnl #    compiler's rule file, e.g, "${top_srcdir}/config/gnu_cxx.mk",
dnl #    for a subsequent Makefile include.
dnl #    If not, substitue to "/dev/null".
dnl #    Substitutes also EXTRA_LDFLAGS.
dnl #    Raw cc is the C compiler associated to the C++ one. By this way
dnl #    C and C++ files are handled with a .c suffix. Special C files that require
dnl #    the cc compiler, such as "alloca.c" use some specific makefile rule.
dnl #
dnl # usage example:
dnl # @example
dnl #    AC_PROG_CC(gcc cc icc cl)
dnl #    AC_PROG_CXX(c++ g++ cxx icpc KCC CC CC cc++ xlC aCC)
dnl #    RHEO_RECOGNIZE_CXX
dnl # @end example
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_RECOGNIZE_CXX],
    [AC_SUBST(RECOGNIZED_CXX)]
    [AC_SUBST(EXTRA_LDFLAGS)]
    [AC_SUBST(OPTIMIZE_CFLAGS)]
    [RECOGNIZED_CXX="/dev/null"]
    rheo_cxx=other
    dnl put intel before gnu, since it is gnu-compatible...
    if test x"${rheo_cxx}" = x"other"; then
      [RHEO_PROG_CC_INTEL]
      if test x"${ac_cv_prog_icc}" = x"yes"; then
        # too much warnings fromsystem headers...
        [CXXFLAGS="${CXXFLAGS} -w"]
        # gnu-compatible:
        [RECOGNIZED_CXX="${top_srcdir}/config/gnu_cxx.mk"]
	rheo_cxx=intel
      fi
    fi
    if test x"${rheo_cxx}" = x"other"; then
      [RHEO_PROG_CC_CLANG]
      if test x"${ac_cv_prog_clang}" = x"yes"; then
        [RHEO_CLANG_LINK_LIBSTDCXX]
        # gnu-compatible:
        [RECOGNIZED_CXX="${top_srcdir}/config/gnu_cxx.mk"]
	rheo_cxx=clang
      fi
    fi
    if test x"${rheo_cxx}" = x"other" -a x"${GXX}" = x"yes"; then
      [RECOGNIZED_CXX="${top_srcdir}/config/gnu_cxx.mk"]
      rheo_cxx=gnu
    fi
    if test ${rheo_cxx} = other; then
      [AC_MSG_RESULT(** WARNING: unknown C++ compiler)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CXX2011}
dnl #
dnl #    Check for the "-std=c++11" support for g++ or clang++.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CXX2011],
  PREV_CXXFLAGS="$CXXFLAGS"
  CXXFLAGS="${CXXFLAGS} -std=c++11"
  [AH_TEMPLATE([HAVE_CXX_STD_2011], Defines if you have a 2011 compilant c++ compiler)]
  [AC_MSG_CHECKING(for 2011 C++ option -std=c++11)]
  [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
	        int a = 0;
	    %%,
	    %%
		return a;
	    %%,
	    changequote([, ])dnl
	    rheo_cxx2011=yes,
	    rheo_cxx2011=no
        )]
  [AC_MSG_RESULT(${rheo_cxx2011})]
  if test x"${rheo_cxx2011}" = x"yes"; then
      # TODO: are some versions of the intel c++ compilers 2011 compilant ?
      [AC_DEFINE(HAVE_CXX_STD_2011)]
  fi
  CXXFLAGS="${PREV_CXXFLAGS}"
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CXX2014}
dnl #
dnl #    Check for the "-std=c++14" support for g++.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CXX2014],
  PREV_CXXFLAGS="$CXXFLAGS"
  CXXFLAGS="${CXXFLAGS} -std=c++14"
  [AH_TEMPLATE([HAVE_CXX_STD_2014], Defines if you have a 2014 compilant c++ compiler)]
  [AC_MSG_CHECKING(for 2014 C++ option -std=c++14)]
  [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
	        int a = 0;
	    %%,
	    %%
		return a;
	    %%,
	    changequote([, ])dnl
	    rheo_cxx2014=yes,
	    rheo_cxx2014=no
        )]
  [AC_MSG_RESULT(${rheo_cxx2014})]
  if test x"${rheo_cxx2014}" = x"yes"; then
      [AC_DEFINE(HAVE_CXX_STD_2014)]
  fi
  CXXFLAGS="${PREV_CXXFLAGS}"
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CXX2017}
dnl #
dnl #    Check for the "-std=c++17" support for g++.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CXX2017],
  PREV_CXXFLAGS="$CXXFLAGS"
  CXXFLAGS="${CXXFLAGS} -std=c++17"
  [AH_TEMPLATE([HAVE_CXX_STD_2017], Defines if you have a 2017 compilant c++ compiler)]
  [AC_MSG_CHECKING(for 2017 C++ option -std=c++17)]
  [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
	        int a = 0;
	    %%,
	    %%
		return a;
	    %%,
	    changequote([, ])dnl
	    rheo_cxx2017=yes,
	    rheo_cxx2017=no
        )]
  [AC_MSG_RESULT(${rheo_cxx2017})]
  if test x"${rheo_cxx2017}" = x"yes"; then
      [AC_DEFINE(HAVE_CXX_STD_2017)]
  fi
  CXXFLAGS="${PREV_CXXFLAGS}"
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_OPTIMIZE_CXX}
dnl #
dnl #    Set some optimization flags associated to the recognized C++ compiler
dnl #    and platform.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_OPTIMIZE_CXX],
    [case x"${rheo_cxx}" in
	x"gnu") 
        	[CXXFLAGS="${CXXFLAGS} -O3"]
		[OPTIMIZE_CFLAGS="-O9"]
		;;
	x"kai")
                [CXXFLAGS="${CXXFLAGS} +K1 -O"]
	    	[OPTIMIZE_CFLAGS="+K3 -O3"]
		;;
	x"cray")
                [CXXFLAGS="${CXXFLAGS} -O"]
	        [OPTIMIZE_CFLAGS="-h inline1,vector3,scalar3,task3 -h ivdep -h restrict=a"]
		;;
	*)
	        [OPTIMIZE_CFLAGS=""]
		;;
     esac]
)
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CXX_FLAGS_IEEE],

    if test x"${ac_cv_cxx_compiler_gnu=yes}" = x"yes"; then
        [AC_MSG_CHECKING(for c++ ieee flag)]
        [AC_SUBST(CXXFLAGS_IEEE)]
        [CXXFLAGS_IEEE=""]
        mach="`uname -m`"
        [case $mach in
          i*86|*86_64)
             [CXXFLAGS_IEEE="-ffloat-store -mieee-fp"]
             ;;
        esac]
        [AC_MSG_RESULT(${CXXFLAGS_IEEE})]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #  
dnl #    @code{RHEO_CHECK_MPI}
dnl #
dnl #    Check for the "mpirun" command, the
dnl #    corresponding header "mpi.h" and library "-lmpi" are available.
dnl #    If so, set the shell variable "rheo_have_mpi"
dnl #    to "yes", and substitues MPIRUN to "mpirun" and
dnl #    RUN to "mpirun -np 2",
dnl #    INCLUDES_MPI and LDADD_MPI.
dnl #    If not, set the shell variable no "no".
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_MPI],
    [AC_SUBST(MPIRUN)]
    [AC_SUBST(RUN)]
    [AC_SUBST(INCLUDES_MPI)]
    [AC_SUBST(LDADD_MPI)]
    [AC_SUBST(INCLUDES_BOOST_MPI)]
    [AC_SUBST(LDADD_BOOST_MPI)]
    rheo_have_mpi=yes
    [AC_PATH_PROG(PATH_MPIRUN, mpirun)]
    if test x"${PATH_MPIRUN}" = x""; then
        rheo_have_mpi=no
    else
        MPIRUN="mpirun"
    fi
    if test x"${rheo_have_mpi}" = x"yes"; then
        [AC_MSG_CHECKING(for mpi style)]
        rheo_recognized_mpi=no
        if    test `$MPIRUN --allow-run-as-root --help 2>&1 | grep "Open.*MPI" | wc -l` -ge 1 \
           || test `$MPIRUN --help --allow-run-as-root 2>&1 | grep "Open.*MPI" | wc -l` -ge 1; then
          rheo_recognized_mpi=openmpi
        elif test `$MPIRUN --help 2>&1 | grep 'MPICH1 compat' | wc -l` -ge 1; then
          rheo_recognized_mpi=mpich2
        elif test `$MPIRUN -h 2>&1 | grep 'mpich/bin' | wc -l` -ge 1; then
          rheo_recognized_mpi=mpich1
	elif test `$MPIRUN -h 2>&1 | grep 'LAM/MPI' | wc -l` -ge 1; then
  	  rheo_recognized_mpi=lam
        fi
        [AC_MSG_RESULT(${rheo_recognized_mpi})]
	[case ${rheo_recognized_mpi} in
          mpich1)
	    INCLUDES_MPI="-I/usr/lib/mpich/include"
	    LDADD_MPI="-L/usr/lib/mpich/lib -lpmpich++ -lmpich -lpthread -lrt"
	    ;;
          mpich2)
	    INCLUDES_MPI="-I/usr/include/mpich2"
	    LDADD_MPI="-lmpichcxx -lmpich -lpthread -lrt"
	    ;;
          openmpi)
            MPIRUN="mpirun --hostfile \${top_builddir}/config/hostfile.txt"
	    if test x"${rheo_incdir_mpi}" = x""; then
	      INCLUDES_MPI="`mpic++ --showme:compile 2>/dev/null`"
	    else
	      INCLUDES_MPI="-I${rheo_incdir_mpi}"
	    fi
	    if test x"${rheo_libs_mpi}" = x"" -a x"${rheo_libdir_mpi}" = x""; then
	      LDADD_MPI="`mpic++ --showme:link 2>/dev/null`"
	    elif test x"${rheo_libdir_mpi}" != x""; then
	      LDADD_MPI="-L${rheo_libdir_mpi} ${rheo_libs_openmpi}"
	    else
	      LDADD_MPI="${rheo_libs_openmpi}"
            fi
	    ;;
          lam)
	    INCLUDES_MPI="-I/usr/include/lam"
	    LDADD_MPI="-llam++ -llam -llamio"
	    ;;
          *)
	    INCLUDES_MPI=""
	    LDADD_MPI="-lmpi"
	    ;;
	esac]
        [AC_MSG_CHECKING(for mpi includes)]
        [AC_MSG_RESULT(${INCLUDES_MPI})]
        [AC_MSG_CHECKING(for libmpi)]
        [AC_MSG_RESULT(${LDADD_MPI})]
        PREV_LIBS="$LIBS"
        LIBS="${LIBS} ${LDADD_MPI}"
        PREV_CXXFLAGS="$CXXFLAGS"
        CXXFLAGS="${CXXFLAGS} ${INCLUDES_MPI} ${INCLUDES_BOOST}"
        PREV_CPPFLAGS="$CPPFLAGS"
        CPPFLAGS="${CPPFLAGS} -w ${INCLUDES_MPI} ${INCLUDES_BOOST}"
        [AC_CHECK_HEADERS(mpi.h, [true], [rheo_have_mpi=no])]
        [AC_CHECK_HEADERS(boost/mpi.hpp, [true], [rheo_have_mpi=no])]
        if test x"${rheo_have_mpi}" != x"no"; then
	    INCLUDE_BOOST_MPI="${INCLUDES_BOOST}"
	    LDADD_BOOST_MPI="${LDADD_BOOST} -lboost_mpi -lboost_serialization -lboost_iostreams"
            [AC_MSG_CHECKING(for boost::mpi)]
            [AC_MSG_RESULT(${LDADD_BOOST_MPI})]
        fi
        LIBS="$PREV_LIBS"
        CXXFLAGS="$PREV_CXXFLAGS"
    fi
    if test x"${rheo_have_mpi}" = x"yes"; then
	# try to run is also important: stretch-2020-03-21 was buggy while runnning only
        [AC_MSG_CHECKING(if programs compile and run with mpi)]
        rm -f core; touch core; chmod a-w core
        PREV_LIBS="$LIBS"
        LIBS="${LIBS} ${LDADD_MPI} ${LDADD_BOOST_MPI}"
        PREV_CXXFLAGS="$CXXFLAGS"
        CXXFLAGS="${CXXFLAGS} ${INCLUDES_MPI} ${INCLUDES_BOOST_MPI}"
        # old openmpi failed within pbuilder ; fixed as:
        # export OMPI_MCA_orte_rsh_agent=/bin/false     ; # for old    openmpi
        # very old openmpi failed within pbuilder ; fixed as:
        # export OMPI_MCA_plm_rsh_agent= ;# =/bin/false      ; # for oldold openmpi
        # with openmpi-3.1.3 on buster, fails in interactive mode when using env variable => no more vars are used
        # [AC_MSG_NOTICE($CXXFLAGS)]
        # [AC_MSG_NOTICE($LDADD_MPI)]
        [AC_RUN_IFELSE([AC_LANG_PROGRAM(
                [#include <mpi.h>],
                [
                
                  MPI_Init(NULL, NULL);
                  MPI_Finalize();
                  return 0;

                ])],
                [rheo_have_mpi=yes],
                [rheo_have_mpi=no],
                [rheo_have_mpi=no false])]
    
        [AC_MSG_RESULT($rheo_have_mpi)]
        rm -f core
        LIBS="$PREV_LIBS"
        CXXFLAGS="$PREV_CXXFLAGS"
    fi
    if test x"${rheo_have_mpi}" = x"yes"; then
        [AC_MSG_CHECKING(if programs compile and link with both mpi and boost::mpi)]
        rm -f core; touch core; chmod a-w core
        PREV_LIBS="$LIBS"
        LIBS="${LIBS} ${LDADD_MPI} ${LDADD_BOOST_MPI}"
        PREV_CXXFLAGS="$CXXFLAGS"
        CXXFLAGS="${CXXFLAGS} ${INCLUDES_MPI} ${INCLUDES_BOOST_MPI}"
        [AC_TRY_LINK(
	    changequote(%%, %%)dnl
	    %%
                #include <mpi.h>
                #include <boost/mpi.hpp>
	    %%,
	    %%
     		MPI_Init(0,0);
     		MPI_Finalize();
		boost::mpi::environment e;
	    %%,
	    changequote([, ])dnl
	    rheo_have_mpi=yes,
	    rheo_have_mpi=no
        )]
        [AC_MSG_RESULT($rheo_have_mpi)]
        rm -f core
        LIBS="$PREV_LIBS"
        CXXFLAGS="$PREV_CXXFLAGS"
    fi
    if test x"${rheo_have_mpi}" != x"yes"; then
	MPIRUN=""
	INCLUDES_MPI=""
	LDADD_MPI=""
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_METIS}
dnl #
dnl #    Check for the "metis" library.
dnl #    Defines HAVE_METIS and substitues 
dnl #    INCLUDES_METIS and LDADD_METIS.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_METIS],
    [AH_TEMPLATE([HAVE_METIS], Defines if you have the METIS library)]
    [AC_SUBST(INCLUDES_METIS)]
    [AC_SUBST(INCLUDES_METIS_INTERN)]
    [AC_SUBST(LDADD_METIS)]
    rheo_have_metis=yes
    INCLUDES_METIS=""
    if test x"${rheo_incdir_metis}" != x"yes" -a x"${rheo_incdir_metis}" != x""; then
      INCLUDES_METIS="-I${rheo_incdir_metis}"
    fi
    LDADD_METIS="-lmetis"
    if test x"${rheo_libdir_metis}" != x"yes" -a x"${rheo_libdir_metis}" != x""; then
      LDADD_METIS="-L${rheo_libdir_metis} ${LDADD_METIS}"
    fi
    PREV_CPPFLAGS=$CPPFLAGS
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_METIS}"
    [AC_CHECK_HEADER(metis.h, true, rheo_have_metis=no)]
    CPPFLAGS="${PREV_CPPFLAGS}"
    if test x"${rheo_have_metis}" = x"yes"; then
      [AC_MSG_CHECKING(for metis library)]
      [AC_DEFINE(HAVE_METIS)]
      [AC_MSG_RESULT(${LDADD_METIS})]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_PARMETIS}
dnl #
dnl #    Check for the "parmetis" libraries.
dnl #    Defines HAVE_PARMETIS and substitues 
dnl #    INCLUDES_PARMETIS and LDADD_PARMETIS.
dnl #	 Requires the MPI library.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_PARMETIS],
    [AH_TEMPLATE([HAVE_PARMETIS], Defines if you have parmetis mesh partitioner libraries)]
    [AC_SUBST(INCLUDES_PARMETIS)]
    [AC_SUBST(LDADD_PARMETIS)]
    rheo_have_parmetis=yes
    INCLUDES_PARMETIS=""
    if test x"${rheo_incdir_parmetis}" != x"yes" -a x"${rheo_incdir_parmetis}" != x""; then
      INCLUDES_PARMETIS="-I${rheo_incdir_parmetis}"
    fi
    INCLUDES_PARMETIS="${INCLUDES_PARMETIS} ${INCLUDES_METIS}"
    PREV_CPPFLAGS=$CPPFLAGS
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_PARMETIS}"
    [AC_CHECK_HEADER(parmetis.h, true, rheo_have_parmetis=no)]
    CPPFLAGS="${PREV_CPPFLAGS}"
    if test x"${rheo_libdir_parmetis}" = x"yes" -o x"${rheo_libdir_parmetis}" = x""; then
      libpath="/usr/lib /lib"
      rheo_libdir_parmetis=""
      for d in ${libpath}; do
        if test -f $d/libparmetis.so || test -f $d/libparmetis.a; then
          rheo_libdir_parmetis=$d
	  break
        fi
      done
    fi
    if test x"${rheo_libdir_parmetis}" = x""; then
        rheo_have_parmetis="no"
    fi
    [AC_MSG_CHECKING(for parmetis library)]
    if test x"${rheo_have_parmetis}" = x"yes"; then
	[AC_DEFINE(HAVE_PARMETIS)]
        LDADD_PARMETIS="-lparmetis"
        if test "${rheo_libdir_parmetis}" != "/lib" -a "${rheo_libdir_parmetis}" != "/usr/lib"; then
          LDADD_PARMETIS="-L${rheo_libdir_parmetis} ${LDADD_PARMETIS}"
        fi
        LDADD_PARMETIS="${LDADD_PARMETIS} ${LDADD_METIS}"
	rheo_have_parmetis=yes
        [AC_MSG_RESULT(${LDADD_PARMETIS})]
    else
        INCLUDES_PARMETIS=""
        LDADD_PARMETIS=""
	rheo_have_parmetis=no
        [AC_MSG_RESULT(no)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_SCOTCH}
dnl #
dnl #    Check for the "scotch" distributed mesh partitioner libraries.
dnl #    Defines HAVE_SCOTCH and substitues 
dnl #    INCLUDES_SCOTCH and LDADD_SCOTCH.
dnl #	 Requires the MPI library.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_SCOTCH],
    [AH_TEMPLATE([HAVE_SCOTCH], Defines if you have the scotch distributed mesh partitioner libraries)]
    [AC_SUBST(INCLUDES_SCOTCH)]
    [AC_SUBST(LDADD_SCOTCH)]
    [AC_MSG_CHECKING(for scotch library)]
    rheo_have_scotch=yes
    if test x"${rheo_libdir_scotch}" != x"yes" -a x"${rheo_libdir_scotch}" != x""; then
       libpath="${rheo_libdir_scotch} ${rheo_libdir_scotch}/lib"
    else
       libpath="/usr/lib /lib"
    fi
    #echo "PT : libpath=$libpath"
    rheo_libdir_scotch=""
    for d in ${libpath}; do
        founded=false
        # sequential libs:
        #echo "PT : d=$d"
        for suffix in so a; do
          #echo "PT : find $d -name libscotch.$suffix -> `find $d -name libscotch.$suffix | wc -l`"
          #echo "PT : find $d -name libscotcherrexit.$suffix -> `find $d -name libscotcherrexit.$suffix | wc -l`"
          if test `find $d -name libscotch.$suffix | wc -l` -ge 1 -a `find $d -name libscotcherrexit.$suffix | wc -l` -ge 1; then
	    #echo "founded=true"
	    founded=true
            break
          fi
        done
        # distributed libs:
	# note: use only scotch, no more ptscotch in distributed mode (more efficient partitions)
        ## if $founded && test x"${rheo_have_mpi}" = x"yes"; then
        ##   if test -f $d/libptscotch.so -a -f $d/libptscotcherrexit.so || test -f $d/libptscotch.a -a -f $d/libptscotcherrexit.a ; then
	##     true
        ##   fi
        ## fi
        if $founded; then
          rheo_libdir_scotch=$d
          break
	fi
    done
    if test x"${rheo_libdir_scotch}" = x""; then
      rheo_have_scotch="no"
    fi
    #echo "PT : rheo_have_scotch=$rheo_have_scotch"
    #echo "PT : rheo_libdir_scotch=$rheo_libdir_scotch"
    #echo "PT : rheo_libs_scotch=$rheo_libs_scotch"
    if test x"${rheo_have_scotch}" = x"yes" -a x"${rheo_libs_scotch}" = x""; then
      variant_list="ptscotchparmetisv3 ptscotchparmetis"
      #echo "PT : variant_list=$variant_list"
      founded=false
      for ptscotchparmetis in ${variant_list}; do
        #echo "PT : ptscotchparmetis=$ptscotchparmetis"
        #echo "PT : find ${rheo_libdir_scotch} -name lib${ptscotchparmetis}.${suffix} -> `find ${rheo_libdir_scotch} -name lib${ptscotchparmetis}.${suffix} | wc -l`"
        if test `find ${rheo_libdir_scotch} -name lib${ptscotchparmetis}.${suffix} | wc -l` -ge 1; then
          rheo_libs_scotch="-l${ptscotchparmetis} -lptscotch -lptscotcherrexit -lz" 
          #echo "PT FOUNDED: rheo_libs_scotch=$rheo_libs_scotch"
          founded=true
          break
        fi
      done
      if test x"${rheo_libdir_scotch}" = x""; then
        rheo_have_scotch="no"
      fi
    fi
    #echo "PT : rheo_have_scotch=$rheo_have_scotch"
    #echo "PT : rheo_libs_scotch=$rheo_libs_scotch"
    if test x"${rheo_have_scotch}" = x"yes"; then
	[AC_DEFINE(HAVE_SCOTCH)]
        INCLUDES_SCOTCH=""
        LDADD_SCOTCH="${rheo_libs_scotch}"
        if test x"${rheo_have_mpi}" != x"yes"; then
          # LDADD_SCOTCH="-lscotch -lscotcherrexit"
          LDADD_SCOTCH="-lscotch -lscotcherrexit"
        fi
        if test x"${rheo_libdir_scotch}" != x"" -a x"${rheo_libdir_scotch}" != x"/usr/lib"; then
          LDADD_SCOTCH="-L${rheo_libdir_scotch} ${LDADD_SCOTCH}"
        fi
	rheo_have_scotch=yes
        [AC_MSG_RESULT(${LDADD_SCOTCH})]
    else
	rheo_have_scotch=no
        [AC_MSG_RESULT(no)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_BOOST}
dnl #
dnl #    Check for the "boost" library.
dnl #    Defines HAVE_BOOST and substitues LDADD_BOOST and INCLUDES_BOOST.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_BOOST],
    [AH_TEMPLATE([HAVE_BOOST], Defines if you have the basic linea algebra subroutines libraries)]
    [AC_SUBST(INCLUDES_BOOST)]
    [AC_SUBST(LDADD_BOOST)]
    [AC_MSG_CHECKING(for boost library)]
    rheo_have_boost=yes
    INCLUDES_BOOST=""
    if test x"${rheo_incdir_boost}" != x"yes" -a x"${rheo_incdir_boost}" != x""; then
      INCLUDES_BOOST="-I${rheo_incdir_boost}"
    fi
    LDADD_BOOST=""
    if test x"${rheo_libdir_boost}" != x"yes" -a x"${rheo_libdir_boost}" != x""; then
      LDADD_BOOST="-L${rheo_libdir_boost}"
    fi
    [AC_DEFINE(HAVE_BOOST)]
    [AC_MSG_RESULT(${LDADD_BOOST})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_BLAS}
dnl #
dnl #    Check for the "blas" basic linear algebra subroutines library.
dnl #    Defines HAVE_BLAS and substitues LDADD_BLAS and INCLUDES_BLAS.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_BLAS],
    [AH_TEMPLATE([HAVE_BLAS], Defines if you have the basic linea algebra subroutines libraries)]
    [AC_SUBST(INCLUDES_BLAS)]
    [AC_SUBST(LDADD_BLAS)]
    [AC_MSG_CHECKING(for blas library)]
    rheo_have_blas=yes
    INCLUDES_BLAS=""
    if test x"${rheo_libs_blas}" != x"yes" -a x"${rheo_libs_blas}" != x""; then
      LDADD_BLAS="${rheo_libs_blas}"
    fi
    if test x"${rheo_libdir_blas}" != x"yes" -a x"${rheo_libdir_blas}" != x""; then
      LDADD_BLAS="-L${rheo_libdir_blas} ${LDADD_BLAS}"
    fi
    [AC_DEFINE(HAVE_BLAS)]
    [AC_MSG_RESULT(${LDADD_BLAS})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_SCALAPACK}
dnl #
dnl #    Check for the "scalapack" basic linear algebra subroutines library.
dnl #    Defines HAVE_SCALAPACK and substitues LDADD_SCALAPACK and INCLUDES_SCALAPACK.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_SCALAPACK],
    [AH_TEMPLATE([HAVE_SCALAPACK], Defines if you have the scalable linea algebra subroutines libraries)]
    [AC_SUBST(INCLUDES_SCALAPACK)]
    [AC_SUBST(LDADD_SCALAPACK)]
    [AC_MSG_CHECKING(for scalapack library)]
    rheo_have_scalapack=yes
    INCLUDES_SCALAPACK=""
    LDADD_SCALAPACK="${rheo_libs_scalapack}"
    if test x"${rheo_libdir_scalapack}" != x"yes" -a x"${rheo_libdir_scalapack}" != x""; then
      LDADD_SCALAPACK="-L${rheo_libdir_scalapack} ${LDADD_SCALAPACK}"
    fi
    [AC_DEFINE(HAVE_SCALAPACK)]
    [AC_MSG_RESULT(${LDADD_SCALAPACK})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_TRILINOS}
dnl #
dnl #    Check for the "trilinos" distributed preconditioner libraries.
dnl #    Defines HAVE_TRILINOS and substitues 
dnl #    INCLUDES_TRILINOS and LDADD_TRILINOS.
dnl #	 Requires the MPI and SCOTCH libraries.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_TRILINOS],
    [AH_TEMPLATE([HAVE_TRILINOS], Defines if you have the trilinos distributed direct solver libraries)]
    [AC_SUBST(INCLUDES_TRILINOS)]
    [AC_SUBST(LDADD_TRILINOS)]
    rheo_have_trilinos=yes
    LDADD_TRILINOS=""
    INCLUDES_TRILINOS=""
    LDADD_TRILINOS="${rheo_libs_trilinos}"
    if test x"${rheo_libdir_trilinos}" != x"yes" -a x"${rheo_libdir_trilinos}" != x""; then
      LDADD_TRILINOS="-L${rheo_libdir_trilinos} ${LDADD_TRILINOS}"
    fi
    if test x"${rheo_incdir_trilinos}" = x"yes" -o x"${rheo_incdir_trilinos}" = x""; then
       rheo_incdir_trilinos="/usr/include/trilinos"
    fi
    INCLUDES_TRILINOS="-I${rheo_incdir_trilinos}"
    PREV_CPPFLAGS=$CPPFLAGS
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_TRILINOS} -w"
    [AC_CHECK_HEADER(Ifpack_Preconditioner.h, true, rheo_have_trilinos=no)]
    CPPFLAGS="${PREV_CPPFLAGS}"
    if test x"${rheo_have_trilinos}" = x"yes"; then
      [AC_DEFINE(HAVE_TRILINOS)]
      [AC_MSG_CHECKING(for trilinos library)]
      [AC_MSG_RESULT(${LDADD_TRILINOS})]
    else
      LDADD_TRILINOS=""
      INCLUDES_TRILINOS=""
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_PASTIX}
dnl #
dnl #    Check for the "pastix" sequential or distributed direct solver libraries,
dnl #    depending on the "rheo_use_distributed" shell variable.
dnl #    Defines HAVE_PASTIX and substitues 
dnl #    INCLUDES_PASTIX and LDADD_PASTIX.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_PASTIX],
    [AH_TEMPLATE([HAVE_PASTIX], Defines if you have the pastix distributed direct solver libraries)]
    [AC_SUBST(INCLUDES_PASTIX)]
    [AC_SUBST(LDADD_PASTIX)]
    rheo_have_pastix=yes
    LDADD_PASTIX=""
    INCLUDES_PASTIX=""
    if test x"${rheo_libdir_pastix}" != x"yes" -a x"${rheo_libdir_pastix}" != x""; then
      LDADD_PASTIX="-L${rheo_libdir_pastix}"
    fi
    if test x"${rheo_use_distributed}" = x"yes"; then
      LDADD_PASTIX="${LDADD_PASTIX} -lpastix_mpi_nosmp -lrt"
    else
      LDADD_PASTIX="${LDADD_PASTIX} -lpastix_seq_nosmp -lrt -lpthread"
    fi
    if test x"${rheo_incdir_pastix}" = x"yes" -o x"${rheo_incdir_pastix}" = x""; then
       rheo_incdir_pastix="/usr/include/pastix"
    fi
    INCLUDES_PASTIX="-I${rheo_incdir_pastix}"
    PREV_CPPFLAGS=$CPPFLAGS
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_PASTIX}"
    dnl custom check header for pastix:
    dnl [AC_CHECK_HEADER(pastix.h, true, rheo_have_pastix=no)]
    [AC_MSG_CHECKING(for pastix.h)]
    [AC_TRY_COMPILE(
	    changequote(%%, %%)dnl
	    %%
            extern "C" {
             #define COMPLEXFLOAT_   /* workarroud a compile problem here */
             #define COMPLEXDOUBLE_
             #ifndef HAVE_MPI_H
             # include <mpi.h>
             #else
             # define FORCE_NOMPI
             # define MPI_Comm int
             #endif // HAVE_MPI_H
             #include "pastix.h"
             #include "cscd_utils.h"
             #undef COMPLEXFLOAT_
             #undef COMPLEXDOUBLE_
             #undef FORCE_NOMPI
            }
	    %%,
	    %%
	      int dummy = 0;
	    %%,
	    changequote([, ])dnl
	    true,
	    rheo_have_pastix=no
        )]
    [AC_MSG_RESULT(${rheo_have_pastix})]
    CPPFLAGS="${PREV_CPPFLAGS}"
    if test x"${rheo_have_pastix}" = x"yes"; then
      [AC_DEFINE(HAVE_PASTIX)]
      [AC_MSG_CHECKING(for pastix library)]
      [AC_MSG_RESULT(${LDADD_PASTIX})]
    else
      LDADD_PASTIX=""
      INCLUDES_PASTIX=""
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_MUMPS}
dnl #
dnl #    Check for the "mumps" distributed direct solver libraries.
dnl #    Defines HAVE_MUMPS and substitues 
dnl #    INCLUDES_MUMPS and LDADD_MUMPS.
dnl #	 Requires the MPI and SCOTCH libraries.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_MUMPS],
    [AH_TEMPLATE([HAVE_MUMPS], Defines if you have the mumps distributed direct solver libraries)]
    [AC_SUBST(INCLUDES_MUMPS)]
    [AC_SUBST(LDADD_MUMPS)]
    LDADD_MUMPS=""
    INCLUDES_MUMPS=""
    # ----------------------------
    # check for mumps header
    # ----------------------------
    if test x"${rheo_incdir_mumps}" != x"yes" -a x"${rheo_incdir_mumps}" != x""; then
      INCLUDES_MUMPS="-I${rheo_incdir_mumps}"
    fi
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_MUMPS}"
    [AC_CHECK_HEADER(dmumps_c.h, true, rheo_have_mumps=no)]
    rheo_have_mumps=no
    #echo "MUMPS: rheo_use_distributed=${rheo_use_distributed}"
    if test x"${rheo_use_distributed}" != x"no"; then
      # ----------------------------
      # check for mumps mpi lib
      # ----------------------------
      if test x"${rheo_libs_mumps}" != x"yes" -a x"${rheo_libs_mumps}" != x""; then
        list_libs_mumps="${rheo_libs_mumps}"
      else
	# mumps+scotch actual debian package does not work yet with rheolef
        # list_libs_mumps="-ldmumps_scotch -ldmumps_metis -ldmumps_ptscotch -ldmumps_parmetis -ldmumps"
        #list_libs_mumps="-ldmumps"
        list_libs_mumps="-ldmumps_ptscotch -ldmumps_parmetis -ldmumps_scotch -ldmumps_metis -ldmumps"
      fi
      #echo "MUMPS: list_libs_mumps=${list_libs_mumps}"
      for rheo_libs_mumps in ${list_libs_mumps}; do
        [AC_MSG_CHECKING(for ${rheo_libs_mumps})]
        LDADD_MUMPS="${rheo_libs_mumps}"
        if test x"${rheo_libdir_mumps}" != x"yes" -a x"${rheo_libdir_mumps}" != x""; then
          LDADD_MUMPS="-L${rheo_libdir_mumps} ${LDADD_MUMPS}"
        fi
        PREV_LIBS=$LIBS
        LIBS="${LIBS} ${LDADD_MUMPS} ${LDADD_SCOTCH} ${LDADD_PARMETIS} ${LDADD_SCALAPACK} ${LDADD_BLAS} ${LDADD_MPI}"
        [AC_TRY_LINK(
	    changequote(%%, %%)dnl
	    %%
	        extern "C" {
		  #include <dmumps_c.h>
		}
	    %%,
	    %%
	        dmumps_c(0);
	    %%,
	    changequote([, ])dnl
            rheo_have_mumps=yes,
	    rheo_have_mumps=no
        )]
        LIBS=$PREV_LIBS
        [AC_MSG_RESULT(${rheo_have_mumps})]
        if test x"$rheo_have_mumps" = x"yes"; then
          break;
        fi
      done
      if test x"$rheo_have_mumps" = x"yes"; then
	true
      else
        rheo_use_distributed=no
        [AC_MSG_RESULT(no)]
	[AC_MSG_RESULT(** WARNING: the distributed feature is turned off because libmumps or mpi is not available)]
      fi
    fi
    if test x"${rheo_have_mumps}" = x"yes"; then
      [AC_DEFINE(HAVE_MUMPS)]
    else
      INCLUDES_MUMPS=""
      LDADD_MUMPS=""
    fi
    CPPFLAGS="${PREV_CPPFLAGS}"
    #echo "rheo_have_mumps=${rheo_have_mumps}"
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_MUMPS_WITH_PTSCOTCH}
dnl #
dnl #    Check whether "mumps" is configured with the "ptscotch" support
dnl #    Defines HAVE_MUMPS_WITH_PTSCOTCH
dnl #    and the shell variable rheo_have_mumps_with_ptscotch.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_MUMPS_WITH_PTSCOTCH],
    [AC_MSG_CHECKING(for mumps compiled with ptscotch support)]
    [AH_TEMPLATE([HAVE_MUMPS_WITH_PTSCOTCH], Defines if you have mumps with ptscotch support)]
    PREV_CXXFLAGS=$CXXFLAGS
    PREV_LIBS=$LIBS
    CXXFLAGS="$CXXFLAGS -w ${INCLUDES_MUMPS} ${INCLUDES_MPI}"
    LIBS="$LIBS ${LDADD_MUMPS} ${LDADD_MPI}"
    [AC_RUN_IFELSE([AC_LANG_PROGRAM(
    [ #include <iostream>
      #include <cmath>
      #include <mpi.h>
      #include <dmumps_c.h>
      ],
    [[
    
                  DMUMPS_STRUC_C id;
		  int n = 2;
		  int nz = 2;
		  int irn[] = {1,2};
		  int jcn[] = {1,2};
		  double a[2];
		  double rhs[2];
		  int myid, ierr;
		  ierr = MPI_Init(NULL,NULL);
		  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);
		  // Define A and rhs
		  rhs[0]=1.0;rhs[1]=4.0;
		  a[0]=1.0;a[1]=2.0;
		  // Initialize a MUMPS instance
		  id.job=-1; id.par=1; id.sym=0;id.comm_fortran=MPI_Comm_c2f (MPI_COMM_WORLD);
		  dmumps_c(&id);
		  // Define the problem on the host
		  if (myid == 0) {
		    id.n = n; id.nz =nz; id.irn=irn; id.jcn=jcn;
		    id.a = a; id.rhs = rhs;
		  }
		  // No outputs
		  id.icntl[1-1]=-1; id.icntl[2-1]=-1; id.icntl[3-1]=-1; id.icntl[4-1]=0;
		  id.icntl[28-1] = 2; // parallel ordering
		  id.icntl[29-1] = 1; // ptscotch
		  id.job=6; dmumps_c(&id); /* Call the MUMPS package. */
		  int used_ordering = id.infog[7-1];
		  id.job=-2; dmumps_c(&id); /* Terminate instance */
		  if (myid == 0) {
		    double err = fabs(rhs[0] - 1) + fabs(rhs[1] - 2);
		    //cout << " err = " << err << endl;
		    if (err > 1e-7) return 1;
		  }
		  ierr = MPI_Finalize();
		  //cout << "symbolic: ordering effectively used = " << used_ordering<< endl;
		  return (id.infog[7-1] == 3) ? 0 : 1;
	]])],
        [AC_DEFINE(HAVE_MUMPS_WITH_PTSCOTCH)]
        [rheo_have_mumps_with_ptscotch=yes], 
        [rheo_have_mumps_with_ptscotch=no],
        [rheo_have_mumps_with_ptscotch=undef
        false])]
    
    CXXFLAGS=$PREV_CXXFLAGS
    LIBS=$PREV_LIBS
    [AC_MSG_RESULT(${rheo_have_mumps_with_ptscotch})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_MUMPS_WITH_SCOTCH}
dnl #
dnl #    Check whether "mumps" is configured with the "scotch" support
dnl #    Defines HAVE_MUMPS_WITH_SCOTCH
dnl #    and the shell variable rheo_have_mumps_with_scotch.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_MUMPS_WITH_SCOTCH],
    [AC_MSG_CHECKING(for mumps compiled with scotch support)]
    [AH_TEMPLATE([HAVE_MUMPS_WITH_SCOTCH], Defines if you have mumps with scotch support)]
    PREV_CXXFLAGS=$CXXFLAGS
    PREV_LIBS=$LIBS
    CXXFLAGS="$CXXFLAGS -w ${INCLUDES_MUMPS} ${INCLUDES_MPI}"
    LIBS="$LIBS ${LDADD_MUMPS} ${LDADD_MPI}"
    [AC_RUN_IFELSE(
        [AC_LANG_SOURCE([[
		#include <iostream>
		#include <cmath>
	        #ifdef HAVE_MPI_H
		#include "mpi.h"
	        #endif // HAVE_MPI_H
		#include "dmumps_c.h"
		using namespace std;
		int main(int argc, char ** argv) {
		  DMUMPS_STRUC_C id;
		  int n = 2;
		  int nz = 2;
		  int irn[] = {1,2};
		  int jcn[] = {1,2};
		  double a[2];
		  double rhs[2];
		  int myid=0, ierr;
	        #ifdef HAVE_MPI_H
		  ierr = MPI_Init(&argc, &argv);
		  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	        #endif // HAVE_MPI_H
		  // Define A and rhs
		  rhs[0]=1.0;rhs[1]=4.0;
		  a[0]=1.0;a[1]=2.0;
		  // Initialize a MUMPS instance
		  id.job=-1; id.par=1; id.sym=0;
	        #ifdef HAVE_MPI_H
                  id.comm_fortran=MPI_Comm_c2f (MPI_COMM_WORLD);
	        #endif // HAVE_MPI_H
		  dmumps_c(&id);
		  // Define the problem on the host
		  if (myid == 0) {
		    id.n = n; id.nz =nz; id.irn=irn; id.jcn=jcn;
		    id.a = a; id.rhs = rhs;
		  }
		  // No outputs
		  id.icntl[1-1]=-1; id.icntl[2-1]=-1; id.icntl[3-1]=-1; id.icntl[4-1]=0;
		  id.icntl[28-1] = 1; // sequential ordering
		  id.icntl[7-1]  = 3; // scotch
		  id.job=6; dmumps_c(&id); /* Call the MUMPS package. */
		  int used_ordering = id.infog[7-1];
		  id.job=-2; dmumps_c(&id); /* Terminate instance */
		  if (myid == 0) {
		    double err = fabs(rhs[0] - 1) + fabs(rhs[1] - 2);
		    //cout << " err = " << err << endl;
		    if (err > 1e-7) return 1;
		  }
	        #ifdef HAVE_MPI_H
		  ierr = MPI_Finalize();
	        #endif // HAVE_MPI_H
		  //cout << "symbolic: ordering effectively used = " << used_ordering<< endl;
		  return (id.infog[7-1] == 3) ? 0 : 1;
		}
        ]])],
        [AC_DEFINE(HAVE_MUMPS_WITH_SCOTCH)]
        rheo_have_mumps_with_scotch=yes, 
        rheo_have_mumps_with_scotch=no
    )]
    CXXFLAGS=$PREV_CXXFLAGS
    LIBS=$PREV_LIBS
    [AC_MSG_RESULT(${rheo_have_mumps_with_scotch})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_MUMPS_WITH_PARMETIS}
dnl #
dnl #    Check whether "mumps" is configured with the "parmetis" support
dnl #    Defines HAVE_MUMPS_WITH_PARMETIS
dnl #    and the shell variable rheo_have_mumps_with_parmetis.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_MUMPS_WITH_PARMETIS],
    [AC_MSG_CHECKING(for mumps compiled with parmetis support)]
    [AH_TEMPLATE([HAVE_MUMPS_WITH_PARMETIS], Defines if you have mumps with parmetis support)]
    PREV_CXXFLAGS=$CXXFLAGS
    PREV_LIBS=$LIBS
    CXXFLAGS="$CXXFLAGS -w ${INCLUDES_MUMPS} ${INCLUDES_MPI}"
    LIBS="$LIBS ${LDADD_MUMPS} ${LDADD_MPI}"
    [AC_TRY_RUN(
        changequote(%%, %%)dnl
        %%
		#include <iostream>
		#include <cmath>
	        #ifdef HAVE_MPI_H
		#include "mpi.h"
	        #endif // HAVE_MPI_H
		#include "dmumps_c.h"
		using namespace std;
		int main(int argc, char ** argv) {
		  DMUMPS_STRUC_C id;
		  int n = 2;
		  int nz = 2;
		  int irn[] = {1,2};
		  int jcn[] = {1,2};
		  double a[2];
		  double rhs[2];
		  int myid=0, ierr;
	        #ifdef HAVE_MPI_H
		  ierr = MPI_Init(&argc, &argv);
		  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	        #endif // HAVE_MPI_H
		  // Define A and rhs
		  rhs[0]=1.0;rhs[1]=4.0;
		  a[0]=1.0;a[1]=2.0;
		  // Initialize a MUMPS instance
		  id.job=-1; id.par=1; id.sym=0;
	        #ifdef HAVE_MPI_H
                  id.comm_fortran=MPI_Comm_c2f (MPI_COMM_WORLD);
	        #endif // HAVE_MPI_H
		  dmumps_c(&id);
		  // Define the problem on the host
		  if (myid == 0) {
		    id.n = n; id.nz =nz; id.irn=irn; id.jcn=jcn;
		    id.a = a; id.rhs = rhs;
		  }
		  // No outputs
		  id.icntl[1-1]=-1; id.icntl[2-1]=-1; id.icntl[3-1]=-1; id.icntl[4-1]=0;
		  id.icntl[28-1] = 2; // parallel ordering
		  id.icntl[29-1] = 2; // parmetis
		  id.job=6; dmumps_c(&id); /* Call the MUMPS package. */
		  int used_ordering = id.infog[7-1];
		  id.job=-2; dmumps_c(&id); /* Terminate instance */
		  if (myid == 0) {
		    double err = fabs(rhs[0] - 1) + fabs(rhs[1] - 2);
		    //cout << " err = " << err << endl;
		    if (err > 1e-7) return 1;
		  }
	        #ifdef HAVE_MPI_H
		  ierr = MPI_Finalize();
	        #endif // HAVE_MPI_H
		  //cout << "symbolic: ordering effectively used = " << used_ordering<< endl;
		  return (id.infog[7-1] == 5) ? 0 : 1;
		}
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_MUMPS_WITH_PARMETIS)]
        rheo_have_mumps_with_parmetis=yes, 
        rheo_have_mumps_with_parmetis=no,
        rheo_have_mumps_with_parmetis=undef
        false
    )]
    CXXFLAGS=$PREV_CXXFLAGS
    LIBS=$PREV_LIBS
    [AC_MSG_RESULT(${rheo_have_mumps_with_parmetis})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_MUMPS_WITH_METIS}
dnl #
dnl #    Check whether "mumps" is configured with the "metis" support
dnl #    Defines HAVE_MUMPS_WITH_METIS
dnl #    and the shell variable rheo_have_mumps_with_metis.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_MUMPS_WITH_METIS],
    [AC_MSG_CHECKING(for mumps compiled with metis support)]
    [AH_TEMPLATE([HAVE_MUMPS_WITH_METIS], Defines if you have mumps with metis support)]
    PREV_CXXFLAGS=$CXXFLAGS
    PREV_LIBS=$LIBS
    CXXFLAGS="$CXXFLAGS -w ${INCLUDES_MUMPS} ${INCLUDES_MPI}"
    LIBS="$LIBS ${LDADD_MUMPS} ${LDADD_MPI}"
    [AC_TRY_RUN(
        changequote(%%, %%)dnl
        %%
		#include <iostream>
		#include <cmath>
	        #ifdef HAVE_MPI_H
		#include "mpi.h"
	        #endif // HAVE_MPI_H
		#include "dmumps_c.h"
		using namespace std;
		int main(int argc, char ** argv) {
		  DMUMPS_STRUC_C id;
		  int n = 2;
		  int nz = 2;
		  int irn[] = {1,2};
		  int jcn[] = {1,2};
		  double a[2];
		  double rhs[2];
		  int myid=0, ierr;
	        #ifdef HAVE_MPI_H
		  ierr = MPI_Init(&argc, &argv);
		  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	        #endif // HAVE_MPI_H
		  // Define A and rhs
		  rhs[0]=1.0;rhs[1]=4.0;
		  a[0]=1.0;a[1]=2.0;
		  // Initialize a MUMPS instance
		  id.job=-1; id.par=1; id.sym=0;
	        #ifdef HAVE_MPI_H
                  id.comm_fortran=MPI_Comm_c2f (MPI_COMM_WORLD);
	        #endif // HAVE_MPI_H
		  dmumps_c(&id);
		  // Define the problem on the host
		  if (myid == 0) {
		    id.n = n; id.nz =nz; id.irn=irn; id.jcn=jcn;
		    id.a = a; id.rhs = rhs;
		  }
		  // No outputs
		  id.icntl[1-1]=-1; id.icntl[2-1]=-1; id.icntl[3-1]=-1; id.icntl[4-1]=0;
		  id.icntl[28-1] = 1; // sequential ordering
		  id.icntl[7-1]  = 5; // metis
		  id.job=6; dmumps_c(&id); /* Call the MUMPS package. */
		  int used_ordering = id.infog[7-1];
		  id.job=-2; dmumps_c(&id); /* Terminate instance */
		  if (myid == 0) {
		    double err = fabs(rhs[0] - 1) + fabs(rhs[1] - 2);
		    //cout << " err = " << err << endl;
		    if (err > 1e-7) return 1;
		  }
	        #ifdef HAVE_MPI_H
		  ierr = MPI_Finalize();
	        #endif // HAVE_MPI_H
		  //cout << "symbolic: ordering effectively used = " << used_ordering<< endl;
		  return (id.infog[7-1] == 5) ? 0 : 1;
		}
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_MUMPS_WITH_METIS)]
        rheo_have_mumps_with_metis=yes, 
        rheo_have_mumps_with_metis=no,
        rheo_have_mumps_with_metis=undef
        false
    )]
    CXXFLAGS=$PREV_CXXFLAGS
    LIBS=$PREV_LIBS
    [AC_MSG_RESULT(${rheo_have_mumps_with_metis})]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_GET_MUMPS_VERSION}
dnl #
dnl #    Get "mumps" version
dnl #    Defines MUMPS_VERSION_MAJOR, MUMPS_VERSION_MINOR
dnl #    and the shell variable rheo_mumps_veion.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_GET_MUMPS_VERSION],[
    AC_MSG_CHECKING(for mumps version)
    AH_TEMPLATE([MUMPS_VERSION_MAJOR], Defines mumps version major)
    AH_TEMPLATE([MUMPS_VERSION_MINOR], Defines mumps version minor)
    PREV_CXXFLAGS=$CXXFLAGS
    PREV_LIBS=$LIBS
    CXXFLAGS="$CXXFLAGS -w ${INCLUDES_MUMPS} ${INCLUDES_MPI}"
    LIBS="$LIBS ${LDADD_MUMPS} ${LDADD_MPI}"
    rm -f mumps_version.txt
    AC_TRY_RUN(
        changequote(%%, %%)dnl
        %%
        #include <fstream>
        #include "dmumps_c.h"
        int main() {
          std::ofstream out ("mumps_version.txt");
          out << MUMPS_VERSION << std::endl;
          out.close();
        }
        %%,
        changequote([, ])dnl
        [AC_DEFINE(HAVE_MUMPS_WITH_METIS)]
        rheo_have_mumps_version=yes, 
        rheo_have_mumps_version=no,
        rheo_have_mumps_version=undef
        false
    )
    CXXFLAGS=$PREV_CXXFLAGS
    LIBS=$PREV_LIBS
    if test x"${rheo_have_mumps_version}" = x"yes" && test -f mumps_version.txt; then
      rheo_mumps_version=`cat mumps_version.txt 2>/dev/null`
      rheo_mumps_version_major=`echo ${rheo_mumps_version} | sed -e 's/\..*//'`
      rheo_mumps_version_minor=`echo ${rheo_mumps_version} | sed -e 's/[[^\.]]*\.//' -e 's/\.[[^\.]]*//'`
    fi
    rm -f mumps_version.txt
    if test x"${rheo_mumps_version_major}" = x""; then
                rheo_mumps_version_major="4"; # assume an old version
    fi
    if test x"${rheo_mumps_version_minor}" = x""; then
                rheo_mumps_version_minor="0"
    fi
    AC_MSG_RESULT(${rheo_mumps_version_major}.${rheo_mumps_version_minor})
    AC_DEFINE_UNQUOTED(MUMPS_VERSION_MAJOR,${rheo_mumps_version_major})
    AC_DEFINE_UNQUOTED(MUMPS_VERSION_MINOR,${rheo_mumps_version_minor})
])
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_STD_INITIALIER_LIST}
dnl #
dnl #    Some compilers (e.g. GNU C++ 4.4.x) does not support
dnl #    the std::initializer_list feature.
dnl #    Set the corresponding 
dnl #    shell variable "rheo_have_std_initializer_list"
dnl #    to "yes" and defines HAVE_STD_INITIALIZER_LIST.
dnl #    If not, set the variable no "no".
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_STD_INITIALIZER_LIST],
    [AC_MSG_CHECKING(for std::initializer_list)]
    [AH_TEMPLATE([HAVE_STD_INITIALIZER_LIST], Defines if you have std::initializer_list, as c++ 2011 standard)]
    [AC_TRY_COMPILE(
        changequote(%%, %%)dnl
	%%
           // model for
           // vec u;
           // vec U = { u, 0 };
           // csr A = { {a,b}, {trans(b), 0} };
           // csr B = { {a,u}, {trans(u), 0} };
           // where trans(vec) is a special wraper vec_trans juste used for 
           // this circonstance
           #include <initializer_list>
           #include <iostream>
           #include <list>
           // union of all possible types:
           // will be : vec<T> or T
           // or, for csr : csr<T>, vec<T> or T
           struct X {
            int i;
            double d;
            bool is_int;
            X(int j =0) : i(j), d(), is_int(true) {}
            X(double e) : i(), d(e), is_int(false) {}
            friend std::ostream& operator<< (std::ostream& o, const X& x) {
             if (x.is_int) return o << "i"<<x.i; else return o << "d"<<x.d;
            }
           };
           template <class T>
           struct U {
            std::list<T> u;
            U () : u() {}
            U (const std::initializer_list<T>& liste) : u() {
               for(const T* i = liste.begin(); i != liste.end(); ++i) {
                   u.push_back(*i);
               }
            }
            friend std::ostream& operator<< (std::ostream& o, const U& v) {
               std::cout << "{";
               for(typename std::list<T>::const_iterator i = v.u.begin(); i != v.u.end(); ++i) {
                   std::cout << *i << " ";
               }
               return std::cout << "}";
             }
           };
	%%,
	%%
           U<X> u = {1,2.3,4};
           std::cout << "u="<<u<<std::endl;
           U<U<X>> a = {{1,2.3},u};
           std::cout << "a="<<a<<std::endl;
    	   return 0;
	%%,
	changequote([, ])dnl
	rheo_have_std_initializer_list=yes
	[AC_DEFINE(HAVE_STD_INITIALIZER_LIST)],
	rheo_have_std_initializer_list=no
    )]
    [AC_MSG_RESULT($rheo_have_std_initializer_list)]
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_GMP}
dnl #
dnl #    Check for the "gmp" gnu multi-precision library.
dnl #    Defines HAVE_GMP and substitues 
dnl #    INCLUDES_GMP and LDADD_GMP.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_GMP],
    [AH_TEMPLATE([HAVE_GMP], Defines if you have the GMP library)]
    [AC_SUBST(INCLUDES_GMP)]
    [AC_SUBST(LDADD_GMP)]
    rheo_have_gmp=yes
    INCLUDES_GMP=""
    if test x"${rheo_incdir_gmp}" != x"yes" -a x"${rheo_incdir_gmp}" != x""; then
      INCLUDES_GMP="-I${rheo_incdir_gmp}"
    fi
    LDADD_GMP="${rheo_libs_gmp}"
    if test x"${rheo_libdir_gmp}" != x"yes" -a x"${rheo_libdir_gmp}" != x""; then
      LDADD_GMP="-L${rheo_libdir_gmp} ${LDADD_GMP}"
    fi
    PREV_CPPFLAGS=$CPPFLAGS
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_GMP}"
    [AC_CHECK_HEADER(gmp.h, true, rheo_have_gmp=no)]
    CPPFLAGS="${PREV_CPPFLAGS}"
    if test x"${rheo_have_gmp}" = x"yes"; then
      [AC_MSG_CHECKING(for gmp library)]
      [AC_DEFINE(HAVE_GMP)]
      [AC_MSG_RESULT(${LDADD_GMP})]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_MPFR}
dnl #
dnl #    Check for the "mpfr" gnu multi-precision library.
dnl #    Defines HAVE_MPFR and substitues 
dnl #    INCLUDES_MPFR and LDADD_MPFR.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_MPFR],
    [AH_TEMPLATE([HAVE_MPFR], Defines if you have the MPFR library)]
    [AC_SUBST(INCLUDES_MPFR)]
    [AC_SUBST(LDADD_MPFR)]
    rheo_have_mpfr=yes
    INCLUDES_MPFR=""
    if test x"${rheo_incdir_mpfr}" != x"yes" -a x"${rheo_incdir_mpfr}" != x""; then
      INCLUDES_MPFR="-I${rheo_incdir_mpfr}"
    fi
    LDADD_MPFR="${rheo_libs_mpfr}"
    if test x"${rheo_libdir_mpfr}" != x"yes" -a x"${rheo_libdir_mpfr}" != x""; then
      LDADD_MPFR="-L${rheo_libdir_mpfr} ${LDADD_MPFR}"
    fi
    PREV_CPPFLAGS=$CPPFLAGS
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_MPFR}"
    [AC_CHECK_HEADER(mpfr.h, true, rheo_have_mpfr=no)]
    CPPFLAGS="${PREV_CPPFLAGS}"
    if test x"${rheo_have_mpfr}" = x"yes"; then
      [AC_MSG_CHECKING(for mpfr library)]
      [AC_DEFINE(HAVE_MPFR)]
      [AC_MSG_RESULT(${LDADD_MPFR})]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_CGAL}
dnl #
dnl #    Check for the "cgal" computational geometry library.
dnl #    Defines HAVE_CGAL and substitues 
dnl #    INCLUDES_CGAL and LDADD_CGAL.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_CGAL],
    [AH_TEMPLATE([HAVE_CGAL], Defines if you have the CGAL library)]
    [AC_SUBST(INCLUDES_CGAL)]
    [AC_SUBST(INCLUDES_CGAL_INTERN)]
    [AC_SUBST(LDADD_CGAL)]
    rheo_have_cgal=yes
    INCLUDES_CGAL=""
    INCLUDES_CGAL_INTERN=""
    if test x"${rheo_cxx}" = x"gnu"; then
      INCLUDES_CGAL_INTERN="-frounding-math"
    fi
    if test x"${rheo_incdir_cgal}" != x"yes" -a x"${rheo_incdir_cgal}" != x""; then
      INCLUDES_CGAL="-I${rheo_incdir_cgal}"
    fi
    INCLUDES_CGAL="${INCLUDES_CGAL} ${INCLUDES_GMP} ${INCLUDES_MPFR}"
    PREV_CPPFLAGS=$CPPFLAGS
    CPPFLAGS="${CPPFLAGS} ${INCLUDES_BOOST} ${INCLUDES_CGAL}"
    [AC_CHECK_HEADER(CGAL/Simple_cartesian.h, true, rheo_have_cgal=no)]
    CPPFLAGS="${PREV_CPPFLAGS}"
    # new CGAL requires nothing      as LIB
    # old CGAL requires -lCGAL -lgmp as LIB
    if test x"$rheo_have_cgal" = x"yes"; then
      [AC_MSG_CHECKING(for CGAL library)]
      for libs in arg nothing something; do
        [case "$libs" in
          "nothing")   LDADD_CGAL="-lgmp";;
          "something") LDADD_CGAL="-lCGAL -lgmp";;
          "arg")     if test x"$rheo_libs_cgal" != x""; then
                        LDADD_CGAL="$rheo_libs_cgal"
                      else 
                        continue
                      fi;;
        esac]
        if test x"${LDADD_CGAL}" != x"" -a x"${rheo_libdir_cgal}" != x"yes" -a x"${rheo_libdir_cgal}" != x""; then
          LDADD_CGAL="-L${rheo_libdir_cgal} ${LDADD_CGAL}"
        fi
        rm -f core; touch core; chmod a-w core
        PREV_LIBS="$LIBS"
        LIBS="${LIBS} ${LDADD_CGAL}"
        PREV_CXXFLAGS="$CXXFLAGS"
        CXXFLAGS="${CPPFLAGS} ${INCLUDES_BOOST} ${INCLUDES_CGAL}"
        [AC_TRY_LINK(
  	    changequote(%%, %%)dnl
  	    %%
                #include <iostream>
  	        #include <CGAL/Simple_cartesian.h>
  	    %%,
  	    %%
                typedef CGAL::Simple_cartesian<double> Kernel;
                typedef Kernel::Point_2 Point_2;
       		Point_2 p(1,1);
  	        std::cout << "p = " << p << std::endl;
  	    %%,
  	    changequote([, ])dnl
  	    rheo_have_cgal=yes,
  	    rheo_have_cgal=no
        )]
        rm -f core
        LIBS="$PREV_LIBS"
        CXXFLAGS="$PREV_CXXFLAGS"
        if test x"$rheo_have_cgal" = x"yes"; then
          break
        fi
      done
    fi
    if test x"${rheo_have_cgal}" = x"yes"; then
      [AC_MSG_RESULT(${LDADD_CGAL})]
      [AC_DEFINE(HAVE_CGAL)]
    else
      [AC_MSG_RESULT(no)]
    fi
)
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_CHECK_FLOAT128}
dnl #
dnl #    Check for the "float128" arithmetic (boost multiprecision and GNU quadmath).
dnl #    Defines HAVE_FLOAT128 and substitues 
dnl #    INCLUDES_FLOAT128 and LDADD_FLOAT128.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_FLOAT128],
    [AH_TEMPLATE([HAVE_FLOAT128], Define when using the boost multiprecision with float128 GNU quadmath)]
    [AC_SUBST(LDADD_FLOAT128)]
    [AC_SUBST(INCLUDES_FLOAT128)]
    [AC_SUBST(QD_EXT)]
    [AC_DEFINE(HAVE_FLOAT128)]
    INCLUDES_FLOAT128=""
    LDADD_FLOAT128="-lquadmath"
    QD_EXT=".float128"
    [AC_MSG_CHECKING(for extended arithmetic library)]
    [AC_MSG_RESULT(${rheo_float_ext})]
)
dnl -----------------------------------------------------------------------------
dnl check for variadic template support
dnl the associated macro is called HAVE_VARIADIC_TEMPLATE
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_CHECK_VARIADIC_TEMPLATE],[
  AC_CACHE_CHECK([for variadic template], rheo_cv_have_variadic_template, [
    AC_LANG_PUSH([C++])
    AC_RUN_IFELSE([
      AC_LANG_PROGRAM([
        #include<cassert>
        template<typename... T> int addints(T... x);
        int add_ints() { return 0; }
        template <typename T1, typename... T>
        int add_ints(T1 t1, T... t) {
          return t1 + add_ints(t...);
        }
      ],
      [
        assert(5 == add_ints(9,3,-5,-2));
        return 0;
      ])],
      rheo_cv_have_variadic_template=yes,
      rheo_cv_have_variadic_template=no)
    AC_LANG_POP
    ])
  if test "x${rheo_cv_have_variadic_template}" = x"yes"; then
    AC_DEFINE(HAVE_VARIADIC_TEMPLATE, 1, [Define to 1 if variadic template is supported])
  fi
])
dnl -----------------------------------------------------------------------------
dnl #
dnl #    @code{RHEO_PARAVIEW_GET_VERSION}
dnl #
dnl #    Check whether "paraview" supports the high-order elements visualization. 
dnl #    Defines HAVE_PARAVIEW_VERSION_MAJOR and HAVE_PARAVIEW_VERSION_MINOR.
dnl -----------------------------------------------------------------------------
AC_DEFUN([RHEO_PARAVIEW_GET_VERSION],[
  AC_CHECK_PROGS(PARAVIEW, paraview)
  if test x"$PARAVIEW" = x""; then
    # no paraview yet installed at configure time: hope to have a recent version at run time
    # have_paraview_high_order_element="yes"
    paraview_version_major="5"; # assume a sufficiently recent version
    paraview_version_minor="7"
  else
    AC_MSG_CHECKING(for $PARAVIEW version)
    dnl old:
    dnl paraview_version=`DISPLAY= pvbatch --version 2>/dev/null | grep 'version' | head -1 | awk '{printf $NF}'`
    dnl paraview_version=`echo ${paraview_version} | sed -e 's/[[A-Za-z ]]*//'`
    paraview_version=`DISPLAY= pvbatch --version 2>&1 | grep 'version' | head -1|awk '{print $NF}'`
    paraview_version=`echo ${paraview_version} | sed -e 's/[[a-z ]]*//'`
    paraview_version_major=`echo ${paraview_version} | sed -e 's/\..*//'`
    paraview_version_minor=`echo ${paraview_version} | sed -e 's/[[^\.]]*\.//' -e 's/\.[[^\.]]*//'`
    if test x"${paraview_version_major}" = x""; then
                paraview_version_major="5"; # assume a sufficiently recent version
    fi
    if test x"${paraview_version_minor}" = x""; then
                paraview_version_minor="7"
    fi
    AC_MSG_RESULT(${paraview_version_major}.${paraview_version_minor})
  fi
  AH_TEMPLATE([PARAVIEW_VERSION_MAJOR], [Defines the paraview major version])
  AH_TEMPLATE([PARAVIEW_VERSION_MINOR], [Defines the paraview minor version])
  AC_DEFINE_UNQUOTED(PARAVIEW_VERSION_MAJOR,${paraview_version_major})
  AC_DEFINE_UNQUOTED(PARAVIEW_VERSION_MINOR,${paraview_version_minor})
])
dnl -----------------------------------------------------------------------------
dnl #SEE ALSO:
dnl #    "autoconf"(1), "automake"(1), 
dnl #    "libtool"(1), "make"(1)
dnl #AUTHOR:
dnl #      Pierre Saramito
dnl #	 | Pierre.Saramito@imag.fr
dnl #    LMC-IMAG,
dnl #    38041 Grenoble cedex 9, France
dnl #
dnl #DATE: 15 january 1999
dnl #END:
dnl -----------------------------------------------------------------------------
