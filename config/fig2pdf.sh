#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# convert .fig to .pdf 
# via combined tex + pdf fig2dev
# => fig can be included via includegraphics
#
# author: Pierre Saramito
#
# changelog:
# * 16 may 2019 : fix pdfcrop with "--resolution 300"
#
# -------------
# utility
# -------------
progname=$0
my_eval () {
  run_status=0
  command="$*"
  echo "! $command" 1>&2
  eval "$command"
  if test $? -ne 0; then 
    echo "$progname: command failed" 1>&2
    exit 1
  fi
}
# -------------
# main
# -------------
suffix1="fig"
suffix2="wfig"
if test $# -eq 0; then
  echo "usage: $0 file[.${suffix1}|.${suffix2}]" 1>&2
  exit 1
fi
file=$1
#echo "file=$file" 1>&2
filename=`expr ${file} : '.*/\(.*\)' \| ${file}`
x=`expr ${filename} : '\(.*\).fig' \| ${filename}`
if test "${filename}" = "${x}"; then
  x=`expr ${filename} : '\(.*\).wfig' \| ${filename}`
fi
#echo "x=${x}" 1>&2
if test "${filename}" = "${x}"; then
  echo "$0: invalid suffix for $file"
  exit 1
fi
if test ! -f $file; then
  echo "$0: file ${file} not found" 1>&2
  exit 0
fi
dir=`expr $file : '\(.*\)/.*' \| .`
#echo "dir=$dir"; exit 0
my_eval "fig2dev -L pdftex_t -p $x.pdf ${file} > $x.tex"
my_eval "fig2dev -L pdftex             ${file} > $x.pdf"

#w=210;  h=297; # A4
w=297; h=420; # A3
cat > ${x}_wrap.tex << EOF
\documentclass{letter}
\usepackage[papersize={${w}mm,${h}mm},includehead,includefoot,%
inner=0cm,outer=0cm,%
top=0cm,bottom=0cm,foot=0cm%
]{geometry}
\usepackage{graphicx}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathrsfs} % \mathscr{D} better than \mathcal{D}
\newcommand{\red}[1]  {{\color[rgb]{1,0,0}{{#1}}}}
\newcommand{\blue}[1] {{\color[rgb]{0,0,1}{{#1}}}}
\newcommand{\nni}{\ni\!\!\!\!\!/\ }
\begin{document}
\begin{center}
\input{${x}.tex}
\end{center}
\thispagestyle{empty}
\end{document}
EOF

echo "! file ${x}_wrap.tex created" 1>&2
my_eval pdflatex ${x}_wrap.tex
my_eval pdfcrop --resolution 300 ${x}_wrap.pdf && mv ${x}_wrap-crop.pdf $x.pdf 
my_eval rm -f ${x}_wrap.pdf
my_eval rm -f ${x}_wrap.aux ${x}_wrap.tex ${x}.tex ${x}_wrap.tex ${x}_wrap.log
if test "$dir" != "."; then
  my_eval "mv ${x}.pdf $dir"
fi
echo "$0: output written to ${x}.pdf" 1>&2

