#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
#
# put a raw copy of FlexLexer.h instead of a #include <FlexLexer.h>
# for portability purpose
# 
# flexfix.sh usage:
#   flex -+ -i $< -t -o file.c | flexfix.sh /usr/local/include/FlexLexer.h > file.c"; \
#
# note: with recent flex version >= 2.6.4 there is no more FlexLexer.h
# and this fix is no more need : in that case, $1="" i.e. $#=0
# and we skip the fix.
#
TMPDIR=${TMPDIR-"/tmp"}
sedfile=${TMPDIR}/flexfix-$$.sed

if test $# -gt 0 && test -f $1; then
  cat > $sedfile << EOF
s/class istream;/#include <iostream.h>/
/^#include <FlexLexer.h>/ {
r $1
d
}
EOF
  sed -f $sedfile
  /bin/rm -f $sedfile
else 
  cat
fi
