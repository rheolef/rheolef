dnl
dnl This file is part of Rheolef.
dnl
dnl Copyright (C) 2000-2009 Pierre Saramito 
dnl
dnl Rheolef is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl 
dnl Rheolef is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with Rheolef; if not, write to the Free Software
dnl Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
dnl 
# ------------------------------------------------------------
# Choosing debugging and/or optimization flags for compilation
# ------------------------------------------------------------


dnl moved in configure.ac :
dnl AC_ARG_ENABLE(profiling,[  --enable-profiling	  turn on profiling])
dnl AC_ARG_ENABLE(optim,    [  --enable-optim	Turn on compiler optimization])
dnl AC_ARG_ENABLE(generic,  [  --enable-generic	  turn off hardware-dependant optimization options])

if test "$enable_profiling" = yes
then
	CXXFLAGS="$CXXFLAGS -pg"
	LDFLAGS="$LDFLAGS -pg"
fi

# Debug mode (no optimisation)
# ----------------------------

AC_MSG_CHECKING(whether to generate debugging information)



if test "$enable_debug" = yes; then
	AC_MSG_RESULT(yes)
	# Autoconf always chooses -O2. -O2 in gcc makes some functions
	# disappear. This is not ideal for debugging. And when we optimize, we
	# do not use -O2 anyway.
	CFLAGS="`echo $CFLAGS | sed -e 's/-O2//g' -e 's/[ \t]-g[ \t]/ /'`"
	FFLAGS="`echo $FFLAGS | sed -e 's/-O2//g' -e 's/[ \t]-g[ \t]/ /'`"
	CXXFLAGS="`echo $CXXFLAGS | sed -e 's/-O2//g' -e 's/[ \t]-g[ \t]/ /'`"
	CFLAGS="$CFLAGS -g"
	FFLAGS="$FFLAGS -g"
	CXXFLAGS="$CXXFLAGS -g"
else
	AC_MSG_RESULT(no)
	# No debugging information in optimized code
	CFLAGS="`echo $CFLAGS | sed 's/[ \t]-g[ \t]/ /g'`"
	FFLAGS="`echo $FFLAGS | sed 's/[ \t]-g[ \t]/ /g'`"
	CXXFLAGS="`echo $CXXFLAGS | sed 's/[ \t]-g[ \t]/ /g'`"
fi

# Hardware-independant optimization
# ---------------------------------

if test "$enable_debug" != yes -a "$enable_optim" != no;
then
	CHECK_COMPILE_FLAG(C,-O3,CFLAGS)
	CHECK_COMPILE_FLAG(C++,-O3,CXXFLAGS)
dnl	CHECK_COMPILE_FLAG(Fortran 77,-O3,FFLAGS)
fi

#echo "OPTIM: enable_debug=$enable_debug"
#echo "OPTIM: enable_optim=$enable_optim"
#echo "OPTIM: enable_generic=$enable_generic"

# Generic code
if test "$enable_debug" != yes \
    -a "$enable_optim" != no \
    -a "$enable_generic" = yes
then
	CHECK_COMPILE_FLAG(C,-mcpu=common,CFLAGS)
	CHECK_COMPILE_FLAG(C++,-mcpu=common,CXXFLAGS)
dnl	CHECK_COMPILE_FLAG(Fortran 77,-mcpu=common,FFLAGS)
fi

# Hardware-dependant optimization
# -------------------------------

if test "$enable_debug" != yes \
    -a "$enable_optim" != no \
    -a "$enable_generic" != yes
then

    # MacOS X Darwin
    if test -x /usr/bin/hostinfo
	then
        

	# If we are on MacOS X to choise the optimisaztion 
	AC_MSG_CHECKING(GCC version)

        ff_gcc4=`$CC  --version |awk  ' NR==1 {print $3}'|sed -e 's/\..*$//'` 
	AC_MSG_RESULT($ff_gcc4)

	# At the moment, we do not know how to produce correct
	# optimizated code on G5.
	AC_MSG_CHECKING(PowerPC architecture)
	ff_machine=`/usr/bin/machine`
        ff_fast="-O3"
	if test `uname` = Darwin 
	    then
	    # Optimization flags: -fast option do not work because the
	    # -malign-natural flags create wrong IO code
            if test  "$ff_gcc4" = 4 
	    then
               ff_fast='-fast -fPIC'
            else
	      ff_fast='-fPIC -O3 -funroll-loops -fstrict-aliasing -fsched-interblock -falign-loops=16 -falign-jumps=16 -falign-functions=16 -falign-jumps-max-skip=15 -falign-loops-max-skip=15 -ffast-math  -mpowerpc-gpopt -force_cpusubtype_ALL -fstrict-aliasing  -mpowerpc64 '
	    fi
	fi        


        # CPU detection

	#echo "FF_MACHINE = $ff_machine"
	case $ff_machine  in
	  ppc7450) # G4
		ff_fast="$ff_fast -mtune=G4 -mcpu=G4";;
          ppc970) # G5 
	        # remove -fstrict-aliasing on G5 to much optim the
	        # code cash in GC
		ff_fast="`echo $ff_fast -mtune=G5 -mcpu=G5| sed 's/-fstrict-aliasing //g'`";;
          ppc*) # G3 ????
	       ff_fast="-O3";;
	  i486)
	    ff_fast="-O3 -march=pentium4";;
	  *)
	    echo "** WARNING: machine-dependent optimization disable (cannot determine apple cpu type)"
	    ff_fast="-O3";;
	 esac


	AC_MSG_RESULT($ff_fast)

        CHECK_COMPILE_FLAG(C,$ff_fast,CFLAGS)
	CHECK_COMPILE_FLAG(C++,$ff_fast,CXXFLAGS)
dnl	CHECK_COMPILE_FLAG(Fortran 77,$ff_fast,FFLAGS)


    # Linux
    elif test -f /proc/cpuinfo
	then

	# Specific processors
	machine="`uname -m`"
	proc_type=unknown
	ff_optim_type=
	if test `grep 'Pentium III (Coppermine)' /proc/cpuinfo|wc -l` -gt 0
	    then
	    proc_type=pentium3
	    ff_optim_type=-P3
	elif test `grep 'Intel(R) Pentium(R) III ' /proc/cpuinfo|wc -l` -gt 0
	    then
	    proc_type=pentium3
	    ff_optim_type=-P3
	elif test `grep 'Intel(R) Pentium(R) 4 ' /proc/cpuinfo|wc -l` -gt 0
	    then
	    proc_type=pentium4
	    ff_optim_type=-P4
	elif test `grep -i 'Intel(R) Xeon(TM) CPU' /proc/cpuinfo|wc -l` -gt 0 || \
	     test `grep -i 'Intel(R) Xeon(R) CPU'  /proc/cpuinfo|wc -l` -gt 0; then
	    if test "$machine" = "x86_64"; then
               proc_type="barcelona"
            else
               proc_type="pentium4"
	    fi
            ff_optim_type=-P4
	elif test `grep 'AMD Athlon(tm) Processor' /proc/cpuinfo|wc -l` -gt 0
	    then
	    proc_type=athlon
	    ff_optim_type=-Athlon
	elif test `grep 'AMD Athlon(tm) XP' /proc/cpuinfo|wc -l` -gt 0
	    then
	    proc_type=athlon-xp
	    ff_optim_type=-AthlonXP
	fi

	echo "recognized architecture for optimization... $proc_type"

	if test "$proc_type" != unknown
	    then
	    CHECK_COMPILE_FLAG(C,-march=$proc_type,CFLAGS)
	    CHECK_COMPILE_FLAG(C++,-march=$proc_type,CXXFLAGS)
dnl	    CHECK_COMPILE_FLAG(Fortran 77,-march=$proc_type,FFLAGS)
	fi

	# If we did not find a processor type (this happens with
	# cygwin), try and select separate capabilities instead.

	if test "$proc_type" = unknown
	    then
	    if test `grep -e '^flags.*mmx' /proc/cpuinfo|wc -l` -gt 0
		then
		CHECK_COMPILE_FLAG(C,-mmmx,CFLAGS)
		CHECK_COMPILE_FLAG(C++,-mmmx,CXXFLAGS)
dnl		CHECK_COMPILE_FLAG(Fortran 77,-mmmx,FFLAGS)
	    fi
	    if test `grep -e '^flags.*sse ' /proc/cpuinfo|wc -l` -gt 0
		then
		CHECK_COMPILE_FLAG(C,-msse,CFLAGS)
		CHECK_COMPILE_FLAG(C++,-msse,CXXFLAGS)
dnl		CHECK_COMPILE_FLAG(Fortran 77,-msse,FFLAGS)
	    fi
	    if test `grep -e '^flags.*sse2' /proc/cpuinfo|wc -l` -gt 0
		then
		CHECK_COMPILE_FLAG(C,-msse2,CFLAGS)
		CHECK_COMPILE_FLAG(C++,-msse2,CXXFLAGS)
dnl		CHECK_COMPILE_FLAG(Fortran 77,-msse2,FFLAGS)
	    fi
	    if test `grep -e '^flags.*3dnow' /proc/cpuinfo|wc -l` -gt 0
		then
		CHECK_COMPILE_FLAG(C,-m3dnow,CFLAGS)
		CHECK_COMPILE_FLAG(C++,-m3dnow,CXXFLAGS)
dnl		CHECK_COMPILE_FLAG(Fortran 77,-m3dnow,FFLAGS)
	    fi
	fi
    fi
fi

# Defines a variable containing the optimization type, to be used in
# binary archive names. It may be empty if only generic optimization
# is used.

AC_SUBST(OPTIM_TYPE,$ff_optim_type)
